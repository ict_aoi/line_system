<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DistribusiModel extends CI_Model {

	public function getBundleLineValid($barcode_id='', $source)
	{
		// if($source == 'sds'){
		// 	$this->dist->where('barcode_id', $barcode_id);
		// 	return $this->dist->get('bundle_line_valid_new');			
		// } else 
		if ($source == 'cdms_new'){					
			$this->cdms_new->where('barcode_id', $barcode_id);
			return $this->cdms_new->get('v_supply_line');
		}
	}

	// public function getFBundleLineValid($barcode_id)
	// {
	// 	/*query utk distribusi sesuai planing*/
	// 	$query = "select * from bundle_line_valid_new($barcode_id, '$line')";
	// 	return $this->dist->query($query);
	// 	/*$this->db->where('barcode_id', $barcode_id);
	// 	return $this->db->get('bundle_line_valid');*/
	// }

	public function cekDistribusi($poreference, $size, $cutnum, $start, $end, $style, $article, $lineid, $source)
	{	
		$this->db->where('poreference', $poreference);
		$this->db->where('line_id', $lineid);
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where('article', $article);
		$this->db->where('style', $style);
		$this->db->where('size', $size);
		$this->db->where('cut_num', $cutnum);
		$this->db->where('start_num', $start);
		$this->db->where('start_end', $end);
		$this->db->where('source', $source);
		return $this->db->get('distribusi');
	}

	public function cekDetail($dist_id, $barcode){
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where('distribusi_id', $dist_id);
		$this->db->where('barcode_id', $barcode);
		return $this->db->get('distribusi_detail');
	}

	public function cekBarcode($barcode_id='')
	{
		$this->db->where('line_id', $this->session->userdata('line_id'));
		$this->db->where('delete_at is null');
		$this->db->where('barcode_id', $barcode_id);
		return $this->db->get('distribusi_detail_view');
	}

	public function cekDistribusiDetail($barcode_id)
	{
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where('barcode_id', $barcode_id);
		return $this->db->get('distribusi_detail');
	}

	public function cekComplete($poreference,$size,$cut_num,$start,$end,$style,$article,$sources)
	{
		// $cond = "'".$style."' ~ CONCAT(E'^',TRIM(style),CASE WHEN set_type='BOTTOM' THEN E'[\\\-\\\_](BOT|BOTTOM)' WHEN set_type='TOP' THEN E'[\\\-\\\_]TOP' WHEN set_type='INNER' THEN E'[\\\-\\\_](INNER|INN)' WHEN set_type='OUTER' THEN E'[\\\-\\\_](OUTER|OUT)' ELSE NULL END,E'$')";
		// $this->dist->where($cond,NULL,FALSE);
		
		// if($sources == 'sds'){
		// 	if(strpos($style, '-BOT')>1) {
		// 		$sety = 'BOTTOM';
		// 	} else if (strpos($style, '-TOP')>1) {
		// 		$sety = 'TOP';
		// 	} else{
		// 		$sety = '0';
		// 	}			
		// 	$where = array(
		// 		'poreference' => $poreference, 
		// 		'cut_num'     => $cut_num, 
		// 		'start_num'   => $start, 
		// 		'end_num'     => $end, 
		// 		'article'     => $article, 
		// 		'size'        => $size,
		// 		'set_type' 	  => $sety
		// 	);
		// 	$this->dist->where($where);
		// 	return $this->dist->get('line_syst_upd_new');
			
		// } else
		 if($sources == 'cdms_new'){
			if(strpos($style, '-BOT')>1) {
				$sety = 'BOTTOM';
				$style = str_replace('-BOT', '', $style);
			} else if (strpos($style, '-TOP')>1) {
				$sety = 'TOP';
				$style = str_replace('-TOP', '', $style);
			} else{
				$sety = 'Non';
			}

			// var_dump($style, $sety); die();
			// $where = array(
			// 	'poreference' => $poreference, 
			// 	'cut_num'     => $cut_num, 
			// 	'start_no'    => $start, 
			// 	'end_no'      => $end, 
			// 	'article'     => $article, 
			// 	'size'        => $size,
			// 	'set_type' 	  => $sety				
			// );
			// $this->cdms_new->where($where);
			// $this->cdms_new->where('scan_dist is NOT NULL');
			// return $this->cdms_new->get('v_supply_line');

			$where = array(
				'poreference' => $poreference
			);

			$this->db->where($where);
			$pobuyer = $this->db->get('po_buyers');

			return $this->cdms_new->query("SELECT * FROM f_qty_komponen('".$poreference."', '".$style."', '".$article."', '".$size."', '".$pobuyer->row_array()['season']."', '".$cut_num."', '".$sety."', ".$start.", ".$end.") where qty_komponen is not null");
		}
	}	

	public function getDistribusi($poreference,$size,$cut_num,$start,$end,$style,$article,$lineid)
	{
		$where = array(
			'poreference' => $poreference, 
			'cut_num'     => $cut_num, 
			'start_num'   => $start, 
			'start_end'   => $end, 
			'style'       => $style,
			'article'     => $article,  
			'size'        => $size,
			'line_id'     => $lineid 
		);
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where($where);
		return $this->db->get('distribusi');
	}

	public function getHeader($poreference,$style,$article)
	{
		$this->db->where('isactive', 't');
		$this->db->where('line_id', $this->session->userdata('line_id'));
		$this->db->where('poreference', $poreference);
		$this->db->where('article', $article);
		$this->db->where('style', $style);
		return $this->db->get('inline_header');
	}

	public function getHeaderSize($po,$style,$size,$header_id)
	{
		$this->db->where('size', $size);
		$this->db->where('poreference', $po);
		$this->db->where('style', $style);
		$this->db->where('inline_header_id', $header_id);
		return $this->db->get('inline_header_size');
	}

	public function getDetailBundle($poreference, $size, $start_num, $start_end, $cut_num, $style, $article, $source)
	{
		// $cond = "'".$style."' ~ CONCAT(E'^',TRIM(style),CASE WHEN set_type='BOTTOM' THEN E'[\\\-\\\_](BOT|BOTTOM)' WHEN set_type='TOP' THEN E'[\\\-\\\_]TOP' WHEN set_type='INNER' THEN E'[\\\-\\\_](INNER|INN)' WHEN set_type='OUTER' THEN E'[\\\-\\\_](OUTER|OUT)' ELSE NULL END,E'$')";
		// $this->dist->where($cond,NULL,FALSE);

		// if($source == 'sds'){
		// 	if(strpos($style, '-BOT')>1) {
		// 		$sety = 'BOTTOM';
		// 	} else if (strpos($style, '-TOP')>1) {
		// 		$sety = 'TOP';
		// 	} else{
		// 		$sety = '0';
		// 	}
	
		// 	$this->dist->where('set_type', $sety);
		// 	$this->dist->where('cut_num', $cut_num);
		// 	$this->dist->where('poreference', $poreference);
		// 	$this->dist->where('article', $article);
		// 	$this->dist->where('size', $size);
		// 	$this->dist->where('start_num', $start_num);
		// 	$this->dist->where('end_num', $start_end);
		// 	$data 	= $this->dist->get('bundle_line_valid_new');
		// 	$source = 'sds';

		// } else
		if($source == 'cdms_new'){
			if(strpos($style, '-BOT')>1) {
				$setype = 'BOTTOM';
			} else if (strpos($style, '-TOP')>1) {
				$setype = 'TOP';
			} else{
				$setype = 'Non';
			}
			
			$query = 'SELECT 
						bundle_header.season,
						bundle_header."style",
						bundle_detail.set_type,
						bundle_header.poreference,
						bundle_header.article,
						bundle_header.cut_num,
						bundle_header."size",
						bundle_header.factory_id,
						bundle_detail.barcode_group as barcode_id,
						bundle_detail.komponen_name,
						bundle_detail.qty,
						bundle_detail.start_no,
						bundle_detail.end_no 
					FROM bundle_header
					LEFT JOIN bundle_detail ON bundle_detail.bundle_header_id = bundle_header.id
					WHERE bundle_detail.set_type = ? 
					AND bundle_header.poreference = ? 
					AND bundle_header.article = ? 
					AND bundle_header."size" = ? 
					AND bundle_header.cut_num = ? 
					AND bundle_detail.start_no = ? 
					AND bundle_detail.end_no = ?';
					
			$data = $this->cdms_new->query($query, array($setype, $poreference, $article, $size, $cut_num, $start_num, $start_end));		
			$source = 'cdms_new';			
		}

		return array('data' => $data, 'source' => $source);
	}

	public function cekAvailable($poreference,$style,$size,$article)
	{
		$where = array(
			'line_id'     => $this->session->userdata('line_id'),
			'poreference' => $poreference, 
			'style'       => $style, 
			'article'     => $article, 
			'size'        => $size 
		);
		$this->db->where($where);
		return $this->db->get('statuspo');
	}

	public function cekStatusHandOver($distribusi_id)
	{
		$this->db->where('distribusi_id', $distribusi_id);
		$all = $this->db->get('distribusi_detail_view')->num_rows();

		$this->db->where('hand_over', 't');
		$this->db->where('distribusi_id', $distribusi_id);
		$hand_over = $this->db->get('distribusi_detail_view')->num_rows();

		if ($all==$hand_over) {
			return 'completed';
		}else{
			return 'on progress';
		}
	}
	
	function posts_search_distributionsummary($line,$factory,$po=null,$size=null)
    {
        // $this->db->group_start();
        // $this->db->group_end();
        if($size!=null){
            $this->db->where('size',$size);            
        }
        if($po!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($po), 'BOTH');
        }
        // $this->db->where('date',$date);
		$this->db->where('factory_id',$factory);
		$this->db->where('line_id',$line);
		$this->db->group_by('line_id, factory_id, line_name, style, poreference, size, article, status');
		$this->db->select('line_id, factory_id, line_name, style, poreference, size, article, status, sum(total)');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_distributionsummary($line,$factory,$po=null,$size=null)
    {

        // $this->db->like('line_name',strtoupper($search), 'BOTH');
        // $this->db->where('factory_id',$factory);
        if($size!=null){
            $this->db->where('size',$size);            
        }
        if($po!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($po), 'BOTH');
		}
		$this->db->where('factory_id',$factory);
		$this->db->where('line_id',$line);
		$this->db->group_by('line_id, factory_id, line_name, style, poreference, size, article, status');
		$this->db->select('line_id, factory_id, line_name, style, poreference, size, article, status, sum(total)');
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

	function allposts_count_receive($line_id,$factory)
    {
        $this->db->where('line_id', $line_id);
        $this->db->where('factory_id', $factory);
        $query = $this->db->get('distribusi');

        return $query->num_rows();
    }

    function allposts_receive($limit,$start,$col,$dir,$line_id,$factory)
    {
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->where('line_id', $line_id);
        $this->db->where('factory_id', $factory);        
        $query = $this->db->get('distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_receive($limit,$start,$search,$col,$dir,$line_id,$factory,$date=NULL,$poreference=NULL,$pilih=NULL)
    {     
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        if ($date!=NULL) {
           $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
        }    
        if ($poreference!=NULL) {
            $this->db->like('poreference', $poreference, 'BOTH');
        }
        if ($pilih!=NULL) {
            $this->db->where('size', $pilih);
        }    
        $this->db->where('line_id', $line_id);
        $this->db->where('factory_id', $factory);        
        $query = $this->db->get('distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_receive($search,$line,$factory,$date=NULL,$poreference=NULL,$pilih=NULL)
    {
        if ($date!=NULL) {
           $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
        }    
        if ($poreference!=NULL) {
            $this->db->like('poreference', $poreference, 'BOTH');
        }if ($pilih!=NULL) {
            $this->db->where('size', $pilih);
        }     
        $this->db->where('line_id', $line);
        $this->db->where('factory_id', $factory);        
        $query = $this->db->get('distribusi');

        return $query->num_rows();
    }

    public function getQtyReceive($poreference,$size,$style,$article,$lineid)
    {
    	$this->db->where('status', 'completed');
    	$this->db->where('poreference', $poreference);
    	$this->db->where('line_id', $lineid);
    	$this->db->where('size', $size);
    	$this->db->where('article', $article);
    	$this->db->where('style', $style);
    	$this->db->select('sum(qty) as qty');
    	return $this->db->get('distribusi')->row_array()['qty'];
    }

    public function getQtyBeda($line_id)
    {
    	$sql ="SELECT
					SUM (di.qty) total_receive,
					ihz.qty,
					di.poreference,
					di.size,
					di.style,
					di.article,
					di.line_id,
					di.factory_id,
					di.inline_header_id
				FROM
					distribusi di
				LEFT JOIN inline_header_size ihz on ihz.inline_header_id=di.inline_header_id and ihz.poreference=di.poreference and ihz.size=di.size
				WHERE
					(di.status='completed' or di.status='packing' or di.status='subcont') and di.line_id=$line_id and delete_at is null
				GROUP BY
				  di.poreference,ihz.qty,
					di.size,
					di.style,
					di.line_id,
					di.article,
					di.factory_id, 
					di.inline_header_id
				HAVING SUM(di.qty)<>COALESCE(ihz.qty,0)";

		return $this->db->query($sql);
    }

    public function cekPanelLain($poreference='',$size='',$style='')
    {
    	$this->db->where('delete_at is null', NULL,FALSE);
    	return $this->db->get('distribusi_detail_view');
    }

    public function getTempPindah($line_id='',$barcode_id='')
    {
    	if ($line_id!='') {
    		$this->db->where('line_id', $line_id);
    	}
    	if ($barcode_id!='') {
    		$this->db->where('barcode_id', $barcode_id);
    	}
    	return $this->db->get('temp_distribusi_pindah');
    }
   
    public function getTemp($poreference,$size,$cutnum,$start,$end,$style,$article)
	{	
		$where = array(
			'style'       => $style, 
			'poreference' => $poreference, 
			'size'        => $size, 
			'cut_num'     => $cutnum, 
			'start_num'   => $start, 
			'article'     => $article, 
			'start_end'   => $end
		);
		
		$this->db->where($where);
		$this->db->select('style,poreference,size,cut_num,start_num,start_end,article');
   		$this->db->distinct();
		return $this->db->get('temp_distribusi_pindah');				
	}

	public function getAvailable($poreference,$size,$style,$qtymove,$article)
	{
		$line_id = $this->session->userdata('line_id');
		$where 	 = array(
			'style'       => $style, 
			'poreference' => $poreference, 
			'line_id'     => $line_id, 
			'article'     => $article, 
			'size'        => $size
		);
		
		$this->db->where($where);
		return $this->db->get('qc_output')->row_array();		
	}

	function getHeaders($poreference,$article)
	{
		$line_id = $this->session->userdata('line_id');
		$this->db->where(array('poreference'=>$poreference,'line_id'=>$line_id,'isactive'=>'t','article'=>$article));
		return $this->db->get('inline_header');
	}

	// public function getLinePlaning($factory='',$poreference)
	// {
	// 	$query = "select string_agg(replace(line,'$factory',''), ', ') as line from ppcm_upload where poreference='$poreference' and deleted_at is NULL";
	// 	return $this->dist->query($query);
	// }

	public function cekPindah($poreference,$size,$cutnum,$start,$end,$style,$article)
	{
		return $this->db
			->where('style',$style)
			->where('poreference',$poreference)
			->where('size',$size)
			->where('article',$article)
			->where('cut_num',$cutnum)
			->where('start_num',$start)
			->where('start_end',$end)
			->get('distribusi_detail_view');
	}

	public function cekBundlePindah($poreference,$size,$cutnum,$start,$end,$style,$article,$lineid)
	{
		$this->db->where('poreference', $poreference);
		$this->db->where('line_id', $lineid);
		$this->db->where('delete_at is null');
		$this->db->where('article', $article);
		$this->db->where('style', $style);
		$this->db->where('size', $size);
		$this->db->where('cut_num', $cutnum);
		$this->db->where('start_num', $start);
		$this->db->where('start_end', $end);
		return $this->db->get('distribusi');
	}
}

/* End of file DistribusiModel.php */
/* Location: ./application/models/DistribusiModel.php */