<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_workflow_model extends CI_Model {
// list master workflow
	function allposts_count_list()
    {   
        $query = $this
                ->db
                ->select('style')
                ->group_by('style')
                ->get('master_workflow');
    
        return $query->num_rows();  

    }
    
    function allposts_list($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->select('style')
                ->group_by('style')
                ->get('master_workflow');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_list($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('style',strtoupper($search))
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->select('style')
                ->group_by('style')
                ->get('master_workflow');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_list($search)
    {
        $query = $this
                ->db
                ->like('style',strtoupper($search))
                ->select('style')
                ->group_by('style')
                ->get('master_workflow');
    
        return $query->num_rows();
    }

    // list master order
    function allposts_count_style()
    {   
       
        $this->db->where('style NOT IN (SELECT distinct style FROM master_workflow)', NULL, FALSE);
        $this->db->select('style');
        $this->db->distinct();
        $query = $this->db->get('master_order');
    
        return $query->num_rows();  

    }
    
    function allposts_style($limit,$start,$col,$dir)
    {  
        $this->db->limit($limit,$start);
        $this->db->where('style NOT IN (SELECT distinct style FROM master_workflow)', NULL, FALSE);
        $this->db->select('style');
        $this->db->distinct();
        $query = $this->db->get('master_order');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_style($limit,$start,$search,$col,$dir)
    {
        $this->db->order_by($col,$dir);
        $this->db->where('style NOT IN (SELECT distinct style FROM master_workflow)', NULL, FALSE);
        $this->db->like('style',strtoupper($search));
        $this->db->limit($limit,$start);
        $this->db->select('style');
        $this->db->distinct();

        $query = $this->db->get('master_order');
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_style($search)
    {
        $this->db->where('style NOT IN (SELECT distinct style FROM master_workflow)', NULL, FALSE);
        $this->db->like('style',strtoupper($search));
        $this->db->select('style');
        $this->db->distinct();
        $query = $this->db->get('master_order');

        return $query->num_rows();
    }
    // end list master order
    public function getworkflow($style)
    {
    	$this->db->join('master_proses', 'master_proses.master_proses_id = master_workflow.master_proses_id', 'left');
        $this->db->where('master_workflow.style', $style);
		$this->db->where('master_workflow.delete_at', NULL);
		$this->db->order_by('master_workflow.master_workflow_id');
        $this->db->select('master_workflow.master_workflow_id,master_workflow.master_proses_id as proses_id,master_workflow.lastproses,master_proses.proses_name, master_workflow.cycle_time, master_workflow.is_critical');
		$query = $this->db->get('master_workflow');
		if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }
	function getworkflow_id($id)
	{
		$this->db->where('master_workflow_id', $id);
		$query = $this->db->get('master_workflow');
		return $query->num_rows();
	}

}

/* End of file master_workflow_model.php */
/* Location: ./application/models/master_workflow_model.php */
