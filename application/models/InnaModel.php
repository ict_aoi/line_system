<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InnaModel extends CI_Model {

    function allposts_daily_count($term)
    {   
        $query = $this->db->query("SELECT date(create_date), line_name, no_urut, style, poreference, size, sum(qty),
		is_integrate from inna_system 
		LEFT JOIN master_line on master_line.master_line_id::VARCHAR = inna_system.line_id
		where is_integrate = '$term' and line_id is not null and date(create_date)>='2023-01-01'
		GROUP BY style,poreference,size,date(create_date), is_integrate, line_name, no_urut");
        return $query->num_rows();
    }
    
    // function allposts_daily($limit,$start,$col,$dir,$tgl,$factory)
    function allposts_daily($limit,$start,$col,$dir,$term)
    {   
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            
            $query = $this->db->query("	SELECT date(create_date), line_name, no_urut, style, poreference, size, sum(qty),
            is_integrate from inna_system 
			LEFT JOIN master_line on master_line.master_line_id::VARCHAR = inna_system.line_id
            where is_integrate = '$term' and line_id is not null and date(create_date)>='2023-01-01'
            GROUP BY style,poreference,size,date(create_date), is_integrate, line_name, no_urut
            order by $col $dir 
            limit $limit offset $start");
        }
        else {

            $query = $this->db->query("SELECT date(create_date), line_name, no_urut, style, poreference, size, sum(qty),
            is_integrate from inna_system 
			LEFT JOIN master_line on master_line.master_line_id::VARCHAR = inna_system.line_id
            where is_integrate = '$term' and line_id is not null
            GROUP BY style,poreference,size,date(create_date), is_integrate, line_name, no_urut
            ORDER BY date,no_urut,style,poreference,size asc");
        }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
	}
	// function posts_daily_search($limit,$start,$col,$dir,$tgl,$search1,$search2,$factory)
	function posts_daily_search($limit,$start,$col,$dir,$search1,$term)
    {
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $text = "";

            $text .= "	SELECT date(create_date), line_name, no_urut, style, poreference, size, sum(qty),
            is_integrate from inna_system 
			LEFT JOIN master_line on master_line.master_line_id::VARCHAR = inna_system.line_id
            where is_integrate = '$term' and line_id is not null
            ";
            
            if($search1!=NULL){
                $text .= "and poreference like '%$search1%'           
	            GROUP BY style,poreference,size,date(create_date), is_integrate, line_name, no_urut
                order by $col $dir limit $limit offset $start";
            }
        }
        else{
            $text = "";

            $text .= "SELECT date(create_date), line_name, no_urut, style, poreference, size, sum(qty),
            is_integrate from inna_system 
			LEFT JOIN master_line on master_line.master_line_id::VARCHAR = inna_system.line_id
            where is_integrate = '$term' and line_id is not null
            ";

            if($search1!=NULL){
				$text .= "and poreference like '%$search1%'
				GROUP BY style,poreference,size,date(create_date), is_integrate, line_name, no_urut
                ORDER BY date,no_urut,style,poreference,size asc";
            }
        }

        $query = $this->db->query($text);
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    // function posts_daily_search_count($search1,$search2,$tgl,$factory)
    function posts_daily_search_count($search1,$term)
    {
        $text = "";

        $text .= "SELECT date(create_date), line_name, no_urut, style, poreference, size, sum(qty),
		is_integrate from inna_system 
		LEFT JOIN master_line on master_line.master_line_id::VARCHAR = inna_system.line_id
		where is_integrate = '$term' and line_id is not null
         ";

        if($search1!=NULL){
            $text .= "and poreference like '%$search1%'
            GROUP BY style,poreference,size,date(create_date), is_integrate, line_name, no_urut
            ORDER BY date,no_urut,style,poreference,size asc";
        }

        $query = $this->db->query($text);
    
        return $query->num_rows();
    }
}
