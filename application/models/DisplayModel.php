<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DisplayModel extends CI_Model {

	public function qcStatus_($line_id,$factory_id)
	{
		$date = date('Y-m-d');
		$this->db->where('to_char(update_at,\'YYYY-mm-dd\')', $date);
		$this->db->where(array('factory_id'=>$factory_id,'line_id'=>$line_id));
		$query = $this->db->get('status_qc_endline');
		return $query->result();
	}

	public function qcStatus($line_id,$factory_id)
	{
		$date = date('Y-m-d');
		$this->db->where('to_char(update_at,\'YYYY-mm-dd\')', $date);
		$this->db->where(array('factory_id'=>$factory_id,'line_id'=>$line_id));
		$query = $this->db->get('status_qc_endline');
		return $query->row_array();
	}
	public function dailyStatus($line_id,$factory_id,$style)
	{
		$date = date('Y-m-d');
		$query ="SELECT
					A . STYLE,
					A .line_id,
					A .factory_id,
					DATE (b.create_date)
				FROM
					daily_header A
				LEFT JOIN daily_detail b ON b.daily_header_id = A .daily_header_id
				where DATE (b.create_date)='$date' and line_id=$line_id and factory_id=$factory_id and style='$style'";

		return $this->db->query($query)->num_rows();
	}
	public function sum_qcendline($awal,$akhir,$style,$line,$factory)
	{
		/*$query = "SELECT DISTINCT
						a.style,
						a.line_id,
						a.factory_id,
						b.line_name,
						tot_endline.total_good_endline,
					  tot_endline.date
					FROM
						inline_header a
					LEFT JOIN  master_line b on b.master_line_id=a.line_id
					JOIN(SELECT
						DATE (qet.create_date) AS DATE,
						COUNT (qet.counter_pcs) AS total_good_endline,
						qet.style,qet.factory_id,
						ih.line_id
					FROM
						qc_endline_trans qet
					JOIN inline_header ih on ih.style=qet.style and ih.inline_header_id=qet.inline_header_id
					WHERE
						qet.create_date >= '$awal'
					and qet.create_date < '$akhir'
					and qet.style='$style' and ih.line_id=$line and qet.factory_id=$factory
					and qet.counter_pcs<>0
					GROUP BY DATE (qet.create_date),qet.STYLE,qet.factory_id,ih.line_id) tot_endline on tot_endline.style=a.style and tot_endline.line_id=a.line_id";
		return $this->db->query($query)->row_array()['total_good_endline'];*/
		$where = array(
			'line_id'       =>$line,
			'style'         =>$style,
			'activity_name' =>'outputqc',
			'factory_id'    =>$factory 
		);
		$this->db->where($where);
		$this->db->where('jam_awal >=',$awal);
		$this->db->where('jam_akhir <=',$akhir);
		$this->db->select('sum(output) as output');
		return $this->db->get('line_summary_po')->row_array()['output'];
	}
	public function sumQcEndlinebyLine($awal,$akhir,$line,$factory)
	{
		$sql ="SELECT DISTINCT
					a.line_id,
					a.factory_id,
					b.line_name,
					tot_endline.total_good_endline,
					tot_endline.date
				FROM
					inline_header a
				LEFT JOIN  master_line b on b.master_line_id=a.line_id
				JOIN(SELECT
					DATE (qet.create_date) AS DATE,
					COUNT (qet.counter_pcs) AS total_good_endline,qet.factory_id,
					ih.line_id
				FROM
					qc_endline_trans qet
				JOIN inline_header ih on ih.inline_header_id=qet.inline_header_id
				WHERE
					qet.create_date >= '$awal'
				and qet.create_date < '$akhir'
				and ih.line_id=$line and qet.factory_id=$factory
				and qet.counter_pcs<>0
				GROUP BY DATE (qet.create_date),qet.factory_id,ih.line_id) tot_endline on tot_endline.line_id=a.line_id";
		return $this->db->query($sql)->row_array();
	}
	public function sum_folding($awal,$akhir,$style,$line,$factory)
	{
		/*$query = "SELECT
					A . STYLE,
					A .counter_folding,
					b.line_id,
					A .factory_id
				FROM
					folding A
				JOIN inline_header b ON b.inline_header_id = A .inline_header_id
				WHERE
					A.create_date >= '$awal'
				and A.create_date < '$akhir'
				and A.style='$style' and b.line_id=$line and a.factory_id=$factory";
		return $this->db->query($query)->num_rows();*/

		$where = array(
			'line_id'       =>$line,
			'style'         =>$style,
			'activity_name' =>'folding',
			'factory_id'    =>$factory 
		);
		$this->db->where($where);
		$this->db->where('jam_awal >=',$awal);
		$this->db->where('jam_akhir <=',$akhir);
		$this->db->select('sum(output) as output');
		return $this->db->get('line_summary_po')->row_array()['output'];
	}
	public function sum_defect_inline($awal,$akhir,$style,$line,$factory)
	{
		/*$query = " SELECT count(a.defect_id) AS defect_inline,
				    date(a.create_date) AS date,
				    b.style,
						a.factory_id,
				    c.line_name,
						c.master_line_id
				   FROM ((inline_detail a
				     LEFT JOIN inline_header b ON (((b.inline_header_id)::text = (a.inline_header_id)::text)))
				     LEFT JOIN master_line c ON ((c.master_line_id = b.line_id)))
				  WHERE c.master_line_id=$line and a.factory_id=$factory and b.style='$style'
				  and a.create_date >= '$awal'
				  and a.create_date < '$akhir'
				  and a.defect_id<>0
				  GROUP BY (date(a.create_date)), b.style, c.line_name,c.master_line_id,a.factory_id
				  ORDER BY (date(a.create_date))";
	 	return $this->db->query($query)->row_array()['defect_inline'];*/
	 	$where = array(
			'line_id'       =>$line,
			'style'         =>$style,
			'activity_name' =>'defect_inline',
			'factory_id'    =>$factory 
		);
		$this->db->where($where);
		$this->db->where('jam_awal >=',$awal);
		$this->db->where('jam_akhir <=',$akhir);
		$this->db->select('sum(output) as output');
		return $this->db->get('line_summary_po')->row_array()['output'];	 
	} 
	public function sum_defect_endline($awal,$akhir,$style,$line,$factory)
	{
		/*$query = "WITH endline_int AS(
					SELECT DISTINCT
						qed.qc_endline_trans_id,
						count(qed.defect_id) AS end_defect,
						qed.style,
						qed.factory_id,
						ih.line_id,
						DATE(qed.create_date)
					FROM
						qc_endline_defect qed 
					JOIN inline_header ih on ih.style=qed.style and ih.inline_header_id=qed.inline_header_id
					WHERE
						ih.line_id=$line and qed.factory_id=$factory and qed.style='$style' and qed.create_date >= '$awal'
				  	and qed.create_date < '$akhir'
					GROUP BY  qed.qc_endline_trans_id,DATE(qed.create_date),qed.style,qed.factory_id,ih.line_id
					)
					 SELECT count(endline_int.end_defect) AS defect_endline,
					    endline_int.date,
					    endline_int.style,
							endline_int.line_id,
							endline_int.factory_id
					   FROM endline_int
					  GROUP BY endline_int.date, endline_int.style,endline_int.factory_id,endline_int.line_id
					  ORDER BY endline_int.date";
		return $this->db->query($query)->row_array()['defect_endline'];	*/	
		$where = array(
			'line_id'       =>$line,
			'style'         =>$style,
			'activity_name' =>'defect_endline',
			'factory_id'    =>$factory 
		);
		$this->db->where($where);
		$this->db->where('jam_awal >=',$awal);
		$this->db->where('jam_akhir <=',$akhir);
		$this->db->select('sum(output) as output');
		return $this->db->get('line_summary_po')->row_array()['output'];		  
	}
	public function getStyleDetail($style,$line_id,$factory_id)
    {
       $date = date('Y-m-d');
       $sql = "SELECT
					dh. STYLE,
					dh.line_id,
					dh.gsd_smv,
					dh.order_qty,
					dh.change_over,
					dh.cumulative_day,
					dd.present_sewer,
					dd.working_hours,
					dh.target_wft,
					dh.job_status
				FROM
					daily_header dh
				LEFT JOIN daily_detail dd ON dd.daily_header_id = dh.daily_header_id
				where dh.line_id='$line_id' and dh.style='$style' and dh.factory_id=$factory_id and job_status='t' and DATE(dd.create_date)='$date'  order by dd.create_date desc";

        return $this->db->query($sql);
    }
    public function LastUpdate($line,$style)
    {
    	$date = date('Y-m-d');
    	$this->db->where(array('line_id'=>$line,'style'=>$style));
    	$this->db->where('date', $date);
    	$this->db->select_max('create_date');
    	return $this->db->get('line_summary_po')->row_array()['create_date'];
	}	
	
	public function JalanStyle($line,$factory)
	 {
		$date = date('Y-m-d');
	
		// $date = '2020-03-27';
		$this->db->order_by('create_date','ASC');
    	$this->db->where(array('line_id'=>$line,'factory_id'=>$factory));
		$this->db->where('date(create_date)', $date);
		$this->db->where('job_status', 't');
    	return $this->db->get('daily_view');
		

		// $query =  $this->db->query("SELECT * FROM daily_view where date(create_date) = '$date' and line_id = '$line' and factory_id = '$factory'
		// and job_status = 't' ORDER BY  create_date asc");
		// return $query;
	

	}

	public function eff_daily($tgl,$factory,$line)
    {

        $query = $this->db->query("SELECT DISTINCT * 
        FROM daily_view WHERE factory_id = '$factory' and date(create_date) = '$tgl' 
		and line_id = '$line' and job_status = 't'
        ORDER BY no_urut ASC");

        // if($query->num_rows()>0)
        // {
            // return $query->result(); 
            return $query; 
        // }
        // else
        // {
        //     return null;
        // }
        
	}

	function cum_count($header, $tgl)
    {
        $query = $this->db->query("SELECT * from daily_detail where daily_header_id = '$header' and date(create_date)<='$tgl'");
        
        $total = $query->num_rows();

        return $total-1;
    }

    function output_count($date,$line,$activity)
    {
        $query = $this->db->query("SELECT sum(output) as total from line_summary_po
         where date = '$date' and activity_name = '$activity' 
         and line_id = '$line'");
        
        return $query->row();
    }

    function eff_count($line_id, $change, $cum)
    {
        $query = $this->db->query("SELECT * from master_line where master_line_id='$line_id'")->row();

        $RL = $query->reqular_line;

        if($RL = 't'){
            $reg = 'RL';
        }
        else{
            $reg = 'SC';
        }

        $query2 = $this->db->query("SELECT * from master_co where line_category='$reg' and category ='$change' and day='$cum'");

        return $query2->row();
    }
}

/* End of file DisplayModel.php */
/* Location: ./application/models/DisplayModel.php */