<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

    function getData($id)
    {
        $query = $this->db->where('id',$id)->get('master_product');

        return $query->row_array();
    }
    
	function allposts_count()
    {   
        $query = $this
				->db
				->where('deleted_at',null)
                ->get('master_product');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
				->where('deleted_at',null)
                ->get('master_product');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('nama_product',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
				->where('deleted_at',null)
                ->get('master_product');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('nama_product',$search)
				->where('deleted_at',null)
                ->get('master_product');
    
        return $query->num_rows();
    }
	

}

/* End of file master_proses_model.php */
/* Location: ./application/models/master_proses_model.php */
