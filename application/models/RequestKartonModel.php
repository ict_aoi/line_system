<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestKartonModel extends CI_Model {

	function get_po($line_id, $factory_id, $balance=0)
	{
		$sql = 'SELECT DISTINCT inline_header_id, poreference, style, article, startdateactual from active_po_folding_new
				where line_id = ? and balance <= ? AND factory_id = ?';

		$sql .= ' ORDER BY startdateactual DESC';

		$params = [$line_id, $balance, $factory_id];

		return $this->db->query($sql, $params);
	}

	function get_article($ih)
	{
		$query = $this->db->query("SELECT DISTINCT poreference, article 
		from active_po_folding_new where inline_header_id = '$ih'");

		return $query->result();
	}

	function get_size($inline_header_id, $factory_id)
	{
		$sql = 'SELECT inline_header_id, poreference, style, startdateactual, size from active_po_folding_new
				where inline_header_id = ? AND factory_id = ?';

		$params = [$inline_header_id, $factory_id];

		return $this->db->query($sql, $params);
	}

	// get list request carton packing
	function get_list_request($line_fg, $factory_id, $status_sewing='request')
	{
		$sql = 'SELECT * FROM v_request_carton where status_sewing = ? AND line_id = ? AND factory_id = ?';

		$sql .= ' ORDER BY request_at ASC';

		$params = [$status_sewing, $line_fg, $factory_id];

		return $this->fg->query($sql, $params);
	}

	function allposts_count_receive($line_id,$factory)
    {
        $this->fg->where('line_id', $line_id);
        $this->fg->where('factory_id', $factory);
        $query = $this->fg->get('v_report_request_receive');

        return $query->num_rows();
    }

    function allposts_receive($limit,$start,$col,$dir,$line_id,$factory)
    {

        $this->fg->limit($limit,$start);
        $this->fg->order_by($col,$dir);
        $this->fg->where('line_id', $line_id);
        $this->fg->where('factory_id', $factory);
        $query = $this->fg->get('v_report_request_receive');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_receive($limit,$start,$search,$col,$dir,$line_id,$factory,$date=NULL)
    {

        $this->fg->limit($limit,$start);
        $this->fg->order_by($col,$dir);
        if ($date!=NULL) {
           $this->fg->where('to_char(request_at,\'YYYY-mm-dd\')',$date);
        }

        $this->fg->where('line_id', $line_id);
        $this->fg->where('factory_id', $factory);
        $query = $this->fg->get('v_report_request_receive');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_receive($search,$line,$factory,$date=NULL)
    {

        if ($date!=NULL) {
           $this->fg->where('to_char(request_at,\'YYYY-mm-dd\')',$date);
        }

        $this->fg->where('line_id', $line);
        $this->fg->where('factory_id', $factory);
        $query = $this->fg->get('v_report_request_receive');

        return $query->num_rows();
    }

    // get packing list package detail
    function gets_packing_list($poreference)
    {
    	$this->fg->where('po_number', $poreference);

    	return $this->fg->get('package_detail');
    }

    function gets_po_summary($poreference)
    {
    	$this->fg->select(
				'po_number,
				plan_ref_number,
				created_at,
				updated_at,
				deleted_at,
				dateordered,
				datepromised,
				grandtotal,
				bp_name,
				bp_code,
				phone,
				fax,
				address,
				city,
				postal,
				country,
				kst_lcdate,
				date(kst_statisticaldate) AS statisticaldate,
				trademark,
				made_in,
				country_of_origin,
				commodities_type,
				persen_inventory,
				persen_qcinspect,
				sample_size_qc,
				completed_qty_qc,
				customer_order_number,
				remark,
				psd,
				factory_id,
				persen_shipping,
				is_integrate,
				integration_date,
				m_locator_id,
				is_complete,
				invoice,
				upc'
    	);
    	$this->fg->where('po_number', $poreference);

    	return $this->fg->get('po_summary');
    }

    // package calculate
    function gets_po_calculate($poreference)
    {
    	$sql = 'SELECT po_number, sum(total_ctn) total_ctn, sum(total_inner_pack) total_inner_pack, sum(total_item_qty) total_item_qty, sum(ctn_count) ctn_count, round(sum(total_net_net), 3) total_net_net, round(sum(total_net), 3) total_net, round(sum(total_gross), 3) total_gross, round(sum(cbm), 3) cbm FROM v_package_detail_calculate WHERE po_number=? GROUP BY po_number';

    	$params[] = $poreference;

    	return $this->fg->query($sql, $params);
    }

}

/* End of file RequestKartonModel.php */
/* Location: ./application/models/RequestKartonModel.php */
