<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cekpermision_model extends CI_Model {

function cekpermision($seq)
{


	$role      = $this->uri->segment($seq);
	
	
	$level     = $this->session->userdata('role_id');

	$this->db->like('link', strtolower($role), 'BOTH');
	$permision = $this->db->get('menu');
	if ($permision->num_rows()>0) {
		$permisions = $permision->result()[0]->menu_id;
	}else{
		$permisions =NULL;
	}

	// $this->db->get_where('menu',array('link'=>strtolower($role)))->result()[0]->menu_id;

	$role_user = $this->db->get_where('role_user', array('menu_id'=>$permisions,'level_id'=>$level));
	return $role_user->num_rows();

}

}

/* End of file cekpermision_model.php */
/* Location: ./application/models/cekpermision_model.php */