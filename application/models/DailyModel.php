<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DailyModel extends CI_Model {

	public function getStyle($line_id)
	{
		// $factory = $this->session->userdata('factory');
		// $this->db->group_by('style');
		// $this->db->where(array('line_id'=>$line_id,'factory_id'=>$factory,'isactive'=>'t'));
		// $this->db->select('sum(targetqty) as totaltarget');
		// $this->db->select('style');
		// $this->db->distinct();
		// $query = $this->db->get('inline_header');
		// return $query;
  //  /* $this->db->where(array('line_id'=>$line_id,'factory_id'=>$factory));
  //   return $this->db->get('active_style_daily');*/
    $this->db->where('line_id', $line_id);
    return $this->db->get('active_style_daily');

	}

	//
    public function getStyleByDate($style,$line_id,$factory_id)
    {
      $date = date('Y-m-d');
       $this->db->where('date(create_date)', $date);
       $this->db->where('style', $style);
       $this->db->where('line_id', $line_id);
       $this->db->where('factory_id', $factory_id);
       return $this->db->get('daily_view');
    }

    public function getStyleDetail($style_id,$line_id,$factory_id)
    {
       $date = date('Y-m-d');
       $sql = "SELECT * FROM daily_header dh LEFT JOIN daily_detail dd ON dd.daily_header_id = dh.daily_header_id
				WHERE dh.style = ? AND dh.line_id = ? AND dh.factory_id = ? AND Date(create_date)=? and dh.job_status='t' order by create_date desc";

        return $this->db->query($sql, [$style_id, $line_id, $factory_id,$date]);
    }

    public function getStyleHeader($style_id,$line_id,$factory_id)
    {
       $sql = "SELECT * FROM daily_header WHERE style = ? AND line_id = ? AND factory_id = ? and job_status='t'";

        return $this->db->query($sql, [$style_id, $line_id, $factory_id]);
    }

    public function getDailyHeaderId($style_id,$line_id,$factory_id)
    {
       $sql = "SELECT daily_header_id FROM daily_header
				WHERE style = ? AND line_id = ? AND factory_id = ? and job_status='t'";

        return $this->db->query($sql, [$style_id, $line_id, $factory_id])->row_array(1)['daily_header_id'];
    }
    public function numStyleHeader($style,$change_over,$line_id,$factory_id)
    {
      $where = array(
        'style'    => $style, 
        'job_status'    => 't', 
        'change_over' => $change_over, 
        'line_id'     => $line_id, 
        'factory_id'  => $factory_id 
      );
      $this->db->where($where);
      return $this->db->get('daily_header');
    }
    public function headerBefore($line_id,$days_ago,$style)
    {
      // $this->db->where('Field / comparison', $Value);
      return $this->db->get('daily_view');
    }
    public function getHeaderView($style,$line_id,$factory,$change_over)
    {
      $date = date('Y-m-d');

      $this->db->order_by('create_date', 'desc');
      $this->db->where('change_over', $change_over);
      $this->db->where('date(create_date)', $date);
      $this->db->where('style', $style);
      $this->db->where('line_id', $line_id);
      $this->db->where('factory_id', $factory);
      $this->db->where('job_status', 't');
      return $this->db->get('daily_view');
    }

    public function getDailyView($line_id)
    {
      $date = date('Y-m-d');

      $this->db->limit(1);
      $this->db->order_by('create_date', 'desc');
      $this->db->where('line_id', $line_id);
      $this->db->where('job_status', 't');
      // $this->db->where('date(create_date)', $date);
      return $this->db->get('daily_view');
    }
    public function detailStyleDate($line_id,$style)
    {
      $date = date('Y-m-d');
      $where = array(
        'line_id'    => $line_id, 
        'job_status' => 't', 
        'style'      => $style 
      );
      $this->db->where('date(create_date)', $date);
      $this->db->where($where);
      return $this->db->get('daily_view');
    }
    public function detailStyle($line_id,$style)
    {

      $where = array(
        'line_id'    => $line_id, 
        'style'      => $style 
      );
      $this->db->limit(1);
      $this->db->order_by('create_date', 'desc');
      $this->db->where($where);
      return $this->db->get('daily_view');
    }
    public function getCumDay($daily_header_id)
    {
      $this->db->where('daily_header_id', $daily_header_id);
      return $this->db->get('daily_detail')->num_rows()-1;
		}
		
		public function getDailyToday_count($line)
		{
			$tgl     = date('Y-m-d');
			$this->db->where('date(create_date)',$tgl);
			$this->db->where('line_id',$line);
			$query = $this->db->get('daily_view');

			return $query->num_rows();
		}

		public function getDailyToday_all($line)
		{
			$tgl     = date('Y-m-d');
			$this->db->where('date(create_date)',$tgl);
			$this->db->where('line_id',$line);
			$query = $this->db->get('daily_view');

			if($query->num_rows()>0)
			{
					return $query->result();
			}
			else
			{
					return null;
			}
		}


}

/* End of file DailyModel.php */
/* Location: ./application/models/DailyModel.php */
