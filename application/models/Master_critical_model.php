<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_critical_model extends CI_Model {

    function getdata_proses()
    {
        $sql = "SELECT * FROM master_proses";
        $query = $this->db->query($sql)->result();

        return $query;
    }

    function getdata_product()
    {
        $sql = "SELECT * FROM master_product";
        $query = $this->db->query($sql)->result();
        return $query;
    }

    function allposts_count()
    {  
        $sql = "SELECT DISTINCT product_id,product_name FROM master_critical ORDER BY product_name";
        $query = $this->db->query($sql);
        
        //   $query = $this
        //         ->db
        //         ->distinct('product_name')
        //         ->select('master_critical_id, product_name')
        //         ->order_by('product_name')
        //         ->get('master_critical');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
        $sql = "SELECT DISTINCT product_id,product_name FROM master_critical 
         group by product_name,product_id  ORDER BY $col $dir limit $limit offset $start ";
        $query = $this->db->query($sql);
        
    //    $query = $this
    //             ->db
    //             ->distinct('product_name')
    //             ->select('master_critical_id, product_name')
    //             ->limit($limit,$start)
    //             ->order_by($col,$dir, 'product_name')
    //             ->get('master_critical');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $sql = "SELECT DISTINCT product_id,product_name FROM master_critical ORDER BY product_name, $col, $dir 
        limit $limit offset $start where product_name LIKE '%$search%'";
        $query = $this->db->query($sql);
        
        // $query = $this
        //         ->db
        //         ->distinct('product_name')
        //         ->select('master_critical_id, product_name')
        //         ->like('product_name',$search)
        //         ->limit($limit,$start)
        //         ->order_by($col,$dir, 'product_name')
        //         ->get('master_critical');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $sql = "SELECT DISTINCT product_id, product_name FROM master_critical ORDER BY product_name
         where product_name LIKE '%$search%'";
        $query = $this->db->query($sql);
        
        // $query = $this
        //         ->db
        //         ->distinct('product_name,product_id')
        //         ->select('master_critical_id, product_name')
        //         ->like('product_name',$search)
        //         ->order_by('product_name')
        //         ->get('master_critical');
    
        return $query->num_rows();
    }
    
    
    //detail
    function allposts_count_detail($product_id)
    {
        $query = $this->db->query("SELECT * FROM master_critical WHERE product_id = '$product_id'");
        return $query->num_rows();

    }  
 
      
    
    
    function allposts_detail($limit,$start,$col,$dir, $product_id)
    {
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            
            $query = $this->db->query("	SELECT * from master_critical WHERE product_id = '$product_id'
            order by $col $dir 
            limit $limit offset $start");
        }
        else {

            $query = $this->db->query("SELECT * from master_critical WHERE product_id = '$product_id'");
        }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
        }
   
    function posts_search_detail($limit,$start,$search,$col,$dir, $product_id)
    {
  
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $text = "";

            $text .= "	SELECT * from master_critical WHERE product_id = '$product_id'
            ";
            
            if($search!=NULL){
                $text .= "and poreference like '%$search%'           
	            GROUP BY product_name
                order by $col $dir limit $limit offset $start";
            }
        }
        else{
            $text = "";

            $text .= "SELECT * from master_critical WHERE product_id = '$product_id'
            ";

            if($search1!=NULL){
				$text .= "and poreference like '%$search%'
				GROUP BY product_name
                ORDER BY product_name";
            }
        }

        $query = $this->db->query($text);
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
       
    
    }

    function posts_search_count_detail($search, $product_id)
    {
        $text = "";

        $text .= "SELECT * from master_critical WHERE product_id = '$product_id'
         ";

        if($search!=NULL){
            $text .= "and poreference like '%$search%'
            GROUP BY product_name
            ORDER BY product_name";
        }

        $query = $this->db->query($text);
    
        return $query->num_rows();
    }
    
    
    function getData($id)
    {
        $query = $this->db->where('master_critical_id',$id)->get('master_critical');

        return $query->row_array();
    }
   
    

}
/* End of file Master_critical_model.php */
