<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdjustmentModel extends CI_Model {

	public function getDetailBukuHijau($line='',$factory)
	{
		$date = date('Y-m-d');
		// $date = '2019-03-27';
		$sql = "select * from ouputqc_byline($line, $factory, '$date')";
		return $this->db->query($sql);
	}	
	public function getDetailBukuHijauParam($qc_endline_id)
	{
		$date = date('Y-m-d');
		// $date = '2019-03-27';
		$this->db->where('date', $date);
		$this->db->where('qc_endline_id', $qc_endline_id);
		return $this->db->get('edm_buku_hijau');
	}
	public function getMax($qc_id='')
	{
		$this->db->where('qc_endline_id',$qc_id);
		$this->db->select_max('counter_pcs');
		$max= $this->db->get('qc_endline_trans')->row_array()['counter_pcs'];
		if ($max!=NULL) {
			return $max;
		}else{
			return 0;
		}
	}
	public function detailDelete($qc_id='',$revisi)
	{
		$this->db->where('qc_endline_id', $qc_id);
		$this->db->where('counter_pcs>', $revisi);
		return $this->db->get('qc_endline_trans');
	}
	public function deleteQCtrans($trans_id='')
	{
		$this->db->where_in('qc_endline_trans_id', $trans_id);
		$this->db->delete('qc_endline_trans');

		$this->db->where_in('qc_endline_trans_id', $trans_id);
		$this->db->delete('qc_endline_defect');

	}	
	// serverside detail buku hijau

	function allposts_count_detail($date,$line,$factory)
    {   
        $sql = "select * from ouputqc_byline($line, $factory, '$date')";
    	$query = $this->db->query($sql);
        return $query->num_rows();  

    }
    
    function allposts_detail($limit,$start,$col,$dir,$date,$line,$factory)
    {   
        $sql = "select * from ouputqc_byline($line, $factory, '$date') order by $col $dir limit $limit offset $start";
        
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_detail($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('username',$search)
                ->or_like('nik',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('active',1)
                ->get('users');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_detail($search)
    {
        $query = $this
                ->db
                ->like('username',$search)
                ->or_like('nik',$search)
                ->where('active',1)
                ->get('users');
    
        return $query->num_rows();
    }
	// end serverside detail buku hijau

    public function count_sering($factory)
    {
        $date = date('Y-m');
        $query = $this->db->query("
                SELECT
                    line_id,
                    line_name,
                    COUNT (*),to_char(tanggal,'YYYY-mm')
                FROM
                    adjustment_qc_view
                WHERE
                    factory_id = '$factory' and to_char(tanggal,'YYYY-mm')='$date'
                GROUP BY
                    line_id,
                    line_name,to_char(tanggal,'YYYY-mm')
                ORDER BY
                    COUNT (*) DESC
                LIMIT 10
            ")->result();

        return $query;
    }

    function allposts_count_dash()
    {
        $factory = $this->session->userdata('factory');
        $this->db->where('factory_id',$factory);        
        $query = $this->db->get('adjustment_qc_view');

        return $query->num_rows();  
    }

    function allposts_dash($limit,$start,$col,$dir)
    {
        $factory = $this->session->userdata('factory');

        
        $this->db->limit($limit,$start);
        $this->db->order_by('tanggal','desc');
        $this->db->where('factory_id',$factory);        
        $query = $this->db->get('adjustment_qc_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_dash($limit,$start,$search,$col,$dir)
    {
        $factory = $this->session->userdata('factory');
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->group_start();
        $this->db->like('poreference',strtoupper($search), 'BOTH');
        $this->db->or_like('line_name', strtoupper($search), 'BOTH');
        $this->db->group_end();
        $this->db->where('factory_id',$factory);
        // $this->db->where('line_id',$id);
        $query = $this->db->get('adjustment_qc_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_dash($search)
    {
        $factory = $this->session->userdata('factory');
        $this->db->group_start();
        $this->db->like('poreference',strtoupper($search), 'BOTH');
        $this->db->or_like('line_name', strtoupper($search), 'BOTH');
        $this->db->group_end();
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('adjustment_qc_view');

        return $query->num_rows();
    }

	public function getLineFg($id)
	{
		$this->fg->where('factory_id',$this->session->userdata('factory'));
		$this->fg->where('deleted_at is NULL');
		$this->fg->where('name', $id);
		$query = $this->fg->get('line');
		if ($query->num_rows()>0) {
			return $query->row_array();
		}else{
			return 0;
		}
    }
    
    function getBarcodeFg($po)
    {
        $query = $this->fg->query("SELECT
            p.barcode_id,po_number,manufacturing_size
        FROM
            package_detail pd
        LEFT JOIN package p on pd.scan_id=p.scan_id
        WHERE
            po_number = '$po'
            ");
        
			return $query->result();	
    }

    function getFolding($po,$line_id)
    {
        $query = $this->db->query("SELECT
            sq.inline_header_id,
            sq.line_id,
            sq.poreference,
            sq. SIZE,
            sq. STYLE,
            sq.total_output,
            sf.total_folding,
            COALESCE(sq.total_output-sf.total_folding,sq.total_output) as balance
        FROM
            statuspo sq
        LEFT JOIN statuspo_folding sf on sf.inline_header_id=sq.inline_header_id and sq.size=sf.size and sq.style=sf.style
        WHERE
            sq.poreference = '$po' and
            sq.line_id=$line_id");

        return $query->result();
    }

    function counter_folding($po, $style, $size, $line)
    {
        $query = $this->db->query("SELECT * from folding_header WHERE 
        poreference = '$po' and size = '$size' and style = '$style' and line_id = '$line'");

        return $query;
    }

}

/* End of file AdjustmentModel.php */
/* Location: ./application/models/AdjustmentModel.php */