<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inline_header_model extends CI_Model {

	function allposts_count($factory)
    {   
        $query = $this
                ->db
                ->where('factory_id',$factory)
                ->where('isactive','t')
                ->get('inline_header');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir,$factory)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,'desc')
                ->where('factory_id',$factory)
                ->where('isactive','t')
                ->get('inline_header');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir,$factory)
    {
        $query = $this
                ->db
                ->like('poreference',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->where('isactive','t')
                ->get('inline_header');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search,$factory)
    {
        $query = $this
                ->db
                ->like('poreference',$search)
                ->where('factory_id',$factory)
                ->where('isactive','t')
                ->get('inline_header');
    
        return $query->num_rows();
    }

    //list po buyer & style

    function allposts_count_list()
    {   
        $query = $this
                ->db
                ->where('balance_input<>0')
                ->get('qtytargetvsqtyorder_views');
    
        return $query->num_rows();  

    }
    
    function allposts_list($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('balance_input<>0')
                ->get('qtytargetvsqtyorder_views');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_list($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->group_start()
                ->like('poreference',strtoupper($search))
                ->or_like('style',strtoupper($search))
                ->group_end()
                ->limit($limit,$start)
                ->where('balance_input<>0')
                ->get('qtytargetvsqtyorder_views');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_list($search)
    {
        $query = $this
                ->db
                ->group_start()
                ->like('poreference',strtoupper($search))
                ->or_like('style',strtoupper($search))
                ->group_end()
                ->where('balance_input<>0')
                ->get('qtytargetvsqtyorder_views');
    
        return $query->num_rows();
    }
    // end list po buyer & style

    // List Line
    function allposts_count_line($factory)
    {   
        $query = $this
                ->db
                ->where('active','t')
                ->where('factory_id',$factory)
                ->get('master_line');
    
        return $query->num_rows();  

    }
    
    function allposts_line($limit,$start,$col,$dir,$factory)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('active','t')
                ->where('factory_id',$factory)
                ->get('master_line');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_line($limit,$start,$search,$col,$dir,$factory)
    {
        $query = $this
                ->db
                ->where('active','t')
                ->where('factory_id',$factory)
                ->like('line_name',strtoupper($search))
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_line');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_line($search,$factory)
    {
        $query = $this
                ->db
                ->where('active','t')
                ->where('factory_id',$factory)
                ->like('line_name',strtoupper($search))
                ->get('master_line');
    
        return $query->num_rows();
    }
    function get_detail_mo($where)
    {
        $this->db->where($where);
        $this->db->where_not_in('balance',0);
        return $this->db->get('qtyinput_size_view');
    }
    public function get_sum_qtyorder($where)
    {
        $this->db->where($where);
        $this->db->where('kst_statisticaldate IS NOT NULL', null, false);
        $this->db->select_sum('qty_ordered');
        return $this->db->get('master_order')->row_array();
    }
    public function get_sum_inline_size($header_id)
    {
        $this->db->where('inline_header_id', $header_id);
        $this->db->select_sum('qty');
        $query = $this->db->get('inline_header_size');
        if($query->num_rows()>0)
        {
            return $query->row_array();  
        }
        else
        {
            return null;
        }
    }
    public function cekQcEndline($header_id,$size_id)
    {
        $factory = $this->session->userdata('factory');
        
        $q = "SELECT
                sum(counter_qc)+sum(repairing) as counter_qc
            FROM
                qc_endline
            WHERE
                inline_header_id='$header_id' 
            and header_size_id='$size_id'";
        $query = $this->db->query($q)->row_array();
       
        if ($query['counter_qc']!=NULL) {
            return $query['counter_qc'];
        }else{
            return 0;
        }
    }

}

/* End of file inline_header_model.php */
/* Location: ./application/models/inline_header_model.php */