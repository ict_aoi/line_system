<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_inspect_model extends CI_Model {

	function allposts_count_line()
    {
        $query = $this
                ->db
                ->get('master_line');

        return $query->num_rows();

    }

    function allposts_line($limit,$start,$col,$dir)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_line');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_line($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('line_name',$search)
                ->or_like('line_name',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_line');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_line($search)
    {
        $query = $this
                ->db
                ->like('line_name',$search)
                ->or_like('line_name',$search)
                ->get('master_line');

        return $query->num_rows();
    }

    // po buyer

    function allposts_count_list($factory,$line_id)
    {
        $query = $this
                ->db
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->get('active_po_endline');

        return $query->num_rows();

    }

    function allposts_list($limit,$start,$col,$dir,$factory,$line_id)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->get('active_po_endline');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_list($limit,$start,$search,$col,$dir,$factory,$line_id)
    {
        $query = $this
                ->db
                ->like('poreference',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->get('active_po_endline');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_list($search,$factory,$line_id)
    {
        $query = $this
                ->db
                ->like('poreference',$search)
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->get('active_po_endline');

        return $query->num_rows();
    }
    // defect

    function allposts_count_defect()
    {
        $query = $this
                ->db
                ->where_not_in('defect_id',0)
                ->where('is_active',true)
                ->get('master_defect');

        return $query->num_rows();

    }

    function allposts_defect($limit,$start,$col,$dir)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('is_active',true)
                ->where_not_in('defect_id',0)
                ->get('master_defect');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_defect($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->group_start()
                ->like('cast(defect_id as text)',$search)
                ->or_like('defect_jenis',$search)
                ->group_end()
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('is_active',true)
                ->where_not_in('defect_id',0)
                ->get('master_defect');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_defect($search)
    {
        $query = $this
                ->db
                ->group_start()
                ->like('defect_jenis',$search)
                ->or_like('defect_jenis',$search)
                ->group_end()
                ->where('is_active',true)
                ->where_not_in('defect_id',0)
                ->get('master_defect');

        return $query->num_rows();
    }
    // end list defect
    function cekheader($line,$poreference,$style)
    {
                  $this->db->where('style', $style);
                  $this->db->where('line_id', $line);
                  $this->db->where('poreference', $poreference);
        $query  = $this->db->get('inline_header');
        if($query->num_rows()>0)
        {
            return $query;
        }
        else
        {
            return null;
        }
    }
    function maxheader($line_id)
    {
        $factory   = $this->session->userdata('factory');
                  $this->db->limit(1);
                  $this->db->order_by('create_date', 'desc');
                  $this->db->where('factory_id', $factory);
                  $this->db->where('line_id', $line_id);
        $query  = $this->db->get('round_status');
        // var_dump ($query->result());
        // die();

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    function headerdetail($detail_id,$date)
    {
        $output = '';
        $jum =1;
        $i =0;
        $sql = "SELECT
                A.round,A.proses_name,A.sewer_nik,A.color,A.create_date,A.defect_id, (
                    SELECT
                        COUNT (round)
                    FROM
                        inlinedetail_view
                    WHERE
                        round = A .round
                    and proses_name=A.proses_name
                    AND sewer_nik=A.sewer_nik
                    and create_date=A.create_date
                ) AS jumlah
            FROM
                inlinedetail_view A
            where inline_header_id='$detail_id'
            and to_char(create_date,'yyyy-mm-dd'::text)='$date'
            ORDER BY
                A .round";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    public function getDetail($proses,$round,$sewer,$date,$line_id)
    {
       $query = $this
                ->db
                ->where('to_char(create_date,\'YYYY-mm-dd\')',$date)
                ->where('master_proses_id',$proses)
                ->where('round',$round)
                ->where('sewer_nik',$sewer)
                ->where('line_id',$line_id)
                ->get('inlinedetail_view');

        return $query->num_rows();
    }

    /*cek grade sebelumnya -- gak jadi*/
    public function cek_grade($header_id,$round,$date)
    {
       $query = $this
                ->db
                ->limit(1)
                ->where('to_char(create_date,\'YYYY-mm-dd\')',$date)
                ->where('inline_header_id',$header_id)
                ->where('round',$round)
                ->get('inlinedetail_view');

        return $query->result();
    }

    // cek nik sewer
    public function getSewerNik($proses_id,$line_id)
    {
       $sql = "SELECT DISTINCT sewer_nik, sewer_name FROM listinspection_view
                WHERE master_proses_id = ? AND line_id = ?";

        return $this->db->query($sql, [$proses_id, $line_id]);
    }

    public function getRow($detail_id)
    {
        $sql = "SELECT
                A.round,A.proses_name,A.sewer_nik,A.defect_id, (
                    SELECT
                        COUNT (round)
                    FROM
                        inlinedetail_view
                    WHERE
                        round = A .round
                    and proses_name=A.proses_name
                    AND sewer_nik=A.sewer_nik
                ) AS jumlah
            FROM
                inlinedetail_view A
            where inline_header_id='$detail_id'
            ORDER BY
                A .round";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    // list proses
    function allposts_count_proses($style)
    {
        $query = $this
                ->db
                ->where('style',$style)
                ->get('proses_view');

        return $query->num_rows();

    }

    function allposts_proses($limit,$start,$col,$dir,$style)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('style',$style)
                ->get('proses_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_proses($limit,$start,$search,$col,$dir,$style)
    {
        $query = $this
                ->db
                ->like('proses_name',strtoupper($search))
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('style',$style)
                ->get('proses_view');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_proses($search,$style)
    {

        $query = $this
                ->db
                ->like('proses_name',strtoupper($search))
                ->where('style',$style)
                ->get('proses_view');

        return $query->num_rows();
    }
    // end list proses

    // list inspection
    public function detail_defect($date,$id)
    {
        $query = $this->db->query("SELECT * from detail_defect_inline 
        where date = '$date' and line_id = $id");

        return $query->result();
    }

    public function sum_defect($date,$id)
    {
        $query = $this->db->query("SELECT sum(count) as persen,total_random 
        from detail_defect_inline where date = '$date' and line_id = $id
        GROUP BY total_random ");

        return $query->row();
    }
    
    function allposts_count_inspect($date,$id)
    {
        $factory = $this->session->userdata('factory');
        $sintak = "select * from f_performance_report('$date',$id, $factory )";
        $query = $this->db->query($sintak);

        return $query->num_rows();
    }

    function allposts_inspect($limit,$start,$col,$dir,$date,$id)
    {
        $factory = $this->session->userdata('factory');
        $sintak = "select * from f_performance_report('$date',$id,$factory) order by date limit $limit offset $start";
        $query = $this->db->query($sintak);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_inspect($limit,$start,$search,$col,$dir,$date,$id)
    {
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->group_start();
        $this->db->like('proses_name',strtoupper($search), 'BOTH');
        $this->db->or_like('sewer_nik', $search, 'BOTH');
        $this->db->group_end();
        $this->db->where('date',$date);
        $this->db->where('line_id',$id);
        $query = $this->db->get('performance_report');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_inspect($search,$date,$id)
    {
        $this->db->group_start();
        $this->db->like('proses_name',strtoupper($search), 'BOTH');
        $this->db->or_like('sewer_nik', $search, 'BOTH');
        $this->db->group_end();
        $this->db->where('date',$date);
        $this->db->where('line_id',$id);
        $query = $this->db->get('performance_report');

        return $query->num_rows();
    }

    // end list inspection
    public function getColor($nik,$proses_id)
    {
        $query  = $this
                 ->db
                 ->limit(1)
                 ->order_by('create_date','desc')
                 ->group_by('master_grade_id,round,color,sewer_nik,create_date')
                 ->where('sewer_nik',$nik)
                 ->where('master_proses_id',$proses_id)
                 ->select('master_grade_id,color,round,sewer_nik,create_date')
                 ->get('inlinedetail_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    // count list defect
    public function reportdefect($id)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');
        $query = $this
                ->db
                ->order_by('line_id')
                ->group_by('line_id,line_name,style,inline_header_id')
                ->where('to_char(create_date,\'YYYY-mm-dd\')',$date)
                ->where('factory_id',$factory)
                ->where('inline_header_id',$id)
                ->select('inline_header_id,line_id,line_name,style')
                ->get('listinspection_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }
    public function detailInlineProses($line_id,$round,$style,$proses_id)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');
        /*$query = "select EXISTS(SELECT DISTINCT
                    round,
                    master_proses_id,
                    factory_id,
                    line_id,
                    STYLE,
                    DATE (create_date) AS DATE,
                    count (master_proses_id) AS jumlah
                FROM
                    inline_detail
                WHERE
                    line_id = $line_id
                AND round = $round
                and master_proses_id=$proses_id
                and style='$style'
                and factory_id=$factory
                AND DATE (create_date) = '$date'
                GROUP BY
                    round,
                    master_proses_id,
                    factory_id,
                    line_id,
                    STYLE,
                    DATE (create_date))";
        return $this->db->query($query);*/
        $where = array(
            'line_id'            => $line_id,
            'round'              => $round,
            'style'              => $style,
            'factory_id'         => $factory,
            'master_proses_id'         => $proses_id,
            'DATE (create_date)=' => $date
        );
        $this->db->where($where);
        return $this->db->get('inline_detail')->num_rows();
	}
	
	 // List pengajuan ad
	 function allposts_count_ad($factory)
	 {   
		 
        $nik = $this->session->userdata('nik');
		$query = $this
				->db
				->where('created_by',$nik)
				->where('factory_id',$factory)
				->get('pengajuan_ad');
	
		 return $query->num_rows();  
 
	 }
	 
	 function allposts_ad($limit,$start,$col,$dir,$factory)
	 {   
        $nik = $this->session->userdata('nik');
		$query = $this
				 ->db
				 ->limit($limit,$start)
				 ->order_by($col,$dir)
				 ->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
				 ->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
				 ->where('pengajuan_ad.created_by',$nik)
				 ->where('pengajuan_ad.factory_id',$factory)
				 ->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
				 ->get('pengajuan_ad');
		 
		 if($query->num_rows()>0)
		 {
			 return $query->result(); 
		 }
		 else
		 {
			 return null;
		 }
		 
	 }
	
	 function posts_search_ad($limit,$start,$search,$col,$dir,$factory)
	 {
        $nik = $this->session->userdata('nik');
		$query = $this
				->db
				->limit($limit,$start)
				->order_by($col,$dir)
				->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
				->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
				->where('pengajuan_ad.created_by',$nik)
				->where('pengajuan_ad.factory_id',$factory)
				->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
				->like('pengajuan_ad.style',strtoupper($search))
				->or_like('pengajuan_ad.poreference',strtoupper($search))
				->get('pengajuan_ad');
		 
		
		 if($query->num_rows()>0)
		 {
			 return $query->result();  
		 }
		 else
		 {
			 return null;
		 }
	 }
 
	 function posts_search_count_ad($search,$factory)
	 {
        $nik = $this->session->userdata('nik');
		$query = $this
				->db
				->where('created_by',$nik)
				->where('factory_id',$factory)
				->like('style',strtoupper($search))
				->or_like('poreference',strtoupper($search))
				->get('pengajuan_ad');
	 
		 return $query->num_rows();
	 }

     public function poSizeQc($line_id,$poreference,$factory){
        $query = $this
                ->db
                ->where('line_id',$line_id)
                ->where('factory_id',$factory)
                ->where('poreference',$poreference)
                ->select('size')
                ->get('ns_active_po_size')
                ->result();

        return $query;
     }

}

/* End of file qc_inspect_model.php */
/* Location: ./application/models/qc_inspect_model.php */
