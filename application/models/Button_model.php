<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Button_model extends CI_Model {

	function cekLineName($line)
	{
		$this->db->where('master_line_id', $line);
		return $this->db->get('master_line')->row_array();
	}
	function getHeader($line,$factory,$style=NULL)
	{
    	if ($style!=NULL) {
    		$this->db->where('style', $style);
    	}
    	$this->db->where(array('line_id'=>$line,'factory_id'=>$factory));
    	$query = $this->db->get('active_style_sewing');
		return $query;
	}
	function getFactory($id)
	{
		$this->db->where('factory_id', $id);
		$query = $this->db->get('master_factory');
		if ($query->num_rows()>0) {
			return $query->row_array();
		}else{
			return 0;
		}
	}
	function styleactive($line,$factory)
	{
		$this->db->where('line_id', $line);
		$this->db->where('factory_id', $factory);
		$query = $this->db->get('status_sewing');
		if ($query->num_rows()>0) {
			return $query->row_array();
		}else{
			return 0;
		}

	
	}

	function cek_master_order($style)
	{
		$this->db->limit(1);
		$this->db->where('value !=', null);
		$this->db->where('style', $style);
		$query = $this->db->get('master_order');
		if ($query->num_rows()>0) {
			return $query->row_array();
		}else{
			return 0;
		}

	}
	function counterSewingdaily($line,$factory,$style)
	{
		$date = date('Y-m-d');
		// $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
		$this->db->where(array('factory_id'=>$factory,'line_id'=>$line,'style'=>$style, 
		'date(create_date)'=>$date));
		$this->db->select('count(counter_pcs) as counter_pcs');
		$day = $this->db->get('sewing_pcs')->row_array()['counter_pcs'];

		return $day;
	}
	function counterSewing($line,$factory,$style)
	{
		$this->db->where(array('factory_id'=>$factory,'line_id'=>$line,'style'=>$style));
		$this->db->select_max('counter_pcs');
		$query = $this->db->get('sewing_pcs')->row_array()['counter_pcs'];

		if ($query!=NULL) {
			return $query;
		}else{
			return 0;
		}
	}

	public function cekBeda($line_id)
    {
		$sql = "select style,line_id,count(*),max(counter_pcs) from sewing_pcs
				WHERE line_id = $line_id
				GROUP BY style,line_id
				HAVING  count(*)<>max(counter_pcs)";
		return $this->db->query($sql);
    }

}

/* End of file Button_model.php */
/* Location: ./application/models/Button_model.php */
