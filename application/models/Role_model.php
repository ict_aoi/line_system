<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends CI_Model {
	function allposts_count()
    {   
        $query = $this
                ->db
                ->get('users');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('active',1)
                ->get('users');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('username',$search)
                ->or_like('nik',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('active',1)
                ->get('users');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('username',$search)
                ->or_like('nik',$search)
                ->where('active',1)
                ->get('users');
    
        return $query->num_rows();
    }
    function getUserDetail()
    {
        $nik = $this->session->userdata('nik');
        $this->db->join('master_factory', 'master_factory.factory_id = users.factory', 'left');
        $this->db->join('level_user', 'level_user.level_id = users.level_id', 'left');
        $this->db->where('nik', $nik);
        $this->db->select('users.nik,users.username,users.email,master_factory.factory_name,level_user.level_name');
        $query = $this->db->get('users');
        if ($query->num_rows()>0) {
            return $query->row_array();
        }else{
            return null;
        }
    }

}

/* End of file role_model.php */
/* Location: ./application/models/role_model.php */