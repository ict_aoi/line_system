<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanModel extends CI_Model {

    function get_buku_hijau($tgl)
    {

        $query = $this->db->query("SELECT * 
        FROM edm_buku_hijau WHERE factory_id = '$factory' and date = '$tgl' 
        ORDER BY no_urut ASC");

        if($query->num_rows()>0)
        {
            // return $query->result(); 
            return $query; 
        }
        else
        {
            return null;
        }
        
    }

    // function allposts_daily_count($tgl,$factory)
    function allposts_daily_count($factory,$dari,$sampai)
    {   
        // {
        //     // $this->db
        //     //     ->where('activity_name','outputqc')
        //     //     ->where('date <=', $tgl)
        //     //     ->select('poreference,  style, size, line_id, sum(output) as out_qc')
        //     //     ->group_by('poreference, style, size, line_id')
        //     //     ->order_by('line_id')
        //     //     ->get('line_summary_po');
            
        //     // $subquery1 = $this->db->_compile_select();

        //     // $this->db->_reset_select(); 

        //     // $this->db
        //     //     ->where('activity_name','folding')
        //     //     ->where('date <=', $tgl)
        //     //     ->select('poreference,  style, size, line_id, sum(output) as out_folding')
        //     //     ->group_by('poreference, style, size, line_id')
        //     //     ->order_by('line_id')
        //     //     ->get('line_summary_po');
        
        //     // $subquery2 = $this->db->_compile_select();

        //     // $this->db->_reset_select(); 

        //     // $query = $this
        //     //         ->db
        //     //         ->where('distribusi.factory_id',$factory)
        //     //         ->where('distribusi.status=','completed') 
        //     //         ->where('date(distribusi.create_date) <=', $tgl)
        //     //         ->select('distribusi.poreference, distribusi.style, distribusi.size, distribusi.line_id, 
        //     //         distribusi.factory_id, sum(distribusi.qty) as total, qc.out_qc, fold.out_folding')
        //     //         ->join('('.$subquery1.')  qc', 'qc.poreference = distribusi.poreference and 
        //     //         qc.size = distribusi.size and qc.line_id = distribusi.line_id and qc.style = distribusi.style', 'left')
        //     //         ->join('('.$subquery2.')  fold', 'fold.poreference = distribusi.poreference and 
        //     //         fold.size = distribusi.size and fold.line_id = distribusi.line_id and fold.style = distribusi.style', 'left')
        //     //         ->group_by('distribusi.poreference, distribusi.style, distribusi.size, distribusi.line_id, distribusi.factory_id
        //     //         qc.out_qc, fold.out_folding')
        //     //         ->order_by('distribusi.line_id', 'asc')
        //     //         ->get('distribusi');
        // }
        // $query = $this->db->query("SELECT d.poreference, d.style, d.size, d.line_id, ml.line_name, ml.no_urut, d.factory_id, sum(d.qty) as total, qc.out_qc, f.out_folding from distribusi d
        // LEFT JOIN (SELECT poreference, style, size, line_id, sum(output) as out_qc from line_summary_po where activity_name = 'outputqc' and date <='$tgl'
        // GROUP BY poreference, style, size, line_id
        // ORDER BY line_id) qc on qc.poreference = d.poreference and qc.style = d.style and qc.size=d.size and qc.line_id = d.line_id
        // LEFT JOIN (SELECT poreference, style, size, line_id, sum(output) as out_folding from line_summary_po where activity_name = 'folding' and date <='$tgl'
        // GROUP BY poreference, style, size, line_id
        // ORDER BY line_id) f on f.poreference = d.poreference and f.style = d.style and f.size=d.size and f.line_id = d.line_id
        // LEFT JOIN master_line ml on ml.master_line_id = d.line_id
        // WHERE d.status = 'completed' and date(d.create_date) <= '$tgl' and d.factory_id = '$factory'
        // GROUP BY d.poreference,d.style, d.size, d.line_id, qc.out_qc, f.out_folding, d.factory_id, ml.line_name, ml.no_urut
        // ORDER BY d.line_id");

        $query = $this->db->query("SELECT
            qo.inline_header_id,
            fh.id,
            ml.line_name,
            qo.line_id,
                        mf.factory_name,
            qo.factory_id,
            qo.style,
            qo.poreference,
            qo.size,
            qo.qty_order,
            qo.qc_output,
            fh.counter as folding_output,
            qo.available as wip_sewing,
            qo.qc_output-COALESCE(fh.counter,0) as wip_folding,
            qo.update_date,
            ml.no_urut
        FROM
            qc_output qo
        LEFT JOIN master_factory mf on mf.factory_id = qo.factory_id
        LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
        LEFT JOIN folding_header fh on fh.inline_header_id=qo.inline_header_id and fh.size=qo.size
        WHERE
            qo.qty_order<> COALESCE(fh.counter,0)
        and qo.update_date>='$dari'
        and qo.update_date<='$sampai'
        and qo.factory_id = '$factory'
        ORDER BY ml.no_urut asc");
        return $query->num_rows();
    }
    
    // function allposts_daily($limit,$start,$col,$dir,$tgl,$factory)
    function allposts_daily($limit,$start,$col,$dir,$factory,$dari,$sampai)
    {   
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            
            $query = $this->db->query("SELECT
                qo.inline_header_id,
                fh.id,
                ml.line_name,
                qo.line_id,
                            mf.factory_name,
                qo.factory_id,
                qo.style,
                qo.poreference,
                qo.size,
                qo.qty_order,
                qo.qc_output,
                fh.counter as folding_output,
                qo.available as wip_sewing,
                qo.qc_output-COALESCE(fh.counter,0) as wip_folding,
                qo.update_date,
                ml.no_urut
            FROM
                qc_output qo
            LEFT JOIN master_factory mf on mf.factory_id = qo.factory_id
            LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
            LEFT JOIN folding_header fh on fh.inline_header_id=qo.inline_header_id and fh.size=qo.size
            WHERE
                qo.qty_order<> COALESCE(fh.counter,0)
            and qo.update_date>='$dari'
            and qo.update_date<='$sampai'
            and qo.factory_id = '$factory'
            order by ml.no_urut, qo.style, qo.poreference
            -- order by $col $dir 
            limit $limit offset $start");
        }
        else {

            $query = $this->db->query("SELECT
                qo.inline_header_id,
                fh.id,
                ml.line_name,
                qo.line_id,
                            mf.factory_name,
                qo.factory_id,
                qo.style,
                qo.poreference,
                qo.size,
                qo.qty_order,
                qo.qc_output,
                fh.counter as folding_output,
                qo.available as wip_sewing,
                qo.qc_output-COALESCE(fh.counter,0) as wip_folding,
                qo.update_date,
                ml.no_urut
            FROM
                qc_output qo
            LEFT JOIN master_factory mf on mf.factory_id = qo.factory_id
            LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
            LEFT JOIN folding_header fh on fh.inline_header_id=qo.inline_header_id and fh.size=qo.size
            WHERE
                qo.qty_order<> COALESCE(fh.counter,0)
            and qo.update_date>='$dari'
            and qo.update_date<='$sampai'
            and qo.factory_id = '$factory'
            ORDER BY ml.no_urut, qo.style, qo.poreference asc");
        }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
	}
	// function posts_daily_search($limit,$start,$col,$dir,$tgl,$search1,$search2,$factory)
	function posts_daily_search($limit,$start,$col,$dir,$search1,$search2,$factory,$dari,$sampai)
    {
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            // $query = $this
            //     ->db
            //     ->like('line_name',strtoupper($search), 'BOTH')
            //     ->or_like('poreference', $search, 'BOTH')

            $text = "";

            $text .= "SELECT
                qo.inline_header_id,
                fh.id,
                ml.line_name,
                qo.line_id,
                            mf.factory_name,
                qo.factory_id,
                qo.style,
                qo.poreference,
                qo.size,
                qo.qty_order,
                qo.qc_output,
                fh.counter as folding_output,
                qo.available as wip_sewing,
                qo.qc_output-COALESCE(fh.counter,0) as wip_folding,
                qo.update_date,
                ml.no_urut
            FROM
                qc_output qo
            LEFT JOIN master_factory mf on mf.factory_id = qo.factory_id
            LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
            LEFT JOIN folding_header fh on fh.inline_header_id=qo.inline_header_id and fh.size=qo.size
            WHERE
                qo.qty_order<> COALESCE(fh.counter,0)
            and qo.update_date>='$dari'
            and qo.update_date<='$sampai'
            and qo.factory_id = '$factory'";
            
            if($search1!=NULL){
                $text .= "and qo.poreference like '%$search1%'
                order by $col $dir limit $limit offset $start";
            }
            else if($search2!=NULL){
                $text .= "and qo.style like '%$search2%'
                order by $col $dir limit $limit offset $start";
            }
        }
        else{
            
            // $query = $this
            //     ->db
            //     ->like('line_name',strtoupper($search), 'BOTH')
            //     ->or_like('poreference', $search, 'BOTH')
            $text = "";

            $text .= "SELECT
                qo.inline_header_id,
                fh.id,
                ml.line_name,
                qo.line_id,
                            mf.factory_name,
                qo.factory_id,
                qo.style,
                qo.poreference,
                qo.size,
                qo.qty_order,
                qo.qc_output,
                fh.counter as folding_output,
                qo.available as wip_sewing,
                qo.qc_output-COALESCE(fh.counter,0) as wip_folding,
                qo.update_date,
                ml.no_urut
            FROM
                qc_output qo
            LEFT JOIN master_factory mf on mf.factory_id = qo.factory_id
            LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
            LEFT JOIN folding_header fh on fh.inline_header_id=qo.inline_header_id and fh.size=qo.size
            WHERE
                qo.qty_order<> COALESCE(fh.counter,0)
            and qo.update_date>='$dari'
            and qo.update_date<='$sampai'
            and qo.factory_id = '$factory'";

            if($search1!=NULL){
                $text .= "and qo.poreference like '%$search1%'
                ORDER BY ml.no_urut, qo.style, qo.poreference";
            }
            else if($search2!=NULL){
                $text .= "and qo.style like '%$search2%'
                ORDER BY ml.no_urut, qo.style, qo.poreference";
            }
        }

        $query = $this->db->query($text);
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    // function posts_daily_search_count($search1,$search2,$tgl,$factory)
    function posts_daily_search_count($search1,$search2,$factory,$dari,$sampai)
    {
        // $query = $this
        //         ->db
        //         ->like('line_name',strtoupper($search), 'BOTH')
        //         ->or_like('poreference', $search, 'BOTH')
        //         ->w

        $text = "";

        $text .= "SELECT
            qo.inline_header_id,
            fh.id,
            ml.line_name,
            qo.line_id,
                        mf.factory_name,
            qo.factory_id,
            qo.style,
            qo.poreference,
            qo.size,
            qo.qty_order,
            qo.qc_output,
            fh.counter as folding_output,
            qo.available as wip_sewing,
            qo.qc_output-COALESCE(fh.counter,0) as wip_folding,
            qo.update_date,
            ml.no_urut
        FROM
            qc_output qo
        LEFT JOIN master_factory mf on mf.factory_id = qo.factory_id
        LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
        LEFT JOIN folding_header fh on fh.inline_header_id=qo.inline_header_id and fh.size=qo.size
        WHERE
            qo.qty_order<> COALESCE(fh.counter,0)
        and qo.update_date>='$dari'
        and qo.update_date<='$sampai'
        and qo.factory_id = '$factory'";

        if($search1!=NULL){
            $text .= "and qo.poreference like '%$search1%'
            ORDER BY ml.no_urut, qo.style, qo.poreference";
        }
        else if($search2!=NULL){
            $text .= "and qo.style like '%$search2%'
            ORDER BY ml.no_urut, qo.style, qo.poreference";
        }

        $query = $this->db->query($text);
    
        return $query->num_rows();
    }

    function get_component($po, $size, $style, $line)
    {
        $query = $this->db->query("SELECT DISTINCT component_name from distribusi_detail_view 
		where poreference = '$po' and size ='$size' and style = '$style' and line_id='$line'");

        if($query->num_rows()>0)
        {
            // return $query->result(); 
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
}
