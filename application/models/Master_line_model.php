<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_line_model extends CI_Model {

    //
    function allposts_count_list($factory)
    {   
        $query = $this
                ->db
                ->where('factory_id',$factory)
                ->get('master_line');
    
        return $query->num_rows();  

    }
    
    function allposts_list($limit,$start,$col,$dir,$factory)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->get('master_line');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_list($limit,$start,$search,$col,$dir,$factory)
    {
        $query = $this
                ->db
                ->like('line_name',strtoupper($search))
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->get('master_line');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_list($search,$factory)
    {
        $query = $this
                ->db
                ->like('line_name',strtoupper($search))
                ->where('factory_id',$factory)
                ->get('master_line');
    
        return $query->num_rows();
    }
	

}

/* End of file master_line_model.php */
/* Location: ./application/models/master_line_model.php */