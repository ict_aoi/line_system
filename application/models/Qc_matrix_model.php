<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_matrix_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('absen',TRUE);
    }
    

	function cekBatch()
    {
		$ie        = $this->session->userdata('nik');
		$factory   = $this->session->userdata('factory');
		
		$this->db->where('finish_date', null);
		$this->db->where('nik_ie', $ie);
		$this->db->where('skill_matrix_batch.factory_id', $factory);
		$this->db->join('master_line', 'master_line.master_line_id = skill_matrix_batch.line_id', 'left');
		$this->db->select('skill_matrix_batch.*,master_line.line_name');
		// $this->db->where('style', $style);
		// $this->db->where('line_id', $line);
        $query  = $this->db->get('skill_matrix_batch');
		
		return $query;
	}
	
	function allposts_count_line()
    {
        $query = $this
                ->db
                ->get('master_line');

        return $query->num_rows();

    }

    function allposts_line($limit,$start,$col,$dir)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_line');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_line($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('line_name',$search)
                ->or_like('line_name',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_line');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_line($search)
    {
        $query = $this
                ->db
                ->like('line_name',$search)
                ->or_like('line_name',$search)
                ->get('master_line');

        return $query->num_rows();
    }

    // po buyer

    function allposts_count_list($factory,$line_id)
    {
        $query = $this
                ->db
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->where('qty_order > qc_output')
                ->select('style')
                ->distinct()
                ->get('qc_output');

        return $query->num_rows();

    }

    function allposts_list($limit,$start,$col,$dir,$factory,$line_id)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->where('qty_order > qc_output')
                ->select('style')
                ->distinct()
                ->get('qc_output');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_list($limit,$start,$search,$col,$dir,$factory,$line_id)
    {
        $query = $this
                ->db
                ->like('style',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->where('qty_order > qc_output')
                ->select('style')
                ->distinct()
                ->get('qc_output');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_list($search,$factory,$line_id)
    {
        $query = $this
                ->db
                ->like('style',$search)
                ->where('factory_id',$factory)
                ->where('line_id',$line_id)
                ->where('qty_order > qc_output')
                ->select('style')
                ->distinct()
                ->get('qc_output');

        return $query->num_rows();
    }
    // defect

    function allposts_count_defect()
    {
        $query = $this
                ->db
                ->get('master_defect');

        return $query->num_rows();

    }

    function allposts_defect($limit,$start,$col,$dir)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where_not_in('defect_id',0)
                ->get('master_defect');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_defect($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('cast(defect_id as text)',$search)
                ->or_like('defect_jenis',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where_not_in('defect_id',0)
                ->get('master_defect');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_defect($search)
    {
        $query = $this
                ->db
                ->like('defect_jenis',$search)
                ->or_like('defect_jenis',$search)
                ->where_not_in('defect_id',0)
                ->get('master_defect');

        return $query->num_rows();
    }
    // end list defect
    function cekheader($line,$style)
    {
                  $this->db->where('style', $style);
                  $this->db->where('line_id', $line);
                //   $this->db->where('poreference', $poreference);
        $query  = $this->db->get('inline_header');
        if($query->num_rows()>0)
        {
            return $query;
        }
        else
        {
            return null;
        }
	}
	
    function maxheader($line_id)
    {
        $factory   = $this->session->userdata('factory');
                  $this->db->limit(1);
                  $this->db->order_by('create_date', 'desc');
                  $this->db->where('factory_id', $factory);
                  $this->db->where('line_id', $line_id);
        $query  = $this->db->get('round_status');
        // var_dump ($query->result());
        // die();

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    function headerdetail($detail_id,$date)
    {
        $output = '';
        $jum =1;
        $i =0;
        $sql = "SELECT
                A.round,A.proses_name,A.sewer_nik,A.color,A.create_date,A.defect_id, (
                    SELECT
                        COUNT (round)
                    FROM
                        inlinedetail_view
                    WHERE
                        round = A .round
                    and proses_name=A.proses_name
                    AND sewer_nik=A.sewer_nik
                    and create_date=A.create_date
                ) AS jumlah
            FROM
                inlinedetail_view A
            where inline_header_id='$detail_id'
            and to_char(create_date,'yyyy-mm-dd'::text)='$date'
            ORDER BY
                A .round";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    public function getDetail($proses,$round,$sewer,$date,$line_id)
    {
       $query = $this
                ->db
                ->where('to_char(create_date,\'YYYY-mm-dd\')',$date)
                ->where('master_proses_id',$proses)
                ->where('round',$round)
                ->where('sewer_nik',$sewer)
                ->where('line_id',$line_id)
                ->get('inlinedetail_view');

        return $query->num_rows();
    }

    /*cek grade sebelumnya -- gak jadi*/
    public function cek_grade($header_id,$round,$date)
    {
       $query = $this
                ->db
                ->limit(1)
                ->where('to_char(create_date,\'YYYY-mm-dd\')',$date)
                ->where('inline_header_id',$header_id)
                ->where('round',$round)
                ->get('inlinedetail_view');

        return $query->result();
    }

    //get mesin
    public function getMesin()
    {
       $query = $this
                ->db
                ->get('master_mesin');

        return $query->result();
    }

    // cek nik sewer
    public function getSewerNik($proses_id,$line_id)
    {
    //    $sql = "SELECT DISTINCT sewer_nik, sewer_name FROM listinspection_view
    //             WHERE master_proses_id = ? AND line_id = ?";

                
       $sql = "SELECT DISTINCT sewer_nik, sewer_name FROM listinspection_view
       WHERE master_proses_id in ($proses_id) AND line_id = $line_id";

        return $this->db->query($sql);
    }

    public function getRow($detail_id)
    {
        $sql = "SELECT
                A.round,A.proses_name,A.sewer_nik,A.defect_id, (
                    SELECT
                        COUNT (round)
                    FROM
                        inlinedetail_view
                    WHERE
                        round = A .round
                    and proses_name=A.proses_name
                    AND sewer_nik=A.sewer_nik
                ) AS jumlah
            FROM
                inlinedetail_view A
            where inline_header_id='$detail_id'
            ORDER BY
                A .round";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	// list proses
	public function getworkflow($style)
    {	
        $query = $this
                ->db
                ->where('style',$style)
                ->order_by('master_workflow_id')
				->get('workflow_view');
				
		if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
	}
	
    function allposts_count_proses($style)
    {
        $query = $this
                ->db
                ->where('style',$style)
                ->order_by('master_workflow_id')
                ->get('workflow_view');

        return $query->num_rows();

    }

    function allposts_proses($limit,$start,$col,$dir,$style)
    {
       $query = $this
                ->db
                // ->limit($limit,$start)
                ->order_by($col,$dir)
                // ->order_by('master_workflow_id')
                ->where('style',$style)
                ->get('workflow_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_proses($limit,$start,$search,$col,$dir,$style)
    {
        $query = $this
                ->db
                ->like('proses_name',strtoupper($search))
                // ->limit($limit,$start)
                ->order_by($col,$dir)
                // ->order_by('master_workflow_id')
                ->where('style',$style)
                ->get('workflow_view');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_proses($search,$style)
    {

        $query = $this
                ->db
                ->like('proses_name',strtoupper($search))
                ->where('style',$style)
                ->order_by('master_workflow_id')
                ->get('workflow_view');

        return $query->num_rows();
    }
    // end list proses

    // list mesin
    function allposts_count_mesin()
    {
        $query = $this
                ->db
                ->order_by('mesin_id')
                ->get('master_mesin');

        return $query->num_rows();

    }

    function allposts_mesin($limit,$start,$col,$dir)
    {
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by('mesin_id')
                ->get('master_mesin');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_mesin($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('nama_mesin',strtoupper($search))
                ->limit($limit,$start)
                ->order_by('mesin_id')
                ->get('master_mesin');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_mesin($search)
    {

        $query = $this
                ->db
                ->like('nama_mesin',strtoupper($search))
                ->order_by('mesin_id')
                ->get('master_mesin');

        return $query->num_rows();
    }
    // end list mesin


    // list inspection
    public function detail_defect($date,$id)
    {
        $query = $this->db->query("SELECT * from detail_defect_inline 
        where date = '$date' and line_id = $id");

        return $query->result();
    }

    public function sum_defect($date,$id)
    {
        $query = $this->db->query("SELECT sum(count) as persen,total_random 
        from detail_defect_inline where date = '$date' and line_id = $id
        GROUP BY total_random ");

        return $query->row();
    }
    
    function allposts_count_inspect($date,$id)
    {
        $factory = $this->session->userdata('factory');
        $sintak = "select * from f_performance_report('$date',$id, $factory )";
        $query = $this->db->query($sintak);

        return $query->num_rows();
    }

    function allposts_inspect($limit,$start,$col,$dir,$date,$id)
    {
        $factory = $this->session->userdata('factory');
        $sintak = "select * from f_performance_report('$date',$id,$factory) order by date limit $limit offset $start";
        $query = $this->db->query($sintak);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_inspect($limit,$start,$search,$col,$dir,$date,$id)
    {
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->group_start();
        $this->db->like('proses_name',strtoupper($search), 'BOTH');
        $this->db->or_like('sewer_nik', $search, 'BOTH');
        $this->db->group_end();
        $this->db->where('date',$date);
        $this->db->where('line_id',$id);
        $query = $this->db->get('performance_report');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_inspect($search,$date,$id)
    {
        $this->db->group_start();
        $this->db->like('proses_name',strtoupper($search), 'BOTH');
        $this->db->or_like('sewer_nik', $search, 'BOTH');
        $this->db->group_end();
        $this->db->where('date',$date);
        $this->db->where('line_id',$id);
        $query = $this->db->get('performance_report');

        return $query->num_rows();
    }

    // end list inspection
    public function getColor($nik,$proses_id)
    {
        $query  = $this
                 ->db
                 ->limit(1)
                 ->order_by('create_date','desc')
                 ->group_by('master_grade_id,round,color,sewer_nik,create_date')
                 ->where('sewer_nik',$nik)
                 ->where('master_proses_id',$proses_id)
                 ->select('master_grade_id,color,round,sewer_nik,create_date')
                 ->get('inlinedetail_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    // count list defect
    public function reportdefect($id)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');
        $query = $this
                ->db
                ->order_by('line_id')
                ->group_by('line_id,line_name,style,inline_header_id')
                ->where('to_char(create_date,\'YYYY-mm-dd\')',$date)
                ->where('factory_id',$factory)
                ->where('inline_header_id',$id)
                ->select('inline_header_id,line_id,line_name,style')
                ->get('listinspection_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }
    public function detailInlineProses($line_id,$round,$style,$proses_id)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');
        /*$query = "select EXISTS(SELECT DISTINCT
                    round,
                    master_proses_id,
                    factory_id,
                    line_id,
                    STYLE,
                    DATE (create_date) AS DATE,
                    count (master_proses_id) AS jumlah
                FROM
                    inline_detail
                WHERE
                    line_id = $line_id
                AND round = $round
                and master_proses_id=$proses_id
                and style='$style'
                and factory_id=$factory
                AND DATE (create_date) = '$date'
                GROUP BY
                    round,
                    master_proses_id,
                    factory_id,
                    line_id,
                    STYLE,
                    DATE (create_date))";
        return $this->db->query($query);*/
        $where = array(
            'line_id'            => $line_id,
            'round'              => $round,
            'style'              => $style,
            'factory_id'         => $factory,
            'master_proses_id'         => $proses_id,
            'DATE (create_date)=' => $date
        );
        $this->db->where($where);
        return $this->db->get('inline_detail')->num_rows();
    }

    public function download_skill_matrix($line,$style,$factory,$remark,$dari,$sampai)
    {
		
		$nik     = $this->session->userdata('nik');

		if($dari==''||$sampai==''){

			// $sql = "SELECT  smh.matrix_header_id, date(smh.create_date) as tgl, ml.line_name, smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name as nama_sewer, smh.nik_ie, smh.burst, msi.name as nama_ie, smh.wip, smh.jenis_wip, smd.round, mw.cycle_time,  mw.smv_spt, smd.durasi  from skill_matrix_header smh
			// LEFT JOIN skill_matrix_detail smd on smd.matrix_header_id = smh.matrix_header_id
			// LEFT JOIN master_line ml on ml.master_line_id = smh.line_id
			// LEFT JOIN master_sewer mss on mss.nik = smh.nik_sewer
			// LEFT JOIN master_sewer msi on msi.nik = smh.nik_ie
			// LEFT JOIN master_proses mp on mp.master_proses_id::varchar = smh.proses_id
			// LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id 
			// LEFT JOIN master_workflow mw on mw.master_proses_id::varchar = smh.proses_id and mw.style = smh.style
			// where mw.delete_at is null and smh.factory_id = '".$factory."'";
			
            $sql = "SELECT smh.style,smh.matrix_header_id, string_agg(mp.proses_name, ', ') as proses_name,  string_agg(smh.proses_id, ', ') as proses_id, sum(mw.smv_spt) as smv_spt,
            SUM(mw.cycle_time) as cycle_time, msi.name as nama_ie,smh.nik_ie, smh.nik_sewer, date(smh.create_date) as tgl, smh.wip, smh.jenis_wip,smh.burst, ml.line_name, mm.nama_mesin, mss.name as nama_sewer, smd.durasi, smd.round FROM master_proses mp join (SELECT style,factory_id, matrix_header_id,unnest(string_to_array(proses_id, ',')) as proses_id, nik_sewer,nik_ie,create_date, wip,jenis_wip,burst, line_id,mesin_id FROM skill_matrix_header
            ) as smh on mp.master_proses_id::varchar = smh.proses_id
            LEFT JOIN master_workflow mw on mw.master_proses_id::varchar = smh.proses_id and mw.style = smh.style
            LEFT JOIN master_line ml on ml.master_line_id = smh.line_id
            LEFT JOIN master_sewer mss on mss.nik = smh.nik_sewer
            LEFT JOIN master_sewer msi on msi.nik = smh.nik_ie
            LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id 
            LEFT JOIN skill_matrix_detail smd on smd.matrix_header_id = smh.matrix_header_id
            where mw.delete_at is null and smh.factory_id = '".$factory."' ";
            
			if($remark == 'single'){
				$sql .= " and smh.nik_ie = '".$nik."'";
			}
			if($line != null){
				$sql.= " and smh.line_id = '".$line."'";
			}
			
			if($style != null){
				$sql.= " and smh.style = '".$style."'";
			}
			
			$sql .= "GROUP BY smh.matrix_header_id, msi.name,smh.style,smh.nik_ie, smh.nik_sewer, smh.create_date, smh.wip, smh.jenis_wip,smh.burst,ml.line_name, mm.nama_mesin, mss.name,smd.durasi, smd.round order by smh.create_date, smd.round ";

		}
		else{
			
			// $sql = "SELECT  smh.matrix_header_id, date(smh.create_date) as tgl, ml.line_name, smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name as nama_sewer, smh.nik_ie, smh.burst, msi.name as nama_ie, smh.wip, smh.jenis_wip, smd.round, mw.cycle_time,  mw.smv_spt, smd.durasi  from skill_matrix_header smh
			// LEFT JOIN skill_matrix_detail smd on smd.matrix_header_id = smh.matrix_header_id
			// LEFT JOIN master_line ml on ml.master_line_id = smh.line_id
			// LEFT JOIN master_sewer mss on mss.nik = smh.nik_sewer
			// LEFT JOIN master_sewer msi on msi.nik = smh.nik_ie
			// LEFT JOIN master_proses mp on mp.master_proses_id::varchar = smh.proses_id
			// LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id 
			// LEFT JOIN master_workflow mw on mw.master_proses_id::varchar = smh.proses_id and mw.style = smh.style
			// where mw.delete_at is null and smh.factory_id = '".$factory."'";
			
            $sql = "SELECT smh.style,smh.matrix_header_id, string_agg(mp.proses_name, ', ') as proses_name,  string_agg(smh.proses_id, ', ') as proses_id, sum(mw.smv_spt) as smv_spt,
            sum(mw.cycle_time) as cycle_time, msi.name as nama_ie,smh.nik_ie, smh.nik_sewer,  date(smh.create_date) as tgl, smh.wip, smh.jenis_wip,smh.burst, ml.line_name, mm.nama_mesin, mss.name as nama_sewer, smd.durasi, smd.round FROM master_proses mp join (SELECT style,factory_id, matrix_header_id,unnest(string_to_array(proses_id, ',')) as proses_id, nik_sewer,nik_ie,create_date, wip,jenis_wip,burst, line_id,mesin_id FROM skill_matrix_header
            ) as smh on mp.master_proses_id::varchar = smh.proses_id
            LEFT JOIN master_workflow mw on mw.master_proses_id::varchar = smh.proses_id and mw.style = smh.style
            LEFT JOIN master_line ml on ml.master_line_id = smh.line_id
            LEFT JOIN master_sewer mss on mss.nik = smh.nik_sewer
            LEFT JOIN master_sewer msi on msi.nik = smh.nik_ie
            LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id 
            LEFT JOIN skill_matrix_detail smd on smd.matrix_header_id = smh.matrix_header_id
            where mw.delete_at is null and smh.factory_id = '".$factory."' ";
            

			
			if($dari != $sampai){
				$sql .= " and smh.create_date >= '$dari' and smh.create_date <= '$sampai' ";
			}else{
				$sql .= " and date(smh.create_date) = '$dari' ";
			}

			if($remark == 'single'){
				$sql .= " and smh.nik_ie = '".$nik."'";
			}
			if($line != null){
				$sql.= " and smh.line_id = '".$line."'";
			}
			
			if($style != null){
				$sql.= " and smh.style = '".$style."'";
			}
			
			$sql .= "GROUP BY smh.matrix_header_id, msi.name,smh.style,smh.nik_ie, smh.nik_sewer, smh.create_date, smh.wip, smh.jenis_wip,smh.burst,ml.line_name, mm.nama_mesin, mss.name,smd.durasi, smd.round order by smh.create_date, smd.round ";
		}
		return $this->db->query($sql);
    }

	function allpost_count_data_matrix($factory,$nik)
    {    
		// $query = "SELECT DISTINCT tgl, line_name, style, proses_id, proses_name, nama_mesin, nik_sewer, nama_sewer, nik_ie, burst, nama_ie, wip, jenis_wip, min(rata_durasi) as rata_durasi, smv_spt, kategori_mesin, is_critical, cycle_time, terbaik
		// from (
		// SELECT  smh.matrix_header_id, date(smh.create_date) as tgl, ml.line_name, 
		// smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name as nama_sewer, 
		// smh.nik_ie, smh.burst, msi.name as nama_ie, smh.wip, smh.jenis_wip, smh.rata_durasi,mw.smv_spt, mw.cycle_time,
		// mm.kategori_mesin, mw.is_critical, x.terbaik from skill_matrix_header smh
		// LEFT JOIN master_line ml on ml.master_line_id = smh.line_id
		// LEFT JOIN master_sewer mss on mss.nik = smh.nik_sewer
		// LEFT JOIN master_sewer msi on msi.nik = smh.nik_ie
		// LEFT JOIN master_proses mp on mp.master_proses_id::varchar = smh.proses_id
		// LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id
		// LEFT JOIN master_workflow mw on mw.master_proses_id::VARCHAR = smh.proses_id and mw.style = smh.style
		// JOIN (SELECT min(master_grade_id) as terbaik, sewer_nik, master_proses_id FROM inlinedetail_view GROUP BY sewer_nik, master_proses_id) x on x.sewer_nik = smh.nik_sewer and x.master_proses_id::VARCHAR = smh.proses_id
		// where mss.name is not null and smh.factory_id = '$factory' 
		// and date(smh.create_date) >= '2020-04-01'
		// ";

		// // if($dari == null || $dari == '' || $sampai == null || $sampai == '' ){
		// // }else{
		// // 	if($dari==$sampai){
		// // 		$query .= " and date(smh.create_date) = '$dari' ";
		// // 	}
		// // 	else{
		// // 		$query .= " and smh.create_date >= '$dari' and smh.create_date <= '$sampai' ";
		// // 	}
	
		// // }

		// if($line != null || $line != ''){
		// 	$query .= " and smh.line_id='$line' ";
		// }
		// else{
		// }

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and smh.mesin_id='$line' ";
		// }
		// else{
		// }

		
		// if($proses != null || $proses != ''){
		// 	$query .= " and smh.proses_id='$proses' ";
		// }
		// else{
		// }

		// $query .= "and mp.proses_name is not null";

		// $query .= " GROUP BY smh.matrix_header_id,ml.line_name, smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name, smh.nik_ie, smh.burst, msi.name, smh.wip, smh.jenis_wip, smh.rata_durasi, 
		// mw.smv_spt, mm.kategori_mesin, mw.is_critical, mw.cycle_time, x.terbaik
		// order by smh.create_date
		// ) as x
		// GROUP BY tgl, line_name, style, proses_id, proses_name, nama_mesin, nik_sewer, nama_sewer, nik_ie, 
		// burst, nama_ie, wip, jenis_wip, smv_spt, kategori_mesin, is_critical, cycle_time, terbaik";

		$query = "SELECT * FROM skill_base where factory_id = '$factory'  ";
		
		if($nik != null || $nik != ''){
			$query .= " and nik_sewer='$nik' ";
		}
		else{
		}

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		
		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }

		// var_dump($query);
		// die();
		$sql = $this->db->query($query);

        return $sql->num_rows();
    }
    
    function allposts_data_matrix($limit,$start,$col,$dir,$factory,$nik)
    {
		$query = "SELECT * FROM skill_base where factory_id = '$factory'  ";
		
		if($nik != null || $nik != ''){
			$query .= " and nik_sewer='$nik' ";
		}
		else{
		}

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }
		
		$query .= " order by $col $dir limit $limit  ";

		$sql = $this->db->query($query);


        if($sql->num_rows()>0)
        {
            return $sql->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_data_matrix($limit,$start,$search,$col,$dir,$factory,$nik)
    {

       	
		$query = "SELECT * FROM skill_base where factory_id = '$factory'  ";
		
		if($nik != null || $nik != ''){
			$query .= " and nik_sewer ='$nik' ";
		}
		else{
		}

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }

		$query .= " and nama_sewer like '%$search%' ";

		$query .= " order by $col $dir limit $limit ";

		$sql = $this->db->query($query);

        if($sql->num_rows()>0)
        {
            return $sql->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_data_matrix($search,$col,$dir,$factory,$line,$mesin,$proses)
    {		
		$query = "SELECT * FROM skill_base where factory_id = '$factory'  ";
		
		if($nik != null || $nik != ''){
			$query .= " and nik_sewer='$nik' ";
		}
		else{
		}

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }		

		$query .= " and nama_sewer like '%$search%' ";

		$query .= " order by $col $dir ";

		$sql = $this->db->query($query);

        return $sql->num_rows();
    }

    function allpost_count_last_line($factory,$nik)
    {    
		// $query = "SELECT DISTINCT tgl, line_name, style, proses_id, proses_name, nama_mesin, nik_sewer, nama_sewer, nik_ie, burst, nama_ie, wip, jenis_wip, min(rata_durasi) as rata_durasi, smv_spt, kategori_mesin, is_critical, cycle_time, terbaik
		// from (
		// SELECT  smh.matrix_header_id, date(smh.create_date) as tgl, ml.line_name, 
		// smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name as nama_sewer, 
		// smh.nik_ie, smh.burst, msi.name as nama_ie, smh.wip, smh.jenis_wip, smh.rata_durasi,mw.smv_spt, mw.cycle_time,
		// mm.kategori_mesin, mw.is_critical, x.terbaik from skill_matrix_header smh
		// LEFT JOIN master_line ml on ml.master_line_id = smh.line_id
		// LEFT JOIN master_sewer mss on mss.nik = smh.nik_sewer
		// LEFT JOIN master_sewer msi on msi.nik = smh.nik_ie
		// LEFT JOIN master_proses mp on mp.master_proses_id::varchar = smh.proses_id
		// LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id
		// LEFT JOIN master_workflow mw on mw.master_proses_id::VARCHAR = smh.proses_id and mw.style = smh.style
		// JOIN (SELECT min(master_grade_id) as terbaik, sewer_nik, master_proses_id FROM inlinedetail_view GROUP BY sewer_nik, master_proses_id) x on x.sewer_nik = smh.nik_sewer and x.master_proses_id::VARCHAR = smh.proses_id
		// where mss.name is not null and smh.factory_id = '$factory' 
		// and date(smh.create_date) >= '2020-04-01'
		// ";

		// // if($dari == null || $dari == '' || $sampai == null || $sampai == '' ){
		// // }else{
		// // 	if($dari==$sampai){
		// // 		$query .= " and date(smh.create_date) = '$dari' ";
		// // 	}
		// // 	else{
		// // 		$query .= " and smh.create_date >= '$dari' and smh.create_date <= '$sampai' ";
		// // 	}
	
		// // }

		// if($line != null || $line != ''){
		// 	$query .= " and smh.line_id='$line' ";
		// }
		// else{
		// }

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and smh.mesin_id='$line' ";
		// }
		// else{
		// }

		
		// if($proses != null || $proses != ''){
		// 	$query .= " and smh.proses_id='$proses' ";
		// }
		// else{
		// }

		// $query .= "and mp.proses_name is not null";

		// $query .= " GROUP BY smh.matrix_header_id,ml.line_name, smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name, smh.nik_ie, smh.burst, msi.name, smh.wip, smh.jenis_wip, smh.rata_durasi, 
		// mw.smv_spt, mm.kategori_mesin, mw.is_critical, mw.cycle_time, x.terbaik
		// order by smh.create_date
		// ) as x
		// GROUP BY tgl, line_name, style, proses_id, proses_name, nama_mesin, nik_sewer, nama_sewer, nik_ie, 
		// burst, nama_ie, wip, jenis_wip, smv_spt, kategori_mesin, is_critical, cycle_time, terbaik";

        $query = "SELECT line_name, max(create_date) as date FROM inlinedetail_view where factory_id = '$factory'";

		if($nik != null || $nik != ''){
			$query .= "and sewer_nik='$nik' GROUP BY line_name  ORDER BY max(create_date) desc";
		}
		else{
		}

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		
		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }

		// var_dump($query);
		// die();
		$sql = $this->db->query($query);

        return $sql->num_rows();
    }
    
    function allposts_last_line($limit,$start,$col,$dir,$factory,$nik)
    {
		
        $query = "SELECT line_name, max(create_date) as date FROM inlinedetail_view where factory_id = '$factory'";

		if($nik != null || $nik != ''){
			$query .= "and sewer_nik='$nik'";
		}
		else{
		}


		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }
		
		$query .= "GROUP BY line_name order by $col $dir , max(create_date) desc limit $limit  ";

		$sql = $this->db->query($query);


        if($sql->num_rows()>0)
        {
            return $sql->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_last_line($limit,$start,$search,$col,$dir,$factory,$nik)
    {

       	
		
        $query = "SELECT line_name, max(create_date) as date FROM inlinedetail_view where factory_id = '$factory'";

		if($nik != null || $nik != ''){
			$query .= "and sewer_nik='$nik'" ;
		}
		else{
		}


		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }

		$query .= " and sewer_nama like '%$search%' ";

		$query .= "GROUP BY line_name order by $col $dir , max(create_date) desc limit $limit ";

		$sql = $this->db->query($query);

        if($sql->num_rows()>0)
        {
            return $sql->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_last_line($search,$col,$dir,$factory,$line,$mesin,$proses)
    {		
		
        $query = "SELECT line_name, max(create_date) as date FROM inlinedetail_view where factory_id = '$factory'";

		if($nik != null || $nik != ''){
			$query .= "and sewer_nik='$nik' ";
		}
		else{
		}


		// if($mesin != null || $mesin != ''){
		// 	$query .= " and mesin_id='$mesin' ";
		// }
		// else{
		// }

		// if($proses != null || $proses != ''){
		// 	$query .= " and proses_id='$proses' ";
		// }
		// else{
		// }		

		$query .= " and sewer_nama like '%$search%' ";

		$query .= "GROUP BY line_name order by $col $dir , max(create_date) desc";

		$sql = $this->db->query($query);

        return $sql->num_rows();
    }

    public function sewer_profil($nik)
    {
        $sql = "SELECT nik, name, department_name, subdept_name, factory from adt_bbigroup_emp_new where nik = '$nik'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }

    
    public function operation_complexity_level($dari,$sampai,$factory,$line,$mesin,$proses)
    {
		// $query = "SELECT DISTINCT tgl, line_name, style, proses_id, proses_name, nama_mesin, nik_sewer, nama_sewer, nik_ie, burst, nama_ie, wip, jenis_wip, min(rata_durasi) as rata_durasi, smv_spt, kategori_mesin, is_critical, cycle_time, terbaik
		// from (
		// SELECT  smh.matrix_header_id, date(smh.create_date) as tgl, ml.line_name, 
		// smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name as nama_sewer, 
		// smh.nik_ie, smh.burst, msi.name as nama_ie, smh.wip, smh.jenis_wip, smh.rata_durasi,mw.smv_spt, mw.cycle_time,
		// mm.kategori_mesin, mw.is_critical, x.terbaik from skill_matrix_header smh
		// LEFT JOIN master_line ml on ml.master_line_id = smh.line_id
		// LEFT JOIN master_sewer mss on mss.nik = smh.nik_sewer
		// LEFT JOIN master_sewer msi on msi.nik = smh.nik_ie
		// LEFT JOIN master_proses mp on mp.master_proses_id::varchar = smh.proses_id
		// LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id
		// LEFT JOIN master_workflow mw on mw.master_proses_id::VARCHAR = smh.proses_id and mw.style = smh.style
		// JOIN (SELECT min(master_grade_id) as terbaik, sewer_nik, master_proses_id FROM inlinedetail_view GROUP BY sewer_nik, master_proses_id) x on x.sewer_nik = smh.nik_sewer and x.master_proses_id::VARCHAR = smh.proses_id
		// where mss.name is not null and smh.factory_id = '$factory' 
		// and date(create_date) >= '2020-04-01'
		// ";

		// if($dari == null || $dari == '' || $sampai == null || $sampai == '' ){
		// }else{
		// 	if($dari==$sampai){
		// 		$query .= " and date(smh.create_date) = '$dari' ";
		// 	}
		// 	else{
		// 		$query .= " and smh.create_date >= '$dari' and smh.create_date <= '$sampai' ";
		// 	}
	
		// }

		// if($line != null || $line != ''){
		// 	$query .= " and smh.line_id='$line' ";
		// }
		// else{
		// }

		// if($mesin != null || $mesin != ''){
		// 	$query .= " and smh.mesin_id='$mesin' ";
		// }
		// else{
		// }

		
		// if($proses != null || $proses != ''){
		// 	$query .= " and smh.proses_id='$proses' ";
		// }
		// else{
		// }

		// $query .= "and mp.proses_name is not null";

		// $query .= " GROUP BY smh.matrix_header_id,ml.line_name, smh.style, smh.proses_id, mp.proses_name, mm.nama_mesin, smh.nik_sewer, mss.name, smh.nik_ie, smh.burst, msi.name, smh.wip, smh.jenis_wip, smh.rata_durasi, 
		// mw.smv_spt, mm.kategori_mesin, mw.is_critical, mw.cycle_time, x.terbaik
		// order by smh.create_date
		// ) as x
		// GROUP BY tgl, line_name, style, proses_id, proses_name, nama_mesin, nik_sewer, nama_sewer, nik_ie, 
		// burst, nama_ie, wip, jenis_wip, smv_spt, kategori_mesin, is_critical, cycle_time, terbaik";

		$query = "SELECT * FROM skill_base where factory_id = '$factory'  ";
		
		if($line != null || $line != ''){
			$query .= " and line_id='$line' ";
		}
		else{
		}

		if($mesin != null || $mesin != ''){
			$query .= " and mesin_id='$mesin' ";
		}
		else{
		}

		if($proses != null || $proses != ''){
			$query .= " and proses_id='$proses' ";
		}
		else{
		}

		// var_dump($query);
		// die();
		return $this->db->query($query);
	}
	
	
	function allpost_count_data_line_balancing($factory,$remark,$line,$style)
    {    
		$nik     = $this->session->userdata('nik');
		$query = "SELECT skill_matrix_batch.*, master_sewer.name, master_line.line_name, master_line.no_urut from skill_matrix_batch
		LEFT JOIN master_sewer on master_sewer.nik = skill_matrix_batch.nik_ie
		LEFT JOIN master_line on master_line.master_line_id = skill_matrix_batch.line_id
		where skill_matrix_batch.factory_id = '$factory' ";
		
		// if($remark == 'single'){
		// 	$sql .= " and skill_matrix_batch.nik_ie = '".$nik."'";
		// }
		
		if($line != null || $line != ''){
			$query .= " and line_id = '$line' ";
		}

		if($style != null || $style != ''){
			$query .= " and style='$style' ";
		}

		$sql = $this->db->query($query);

        return $sql->num_rows();
    }
    
    function allposts_data_line_balancing($limit,$start,$col,$dir,$factory,$remark,$line,$style)
    {
		
		$nik     = $this->session->userdata('nik');
		$query = "SELECT skill_matrix_batch.*, master_sewer.name, master_line.line_name, master_line.no_urut from skill_matrix_batch
		LEFT JOIN master_sewer on master_sewer.nik = skill_matrix_batch.nik_ie
		LEFT JOIN master_line on master_line.master_line_id = skill_matrix_batch.line_id
		where skill_matrix_batch.factory_id = '$factory' ";
		
		// if($remark == 'single'){
		// 	$sql .= " and skill_matrix_batch.nik_ie = '".$nik."'";
		// }
		
		if($line != null || $line != ''){
			$query .= " and line_id = '$line' ";
		}

		if($style != null || $style != ''){
			$query .= " and style='$style' ";
		}

		$query .= " order by $col $dir limit $limit  ";

		$sql = $this->db->query($query);


        if($sql->num_rows()>0)
        {
            return $sql->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_data_line_balancing($limit,$start,$search,$col,$dir,$factory,$remark)
    {

       	
		$query = "SELECT * FROM skill_base where factory_id = '$factory'  ";
		
		if($line != null || $line != ''){
			$query .= " and line_id='$line' ";
		}
		else{
		}

		if($mesin != null || $mesin != ''){
			$query .= " and mesin_id='$mesin' ";
		}
		else{
		}

		if($proses != null || $proses != ''){
			$query .= " and proses_id='$proses' ";
		}
		else{
		}

		$query .= " and nama_sewer like '%$search%' ";

		$query .= " order by $col $dir limit $limit ";

		$sql = $this->db->query($query);

        if($sql->num_rows()>0)
        {
            return $sql->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_data_line_balancing($search,$col,$dir,$factory,$line,$mesin,$proses)
    {		
		$query = "SELECT * FROM skill_base where factory_id = '$factory'  ";
		
		if($line != null || $line != ''){
			$query .= " and line_id='$line' ";
		}
		else{
		}

		if($mesin != null || $mesin != ''){
			$query .= " and mesin_id='$mesin' ";
		}
		else{
		}

		if($proses != null || $proses != ''){
			$query .= " and proses_id='$proses' ";
		}
		else{
		}		

		$query .= " and nama_sewer like '%$search%' ";

		$query .= " order by $col $dir ";

		$sql = $this->db->query($query);

        return $sql->num_rows();
    }


}

/* End of file qc_inspect_model.php */
/* Location: ./application/models/qc_inspect_model.php */
