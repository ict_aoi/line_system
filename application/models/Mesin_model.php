<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesin_model extends CI_Model {

    function getData($id)
    {
        $query = $this->db->where('mesin_id',$id)->get('master_mesin');

        return $query->row_array();
    }
    
	function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_mesin');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_mesin');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('adidas_name',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_mesin');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('adidas_name',$search)
                ->get('master_mesin');
    
        return $query->num_rows();
    }
	

}

/* End of file master_proses_model.php */
/* Location: ./application/models/master_proses_model.php */