<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResewingModel extends CI_Model {

    function get_buku_hijau($tgl)
    {

        $query = $this->db->query("SELECT * 
        FROM edm_buku_hijau WHERE factory_id = '$factory' and date = '$tgl' 
        ORDER BY no_urut ASC");

        if($query->num_rows()>0)
        {
            // return $query->result(); 
            return $query; 
        }
        else
        {
            return null;
        }
        
    }

	function allposts_style_count($line)
    {   
        $query = $this->db->query("SELECT
            ih.style,
            ih.line_id,
            ml.line_name,
            ih.factory_id,
            SUM (ih.targetqty) AS targetqty,
            sw.qty_sewing
        FROM
            inline_header ih
        LEFT JOIN master_line ml on ml.master_line_id = ih.line_id
        LEFT JOIN 
        (SELECT
            COUNT (*) AS qty_sewing,
            STYLE,
            line_id,
            factory_id
        FROM
            sewing_pcs
        GROUP BY 
        STYLE,
            line_id,
            factory_id)
        sw on sw.style=ih.style and sw.line_id=ih.line_id and sw.factory_id=ih.factory_id
        WHERE
            ih.status_po IS NULL and ih.line_id=$line and ih.isactive='t'
        GROUP BY
            ih.style,
            ih.line_id,
            ih.factory_id,sw.qty_sewing,
            ml.line_name
        HAVING sw.qty_sewing>SUM (ih.targetqty)
        ORDER BY style,line_id");
        return $query->num_rows();
    }
    
    function allposts_style($limit,$start,$col,$dir,$line)
    {   
        $query = $this->db->query("SELECT
            ih.style,
            ih.line_id,
            ml.line_name,
            ih.factory_id,
            SUM (ih.targetqty) AS targetqty,
            sw.qty_sewing
        FROM
            inline_header ih
        LEFT JOIN master_line ml on ml.master_line_id = ih.line_id
        LEFT JOIN 
        (SELECT
            COUNT (*) AS qty_sewing,
            STYLE,
            line_id,
            factory_id
        FROM
            sewing_pcs
        GROUP BY 
        STYLE,
            line_id,
            factory_id)
        sw on sw.style=ih.style and sw.line_id=ih.line_id and sw.factory_id=ih.factory_id
        WHERE
            ih.status_po IS NULL and ih.line_id=$line and ih.isactive='t'
        GROUP BY
            ih.style,
            ih.line_id,
            ih.factory_id,sw.qty_sewing,
            ml.line_name
        HAVING sw.qty_sewing>SUM (ih.targetqty)
        order by $col $dir limit $limit offset $start");

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
	}
	
	function posts_style_search($limit,$start,$col,$dir,$search,$line)
    {
        $cari = strtoupper($search);
        $query = $this->db->query("SELECT
            ih.style,
            ih.line_id,
            ml.line_name,
            ih.factory_id,
            SUM (ih.targetqty) AS targetqty,
            sw.qty_sewing
        FROM
            inline_header ih
        LEFT JOIN master_line ml on ml.master_line_id = ih.line_id
        LEFT JOIN 
        (SELECT
            COUNT (*) AS qty_sewing,
            STYLE,
            line_id,
            factory_id
        FROM
            sewing_pcs
        GROUP BY 
        STYLE,
            line_id,
            factory_id)
        sw on sw.style=ih.style and sw.line_id=ih.line_id and sw.factory_id=ih.factory_id
        WHERE
            ih.status_po IS NULL and ih.line_id=$line and ih.isactive='t'
            and ih.style like '%$cari%'
        GROUP BY
            ih.style,
            ih.line_id,
            ih.factory_id,sw.qty_sewing,
            ml.line_name
        HAVING sw.qty_sewing>SUM (ih.targetqty)
        order by $col $dir limit $limit offset $start");
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_style_search_count($search,$line)
    {
        $query = $this->db->query("SELECT
            ih.style,
            ih.line_id,
            ml.line_name,
            ih.factory_id,
            SUM (ih.targetqty) AS targetqty,
            sw.qty_sewing
        FROM
            inline_header ih
        LEFT JOIN master_line ml on ml.master_line_id = ih.line_id
        LEFT JOIN 
        (SELECT
            COUNT (*) AS qty_sewing,
            STYLE,
            line_id,
            factory_id
        FROM
            sewing_pcs
        GROUP BY 
        STYLE,
            line_id,
            factory_id)
        sw on sw.style=ih.style and sw.line_id=ih.line_id and sw.factory_id=ih.factory_id
        WHERE
            ih.status_po IS NULL and ih.line_id=$line and ih.isactive='t'
            and ih.style like '%$cari%'
        GROUP BY
            ih.style,
            ih.line_id,
            ih.factory_id,sw.qty_sewing,
            ml.line_name
        HAVING sw.qty_sewing>SUM (ih.targetqty)
        ORDER BY style,line_id");
    
        return $query->num_rows();
    }

}
