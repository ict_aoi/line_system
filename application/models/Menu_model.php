<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	function allposts_count()
    {   
        $query = $this
                ->db
                ->get('menu');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('menu');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('nama_menu',$search)
                ->or_like('link',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('menu');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('nama_menu',$search)
                ->or_like('link',$search)
                ->get('menu');
    
        return $query->num_rows();
    }

}

/* End of file menu_model.php */
/* Location: ./application/models/menu_model.php */