<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QcEndlineModel extends CI_Model {
	public function headerline()
	{
		$factory = $this->session->userdata('factory');
		$line_id = $this->session->userdata('line_id');

		$this->db->order_by('startdateactual', 'desc');
		$this->db->where(array('line_id'=>$line_id,'factory_id'=>$factory));
		$this->db->where('balance<0', NULL,FALSE);
		$this->db->select('inline_header_id,poreference,style,startdateactual,article');
		$this->db->distinct();
		return $this->db->get('detail_order_bysize');
	}
	public function listporefence($factory)
	{
		$line_id = $this->session->userdata('line_id');
		
		$this->db->where('factory_id', $factory);
		$this->db->where('line_id', $line_id);
		$query = $this->db->get('active_po_endline');
		return $query;
		/*$line_id = $this->session->userdata('line_id');
		$query ="SELECT
					ih.inline_header_id,
					ih.poreference,
					ih.style,
					ih.line_id,
					ih.targetqty,
					COALESCE(sum(qe.counter_qc),0) as counter_qc
				FROM
					inline_header ih
				LEFT JOIN qc_endline qe on qe.inline_header_id=ih.inline_header_id
				WHERE
					ih.line_id = $line_id
					and ih.factory_id=$factory
					and ih.isactive='t'
				GROUP BY ih.inline_header_id,
					ih.poreference,
					ih.style,
					ih.line_id,
					ih.targetqty
				having COALESCE(sum(qe.counter_qc),0) < ih.targetqty
				ORDER BY ih.startdateactual desc";
		return $this->db->query($query);*/
	}
	public function listSize($id)
	{
		$this->db->order_by('size', 'asc');
		$this->db->where('inline_header_id', $id);
		$query = $this->db->get('inline_header_size');
		return $query;
				
	}
	public function qcEndlineDetail($header_id,$size_id)
	{
		$this->db->where('inline_header_id',$header_id);
		$this->db->where('header_size_id',$size_id);
		
		return $this->db->get('qc_endline')->row_array();

	}
	function counterQcall($header_id,$size_id)
	{
		$this->db->where('inline_header_id',$header_id);
		$this->db->where('header_size_id',$size_id);
		$query = $this->db->get('qc_endline')->row_array();
		
		if ($query['counter_qc']!=NULL) {
			return $query;
		}else{
			return 0;
		}
	}
	public function counterDaily($id)
	{
		$date = date('Y-m-d');
		/*$this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
		$this->db->where('qc_endline_id',$id);
		$this->db->where('counter_pcs<>',0);
		$query = $this->db->get('qc_endline_trans');*/

		// $this->db->where(array('factory_id'=>$factory,'line_id'=>$line,'style'=>$style, 
		// 'date(create_date)'=>$date));
		// $this->db->select('count(counter_pcs) as counter_pcs');
		// $day = $this->db->get('sewing_pcs')->row_array()['counter_pcs'];

		$this->db->where('date(create_date)',$date);
		$this->db->where('qc_endline_id',$id);
		$this->db->where('counter_pcs<>',0);
		$this->db->select('count(counter_pcs) as counter_pcs');
		$query = $this->db->get('qc_endline_trans')->row_array()['counter_pcs'];

		// $this->db->where('to_char(create_date,\'YYYY-mm-dd\')<',$date);
		// $this->db->where('qc_endline_id',$id);
		// $this->db->where('counter_pcs<>',0);
		// $this->db->select_max('counter_pcs');
		// $before = $this->db->get('qc_endline_trans')->row_array()['counter_pcs'];
		// $query = $day-$before;
		


		if ($query!=NULL) {
			return $query;
		}else{
			return 0;
		}
	}
	public function counterStyle($line_id,$style)
	{
		$factory = $this->session->userdata('factory');
		
		$q = "SELECT
				sum(counter_qc)+sum(repairing) as counter_qc
			FROM
				qc_endline
			WHERE
				inline_header_id IN (
					SELECT
						inline_header_id
					FROM
						inline_header
					WHERE
						style = '$style'
					AND line_id = $line_id
					and factory_id=$factory)"; 
        $query = $this->db->query($q)->row_array();
       
		if ($query['counter_qc']!=NULL) {
			return $query['counter_qc'];
		}else{
			return 0;
		}
	}
	function qtyOrderSize($id,$size)
	{
		$this->db->where(array('inline_header_id'=>$id,'header_size_id'=>$size));
		return $this->db->get('inline_header_size')->row_array();
	}
	
	function cekInputQc($id,$size_id)
	{
		$this->db->where('header_size_id', $size_id);
		$this->db->where('inline_header_id', $id);
		$query = $this->db->get('qc_endline');
		if ($query->num_rows()>0) {
			return 1;
		}else{
			return 0;
		}
	}
	function getEndline($id,$size_id)
	{
		$this->db->where('header_size_id', $size_id);
		$this->db->where('inline_header_id', $id);
		return $this->db->get('qc_endline')->row_array();

	}
	function countPcs($id)
	{
		$this->db->where('qc_endline_id',$id);
		$this->db->select_max('counter_pcs');
		$query = $this->db->get('qc_endline_trans')->row_array();

		if ($query['counter_pcs']!=NULL) {
			return $query['counter_pcs'];
		}else{
			return 0;
		}
	}
	public function cekCountertrans()
	{
		// $line_id = $this->session->userdata('line_id');
		$query = "SELECT
					qec.qc_endline_id,ih.line_id,
					qec.counter_qc,
					COALESCE(counter_pcs.count,0)
				FROM
					qc_endline qec
				LEFT JOIN (
					SELECT
					qc_endline_id,
					count (qc_endline_id)
				FROM
					qc_endline_trans
				WHERE
					counter_pcs<>0
				GROUP BY qc_endline_id
				) counter_pcs on counter_pcs.qc_endline_id=qec.qc_endline_id
				LEFT JOIN inline_header ih on ih.inline_header_id=qec.inline_header_id
				LEFT JOIN inline_header_size ihz on ihz.header_size_id=qec.header_size_id

				WHERE

				qec.counter_qc > 0
				and qec.counter_qc <> COALESCE(counter_pcs.count,0)";
	    return $this->db->query($query);
	}

	// -- 	--qec.qc_endline_id='72d70060-e89f-42a8-afbf-e67b9581e5e7'
	// -- ih.line_id=$line_id
	// 	-- and	
	function getCounterDefect($endline_id,$trans_id)
	{
		$this->db->group_by('qc_endline_trans_id');
		$this->db->where('status_defect', 0);
		$this->db->where('qc_endline_id', $endline_id);
		$this->db->distinct();
		$this->db->select('qc_endline_trans_id');
		return $this->db->get('qc_endline_defect')->num_rows();
		
	}
	function getTotalDefect($endline_id)
	{
		$this->db->group_by('qc_endline_trans_id');
		$this->db->where('status_defect', 1);
		$this->db->where('qc_endline_id', $endline_id);
		$this->db->distinct();
		$this->db->select('qc_endline_trans_id');
		return $this->db->get('qc_endline_defect')->num_rows();
		
	}
	
	public function show_repairing($qc_id)
	{
		$query = "SELECT
					qt.qc_endline_trans_id,
					qt.qc_endline_id,
					array_to_string(array_agg(qd.defect_id), ','::text) AS defect_id,
					DATE (qt.create_date) as create_date
				FROM
					qc_endline_trans qt
				LEFT JOIN qc_endline_defect qd ON qd.qc_endline_trans_id = qt.qc_endline_trans_id
				WHERE
					qt.counter_pcs = 0
				and qt.qc_endline_id='$qc_id'
				and	qd.status_defect=0
				GROUP BY 
					qt.qc_endline_trans_id,
					qt.qc_endline_id,
					qt.create_date";
		return $this->db->query($query)->result();
	}
	public function countdefect_endline($line_id,$date)
	{
		$factory_id = $this->session->userdata('factory');
		$query = "SELECT
					ih.line_id,					
					A.qc_endline_trans_id,
					COUNT (A .defect_id) AS total
				FROM
					qc_endline_defect A
				LEFT JOIN inline_header ih ON ih.inline_header_id = A .inline_header_id
				AND ih. STYLE = A . STYLE
				WHERE
					DATE (A .create_date) = '$date' and  ih.line_id='$line_id' and A.factory_id='$factory_id'
				GROUP BY
					ih.line_id,A.qc_endline_trans_id";
		return $this->db->query($query);
	}
	public function cekHeader($id,$size_id)
	{
		$this->db->where(array('inline_header.inline_header_id'=>$id,'inline_header_size.header_size_id'=>$size_id));
		$this->db->join('inline_header_size', 'inline_header_size.inline_header_id = inline_header.inline_header_id', 'left');
		$this->db->select('inline_header.*, inline_header_size.size');
		return $this->db->get('inline_header');
	}
	public function result_inspect($line_id, $factory_id)
	{
		$date = date('Y-m-d');
		$sql ="SELECT
					count(*) as counter_pcs
				FROM
					qc_endline_trans qet
				JOIN inline_header ih on ih.inline_header_id=qet.inline_header_id
				where DATE(qet.create_date)='$date' and ih.line_id=$line_id and counter_pcs<>0 and qet.factory_id=$factory_id";
	

		$query = $this->db->query($sql)->row_array();

		if ($query['counter_pcs']!=NULL) {
			return $query['counter_pcs'];
		}else{
			return 0;
		}

		// return $this->db->query($query, [$line_id, $factory_id, 0, date('Y-m-d')])->row_array();
	}

	public function list_result_inspect($line_id, $factory_id)
	{
		// $query = "SELECT * FROM edm_buku_hijau WHERE DATE = ? AND line_id = ? AND factory_id = ?";
		$date = date('Y-m-d');
		return $this->db->query("SELECT * from edm_buku_hijau where factory_id = '$factory_id' 
		and line_id = '$line_id' and date = '$date'")->result();
	}

	/**/
	public function defect_outstanding($line_id,$factory)
	{
		$query = "SELECT
					qe.inline_header_id,
					qe.header_size_id,
					qe.qc_endline_id,
					ih.line_id,
					ih.poreference,
					ih.article,
					ih.style,
					ihz.size,
					qe.repairing
				FROM
					qc_endline qe
				LEFT JOIN inline_header ih on ih.inline_header_id=qe.inline_header_id
				LEFT JOIN inline_header_size ihz on ihz.header_size_id=qe.header_size_id
				WHERE
					qe.repairing <> 0
				and ih.line_id=$line_id and isactive='t' and qe.factory_id=$factory";
		return $this->db->query($query);
	}
	public function detailDefect($qc_endline_id)
	{
		$date = date('Y-m-d');
		$sql = "SELECT
					qed.defect_id,
					md.defect_jenis,
					count(qed.defect_id) as jumlah_defect
				FROM
					qc_endline_defect qed
				LEFT JOIN master_defect md on md.defect_id=qed.defect_id
				WHERE
					qed.qc_endline_id = '$qc_endline_id' and DATE(create_date)='$date'
				GROUP BY md.defect_jenis,qed.defect_id";
		return $this->db->query($sql)->result();
	}
	public function detailPo($inline_header_id,$size)
	{
		$this->db->where('size', $size);
		$this->db->where('inline_header_id', $inline_header_id);
		return $this->db->get('qc_output');
	}
	public function header_po($poreference)
	{
		$line_id = $this->session->userdata('line_id');

		$sql ="SELECT
					ih.inline_header_id,ihz.header_size_id,
					ih.line_id,
					ih.poreference,
					ih.style,
					ih.article,
					ihz.size,
					ihz.qty
				FROM
					inline_header ih
				JOIN inline_header_size ihz ON ihz.inline_header_id = ih.inline_header_id
				where ih.poreference like '%$poreference%'
				and ih.isactive = 't'
				and line_id=$line_id";
		return $this->db->query($sql);
	}

	public function DetailEndline($qc_end_id)
	{
		$line_id = $this->session->userdata('line_id');
		$sql = "select * from edm_buku_hijau where qc_endline_id='$qc_end_id' and line_id=$line_id";

		return $this->db->query($sql);
	}

	public function qc_output($header_id,$size_id)
	{
		$query = $this->db->query("SELECT * FROM qc_output WHERE 
		inline_header_id = '$header_id' and header_size_id = '$size_id'");

		return $query->row();
	}

	public function cek_qc_output($header_id,$size_id)
	{
		$query = $this->db->query("SELECT * FROM qc_output WHERE 
		inline_header_id = '$header_id' and header_size_id = '$size_id'");

		return $query;
	}

	public function cek_daily($qc_id)
	{
		$now = date('Y-m-d');
		$this->db->query("UPDATE qc_endline SET counter_daily=0, 
		update_date = '$now' WHERE qc_endline_id = '$qc_id'");
	}

	public function update_counter($id)
	{
		// $update = $this->db->callFunction('update_counter', $id);
		$query = $this->db->query("SELECT * FROM update_counter('$id')");
		return $query->num_rows();
	}

	public function update_counter_qc($id)
	{
		$query = $this->db->query("SELECT * FROM update_counter_qc('$id')");
		return $query->num_rows();
	}

}

/* End of file QcEndlineModel.php */
/* Location: ./application/models/QcEndlineModel.php */