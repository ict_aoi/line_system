<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FoldingModel extends CI_Model {

	public function listoutstanding()
	{
		$line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');

		$this->db->order_by('startdateactual', 'desc');
		$this->db->where('line_id', $line_id);
		$this->db->where('factory_id', $factory);
		$this->db->where('balance<0', NULL,FALSE);
		$this->db->where('remark', '1');
		$this->db->select('inline_header_id,poreference,style,article,startdateactual');
		$this->db->distinct();
		return $this->db->get('active_po_folding_new');
	}
	public function listporeference()
	{
		$line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');

		$this->db->where('line_id', $line_id);
		$this->db->where('factory_id', $factory);
		$this->db->distinct();
		$this->db->select('inline_header_id,poreference,style,factory_id');
		$query 	= $this->db->get('listpo_folding');
		return $query;
	}
	public function listsize($po,$id)
	{
		$this->db->where(array('poreference'=> $po,'inline_header_id'=>$id));
		 return $this->db->get('listpo_folding');
	}
	public function header($header_id)
	{
		return $this->db->get_where('inline_header',array('inline_header_id'=>$header_id))->row_array();
	}
	public function package($scan)
	{
		$this->fg->where('barcode_id', $scan);
		return $this->fg->get('package')->row_array();
	}
	public function package_detail($scan_id)
	{
		$this->fg->where('scan_id', $scan_id);
		return $this->fg->get('package_detail')->row_array();
	}
	public function po_summary($po)
	{
		$this->fg->where('po_number', $po);
		return  $this->fg->get('po_summary')->row_array();
	}
	public function counterFoldingSets($style,$poreference,$size)
	{
		$this->db->like('style', $style, 'BOTH');
		$this->db->where('size', $size);
		$this->db->where('poreference', $poreference);
		$this->db->select('sum(counter) as counter');
		return $this->db->get('folding_header')->row_array()['counter'];
	}
	public function counter_folding($header_id,$size)
	{
		$this->db->where('size', $size);
		$this->db->where('inline_header_id', $header_id);
		$query = $this->db->get('folding_header')->row_array();
		if ($query['counter']!=NULL) {
			return $query;
		}else{
			return 0;
		}
	}
	
	public function counterFoldingMax($header_id,$size)
	{
		$this->db->where('size', $size);
		$this->db->where('inline_header_id', $header_id);
		$this->db->select_max('counter_folding');
		$query = $this->db->get('folding')->row_array()['counter_folding'];
		if ($query!=NULL) {
			return $query;
		}else{
			return 0;
		}

	}
	public function counterPackage($header_id,$size,$barcode_package)
	{
		$this->db->where('barcode_package', $barcode_package);
		$this->db->where(array('inline_header_id'=>$header_id,'size'=>$size));
		$query = $this->db->get('folding')->num_rows();
		return $query;
	}
	public function counterPackageBarcode($barcode_package)
	{
		$this->db->where('barcode_package', $barcode_package);
		$query = $this->db->get('folding')->num_rows();
		return $query;
	}
	public function getUnallocation($style,$poreference,$size,$qty)
	{
		$this->db->limit($qty);
		$this->db->order_by('counter_folding', 'asc');
		$this->db->where('status_folding', 'unallocation');
		$this->db->where('size', $size);
		$this->db->where('poreference', $poreference);
		$this->db->where('style', $style);
		return $this->db->get('folding');
	}
	public function getallocation($inline_header_id,$size,$status)
	{
		$this->db->where('status_folding', $status);
		$this->db->where('size', $size);
		$this->db->where('inline_header_id', $inline_header_id);
		return $this->db->get('folding');
	}
	public function totalKarton($scan_id)
	{
		$this->db->where('scan_id', $scan_id);
		$this->db->distinct();
		$this->db->select('barcode_id');
		$query = $this->db->get('folding_package_barcode')->num_rows();
		return $query;
	}
	public function countertoday($header_id,$size)
	{
		$date = date('Y-m-d');
		$this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
		$this->db->where(array('inline_header_id'=>$header_id,'size'=>$size));
		$query = $this->db->get('folding')->num_rows();
		if ($query>0) {
			return $query;
		}else{
			return 0;
		}
	}
	public function counterScan($barcode_package)
	{
		$this->db->where('barcode_id', $barcode_package);
		$query = $this->db->get('folding_package_barcode')->row_array()['counter'];
		if ($query!=NULL) {
			return $query;
		}else{
			return 0;
		}
	}
	public function foldingPackage($scan_id)
	{
		$this->db->where('scan_id', $scan_id);
		return $this->db->get('folding_package')->row_array();
	}
	// data table serverside list po
	function allposts_count_headerfolding()
    {   
    	$line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');

        $this->db->where('line_id', $line_id);
        $this->db->where('remark', 1);
		$this->db->where('factory_id', $factory);
		$this->db->distinct();
		$this->db->select('inline_header_id,poreference,style,factory_id,startdateactual');
		$query 	= $this->db->get('listpo_folding');
    
        return $query->num_rows();  

    }
    
    function allposts_headerfolding($limit,$start,$col,$dir)
    {   
        $line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');
		
		$this->db->limit($limit,$start);   
		$this->db->order_by('startdateactual','desc');     
        $this->db->where('line_id', $line_id);
        $this->db->where('remark', 1);
		$this->db->where('factory_id', $factory);
		$this->db->distinct();
		$this->db->select('inline_header_id,poreference,style,factory_id,startdateactual');
		$query 	= $this->db->get('listpo_folding');

        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_headerfolding($limit,$start,$search,$col,$dir)
    {	
    	$line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');
       	
       	$this->db->limit($limit,$start);
		$this->db->order_by('startdateactual','desc');
        $this->db->like('poreference',strtoupper($search), 'BOTH');
        $this->db->or_like('style',strtoupper($search), 'BOTH');
        $this->db->where('line_id', $line_id);
        $this->db->where('remark', 1);
		$this->db->where('factory_id', $factory);
		$this->db->distinct();
		$this->db->select('inline_header_id,poreference,style,factory_id,startdateactual');
		$query 	= $this->db->get('listpo_folding');
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_headerfolding($search)
    {
    	$line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');

        $this->db->like('poreference',strtoupper($search), 'BOTH');
        $this->db->or_like('style',strtoupper($search), 'BOTH');
        $this->db->where('line_id', $line_id);
        $this->db->where('remark', 1);
		$this->db->where('factory_id', $factory);
		$this->db->distinct();
		$this->db->select('inline_header_id,poreference,style,factory_id,startdateactual');
		$query 	= $this->db->get('listpo_folding');
    
        return $query->num_rows();
    }
	// end data table serverside
	public function package_barcode($barcode_id)
	{
		$this->db->where('barcode_id', $barcode_id);
		return $this->db->get('folding_package_barcode');
	}
	public function getLineFg()
	{
		$this->fg->where('factory_id',$this->session->userdata('factory'));
		$this->fg->where('deleted_at is NULL');
		$this->fg->where('name', $this->session->userdata('line_id'));
		$query = $this->fg->get('line');
		if ($query->num_rows()) {
			return $query->row_array();
		}else{
			return 0;
		}
	}
	
	public function getListBarcodePackage($poreference,$size)
	{
		$query = "SELECT
					fp.scan_id,
					fp.poreference,
					fp.size,
					fpb.barcode_id
				FROM
					folding_package fp
				LEFT JOIN folding_package_barcode fpb on fpb.scan_id=fp.scan_id
				where fp.poreference='$poreference' and fp.size='$size' and fpb.status='onprogress'
				ORDER BY barcode_id";
		return $this->db->query($query)->result();
	}
	public function getBarcodeGarment($style,$poreference,$size,$article)
	{
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where('article', trim($article));
		$this->db->where('size', trim($size));
		$this->db->where('poreference', trim($poreference));
		$this->db->where('style', trim($style));
		return $this->db->get('barcode_garment');
	}
	public function totalOutputFolding($date)
	{
		$line_id = $this->session->userdata('line_id');
		$factory_id = $this->session->userdata('factory');

		// $this->db->where('factory_id', $factory_id);
		// $this->db->where('date', $date);
		// $this->db->where('line_id', $line_id);
		// $this->db->select_sum('daily_output');
		// return $this->db->get('folding_outputday');
		
		$this->db->where('factory_id', $factory_id);
		$this->db->where('activity_name', 'folding');
		$this->db->where('date(create_date)', $date);
		$this->db->where('line_id', $line_id);
		$this->db->select_sum('output');
		return $this->db->get('line_summary_po');
	}
	public function totalCompleteCarton($date)
	{
		$line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');
		$sql = "SELECT DISTINCT
					fb.barcode_id,
					ih.line_id
				FROM
					folding_package_barcode fb
				LEFT JOIN folding f ON f.barcode_package = fb.barcode_id
				LEFT JOIN inline_header ih ON ih.inline_header_id = f.inline_header_id
				WHERE
					DATE (complete_at) = '$date'
					and ih.line_id=$line_id
					and f.factory_id=$factory
				ORDER BY
					line_id";
		return $this->db->query($sql);
	}
	// data table serverside outputfolding
	function allposts_count_outputfolding($date=NULL)
    {
        $line_id = $this->session->userdata('line_id');
		$factory_id = $this->session->userdata('factory');
		if ($date!=NULL) {
			$this->db->where('date',$date);
		}else{
			$date =date('Y-m-d');
			$this->db->where('date',$date);
		}

		$this->db->where('factory_id', $factory_id);
		$this->db->where('line_id', $line_id);
        
        $query = $this->db->get('folding_outputday');

        return $query->num_rows();
    }
    
    function allposts_outputfolding($limit,$start,$col,$dir)
    {
        $line_id = $this->session->userdata('line_id');
		$factory_id = $this->session->userdata('factory');
		$date =date('Y-m-d');
		

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
       	$this->db->where('factory_id', $factory_id);
		$this->db->where('date', $date);
		$this->db->where('line_id', $line_id);
        $query = $this->db->get('folding_outputday');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_outputfolding($limit,$start,$search=NULL,$col,$dir,$date)
    {

        $line_id = $this->session->userdata('line_id');
		$factory_id = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        if ($search!=NULL) {
        	$this->db->group_start();
	        $this->db->like('poreference',strtoupper($search), 'BOTH');
	        $this->db->or_like('size', $search, 'BOTH');
	        $this->db->group_end();
        }
        $this->db->where('factory_id', $factory_id);
		$this->db->where('date', $date);
		$this->db->where('line_id', $line_id);
		
        $query = $this->db->get('folding_outputday');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_outputfolding($search=NULL,$date)
    {
        $line_id = $this->session->userdata('line_id');
		$factory_id = $this->session->userdata('factory');

		if ($search!=NULL) {
        	$this->db->group_start();
	        $this->db->like('poreference',strtoupper($search), 'BOTH');
	        $this->db->or_like('size', $search, 'BOTH');
	        $this->db->group_end();
        }
		$this->db->where('line_id', $line_id);
		$this->db->where('factory_id', $factory_id);
		$this->db->where('date', $date);
        $query = $this->db->get('folding_outputday');

        return $query->num_rows();
    }
	// end data table outputfolding
    public function getBarcodePackage($barcode_id)
    {
    	$this->db->where('barcode_id', $barcode_id);
    	return $this->db->get('folding_package_barcode');
    }
    public function cekHeader($poreference,$size,$article)
	{
		$line_id = $this->session->userdata('line_id');

		$this->db->where('inline_header.article', $article);
		$this->db->where_in('inline_header_size.size',$size);
		$this->db->where(array('inline_header.poreference'=>trim($poreference),'inline_header.line_id'=>$line_id));
		$this->db->join('inline_header_size', 'inline_header_size.inline_header_id = inline_header.inline_header_id', 'left');
		$this->db->select('inline_header.*, inline_header_size.size, inline_header_size.header_size_id');
		return $this->db->get('inline_header');
	}
	public function getPackage($barcode_id)
	{
		$sql = "SELECT
					pc.barcode_id,
					pc.scan_id,
					pd.inner_pack,
					pc.current_department,
					pc.current_status,
					pd.po_number,
					pd.item_qty,
					manufacturing_size,buyer_item,
					pkg_count,
					pd.rasio
				FROM
					package pc
				LEFT JOIN package_detail pd ON pd.scan_id = pc.scan_id
				WHERE
					barcode_id = '$barcode_id'";
		return $this->fg->query($sql);
	}
	public function CartonSend($line_header,$poreference,$size,$style,$date=NULL)
    {
    	$this->db->where('inline_header_id', $line_header);
    	$this->db->where('poreference', trim($poreference));
    	$this->db->where('size', trim($size));
    	$this->db->where('style', $style);
    	$this->db->select('barcode_package');
    	$this->db->distinct();
        $barcode = $this->db->get('folding')->result();
        foreach ($barcode as $key => $barcode) {
        	$barcode_package[]=$barcode->barcode_package;
        }
        if ($date!=NULL) {
    		$this->db->where('to_char(complete_at,\'YYYY-mm-dd\')',$date);
    	}
        $this->db->where('status', 'completed');
        $this->db->where_in('barcode_id', $barcode_package);
        return $this->db->get('folding_package_barcode')->num_rows();
    }
    public function countCarton($line_header,$poreference,$size,$style)
    {
    	
    	$this->db->where('inline_header_id', $line_header);
    	$this->db->where('poreference', $poreference);
    	$this->db->where('size', $size);
    	$this->db->where('style', $style);
    	$this->db->select('scan_id');
    	$this->db->distinct();
        $scan = $this->db->get('folding')->result();
        foreach ($scan as $key => $scan) {
        	$scan_id[]=$scan->scan_id;
        }

        $this->db->where_in('scan_id', $scan_id);
        $this->db->select('sum(pkg_count) as pkg_count');
        return $this->db->get('folding_package')->row_array()['pkg_count'];
    }
    public function getStyle($poreference='',$size='')
    {
    	$this->db->group_by('poreference,size,style');
    	$this->db->where('poreference', $poreference);
    	$this->db->where('size', $size);
    	$this->db->select('poreference,size,style');
    	return $this->db->get('folding_header');
    }
    public function wipStyleSets($poreference='',$size='',$style='')
    {

    	$this->db->group_by('poreference,size,style');
    	$this->db->where(array('poreference'=>trim($poreference),'size'=>trim($size),'style'=>$style));
    	$this->db->select('sum(counter_sets) as counter_sets,poreference,size,style');
    	$query = $this->db->get('folding_header')->row_array()['counter_sets'];
    	if ($query!=NULL) {
    		return $query;
    	}else{
    		return 0;
    	}
    }
    public function getOrder($poreference='',$size='',$style='')
    {
    	$this->db->where(array('poreference'=>trim($poreference),'size'=>trim($size),'style'=>trim($style)));
    	return $this->db->get('master_order');
    }
    public function getMinSets($poreference='',$size='')
    {
    	$this->db->group_by('style,poreference,size');
    	$this->db->where(array('poreference'=>trim($poreference),'size'=>trim($size),'status_folding'=>'unallocation'));
    	$this->db->select('style,poreference,size');
    	return $this->db->get('folding');
    }
    public function getStatusPO()
    {

    	return $this->db->get('statuspo_folding');
    }
    public function GetHeader($inline_header_id,$size,$style)
    {
    	$this->db->where('inline_header_id', $inline_header_id);
    	$this->db->where('size', $size);
    	$this->db->where('style', $style);
    	return $this->db->get('folding_header');
    }
    public function getCounterBarcode()
    {
    	return $this->db->query("SELECT
							fo.poreference,
							barcode_package,
							remark,fp.inner_pack,
						CASE
						when remark='SETS' then COUNT (barcode_package)/2
						when remark='PCS' then COUNT (barcode_package)
						end as total
						FROM
							folding fo
						LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
						WHERE
							status_folding = 'allocation' 
						GROUP BY
							fo.poreference,
							barcode_package,
							remark,fp.inner_pack");
    }
    public function counterScanRasio($barcode_package,$poreference,$size)
    {
    	$this->db->where(
    		array(
    			'barcode_package'=>$barcode_package,
    			'poreference'=>$poreference,
    			'size'=>$size
    		)
    	);
    	$this->db->select('count(*)as jumlah');
    	return $this->db->get('folding')->row_array()['jumlah'];
    }
     public function counterScanRasioSets($barcode_package,$poreference,$size)
    {
    	$this->db->where(
    		array(
    			'barcode_package'=>$barcode_package,
    			'poreference'=>$poreference,
    			'size'=>$size
    		)
    	);
    	$this->db->select('count(*)as jumlah');
    	return $this->db->get('folding')->row_array()['jumlah'];
    }
    public function getCounterSets($value='')
    {
    	$query = "SELECT
					fo.inline_header_id,
					ih.line_id,
					fo.factory_id,
					fo.style,
					fo.poreference,
					fo.size,
					count(*)

					FROM
						folding fo
					LEFT JOIN inline_header ih on ih.inline_header_id=fo.inline_header_id
					WHERE
						status_folding = 'unallocation'
					GROUP BY 
					fo.inline_header_id,
					ih.line_id,
					fo.factory_id,
					fo.style,
					fo.poreference,
					fo.size";
		return $this->db->query($query);
    }
    public function getCounterSetsBeda()
    {
    	return $this->db->query("select * from get_count_sets where total_detail<>total_header");

    }
    public function getCounterBeda()
    {
    	$query ="SELECT 
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,COALESCE(counter.count,0) as counter_alocation,fh.counter
					 from folding_header fh
					LEFT JOIN (
					select inline_header_id,style,poreference,size,count(*) from folding
					where status_folding='allocation' 
					GROUP BY 
					inline_header_id,style,poreference,size
					) counter on counter.inline_header_id=fh.inline_header_id and counter.size=fh.size
					GROUP BY
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,counter.count,fh.counter
					HAVING COALESCE(counter.count,0)<>fh.counter";
		return $this->db->query($query);
    }
    public function getCounterBedasets()
    {
    	$query ="SELECT 
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,COALESCE(counter.count,0) as counter_alocation,fh.counter_sets
					 from folding_header fh
					LEFT JOIN (
					select inline_header_id,style,poreference,size,count(*) from folding
					where status_folding='unallocation' 
					GROUP BY 
					inline_header_id,style,poreference,size
					) counter on counter.inline_header_id=fh.inline_header_id and counter.size=fh.size

					GROUP BY
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,counter.count,fh.counter_sets
					HAVING COALESCE(counter.count,0)<>fh.counter_sets";
		return $this->db->query($query);
	}
	
	public function cekValidasi($po, $size, $style, $header)
	{
		// $query = $this->db->query("SELECT * FROM validasi_folding where poreference = '$po' and size = '$size'");

		// return $query->result();

		
    	$this->db->where('poreference', $po);
    	$this->db->where('size', $size);
		$this->db->where('style', $style);
		$this->db->where('inline_header_id', $header);
    	return $this->db->get('validasi_folding');
	}

	public function cekKarton($scan_id, $barcode_id)
	{
		$this->db->where('folding_package_barcode.scan_id', $scan_id);
		$this->db->where('folding_package_barcode.barcode_id', $barcode_id);
		$this->db->join('folding_package', 'folding_package.scan_id = folding_package_barcode.scan_id', 'left');
		// $this->db->select('folding_package_barcode.*, folding_package.inner_pack');
    	return $this->db->get('folding_package_barcode')->row_array();
	}
	public function counterScanSets($barcode_package,$poreference)
    {
    	$this->db->where(
    		array(
    			'barcode_package'=>$barcode_package,
    			'poreference'=>$poreference
    		)
    	);
    	$this->db->select('count(*)as jumlah');
    	return $this->db->get('folding')->row_array()['jumlah'];
	}
	
	public function list_size($barcode_package,$poreference)
    {
    	return $this->db->query("SELECT DISTINCT qo.size, COALESCE(f.total,0) as total from qc_output qo
		LEFT JOIN (SELECT barcode_package, poreference, size, count(size) as total from folding
		where barcode_package = '$barcode_package' GROUP BY barcode_package, poreference, size) f 
		on f.poreference = qo.poreference and f.size = qo.size
		WHERE qo.poreference = '$poreference'
		order by qo.size")->result();
	}

	public function cek_qc_output($header_id,$size_id)
	{
		$query = $this->db->query("SELECT * FROM qc_output WHERE 
		inline_header_id = '$header_id' and header_size_id = '$size_id'");

		return $query;
	}

	public function load_folding($header_id, $size_id)
	{
		$query = $this->db->query("SELECT
			qo.line_id,
			qo. STYLE,
			qo.poreference,
			qo. SIZE,
			qo.qty_order,
			qo.qc_output,
			(fh.counter+fh.counter_sets) as folding_output
		FROM
			qc_output qo
		LEFT JOIN folding_header fh on fh.line_id= qo.line_id and fh.style=qo.style
		and fh.poreference=qo.poreference and fh.size=qo.size
		WHERE
			qo.inline_header_id = '$header_id'
			and qo.size = '$size_id'");

		return $query;
	}

	function output_folding_detail_count($poreference,$size)
    {   
		$line_id = $this->session->userdata('line_id');
		// if(!empty($search)){
		// 	$query = $this->db->query("SELECT count(*) as total,fp.inner_pack,fo.poreference,style,fo.size,
		// 	barcode_package, fpb.line_id, ml.no_urut, ml.line_name,date(fpb.create_date) as create_on, date(fpb.complete_at) as complete_on from folding fo
		// 	LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
		// 	LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
		// 	LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
		// 	where fo.poreference='$poreference' and fo.size = '$size' and fpb.status = 'completed' and fpb.line_id = '$line_id'
		// 	and fo.barcode_package like '%$search%'
		// 	GROUP BY fp.inner_pack,fo.poreference, fo.size,style, barcode_package, fpb.line_id, ml.no_urut,create_on, complete_on, ml.line_name
		// 	ORDER BY size, barcode_package;");
		// }
        // else{
			$query = $this->db->query("SELECT count(*) as total,fp.inner_pack,fo.poreference,style,fo.size,
			barcode_package, fpb.line_id, ml.no_urut, ml.line_name,date(fpb.create_date) as create_on, date(fpb.complete_at) as complete_on from folding fo
			LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
			LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
			LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
			where fo.poreference='$poreference' and fo.size = '$size' and fpb.status = 'completed' and fpb.line_id = '$line_id'
			GROUP BY fp.inner_pack,fo.poreference, fo.size,style, barcode_package, fpb.line_id, ml.no_urut,create_on, complete_on, ml.line_name
			ORDER BY size, barcode_package;");
		// }
    
        return $query->num_rows();  

    }
    
    function output_folding_detail_all($limit,$start,$col,$dir,$poreference,$size)
    {   
		$line_id = $this->session->userdata('line_id');
		// if(!empty($search)){
		// 	$query = $this->db->query("SELECT count(*) as total,fp.inner_pack,fo.poreference,style,fo.size,
		// 	barcode_package, fpb.line_id, ml.no_urut, ml.line_name,date(fpb.create_date) as create_on, date(fpb.complete_at) as complete_on from folding fo
		// 	LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
		// 	LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
		// 	LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
		// 	where fo.poreference='$poreference' and fo.size = '$size' and fpb.status = 'completed' and fpb.line_id = '$line_id'
		// 	and fo.barcode_package like '%$search%'
		// 	GROUP BY fp.inner_pack,fo.poreference, fo.size,style, barcode_package, fpb.line_id, ml.no_urut,create_on, complete_on, ml.line_name
		// 	-- ORDER BY size, barcode_package
		// 	order by $col $dir
		// 	limit $limit offset $start");
		// }
		// else{
			$query = $this->db->query("SELECT count(*) as total,fp.inner_pack,fo.poreference,style,fo.size,
			barcode_package, fpb.line_id, ml.no_urut, ml.line_name,date(fpb.create_date) as create_on, date(fpb.complete_at) as complete_on from folding fo
			LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
			LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
			LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
			where fo.poreference='$poreference' and fo.size = '$size' and fpb.status = 'completed' and fpb.line_id = '$line_id'
			GROUP BY fp.inner_pack,fo.poreference, fo.size,style, barcode_package, fpb.line_id, ml.no_urut,create_on, complete_on, ml.line_name
			order by $col $dir
			limit $limit offset $start");
		// }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
	}

	function output_karton_count($poreference)
	{
		
		$line_id = $this->session->userdata('line_id');
		
		if(!empty($poreference)){
			$query = $this->db->query("SELECT count(*) as total, count(distinct barcode_package)as total_karton, fo.poreference,style,fo.size from folding fo
				LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
				LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
				LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
				where fo.poreference like '%$poreference%' and fpb.status = 'completed' and fpb.line_id = '$line_id'
				GROUP BY fo.poreference, fo.size,style, ml.no_urut
				ORDER BY poreference, size");
			
			return $query->num_rows(); 
		}
		else{
			return 0;
		}
	}
	
	function output_karton_all($limit,$start,$col,$dir,$poreference)
	{
		
		$line_id = $this->session->userdata('line_id');
		if(!empty($poreference)){
			$query = $this->db->query("SELECT count(*) as total, count(distinct barcode_package)as total_karton, fo.poreference,style,fo.size from folding fo
			LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
			LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
			LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
			where fo.poreference like '%$poreference%' and fpb.status = 'completed' and fpb.line_id = '$line_id'
			GROUP BY fo.poreference, fo.size,style, ml.no_urut
			ORDER BY poreference, size
			limit $limit offset $start");
			
			if($query->num_rows()>0)
			{
				return $query->result(); 
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
		
		
		
	}
	
	function output_karton_no_ajax($poreference)
	{
		
		$line_id = $this->session->userdata('line_id');
		if(!empty($poreference)){
			$query = $this->db->query("SELECT count(*) as total, count(distinct barcode_package)as total_karton, fo.poreference,style,fo.size from folding fo
			LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
			LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
			LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
			where fo.poreference like '%$poreference%' and fpb.status = 'completed' and fpb.line_id = '$line_id'
			GROUP BY fo.poreference, fo.size,style, ml.no_urut
			ORDER BY poreference, size");
			
			if($query->num_rows()>0)
			{
				return $query->result(); 
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
		
		
		
	}

	function detail_karton_no_ajax($poreference,$size)
	{
		
		$line_id = $this->session->userdata('line_id');
		if(!empty($poreference)){
			$query = $this->db->query("SELECT date(complete_at) as complete_at, count(*) as total, count(distinct barcode_package)as total_karton, fo.poreference,style,fo.size from folding fo
			LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
			LEFT JOIN folding_package_barcode fpb on fpb.scan_id = fo.scan_id and fpb.barcode_id = fo.barcode_package
			LEFT JOIN master_line ml on ml.master_line_id = fpb.line_id
			where fo.poreference = '$poreference' and fo.size = '$size'
			 and fpb.status = 'completed' and fpb.line_id = '$line_id'
			GROUP BY date(complete_at), fo.poreference, fo.size,style, ml.no_urut
			ORDER BY date(complete_at), poreference, size");
			
			if($query->num_rows()>0)
			{
				return $query->result(); 
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
		
		
		
	}

	function cek_master_order($style)
	{
		$this->db->limit(1);
		$this->db->where('value !=', null);
		$this->db->where('style', $style);
		$query = $this->db->get('master_order');
		if ($query->num_rows()>0) {
			return $query->row_array();
		}else{
			return 0;
		}

	}
	
}

/* End of file FoldingModel.php */
/* Location: ./application/models/FoldingModel.php */
