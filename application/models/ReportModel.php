<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportModel extends CI_Model {

	public function get_all($limit, $start = 0, $date = NULL,$factory)
	{
		$query = $this
                ->db
                ->limit($limit,$start)
                ->order_by('date')
                ->where('date',$date)
                // ->where('factory_id',$factory)
                ->get('performance_report');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
	}
	public function total_rows($tanggal,$factory)
	{
		$query = $this
                ->db
                ->where('date',$tanggal)
                // ->where('factory_id',$factory)
                ->get('performance_report');

        return $query->num_rows();
	}

	function get_limit_data($limit, $start = 0, $tanggal = NULL,$factory)
    {
        $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by('date')
                ->where('date',$tanggal)
                // ->where('factory_id',$factory)
                ->get('performance_report');
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    public function download($date,$factory,$line_id=NULL,$style=NULL)
    {
                $this->db->order_by('line_id', 'asc');
                $this->db->where('date', $date);
                // $this->db->where('factory_id', $factory);
        $query=  $this->db->get('performance_report');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    function downloadLinePerformance($date, $nik_sewer, $round, $color, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $query = $this->db->query("select * from f_performance_report_by_date('$date',$factory)");
        }else if ($filter == 'by_nik') {
            $query = $this->db->query("select * from f_performance_report_by_nik('$date',$factory,'$nik_sewer')");
        }else if ($filter == 'by_grade') {
            $query = $this->db->query("select * from f_performance_report_by_grade('$date',$factory,$round,'$color')");
        }


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }



    //
    function downloadMeasurement($dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $query = $this->db->query("select * from f_summary_inline_ranged('$dari', '$sampai',$factory) order by date,no_urut,master_proses_id");
        }else if ($filter == 'by_line') {
            $query = $this->db->query("select * from f_cap_inline_beetwen_ranged('$dari', '$sampai',$line_from, $line_to, $factory) order by date,no_urut,master_proses_id");
        }


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    //
    function downloadMeasurementEndline($dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id',$factory);
            $query = $this->db->get("adt_cap_measurement");
        }else if ($filter == 'by_line') {
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id',$factory);
            $this->db->where('line_id >=',$line_from);
            $this->db->where('line_id <=',$line_to);
            $query = $this->db->get("adt_cap_measurement");
        }


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    public function total_inspect($tanggal,$factory,$line_id,$style)
    {
        $this->db->where('to_char(create_date,\'YYYY-mm-dd\')', $tanggal);
        $this->db->where('factory_id', $factory);
        $this->db->where('line_id', $line_id);
        $this->db->where('style', $style);
        $query = $this->db->get('inlinedetail_view');
        return $query->num_rows();
    }
    public function reportdefect($date,$factory)
    {
        $query = $this
                ->db
                ->order_by('line_id')
                ->group_by('line_id,line_name,style')
                ->where('create_date',$date)
                ->where('factory_id',$factory)
                ->select('line_id,line_name,style')
                ->get('listinspection_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    public function total_defect($tanggal,$factory,$line_id,$style)
    {
        $this->db->where('to_char(create_date,\'YYYY-mm-dd\')', $tanggal);
        $this->db->where('factory_id', $factory);
        $this->db->where('defect_id<>', 0);
        $this->db->where('line_id', $line_id);
        $this->db->where('style', $style);
        $query = $this->db->get('inlinedetail_view');
        return $query->num_rows();
    }

    // report daily inspection
    function allposts_count_reportinspect($date, $nik_sewer, $color, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $query = $this->db->query("select * from f_performance_report_by_date('$date',$factory)");
        }else if ($filter == 'by_nik') {
            $query = $this->db->query("select * from f_performance_report_by_date('$date',$factory) where sewer_nik='$nik_sewer'");
        }else if ($filter == 'by_grade') {
            $query = $this->db->query("select * from f_performance_report_by_grade('$date',$factory,'$color')");
        }
        return $query->num_rows();
    }

    function allposts_reportinspect($limit,$start,$col,$dir,$date, $nik_sewer, $color, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $query = $this->db->query("select * from f_performance_report_by_date('$date',$factory) order by $col $dir limit $limit offset $start");
        }elseif ($filter == 'by_nik') {
             $query = $this->db->query("select * from f_performance_report_by_nik('$date',$factory,'$nik_sewer') order by $col $dir limit $limit offset $start");
        }elseif ($filter == 'by_grade') {
            $query = $this->db->query("select * from f_performance_report_by_grade('$date',$factory,'$color') order by $col $dir limit $limit offset $start");
        }

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_reportinspect($limit,$start,$search,$col,$dir,$date, $nik_sewer, $color, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $this->db->limit($limit,$start);
            $this->db->order_by($col,$dir);
            $this->db->like('line_name',strtoupper($search), 'BOTH');
            // $this->db->where('factory_id',$factory);
            $query = $this->db->get("f_performance_report_by_date('$date',$factory)");
        }elseif ($filter == 'by_nik') {
            $this->db->limit($limit,$start);
            $this->db->order_by($col,$dir);
            $this->db->like('line_name',strtoupper($search), 'BOTH');
            // $this->db->where('sewer_nik',$nik_sewer);
            // $this->db->where('factory_id',$factory);
            $query = $this->db->get("f_performance_report_by_nik('$date',$factory,'$nik_sewer')");
        }elseif ($filter == 'by_grade') {
            $this->db->limit($limit,$start);
            $this->db->order_by($col,$dir);
            $this->db->like('line_name',strtoupper($search), 'BOTH');
            // $this->db->where('grade_round'.$round,$color);
            // $this->db->where('factory_id',$factory);
            $query = $this->db->get("f_performance_report_by_grade('$date',$factory,'$color')");
        }

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_reportinspect($search,$date, $nik_sewer, $color, $filter, $factory)
    {
        if ($filter == 'by_date') {
            // $this->db->where('date',$date);
            // $this->db->where('factory_id',$factory);
            // $query = $this->db->get('performance_report');
            $query = $this->db->get("f_performance_report_by_date('$date',$factory)");
        }elseif ($filter == 'by_nik') {
            // $this->db->where('sewer_nik',$nik_sewer);
            // $this->db->where('factory_id',$factory);
            // $query = $this->db->get('performance_report');
            $query = $this->db->get("f_performance_report_by_nik('$date',$factory,'$nik_sewer')");
        }elseif ($filter == 'by_grade') {
            // $this->db->where('grade_round'.$round,$color);
            // $this->db->where('factory_id',$factory);
            // $query = $this->db->get('performance_report');
            $query = $this->db->get("f_performance_report_by_grade('$date',$factory,'$color')");
        }

        return $query->num_rows();
    }
    // end report daily inspection
    //

     /*download operator performance without proses lama*/
     function download_ops_perform($datefrom=0,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from)
    {
        $date = ($dateto!=0?$dateto:date('Y-m-d'));
        
        $sql = "SELECT iop.date, iop.nik_sewer, iop.line_id, iop.nama_proses, ms.name, ml.line_name,
        iop.total_score, iop.total_avg, iop.grade, iop.factory_id, iop.jenis_defect FROM inline_operator_performance iop 
        LEFT JOIN master_sewer ms on ms.nik = iop.nik_sewer
        LEFT JOIN master_line ml on ml.master_line_id = iop.line_id	
        WHERE ";

        if ($filter == 'by_date') {
            $sql .= "date >= '".$datefrom."'and
            date <= '".$dateto."'
            and iop.factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {
            $sql .= "date = '".$date."'
            and iop.factory_id = '".$factory."'
            AND nik_sewer = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            $sql .= "date = '".$date."'
            and iop.factory_id = '".$factory."'
            AND grade = '".$grade."'";
        }else if ($filter == 'by_line') {
            $sql .= "date = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= '
        GROUP BY
        iop.date,iop.nik_sewer,ms.name,iop.line_id,ml.line_name,iop.nama_proses, iop.total_score, iop.total_avg, iop.grade, iop.factory_id, iop.jenis_defect';

        // $query = $this->db->get('adt_opt_performance_sum');
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
     /*download operator performance*/
    function downloadOperatorPerformance($datefrom=0,$dateto, $nik_sewer, $grade, $filter, $factory,$line_from,$line_to)
    {
        $sql = "SELECT
            asd.date,asd.sewer_nik,ms.name,asd.line_id,ml.line_name,asd.master_proses_id,asd.proses_name,sum(asd.point) as point,
            round((sum(asd.point)::float/count(asd.round)::float)) as total, 
            CASE
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 4 THEN
                'A'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                'B'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                'C'
            END AS grade,
            CASE
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 5 THEN
                    0
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                    2
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                    5
            END AS point_defect
        FROM
            (
                SELECT DISTINCT
                    sewer_nik,
                    date(create_date) as date,
                    proses_name,
                    line_id,
                    round,
                    master_proses_id,
                    CASE
                WHEN master_grade_id = 1 THEN
                    5
                WHEN master_grade_id = 2 THEN
                    2
                WHEN master_grade_id = 3 THEN
                    0
                END AS point
                FROM
                inlinedetail_view
                WHERE ";

        if ($filter == 'by_date') {
            // $this->db->where('date BETWEEN \''.$datefrom. '\' and \''.$dateto.'\'');
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) >= '".$datefrom."'and
            DATE (create_date) <= '".$dateto."'
            and factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {            
            // $this->db->where('date',$date);
            // $this->db->where('sewer_nik',$nik_sewer);
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND sewer_nik = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            // $this->db->where('date',$date);
            // $this->db->where('grade',$grade);
            // $this->db->where('factory_id',$factory);
            
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND grade = '".$grade."'";

        }else if ($filter == 'by_line') {
            // $this->db->where('date',$date);
            // $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');

            
            $sql .= "DATE (create_date) = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= ') asd
        LEFT JOIN master_sewer ms on ms.nik = asd.sewer_nik
        LEFT JOIN master_line ml on ml.master_line_id = asd.line_id	
        GROUP BY
        asd.date,asd.sewer_nik,ms.name,	asd.line_id,ml.line_name,asd.proses_name,asd.master_proses_id';

        $query = $this->db->query($sql);

        return $query->result();
        
    }
    // report operator performance without proses
    function allpost_count_operator($datefrom=0,$dateto=0, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from)
    {
        $date = ($dateto!=0?$dateto:date('Y-m-d'));

        $sql = "SELECT iop.date, iop.nik_sewer, iop.line_id, iop.nama_proses, ms.name, ml.line_name, 
        iop.total_score, iop.total_avg, iop.grade, iop.factory_id, iop.jenis_defect FROM inline_operator_performance iop 
        LEFT JOIN master_sewer ms on ms.nik = iop.nik_sewer
        LEFT JOIN master_line ml on ml.master_line_id = iop.line_id	WHERE ";

        if ($filter == 'by_date') {
            $sql .= "date >= '".$datefrom."'and
            date <= '".$dateto."'
            and iop.factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {
            $sql .= "date = '".$date."'
            and iop.factory_id = '".$factory."'
            AND nik_sewer = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            $sql .= "date = '".$date."'
            and iop.factory_id = '".$factory."'
            AND grade = '".$grade."'";
        }else if ($filter == 'by_line') {
            $sql .= "date = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= '
        GROUP BY
        iop.date,iop.nik_sewer,ms.name,iop.line_id,ml.line_name,iop.nama_proses, iop.total_score, iop.total_avg, iop.grade, iop.factory_id, iop.jenis_defect';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }   

    function allpost_result_operator($limit,$start,$col,$dir,$datefrom=0,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from)
    {
        $date = ($dateto!=0?$dateto:date('Y-m-d'));
        
        $sql = "SELECT iop.date, iop.nik_sewer, iop.line_id, iop.nama_proses, ms.name, ml.line_name,
        iop.total_score, iop.total_avg, iop.grade, iop.factory_id, iop.jenis_defect FROM inline_operator_performance iop 
        LEFT JOIN master_sewer ms on ms.nik = iop.nik_sewer
        LEFT JOIN master_line ml on ml.master_line_id = iop.line_id	
        WHERE ";

        if ($filter == 'by_date') {
            $sql .= "date >= '".$datefrom."'and
            date <= '".$dateto."'
            and iop.factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {
            $sql .= "date = '".$date."'
            and iop.factory_id = '".$factory."'
            AND nik_sewer = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            $sql .= "date = '".$date."'
            and iop.factory_id = '".$factory."'
            AND grade = '".$grade."'";
        }else if ($filter == 'by_line') {
            $sql .= "date = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= '
        GROUP BY
        iop.date,iop.nik_sewer,ms.name,iop.line_id,ml.line_name,iop.nama_proses, iop.total_score, iop.total_avg, iop.grade, iop.factory_id, iop.jenis_defect
        order by '.$col.' '.$dir.' limit '.$limit.' offset '.$start.'';

        // $query = $this->db->get('adt_opt_performance_sum');
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    // report operator performance
    function allposts_count_reportinspect_operator($datefrom=0,$dateto=0, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from)
    {
        $date = ($dateto!=0?$dateto:date('Y-m-d'));

        $sql = "SELECT
            asd.date,asd.sewer_nik,ms.name,asd.line_id,ml.line_name,asd.master_proses_id,asd.proses_name,sum(asd.point) as point,
            round((sum(asd.point)::float/count(asd.round)::float)) as total, 
            CASE
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 4 THEN
                'A'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                'B'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                'C'
            END AS grade,
            CASE
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 5 THEN
                    0
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                    2
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                    5
            END AS point_defect
        FROM
            (
                SELECT DISTINCT
                    sewer_nik,
                    date(create_date) as date,
                    proses_name,
                    line_id,
                    round,
                    master_proses_id,
                    CASE
                WHEN master_grade_id = 1 THEN
                    5
                WHEN master_grade_id = 2 THEN
                    2
                WHEN master_grade_id = 3 THEN
                    0
                END AS point
                FROM
                inlinedetail_view
                WHERE ";

        if ($filter == 'by_date') {
            // $this->db->where('date BETWEEN \''.$datefrom. '\' and \''.$dateto.'\'');
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) >= '".$datefrom."'and
            DATE (create_date) <= '".$dateto."'
            and factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {            
            // $this->db->where('date',$date);
            // $this->db->where('sewer_nik',$nik_sewer);
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND sewer_nik = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            // $this->db->where('date',$date);
            // $this->db->where('grade',$grade);
            // $this->db->where('factory_id',$factory);
            
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND grade = '".$grade."'";

        }else if ($filter == 'by_line') {
            // $this->db->where('date',$date);
            // $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');

            
            $sql .= "DATE (create_date) = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= ') asd
        LEFT JOIN master_sewer ms on ms.nik = asd.sewer_nik
        LEFT JOIN master_line ml on ml.master_line_id = asd.line_id	
        GROUP BY
        asd.date,asd.sewer_nik,ms.name,	asd.line_id,ml.line_name,asd.proses_name,asd.master_proses_id';

        // $query = $this->db->get('adt_opt_performance_sum');
        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    function allposts_reportinspect_operator($limit,$start,$col,$dir,$datefrom=0,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from)
    {
        $date = ($dateto!=0?$dateto:date('Y-m-d'));
        

        // $this->db->limit($limit,$start);
        // $this->db->order_by($col,$dir);
       
        $sql = "SELECT
            asd.date,asd.sewer_nik,ms.name,asd.line_id,ml.line_name,asd.master_proses_id,asd.proses_name,sum(asd.point) as point,
            round((sum(asd.point)::float/count(asd.round)::float)) as total, 
            CASE
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 4 THEN
                'A'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                'B'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                'C'
            END AS grade,
            CASE
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 5 THEN
                    0
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                    2
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                    5
            END AS point_defect
        FROM
            (
                SELECT DISTINCT
                    sewer_nik,
                    date(create_date) as date,
                    proses_name,
                    line_id,
                    round,
                    master_proses_id,
                    CASE
                WHEN master_grade_id = 1 THEN
                    5
                WHEN master_grade_id = 2 THEN
                    2
                WHEN master_grade_id = 3 THEN
                    0
                END AS point
                FROM
                inlinedetail_view
                WHERE ";

        if ($filter == 'by_date') {
            // $this->db->where('date BETWEEN \''.$datefrom. '\' and \''.$dateto.'\'');
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) >= '".$datefrom."'and
            DATE (create_date) <= '".$dateto."'
            and factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {            
            // $this->db->where('date',$date);
            // $this->db->where('sewer_nik',$nik_sewer);
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND sewer_nik = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            // $this->db->where('date',$date);
            // $this->db->where('grade',$grade);
            // $this->db->where('factory_id',$factory);
            
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND grade = '".$grade."'";

        }else if ($filter == 'by_line') {
            // $this->db->where('date',$date);
            // $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');

            
            $sql .= "DATE (create_date) = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= ') asd
        LEFT JOIN master_sewer ms on ms.nik = asd.sewer_nik
        LEFT JOIN master_line ml on ml.master_line_id = asd.line_id	
        GROUP BY
        asd.date,asd.sewer_nik,ms.name,	asd.line_id,ml.line_name,asd.proses_name,asd.master_proses_id
        order by '.$col.' '.$dir.' limit '.$limit.' offset '.$start.'';

        // $query = $this->db->get('adt_opt_performance_sum');
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_reportinspect_operator($limit,$start,$search,$col,$dir,$datefrom=0,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from)
    {
        $date = ($dateto!=0?$dateto:date('Y-m-d'));

        $sql = "SELECT
            asd.date,asd.sewer_nik,ms.name,asd.line_id,ml.line_name,asd.master_proses_id,asd.proses_name,sum(asd.point) as point,
            round((sum(asd.point)::float/count(asd.round)::float)) as total, 
            CASE
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 4 THEN
                'A'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                'B'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                'C'
            END AS grade,
            CASE
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 5 THEN
                    0
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                    2
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                    5
            END AS point_defect
        FROM
            (
                SELECT DISTINCT
                    sewer_nik,
                    date(create_date) as date,
                    proses_name,
                    line_id,
                    round,
                    master_proses_id,
                    CASE
                WHEN master_grade_id = 1 THEN
                    5
                WHEN master_grade_id = 2 THEN
                    2
                WHEN master_grade_id = 3 THEN
                    0
                END AS point
                FROM
                inlinedetail_view
                WHERE ";

        if ($filter == 'by_date') {
            // $this->db->where('date BETWEEN \''.$datefrom. '\' and \''.$dateto.'\'');
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) >= '".$datefrom."'and
            DATE (create_date) <= '".$dateto."'
            and factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {            
            // $this->db->where('date',$date);
            // $this->db->where('sewer_nik',$nik_sewer);
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND sewer_nik = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            // $this->db->where('date',$date);
            // $this->db->where('grade',$grade);
            // $this->db->where('factory_id',$factory);
            
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND grade = '".$grade."'";

        }else if ($filter == 'by_line') {
            // $this->db->where('date',$date);
            // $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');

            
            $sql .= "DATE (create_date) = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= ') asd
        LEFT JOIN master_sewer ms on ms.nik = asd.sewer_nik
        LEFT JOIN master_line ml on ml.master_line_id = asd.line_id	
        GROUP BY
        asd.date,asd.sewer_nik,ms.name,	asd.line_id,ml.line_name,asd.proses_name,asd.master_proses_id
        order by '.$col.' '.$dir.' limit '.$limit.' offset '.$start.'';

        // $query = $this->db->get('adt_opt_performance_sum');
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_reportinspect_operator($search,$datefrom=0,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from)
    {
        $date = ($dateto!=0?$dateto:date('Y-m-d'));

        $sql = "SELECT
            asd.date,asd.sewer_nik,ms.name,asd.line_id,ml.line_name,asd.master_proses_id,asd.proses_name,sum(asd.point) as point,
            round((sum(asd.point)::float/count(asd.round)::float)) as total, 
            CASE
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 4 THEN
                'A'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                'B'
            WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                'C'
            END AS grade,
            CASE
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 5 THEN
                    0
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) >= 3 and round((sum(asd.point)::float/count(asd.round)::float)) < 4 THEN
                    2
                WHEN round((sum(asd.point)::float/count(asd.round)::float)) < 3 THEN
                    5
            END AS point_defect
        FROM
            (
                SELECT DISTINCT
                    sewer_nik,
                    date(create_date) as date,
                    proses_name,
                    line_id,
                    round,
                    master_proses_id,
                    CASE
                WHEN master_grade_id = 1 THEN
                    5
                WHEN master_grade_id = 2 THEN
                    2
                WHEN master_grade_id = 3 THEN
                    0
                END AS point
                FROM
                inlinedetail_view
                WHERE ";

        if ($filter == 'by_date') {
            // $this->db->where('date BETWEEN \''.$datefrom. '\' and \''.$dateto.'\'');
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) >= '".$datefrom."'and
            DATE (create_date) <= '".$dateto."'
            and factory_id = '".$factory."'";
        }else if ($filter == 'by_nik') {            
            // $this->db->where('date',$date);
            // $this->db->where('sewer_nik',$nik_sewer);
            // $this->db->where('factory_id',$factory);
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND sewer_nik = '".$nik_sewer."'";
        }else if ($filter == 'by_grade') {
            // $this->db->where('date',$date);
            // $this->db->where('grade',$grade);
            // $this->db->where('factory_id',$factory);
            
            
            $sql .= "DATE (create_date) = '".$date."'
            and factory_id = '".$factory."'
            AND grade = '".$grade."'";

        }else if ($filter == 'by_line') {
            // $this->db->where('date',$date);
            // $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');

            
            $sql .= "DATE (create_date) = '".$date."'
            AND line_id BETWEEN '".$line_from."' and '".$line_to."'";
        }

        $sql .= ') asd
        LEFT JOIN master_sewer ms on ms.nik = asd.sewer_nik
        LEFT JOIN master_line ml on ml.master_line_id = asd.line_id	
        GROUP BY
        asd.date,asd.sewer_nik,ms.name,	asd.line_id,ml.line_name,asd.proses_name,asd.master_proses_id';

        // $query = $this->db->get('adt_opt_performance_sum');
        $query = $this->db->query($sql);


        return $query->num_rows();
    }
    // end report operator performance
    public function getDefect($sewer_nik,$tanggal,$id)
    {
        $factory_id = $this->session->userdata('factory');
        $query = "SELECT
                        defect_jenis,count(defect_jenis) as jumlah
                    FROM
                        inlinedetail_view
                    WHERE
                        defect_id <> 0
                    AND to_char(create_date, 'YYYY-mm-dd') = '$tanggal'
                    AND factory_id = '$factory_id'
                    AND sewer_nik = '$sewer_nik'
                    AND master_proses_id = $id
                    GROUP BY
                        defect_jenis";
        return $this->db->query($query);
        
    }
    //cron operator performance day
    public function getDefect_cron($sewer_nik,$tanggal,$id,$factory_id)
    {
        $query = "SELECT
                        defect_jenis,count(defect_jenis) as jumlah
                    FROM
                        inlinedetail_view
                    WHERE
                        defect_id <> 0
                    AND to_char(create_date, 'YYYY-mm-dd') = '$tanggal'
                    AND factory_id = '$factory_id'
                    AND sewer_nik = '$sewer_nik'
                    AND master_proses_id = $id
                    GROUP BY
                        defect_jenis";
        return $this->db->query($query);
        
    }
    // report tls
    function allposts_count_reportrft($date)
    {
        $factory = $this->session->userdata('factory');
        $this->db->where('factory_id',$factory);
        $this->db->where('create_date',$date);
        $query = $this->db->get('adt_qc_tls_v2');

        return $query->num_rows();
    }

    function allposts_reportrft($limit,$start,$col,$dir,$date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->where('factory_id',$factory);
        $this->db->where('create_date',$date);
        $query = $this->db->get('adt_qc_tls_v2');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_reportrft($limit,$start,$search,$col,$dir,$date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->like('line_name',strtoupper($search), 'BOTH');
        $this->db->or_like('poreference', $search, 'BOTH');
        $this->db->where('create_date',$date);
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('adt_qc_tls_v2');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_reportrft($search,$date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->like('line_name',strtoupper($search), 'BOTH');
        $this->db->or_like('poreference', $search, 'BOTH');
        $this->db->where('factory_id',$factory);
        $this->db->where('create_date',$date);
        $query = $this->db->get('adt_qc_tls_v2');

        return $query->num_rows();
    }
    // end report tls
    /*download rft by style*/
    public function downloadReportTls($date)
    {
        $factory = $this->session->userdata('factory');
        $this->db->where(array('create_date'=>$date,'factory_id'=>$factory));
        return $this->db->get('adt_qc_tls_v2');

    }

    public function downloadReportTls_ranged($dari,$sampai,$factory)
    {
        // $factory = $this->session->userdata('factory');
        
        $this->db->where('factory_id',$factory);
        $this->db->where('create_date >=',$dari);
        $this->db->where('create_date <=',$sampai);
        // $this->db->where(array('create_date'=>$date,'factory_id'=>$factory));
        return $this->db->get('adt_qc_tls_v2');

    }
    /*download rft by line*/
    public function downloadReportTlsLine($date,$round)
    {
        $factory = $this->session->userdata('factory');
        $this->db->where(array('create_date'=>$date,'factory_id'=>$factory,'round'=>$round));
        return $this->db->get('rft_round_inline');

    }
    public function total_defectEndline($date,$factory_id,$line_id)
    {
        $this->db->group_by('line_id,date');
        $this->db->where('factory_id',$factory_id);
        $this->db->where('date',$date);
        $this->db->where('line_id',$line_id);
        $this->db->select('sum(defect_endline) as defect_endline');
        $this->db->select('line_id,date');
        return $this->db->get('wft_endline')->result();
    }
    public function total_defectInline($date,$factory_id,$line_id)
    {
        $this->db->group_by('master_line_id,date,');
        $this->db->where('factory_id',$factory_id);
        $this->db->where('date',$date);
        $this->db->where('master_line_id',$line_id);
        $this->db->select('sum(defect_inline) as defect_inline');
        $this->db->select('master_line_id,date,');
        return $this->db->get('wft_inline')->result();
    }

    // report wft
    public function downloadwft($factory,$dari,$sampai)
    {
        $query = $this->db->query("SELECT lsp.date, lsp.line_id, lsp.line_name, lsp.factory_id, sum(lsp.output) as total_qc,
            di.total_inline, de.total_endline, ml.no_urut from line_summary_po lsp
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_inline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_inline' 
                GROUP BY date, line_id, line_name, factory_id) di on di.date=lsp.date 
                and di.factory_id = lsp.factory_id and di.line_id=lsp.line_id
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_endline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_endline' 
                GROUP BY date, line_id, line_name, factory_id) de on de.date=lsp.date 
                and de.factory_id = lsp.factory_id and de.line_id=lsp.line_id
            LEFT JOIN master_line ml on ml.master_line_id = lsp.line_id
            where lsp.date >= '$dari' and lsp.date <= '$sampai' and lsp.factory_id = '$factory' and lsp.activity_name = 'outputqc'
            GROUP BY lsp.line_id, lsp.line_name, lsp.factory_id, di.total_inline, de.total_endline, ml.no_urut, lsp.date
            ORDER BY lsp.date, ml.no_urut");
        
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function downloadwft_style($dari,$sampai)
    {
        $factory = $this->session->userdata('factory');
        // $query = $this->db->query("SELECT lsp.style, lsp.date, lsp.line_id, lsp.line_name, lsp.factory_id, sum(lsp.output) as total_qc,
        //     COALESCE(di.total_inline,0) as total_inline, COALESCE(de.total_endline,0) as total_endline, ml.no_urut from line_summary_po lsp
        //     LEFT JOIN (
        //         SELECT date, style, line_id, line_name, factory_id, sum(output) as total_inline from line_summary_po 
        //         where date >= '$dari' and date <= '$sampai'  and factory_id = '$factory' and activity_name = 'defect_inline' 
        //         GROUP BY date, style, line_id, line_name, factory_id) di on di.date=lsp.date 
        //         and di.factory_id = lsp.factory_id and di.line_id=lsp.line_id and di.style = lsp.style
        //     LEFT JOIN (
        //         SELECT date, style, line_id, line_name, factory_id, sum(output) as total_endline from line_summary_po 
        //         where date >= '$dari' and date <= '$sampai'  and factory_id = '$factory' and activity_name = 'defect_endline' 
        //         GROUP BY date, style, line_id, line_name, factory_id) de on de.date=lsp.date 
        //         and de.factory_id = lsp.factory_id and de.line_id=lsp.line_id and de.style = lsp.style
        //     LEFT JOIN master_line ml on ml.master_line_id = lsp.line_id
        //     where lsp.date >= '$dari' and lsp.date <= '$sampai' and lsp.factory_id = '$factory' and lsp.activity_name = 'outputqc'
        //     GROUP BY lsp.line_id, lsp.line_name, lsp.factory_id, di.total_inline, de.total_endline, ml.no_urut,
        //      lsp.date, lsp.style
        //     ORDER BY lsp.date, ml.no_urut");

        $query = $this->db->query("
            SELECT 
                lsp.style,
                lsp.date,
                lsp.line_id,
                ml.line_name,
                lsp.factory_id,
                COALESCE(qo.total_qc,0) as total_qc,
                COALESCE(di.total_inline,0) as total_inline,
                COALESCE(de.total_endline,0) as total_endline,
                ml.no_urut
            FROM
                line_summary_po lsp join master_line ml on lsp.line_id=ml.master_line_id
            LEFT JOIN (
                    
                            SELECT date, style, line_id, line_name, factory_id, sum(output) as total_inline from line_summary_po 
                            where date >= '$dari' and date <= '$sampai'  and factory_id = '2' and activity_name = 'defect_inline' 
                            GROUP BY date, style, line_id, line_name, factory_id
                ) di on di.date=lsp.date and di.factory_id = lsp.factory_id and di.line_id=lsp.line_id and di.style = lsp.style
            LEFT JOIN(
                    SELECT date, style, line_id, line_name, factory_id, sum(output) as total_endline from line_summary_po 
                            where date >= '$dari' and date <= '$sampai'  and factory_id = '2' and activity_name = 'defect_endline' 
                            GROUP BY date, style, line_id, line_name, factory_id
                ) de on  de.date=lsp.date and de.factory_id = lsp.factory_id and de.line_id=lsp.line_id and de.style = lsp.style
            LEFT JOIN(
                        SELECT date, style, line_id, line_name, factory_id, sum(output) as total_qc from line_summary_po 
                            where date >= '$dari' and date <= '$sampai'  and factory_id = '2' and activity_name = 'outputqc' 
                            GROUP BY date, style, line_id, line_name, factory_id
                ) qo on  qo.date=lsp.date and qo.factory_id = lsp.factory_id and qo.line_id=lsp.line_id and qo.style = lsp.style
            WHERE lsp.date >= '$dari' and lsp.date <= '$sampai' and lsp.factory_id = '$factory' and activity_name in ('outputqc','defect_endline','defect_inline')
            GROUP BY 
            lsp.style,
            lsp.date,
            lsp.line_id,
            ml.line_name,
            lsp.factory_id,
            total_qc,
            total_inline,
            total_endline,
            ml.no_urut
                
            ORDER BY ml.no_urut
        ");
            
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    
    function allposts_count_reportwft($dari,$sampai)
    {
        $factory = $this->session->userdata('factory');

        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        // $this->db->where('factory_id',$factory);
        // $this->db->where('date',$date);
        // $this->db->where('activity_name','outputqc');
        // $this->db->select('sum(output) as total_good_endline');
        // $this->db->select('line_id,line_name,date,factory_id');
        // $query = $this->db->get('line_summary_po');


        $query = $this->db->query("SELECT lsp.date, lsp.line_id, lsp.line_name, lsp.factory_id, sum(lsp.output) as total_qc,
            di.total_inline, de.total_endline, ml.no_urut from line_summary_po lsp
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_inline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_inline' 
                GROUP BY date, line_id, line_name, factory_id) di on di.date=lsp.date 
                and di.factory_id = lsp.factory_id and di.line_id=lsp.line_id
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_endline from line_summary_po 
                where  date >= '$dari' and date <= '$sampai'and factory_id = '$factory' and activity_name = 'defect_endline' 
                GROUP BY date, line_id, line_name, factory_id) de on de.date=lsp.date 
                and de.factory_id = lsp.factory_id and de.line_id=lsp.line_id
            LEFT JOIN master_line ml on ml.master_line_id = lsp.line_id
            where lsp.date >= '$dari' and lsp.date <= '$sampai' and lsp.factory_id = '$factory' and lsp.activity_name = 'outputqc'
            GROUP BY lsp.line_id, lsp.line_name, lsp.factory_id, di.total_inline, de.total_endline, ml.no_urut, lsp.date
            ORDER BY ml.no_urut");

        return $query->num_rows();
    }

    function allposts_reportwft($limit,$start,$col,$dir,$dari,$sampai)
    {
        $factory = $this->session->userdata('factory');

        // $this->db->limit($limit,$start);
        // $this->db->order_by($col,$dir);
        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        // $this->db->where('factory_id',$factory);
        // $this->db->where('date',$date);
        // $this->db->where('activity_name','outputqc');
        // $this->db->select('sum(output) as total_good_endline');
        // $this->db->select('line_id,line_name,date,factory_id');
        // $query = $this->db->get('line_summary_po');

        $query = $this->db->query("SELECT lsp.date, lsp.line_id, lsp.line_name, lsp.factory_id, sum(lsp.output) as total_qc,
            di.total_inline, de.total_endline, ml.no_urut from line_summary_po lsp
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_inline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_inline' 
                GROUP BY date, line_id, line_name, factory_id) di on di.date=lsp.date 
                and di.factory_id = lsp.factory_id and di.line_id=lsp.line_id
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_endline from line_summary_po 
                where  date >= '$dari' and date <= '$sampai'and factory_id = '$factory' and activity_name = 'defect_endline' 
                GROUP BY date, line_id, line_name, factory_id) de on de.date=lsp.date 
                and de.factory_id = lsp.factory_id and de.line_id=lsp.line_id
            LEFT JOIN master_line ml on ml.master_line_id = lsp.line_id
            where lsp.date >= '$dari' and lsp.date <= '$sampai' and lsp.factory_id = '$factory' and lsp.activity_name = 'outputqc'
            GROUP BY lsp.line_id, lsp.line_name, lsp.factory_id, di.total_inline, de.total_endline, ml.no_urut, lsp.date
            order by $col $dir limit $limit offset $start
            ORDER BY ml.no_urut");

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_reportwft($limit,$start,$search,$col,$dir,$dari,$sampai)
    {
        $factory = $this->session->userdata('factory');
        $kata    = strtoupper($search);

        $query = $this->db->query("SELECT lsp.date, lsp.line_id, lsp.line_name, lsp.factory_id, sum(lsp.output) as total_qc,
            di.total_inline, de.total_endline, ml.no_urut from line_summary_po lsp
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_inline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_inline' 
                GROUP BY date, line_id, line_name, factory_id) di on di.date=lsp.date 
                and di.factory_id = lsp.factory_id and di.line_id=lsp.line_id
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_endline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_endline' 
                GROUP BY date, line_id, line_name, factory_id) de on de.date=lsp.date 
                and de.factory_id = lsp.factory_id and de.line_id=lsp.line_id
            LEFT JOIN master_line ml on ml.master_line_id = lsp.line_id
            where lsp.date >= '$dari' and lsp.date <= '$sampai' and lsp.factory_id = '$factory' and lsp.activity_name = 'outputqc'
            and lsp.line_name like '%$kata%'
            GROUP BY lsp.line_id, lsp.line_name, lsp.factory_id, di.total_inline, de.total_endline, ml.no_urut, lsp.date
            order by $col $dir limit $limit offset $start
            ORDER BY ml.no_urut");


        // $this->db->limit($limit,$start);
        // $this->db->order_by($col,$dir);
        // $this->db->like('line_name',strtoupper($search), 'BOTH');
        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        // $this->db->where('factory_id',$factory);
        // $this->db->where('date',$date);
        // $this->db->where('activity_name','outputqc');
        // $this->db->select('sum(output) as total_good_endline');
        // $this->db->select('line_id,line_name,date,factory_id');
        // $query = $this->db->get('line_summary_po');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_reportwft($search,$dari,$sampai)
    {
        $factory = $this->session->userdata('factory');
        $kata    = strtoupper($search);

        $query = $this->db->query("SELECT lsp.date, lsp.line_id, lsp.line_name, lsp.factory_id, sum(lsp.output) as total_qc,
            di.total_inline, de.total_endline, ml.no_urut from line_summary_po lsp
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_inline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_inline' 
                GROUP BY date, line_id, line_name, factory_id) di on di.date=lsp.date 
                and di.factory_id = lsp.factory_id and di.line_id=lsp.line_id
            LEFT JOIN (
                SELECT date, line_id, line_name, factory_id, sum(output) as total_endline from line_summary_po 
                where date >= '$dari' and date <= '$sampai' and factory_id = '$factory' and activity_name = 'defect_endline' 
                GROUP BY date, line_id, line_name, factory_id) de on de.date=lsp.date 
                and de.factory_id = lsp.factory_id and de.line_id=lsp.line_id
            LEFT JOIN master_line ml on ml.master_line_id = lsp.line_id
            where lsp.date >= '$dari' and lsp.date <= '$sampai' and lsp.factory_id = '$factory' and lsp.activity_name = 'outputqc'
            and lsp.line_name like '%$kata%'
            GROUP BY lsp.line_id, lsp.line_name, lsp.factory_id, di.total_inline, de.total_endline, ml.no_urut, lsp.date
            ORDER BY ml.no_urut");
        
        return $query->num_rows();
    }
    // end report wft
    //
    //
    // report measurement inline
    function allposts_count_reportmeasurement($dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $query = $this->db->query("select * from f_summary_inline_ranged('$dari','$sampai', $factory)");
        }else if ($filter == 'by_line') {
            $query = $this->db->query("select * from f_cap_inline_beetwen_ranged('$dari','$sampai', $line_from, $line_to, $factory)");

        }

        return $query->num_rows();
    }

    function allposts_reportmeasurement($limit,$start,$col,$dir,$dari,$sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            // $this->db->limit($limit,$start);
            // $this->db->order_by($col,$dir);
            $query = $this->db->query("select * from f_summary_inline_ranged('$dari','$sampai',$factory) order by date,no_urut,master_proses_id limit $limit offset $start");
        }elseif ($filter == 'by_line') {
            $query = $this->db->query("select * from f_cap_inline_beetwen_ranged('$dari','$sampai',$line_from, $line_to, $factory) order by date,no_urut,master_proses_id limit $limit offset $start");

        }

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_reportmeasurement($limit,$start,$search,$col,$dir,$dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            // $this->db->limit($limit,$start);
            // $this->db->order_by($col,$dir);
            $query = $this->db->query("select * from f_summary_inline_ranged('$dari','$sampai',$factory) order by date,no_urut,master_proses_id limit $limit offset $start");
        }elseif ($filter == 'by_line') {
            $query = $this->db->query("select * from f_cap_inline_beetwen_ranged('$dari','$sampai',$line_from, $line_to, $factory) order by date,no_urut,master_proses_id limit $limit offset $start");

        }

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_reportmeasurement($search,$dari,$sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $query = $this->db->query("select * from f_summary_inline_ranged('$dari','$sampai',$factory)");
        }elseif ($filter == 'by_line') {
            $query = $this->db->query("select * from f_cap_inline_beetwen_ranged('$dari','$sampai',$line_from, $line_to, $factory)");
        }

        return $query->num_rows();
    }
    // end report measurement inline

    // report measurement endline
    function allposts_count_reportmeasurement_endline($dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id',$factory);
            $query = $this->db->get('adt_cap_measurement');
        }else if ($filter == 'by_line') {
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id',$factory);
            $this->db->where('line_id >=',$line_from);
            $this->db->where('line_id <=',$line_to);
            $query = $this->db->get('adt_cap_measurement');

        }

        return $query->num_rows();
    }

    function allposts_reportmeasurement_endline($limit,$start,$col,$dir,$dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $this->db->limit($limit,$start);
            $this->db->order_by($col,$dir);
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id', $factory);
            $query = $this->db->get('adt_cap_measurement');
        }elseif ($filter == 'by_line') {
            $this->db->limit($limit,$start);
            $this->db->order_by($col,$dir);
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id', $factory);
            $this->db->where('line_id >=',$line_from);
            $this->db->where('line_id <=',$line_to);
            $query = $this->db->get('adt_cap_measurement');
        }

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_reportmeasurement_endline($limit,$start,$search,$col,$dir,$dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $this->db->limit($limit,$start);
            $this->db->order_by($col,$dir);
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id', $factory);
            $query = $this->db->get('adt_cap_measurement');
        }elseif ($filter == 'by_line') {
            $this->db->limit($limit,$start);
            $this->db->order_by($col,$dir);
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id', $factory);
            $this->db->where('line_id >=',$line_from);
            $this->db->where('line_id <=',$line_to);
            $query = $this->db->get('adt_cap_measurement');

        }

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_reportmeasurement_endline($search,$dari, $sampai, $line_from, $line_to, $filter, $factory)
    {
        if ($filter == 'by_date') {
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id',$factory);
            $query = $this->db->get('adt_cap_measurement');
        }elseif ($filter == 'by_line') {
            $this->db->where('create_date >=',$dari);
            $this->db->where('create_date <=',$sampai);
            $this->db->where('factory_id',$factory);
            $this->db->where('line_id >=',$line_from);
            $this->db->where('line_id <=',$line_to);
            $query = $this->db->get('adt_cap_measurement');
        }

        return $query->num_rows();
    }
    // end report measurement endline
    /*hitung jumlah line yang aktif inline*/
    public function count_line($date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->group_by('line_id');
        $this->db->where(array('DATE(create_date)'=>$date,'factory_id'=>$factory));
        $this->db->select('line_id');
        $this->db->distinct();
        return $this->db->get('inline_detail')->num_rows();
    }
    /*data table tls endline*/
    function allposts_count_tlsendline($dari,$sampai)
    {
        $factory = $this->session->userdata('factory');
        $this->db->where('factory_id',$factory);
        $this->db->where('date >=',$dari);
        $this->db->where('date <=',$sampai);
        $query = $this->db->get('wft_total_good');


        return $query->num_rows();
    }

    function allposts_tlsendline($limit,$start,$col,$dir,$dari,$sampai)
    {
        $factory = $this->session->userdata('factory');

        // $this->db->limit($limit,$start);
        // $this->db->order_by($col,$dir);
        // $this->db->where('factory_id',$factory);
        // $this->db->where('date >=',$dari);
        // $this->db->where('date <=',$sampai);
        // $query = $this->db->get('wft_total_good');

        
        $query = $this->db->query("SELECT wtg.*,ml.no_urut from wft_total_good wtg
        LEFT JOIN master_line ml on ml.master_line_id = wtg.line_id
        where wtg.factory_id = '$factory'
        and wtg.date >= '$dari' and wtg.date <= '$sampai'
        ORDER BY date,ml.no_urut
        limit $limit offset $start");

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_tlsendline($limit,$start,$search,$col,$dir,$dari,$sampai)
    {
        $factory = $this->session->userdata('factory');

        // $this->db->limit($limit,$start);
        // $this->db->order_by($col,$dir);
        // $this->db->like('line_name',strtoupper($search), 'BOTH');
        // $this->db->or_like('poreference', $search, 'BOTH');
        // $this->db->where('date >=',$dari);
        // $this->db->where('date <=',$sampai);
        // $this->db->where('factory_id',$factory);
        // $query = $this->db->get('wft_total_good');
        
        $query = $this->db->query("SELECT wtg.*,ml.no_urut from wft_total_good wtg
        LEFT JOIN master_line ml on ml.master_line_id = wtg.line_id
        where wtg.factory_id = '$factory'
        and wtg.date >= '$dari' and wtg.date <= '$sampai'
        ORDER BY date, ml.no_urut
        limit $limit offset $start");
        

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_tlsendline($search,$dari,$sampai)
    {
        $factory = $this->session->userdata('factory');

        // $this->db->like('line_name',strtoupper($search), 'BOTH');
        // $this->db->or_like('poreference', $search, 'BOTH');
        // $this->db->where('factory_id',$factory);
        // $this->db->where('date >=',$dari);
        // $this->db->where('date <=',$sampai);
        // $query = $this->db->get('wft_total_good');

        $query = $this->db->query("SELECT wtg.*,ml.no_urut from wft_total_good wtg
        LEFT JOIN master_line ml on ml.master_line_id = wtg.line_id
        where wtg.factory_id = '$factory'
        and wtg.date >= '$dari' and wtg.date <= '$sampai'");

        return $query->num_rows();
    }
    public function downloadReportTlsEndline($dari,$sampai)
    {
        $factory = $this->session->userdata('factory');

        $query = $this->db->query("SELECT wtg.*,ml.no_urut from wft_total_good wtg
        LEFT JOIN master_line ml on ml.master_line_id = wtg.line_id
        where wtg.factory_id = '$factory'
        and wtg.date >= '$dari' and wtg.date <= '$sampai'
        ORDER BY date, ml.no_urut");

        return $query;

        // $this->db->where(array('date'=>$date,'factory_id'=>$factory));
        // return $this->db->get('wft_total_good');

    }
    /*end data table tls endline*/
    public function downloadDataEntryEndline($date,$line_id,$style,$factory_id)
    {
        $query = " WITH endline_int AS (
                         SELECT DISTINCT qed.qc_endline_trans_id,
                                        qed.defect_id,
                            count(qed.defect_id) AS total,
                            qed.style,
                            qed.factory_id,
                            ih.line_id,
                            date(qed.create_date) AS date
                           FROM qc_endline_defect qed
                             JOIN inline_header ih ON ih.style = qed.style AND ih.inline_header_id = qed.inline_header_id
                where date(qed.create_date)='$date' and ih.line_id=$line_id and qed.style='$style' and qed.factory_id=$factory_id
                GROUP BY qed.qc_endline_trans_id, date(qed.create_date), qed.style, qed.factory_id, ih.line_id,qed.defect_id
                        )
                 SELECT 
                endline_int.date,
                        endline_int.line_id,
                    endline_int.style,
                        endline_int.defect_id,
                        master_defect.defect_code,
                count(endline_int.total) AS total_defect,
                    endline_int.factory_id
                   FROM endline_int JOIN master_defect ON endline_int.defect_id=master_defect.defect_id
                  GROUP BY endline_int.date, endline_int.style, endline_int.factory_id, endline_int.line_id,endline_int.defect_id,master_defect.defect_code
                  ORDER BY endline_int.line_id";
        return $this->db->query($query);
    }
    public function dataEntryInline($date)
    {
        $factory = $this->session->userdata('factory');
        // $query = "SELECT inline_detail.round,
        //             b.line_id,
        //             ml.line_name,
        //             b.style,
        //             date(inline_detail.create_date) AS date,
        //             inline_detail.defect_id,
        //             count(inline_detail.defect_id) AS total_per_defect,
        //             inline_detail.factory_id
        //            FROM inline_detail inline_detail
        //         LEFT JOIN inline_header b on b.inline_header_id=inline_detail.inline_header_id
        //         left join master_line ml on ml.master_line_id=b.line_id
        //           WHERE inline_detail.defect_id <> 0 and date(inline_detail.create_date) ='$date' and inline_detail.factory_id=$factory
        //           GROUP BY inline_detail.round, (date(inline_detail.create_date)), inline_detail.defect_id, inline_detail.factory_id,b.line_id,b.style,ml.line_name
        //           ORDER BY b.line_id,inline_detail.round";
        $query = "SELECT a.round,
                    b.line_id,
                    ml.line_name,
                    b.style,
                    date(a.create_date) AS date,
                    a.defect_id,
                    count(a.defect_id) AS total_per_defect,
                    a.factory_id,
                                        md.defect_code
                   FROM inline_detail a
                LEFT JOIN inline_header b on b.inline_header_id=a.inline_header_id
                left join master_line ml on ml.master_line_id=b.line_id
                                join master_defect md on a.defect_id=md.defect_id
                  WHERE a.defect_id <> 0 and date(a.create_date) ='$date' and a.factory_id=$factory
                  GROUP BY a.round, (date(a.create_date)), a.defect_id, a.factory_id,b.line_id,b.style,ml.line_name,md.defect_code
                  ORDER BY b.line_id,a.round";
        return $this->db->query($query);
    }
    public function dataentryinlinerange($awal,$akhir)
    {
        $this->db->where('date BETWEEN \''.$awal. '\' and \''.$akhir.'\'');
        $this->db->where('factory_id', $this->session->userdata('factory'));
        return $this->db->get('dataentry_inline');
    }
    /*data table serverside output production*/
    //hari H

    //REPORT SUMMARY DETAIL
    function allposts_count_distributionrecieve($date,$factory)
    {
        

        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        $this->db->where('factory_id',$factory);
        $this->db->where('date',$date);
        // $this->db->group_start();
            $this->db->where('status <>','onprogress');
            // $this->db->where('status','completed');
            
	    //     $this->db->or_where('status', 'pindah line');
	    // $this->db->group_end();
        // $this->db->select('*');
        // $this->db->select('line_id,line_name,line_id,factory_id');
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

    function allposts_distributionrecieve($limit,$start,$col,$dir,$date,$factory)
    {
       
        // $tanggal =($date!=0?$date:date('Y-m-d'));

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $this->db->order_by($date);
        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        // $this->db->group_start();
        $this->db->where('status <>','onprogress');
        //     $this->db->or_where('status', 'pindah line');
        // $this->db->group_end();
        //  $this->db->select('*');
        // $this->db->select('date,line_id,line_name,line_id,factory_id');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }
    
    function posts_search_distributionrecieve($limit,$start,$search,$col,$dir,$dari='',$sampai='',$pobuyer=null, $factory)
    {

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $this->db->group_start();
        // $this->db->group_end();
        $this->db->where('status <>','onprogress');
        if($dari!='' && $sampai!=''){

            $this->db->where('date >=',$dari);
            $this->db->where('date <=',$sampai);
            
            
        }
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        // $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_distributionrecieve($search,$dari='',$sampai='',$pobuyer=null, $factory)
    {
        $factory = $this->session->userdata('factory');

        $this->db->where('status <>','onprogress');
        if($dari!='' && $sampai!=''){
            
            $this->db->where('date >=',$dari);
            $this->db->where('date <=',$sampai);            
        }
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

    //REPORT SUMMARY PO
    function allposts_count_distributionrecieve_po($date,$factory)
    {
        // select date,line_id,line_name,factory_id,style,poreference,article,status, sum(total)as total from summary_distribusi 
        // where date='2019-05-16' and poreference='0122795443' and status = 'completed'
		// 	GROUP BY date,line_id,line_name,factory_id,style,poreference,article,status

        $this->db->group_by('date,line_id,line_name,factory_id,style,poreference,article,status');
        $this->db->where('factory_id',$factory);
        $this->db->where('date',$date);
        // $this->db->group_start();
        $this->db->where('status <>','onprogress');
            // $this->db->where('status','completed');
            
	    //     $this->db->or_where('status', 'pindah line');
	    // $this->db->group_end();
        $this->db->select('date,line_id,line_name,factory_id,style,poreference,article,status, sum(total)as total ');
        // $this->db->select('line_id,line_name,line_id,factory_id');
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

    function allposts_distributionrecieve_po($limit,$start,$col,$dir,$date,$factory)
    {
       
        $tanggal =($date!=0?$date:date('Y-m-d'));

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $this->db->order_by($date);
        $this->db->group_by('date,line_id,line_name,factory_id,style,poreference,article,status');
        $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        // $this->db->group_start();
        $this->db->where('status <>','onprogress');
        //     $this->db->or_where('status', 'pindah line');
        // $this->db->group_end();
        $this->db->select('date,line_id,line_name,factory_id,style,poreference,article,status, sum(total)as total ');
        // $this->db->select('date,line_id,line_name,line_id,factory_id');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }
    
    function posts_search_distributionrecieve_po($limit,$start,$search,$col,$dir,$dari='',$sampai='',$pobuyer=null, $factory)
    {

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $this->db->group_start();
        // $this->db->group_end();
        $this->db->group_by('date,line_id,line_name,factory_id,style,poreference,article,status');
        $this->db->where('status <>','onprogress');
        if($dari!='' && $sampai!=''){

            $this->db->where('date >=',$dari);
            $this->db->where('date <=',$sampai);
            
            
        }
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        // $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        $this->db->select('date,line_id,line_name,factory_id,style,poreference,article,status, sum(total)as total ');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_distributionrecieve_po($search,$dari='',$sampai='',$pobuyer=null, $factory)
    {
        $factory = $this->session->userdata('factory');

        $this->db->group_by('date,line_id,line_name,factory_id,style,poreference,article,status');
        $this->db->where('status <>','onprogress');
        if($dari!='' && $sampai!=''){
            
            $this->db->where('date >=',$dari);
            $this->db->where('date <=',$sampai);            
        }
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        $this->db->select('date,line_id,line_name,factory_id,style,poreference,article,status, sum(total)as total ');
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

    //REPORT SCANNED ONLY
    function allposts_count_distribution_scanned($date,$factory)
    {
        

        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        $this->db->where('factory_id',$factory);
        $this->db->where('date',$date);
        // $this->db->group_start();
        // $this->db->where('status <>','onprogress');
            // $this->db->where('status','completed');
            
	    //     $this->db->or_where('status', 'pindah line');
	    // $this->db->group_end();
        // $this->db->select('*');
        // $this->db->select('line_id,line_name,line_id,factory_id');
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

    function allposts_distribution_scanned($limit,$start,$col,$dir,$date,$factory)
    {
       
        $tanggal =($date!=0?$date:date('Y-m-d'));

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $this->db->order_by($date);
        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        // $this->db->group_start();
        // $this->db->where('status <>','onprogress');
        //     $this->db->or_where('status', 'pindah line');
        // $this->db->group_end();
        //  $this->db->select('*');
        // $this->db->select('date,line_id,line_name,line_id,factory_id');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }
    
    function posts_search_distribution_scanned($limit,$start,$search,$col,$dir,$dari,$sampai,$pobuyer,$factory)
    {

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $this->db->group_start();
        // $this->db->group_end();
        // $this->db->where('status <>','onprogress');
        
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        else{
            if($dari!='' && $sampai!=''){
                $this->db->where('date >=',$dari);
                $this->db->where('date <=',$sampai);
            }
        }
        // $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_distribution_scanned($search,$dari,$sampai,$pobuyer,$factory)
    {
        $factory = $this->session->userdata('factory');

        // $this->db->where('status <>','onprogress');
        
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        else{
            if($dari!='' && $sampai!=''){
                $this->db->where('date >=',$dari);
                $this->db->where('date <=',$sampai);
            }
        }
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

    //DETAIL RECEIVED

    function allposts_count_detail_scanned($po,$style,$size,$article)
    {
        

        // $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        // $this->db->where('factory_id',$factory);
        $this->db->where('poreference',$po);
        $this->db->where('style',$style);
        $this->db->where('size',$size);
        $this->db->where('article',$article);
        // $this->db->group_start();
            // $this->db->where('status <>','onprogress');
            // $this->db->where('status','completed');
            
	    //     $this->db->or_where('status', 'pindah line');
	    // $this->db->group_end();
        // $this->db->select('*');
        // $this->db->select('line_id,line_name,line_id,factory_id');
        $query = $this->db->get('distribusi_detail_view');

        return $query->num_rows();
    }

    function allposts_detail_scanned($limit,$start,$col,$dir,$po,$style,$size,$article)
    {
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $this->db->order_by($date);
        // $this->db->group_by('date,line_id,line_name,line_id,factory_id
        // $this->db->where('factory_id',$factory);
        $this->db->where('poreference',$po);
        $this->db->where('style',$style);
        $this->db->where('size',$size);
        $this->db->where('article',$article);
        //     $this->db->or_where('status', 'pindah line');
        // $this->db->group_end();
        //  $this->db->select('*');
        // $this->db->select('date,line_id,line_name,line_id,factory_id');
        $query = $this->db->get('distribusi_detail_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function download_distribution_scanned($dari,$sampai,$po,$factory)
    {
        if($po!=''){
            $this->db->where('poreference',$po);
        }
        else{
            $this->db->where('date >=',$dari);
            $this->db->where('date <=',$sampai);
        }
        $this->db->where('factory_id',$factory);
        // $this->db->where('status <>','onprogress');
        //     $this->db->or_where('status', 'pindah line');
        // $this->db->group_end();
        //  $this->db->select('*');
        // $this->db->select('date,line_id,line_name,line_id,factory_id');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function download_detail_scanned($po,$style,$size,$article)
    {
        // $this->db->limit($limit,$start);
        // $this->db->order_by($col,$dir);
        // $this->db->order_by($date);
        // $this->db->group_by('date,line_id,line_name,line_id,factory_id
        // $this->db->where('factory_id',$factory);
        $this->db->where('poreference',$po);
        $this->db->where('style',$style);
        $this->db->where('size',$size);
        $this->db->where('article',$article);
        //     $this->db->or_where('status', 'pindah line');
        // $this->db->group_end();
        //  $this->db->select('*');
        // $this->db->select('date,line_id,line_name,line_id,factory_id');
        $query = $this->db->get('distribusi_detail_view');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    public function sum_GrandTotal($po,$size,$style,$dari,$sampai,$line_id)
    {
        return $this->db->query("select sum(total) from summary_distribusi where line_id= '$line_id' 
        and poreference='$po' and size='$size' and style='$style' 
        and date >= '$dari' and date<='$sampai' and status <> 'onprogress'")->row();
    }

    public function sum_GrandTotal_po($po,$style,$dari,$sampai,$line_id)
    {
        return $this->db->query("select sum(total) from summary_distribusi where line_id= '$line_id' 
        and poreference='$po' and style='$style' 
        and date >= '$dari' and date<='$sampai' and status <> 'onprogress'")->row();
    }

    public function download_outputDistribution($dari='',$sampai='',$pobuyer=null, $factory)
    {
        // $factory = $this->session->userdata('factory');
        $this->db->where('status <>','onprogress');
        if($dari!='' && $sampai!=''){
            
            $this->db->where('date >=',$dari);
            $this->db->where('date <=',$sampai);            
        }
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        // $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    public function download_outputDistribution_po($dari='',$sampai='',$pobuyer=null, $factory)
    {
        // $factory = $this->session->userdata('factory');
        $this->db->group_by('date,line_id,line_name,factory_id,style,poreference,article,status');
        $this->db->where('status <>','onprogress');
        if($dari!='' && $sampai!=''){
            
            $this->db->where('date >=',$dari);
            $this->db->where('date <=',$sampai);            
        }
        if($pobuyer!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($pobuyer), 'BOTH');
        }
        // $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        $this->db->select('date,line_id,line_name,factory_id,style,poreference,article,status, sum(total)as total ');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function allposts_count_productionoutput($dari,$sampai,$factory)
    {

        // $tanggal =($date!=0?$date:date('Y-m-d'));
        $this->db->where('factory_id',$factory);

        
        $awal =($dari!=0?$dari:date('Y-m-d'));
        $akhir =($sampai!=0?$sampai:date('Y-m-d'));
        $this->db->where('date >= ',$awal);
        $this->db->where('date <= ',$akhir);
        
        $query = $this->db->get('edm_buku_hijau');

        return $query->num_rows();
    }
    
    function allposts_productionoutput($limit,$start,$col,$dir,$dari,$sampai,$factory)
    {
       
        // $tanggal =($date!=0?$date:date('Y-m-d'));

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->where('factory_id',$factory);
        $awal =($dari!=0?$dari:date('Y-m-d'));
        $akhir =($sampai!=0?$sampai:date('Y-m-d'));
        
        $this->db->where('date >= ',$awal);
        $this->db->where('date <= ',$akhir);

        
        $query = $this->db->get('edm_buku_hijau');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_productionoutput($limit,$start,$search,$col,$dir,$dari,$sampai,$filter,$pobuyer,$line_from,$line_to,$balance,$factory)
    {
        // $tanggal =($date!=0?$date:date('Y-m-d'));
        
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->where('factory_id',$factory);
        
        $tgl = date('Y-m-d');
        if($dari!=0){
            $this->db->where('date >= ',$dari);
        }
        else{
            $this->db->where('date >= ',$tgl);
        }
        if($sampai!=0){
            $this->db->where('date <= ',$sampai);
        }
        else{
            $this->db->where('date <= ',$tgl);
        }
        
        if ($filter == 'by_line') {

            $this->db->where('no_urut BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');
            
            
        }
        if ($filter == 'by_pobuyer') {

            $this->db->where('poreference', $pobuyer);

        }
        if ($filter=='by_balance') {
            if ($balance==0) {
                $this->db->where('balance_per_day <=', '0');
            }else if ($balance==1) {
                 $this->db->where('balance_per_day >', '0');
            }else if ($balance==2) {
                $this->db->where('balance_per_day =', '0');
            }elseif ($balance==3) {
                $this->db->where('balance_per_day <', '0');
            }
        }

        $query = $this->db->get('edm_buku_hijau');
        
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_productionoutput($search,$dari,$sampai,$filter,$pobuyer,$line_from,$line_to,$balance,$factory)
    {
        $tgl = date('Y-m-d');
        // $tanggal =($date!=0?$date:date('Y-m-d'));
        $this->db->where('factory_id',$factory);
         if($dari!=0){
            $this->db->where('date >= ',$dari);
        }
        else{
            $this->db->where('date >= ',$tgl);
        }
        if($sampai!=0){
            $this->db->where('date <= ',$sampai);
        }
        else{
            $this->db->where('date <= ',$tgl);
        }
        if ($filter == 'by_line') {

            $this->db->where('no_urut BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');
            
            
        }
        if ($filter == 'by_pobuyer') {

            $this->db->where('poreference', $pobuyer);

        }
        if ($filter=='by_balance') {
            if ($balance==0) {
                $this->db->where('balance_per_day <=', '0');
            }else if ($balance==1) {
                 $this->db->where('balance_per_day >', '0');
            }else if ($balance==2) {
                $this->db->where('balance_per_day =', '0');
            }elseif ($balance==3) {
                $this->db->where('balance_per_day <', '0');
            }
        }

        $query = $this->db->get('edm_buku_hijau');
       
        return $query->num_rows();
    }
    /*end data table serverside output production*/
    public function downloadProduction($dari,$sampai,$filter,$pobuyer,$line_from,$line_to,$balance,$factory)
    {
        // $tanggal =($date!=0?$date:date('Y-m-d'));
        $this->db->where('factory_id',$factory);
        // $this->db->where('date',$tanggal);
        $tgl = date('Y-m-d');
        
        if ($filter == 'by_line') {

            if($dari!=0){
                $this->db->where('date >= ',$dari);
            }
            else{
                $this->db->where('date >= ',$tgl);
            }
            if($sampai!=0){
                $this->db->where('date <= ',$sampai);
            }
            else{
                $this->db->where('date <= ',$tgl);
            }
            $this->db->where('no_urut BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');
            
            
        }
        else if ($filter == 'by_pobuyer') {

            if($dari!=0){
                $this->db->where('date >= ',$dari);
            }
            else{
                $this->db->where('date >= ',$tgl);
            }
            if($sampai!=0){
                $this->db->where('date <= ',$sampai);
            }
            else{
                $this->db->where('date <= ',$tgl);
            }
            $this->db->where('poreference', $pobuyer);

        }
        else if ($filter=='by_balance') {
            if($dari!=0){
                $this->db->where('date >= ',$dari);
            }
            else{
                $this->db->where('date >= ',$tgl);
            }
            if($sampai!=0){
                $this->db->where('date <= ',$sampai);
            }
            else{
                $this->db->where('date <= ',$tgl);
            }
            if ($balance==0) {
                $this->db->where('balance_per_day <=', '0');
            }else if ($balance==1) {
                 $this->db->where('balance_per_day >', '0');
            }else if ($balance==2) {
                $this->db->where('balance_per_day =', '0');
            }elseif ($balance==3) {
                $this->db->where('balance_per_day <', '0');
            }
        }
        else{
            $awal =($dari!=0?$dari:date('Y-m-d'));
                $this->db->where('date >= ',$awal);
            $akhir =($sampai!=0?$sampai:date('Y-m-d'));
                $this->db->where('date <= ',$akhir);
        }

        $query = $this->db->get('edm_buku_hijau');
        return $query;
    }
    /*START LINE PERFORMANCE*/
    function allposts_count_lineperformance($date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        $this->db->where('factory_id',$factory);
        $this->db->where('date',$date);
        $this->db->select('sum(total_good_endline)');
        $this->db->select('line_id,line_name,line_id,factory_id');
        $query = $this->db->get('wft_total_good');

        return $query->num_rows();
    }

    function allposts_lineperformance($limit,$start,$col,$dir,$date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->group_by('date,line_id,line_name,line_id,factory_id');
        $this->db->where('factory_id',$factory);
        $this->db->where('date',$date);
         $this->db->select('sum(total_good_endline) as total_good_endline');
        $this->db->select('date,line_id,line_name,line_id,factory_id');
        $query = $this->db->get('wft_total_good');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_lineperformance($limit,$start,$search,$col,$dir,$date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->like('line_name',strtoupper($search), 'BOTH');
        $this->db->where('date',$date);
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('wft_total_good');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_lineperformance($search,$date)
    {
        $factory = $this->session->userdata('factory');

        $this->db->like('line_name',strtoupper($search), 'BOTH');
        $this->db->where('factory_id',$factory);
        $this->db->where('date',$date);
        $query = $this->db->get('wft_total_good');

        return $query->num_rows();
    }
    /*END LINE PERFORMANCE*/
    /*data table serverside output production*/
    function allposts_count_productionoutputfolding($dari,$sampai,$factory)
    {
        $awal  = ($dari!=0?$dari:date('Y-m-d'));
        $akhir = ($sampai!=0?$sampai:date('Y-m-d'));
        // $factory = $this->session->userdata('factory');
        $this->db->where('factory_id',$factory);
        $this->db->where('date >= ',$awal);
        $this->db->where('date <= ',$akhir);
        $query = $this->db->get('folding_outputday');

        return $query->num_rows();
    }
    
    function allposts_productionoutputfolding($limit,$start,$col,$dir,$dari,$sampai,$factory)
    {
        // $factory = $this->session->userdata('factory');
        $awal  = ($dari!=0?$dari:date('Y-m-d'));
        $akhir = ($sampai!=0?$sampai:date('Y-m-d'));

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        // $factory = $this->session->userdata('factory');
        $this->db->where('factory_id',$factory);
        $this->db->where('date >= ',$awal);
        $this->db->where('date <= ',$akhir);
        $query = $this->db->get('folding_outputday');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_productionoutputfolding($limit,$start,$search,$col,$dir,$dari,$sampai,$factory,$filter,$pobuyer,$line_from,$line_to,$balance)
    {

        // $factory = $this->session->userdata('factory');
		
		$awal  = ($dari!=0?$dari:date('Y-m-d'));
		$akhir = ($sampai!=0?$sampai:date('Y-m-d'));
		
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        if ($filter == 'by_line') {
			if ($dari!=0 || $sampai!=0) {
				$this->db->where('date >= ',$awal);
				$this->db->where('date <= ',$akhir);
            }
            $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');
        }elseif ($filter == 'by_pobuyer') {
            if ($dari!=0 || $sampai!=0) {
				$this->db->where('date >= ',$awal);
				$this->db->where('date <= ',$akhir);
            }
            $this->db->where('poreference',$pobuyer);

        }else if ($filter=='by_balance') {
            if ($dari!=0 || $sampai!=0) {
				$this->db->where('date >= ',$awal);
				$this->db->where('date <= ',$akhir);
            }
            if ($balance==0) {
                $this->db->where('balance_folding <=',0);
            }else if ($balance==1) {
                $this->db->where('balance_folding >',0);
            }else if ($balance==2) {
               $this->db->where('balance_folding =',0);
            }elseif ($balance==3) {
                $this->db->where('balance_folding <',0);
            }
        }else{
            // $tanggal =($date!=0?$date:date('Y-m-d'));
            $this->db->where('date >= ',$awal);
			$this->db->where('date <= ',$akhir);
		}
		
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('folding_outputday');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_productionoutputfolding($search,$dari,$sampai,$factory,$filter,$pobuyer,$line_from,$line_to,$balance)
    {
        $awal  = ($dari!=0?$dari:date('Y-m-d'));
		$akhir = ($sampai!=0?$sampai:date('Y-m-d'));
	
        if ($filter == 'by_line') {
			if ($dari!=0 || $sampai!=0) {
				$this->db->where('date >= ',$awal);
				$this->db->where('date <= ',$akhir);
            }
            $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');
        }elseif ($filter == 'by_pobuyer') {
            if ($dari!=0 || $sampai!=0) {
				$this->db->where('date >= ',$awal);
				$this->db->where('date <= ',$akhir);
            }
            $this->db->where('poreference',$pobuyer);

        }else if ($filter=='by_balance') {
            if ($dari!=0 || $sampai!=0) {
				$this->db->where('date >= ',$awal);
				$this->db->where('date <= ',$akhir);
            }
            if ($balance==0) {
                $this->db->where('balance_folding <=',0);
            }else if ($balance==1) {
                $this->db->where('balance_folding >',0);
            }else if ($balance==2) {
               $this->db->where('balance_folding =',0);
            }elseif ($balance==3) {
                $this->db->where('balance_folding <',0);
            }
        }else{
            // $tanggal =($date!=0?$date:date('Y-m-d'));
            $this->db->where('date >= ',$awal);
			$this->db->where('date <= ',$akhir);
		}
		
        $this->db->where('factory_id',$factory);
        $query = $this->db->get('folding_outputday');

        return $query->num_rows();
    }
    /*end data table serverside output production*/
    public function download_outputfolding($dari,$sampai,$factory,$filter,$pobuyer,$line_from,$line_to,$balance)
    {
        // $factory = $this->session->userdata('factory');
		$tgl = date('Y-m-d');
        
        if ($filter == 'by_line') {

            if($dari!=0){
                $this->db->where('date >= ',$dari);
            }
            else{
                $this->db->where('date >= ',$tgl);
            }
            if($sampai!=0){
                $this->db->where('date <= ',$sampai);
            }
            else{
                $this->db->where('date <= ',$tgl);
            }
            $this->db->where('line_id BETWEEN \''.$line_from. '\' and \''.$line_to.'\'');
        }elseif ($filter == 'by_pobuyer') {
			if($dari!=0){
                $this->db->where('date >= ',$dari);
            }
            else{
                $this->db->where('date >= ',$tgl);
            }
            if($sampai!=0){
                $this->db->where('date <= ',$sampai);
            }
            else{
                $this->db->where('date <= ',$tgl);
            }
            $this->db->where('poreference',$pobuyer);

        }else if ($filter=='by_balance') {
			if($dari!=0){
                $this->db->where('date >= ',$dari);
            }
            else{
                $this->db->where('date >= ',$tgl);
            }
            if($sampai!=0){
                $this->db->where('date <= ',$sampai);
            }
            else{
                $this->db->where('date <= ',$tgl);
            }
            if ($balance==0) {
                $this->db->where('balance_folding <=',0);
            }else if ($balance==1) {
                $this->db->where('balance_folding >',0);
            }else if ($balance==2) {
               $this->db->where('balance_folding =',0);
            }elseif ($balance==3) {
                $this->db->where('balance_folding <',0);
            }
        }else{
			$awal =($dari!=0?$dari:date('Y-m-d'));
			$this->db->where('date >= ',$awal);
			$akhir =($sampai!=0?$sampai:date('Y-m-d'));
			$this->db->where('date <= ',$akhir);
        }
		$this->db->where('factory_id',$factory);
        $this->db->order_by('date');
        return $this->db->get('folding_outputday');
    }
    public function getLineSummaryPO($dari,$sampai)
    {
       $factory_id = $this->session->userdata('factory');
       $sql = "select * from f_line_summary_2('$dari','$sampai',$factory_id)";
       return $this->db->query($sql);
    }
    public function getLineSummaryQC($dari,$sampai)
    {
        $factory_id = $this->session->userdata('factory');
        $this->db->where('factory_id', $factory_id);
        $this->db->where('date BETWEEN \''.$dari. '\' and \''.$sampai.'\'');
        return $this->db->get('summary_perjam_qc');
    }
    public function getDetailOutputForFastReact($date)
    {
         // $date    = isset($date) ? $date: date('Y-m-d')
        $factory = $this->session->userdata('factory');
        $sql = "select * from get_output_forfastreact('$date',$factory)";
       return $this->db->query($sql);
    }
    public function getQtyMade($poreference,$days_ago)
    {
        $this->db->where('date', $days_ago);
        $this->db->select('sum(max) as qty');
        $this->db->where('poreference', $poreference);
        return $this->db->get('edm_buku_hijau')->row_array()['qty'];
    }
    
    
    /*START LINE PERFORMANCE*/
    function allposts_count_report_set($dari,$sampai,$poreference)
    {
        $factory = $this->session->userdata('factory');

        $sql = "SELECT DATE
            ( scan_date ) AS DATE,
        CASE
                
                WHEN POSITION ( '-' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '-' IN fo.STYLE ) - 1 ) 
                WHEN POSITION ( '_' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '_' IN fo.STYLE ) - 1 ) ELSE fo.STYLE 
            END AS STYLE,
            fo.poreference,
            ih.article,
            fo.SIZE,
            mo.qty_ordered,
            ( mo.qty_ordered - MAX ( counter_folding ) ) AS balance_day,
            count ( counter_folding ) / 2 AS counter_day,
            string_agg ( DISTINCT ml.line_name, ', ' ) AS line 
        FROM
            folding fo
            LEFT JOIN header_line ih ON ih.inline_header_id = fo.inline_header_id 
            AND ih.SIZE = fo.
            SIZE LEFT JOIN master_order mo ON mo.STYLE = ih.STYLE 
            AND mo.poreference = ih.poreference 
            AND mo.SIZE = ih.size 
            AND mo.kst_articleno = ih.article
            LEFT JOIN folding_package_barcode fpb on fpb.barcode_id = fo.barcode_package
            LEFT JOIN master_line ml ON ml.master_line_id = fpb.line_id 
            -- LEFT JOIN master_line ml ON ml.master_line_id = ih.line_id 
        WHERE
            remark = 'SETS' 
            AND status_folding = 'allocation'
            AND ih.factory_id = '$factory'";

        if($poreference!=0){
            $sql.= "AND fo.poreference LIKE '%$poreference%'";
        }
        else{
            if($dari==$sampai){
                $sql.= "AND date(fo.scan_date) = '$dari'";
            }
            else{
                $sql.= "AND date(fo.scan_date) >= '$dari'
                AND date(fo.scan_date) <= '$sampai'";
            }
            
        }
        
        $sql.= " 
        GROUP BY
            fo.poreference,
            DATE ( scan_date ),
            ih.article,
            fo.SIZE,
            mo.qty_ordered,
        CASE  
            WHEN POSITION ( '-' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '-' IN fo.STYLE ) - 1 ) 
            WHEN POSITION ( '_' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '_' IN fo.STYLE ) - 1 ) ELSE fo.STYLE 
        END";

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    function allposts_report_set($limit,$start,$col,$dir,$dari,$sampai,$poreference)
    {
        $factory = $this->session->userdata('factory');

        $sql = "SELECT DATE
            ( scan_date ) AS DATE,
        CASE
                
                WHEN POSITION ( '-' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '-' IN fo.STYLE ) - 1 ) 
                WHEN POSITION ( '_' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '_' IN fo.STYLE ) - 1 ) ELSE fo.STYLE 
            END AS STYLE,
            fo.poreference,
            ih.article,
            fo.SIZE,
            mo.qty_ordered,
            ( mo.qty_ordered - MAX ( counter_folding ) ) AS balance_day,
            count ( counter_folding ) / 2 AS counter_day,
            string_agg ( DISTINCT ml.line_name, ', ' ) AS line 
        FROM
            folding fo
            LEFT JOIN header_line ih ON ih.inline_header_id = fo.inline_header_id 
            AND ih.SIZE = fo.
            SIZE LEFT JOIN master_order mo ON mo.STYLE = ih.STYLE 
            AND mo.poreference = ih.poreference 
            AND mo.SIZE = ih.size 
            AND mo.kst_articleno = ih.article
            LEFT JOIN folding_package_barcode fpb on fpb.barcode_id = fo.barcode_package
            LEFT JOIN master_line ml ON ml.master_line_id = fpb.line_id 
            -- LEFT JOIN master_line ml ON ml.master_line_id = ih.line_id 
        WHERE
            remark = 'SETS' 
            AND status_folding = 'allocation'
            AND ih.factory_id = '$factory'";

        if($poreference!=0){
            $sql.= "AND fo.poreference LIKE '%$poreference%'";
        }
        else{
            if($dari==$sampai){
                $sql.= "AND date(fo.scan_date) = '$dari'";
            }
            else{
                $sql.= "AND date(fo.scan_date) >= '$dari'
                AND date(fo.scan_date) <= '$sampai'";
            }
            
        }

        $sql.= "GROUP BY
                fo.poreference,
                DATE ( scan_date ),
                ih.article,
                fo.SIZE,
                mo.qty_ordered,
            CASE  
                WHEN POSITION ( '-' IN fo.STYLE ) > 0 THEN
                    LEFT ( fo.STYLE, POSITION ( '-' IN fo.STYLE ) - 1 ) 
                WHEN POSITION ( '_' IN fo.STYLE ) > 0 THEN
                    LEFT ( fo.STYLE, POSITION ( '_' IN fo.STYLE ) - 1 ) ELSE fo.STYLE 
            END
            order by $col $dir
            LIMIT $limit offset $start";
            
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }
    
    function download_report_set($dari,$sampai,$poreference)
    {
        $factory = $this->session->userdata('factory');

        $sql = "SELECT DATE
            ( scan_date ) AS DATE,
        CASE
                
                WHEN POSITION ( '-' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '-' IN fo.STYLE ) - 1 ) 
                WHEN POSITION ( '_' IN fo.STYLE ) > 0 THEN
                LEFT ( fo.STYLE, POSITION ( '_' IN fo.STYLE ) - 1 ) ELSE fo.STYLE 
            END AS STYLE,
            fo.poreference,
            ih.article,
            fo.SIZE,
            mo.qty_ordered,
            ( mo.qty_ordered - MAX ( counter_folding ) ) AS balance_day,
            count ( counter_folding ) / 2 AS counter_day,
            string_agg ( DISTINCT ml.line_name, ', ' ) AS line 
        FROM
            folding fo
            LEFT JOIN header_line ih ON ih.inline_header_id = fo.inline_header_id 
            AND ih.SIZE = fo.
            SIZE LEFT JOIN master_order mo ON mo.STYLE = ih.STYLE 
            AND mo.poreference = ih.poreference 
            AND mo.SIZE = ih.size 
            AND mo.kst_articleno = ih.article
            LEFT JOIN folding_package_barcode fpb on fpb.barcode_id = fo.barcode_package
            LEFT JOIN master_line ml ON ml.master_line_id = fpb.line_id 
            -- LEFT JOIN master_line ml ON ml.master_line_id = ih.line_id 
        WHERE
            remark = 'SETS' 
            AND status_folding = 'allocation'
            AND ih.factory_id = '$factory'";

        if($poreference!=0){
            $sql.= "AND fo.poreference LIKE '%$poreference%'";
        }
        else{
            if($dari==$sampai){
                $sql.= "AND date(fo.scan_date) = '$dari'";
            }
            else{
                $sql.= "AND date(fo.scan_date) >= '$dari'
                AND date(fo.scan_date) <= '$sampai'";
            }
            
        }

        $sql.= "GROUP BY
                fo.poreference,
                DATE ( scan_date ),
                ih.article,
                fo.SIZE,
                mo.qty_ordered,
            CASE  
                WHEN POSITION ( '-' IN fo.STYLE ) > 0 THEN
                    LEFT ( fo.STYLE, POSITION ( '-' IN fo.STYLE ) - 1 ) 
                WHEN POSITION ( '_' IN fo.STYLE ) > 0 THEN
                    LEFT ( fo.STYLE, POSITION ( '_' IN fo.STYLE ) - 1 ) ELSE fo.STYLE 
            END
            order by date,style,poreference,size";

        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    /*START SUPPLY TIME*/
    function allposts_count_report_st($dari,$sampai,$line_id)
    {
        $factory = $this->session->userdata('factory');

        $sql = "SELECT ml.line_name, sum(d.qty) as qty, extract(hour from d.complete_at) as jam, ml.no_urut from distribusi d
        LEFT JOIN master_line ml on ml.master_line_id = d.line_id
        WHERE d.factory_id = '$factory'";

        if($line_id!=0){
            $sql.= "AND ml.line_name LIKE '%$line_id%'";
        }
        // else{
            if($dari==$sampai){
                $sql.= "AND date(d.complete_at) = '$dari'";
            }
            else{
                $sql.= "AND date(d.complete_at) >= '$dari'
                AND date(d.complete_at) <= '$sampai'";
            }
            
        // }
        
        $sql.= "GROUP BY ml.line_name, jam, ml.no_urut ORDER BY ml.no_urut,jam";

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    function allposts_report_st($limit,$start,$col,$dir,$dari,$sampai,$line_id)
    {
        $factory = $this->session->userdata('factory');

        $sql = "SELECT ml.line_name, sum(d.qty) as qty, extract(hour from d.complete_at) as jam, ml.no_urut from distribusi d
        LEFT JOIN master_line ml on ml.master_line_id = d.line_id
        WHERE d.factory_id = '$factory'";

        if($line_id!=0){
            $sql.= "AND ml.line_name LIKE '%$line_id%'";
        }
        // else{
        if($dari==$sampai){
            $sql.= "AND date(d.complete_at) = '$dari'";
        }
        else{
            $sql.= "AND date(d.complete_at) >= '$dari'
            AND date(d.complete_at) <= '$sampai'";
        }
            
        // }
        
        $sql.= "GROUP BY ml.line_name, jam, ml.no_urut ORDER BY $col $dir
            LIMIT $limit offset $start";
            
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }
    
    function download_report_st($dari,$sampai,$line_id)
    {
        $factory = $this->session->userdata('factory');

        $sql = "SELECT ml.line_name, sum(d.qty) as qty	, extract(hour from d.complete_at) as jam, ml.no_urut from distribusi d
        LEFT JOIN master_line ml on ml.master_line_id = d.line_id
        WHERE d.factory_id = '$factory'";

        if($line_id!=0){
            $sql.= "AND ml.line_name LIKE '%$line_id%'";
        }
        // else{
        if($dari==$sampai){
            $sql.= "AND date(d.complete_at) = '$dari'";
        }
        else{
            $sql.= "AND date(d.complete_at) >= '$dari'
            AND date(d.complete_at) <= '$sampai'";
        }
            
        // }
        
        $sql.= "GROUP BY ml.line_name, jam, ml.no_urut ORDER BY ml.no_urut,jam";

        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

	}
	// List pengajuan ad
	function allposts_count_ad($factory,$line_id,$dari,$sampai)
	{  
		if($dari == 0 || $sampai == 0){
			if($line_id == 0){
				$query = $this
				->db
				->where('factory_id',$factory)
                ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
				->get('pengajuan_ad');
			}
			else{
				$query = $this
				->db
				->where('factory_id',$factory)
				->where('line_id',$line_id)
                ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
				->get('pengajuan_ad');
			}
		}
		else{
			if($dari == $sampai){
				if($line_id == 0){
					$query = $this
					->db
					->where('factory_id',$factory)
					->where('tanggal_permintaan',$dari)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->get('pengajuan_ad');
				}
				else{
					$query = $this
					->db
					->where('factory_id',$factory)
					->where('line_id',$line_id)
					->where('tanggal_permintaan',$dari)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->get('pengajuan_ad');
				}
			}
			else{
				if($line_id == 0){
					$query = $this
					->db
					->where('factory_id',$factory)
					->where('tanggal_permintaan >=',$dari)
					->where('tanggal_permintaan <=',$dari)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->get('pengajuan_ad');
				}
				else{
					$query = $this
					->db
					->where('factory_id',$factory)
					->where('line_id',$line_id)
					->where('tanggal_permintaan >=',$dari)
					->where('tanggal_permintaan <=',$dari)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->get('pengajuan_ad');
				}
			}
		}
   
		return $query->num_rows();  

	}
	
	function allposts_ad($limit,$start,$col,$dir,$factory,$line_id,$dari,$sampai)
	{   
		if($dari == 0 || $sampai == 0){
			if($line_id == 0){
				$query = $this
						->db
						->limit($limit,$start)
						->order_by($col,$dir)
						->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
						->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
						->where('pengajuan_ad.factory_id',$factory)
						->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
						->get('pengajuan_ad');
			}
			else{
				$query = $this
						->db
						->limit($limit,$start)
						->order_by($col,$dir)
						->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
						->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
						->where('pengajuan_ad.factory_id',$factory)
						->where('pengajuan_ad.line_id',$line_id)
						->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
						->get('pengajuan_ad');
			}
		}
		else{
			if($dari == $sampai){
				if($line_id == 0){
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.tanggal_permintaan',$dari)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
				else{
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.line_id',$line_id)
							->where('pengajuan_ad.tanggal_permintaan',$dari)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
			}
			else{
				if($line_id == 0){
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.tanggal_permintaan >=',$dari)
							->where('pengajuan_ad.tanggal_permintaan <=',$sampai)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
				else{
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.line_id',$line_id)
							->where('pengajuan_ad.tanggal_permintaan >=',$dari)
							->where('pengajuan_ad.tanggal_permintaan <=',$sampai)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
			}
		}
		if($query->num_rows()>0)
		{
			return $query->result(); 
		}
		else
		{
			return null;
		}
		
	}
   
	function posts_search_ad($limit,$start,$search,$col,$dir,$factory,$line_id,$dari,$sampai)
	{
		
		if($dari == 0 || $sampai == 0){
			if($line_id == 0){
				$query = $this
						->db
						->limit($limit,$start)
						->order_by($col,$dir)
						->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
						->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
						->where('pengajuan_ad.factory_id',$factory)
						->like('pengajuan_ad.style',strtoupper($search))
						->or_like('pengajuan_ad.poreference',strtoupper($search))
						->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
						->get('pengajuan_ad');
			}
			else{
				$query = $this
						->db
						->limit($limit,$start)
						->order_by($col,$dir)
						->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
						->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
						->like('pengajuan_ad.style',strtoupper($search))
						->or_like('pengajuan_ad.poreference',strtoupper($search))
						->where('pengajuan_ad.factory_id',$factory)
						->where('pengajuan_ad.line_id',$line_id)
						->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
						->get('pengajuan_ad');
			}
		}
		else{
			if($dari == $sampai){
				if($line_id == 0){
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->like('pengajuan_ad.style',strtoupper($search))
							->or_like('pengajuan_ad.poreference',strtoupper($search))
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.tanggal_permintaan',$dari)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
				else{
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->like('pengajuan_ad.style',strtoupper($search))
							->or_like('pengajuan_ad.poreference',strtoupper($search))
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.line_id',$line_id)
							->where('pengajuan_ad.tanggal_permintaan',$dari)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
			}
			else{
				if($line_id == 0){
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->like('pengajuan_ad.style',strtoupper($search))
							->or_like('pengajuan_ad.poreference',strtoupper($search))
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.tanggal_permintaan >=',$dari)
							->where('pengajuan_ad.tanggal_permintaan <=',$sampai)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
				else{
					$query = $this
							->db
							->limit($limit,$start)
							->order_by($col,$dir)
							->join('master_sewer', 'master_sewer.nik = pengajuan_ad.created_by', 'left')
							->join('master_line', 'master_line.master_line_id = pengajuan_ad.line_id', 'left')
							->like('pengajuan_ad.style',strtoupper($search))
							->or_like('pengajuan_ad.poreference',strtoupper($search))
							->where('pengajuan_ad.factory_id',$factory)
							->where('pengajuan_ad.line_id',$line_id)
							->where('pengajuan_ad.tanggal_permintaan >=',$dari)
							->where('pengajuan_ad.tanggal_permintaan <=',$sampai)
							->select('pengajuan_ad.*, master_sewer.name, master_line.line_name')
							->get('pengajuan_ad');
				}
			}
		}
	   
		if($query->num_rows()>0)
		{
			return $query->result();  
		}
		else
		{
			return null;
		}
	}

	function posts_search_count_ad($search,$factory,$line_id,$dari,$sampai)
	{
		if($dari == 0 || $sampai == 0){
			if($line_id == 0){
				$query = $this
				->db
				->where('factory_id',$factory)
                ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
				->like('style',strtoupper($search))
				->or_like('poreference',strtoupper($search))
				->get('pengajuan_ad');
			}
			else{
				$query = $this
				->db
				->where('factory_id',$factory)
				->where('line_id',$line_id)
                ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
				->like('style',strtoupper($search))
				->or_like('poreference',strtoupper($search))
				->get('pengajuan_ad');
			}
		}
		else{
			if($dari == $sampai){
				if($line_id == 0){
					$query = $this
					->db
					->where('factory_id',$factory)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->like('style',strtoupper($search))
					->or_like('poreference',strtoupper($search))
					->where('tanggal_permintaan',$dari)
					->get('pengajuan_ad');
				}
				else{
					$query = $this
					->db
					->where('factory_id',$factory)
					->where('tanggal_permintaan',$dari)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->where('line_id',$line_id)
					->like('style',strtoupper($search))
					->or_like('poreference',strtoupper($search))
					->get('pengajuan_ad');
				}
			}
			else{
				if($line_id == 0){
					$query = $this
					->db
					->where('factory_id',$factory)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->like('style',strtoupper($search))
					->or_like('poreference',strtoupper($search))
					->where('tanggal_permintaan >=',$dari)
					->where('tanggal_permintaan <=',$sampai)
					->get('pengajuan_ad');
				}
				else{
					$query = $this
					->db
					->where('factory_id',$factory)
					->where('tanggal_permintaan >=',$dari)
					->where('tanggal_permintaan <=',$sampai)
					->where('line_id',$line_id)
                    ->join('master_sewer','master_sewer.nik = pengajuan_ad.created_by','LEFT')
					->like('style',strtoupper($search))
					->or_like('poreference',strtoupper($search))
					->get('pengajuan_ad');
				}
			}

		}
	
		return $query->num_rows();
	}
	
    function download_report_pengajuan_ad($dari,$sampai,$line_id)
    {
        $factory = $this->session->userdata('factory');

        $sql = "SELECT ml.line_name, pad.*, ms.name from pengajuan_ad pad
        LEFT JOIN master_line ml on ml.master_line_id = pad.line_id
		LEFT JOIN master_sewer ms on ms.nik = pad.created_by
        WHERE pad.factory_id = '$factory'";

        if($line_id!=0){
            $sql.= "AND ml.line_name LIKE '%$line_id%'";
        }
        // else{
        if($dari==$sampai){
            $sql.= "AND date(pad.tanggal_permintaan) = '$dari'";
        }
        else{
            $sql.= "AND date(d.tanggal_permintaan) >= '$dari'
            AND date(d.tanggal_permintaan) <= '$sampai'";
        }
            
        // }
        
        $sql.= "ORDER BY ml.no_urut, tanggal_permintaan, jam_permintaan";

        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
		}
	}
    function downloadwftPoInline($dari,$sampai,$factory)
    {
        $query = $this->db->query("
        Select 
            date(ind.create_date) as date,
            ind.factory_id,
            ih.line_id,
            ml.line_name,
            ih.style,
            ih.poreference,
            ih.article,
            ind.size,
            md.defect_code,
            md.defect_jenis,
            count(*) as total,
            ml.no_urut
        
        from inline_detail  ind
        LEFT JOIN inline_header ih on ih.inline_header_id=ind.inline_header_id
        LEFT JOIN master_defect md on md.defect_id=ind.defect_id
        LEFT JOIN master_line ml on ml.master_line_id=ih.line_id
        where date(ind.create_date) BETWEEN '$dari' and '$sampai' and ind.defect_id<>0
        and ind.factory_id=$factory
        
        GROUP BY
        date(ind.create_date),
        ind.factory_id,
            ih.line_id,
            ih.style,
            ih.poreference,
            ih.article,
            ind.size,
            md.defect_code,
            md.defect_jenis,
            ml.line_name,
            ml.no_urut
        ORDER BY ml.no_urut
        ");
            
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    function downloadReportCopt($tanggal,$factory)
    {
        $query = $this->db->query("
            select * from report_copt where date='$tanggal' and factory_id='$factory'
        ");
            
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }
    function downloadwftPoEndline($dari,$sampai,$factory)
    {
        $query = $this->db->query("
            select 
            date(qed.create_date) as date,
            ih.line_id,
            ml.line_name,
            ih.factory_id,
            ih.style,
            ih.poreference,
            ih.article,
            ihz.size,
            qed.defect_id,
            md.defect_code,
            md.defect_jenis,
            count(*) as total,
            ml.no_urut
            from qc_endline_defect qed
            left join qc_endline qe on qe.qc_endline_id=qed.qc_endline_id
            left join inline_header ih on ih.inline_header_id=qe.inline_header_id
            LEFT JOIN inline_header_size ihz on ihz.header_size_id=qe.header_size_id
            LEFT JOIN master_line ml on ml.master_line_id=ih.line_id
            LEFT JOIN master_defect md on md.defect_id=qed.defect_id
            where date(qed.create_date) BETWEEN '$dari' and '$sampai'
            and ih.factory_id=$factory
            GROUP BY
            date(qed.create_date),
            ih.line_id,
            ml.line_name,
            ml.no_urut,
            ih.factory_id,
            ih.style,
            ih.poreference,
            ih.article,
            ihz.size,
            md.defect_code,
            md.defect_jenis,
            qed.defect_id
            ORDER BY
            ml.no_urut
        ");
            
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }


}

/* End of file ReportModel.php */
/* Location: ./application/models/ReportModel.php */