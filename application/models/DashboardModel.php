<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends CI_Model {

    function wft_daily($tgl,$factory)
    {

        $query = $this->db->query("SELECT DISTINCT line_id, factory_id, line_name, no_urut 
        FROM daily_view WHERE factory_id = '$factory' and date(create_date) = '$tgl' 
        ORDER BY no_urut ASC");

        if($query->num_rows()>0)
        {
            // return $query->result(); 
            return $query; 
        }
        else
        {
            return null;
        }
        
    }

	function allposts_daily_count($tgl)
    {   
        $factory   = $this->session->userdata('factory');
        $query = $this
                ->db
                ->where('factory_id',$factory)
                ->where('DATE(create_date)',$tgl)
                ->get('daily_view');

        return $query->num_rows();
    }
    
    function allposts_daily($limit,$start,$col,$dir,$tgl)
    {   
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $factory   = $this->session->userdata('factory');
            $query = $this
                    ->db
                    ->limit($limit,$start)
                    ->order_by($col,$dir)
                    ->where('factory_id',$factory)
                    ->where('DATE(create_date)',$tgl)
                    ->get('daily_view');
        }
        else {
            $factory   = $this->session->userdata('factory');
            $query = $this
                ->db
                // ->limit($limit,$start)
                // ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->where('DATE(create_date)',$tgl)
                ->get('daily_view');
        }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
	}
	
	function posts_daily_search($limit,$start,$search,$col,$dir,$tgl)
    {
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $factory   = $this->session->userdata('factory');
            $query = $this
                ->db
                ->like('line_name',$search, 'BOTH')
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->where('DATE(create_date)',$tgl)
                ->get('daily_view');
        }
        else{
            $factory   = $this->session->userdata('factory');
            $query = $this
                ->db
                ->like('line_name',$search, 'BOTH')
                // ->limit($limit,$start)
                // ->order_by($col,$dir)
                ->where('factory_id',$factory)
                ->where('DATE(create_date)',$tgl)
                ->get('daily_view');
        
        }
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_daily_search_count($search,$tgl)
    {
        $factory   = $this->session->userdata('factory');
        $query = $this
                ->db
                ->like('line_name',$search, 'BOTH')
                ->where('factory_id',$factory)
                ->where('DATE(create_date)',$tgl)
                ->get('daily_view');
    
        return $query->num_rows();
    }
    
    function cum_count($header, $tgl)
    {
        $query = $this->db->query("SELECT * from daily_detail where daily_header_id = '$header' and date(create_date)<='$tgl'");
        
        $total = $query->num_rows();

        return $total-1;
    }

    function output_count($date,$line,$style,$activity)
    {
        $query = $this->db->query("SELECT sum(output) as total from line_summary_po
         where date = '$date' and activity_name = '$activity' 
         and line_id = '$line' and style='$style'");
        if ($query->row()==NULL) {
           return 0;
        }else{
            return $query->row();
        }
        
    }

    function output_count_nostyle($date,$line,$activity)
    {
        $query = $this->db->query("SELECT sum(output) as total from line_summary_po
         where date = '$date' and activity_name = '$activity' 
         and line_id = '$line'");

        // $query = $this->db->query("SELECT sum(output) as total, line_id from line_summary_po 
        // where date = '$date' and line_id = '$line' and activity_name = '$activity' 
        // GROUP BY line_id ORDER BY line_id");
        
        return $query->row();
    }

    function output_count_jam($awal, $akhir, $line, $style, $activity)
    {
        $factory   = $this->session->userdata('factory');
        $where = array(
			'line_id'       =>$line,
			'style'         =>$style,
			'activity_name' =>$activity,
			'factory_id'    =>$factory 
        );
        
        $this->db->where($where);
        $this->db->where('jam_awal >=',$awal);
        $this->db->where('jam_akhir <=',$akhir);
        $this->db->select('sum(output) as output');
        $get = $this->db->get('line_summary_po')->row_array()['output'];    			
		return $get;
    }

    function get_daily_id($id)
    {
        $query = $this->db->query("SELECT * FROM daily_view where daily_header_id = '$id' limit 1")->row();

        return $query;
    }

    function eff_count($line_id, $change, $cum)
    {
        $query = $this->db->query("SELECT * from master_line where master_line_id='$line_id'")->row();

        $RL = $query->reqular_line;

        if($RL = 't'){
            $reg = 'RL';
        }
        else{
            $reg = 'SC';
        }

        $query2 = $this->db->query("SELECT * from master_co where line_category='$reg' and category ='$change' and day='$cum'");

        return $query2->row();
    }

    function defect_count($date,$line,$style,$activity)
    {
        $query = $this->db->query("SELECT sum(output) as total from line_summary_po
        where date = '$date' and activity_name = '$activity' 
        and line_id = '$line' and style='$style'");
       
        if ($query->row()==NULL) {
           return 0;
       }else{
            return $query->row();
       }
    }

    function defect_count_nostyle($date,$line,$activity)
    {
        $query = $this->db->query("SELECT sum(output) as total from line_summary_po
        where date = '$date' and activity_name = '$activity' 
        and line_id = '$line'");
       if ($query->row()==NULL) {
           return 0;
       }else{
            return $query->row();
       }
        
    }

    function defect_count_jam($awal, $akhir, $line, $style, $activity)
    {
        $factory   = $this->session->userdata('factory');
        $where = array(
			'line_id'       =>$line,
			'style'         =>$style,
			'activity_name' =>$activity,
			'factory_id'    =>$factory 
		);
		$this->db->where($where);
		$this->db->where('jam_awal >=',$awal);
		$this->db->where('jam_akhir <=',$akhir);
		$this->db->select('sum(output) as output');
		return $this->db->get('line_summary_po')->row_array()['output'];
    }
}
