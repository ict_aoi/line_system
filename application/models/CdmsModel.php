<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CdmsModel extends CI_Model {

	public function getBundleLineValid($barcode_id='')
	{
		$this->cdms->where('barcode_id', $barcode_id);
		return $this->cdms->get('bundle_line_valid_new');
	}
	public function cekDistribusi($poreference,$size,$cutnum,$start,$end,$style,$article)
	{
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where('article', $article);
		$this->db->where('style', $style);
		$this->db->where('poreference', $poreference);
		$this->db->where('size', $size);
		$this->db->where('cut_num', $cutnum);
		$this->db->where('start_num', $start);
		$this->db->where('start_end', $end);
		return $this->db->get('distribusi');
	}
	public function cekBarcode($barcode_id='')
	{
		$this->db->where('line_id', $this->session->userdata('line_id'));
		$this->db->where('delete_at is null');
		$this->db->where('barcode_id', $barcode_id);
		return $this->db->get('distribusi_detail_view');
	}
	public function cekDistribusiDetail($barcode_id)
	{
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where('barcode_id', $barcode_id);
		return $this->db->get('distribusi_detail');
	}
	public function cekComplete($poreference,$size,$cut_num,$start,$end,$style,$article)
	{
		$cond = "'".$style."' ~ CONCAT(E'^',TRIM(style),CASE WHEN set_type='BOTTOM' THEN E'[\\\-\\\_](BOT|BOTTOM)' WHEN set_type='TOP' THEN E'[\\\-\\\_]TOP' WHEN set_type='INNER' THEN E'[\\\-\\\_](INNER|INN)' WHEN set_type='OUTER' THEN E'[\\\-\\\_](OUTER|OUT)' ELSE NULL END,E'$')";
		$this->cdms->where($cond,NULL,FALSE);
		$where = array(
			// 'poreference' => $poreference, 
			'cut_num'     => $cut_num, 
			'start_num'   => $start, 
			'end_num'     => $end, 
			'article'     => $article, 
			'size'        => $size 
		);
		$this->cdms->where($where);
		// $quer = "poreference = '$poreference' OR po_reroute = '$poreference'";
		// $this->cdms->where($quer,NULL,FALSE);
		$this->cdms->where('poreference', $poreference);
		return $this->cdms->get('line_syst_upd_new');
	}
	public function getDistribusi($poreference,$size,$cut_num,$start,$end,$style,$article)
	{

		$where = array(
			'poreference' => $poreference, 
			'cut_num'     => $cut_num, 
			'start_num'   => $start, 
			'start_end'   => $end, 
			'style'       => $style,
			'article'     => $article,  
			'size'        => $size 
		);
		$this->db->where('delete_at is null', NULL,FALSE);
		$this->db->where($where);
		return $this->db->get('distribusi');
	}
	public function getHeader($poreference,$style,$article)
	{
		$this->db->where('isactive', 't');
		$this->db->where('line_id', $this->session->userdata('line_id'));
		$this->db->where('poreference', $poreference);
		$this->db->where('article', $article);
		$this->db->where('style', $style);
		return $this->db->get('inline_header');
	}
	public function getHeaderSize($po,$style,$size,$header_id)
	{
		$this->db->where('size', $size);
		$this->db->where('poreference', $po);
		$this->db->where('style', $style);
		$this->db->where('inline_header_id', $header_id);
		return $this->db->get('inline_header_size');
	}
	public function getDetailBundle($poreference,$size,$start_num,$start_end,$cut_num,$style,$article)
	{
		$cond = "'".$style."' ~ CONCAT(E'^',TRIM(style),CASE WHEN set_type='BOTTOM' THEN E'[\\\-\\\_](BOT|BOTTOM)' WHEN set_type='TOP' THEN E'[\\\-\\\_]TOP' WHEN set_type='INNER' THEN E'[\\\-\\\_](INNER|INN)' WHEN set_type='OUTER' THEN E'[\\\-\\\_](OUTER|OUT)' ELSE NULL END,E'$')";
		$this->cdms->where($cond,NULL,FALSE);
		$this->cdms->where('cut_num', $cut_num);
		$this->cdms->where('poreference', $poreference);
		// $quer = "poreference = '$poreference' OR po_reroute = '$poreference'";
		// $this->cdms->where($quer,NULL,FALSE);
		$this->cdms->where('article', $article);
		$this->cdms->where('size', $size);
		$this->cdms->where('start_num', $start_num);
		$this->cdms->where('end_num', $start_end);
		return $this->cdms->get('bundle_line_valid_new');
	}
	public function cekAvailable($poreference,$style,$size,$article)
	{
		$where = array(
			'line_id'     =>$this->session->userdata('line_id'),
			'poreference' => $poreference, 
			'style'       => $style, 
			'article'     => $article, 
			'size'        => $size 
		);
		$this->db->where($where);
		return $this->db->get('statuspo');
	}
	public function cekStatusHandOver($distribusi_id)
	{
		$this->db->where('distribusi_id', $distribusi_id);
		$all = $this->db->get('distribusi_detail_view')->num_rows();

		$this->db->where('hand_over', 't');
		$this->db->where('distribusi_id', $distribusi_id);
		$hand_over = $this->db->get('distribusi_detail_view')->num_rows();

		if ($all==$hand_over) {
			return 'completed';
		}else{
			return 'on progress';
		}
	}
	
	function posts_search_distributionsummary($line,$factory,$po=null,$size=null)
    {
        // $this->db->group_start();
        // $this->db->group_end();
        if($size!=null){
            $this->db->where('size',$size);
            
        }
        if($po!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($po), 'BOTH');
        }
        // $this->db->where('date',$date);
		$this->db->where('factory_id',$factory);
		$this->db->where('line_id',$line);
		$this->db->group_by('line_id, factory_id, line_name, style, poreference, size, article, status');
		$this->db->select('line_id, factory_id, line_name, style, poreference, size, article, status, sum(total)');
        $query = $this->db->get('summary_distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_distributionsummary($line,$factory,$po=null,$size=null)
    {

        // $this->db->like('line_name',strtoupper($search), 'BOTH');
        // $this->db->where('factory_id',$factory);
        if($size!=null){
            $this->db->where('size',$size);
            
        }
        if($po!=null){
            // $this->db->where('poreference',$pobuyer);
            $this->db->like('poreference',strtoupper($po), 'BOTH');
		}
		$this->db->where('factory_id',$factory);
		$this->db->where('line_id',$line);
		$this->db->group_by('line_id, factory_id, line_name, style, poreference, size, article, status');
		$this->db->select('line_id, factory_id, line_name, style, poreference, size, article, status, sum(total)');
        $query = $this->db->get('summary_distribusi');

        return $query->num_rows();
    }

	function allposts_count_receive($line_id,$factory)
    {
        $this->db->where('line_id', $line_id);
        $this->db->where('factory_id', $factory);
        $query = $this->db->get('distribusi');

        return $query->num_rows();
    }

    function allposts_receive($limit,$start,$col,$dir,$line_id,$factory)
    {

        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        $this->db->where('line_id', $line_id);
        $this->db->where('factory_id', $factory);        
        $query = $this->db->get('distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

    function posts_search_receive($limit,$start,$search,$col,$dir,$line_id,$factory,$date=NULL,$poreference=NULL,$pilih=NULL)
    {
     
        $this->db->limit($limit,$start);
        $this->db->order_by($col,$dir);
        if ($date!=NULL) {
           $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
        }    
        if ($poreference!=NULL) {
            $this->db->like('poreference', $poreference, 'BOTH');
        }
        if ($pilih!=NULL) {
            $this->db->where('size', $pilih);
        }    
        $this->db->where('line_id', $line_id);
        $this->db->where('factory_id', $factory);        
        $query = $this->db->get('distribusi');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_receive($search,$line,$factory,$date=NULL,$poreference=NULL,$pilih=NULL)
    {

        if ($date!=NULL) {
           $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
        }    
        if ($poreference!=NULL) {
            $this->db->like('poreference', $poreference, 'BOTH');
        }if ($pilih!=NULL) {
            $this->db->where('size', $pilih);
        }     
        $this->db->where('line_id', $line);
        $this->db->where('factory_id', $factory);        
        $query = $this->db->get('distribusi');

        return $query->num_rows();
    }
    public function getQtyReceive($poreference,$size,$style,$article)
    {
    	$this->db->where('status', 'completed');
    	$this->db->where('poreference', $poreference);
    	$this->db->where('size', $size);
    	$this->db->where('article', $article);
    	$this->db->where('style', $style);
    	$this->db->select('sum(qty) as qty');
    	return $this->db->get('distribusi')->row_array()['qty'];
    }
    public function getQtyBeda($line_id)
    {
    	$sql ="SELECT
					SUM (di.qty) total_receive,
					ihz.qty,
					di.poreference,
					di.size,
					di.style,
					di.article,
					di.line_id,
					di.factory_id,
					di.inline_header_id
				FROM
					distribusi di
				LEFT JOIN inline_header_size ihz on ihz.inline_header_id=di.inline_header_id and ihz.poreference=di.poreference and ihz.size=di.size
				WHERE
					(di.status='completed' or di.status='packing' or di.status='subcont') and di.line_id=$line_id and delete_at is null
				GROUP BY
				  di.poreference,ihz.qty,
					di.size,
					di.style,
					di.line_id,
					di.article,
					di.factory_id, 
					di.inline_header_id
				HAVING SUM(di.qty)<>COALESCE(ihz.qty,0)";
		return $this->db->query($sql);

    }
    public function cekPanelLain($poreference='',$size='',$style='')
    {
    	$this->db->where('delete_at is null', NULL,FALSE);
    	return $this->db->get('distribusi_detail_view');
    }
    public function getTempPindah($line_id='',$barcode_id='')
    {
    	if ($line_id!='') {
    		$this->db->where('line_id', $line_id);
    	}
    	if ($barcode_id!='') {
    		$this->db->where('barcode_id', $barcode_id);
    	}
    	return $this->db->get('temp_distribusi_pindah');
    }
   
    public function getTemp($poreference,$size,$cutnum,$start,$end,$style,$article)
	{
	
		$where = array(
			'style'       => $style, 
			'poreference' => $poreference, 
			'size'        => $size, 
			'cut_num'     => $cutnum, 
			'start_num'   => $start, 
			'article'     => $article, 
			'start_end'   => $end
		);
		
		$this->db->where($where);
		$this->db->select('style,poreference,size,cut_num,start_num,start_end,article');
   		$this->db->distinct();
		return $this->db->get('temp_distribusi_pindah');
		
		
	}
	public function getAvailable($poreference,$size,$style,$qtymove,$article)
	{
		$line_id = $this->session->userdata('line_id');
		$where = array(
			'style'       => $style, 
			'poreference' => $poreference, 
			'line_id'     => $line_id, 
			'article'     => $article, 
			'size'        => $size
		);
		
		$this->db->where($where);
		return $this->db->get('qc_output')->row_array();
		
		// return $data;

	}
	function getHeaders($poreference,$article)
	{
		$line_id = $this->session->userdata('line_id');

		$this->db->where(array('poreference'=>$poreference,'line_id'=>$line_id,'isactive'=>'t','article'=>$article));
		return $this->db->get('inline_header');
	}
	public function getLinePlaning($factory='',$poreference)
	{
		$query = "select string_agg(replace(line,'$factory',''), ', ') as line from ppcm_upload where poreference='$poreference' and deleted_at is NULL";
		return $this->cdms->query($query);

	}
	public function cekPindah($poreference,$size,$cutnum,$start,$end,$style,$article)
	{
		return $this->db
		->where('style',$style)
		->where('poreference',$poreference)
		->where('size',$size)
		->where('article',$article)
		->where('cut_num',$cutnum)
		->where('start_num',$start)
		->where('start_end',$end)
		->get('distribusi_detail_view');
	}

	public function cekBundlePindah($poreference,$size,$cutnum,$start,$end,$style,$article)
	{
		$this->db->where('delete_at is null');
		$this->db->where('article', $article);
		$this->db->where('style', $style);
		$this->db->where('poreference', $poreference);
		$this->db->where('size', $size);
		$this->db->where('cut_num', $cutnum);
		$this->db->where('start_num', $start);
		$this->db->where('start_end', $end);
		return $this->db->get('distribusi');
	}

}

/* End of file DistribusiModel.php */
/* Location: ./application/models/DistribusiModel.php */
