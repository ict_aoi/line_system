<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_model extends CI_Model {

	public function get_detail($poreference)
	{
		$query = "SELECT DISTINCT
					DATE (so.kst_lcdate) AS kst_lcdate,
					so.kst_joborder,
					so.documentno,
					so.poreference,
					mp.kst_articleno,
					so.datepromised,
					mp.kst_name,
					sz.
				VALUE
					AS SIZE,
					sol.qtyordered,
					sol.qtydelivered,
					DATE (so.kst_statisticaldate) AS kst_statisticaldate,
					fmr. STYLE,
					(
						sol.qtyordered - sol.qtydelivered
					) AS balance,so.c_doctypetarget_id,bp.name as bp,mp.value as product,loc.name as location, mpc.value
					
				FROM
					c_order so
				LEFT JOIN c_orderline sol ON sol.c_order_id = so.c_order_id
				LEFT JOIN m_product mp ON mp.m_product_id = sol.m_product_id
				LEFT JOIN kst_sourcesize sz ON sz.kst_sourcesize_id = mp.kst_sourcesize_id
				LEFT JOIN f_material_requirement fmr ON fmr.poreference = so.poreference
				left join c_bpartner bp on bp.c_bpartner_id = so.c_bpartner_id
				left join C_BPartner_Location loc on loc.c_bpartner_id = bp.c_bpartner_id
				LEFT JOIN m_product_category mpc ON mpc.m_product_category_id = mp.m_product_category_id
				WHERE
					1 = 1
				AND so.issotrx = 'Y' :: bpchar
				AND so.c_doctypetarget_id in(1000121,1000051,1000165,1000168,1000172,1000163,1000082,1000247,1000248,1000085,1000335)
				AND fmr. STYLE IS NOT NULL
				and sz.VALUE is not null
				-- AND so.kst_statisticaldate IS NOT NULL
				AND sol.qtyordered > 0
				-- AND sol.qtydelivered < sol.qtyordered
				AND so.poreference = '$poreference'
				
				ORDER BY size";

		return $this->db2->query($query);
	}
	public function SumSewing($factory,$awal,$akhir)
	{
		// $this->db->group_by('date(create_date),line_id,style,sewing_pcs.factory_id,master_line.line_name');
		// $this->db->where('create_date >=',$awal);
		// $this->db->where('create_dates <',$akhir);
		// $this->db->where('sewing_pcs.factory_id',$factory);
		// $this->db->join('master_line', 'master_line.master_line_id = sewing_pcs.line_id', 'left');
		// $this->db->select('date(create_date),line_id,master_line.line_name,style,sewing_pcs.factory_id,count(*)');
		// return $this->db->get('sewing_pcs');

		
		$this->db->group_by('date(create_date),line_id,style,sewing_pcs.factory_id,master_line.line_name');
		$this->db->where('create_date >=',$awal);
		$this->db->where('create_date <',$akhir);
		$this->db->where('sewing_pcs.factory_id',$factory);
		$this->db->join('master_line', 'master_line.master_line_id = sewing_pcs.line_id', 'left');
		$this->db->select('date(create_date),line_id,master_line.line_name,style,sewing_pcs.factory_id,count(*)');
		return $this->db->get('sewing_pcs');
	}
	public function cekLineSummary($jam_awal,$jam_akhir,$line_id,$factory_id,$name,$style=NULL,$poreference=NULL,$size=NULL)
	{
		if ($poreference!=NULL&&$size!=NULL) {
			$this->db->where('poreference', $poreference);
			$this->db->where('size', $size);
		}
		if ($style!=NULL) {
			$this->db->where('style', $style);
		}
		$this->db->where('activity_name', $name);
		$this->db->where('jam_awal', $jam_awal);
		$this->db->where('jam_akhir', $jam_akhir);
		$this->db->where('line_id', $line_id);
		$this->db->where('factory_id', $factory_id);
		return $this->db->get('line_summary_po');
	}
	public function sumQcendlineOut($awal,$akhir)
	{
		$sql ="SELECT DISTINCT
					a.line_id,
					a.factory_id,
					b.line_name,
					tot_endline.total_good_endline,
					tot_endline.date
				FROM
					inline_header a
				LEFT JOIN  master_line b on b.master_line_id=a.line_id
				JOIN(SELECT
					DATE (qet.create_date) AS DATE,
					COUNT (qet.counter_pcs) AS total_good_endline,qet.factory_id,
					ih.line_id
				FROM
					qc_endline_trans qet
				JOIN inline_header ih on ih.inline_header_id=qet.inline_header_id
				WHERE
				-- qet.create_date between '$awal' and '$akhir'
					qet.create_date >= '$awal'
				and qet.create_date < '$akhir'
				and qet.counter_pcs<>0
				GROUP BY DATE (qet.create_date),qet.factory_id,ih.line_id) tot_endline on tot_endline.line_id=a.line_id";
		return $this->db->query($sql);
	}
	
	public function sumFolding($factory,$awal,$akhir)
	{
		$sql = "SELECT
					count(*),
					b.line_id,date(A.create_date),
					b.poreference,b.article,A.size,A.style,ml.line_name,
					A .factory_id
				FROM
					folding A
				JOIN inline_header b ON b.inline_header_id = A .inline_header_id
				LEFT JOIN master_line ml on ml.master_line_id=b.line_id
				WHERE
				
				-- A.create_date between '$awal' and '$akhir'
					A.create_date >= '$awal'
				and A.create_date < '$akhir'
				and A.factory_id = '$factory'
				GROUP BY b.line_id,date(A.create_date),
					A .factory_id,b.poreference,A.size,A.style,ml.line_name,b.article
				ORDER BY line_id";
		return $this->db->query($sql);
	}
	public function sumDefectEndline($factory,$awal,$akhir)
	{

		$sql = "WITH endline_int AS(
					SELECT DISTINCT
						qed.qc_endline_trans_id,
						count(qed.defect_id) AS end_defect,
						qed.style,
						qed.factory_id,
						ih.line_id,
						ih.article,
						DATE(qed.create_date)
					FROM
						qc_endline_defect qed 
					JOIN inline_header ih on ih.style=qed.style and ih.inline_header_id=qed.inline_header_id
					WHERE
					
					-- qed.create_date between '$awal' and '$akhir'
					qed.create_date >= '$awal'
				  	and qed.create_date < '$akhir'
					  and qed.factory_id = '$factory'
					GROUP BY  qed.qc_endline_trans_id,DATE(qed.create_date),qed.style,qed.factory_id,ih.line_id,ih.article
					)
					 SELECT count(endline_int.end_defect) AS defect_endline,
					    endline_int.date,
					    endline_int.style,
							endline_int.line_id,
							endline_int.article,
							endline_int.factory_id,ml.line_name
					   FROM endline_int
					   LEFT JOIN master_line ml on ml.master_line_id=endline_int.line_id
					  GROUP BY endline_int.date, endline_int.style,endline_int.factory_id,endline_int.line_id,ml.line_name,
					  endline_int.article
					  ORDER BY endline_int.date";
		return $this->db->query($sql);
	}
	public function sumDefectInline($factory,$awal,$akhir)
	{
		$sql ="SELECT count(a.defect_id) AS defect_inline,
				    date(a.create_date) AS date,
				    b.style,
						a.factory_id,
				    c.line_name,
						c.master_line_id
				   FROM ((inline_detail a
				     LEFT JOIN inline_header b ON (((b.inline_header_id)::text = (a.inline_header_id)::text)))
				     LEFT JOIN master_line c ON ((c.master_line_id = b.line_id)))
				  WHERE
				--  a.create_date between '$awal' and '$akhir' 
				  a.create_date >= '$awal'
				  and a.create_date < '$akhir'
				  and a.defect_id<>0
				  and a.factory_id = '$factory'
				  GROUP BY (date(a.create_date)), b.style, c.line_name,c.master_line_id,a.factory_id
				  ORDER BY (date(a.create_date))";
		return $this->db->query($sql);
	}
	public function sumQcEndlinePerPO($factory,$awal,$akhir)
    {
        $sql = "SELECT qet.inline_header_id,
			    qet.qc_endline_id,
			    qe.header_size_id,
			    DATE (qet.create_date) AS create_date,
			    ih.line_id,
					ml.line_name,
					ih.poreference,
					ih.article,
					ihz.size,
			    qe.style,
			    count(qet.counter_pcs) AS daily_output,
			    qet.factory_id,
			    ml.no_urut
			   FROM qc_endline_trans qet
			     LEFT JOIN inline_header ih ON ih.inline_header_id::text = qet.inline_header_id::text
			     LEFT JOIN qc_endline qe ON qe.qc_endline_id::text = qet.qc_endline_id::text
			     LEFT JOIN inline_header_size ihz ON ihz.header_size_id::text = qe.header_size_id::text
			     LEFT JOIN master_line ml ON ml.master_line_id = ih.line_id
			     WHERE qet.counter_pcs <> 0
				--  and qet.create_date between '$awal' and '$akhir'
				 AND qet.create_date >= '$awal'
				 AND qet.create_date < '$akhir'
				 AND qet.factory_id = '$factory'
			  GROUP BY (date(qet.create_date)), qet.inline_header_id, ih.line_id, ihz.qty, qet.qc_endline_id, qe.header_size_id, qe.style, qe.counter_qc, qet.factory_id, ml.no_urut,ml.line_name,
					ih.poreference,ih.article,
					ihz.size
			  ORDER BY (date(qet.create_date)), ml.no_urut,qet.factory_id";
        return $this->db->query($sql);
    }
    public function cekBeda()
    {
    	$sql = "select style,line_id,count(*),max(counter_pcs) from sewing_pcs
				GROUP BY style,line_id
				HAVING  count(*)<>max(counter_pcs)";
		return $this->db->query($sql);
    }
    public function cekBedaFolding()
    {
    	$sql = "SELECT
					ih.inline_header_id,
					ih.line_id,
					fo.style,
					fo.poreference,
					fo.size,
					COUNT (*),
					MAX (counter_folding)
				FROM
					folding fo
				LEFT JOIN inline_header ih on ih.inline_header_id=fo.inline_header_id
				GROUP BY
					ih.inline_header_id,
					ih.line_id,
					fo.style,
					fo.poreference,
					fo.size
				HAVING
					COUNT (*) <> MAX (counter_folding)";
		return $this->db->query($sql);
	}
	
	//========================= PO BUYER =======================
	
	function allposts_po_count()
    {   
        $query = $this
                ->db
                ->get('po_buyers');
    
        return $query->num_rows();  

    }
    
    function allposts_po($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,'desc')
                ->get('po_buyers');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
	}
	
	function posts_po_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('poreference',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('po_buyers');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_po_search_count($search)
    {
        $query = $this
                ->db
                ->like('poreference',$search)
                ->get('po_buyers');
    
        return $query->num_rows();
	}
	
	function getDistribusi($po)
	{
		$query = $this->db->query("SELECT style,poreference,size,status,sum(qty) as total FROM 
		distribusi WHERE poreference = '$po' GROUP BY poreference,style,size,status ORDER BY size");
		return $query->result();

		// $this->db->group_by('poreference,style,size,status');
		// $this->db->where('poreference','$po');
		// $this->db->select('style,poreference,size,status,sum(qty) as total');
		// return $this->db->get('distribusi');

		// return $query->result();
	}

	function reduce_po($po)
	{
		$this->db->query("UPDATE po_buyers set requirement_date  = null where poreference = '$po'");

		// $this->db->query("DELETE from master_order where poreference = '$po'");
	}

	function cancel_po($po)
	{
		// $this->db->query("DELETE from master_order where poreference = '$po'");
		
		$this->db->query("UPDATE po_buyers set delete_date  = '".date('Y-m-d H:i:s')."' where poreference = '$po'");
	}

	function update_po_buyers($po)
	{
		$this->db->query("UPDATE po_buyers set update_date  = '".date('Y-m-d H:i:s')."' where poreference = '$po'");
	}

	function update_po_baru($po_lama,$po_baru)
	{
		// $this->db->query("DELETE from master_order where poreference = '$po_lama'");

		$this->db->query("UPDATE po_buyers set po_new = '$po_baru' where poreference = '$po_lama'");

		$this->db->query("UPDATE master_order set delete_date  = '".date('Y-m-d H:i:s')."' where poreference = '$po_lama'");
	}
	public function getCounterBeda()
    {
    	$query ="SELECT 
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,COALESCE(counter.count,0) as counter_alocation,fh.counter
					 from folding_header fh
					LEFT JOIN (
					select inline_header_id,style,poreference,size,count(*) from folding
					where status_folding='allocation' 
					GROUP BY 
					inline_header_id,style,poreference,size
					) counter on counter.inline_header_id=fh.inline_header_id and counter.size=fh.size
					GROUP BY
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,counter.count,fh.counter
					HAVING COALESCE(counter.count,0)<>fh.counter";
		return $this->db->query($query);
    }
    public function getCounterBedasets()
    {
    	$query ="SELECT 
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,COALESCE(counter.count,0) as counter_alocation,fh.counter_sets
					 from folding_header fh
					LEFT JOIN (
					select inline_header_id,style,poreference,size,count(*) from folding
					where status_folding='unallocation' 
					GROUP BY 
					inline_header_id,style,poreference,size
					) counter on counter.inline_header_id=fh.inline_header_id and counter.size=fh.size

					GROUP BY
					fh.inline_header_id,fh.id,
					fh.poreference,
					fh.style,
					fh.size,counter.count,fh.counter_sets
					HAVING COALESCE(counter.count,0)<>fh.counter_sets";
		return $this->db->query($query);
    }

}

/* End of file Cron_model.php */
/* Location: ./application/models/Cron_model.php */
