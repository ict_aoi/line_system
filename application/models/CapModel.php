<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CapModel extends CI_Model {


public function cap_endline($line_id,$style)
{
	 $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
	 return $this->db->get('cap_endline');
}
public function countDefectIntv($line_id,$style)
{
	 $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
	 $this->db->where(array('line_id'=> $line_id,'style'=>$style));
	 return $this->db->get('cap_endline');
}
/*start datatable serverside wft*/
    function allposts_count_wft($fromline=NULL,$toline=NULL)
    {   

        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');
        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
        $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_endline_view');

        return $query->num_rows();  

    }

    function allposts_wft($limit,$start,$col,$dir,$fromline=NULL,$toline=NULL)
    {   
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');
        $this->db->limit($limit,$start);
        // $this->db->order_by($col, $dir);
        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
        $this->db->where(array('factory_id'=>$factory,'date'=>$date));
        $query = $this->db->get('cap_endline_view');

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }

    function posts_search_wft($limit,$start,$search=NULL,$col,$dir,$fromline=NULL,$toline=NULL)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        $this->db->order_by($col, $dir);
        if ($search!=NULL) {
            $this->db->like('cast(no_urut)', $search, 'BOTH');
        }
        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
         $this->db->where('date', $date);
        $this->db->where(array('factory_id'=>$factory));
        $query = $this->db->get('cap_endline_view');

        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_wft($search,$date,$fromline=NULL,$toline=NULL)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');

        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
        $this->db->where('date', $date);
        $this->db->where(array('factory_id'=>$factory));
        $query = $this->db->get('cap_endline_view');

        return $query->num_rows();
    }
/*end datatable serverside wft*/

/*start data table serverside cap inline*/
  function allposts_count_capinline($fromline=NULL,$toline=NULL)
    {   

        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');

        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
         $this->db->where('date', $date);
        $this->db->where(array('factory_id'=>$factory));
        $query = $this->db->get('cap_inline_view_new');

        return $query->num_rows();  

    }

    function allposts_capinline($limit,$start,$col,$dir,$fromline=NULL,$toline=NULL)
    {   
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        $this->db->order_by($col, $dir);
        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
        $this->db->where(array('factory_id'=>$factory,'date'=>$date));
        $query = $this->db->get('cap_inline_view_new');

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }

    function posts_search_capinline($limit,$start,$search=NULL,$col,$dir,$fromline=0,$toline=0)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');

        $this->db->limit($limit,$start);
        // $this->db->order_by($col, $dir);
        if ($search!=NULL) {
            $this->db->like('cast(no_urut as text)', $search, 'BOTH');
        }
        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
         $this->db->where('date', $date);
        $this->db->where(array('factory_id'=>$factory));
        $query = $this->db->get('cap_inline_view_new');

        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_capinline($search,$fromline=NULL,$toline=NULL)
    {
        $date = date('Y-m-d');
        $factory = $this->session->userdata('factory');
       
        if ($search!=NULL) {
            $this->db->like('cast(no_urut as text)', $search, 'BOTH');
        }
        if ($fromline!=NULL&&$toline!=NULL) {
            $this->db->where('no_urut >=', $fromline);
            $this->db->where('no_urut <=', $toline);
        }
        $this->db->where('date', $date);
        $this->db->where(array('factory_id'=>$factory));
        $query = $this->db->get('cap_inline_view_new');

        return $query->num_rows();
    }
/*end data table serverside cap inline*/

    // list verify qc
    function allposts_count_verify($date,$factory,$nik)
    {   

        $this->db->where('corective_action_date is not null', NULL,FALSE);
        $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_endline_view');
    
        return $query->num_rows();  

    }
    
    function allposts_verify($limit,$start,$col,$dir,$date,$factory,$nik)
    {   
        $this->db->limit($limit,$start);
        $this->db->order_by($col, $dir);
        $this->db->where('corective_action_date is not null', NULL,FALSE);
        $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_endline_view');

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_verify($limit,$start,$search,$col,$dir,$date,$factory,$nik)
    {
        $this->db->limit($limit,$start);
        $this->db->order_by($col, $dir);
        $this->db->like('cast(line_id)', $search, 'BOTH');
        $this->db->where('corective_action_date is not null', NULL,FALSE);
        $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_endline_view');

        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_verify($search,$date,$factory,$nik)
    {
        $this->db->like('cast(line_id)', $search, 'BOTH');
        $this->db->where('corective_action_date is not null', NULL,FALSE);
         $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_endline_view');

        return $query->num_rows();
    }
// end list verify qc
    // start list verify cap inline

    function allposts_count_verify_inline($date,$factory,$nik)
    {   

        $this->db->where('corective_action_date is not null', NULL,FALSE);
        $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_inline_view_new');
    
        return $query->num_rows();  

    }
    
    function allposts_verify_inline($limit,$start,$col,$dir,$date,$factory,$nik)
    {   
        $this->db->limit($limit,$start);
        $this->db->order_by($col, $dir);
        $this->db->where('corective_action_date is not null', NULL,FALSE);
        $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_inline_view_new');

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_verify_inline($limit,$start,$search,$col,$dir,$date,$factory,$nik)
    {
        $this->db->limit($limit,$start);
        $this->db->order_by($col, $dir);
        // $this->db->like('cast(line_id)', $search, 'BOTH');
        $this->db->like('sewer_name', $search, 'BOTH');
        $this->db->where('corective_action_date is not null', NULL,FALSE);
        $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_inline_view_new');

        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_verify_inline($search,$date,$factory,$nik)
    {
        // $this->db->like('cast(line_id)', $search, 'BOTH');
        $this->db->like('sewer_name', $search, 'BOTH');
        $this->db->where('corective_action_date is not null', NULL,FALSE);
         $this->db->where(array('date'=> $date,'factory_id'=>$factory/*,'spv_qc'=>$nik*/));
        $query = $this->db->get('cap_inline_view_new');

        return $query->num_rows();
    }
    // end list verify cap inline
    public function maxround($line_id)
    {
        $date = date('Y-m-d');
        $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
        $this->db->where('line_id', $line_id);
        $this->db->select_max('round');
        return $this->db->get('cap_inline')->result();
    }
    public function getLine()
    {
        $factory = $this->session->userdata('factory');
        $this->db->order_by('no_urut', 'asc');
        $this->db->where('factory_id', $factory);
        $this->db->where('active', 't');
        return $this->db->get('master_line');
    }

    //summary report cap inline
    function allposts_count_summary_inline($date1,$date2)
    {   
        $factory = $this->session->userdata('factory');

        $query = $this->db->query("SELECT civ.cap_inline_id, civ.line_id, civ.factory_id, civ.line_name, 
        civ.round, civ.sewer_nik, civ.sewer_name, civ.qc_name, civ.proses_name, civ.date, 
        civ.corective_action_date, civ.verify_date, civ.create_date, civ.status, civ.max, civ.defect_jenis,
        civ.no_urut, cit.major_couse, cit.root_couse, cit.action, cit.create_by as dibuat_oleh, cit.create_date as dibuat_kapan,
        ms.name as operator from cap_inline_view civ
        LEFT JOIN cap_inline_trans cit on cit.cap_inline_id = civ.cap_inline_id
        LEFT JOIN master_sewer ms on ms.nik = cit.create_by
        where civ.date <= '$date2' and civ.date >= '$date1' and civ.factory_id = '$factory'");
        
        return $query->num_rows();  
    }
    
    function allposts_summary_inline($limit,$start,$col,$dir,$date1,$date2)
    {   
        $factory = $this->session->userdata('factory');

        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){

            $query = $this->db->query("SELECT civ.cap_inline_id, civ.line_id, civ.factory_id, civ.line_name, 
            civ.round, civ.sewer_nik, civ.sewer_name, civ.qc_name, civ.proses_name, civ.date, 
            civ.corective_action_date, civ.verify_date, civ.create_date, civ.status, civ.max, civ.defect_jenis,
            civ.no_urut, cit.major_couse, cit.root_couse, cit.action, cit.create_by as dibuat_oleh, cit.create_date as dibuat_kapan, 
            ms.name as operator from cap_inline_view civ
            LEFT JOIN cap_inline_trans cit on cit.cap_inline_id = civ.cap_inline_id
            LEFT JOIN master_sewer ms on ms.nik = cit.create_by
            where civ.date <= '$date2' and civ.date >= '$date1' and civ.factory_id = '$factory'
            -- order by $col $dir 
            ORDER BY civ.date, civ.no_urut, civ.round
            limit $limit offset $start");
        }
        else{
            $query = $this->db->query("SELECT civ.cap_inline_id, civ.line_id, civ.factory_id, civ.line_name, 
            civ.round, civ.sewer_nik, civ.sewer_name, civ.qc_name, civ.proses_name, civ.date, 
            civ.corective_action_date, civ.verify_date, civ.create_date, civ.status, civ.max, civ.defect_jenis,
            civ.no_urut, cit.major_couse, cit.root_couse, cit.action, cit.create_by as dibuat_oleh, cit.create_date as dibuat_kapan, 
            ms.name as operator from cap_inline_view civ
            LEFT JOIN cap_inline_trans cit on cit.cap_inline_id = civ.cap_inline_id
            LEFT JOIN master_sewer ms on ms.nik = cit.create_by
            where civ.date <= '$date2' and civ.date >= '$date1' and civ.factory_id = '$factory'
            order by civ.no_urut");
        }
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
    }
   
    function posts_search_summary_inline($limit,$start,$search,$col,$dir,$date1,$date2)
    {
        $factory = $this->session->userdata('factory');
        $kata = strtoupper($search);

        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){

            $query = $this->db->query("SELECT civ.cap_inline_id, civ.line_id, civ.factory_id, civ.line_name, 
            civ.round, civ.sewer_nik, civ.sewer_name, civ.qc_name, civ.proses_name, civ.date, 
            civ.corective_action_date, civ.verify_date, civ.create_date, civ.status, civ.max, civ.defect_jenis,
            civ.no_urut, cit.major_couse, cit.root_couse, cit.action, cit.create_by as dibuat_oleh, cit.create_date as dibuat_kapan,
            ms.name as operator from cap_inline_view civ
            LEFT JOIN cap_inline_trans cit on cit.cap_inline_id = civ.cap_inline_id
            LEFT JOIN master_sewer ms on ms.nik = cit.create_by
            where civ.date <= '$date2' and civ.date >= '$date1'and civ.factory_id = '$factory'
            and civ.line_name like '%$kata%'
            -- order by $col $dir limit $limit 
            ORDER BY civ.date, civ.no_urut, civ.round
            offset $start");
        }
        else{
            $query = $this->db->query("SELECT civ.cap_inline_id, civ.line_id, civ.factory_id, civ.line_name, 
            civ.round, civ.sewer_nik, civ.sewer_name, civ.qc_name, civ.proses_name, civ.date, 
            civ.corective_action_date, civ.verify_date, civ.create_date, civ.status, civ.max, civ.defect_jenis,
            civ.no_urut, cit.major_couse, cit.root_couse, cit.action, cit.create_by as dibuat_oleh, cit.create_date as dibuat_kapan,
            ms.name as operator from cap_inline_view civ
            LEFT JOIN cap_inline_trans cit on cit.cap_inline_id = civ.cap_inline_id
            LEFT JOIN master_sewer ms on ms.nik = cit.create_by
            where civ.date <= '$date2' and civ.date >= '$date1' and civ.factory_id = '$factory'
            and civ.line_name like '%$kata%'
            order by civ.no_urut");
        }

        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_summary_inline($search,$date1,$date2)
    {
        $factory = $this->session->userdata('factory');
        $kata = strtoupper($search);

        $query = $this->db->query("SELECT civ.cap_inline_id, civ.line_id, civ.factory_id, civ.line_name, 
        civ.round, civ.sewer_nik, civ.sewer_name, civ.qc_name, civ.proses_name, civ.date, 
        civ.corective_action_date, civ.verify_date, civ.create_date, civ.status, civ.max, civ.defect_jenis,
        civ.no_urut, cit.major_couse, cit.root_couse, cit.action, cit.create_by as dibuat_oleh, cit.create_date as dibuat_kapan,
        ms.name as operator from cap_inline_view civ
        LEFT JOIN cap_inline_trans cit on cit.cap_inline_id = civ.cap_inline_id
        LEFT JOIN master_sewer ms on ms.nik = cit.create_by
        where civ.date <= '$date2' and civ.date >= '$date1' and civ.factory_id = '$factory'
        and civ.line_name like '%$kata%'");

        return $query->num_rows();
    }

    //summary report cap endline
    function allposts_count_summary_endline($date1,$date2)
    {   
        $factory = $this->session->userdata('factory');

        $query = $this->db->query("SELECT cev.*, cet.major_couse, cet.root_couse, 
        cet.action, cet.create_by as dibuat_oleh, cet.create_date as dibuat_kapan,
        ms.name as operator from cap_endline_view cev
        LEFT JOIN cap_endline_trans cet on cet.cap_endline_detail_id = cev.cap_endline_detail_id
        LEFT JOIN master_sewer ms on ms.nik = cet.create_by
        where cev.date <= '$date2' and cev.date >= '$date1' and cev.factory_id = '$factory'");
        
        return $query->num_rows();  
    }
    
    function allposts_summary_endline($limit,$start,$col,$dir,$date1,$date2)
    {   
        $factory = $this->session->userdata('factory');

        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){

            $query = $this->db->query("SELECT cev.*, cet.major_couse, cet.root_couse, 
            cet.action, cet.create_by as dibuat_oleh, cet.create_date as dibuat_kapan,
            ms.name as operator from cap_endline_view cev
            LEFT JOIN cap_endline_trans cet on cet.cap_endline_detail_id = cev.cap_endline_detail_id
            LEFT JOIN master_sewer ms on ms.nik = cet.create_by
            where cev.date <= '$date2' and cev.date >= '$date1' and cev.factory_id = '$factory'
            -- order by $col $dir 
				ORDER BY cev.date, cev.no_urut, cev.jam
            limit $limit offset $start");
        }
        else{
            $query = $this->db->query("SELECT cev.*, cet.major_couse, cet.root_couse, 
            cet.action, cet.create_by as dibuat_oleh, cet.create_date as dibuat_kapan,
            ms.name as operator from cap_endline_view cev
            LEFT JOIN cap_endline_trans cet on cet.cap_endline_detail_id = cev.cap_endline_detail_id
            LEFT JOIN master_sewer ms on ms.nik = cet.create_by
            where cev.date <= '$date2' and cev.date >= '$date1' and cev.factory_id = '$factory'
            order by cev.no_urut");
        }
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
    }
   
    function posts_search_summary_endline($limit,$start,$search,$col,$dir,$date1,$date2)
    {
        $factory = $this->session->userdata('factory');
        $kata = strtoupper($search);

        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){

            $query = $this->db->query("SELECT cev.*, cet.major_couse, cet.root_couse, 
            cet.action, cet.create_by as dibuat_oleh, cet.create_date as dibuat_kapan,
            ms.name as operator from cap_endline_view cev
            LEFT JOIN cap_endline_trans cet on cet.cap_endline_detail_id = cev.cap_endline_detail_id
            LEFT JOIN master_sewer ms on ms.nik = cet.create_by
            where cev.date <= '$date2' and cev.date >= '$date1' and cev.factory_id = '$factory'
            and cev.line_name like '%$kata%'
            -- order by $col $dir
			ORDER BY cev.date, cev.no_urut, cev.jam 
            limit $limit offset $start");
        }
        else{
            $query = $this->db->query("SELECT cev.*, cet.major_couse, cet.root_couse, 
            cet.action, cet.create_by as dibuat_oleh, cet.create_date as dibuat_kapan,
            ms.name as operator from cap_endline_view cev
            LEFT JOIN cap_endline_trans cet on cet.cap_endline_detail_id = cev.cap_endline_detail_id
            LEFT JOIN master_sewer ms on ms.nik = cet.create_by
            where cev.date <= '$date2' and cev.date >= '$date1' and cev.factory_id = '$factory'
            and cev.line_name like '%$kata%'
            order by cev.no_urut");
        }

        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_summary_endline($search,$date1,$date2)
    {
        $factory = $this->session->userdata('factory');
        $kata = strtoupper($search);

        $query = $this->db->query("SELECT cev.*, cet.major_couse, cet.root_couse, 
        cet.action, cet.create_by as dibuat_oleh, cet.create_date as dibuat_kapan,
        ms.name as operator from cap_endline_view cev
        LEFT JOIN cap_endline_trans cet on cet.cap_endline_detail_id = cev.cap_endline_detail_id
        LEFT JOIN master_sewer ms on ms.nik = cet.create_by
        where cev.date <= '$date2' and cev.date >= '$date1' and cev.factory_id = '$factory'
        and cev.line_name like '%$kata%'");

        return $query->num_rows();
    }
	

}

/* End of file CapModel.php */
/* Location: ./application/models/CapModel.php */
