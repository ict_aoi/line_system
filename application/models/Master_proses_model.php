<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_proses_model extends CI_Model {
	function allposts_count()
    {   
        $query = $this
                ->db
                ->select('style')
                ->group_by('style')
                ->where('flag','f')
                ->get('master_style');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->select('style')
                ->group_by('style')
                ->where('flag','f')
                ->get('master_style');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('style',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->select('style')
                ->group_by('style')
                ->where('flag','f')
                ->get('master_style');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('style',$search)
                ->select('style')
                ->group_by('style')
                ->where('flag','f')
                ->get('master_style');
    
        return $query->num_rows();
    }

    //list master proses

    function allposts_count_proses()
    {   
        $query = $this
                ->db
                ->get('master_proses');
    
        return $query->num_rows();  

    }
    
    function allposts_proses($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_proses');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search_proses($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('proses_name',strtoupper($search))
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_proses');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count_proses($search)
    {
        $query = $this
                ->db
                ->like('proses_name',strtoupper($search))
                ->get('master_proses');
    
        return $query->num_rows();
    }
	

}

/* End of file master_proses_model.php */
/* Location: ./application/models/master_proses_model.php */