<style type="text/css">

.modal-lg {
     width: 900px;
    }
</style>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Laporan Terima Karton</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('folding');?>">home</a></li>
        <li><i class="fa fa-bars"></i>Laporan Terima Karton</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <div class="panel-body">
    <div class="col-md-3">
      <div class="box box-success">
        <div class="box-header with-border">
          <div class="box-title">Pilih Tanggal Permintaan Karton</div>
            <div class="box-body">
              <div class="input-group">
                <input class="form-control" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d'); ?>" autocomplete="off" required>
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" id="clear"><i class="fa fa-times"></i></button>
                </span>
              </div>
              <hr>
                <!-- <div class="text-right">
                  <button type="button" class="btn btn-primary tampilkan"><i class="fa fa-file-excel-o"></i> Tampilkan</button>
                </div> -->
            </div>
        </div>
      </div>
    </div>
  <div class="clearfix"></div>
  <div class="box-body">
    <div class="table-responsive">
        <table class="table datatable-save-state" id="table-list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>LINE</th>
                    <th>PO</th>
                    <th>ARTICLE</th>
                    <th>SIZE</th>
                    <th>CTN</th>
                    <th>PACKING DONE</th>
                    <th>SEWING RECEIVE</th>
                    <!-- <th>INFO</th> -->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
  </div>
  </div>
</div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";

  $(function(){
      $('#tanggal').trigger('change');

      $('#tanggal').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });
      var url_loading = $('#loading_gif').attr('href');
      var table = $('#table-list').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            "order": [[ 0, "desc" ]],
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "<?=base_url();?>RequestKarton/report_receive_carton_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                  data.tanggal     = $('#tanggal').val();
                  data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
              },
              "columns": [
                  { "data": null, 'sortable' : false},
                  { "data": "request_at" },
                  { "data": "description" },
                  { "data": "po_number_concat" },
                  { "data": "buyer_item_concat" },
                  { "data": "manufacturing_size_concat" },
                  { "data": "carton_qty_concat" },
                  { "data": "done" },
                  { "data": "receive" }
               ],
               fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }

        });

        // $('.tampilkan').click(function () {
        //   table.draw();
        // });

        $('#clear').click(function () {
          $('#tanggal').val('');
          table.draw();
        });

        $('#tanggal').on('change', function(event) {
          event.preventDefault();
          table.draw();
        });

  });
</script>