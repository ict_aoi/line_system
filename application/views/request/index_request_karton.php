<link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.min.css'); ?>">
<style>
  #inputPoreference option{
    font-size: 3.5rem !important;
  }
  .badge {
      padding: 1px 9px 2px;
      font-size: 12.025px;
      font-weight: bold;
      white-space: nowrap;
      color: #ffffff;
      background-color: #999999;
      -webkit-border-radius: 9px;
      -moz-border-radius: 9px;
      border-radius: 9px;
    }
    .badge:hover {
      color: #ffffff;
      text-decoration: none;
      cursor: pointer;
    }
    .badge-error {
      background-color: #b94a48;
    }
    .badge-error:hover {
      background-color: #953b39;
    }
    .badge-warning {
      background-color: #f89406;
    }
    .badge-warning:hover {
      background-color: #c67605;
    }
    .badge-success {
      background-color: #468847;
    }
    .badge-success:hover {
      background-color: #356635;
    }
    .badge-info {
      background-color: #3a87ad;
    }
    .badge-info:hover {
      background-color: #2d6987;
    }
    .badge-inverse {
      background-color: #333333;
    }
    .badge-inverse:hover {
      background-color: #1a1a1a;
    }

</style>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Request Karton</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('folding');?>">Menu Folding</a></li>
        <li><i class="fa fa-bars"></i>Request Karton</li>
      </ol>
    </div>
  </div>
</div>
<div class="row">
  <div class="panel panel-default border-grey">
    <div class="col-md-1 pull-right">
      <button type="button" class="btn btn-primary" id="addRequest">Tambah</button>
    </div>
  </div>
  </div>
<div class="row">
<div class="col-md-12 table_detail">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Menu Request Karton
             </div>
             <div class="col-md-2 pull-right">
               <!-- <span class="style"></span> -->
             </div>
           </h6>
        </div>
          <div class="panel-body">
            <div class="table-responsive">
                <table class="table datatable-save-state" id="table-list">
                    <thead>
                        <tr>
                            <!-- <th>#</th> -->
                            <th>DATE</th>
                            <!-- <th>LINE</th> -->
                            <th>PO NUMBER</th>
                            <th>ARTICLE</th>
                            <th>SIZE</th>
                            <th>CTN</th>
                            <th class="text-center">PACKING</th>
                            <th class="text-center">SEWING</th>
                            <th>INFO</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<div id="modal_request" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="panel-heading">
              <div class="pull-right">
               <!--  <button type="button" class="btn btn-small btn-default" id="hideModal"><i class="fa fa-times" style="font-size:16px;"></i></button> -->
              </div>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <legend class="text-semibold">
                            <h4 id=""><i class="position-left"></i>Tambah Request Carton</h4>
                        </legend>
                        <form action="post" id="form_request">
                          <table class="table table-hover" width="100%" id="tableRequestCarton">
                            <thead>
                              <tr>
                                <th>PO</th>
                                <th>Article</th>
                                <th>Size</th>
                                <th>QTY</th>
                                <th>Act</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr id="requestForm">
                                <td style="width: 32%;">
                                  <select name="inputPoreference" id="inputPoreference" class="form-control" style="height: 4rem; font-size: 1.7rem;">
                                    <option value=""> -- Pilih PO -- </option>
                                    <?php foreach ($get_po as $gets_po) {
                                    ?>
                                          <option data-po="<?=$gets_po->poreference;?>" value="<?=$gets_po->inline_header_id;?>"><?=$gets_po->poreference?></option>
                                    <?php
                                    } ?>
                                  </select>
                                </td>
                                
                                <td style="width: 32%;">
                                  <select name="inputArticle" id="inputArticle" class="form-control" style="height: 4rem; font-size: 1.7rem;">
                                    <option value=""> -- Pilih Article -- </option>
                                  </select>
                                </td>
                                    
                                <td style="width: 27%;">
                                 <select multiple name="inputSize" id="inputSize" class="form-control" style="width: 100%; font-size: 125%;"></select><br>
                                 <input type="checkbox" id="check_all" value="1"> All Size
                                </td>
                                <td style="width: 25%;">
                                  <div class="input-group">
                                    <input type="text" id="numpadButton" class="form-control" placeholder="Qty" aria-describedby="numpadButton-btn">
                                    <span class="input-group-btn">
                                      <button class="btn btn-default" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
                                    </span>
                                  </div>
                                  <!-- <br> -->
                                    <input type="checkbox" id="check_last_carton" value="1"> Last Carton
                                </td>
                                <td class="text-center">
                                  <button type="button" id="btnRequestSave" onclick="saveRequest();" class="btn btn-sm btn-primary btn-save" title="Simpan"><i class="fa fa-save"></i></button>
                                  <button type="button" id="btnRateClose" onclick="$('#requestForm').hide();$('#btnRequestAdd').attr('disabled', false);" class="btn btn-sm btn-danger" title="Hapus"><i class="fa fa-times"></i></button>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="4"><button type="button" id="btnRequestAdd" onclick="addRequest();" class="btn btn-success btn-xs">Tambah</button></td>
                              </tr>
                            </tbody>
                          </table>
                        </form>
                    </fieldset>
                </div>
                <div class="modal-footer">
                   <button type="button" class="btn btn-small btn-default" id="hideModal">Close</button>
                  <button type="button" class="btn btn-success" id="save_request">Submit</button>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
<a href="<?php echo base_url('RequestKarton/searchSize'); ?>" id="searchSize"></a>
<a href="<?php echo base_url('RequestKarton/submitRequest'); ?>" id="submitRequest"></a>
<a href="<?php echo base_url('RequestKarton/submitRequestReceive'); ?>" id="submitRequestReceive"></a>
<a href="<?php echo base_url('RequestKarton/submitRequestCancel'); ?>" id="submitRequestCancel"></a>
<a href="<?php echo base_url('RequestKarton/ajaxGetListRequest'); ?>" id="ajaxGetListRequest"></a>

<script src="<?=base_url('assets/plugins/select2/select2.min.js');?>"></script>
<script type="text/javascript">

  var url = $('#searchSize').attr('href');
  var url_post = $('#submitRequest').attr('href');
  var url_carton = $('#ajaxGetListRequest').attr('href');
  var url_receive = $('#submitRequestReceive').attr('href');
  var url_cancel = $('#submitRequestCancel').attr('href');

  //chose article
  // $('#inputPoreference').change(function(){ 
  //     var ih=$(this).val();
  //     $.ajax({
  //         url : "<?php //echo site_url('RequestKarton/get_article');?>",
  //         method : "POST",
  //         data : {ih: ih},
  //         async : true,
  //         dataType : 'json',
  //         success: function(data){
                
  //             var html = '';
  //             var i;
  //             for(i=0; i<data.length; i++){
  //                 html += '<option value='+data[i].article+'>'+data[i].article+'</option>';
  //             }
  //             $('#inputArticle').html(html);

  //         }
  //     });
  //     return false;
  // }); 

  // start datatables
  var table = $('#table-list').DataTable({
        processing: true,
        // serverSide: true,
        ajax: {
            url: url_carton,
            type: 'GET',
            data: function (d) {
                return $.extend({},d,{

                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(8).css('min-width', '110px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: 'request_at', name: 'request_at'},
            // {data: 'description', name: 'description'},
            {data: 'po_number_concat', name: 'po_number_concat'},
            {data: 'buyer_item_concat', name: 'buyer_item_concat'},
            {data: 'manufacturing_size_concat', name: 'manufacturing_size_concat'},
            {data: 'carton_qty_concat', name: 'carton_qty_concat'},
            {data: 'status_packing', name: 'status_packing', sortable: false, orderable: false, searchable: false},
            {data: 'status_sewing', name: 'status_sewing', sortable: false, orderable: false, searchable: false},
            {data: 'info', name: 'info'},
            {data: 'action', name: 'button', orderable: false, searchable: false},
        ],
    });
    //end of datatables

  $('#requestForm').hide();

  $('#inputSize').select2({
      maximumSelectionLength: 1
    });

  $('#addRequest').click(function(event) {

    $('#modal_request').modal({backdrop: 'static', keyboard: false});
  });

  // hide modal
  $('#hideModal').on('click', function(event) {
    event.preventDefault();

      $("#tableRequestCarton tbody").find('tr:not(:last)').not($("#requestForm")).remove();
      $('#requestForm').hide();

      $('#modal_request').modal('hide');
  });

  // modal on hide
  $('#modal_request').on('hidden.bs.modal', function() {
    $("#tableRequestCarton tbody").find('tr:not(:last)').not($("#requestForm")).remove();
  });


  $('#inputPoreference').on('change', function(event) {
    event.preventDefault();

    $("#check_all").prop("checked",false);
    $("#check_last_carton").prop("checked",false);
    $('#inputSize').attr('disabled', false);

    var arr_size = Array();
    var multisize = Array();
    var valueToPush = { };

    var header_id_check = $('input[name="dynamic_req[inline_header_id][]"]').val();

    if (header_id_check !== undefined) {
      $.each($('input[name="dynamic_req[inline_header_id][]"]'), function(index, value) {
          valueToPush['inline_header_id'] = $('input[name="dynamic_req[inline_header_id][]"]').val();
          valueToPush['poreference'] = $('input[name="dynamic_req[poreference][]"]').val();
          valueToPush['article'] = $('input[name="dynamic_req[article][]"]').val();
          valueToPush['size'] = $('input[name="dynamic_req[size][]"]').val();
          valueToPush['qty'] = $('input[name="dynamic_req[qty][]"]').val();

          arr_size.push(valueToPush);
      });
    }

      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {inline_header_id: $(this).val(), arr_size: arr_size},
        success: function(data){
          $('#inputSize').html('');
          // var html = '<option value=""> -- Pilih Size --</option>';
          var html = '';
          for (var i = 0; i < data.length; i++) {
            html += '<option value="'+data[i].size+'">'+data[i].size+'</option>';
          }

          $('#inputSize').append(html);
          $('#inputSize').trigger('change');
        },
        error: function(data) {
          console.log(data);
        }
      });

      $.ajax({
          url : "<?php echo site_url('RequestKarton/searchArticle');?>",
          method : "POST",
          data : {ih: $(this).val(), arr_size: arr_size},
          async : true,
          dataType : 'json',
          success: function(data){
            $('#inputArticle').html('');
            var html = '';
            var i;
            for(i=0; i<data.length; i++){
                html += '<option value='+data[i].article+'>'+data[i].article+'</option>';
            }
            $('#inputArticle').append(html);
            $('#inputArticle').trigger('change');

          }
      });

  });

  $("#check_all").click(function(){
      if($("#check_all").is(':checked') ){
          // $("select > option").prop("selected","selected");
          $("#inputSize").find('option').prop("selected",true);
          $("#inputSize").attr('disabled', true);
          $("#inputSize").trigger('change');
      }else{
          // $("select > option").removeAttr("selected");
          $("#inputSize").find('option').prop("selected",false);
          $("#inputSize").attr('disabled', false);
          $("#inputSize").trigger('change');
       }
  });

  $('#save_request').on('click', function(event) {
    event.preventDefault();

   var data = $('#form_request').serializeArray();

   var dynamic_req_po = $('input[name="dynamic_req[poreference][]"]').val();

   if(dynamic_req_po === undefined){
      swal({
            icon: 'error',
            title: 'Oops..',
            text: 'data tidak ditemukan..!'
          });

      return false;
   }

   swal({
      title: "Apakah anda yakin..?",
      text: "menambahkan permintaan karton..?",
      icon: "info",
      buttons: true,
      dangerMode: true,
    })
    .then((execute) => {
      if (execute) {
        $.ajax({
            url: url_post,
            type: 'POST',
            dataType: 'json',
            data: $('#form_request').serialize(),
            success:function(data){
                if (data == 1) {
                  swal({
                    icon: 'success',
                    title: 'Good',
                    text: 'data request berhasil disimpan..!'
                  });
                }else{
                  swal({
                    icon: 'error',
                    title: 'Oops',
                    text: 'Something Wrong..!'
                  });
                }
                table.ajax.reload(null, false);
                $('#modal_request').modal('hide');
                // location.reload();
            },
            error:function(data) {
              console.log(data);
              swal({
                icon: 'error',
                title: 'Oops..',
                text: data.responseJSON
              });

              // location.reload();
            }
          });

      }
    });

  });

  // sewing receive
  function receive_carton(e) {
      var id =  e.getAttribute('data-id');
      var po_number =  e.getAttribute('data-po');
      var article = e.getAttribute('data-article');
      var manufacturing_size =  e.getAttribute('data-manufaturingsize');

      swal({
          title: "Apakah anda yakin..?",
          text: "menerima karton ( "+po_number+" - Article: "+article+" - Size: "+manufacturing_size+") ..?",
          icon: "info",
          buttons: true,
          dangerMode: true,
        })
        .then((execute) => {
          if (execute) {
            $.ajax({
                url: url_receive,
                type: 'POST',
                dataType: 'json',
                data: {po_number: po_number, id: id, article: article},
                success:function(data){
                    if (data == 1) {
                      swal({
                        icon: 'success',
                        title: 'Good',
                        text: 'karton berhasil diterima..!'
                      });

                    }else{
                      swal({
                        icon: 'error',
                        title: 'Oops',
                        text: 'Something Wrong..!'
                      });

                    }
                    // location.reload();
                    table.ajax.reload(null, false);
                    $('#modal_request').modal('hide');
                },
                error:function(data) {
                  console.log(data);
                  swal({
                    icon: 'error',
                    title: 'Oops..',
                    text: data.responseJSON
                  });

                  // location.reload();
                }
              });

          }
        });

  }

  // sewing cancel
  function cancel_carton(e) {
      var id =  e.getAttribute('data-id');
      var po_number =  e.getAttribute('data-po');
      var article =  e.getAttribute('data-article');
      var manufacturing_size =  e.getAttribute('data-manufaturingsize');

      swal({
          title: "Apakah anda yakin..?",
          text: "Cancel permintaan karton ( "+po_number+" - Article:"+article+" - Size: "+manufacturing_size+") ..?",
          icon: "error",
          buttons: true,
          dangerMode: true,
        })
        .then((execute) => {
          if (execute) {
            $.ajax({
                url: url_cancel,
                type: 'POST',
                dataType: 'json',
                data: {po_number: po_number, id: id},
                success:function(data){
                    if (data == 1) {
                      swal({
                        icon: 'success',
                        title: 'Good',
                        text: 'permintaan karton berhasil dicancel..!'
                      });

                    }else{
                      swal({
                        icon: 'error',
                        title: 'Oops',
                        text: 'Something Wrong..!'
                      });

                    }
                    // location.reload();
                    table.ajax.reload(null, false);
                    $('#modal_request').modal('hide');
                },
                error:function(data) {
                  console.log(data);
                  swal({
                    icon: 'error',
                    title: 'Oops..',
                    text: data.responseJSON
                  });

                  // location.reload();
                }
              });

          }
        });

  }

  function addRequest() {
    $('#inputPoreference').trigger('change');

    $('#numpadButton').val('');

    $('#requestForm').show();
  }

  function saveRequest(index){
    var inline_header_id = $("#inputPoreference").val();
    var poreference = $("#inputPoreference option:selected").text();
    var article = $('#inputArticle').val();
    var size = $('#inputSize').val();
    var qty = $('#numpadButton').val();

    var check_all_size = $('#check_all:checked').val() === undefined ? 0 : $('#check_all:checked').val();
    var check_last_carton = $('#check_last_carton:checked').val() == undefined ? 0 : $('#check_last_carton:checked').val();

    var table = $('#tableRequestCarton');
    var form = $('#requestForm');

    if (!poreference || !size || !qty) {

      swal({
        icon: 'warning',
        title: 'Oops...',
        text: 'data belum terisi semua'
      });

    }else{

      if(index === undefined) {

              $('<tr>' +
                '  <td>' + poreference + '</td>' +
                '  <td class="">' + article + '</td>' +
                '  <td class="">' + size + '</td>' +
                '  <td class="text-right">' + qty + '</td>' +
                '  <td class="text-center">' +
                '    <input type="hidden" name="dynamic_req[inline_header_id][]" value="' + inline_header_id + '">' +
                '    <input type="hidden" name="dynamic_req[poreference][]" value="' + poreference + '">' +
                '    <input type="hidden" name="dynamic_req[article][]" value="' + article + '">' +
                '    <input type="hidden" name="dynamic_req[size][]" value="' + size + '">' +
                '    <input type="hidden" name="dynamic_req[qty][]" value="' + qty + '">' +
                '    <input type="hidden" name="dynamic_req[check_all_size][]" value="' + check_all_size + '">' +
                '    <input type="hidden" name="dynamic_req[check_last_carton][]" value="' + check_last_carton + '">' +
                '    <button type="button" onclick="editRequest(this);" class="btn btn-sm btn-success btnRequestEdit" title="Edit"><i class="fa fa-pencil"></i></button>' +
                '    <button type="button" onclick="deleteRequestRow(this);" class="btn btn-sm btn-danger btnRequestDelete" title="Hapus"><i class="fa fa-times"></i></button>' +
                '  </td>' +
                '</tr>').insertBefore(form);

            } else {

              var tr = table.find('tbody > tr').eq(index);

              $(tr).find('td:eq(0)').text(poreference);
              $(tr).find('td:eq(1)').text(article);
              $(tr).find('td:eq(2)').text(size);
              $(tr).find('td:eq(3)').text(qty);

              $(tr).find('td:eq(4) input[name="dynamic_req[inline_header_id][]"]').val(inline_header_id);
              $(tr).find('td:eq(4) input[name="dynamic_req[poreference][]"]').val(poreference);
              $(tr).find('td:eq(4) input[name="dynamic_req[article][]"]').val(article);
              $(tr).find('td:eq(4) input[name="dynamic_req[size][]"]').val(size);
              $(tr).find('td:eq(4) input[name="dynamic_req[qty][]"]').val(qty);
              $(tr).find('td:eq(4) input[name="dynamic_req[check_all_size][]"]').val(check_all_size);
              $(tr).find('td:eq(4) input[name="dynamic_req[check_last_carton][]"]').val(check_last_carton);

              $("#btnRequestSave").attr("onclick", "saveRequest()");

              $(tr).show();
            }

            form.insertBefore(table.find("tr").last()).hide();
            $('#btnRequestAdd').attr('disabled', false);
    }
  }

  function editRequest(self) {
    $('#btnRequestAdd').attr('disabled', true);

    var parent = $(self).parent();
    var row = $(self).parent().parent();
    var form = $('#requestForm');


    var inline_header_id = $(parent).find('input[name="dynamic_req[inline_header_id][]"]').val();
    var poreference = $(parent).find('input[name="dynamic_req[poreference][]"]').val();
    var article = $(parent).find('input[name="dynamic_req[article][]"]').val();
    var size = $(parent).find('input[name="dynamic_req[size][]"]').val();
    var qty = $(parent).find('input[name="dynamic_req[qty][]"]').val();
    var check_all_size = $(parent).find('input[name="dynamic_req[check_all_size][]"]').val();
    var check_last_carton = $(parent).find('input[name="dynamic_req[check_last_carton][]"]').val();


    $("#check_all").prop("checked",false);
    $("#inputSize").find('option').prop("selected",false);
    $("#inputSize").trigger('change');
    
    $("#inputArticle").find('option').prop("selected",false);
    $("#inputArticle").trigger('change');


    $('#inputPoreference').val(inline_header_id);
    $('#inputSize').val(size);
    $('#inputArticle').val(article);
    $('#numpadButton').val(qty);

    form.insertAfter(row);
    row.hide();
    $("#btnRequestSave").attr("onclick", "saveRequest(" + $(parent).parent().index() + ")");
    $("#requestForm").show();

    $('#btnRequestAdd').attr('disabled', true);
    $('#inputSize').attr('disabled', false);
    $('#inputArticle').attr('disabled', false);
  }

  function deleteRequestRow(self) {
    $(self).parent().parent().remove();
    $('#btnRequestAdd').attr('disabled', false);
    $('#inputSize').attr('disabled', false);
    $('#inputArticle').attr('disabled', false);

    return false;
  }
</script>