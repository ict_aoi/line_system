<style type="text/css">

.modal-lg {
     width: 900px;
    }
</style>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Packing List</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('folding');?>">home</a></li>
        <li><i class="fa fa-bars"></i>Packing List</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <div class="panel-body">
      <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Cari PO:</label>
                    <select name="inputPoreference" id="inputPoreference" class="form-control" style="height: 4rem; font-size: 2.2rem;">
                      <option value=""> -- Pilih PO -- </option>
                      <?php foreach ($get_po as $gets_po) {
                      ?>
                            <option value="<?=$gets_po->poreference;?>" data-inline="<?=$gets_po->inline_header_id;?>"><?=$gets_po->poreference?></option>
                      <?php
                      } ?>
                    </select>
                    <br>
                </div>
            </div>
      </div>
      <br>
      <!-- <div class="row"> -->
        <div class="col-md-4">
          <div class="form-group row">
            <label for="consignee">Consignee:</label><br>
            <span id="consignee" name="consignee"></span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group row">
            <label for="customer_order_number">Customer No : </label>
            <span id="customer_order_number" name="customer_order_number"></span>
          </div>
          <div class="form-group row">
            <label for="ex_factory_date">Ex-Factory Date : </label>
            <span id="ex_factory_date" name="ex_factory_date"></span>
          </div>
          <div class="form-group row">
            <label for="working_no">Working No : </label>
            <span id="working_no" name="working_no"></span>
          </div>
          <div class="form-group row">
            <label for="qty">QTY : </label>
            <span id="qty" name="qty"></span>
          </div>
          <div class="form-group row">
            <label for="ctn_count">CTN Count : </label>
            <span id="ctn_count" name="ctn_count"></span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group row">
            <label for="net_net">Net Net : </label>
            <span id="net_net" name="net_net"></span>
          </div>
          <div class="form-group row">
            <label for="net">Net : </label>
            <span id="net" name="net"></span>
          </div>
          <div class="form-group row">
            <label for="gross">Gross : </label>
            <span id="gross" name="gross"></span>
          </div>
          <div class="form-group row">
            <label for="cbm">CBM : </label>
            <span id="cbm" name="cbm"></span>
          </div>
        </div>
      <!-- </div> -->
      <!-- <div class="row"> -->
        <!-- <div style="overflow-y: auto; height:700px;width: 700px;"> -->
          <div class="table-responsive">
            
              <table class="table table-striped table-hover table-bordered datatable-save-state" id="table-list">
                  <thead class="bg-gray">
                      <th>Range</th>
                      <th>PO</th>
                      <th>Article</th>
                      <th>Customer Size</th>
                      <th>Manufacturing Size</th>
                      <th>Qty</th>
                      <th>Qty /CTN</th>
                      <th>Ctn Count</th>
                      <th>R</th>
                  </thead>
              </table>
            </div>
          <!-- </div> -->
      <!-- </div> -->
      <a href="<?=base_url('RequestKarton/show_packing_list');?>" id="get_packing_list"></a>
  </div>
</div>
<script>
    // $(function(){
        var url = $('#get_packing_list').attr('href');

        //datatables
        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            paging: false,
            ordering: false,
            info: false,
            searching: false
        });

        var table = $('#table-list').DataTable({
            ajax: {
                url: url,
                data: function (d) {
                    return $.extend({},d,{
                        "inputPoreference": $('#inputPoreference').val()
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                // $('td', row).eq(0).html(value);
            },
            columnDefs: [
                {
                      targets: [5, 6, 7],
                      className: 'text-right'
                 }
            ],
            columns: [
                // {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'package_range', name: 'package_range', sortable: false, orderable: false, searchable: false},
                {data: 'po_number', name: 'po_number'},
                {data: 'buyer_item', name: 'buyer_item'},
                {data: 'customer_size', name: 'customer_size'},
                {data: 'manufacturing_size', name: 'manufacturing_size'},
                {data: 'item_qty', name: 'item_qty', sortable: false, orderable: false, searchable: false},
                {data: 'inner_pack', name: 'inner_pack', sortable: false, orderable: false, searchable: false},
                {data: 'pkg_count', name: 'pkg_count', sortable: false, orderable: false, searchable: false},
                {data: 'r', name: 'r', sortable: false, orderable: false, searchable: false}
            ],
        });

        $('#inputPoreference').on('change', function(e) {
            po_summary();

            table.draw();
        });

    // })
    function po_summary() {

        $.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
          data: {inputPoreference: $('#inputPoreference').val()},
          success: function(data) {

            var data_po = data.data_po;
            var data_calculate = data.data_calculate;

            var bp_name = data_po.bp_name ? data_po.bp_name : '';
            var address = data_po.address ? data_po.address : '';
            var country = data_po.country ? data_po.country : '';
            var postal = data_po.postal ? data_po.postal : '';
            var phone = data_po.phone ? data_po.phone : '';
            var fax = data_po.fax ? data_po.fax : '';
            var customer_order_number = data_po.customer_order_number ? data_po.customer_order_number : '';
            var statisticaldate = data_po.statisticaldate ? data_po.statisticaldate : '';
            var upc = data_po.upc ? data_po.upc : '';
            var total_item_qty = data_calculate.total_item_qty ? data_calculate.total_item_qty : '';
            var ctn_count = data_calculate.ctn_count ? data_calculate.ctn_count : '';
            var total_net_net = data_calculate.total_net_net ? data_calculate.total_net_net : '';
            var total_net = data_calculate.total_net ? data_calculate.total_net : '';
            var total_gross = data_calculate.total_gross ? data_calculate.total_gross : '';
            var cbm = data_calculate.cbm ? data_calculate.cbm : '';

            $('#consignee').html('<b>'+bp_name+'<br>'+address+'<br>'+country+' '+postal+'<br>phone:'+phone+'<br>fax:'+fax+'</b>');
            $('#customer_order_number').html('<b>'+customer_order_number+'</b>');
            $('#ex_factory_date').html('<b>'+statisticaldate+'</b>');
            $('#working_no').html('<b>'+upc+'</b>');
            $('#qty').html('<b>'+total_item_qty+'</b>');
            $('#ctn_count').html('<b>'+ctn_count+'</b>');
            $('#net_net').html('<b>'+total_net_net+'</b>');
            $('#net').html('<b>'+total_net+'</b>');
            $('#gross').html('<b>'+total_gross+'</b>');
            $('#cbm').html('<b>'+cbm+'</b>');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
          }
        });

    }

</script>