<script type="x-tmpl-mustache" id="items-table">

  {% #item %}
    <tr>
      <td style="text-align:center;">{% no %}</td>
      <td>
        <div id="namasubkriteria_{% _id %}">{% nama_subkriteria %}</div>
        <input type="text" class="form-control input-sm d-none" id="itemInputId_{% _id %}" value="{% nama_subkriteria %}" data-id="{% _id %}"
        >
        <td>
          <div id="bobot_{% _id %}">{% bobot %}</div>
          <input type="text" width="20px" class="form-control d-none input-sm " id="inputbobot_{% _id %}" value="{% bobot %}" data-id="{% _id %}"
        >
      </td>

      <td width="50px">
        <button type="button" id="edit_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-edit-item"><i class="fas fa-pencil-alt"></i></button>
        <button type="button" id="simpan_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-save-item d-none"><i class="fas fa-save"></i></button>
        <button type="button" id="cancel_{% _id %}" data-id="{% _id %}" class="btn btn-warning btn-cancel-item d-none"><i class="fas fa-window-close""></i></button>
        <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-circle btn-delete-item"><i class="fas fa-trash"></i></button>
      </td>
    </tr>
  {%/item%}

   <tr>
    <td width="20px">
      #
    </td>
    <td >
      <input type="text" class="form-control" id="pilihpo" placeholder="Masukan Sub kriteria">
    </td>
    <td >
      <input type="number" class="form-control" id="pilihsize" placeholder="Masukan bobot">
    </td>
    <td width="20px">
      <button type="button"class="btn btn-primary" id="addSubKriteria"><i class="fas fa-plus-square"></i> Tambah</button>
    </td>


  </tr>
</script>