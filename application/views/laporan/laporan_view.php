<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <h1 class="text-center"> LAPORAN STO </h1>
        <br>
        <div class="panel-body">
        
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group" id="accordion">
                        <div class="row">
                            <div class="col-md-6">
                                
                                <label>Input Factory</label>
                                <br>
                                <select class="form-control select2" id="factory" name="factory">
                                    <option value="">-- Pilih --</option>
                                    <option value="1">AOI 1</option>
                                    <option value="2">AOI 2</option>
                                </select>
                                <br>
                                <label>Input Tanggal Awal</label>
                                <br>
                                <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off">
                                <br>
                                <label>Input Tanggal Akhir</label>
                                <br>
                                <input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off">
                                <!-- <input class="form-control" type="hidden" id="tgl"> -->
                                <br><br>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" id='collapse1' href="#collapseOne">Pencarian Berdasarkan PO</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <input class="form-control" type="text" id="po_number">        
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" id='collapse2' href="#collapseTwo">Pencarian Berdasarkan Style</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <input class="form-control" type="text" id="style">        
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>

                        </div>
                        <!-- <br> -->
                        
                        <div class="row">
                            <div class="col-md-6">
                                <button onclick='return download()' href='javascript:void(0)' class='btn btn-md btn-primary'><i class='fa fa-file-text'></i> Download Report</button>
                                <button class='btn btn-md btn-danger' id='clear_filter'><i class='fa fa-cross'></i> Clear</button>
                                <div class="pull-right">
                                    <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button>
                                </div>    
                                <br>
                            </div>
                            
                        </div>
                         
                        <!-- <button id="download_report" class='btn btn-md btn-warning'><i class='fa fa-th-list'></i> Download Report</button> -->
                        
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-dash" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">LAST UPDATED QC</th>
                        <th class="text-center">FACTORY</th>
                        <th class="text-center">LINE</th>
                        <th class="text-center">STYLE</th>
                        <th class="text-center">PO</th>
                        <th class="text-center">SIZE</th>
                        <th class="text-center">WIP QC</th>
                        <th class="text-center">WIP FOLDING</th>
                        <th class="text-center">OUTPUT PACKING</th>
                        <th class="text-center">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>  
<script type="text/javascript">
    $(document).ready(function () {

        $('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });

        $('#tanggal2').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });

        $('#clear_filter').click(function() {
            $('#tanggal1').val('');
            $('#tanggal2').val('');
            table.draw();
        });
        

        var table = $('#table-dash').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "searching": false,
            "orderMulti"  : true,
            "ajax":{
                "url": "laporan/list_laporan",
                "dataType": "json",
                "type": "POST",
                "data":function(data) {
                                        
                            data.dari       = $('#tanggal1').val();
                            data.sampai     = $('#tanggal2').val();
                            data.po         = $('#po_number').val();
                            data.style      = $('#style').val();
                            data.factory    = $('#factory').val();
                            data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                        },
                            },
                "columns": [
                    { "data": "date" },
                    { "data": "factory_name" , 'sortable' : false},
                    { "data": "line_name" },
                    { "data": "style"},
                    // { "data": "start_date" , 'sortable' : false},
                    // { "data": "export_date" , 'sortable' : false},  
                    { "data": "poreference" , 'sortable' : false},
                    { "data": "size" , 'sortable' : false},
                    // { "data": "order" , 'sortable' : false},
                    { "data": "wip_qc" , 'sortable' : false},
                    { "data": "wip_folding" , 'sortable' : false},
                    { "data": "output" , 'sortable' : false},
                    { "data": "action" , 'sortable' : false},
                ],

            //buat nomor
            // fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
            //   ,
            //   "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull )
            //       {
            //         if(aData.cek==1){
            //             $(nRow).css('background-color', '#EC7063');
            //             $(nRow).css('color', 'white');
            //         }else {
            //             $(nRow).css('background-color', '#4BC559');
            //             $(nRow).css('color', 'white');
            //         }
            //       },
        });
        $('#table-dash_filter input').unbind();
        $('#table-dash_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-dash').DataTable().ajax.reload();
        });
        // $('#tgl').change(function(){
        //     // alert("man"+$('#tgl').val());
        //     table.draw();
        // });

        $('#pencarian').click(function(event) {
            
            var factory    = $('#factory').val();
            if (!factory) {
                $("#alert_warning").trigger("click", 'PILIH FACTORY TERLEBIH DAHULU');
            }
            else{
                table.draw();
                if (event.keyCode == 13 && $(this).val() != '') {
                table.draw();
                }
            }
        });

        $('#collapse1').click(function(event) {
            $('#style').val('');
        });
        
        $('#collapse2').click(function(event) {
            $('#po_number').val('');
        });

        
    });
    function detail(po,line,size,style,bundle) {
        var url_loading = $('#loading_gif').attr('href');
        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Detail Component');
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:170px;margin-right=3000px;width=100px'>");
        modal.children('.modal-body').load("laporan/get_detail?po="+po+"&line="+line+"&size="+size+"&style="+style+"&bundle="+bundle);
        return false;
    }

    function download() {
        // var tgl = $('#tgl').val();
        var dari    = $('#tanggal1').val();
        var sampai  = $('#tanggal2').val();
        var po      = $('#po_number').val();
        var style   = $('#style').val();
        var factory = $('#factory').val();

        text = 'dari='+dari+'&sampai='+sampai+'&po='+po+'&style='+style+'&factory='+factory;
        
        window.location = '<?=base_url();?>'+'/laporan/download?'+text;
        return false;
    }
</script>