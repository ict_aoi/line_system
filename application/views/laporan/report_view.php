<?php
        
  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report STO Line.xls"');
?>

<table border="1px">
    <thead>
        <tr>
          <th class="text-center">LAST UPDATED QC</th>
          <th class="text-center">FACTORY</th>
          <th class="text-center">LINE</th>
          <th class="text-center">STYLE</th>
          <th class="text-center">PO</th>
          <th class="text-center">SIZE</th>
          <th class="text-center">WIP QC</th>
          <th class="text-center">WIP FOLDING</th>
          <th class="text-center">OUTPUT PACKING</th>
          <th class="text-center">COMPONENT NAME</th>
          <th class="text-center">QTY COMPONENT</th>
        </tr>
    </thead>
  <tbody>
  
    <?php 
    foreach($data as $d){
    ?>
        <?php echo $d; ?>
    <?php 
        }
    ?>
  </tbody>
</table> 
