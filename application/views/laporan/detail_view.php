<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>

<div class="panel panel-default border-grey">
  <!-- <form id="form_download"> -->
 <div class="panel-body">
	<div class="col-xs-12">
		<br>
		<div class="col-md-6 pull-right"></div>
		<table class="table table-bordered">
			<thead class="bg-primary">
				<tr>
					<th>NAMA COMPONENT</th>
					<th>QTY</th>
				</tr>
			</thead>
			<tbody>
                <?php 
                            
                    if($component==NULL){
                ?>
                    <tr>
                        <td> -
                        </td>
                        <td>
                            <?php echo $bundle ?>
                        </td>
                    </tr>                        
                <?php
                    }
                    else{
					    foreach ($component as $com): ?>
					<tr>
						<td>
							<?php echo $com->component_name ?>
						</td>
						<td>
							<?php echo $bundle ?>
						</td>
					</tr>
                    <?php endforeach; 
                    }?>
			</tbody>
		</table>
		</div>
  </div>
</div>
<!-- </form> -->
<script>
    $(document).ready(function(){
        $('#sinkron-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po='+$('#po').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/sinkron_po',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    // else if(result.status==3){
                    //     $("#alert_warning").trigger("click", result.pesan);
                    // }


                    // if (response=='sukses') {
                    //      $("#myModal").modal('hide');
                    //      $("#alert_success").trigger("click", 'Sinkron Berhasil');
                    //      $('#refresh').trigger("click");
                    // }
                }
            })
            return false;
        });
        $('#cancel-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po='+$('#po').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/cancel_po',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    // else if(result.status==3){
                    //     $("#alert_warning").trigger("click", result.pesan);
                    // }


                    // if (response=='sukses') {
                    //      $("#myModal").modal('hide');
                    //      $("#alert_success").trigger("click", 'Sinkron Berhasil');
                    //      $('#refresh').trigger("click");
                    // }
                }
            })
            return false;
        });
    });
</script>