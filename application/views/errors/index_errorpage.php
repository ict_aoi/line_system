<!-- <div class="page-404">
	<p class="text-404">404</p>

	<h4>OOOPS, ANDA TIDAK DAPAT MENGAKSES MENU INI. FORBIDDEN AREA !!</h4>
	<p>Jika anda bersikeras mau membuka menu ini, silahkan hubungi ICT untuk diberikan akses ke menu ini<br><br><a href="<?=base_url('auth');?>" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i> Return Home</a></p>
</div> -->
<br>
<div class="row">
	<div class="col-sm-12 page-error">
		<div class="error-number teal">
			404
		</div>
		<div class="error-details col-sm-6 col-sm-offset-3">
			<h3>OOOPS, ANDA TIDAK DAPAT MENGAKSES MENU INI. FORBIDDEN AREA !!</h3>
			<p>
				Jika anda bersikeras mau membuka menu ini,<br> silahkan hubungi <b>Administrator</b> untuk diberikan akses ke menu ini
				<br>
				<a href="<?=base_url('auth');?>" class="btn btn-teal btn-return">
					Return home
				</a>
			</p>
			
		</div>
	</div>
</div>