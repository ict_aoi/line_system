<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <h1 class="text-center"> LINE PERFORMANCE </h1>
        <br>
        <div class="panel-body">
        
            <div class="row">
                <div class="col-md-4">
                    <label>Input Tanggal</label>
                    <br>
                    <input class="form-control" type="date" id="tgl">
                    <br>
                    <!-- <button id="download_report" class='btn btn-md btn-warning'><i class='fa fa-th-list'></i> Download Report</button> -->
                    <button onclick='return download()' href='javascript:void(0)' class='btn btn-md btn-warning'><i class='fa fa-th-list'></i> Download Report</button>
                </div>
            </div>
            <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-dash" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" rowspan="2">TANGGAL</th>
                        <th class="text-center" rowspan="2">LINE</th>
                        <th class="text-center" rowspan="2">STYLE</th>
                        <!-- <th class="text-center">START DATE</th>
                        <th class="text-center">EXPORT DATE</th> -->
                        <th class="text-center" rowspan="2">SMV</th>
                        <th class="text-center" rowspan="2">PRESENT SEWER</th>
                        <th class="text-center" rowspan="2">PRESENT FOLDING</th>
                        <th class="text-center" rowspan="2">WORKING HOURS PLANNING</th>
                        <th class="text-center" rowspan="2">CHANGE OVER</th>
                        <th class="text-center" rowspan="2">CUM DAY</th>
                        <th class="text-center" rowspan="2">TARGET EFF</th>
                        <th class="text-center" rowspan="2">ACTUAL EFF</th>
                        <th class="text-center" rowspan="2">WFT</th>
                        <th class="text-center"colspan="3">OUTPUT</th>
                        <th class="text-center" rowspan="2">ACTION</th>
                    </tr>
                    <tr>
                        <th class="text-center">SEWING</th>
                        <th class="text-center">QC</th>
                        <th class="text-center">FOLDING</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>  
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#table-dash').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
            "url": "dashboard/list_dashboard",
            "dataType": "json",
            "type": "POST",
            "data":function(data) {
                        data.tgl    = $('#tgl').val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                        },
            "columns": [
                { "data": "create_date" },
                { "data": "line_name" },
                { "data": "style" , 'sortable' : false},
                // { "data": "start_date" , 'sortable' : false},
                // { "data": "export_date" , 'sortable' : false},  
                { "data": "gsd_smv" , 'sortable' : false},
                { "data": "present_sewer" , 'sortable' : false},
                { "data": "present_folding" , 'sortable' : false},
                { "data": "working_hours" , 'sortable' : false},
                { "data": "change_over" , 'sortable' : false},
                { "data": "cum_day" , 'sortable' : false},
                { "data": "target_eff" , 'sortable' : false},
                { "data": "efficiency" , 'sortable' : false},
                { "data": "wft" , 'sortable' : false},
                { "data": "sewing" , 'sortable' : false},
                { "data": "qc" , 'sortable' : false},
                { "data": "folding" , 'sortable' : false},
                { "data": "action" , 'sortable' : false},
            ],

            //buat nomor
            // fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
            //   ,
              "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull )
                  {
                    if(aData.cek==1){
                        $(nRow).css('background-color', '#EC7063');
                        $(nRow).css('color', 'white');
                    }else {
                        $(nRow).css('background-color', '#4BC559');
                        $(nRow).css('color', 'white');
                    }
                  },
        });
        $('#table-dash_filter input').unbind();
        $('#table-dash_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-dash').DataTable().ajax.reload();
        });
        // $('#tgl').change(function(){
        //     // alert("man"+$('#tgl').val());
        //     table.draw();
        // });

        $('#tgl').change(function(event) {
          table.draw();
          if (event.keyCode == 13 && $(this).val() != '') {
              table.draw();
            }
        });

        
    });

    function detail(tgl, id,line,style,smv,sewer,change,cum,target_eff) {
        var url_loading = $('#loading_gif').attr('href');
        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Detail Performance');
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:170px;margin-right=3000px;width=100px'>");
        modal.children('.modal-body').load("dashboard/get_detail?tgl="+tgl+"&id="+id+"&line="+line+"&style="+style+"&smv="+smv+"&sewer="+sewer+"&change="+change+"&cum="+cum+"&target_eff="+target_eff);
        return false;
    }

    function download() {
        var tgl = $('#tgl').val();
        var tanggal = '';
        if(!tgl){
            // alert("harus diisi tanggal");
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd = '0'+dd;
            } 

            if(mm<10) {
                mm = '0'+mm;
            } 

            tanggal = yyyy + '-' + mm + '-' + dd;
        }
        else{
            tanggal = tgl;
        }
        // alert(tgl);
        // alert(tanggal);
        window.location = '<?=base_url();?>'+'/dashboard/download/' + tanggal;
        // $("#myModal").modal('show');
        // $('.modal-title').text("Download Report");
        // $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px;margin-top:50px'>");
        // $('.modal-body').load('dashboard/download/'+tgl);
        return false;
    }
</script>