<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>

<div class="panel panel-default border-grey">
  <!-- <form id="form_download"> -->
 <div class="panel-body">
   <div class="col-xs-1 line">
 		<div class="col-xs-12 title">LINE</div>
 		<div class="col-xs-12 line-number"><?php echo $line_name;?></div>
 	</div>
 	<div class="col-xs-3">
 		<div class="row">
 			<div class="col-xs-6 title2">DATE</div>
 			<div class="col-xs-6 value2"><?=date("d-m-Y");?></div>
 			<div class="col-xs-6 title2">STYLE</div>
 			<div class="col-xs-6 value2"><?php echo $style;?></div>
 			<!-- <div class="col-xs-6 title2">ORDER QTY</div>
 			<div class="col-xs-6 value2"><//$detail['order_qty'];?></div> -->
 			<div class="col-xs-6 title2">SMV</div>
 			<div class="col-xs-6 value2"><?=$detail['smv'];?></div>
 		</div>
 	</div>
 	<div class="col-xs-3">
 		<div class="row">
 			<div class="col-xs-6 title2">TARGET/HOUR</div>
 			<div class="col-xs-6 value2"><?php echo round($target_hour) ?></div>
 			<div class="col-xs-6 title2">CUM DAY</div>
 			<div class="col-xs-6 value2"><?=$detail['cum'];?></div>
 			<div class="col-xs-6 title2">OPERATOR</div>
 			<div class="col-xs-6 value2"><?=$detail['sewer'];?></div>
 		</div>
 	</div>
	<div class="col-xs-3 pull-right">
		<div class="row">
			<br><br><br><br>
			<h3><b>Last Update : <?php 	echo $last_update; ?></b></h3>
		</div>
	</div>
	<div class="col-xs-12">
		<br>
		<div class="col-md-6 pull-right"></div>
		<table class="table table-bordered">
			<thead class="bg-primary">
				<tr>
					<th colspan="2" class="col-xs-2">TIME</th>
					<?php foreach ($jam as $key => $jam): ?>
						<th class="text-center col-xs-1"><?=$jam['name'];?></th>	
					<?php endforeach ?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td rowspan="3" class="col-xs-1" style="vertical-align: middle;">OUTPUT</td>
					<td class="col-xs-1" style="vertical-align: middle;">SEWING (Pcs)</td>
					<?php echo $sumSewing;?><!-- 
					<td class='count' style='background:#CCC'></td> -->
				</tr>
				<tr>
					<td style="vertical-align: middle;">QC (Pcs)</td>
					<?php echo $sumqcendline;?>
				</tr>
				<tr>
					<td style="vertical-align: middle;">FOLDING (Pcs)</td>
					<?php echo $sumfolding;?>
				</tr>
				<!-- <tr>
					<td rowspan="2" style="vertical-align: middle;">EFFICIENCY</td>
					<td style="vertical-align: middle;">TARGET (%)</td>
					<?php //echo $target;?>
				</tr> -->
				<!-- <tr>
					<td style="vertical-align: middle;">ACTUAL (%)</td>
					<?php //echo $actual;?>
				</tr> -->
				<tr>
					<td rowspan="2" style="vertical-align: middle;">DEFECT</td>
					<td style="vertical-align: middle;">INLINE (Pcs)</td>
					<?php echo $sum_inline;?>
				</tr>
				<tr>
					<td style="vertical-align: middle;">ENDLINE (Pcs)</td>
					<?php echo $sum_endline;?>
				</tr>
				<!-- <tr>
					<td rowspan="2" style="vertical-align: middle;">WFT</td>
					<td style="vertical-align: middle;">TARGET (%)</td>
					<?php //echo $wft_target;?>
				</tr> -->
				<!-- <tr>
					<td style="vertical-align: middle;">ACTUAL (%)</td>
					<?php //echo $wft_actual;?>
				</tr> -->
			</tbody>
		</table>
		</div>
  </div>
</div>
<!-- </form> -->
<script>
    $(document).ready(function(){
        $('#sinkron-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po='+$('#po').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/sinkron_po',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    // else if(result.status==3){
                    //     $("#alert_warning").trigger("click", result.pesan);
                    // }


                    // if (response=='sukses') {
                    //      $("#myModal").modal('hide');
                    //      $("#alert_success").trigger("click", 'Sinkron Berhasil');
                    //      $('#refresh').trigger("click");
                    // }
                }
            })
            return false;
        });
        $('#cancel-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po='+$('#po').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/cancel_po',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    // else if(result.status==3){
                    //     $("#alert_warning").trigger("click", result.pesan);
                    // }


                    // if (response=='sukses') {
                    //      $("#myModal").modal('hide');
                    //      $("#alert_success").trigger("click", 'Sinkron Berhasil');
                    //      $('#refresh').trigger("click");
                    // }
                }
            })
            return false;
        });
    });
</script>