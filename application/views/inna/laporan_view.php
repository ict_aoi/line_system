<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <h1 class="text-center"> LAPORAN INNA </h1>
        <br>
        <div class="panel-body">
        
            <div class="row">
                <div class="col-md-4">
                    <div class="panel-group" id="accordion">
                        <!-- <label>Input Tanggal</label> -->
                        <!-- <br> -->
                        <!-- <input class="form-control" type="hidden" id="tgl"> -->
                        <!-- <br> -->
                        <label>Input Term</label>
                        <br>
                        <select class="form-control select2" id="term" name="term">
                            <option value="">-- Pilih --</option>
                            <option value="t">SUDAH TERINTEGRASI</option>
                            <option value="f">BELUM TERINTEGRASI</option>
                        </select>
                        
                        <br>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" id='collapse1' href="#collapseOne">Pencarian Berdasarkan PO</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <input class="form-control" type="text" id="po_number">        
                                    </div>
                                </div>
                            </div>
                        <br>
                        <!-- <button id="download_report" class='btn btn-md btn-warning'><i class='fa fa-th-list'></i> Download Report</button> -->
                        <button onclick='return download()' href='javascript:void(0)' class='btn btn-md btn-primary'><i class='fa fa-file-text'></i> Download Report</button>
                        <div class="pull-right">
                            <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-dash" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">DATE</th>
                        <th class="text-center">LINE</th>
                        <th class="text-center">STYLE</th>
                        <th class="text-center">PO BUYER</th>
                        <th class="text-center">SIZE</th>
                        <th class="text-center">QTY</th>
                        <th class="text-center">REMARK</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>  
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#table-dash').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "searching": false,
            "orderMulti"  : true,
            "ajax":{
                "url": "inna/list_laporan",
                "dataType": "json",
                "type": "POST",
                "data":function(data) {
                            // data.tgl        = $('#tgl').val();
                            data.po         = $('#po_number').val();
                            data.term    = $('#term').val();
                            data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                        },
                            },
                "columns": [
                    { "data": "date" },
                    { "data": "line" },
                    { "data": "style"},  
                    { "data": "poreference" , 'sortable' : false},
                    { "data": "size" , 'sortable' : false},
                    { "data": "qty" , 'sortable' : false},
                    { "data": "remark" , 'sortable' : false},
                ],

            //buat nomor
            // fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
            //   ,
            //   "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull )
            //       {
            //         if(aData.cek==1){
            //             $(nRow).css('background-color', '#EC7063');
            //             $(nRow).css('color', 'white');
            //         }else {
            //             $(nRow).css('background-color', '#4BC559');
            //             $(nRow).css('color', 'white');
            //         }
            //       },
        });
        $('#table-dash_filter input').unbind();
        $('#table-dash_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-dash').DataTable().ajax.reload();
        });
        // $('#tgl').change(function(){
        //     // alert("man"+$('#tgl').val());
        //     table.draw();
        // });

        $('#pencarian').click(function(event) {
          table.draw();
          if (event.keyCode == 13 && $(this).val() != '') {
              table.draw();
            }
        });

        // $('#collapse1').click(function(event) {
        //     $('#style').val('');
        // });
        
        $('#collapse1').click(function(event) {
            $('#po_number').val('');
        });

        
    });

    function download() {
        // var tgl = $('#tgl').val();
        var po = $('#po_number').val();
        // var style = $('#style').val();
        var term = $('#term').val();

        text = 'po='+po+'&term='+term;
        
        window.location = '<?=base_url();?>'+'/inna/download?'+text;
        return false;
    }
</script>
