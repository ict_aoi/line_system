
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box green-bg">
                        <i class="fa fa-thumbs-o-up"></i>
                    <div class="count"><h3 class="no-margin text-semibold" id="total_qcinspect" style="font-size: 24px;"><span><?php echo $persen?>%</span></h3></div>
                    <div class="title"><span class="text-uppercase text-size-mini text-muted">RFT</span></div>
                </div>       
            </div><!--/.col-->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box blue-bg">
                    <i class="fa  fa-check-circle"></i>
                    <div class="count"><h3 class="no-margin text-semibold" id="total_qcinspect" style="font-size: 20px;"><span><?php if($detail!=0) echo $sum->total_random?> </span></h3></div>
                    <div class="title"><span class="text-uppercase text-size-mini text-muted">Total Random Check</span></div>                      
                </div><!--/.info-box-->         
            </div><!--/.col-->  
        </div>
    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-inspect" class="table table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>KODE DEFECT</th>
                        <th>JENIS DEFECT</th>
                        <th>JUMLAH</th>                        
                    </tr>
                </thead>
                <tbody>

                    <?php 
                    if($detail!=0){
                        
                    foreach ($detail as $d): 
                    ?>
                    <tr>
                        <td>
                            <?php echo $d->defect_id ?>
                        </td>
                        <td>
                            <?php echo $d->defect_jenis ?>
                        </td>	
                        <td  style="text-align:center">
                            <?php echo $d->count ?>
                        </td>								
                    </tr>
                    <?php endforeach; 
                    }
                    ?>
                </tbody>
            </table>
        </div>
      </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">

    // $(document).ready(function () {
    //     var sumdaily=$('#sumdaily').val();
    //     var sumdefect=$('#sumdefect').val();
        
    //     $('.total').text(sumdaily);
    //     $('.totaldefect').text(sumdefect);
    //      var table =$('#table-inspect').DataTable({
    //         "processing": true,
    //         "serverSide": true,
    //         // "order": [],
    //         "orderMulti"  : true,
    //         sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
    //         "ajax":{
    //          "url": "inspection_summary_ajax",
    //          "dataType": "json",
    //          "type": "POST",
    //          "data":{  '<?php //echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
    //          'line_id':$('#line_id').val(),
    //           }
    //                        },
    //     "columns": [
    //               {"data" : null, 'sortable' : false},
    //               { "data": "kode",'sortable' : false  },
    //               { "data": "round1" },
    //               { "data": "round2" },

    //            ],
    //            fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); },               
    //     });
    //     $('#table-inspect_filter input').unbind();
    //     $('#table-inspect_filter input').bind('keyup', function(e) {
    //         if (e.keyCode == 13  ) {
    //             table.search($(this).val()).draw();
    //         }
    //         /*if ($(this).val().length == 0 || $(this).val().length >= 3) {
    //             table.search($(this).val()).draw();
    //         }*/
    //     });         
         
    // });
    // function detail_inspect(prosesname,round,nik,proses_id) {
    //   var modal = $('#myModal_ > div > div');
    //   $('#myModal_').modal('show');
    //   modal.children('.modal-title').text();
    //   modal.parent('.modal-dialog').addClass('modal-md');
    //   modal.children('.modal-body').load('Qc_inspect/list_detail_inspection?round='+round+'&nik='+nik+'&proses_id='+proses_id);
    //   return false;  
    // }
    
</script>
