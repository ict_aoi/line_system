<style>
#myModal {
  overflow-x: hidden;
  overflow-y: auto;
}
</style>

<div class="panel panel-default border-grey" >
      <div class="panel-heading">
       <h6 class="panel-title">
         <div class="col-md-2">
           Daily Inline Inspection
         </div>
         <div class="col-sm-3 pull-right">
          <div class="input-group">
              <span>ROUND <input type="text" name="round" id="round" style="border: 0px;background-color: inherit" readonly value="<?=$round; ?>"></span>
          </div>

            <!-- <div class="row">
              <button type="button" class="btn btn-primary" id="btnBack"><i class="fa fa-arrow-circle-left"></i> Back </button>
              <button type="button" class="btn btn-primary" id="btnNext"><i class="fa fa-arrow-circle-right"></i> Next </button>
            </div> -->
         </div>
       </h6>
      </div>
      <br>
      <br>
      <div class="panel-body">
       <div class="table-responsive">
        <div class="col-md-12">

        <!-- <br>
        <div class="col-xs-3"> -->
          <div class="btn-group">
            <button type="button" class="btn btn-primary" onclick="return loadinspect()">Lihat Data</button>
            <button type="button" class="btn btn-success" onclick="return loadsummary()">Detail Defect</button>
          </div>
        <!-- </div> -->
          <table class="table table-striped table-bordered table-hover table-full-width">
           <thead>
             <tr>
              <th width="20%">Style</th>
              <th width="20%">Nama Proses</th>
              <th width="20%">NIK</th>
              <th width="20%">Nama Sewer</th>
              <th width="20%">Size</th>
            </tr>
           </thead>
          <tbody>
            <tr>
              <input type="hidden" value="<?=$record['inline_header_id']?>">
              <td><input type="text" class="form-control" name="style" value="<?=$record['style']?>" id="style" required readonly></input></td>
              <td>
                <div class="input-group">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-info" onclick="return loadproses()"><i class="fa fa-list" /></button>
                </div>
                <input type="text" class="form-control" name="proses" style="left" id="proses" onclick="return loadproses()" readonly="readonly" placeholder="PILIH PROSES">
             </td>
              <td><input type="text" class="form-control" name="nik" id="nik" required autocomplete="off"></td>
              <td><input type="text" class="form-control" name="sewer" id="sewer" required readonly></td>
              <td>
                <select class="form-control" id="selc_size">
                  <option value="">--Pilih Size--</option>
                    <?php 
                        foreach ($size as $sz) {
                      ?>
                          <option value="<?php echo $sz->size; ?>"><?php echo $sz->size; ?></option>
                      <?php 
                        }
                      ?>
                </select>
              </td>
          </tr>
          </tbody>
        </table>
        </div>
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover table-full-width">
         <thead>
          <tr>
           <th>No</th>
           <th>Defect Code</th>
           <th>Size</th>
           <th>ACTION</th>
          </tr>
         </thead>
         <tbody id="tbody-items">
         </tbody>
        </table>
        </div>
       </div>
       <div class="col-md-12">
          <div class="input-group">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-info" onclick=""><i class="fa fa-list" /></button>
                </div>
                <input type="text" class="form-control" style="left" id="grade" onclick="" readonly="readonly" placeholder="">
           </div>
        <br><br>
        </div>
       <form class="frmsave" method="POST">

        <input type="hidden" name="lastproses" id="lastproses-hid">
        <input type="hidden" name="grade" id="grade-hid">
        <input type="hidden" value="<?=$round; ?>" id="round-hid" name="round">
        <input type="hidden" value="<?=$record['inline_header_id']?>" name="header_id" id="header_id">
        <input type="hidden" value="<?=$record['poreference']?>" name="poreference">
        <input type="hidden" value="<?=$record['line_id']?>" name="line_id" id="line_id">
        <input type="hidden" name="style" value="<?=$record['style'] ?>">
        <input type="hidden" name="sewer" id="sewer-hid">
        <input type="hidden" name="proses" id="proses-hid" value="0">
        <input type="hidden" name="items" id="items" value="[]">
        <input type="hidden" name="items_grade" id="items_grade" value="">
        <input type="hidden" name="grade_before" id="grade_before" value="0">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
        </div>
     </form>
      </div>

     </div>


<script type="x-tmpl-mustache" id="items-table">

  {% #item %}
    <tr>
      <td style="text-align:center;">{% no %}</td>
      <td>
        <div id="item_{% _id %}">{% defect_code %}</div>
        <input type="text" class="form-control input-sm hidden" id="itemInputId_{% _id %}"
          value="{% id %}" data-id="{% _id %}"
        >
        <span class="text-danger hidden" id="errorInput_{% _id %}">
          Role Already Selected.
        </span>
      </td>

      <td>
        <div id="item_{% _id %}">{% size %}</div>
        <input type="text" class="form-control input-sm hidden" id="itemInputId_{% _id %}"
          value="{% id %}" data-id="{% _id %}"
        >
        <span class="text-danger hidden" id="errorInput_{% _id %}">
          Role Already Selected.
        </span>
      </td>

      <td width="50px">
        <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="fa fa-trash-o"></i> Del</button>
      </td>
    </tr>
  {%/item%}

   <tr>
    <td width="20px">
      #
    </td>
    <td colspan="3">
      <input type="hidden" id="defect_id">
      <br>
      <div class="row">
        <div class="col-xs-6">
          <button type="button" id="addGood"  class="btn btn-success btn-lg btn-block"><i class="fa fa-check"></i> GOOD</button>
        </div>
        <div class="col-xs-6">
          <button type="button" id="AddDefect" class="btn btn-danger btn-lg btn-block"><i class="fa fa-times"></i> Defect</button>
        </div>
      </div>
      <br>
    </td>

  </tr>
</script>
<button type="button" class="hidden" id="refresh"></button>
<button type="button" class="hidden" id="reset"></button>
<a href="<?php echo base_url('qc_inspect') ?>" id="logo" class="hidden"></a>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/qc_inspect.js"></script>

