<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          
            <div style="overflow-x:auto;overflow-y:auto">
                <table id="table-po" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="20px">NO</th>
                            <th>NAMA LINE</th>
                            <th width="20px">ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
         var table =$('#table-po').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Header_inline/listline",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "line_name" },
                  { "data": "action",'sortable' : false },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
    });
    function select(id,line) {
      
      $('#line_id').val(id);
      $('#line').val(line);
      var line_id = $('#line_id').val();

      $("#myModal").modal('show');
      var url_loading = $('#loading_gif').attr('href');
      $('.modal-title').text("Pilih PO BUYER");
      $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
      $('.modal-body').load('qc_inspect/loadpobuyer?id='+line_id);
      return false;
    }
    
</script>