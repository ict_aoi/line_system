<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table-grade" class="table table-striped table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">NO</th>
                        <th>GRADE NAME</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php 
                        $no =1;
                        $grades = $this->db->get('master_grade')->result();
                        foreach ($grades as $key => $grade) {
                            $temp = '"'.$grade->grade_id.'","'.$grade->grade_name.'","'.$grade->color.'"';
                            echo "<tr onclick='return selectGrade($temp)'>";
                                echo "<td>".$no++."</td>";
                                echo "<td style='background-color:".$grade->color."'>".ucwords($grade->grade_name)."</td>";
                            echo "</tr>";
                        }
                     ?>
                   
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script>
    function selectGrade(id,grade,color) {
        $('#grade-hid').val(id);
        $('#grade').val(grade).css('background-color', color);
        $('#grade').val(grade).css('color', 'white');
        $("#myModal").modal('hide')
    }
</script>