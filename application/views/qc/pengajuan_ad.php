<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->

		<div class="panel panel-default">
			<br>
			<div class="panel-body">
				<div class="col-md-12">
					<a href="<?php echo base_url('Qc_inspect') ?>" class="btn btn-primary">
						<span><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Kembali</b></span>
					</a>
					<div class="pull-right">
						<button type="button" class="btn btn-success" onclick="return loadpengajuan()"><b>Pengajuan AD</b></button>
					</div>
					<br>
					<br>
					<!-- <div style="overflow-x:auto;overflow-y:auto"> -->
					<table id="table-ad" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-center" width="20px">NO</th>
								<th width="170px">TANGGAL PERMINTAAN</th>
								<th width="130px">JAM PERMINTAAN</th>
								<th>STYLE</th>
								<th>PO</th>
								<th width="80px">NAMA LINE</th>
								<th width="100px">STATUS</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<button type="button" class="hidden" id="refresh"></button>

<script>
	$(document).ready(function() {
		var element = document.getElementById("container");
		element.classList.add("sidebar-closed");
		var table = $('#table-ad').DataTable({
			"processing": true,
			"serverSide": true,
			// "order": [],
			"orderMulti": true,
			sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
			"ajax": {
				"url": "listpengajuan",
				"dataType": "json",
				"type": "POST",
				"data": {
					'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
				}
			},
			"columns": [{
					"data": null,
					'sortable': false
				},
				{
					"data": "tanggal_permintaan"
				},
				{
					"data": "jam_permintaan",
					"sortable": false
				},
				{
					"data": "style"
				},
				{
					"data": "poreference"
				},
				{
					"data": "line_name",
					"sortable": false
				},
				{
					"data": "status"
				},
				// { "data": "action",'sortable' : false },
			],
			fnCreatedRow: function(row, data, index) {
				var info = table.page.info();
				var value = index + 1 + info.start;
				$('td', row).eq(0).html(value);
			}
		});

		$('#table-ad_filter input').unbind();
		$('#table-ad_filter input').bind('keyup', function(e) {
			if (e.keyCode == 13 || $(this).val().length == 0) {
				table.search($(this).val()).draw();
			}
			// if ($(this).val().length == 0 || $(this).val().length >= 3) {
			//     table.search($(this).val()).draw();
			// }
		});
		$('#refresh').bind('click', function() {
			$('#table-ad').DataTable().ajax.reload();
		});


		$('#tanggal_pengajuan').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true
		});


		$('.submit_data').submit(function() {
			// var line = $('#line').val();
			var tanggal_pengajuan = $('#tanggal_pengajuan').val();
			// if(!poreference){
			// 	$("#alert_warning").trigger("click", 'PO BUYER TIDAK BOLEH KOSONG');
			// 	$('#result').html(' ');
			if (!tanggal_pengajuan) {
				$("#alert_warning").trigger("click", 'LINE TIDAK BOLEH KOSONG');
				$('#result').html(' ');
			} else {
				var data = new FormData(this);
				// console.log(data);
				var url_loading = $('#loading_gif').attr('href');
				$.ajax({
					url: 'simpan_pengajuan',
					type: "POST",
					data: data,
					contentType: false,
					cache: false,
					processData: false,
					beforeSend: function() {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});
					},
					success: function(data) {
						var result = JSON.parse(data);
						if (result.status == '400') {
							$("#alert_warning").trigger("click", 'Data Sudah Ada');
							$.unblockUI();
						} else {
							$("#alert_success").trigger("click", 'Data Berhasil Diinput');
							$.unblockUI();
							$('#myModal').modal('hide');
							$('#table-ad').DataTable().ajax.reload();
						}
					},
					error: function(response) {
						if (response.status = 500) {
							$("#alert_info").trigger("click", 'Cek Input Data');
							$.unblockUI();
						}
					}
				});
			}
			return false
		})

	});


	function loadpengajuan() {
		var url_loading = $('#loading_gif').attr('href');
		$("#myModal").modal('show');
		$('.modal-title').text("Form Pengajuan");
		$('.modal-body').html("<img src='" + url_loading + "' style='margin-left:150px;margin-right=150px;width=100px'>");
		$('.modal-body').load('loadpengajuan');
		return false;
	}
</script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/resource/qc_inspect_chose.js"></script> -->
