<!-- <style>
#myModal {
  overflow-x: hidden;
  overflow-y: auto;
}
</style> -->
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <div class="col-md-8 pull-right alert alert-success fade in">
              <button data-dismiss="alert" class="close close-sm" type="button">
                  <i class="icon-remove"></i>
              </button>
              <strong>INFORMASI</strong><br> Untuk Menampilkan Pencarian Proses Tekan Enter  <br>
            </div>
              <table id="table-defect" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th class="text-center" width="20px">NO</th>
                          <th>PROSES NAME</th>
                          <th width="20px">ACTION</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
          </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

         var table =$('#table-defect').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Qc_inspect/listproses",
             "dataType": "json",
             "type": "POST",
             "data":{
              '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
              'style':$('#style').val(),'line_id':$('#line_id').val(),'round':$('#round').val()
            }
                           },
        "columns": [
                  {"data" : null, 'sortable' : true},
                  { "data": "prosesname",'sortable' : true },
                  { "data": "action",'sortable' : false },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
              ,
              "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull )
                  {
                    if(aData.lastproses=="t"){
                        $(nRow).css('background-color', '#EC7063');
                        $(nRow).css('color', 'white');
                    }/*else if (aData.jumlah==5) {
                        $(nRow).css('background-color', '#4BC559');
                        $(nRow).css('color', 'white');
                    }*/
                  },
        });
         $('#table-defect_filter input').unbind();
          $('#table-defect_filter input').bind('keyup', function(e) {
              if (e.keyCode == 13 || $(this).val().length == 0 ) {
                  table.search($(this).val()).draw();
              }
              /*if ($(this).val().length == 0 || $(this).val().length >= 4) {
                  table.search($(this).val()).draw();
              }*/
          });
    });
    function pilih(last,id,name) {
      var url_loading = $('#loading_gif').attr('href');
      // var sewer       = $('#sewer-hid').val();
      var header_id = $('#header_id').val();
      var line_id = $('[name=line_id]').val();


      $('#lastproses-hid').val(last);
      $('#proses').val(name);
      $('#proses-hid').val(id);

      $.ajax({
            url:'Qc_inspect/loadsewerproses',
            data:{proses_id:id, line_id:line_id},
            type:'GET',
            dataType: "JSON",
            beforeSend: function () {
              $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
            success:function(response){
              $.unblockUI();

              if (response.length >=1) {

                  $("#myModal").modal('show');
                  var url_loading = $('#loading_gif').attr('href');
                  $('.modal-title').text("Pilih Nama Sewer");
                  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
                  $('.modal-body').load('qc_inspect/viewsewerproses?proses_id='+id+'&header_id='+header_id+'&line_id='+line_id);
                  $('#refresh').trigger("click");
                  $('#reset').trigger("click");

                  return false;

              }else if (response.length == 0) {
                $('#nik').val('');
                $('#sewer').val('');
                $("#myModal").modal('hide');
                $('#nik').focus();
                $('#refresh').trigger("click");
                $('#reset').trigger("click");
                return false;
                // getcolors(header_id,value['sewer_nik']);
              }
             /* else if (response.length == 1) {
                $.each(response, function(index, value) {
                  $('#nik').val(value['sewer_nik']).trigger('change');
                  $("#myModal").modal('hide');
                  $('#reset').trigger("click");
                  return false;
                });

              }*/


            }
        })

    }

    function getcolors(header_id, sewer) {
      var url_loading = $('#loading_gif').attr('href');
      var data = 'idHeader='+header_id+'&nik='+sewer+'&proses_id='+$('#proses-hid').val();

        $.ajax({
            url:'Qc_inspect/getColor',
            data:data,
            type:'GET',
            dataType: "JSON",
            beforeSend: function () {
              $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
            success:function(response){
              $.unblockUI();
                $('.panel-heading').css('background-color',response['color']);
                $('#grade_before').val(response['grade']).trigger('change');
            },
            error:function(response){
                if (response.status==500) {
                  $.unblockUI();
                  $("#alert_warning").trigger("click", 'Kontak ICT');
                }
            },
            // timeout: 15000
        })

      $("#myModal").modal('hide');
      // return false;
    }

</script>
