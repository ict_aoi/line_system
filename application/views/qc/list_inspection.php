
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-inspect" class="table table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>OPERATION NAME</th>
                        <th colspan="5" class="text-center">DEFECT CODE</th>                        
                    </tr>
                    <tr>
                      <th></th>
                      <th></th>
                      <th class="text-center" width="50px">R1</th>
                      <th class="text-center" width="50px">R2</th>
                      <th class="text-center" width="50px">R3</th>
                      <th class="text-center" width="50px">R4</th>
                      <th class="text-center" width="50px">R5</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
      </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
         var table =$('#table-inspect').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Qc_inspect/listinspection_ajax",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
             'line_id':$('#line_id').val(),
              }
                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "prosesname",'sortable' : false  },
                  { "data": "round1" },
                  { "data": "round2" },
                  { "data": "round3" },
                  { "data": "round4" },
                  { "data": "round5" },
               ],
               fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); },               
        });
        $('#table-inspect_filter input').unbind();
        $('#table-inspect_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13  ) {
                table.search($(this).val()).draw();
            }
            /*if ($(this).val().length == 0 || $(this).val().length >= 3) {
                table.search($(this).val()).draw();
            }*/
        });         
         
    });
    function detail_inspect(prosesname,round,nik,proses_id) {
      var modal = $('#myModal_ > div > div');
      $('#myModal_').modal('show');
      modal.children('.modal-title').text();
      modal.parent('.modal-dialog').addClass('modal-md');
      modal.children('.modal-body').load('Qc_inspect/list_detail_inspection?round='+round+'&nik='+nik+'&proses_id='+proses_id);
      return false;  
    }
    
</script>
