<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <br>
        <div class="panel-body">
            <div class="col-md-12">
							<a href="<?php echo base_url('Qc_inspect/pengajuan_ad') ?>" class="btn btn-danger"id="login_pindah">
									<span><i class="fa fa-files-o"></i></span>
									<b>Pengajuan AD</b>
							</a>
							<br>
              <form class="search_header" method="POST">
              <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>LINE</th>
                        <th>PO BUYER</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadline()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="line_id" id="line_id">
                        <input type="text" class="form-control" name="line" id="line" onclick="return loadline()" readonly="readonly" placeholder="PILIH LINE">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>
                    </div>
                    </td>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadpo()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="text" class="form-control" name="poreference" style="left" id="poreference" onclick="return loadpo()" readonly="readonly" placeholder="PILIH PO BUYER">
                        <input type="hidden" id="style" name="style">
                    </td>
                  </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-search"></i> Cari</button>
          </form>
            </div>
        </div>
    </div>
    <div id="result">

    </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/qc_inspect_chose.js"></script>
