<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-defect" class="table table-striped table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">NO</th>
                        <th>DEFECT NAME</th>
                        <th>GRADE</th>
                        <th width="20px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
         var table =$('#table-defect').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Qc_inspect/list_defect",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           },
            "columnDefs": [{
              },
              { "bVisible": false, "aTargets": [2] },
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                    if ( aData['grade'] == "1" )
                    {
                        $('td', nRow).css('background-color', 'green');
                        $('td', nRow).css('color', 'white');
                    }
                    else if ( aData['grade'] == "2" )
                    {
                        $('td', nRow).css('background-color', 'yellow');
                        // $('td', nRow).css('color', 'white');
                    }
                    else if ( aData['grade'] == "3" )
                    {
                        $('td', nRow).css('background-color', 'red');
                        $('td', nRow).css('color', 'white');
                    }
                },
            "columns": [
                      { "data": "no",'sortable' : false },
                      { "data": "defect",'sortable' : false },
                      { "data": "grade",'sortable' : false },
                      { "data": "action",'sortable' : false },
                   ]
        });
    });
    function pilih(id,name,grade) {
      var temp = id+':'+name+':'+grade;
      $('#defect_id').val(temp).trigger('change');
      $("#myModal").modal('hide');
      return false;
    }

</script>