<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="col-md-2">
      <br>

    </div>
    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-po" class="table table-striped table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NIK</th>
                        <th>NAMA</th>
                        <th width="20px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                   foreach($sewer->result_array() as $rows) {
                  ?>
                  <tr>
                    <td><?php echo $rows['sewer_nik']; ?></td>
                    <td><?php echo $rows['sewer_name']; ?></td>
                    <td>
                      <div class="hidden">
                          <span u_nik><?php echo $rows['sewer_nik']; ?></span>
                          <!-- <span u_header><?php echo $rows['inline_header_id']; ?></span> -->
                      </div>
                      <button type="button" class="btn btn-xs btn-success" onclick="pilih_sewer(this)">SELECT</button>
                    </td>
                  </tr>

                  <?php } ?>
                </tbody>
            </table>
          </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#table-po').DataTable({
        'paging':true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
    });
  })
    function pilih_sewer(self) {
      var data = $(self).parent().parent();
      var nik = data.find('[u_nik]').text();
      // var header_id = data.find('[u_header]').text();

      // $('#header_id').val(header_id);
      $('#nik').val(nik).trigger('change');

      // $("#myModal").modal('hide');
      // return false;
    }

</script>