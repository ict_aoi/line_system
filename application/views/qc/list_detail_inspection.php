<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <label for="">Operator Name : <?=$record->row_array()['name']?></label>
            <table id="table-inspect" class="table table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Defect Id</th>
                        <th>Defect Jenis</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  
                        foreach ($record->result() as $key => $r) {
                           echo "<tr>";
                                echo "<td>".$r->defect_id."</>";
                                echo "<td>".$r->defect_jenis."</>";
                           echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>