<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
			<form class="submit_data" method="POST">
				<label>Line</label>
				<br>
				<select class="form-control line_pengajuan" id="line_pengajuan" name="line_pengajuan">
					<option value="">Pilih Line</option>
					<?php
					foreach ($ml->result() as $key => $ml) {
						echo "<option value='" . $ml->master_line_id . "' name='" . $ml->line_name . "'>" . $ml->line_name . "</option>";
					}
					?>
				</select>
			
				<br>
				<label>Nama QC</label>
				<br>
				<input class="form-control" name="nama_qc" id="nama_qc" value = "<?php echo $this->session->userdata('name'); ?>" autocomplete="off" readonly>
		
				<br>
				<label>Tanggal Pengajuan</label>
				<br>
				<input class="form-control" name="tanggal_pengajuan" id="tanggal_pengajuan" value="<?php echo date('Y-m-d') ?>" autocomplete="off" readonly>
				<br>
				<!-- <label>Jam Pengajuan</label>
				<br>
				<input type="time" class="form-control" name="waktu_pengajuan" id="waktu_pengajuan" autocomplete="off" required>
				<br> -->
				<label>Style</label>
				<br>
				<input type="text" class="form-control" name="style_pengajuan" id="style_pengajuan" autocomplete="off" required>
				<br>
				<label>PO</label>
				<br>
				<input type="text" class="form-control" name="po_pengajuan" id="po_pengajuan" autocomplete="off" required>
				<br>
				
			
				<div class="pull-right">
					<button class='btn btn-md btn-success' id='simpan_data'> Simpan</button>
				</div>
			</form>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
	// $("#line_pengajuan").select2();
	
    $(document).ready(function () {
	
		// $("#line_pengajuan").chosen();

		// $('#tanggal_pengajuan').datepicker({
		// 	format: "yyyy-mm-dd",
		// 	autoclose: true
		// });
		
		
		$('.submit_data').submit(function() {
			// var line = $('#line').val();
			var tanggal_pengajuan = $('#tanggal_pengajuan').val();
			
			// if(!poreference){
			// 	$("#alert_warning").trigger("click", 'PO BUYER TIDAK BOLEH KOSONG');
			// 	$('#result').html(' ');
		 	if(!tanggal_pengajuan){
				$("#alert_warning").trigger("click", 'LINE TIDAK BOLEH KOSONG');
				$('#result').html(' ');
			}else{
				var data = new FormData(this);
				// console.log(data);
				var url_loading = $('#loading_gif').attr('href');
				$.ajax({
					url:'simpan_pengajuan',
					type: "POST",
					data: data,
					contentType: false,       
					cache: false,          
					processData:false,
					beforeSend: function () {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});
					},
		            success:function(data){
		                var result = JSON.parse(data);
						if (result.status=='400') {
							$("#alert_warning").trigger("click", 'Data Sudah Ada');
							$.unblockUI();
						}else{
							$("#alert_success").trigger("click", 'Data Berhasil Diinput');
							$.unblockUI();
							$('#refresh').trigger("click");	
							$('#myModal').modal('hide');
							
						}
					},
					error:function(response){
						if (response.status=500) {
							$("#alert_info").trigger("click", 'Cek Input Data');
							$.unblockUI();
						}
					}
				});
			}
			return false
		})
		
	});
    
</script>
