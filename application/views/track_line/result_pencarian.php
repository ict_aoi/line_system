<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->


<!-- <div class="panel panel-default border-grey"> 
<div class="panel-body"> -->
<!-- <div class="row">
<div class="col-md-12"> -->
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Track PO Buyer
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        <div class="panel-body">
        
          <input type="hidden" id="po" value="<?=$po;?>">
          <div class="clearfix"></div><br>
          <div class="box-body">
    
          <div class="clearfix"></div>
          <!-- <div class="table-responsive"><br> -->
            <div style="height:100%; overflow-x:auto;overflow-y:auto">
              <table width="100%" class="table table-striped table-bordered table-hover dataTables" id="table-result">
                  <thead>
                      <tr>
                          <!-- <th class="text-center">DATE</th> -->
                          <th class="text-center">LINE</th>                  
                          <th class="text-center">PO BUYER</th>              
                          <th class="text-center">STYLE</th>
                          <th class="text-center">ARTICLE</th>
                          <th class="text-center">QTY LOADING</th>
                          <th class="text-center">QTY OUTPUT</th>
                          <th class="text-center">BALANCE</th>
                          <th class="text-center">ACTION</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
<!--     
</div>
</div> -->

<!-- </div>
</div> -->

<script>
var url_loading = $('#loading_gif').attr('href');
    var table = $('#table-result').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "hasil_pencarian_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
              //filter
             "data":function(data) {
                        // data.tanggal = $('#tanggal').val();
                        // data.dari    = $('#dari').val();
                        // data.sampai  = $('#sampai').val();
                        data.pobuyer = $('#po').val();
                        // data.balance   = $('#balance').val();
                        // data.filter    = $("input[name=filters]:checked").val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
          //sesuai nested
          //sort dari db
                  // { "data": "date" },
                  { "data": "line" },                 
                  { "data": "poreference" },
                  { "data": "style" },
                  { "data": "article" },
                  { "data": "loading" },
                  { "data": "output" },
                  { "data": "balance" },
                  { "data": "action",'sortable' : false },
               ],
               
        });

function detail(line,po,style,article) {
  var url_loading = $('#loading_gif').attr('href');
  var modal = $('#myModal_ > div > div');
  $('#myModal_').modal('show');
  modal.children('.modal-header').children('.modal-title').html('Detail Track');
  modal.parent('.modal-dialog').addClass('modal-lg');
  modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
  modal.children('.modal-body').load('detail_track?line='+line+'&po='+po+'&style='+style+'&article='+article);
}
</script>