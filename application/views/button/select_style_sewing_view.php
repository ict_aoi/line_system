<!DOCTYPE html>
<html>
<head>
	<title>PILIH STYLE <?=$line;?></title>
	<link type="text/css" rel="stylesheet" href="<?=site_url('assets/css/bootstrap.min.css');?>">
	<link href="<?=Base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
	<link href="<?=Base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
	<script src="<?=Base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?=Base_url();?>assets/js/bootstrap.min.js"></script>
	<style type="text/css">
		body{
			background: #EAEAEA;
			/* -webkit-user-select: none;  Chrome all / Safari all
			      -moz-user-select: none;     Firefox all
			      -ms-user-select: none;      IE 10+
			      user-select: none;     */      /* Likely future */
		}
		label{
			margin: 0px;
			width: 100%;
			height: 50px;
			line-height: 50px;
			font-size: 20px;
		}
		
		.pilih{
			font-size: 40px;
			font-weight: bolder;
		}
		.pilih{
			border:0px solid #000;
			line-height: 70px;
			text-decoration: underline;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">

		<div class="col-xs-2">
			<a href="<?=base_url('button/sewing/'.$mesin.'/'.$factory);?>" class="btn btn-warning btn-block btn-lg"><i class="fa fa-arrow-circle-left">BACK</i></a>
		</div>
		<div class="col-xs-10 text-center"><div class="pilih">Pilih Style <?php echo $line; ?></div></div>
		</div>
		<a href=""></a>
		<div class="row">
			<div class="col-xs-12">
			<table class="table table-striped">
			<form action="<?=base_url('qc/submit_send');?>" method='POST'>
				<?php 
					foreach ($record->result() as $key => $r) {
						echo '<tr>';
							echo '<td style="vertical-align:middle; width:50px;text-align:center;font-size:20px"><a href="'.base_url('Button/select_styleactive_sewing/'.$r->style.'/'.$r->line_id.'/'.$r->factory_id).'">'.$r->style.'</a></td>';
						echo "</tr>";
					}
				?>
			</form>
			</table>
		</div>
		</div>
</body>
</html>
