<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>SEWING <?=$line_name['line_name'];?></title>
	<link type="text/css" rel="stylesheet" href="<?=site_url('assets/css/bootstrap.min.css');?>">
	<link href="<?=Base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
	<link href="<?=Base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
	<script src="<?=Base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?=Base_url();?>assets/js/jquery.idle.min.js"></script>
	<script src="<?=Base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/resource/notification.js"></script>
	<style type="text/css">
		body,
		html{
			background:#EAEAEA;
			max-width: 480px;
			max-height: 320px;
			width: 480px;
			height: 320px;
			border:0px solid #0F0;
			overflow: hidden;
			-webkit-user-select: none;  /* Chrome all / Safari all */
            -moz-user-select: none;     /* Firefox all */
            -ms-user-select: none;      /* IE 10+ */
            user-select: none;          /* Likely future */     

		}
		.head{
			border: 1px solid #000;
			line-height: 40px;
			text-align: center;
			background: #FFF;
		}
		.body{

		}
		.counter{
			border-top:1px solid #000;
			border-bottom:1px solid #000;
			font-size: 70px;
			font-weight: bold;
			height: 150px;
			line-height: 150px;
			background:#FAFAFA;
		}
		.button{
			font-size: 80px;
			height: 100px;
			line-height: 100px;
			cursor: pointer;
		}
		.plus{
			background:#00FF00;
		}
		.minus{
			background:#FF0000;
		}
		.btn-flat{
			border-radius: 0px;			
		}
		.style{
			font-weight: bolder;
			height: 60px;
			
		}
		#counter{
			border: 0px;
			background:#FAFAFA;
			font-size: 70px;
			font-weight: bold;
			width: 528px;
			height: 150px;
		}
		#balance{
			border: 0px;
			background: #FFF;
			
		}
	</style>
</head>
<body onload="startTime()">
	<div class="container-fluid">
	<div class="head row">
		<div class="col-xs-3">
			<a href='<?php echo base_url('Button/sewing/'.$line_name['master_line_id'].'/'.$factory_id['factory_id'])?>'><?=$line_name['line_name']?></a>
		</div>
		<!-- type="hidden" -->
		
		<div class="col-xs-5">
		<input type="hidden" id="qtyidle" value="0">
			<label>Status :</label> 
			<span class="proses hidden"><span class="status label label-primary">Proses Simpan</span></h5></span>
			<span class="selesai hidden"><span class="status label label-danger">Proses Selesai</span></h5></span>
		</div>
		<!-- <div class="col-xs-6"><span>Balance :</span><span id="balance"></span></div> -->
		<div class="col-xs-4">
			<h6>
				<span><?php echo $factory_id['factory_name'];?>-</span>
				<?=date("d/m/Y");?>
				<div class="time"></div>
			</h6>
		</div>
	</div>
	<div class="row head">
		<!-- <div class="col-md-3">			 -->
		<!-- <h3><span class=""><b><//?=$status;?></b></span></h3> -->
		<!-- </div> -->

		<div class="col-xs-12 <?php $ukuran = 'style="height: 15px;font-size:10px"'; echo $ukuran;?>">
			<?php 

				if ($active==0) {
					echo "<a href='".base_url('button/select_style_sewing/'.$line_name['mesin_id'])."/".$factory_id['factory_id']."'><h3>Pilih Style</h3></a>";
				}else{
					echo "<a href='".base_url('button/select_style_sewing/'.$line_name['mesin_id'])."/".$factory_id['factory_id']."'><h3>".$active['style']."</h3></a>";
				}
			?>
		</div>

		<input type="text" class="text-center" id="counter" style="" disabled >
		<div class="col-xs-12"><button type="button" id="plus" style="height: 75px;font-size:40px"  class="btn btn-success btn-lg btn-block">+</button></div>
		<!-- <div class="col-xs-12 button plus"><i class="fa fa-plus"></i></div> -->
		<input type="hidden" id="line" value="<?=$line_name['master_line_id']?>">
		<input type="hidden" id="factory_id" value="<?=$factory_id['factory_id']?>">
		
		<!-- <div class="col-xs-6 col-md-4">
			<div class="time"></div>
		</div> -->
		

	</div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
		
		function counter_sewing() {			
			var idle = 0;
			var data = 'line='+$('#line').val()+'&factory='+$('#factory_id').val()
			+'&qtyidle='+$('#qtyidle').val()+'&idle='+idle;

	        $.ajax({
	            url:'<?=base_url('Button/sewing_status') ?>',
	            data:data,
	            type:'POST',	            
	            success:function(data){
	                var result = JSON.parse(data);
					
					$('#counter').val(result.counter);	
	                // $('#balance').text(result.balance);
	                
	            },
	            error:function(response){
	                if (response.status==500) {
	                  $.unblockUI();
	                }                    
	            }
	        })

		}
		$('#plus').click(function() {
			
			// if (parseInt($('.balance').text())<0) {
				sew_up();
			// }else{
			// 	$("#alert_error").trigger("click", 'PO Dan Size Sudah Balance');
			// }




			// var data = 'line='+$('#line').val()+'&factory='+$('#factory_id').val();	
			// // if (balance!=0) {
			// 	$.ajax({
		    //         url:'<//?=base_url('Button/sewing_up') ?>',
		    //         data:data,
		    //         type:'POST',
		    //         beforeSend: function () {
			// 			var counter = parseInt($('#counter').val());
	        //        		var counter_up = counter+1;
	        //        		$('#counter').val(counter_up);
			// 		},	             
		    //         success:function(data){
		    //             var result = JSON.parse(data);
		    //            	if (result.hasil=='sukses') {
		               		
		    //            	}else{
		    //            		counter_sewing();
		    //            	}
		                 
		    //         },
		    //         error:function(data){
		    //             if (data.status==500) {
		    //               counter_sewing();
		    //             }                    
		    //         }
		    //     })
			// }
		})
		counter_sewing();
		
		// $(document).idle({
			
	    //     onIdle: function(){

		// 		var idle = 1;
		// 		var data = 'line='+$('#line').val()+'&factory='+$('#factory_id').val()+'&idle='+idle;

		// 		$.ajax({
		// 			url:'</?=base_url('Button/sewing_status') ?>',
		// 			data:data,
		// 			type:'POST',	            
		// 			success:function(data){
		// 				var result = JSON.parse(data);
		// 				var count = $('#counter').val();	
						
		// 				if(parseInt(result.counter) != parseInt(count)){
		// 					// $("#alert_warning").trigger("click", 'Data Berbeda');

		// 					swal({
		// 						title: "For your information",
		// 						text: ("simpan data gagal, silahkan input ulang sesuai aktual").toUpperCase(),
		// 						//confirmButtonColor: "#2196F3",
		// 						showConfirmButton: false,
		// 						icon: "info"
		// 					});
		// 					$('#counter').val(result.counter);

		// 				}else{
		// 					$('#counter').val(result.counter);    
		// 				}
						
		// 			},
		// 			error:function(response){
		// 				if (response.status==500) {
		// 				$.unblockUI();
		// 				}                    
		// 			}
		// 		})
				
		// 		// counter_sewing();      
				
	    //     },
	    //     idle: 60000
		// });
		
		$(document).idle({
			onIdle: function(){
				var qtyidle = parseInt($('#qtyidle').val());
				// console.log(qtyidle>0);
				if (qtyidle>0) {
					upsave();
				}      
			},
			idle: 3000
		});
	});

	function sew_up(){
		// var balance  = parseInt($('.balance').text());
		// var wft      = parseInt($('.wft').text());
		var qtyidle = parseInt($('#qtyidle').val()); 
		
		// if (wft>0) {
		// 	if (balance<0) {
				$(".proses").removeClass("hidden");
				$(".selesai").addClass("hidden");
				$('#flag_qtyscan').val('1');
				$('#qtyidle').val(qtyidle+1);
				hitungClient();

			// }else{
			// 	$("#alert_warning").trigger("click", 'po buyer telah balance');
			// }
		// }else{
		// 	$('#scan').val('');
		// 	$("#alert_warning").trigger("click", 'tidak ada tumpukan garment');
		// 	$('#refresh').trigger("click");
		// }
	}

	function hitungClient(){
		// var wip             = parseInt($('.wft').text())-1;
		// var counter_folding = parseInt($('.counter_folding').text())+1;
		var counter_sew = parseInt($('#counter').val())+1;
		// var count =parseInt()
		// var n =$('#counter').val();
		// console.log(n);
		// var qty_scan    = parseInt($('.qty_scan').text())+1;
		// var balance     = parseInt($('.balance').text())+1;
		// $('.wft').text(wip);
		// console.log(counter_sew);
		$('#counter').val(counter_sew);
		// $('.qty_scan').text(qty_scan);
		// $('.balance').text(balance);
	}
	
	function upsave() {
		var url_loading = $('#loading_gif').attr('href');
		// var data    = 'id='+$('#header_id').val()+'&barcode_package='+$('#barcodePackage').val()
		// 				+'&scan='+$('#scan').val()+'&remark='+$('#remark').val()
		// 				+'&poreference='+$('.poreference').text()+'&size='+$('.size').text()+'&style='+$('.style').text()+'&qtyidle='+$('#qtyidle').val();

		// var idle = 1;
		// var data = 'line='+$('#line').val()+'&factory='+$('#factory_id').val()+'&idle='+idle;
		var data = 'line='+$('#line').val()+'&factory='+$('#factory_id').val()+'&qtyidle='+$('#qtyidle').val();	
		console.log(data);

			$.ajax({
				url:'<?=base_url('Button/sewing_up') ?>',
				data:data,
				type:'POST',
				beforeSend: function () {
					// var counter = parseInt($('#counter').val());
					// var counter_up = counter+1;
					// $('#counter').val(counter_up);
					
					$('#qtyidle').val('0');
					$.blockUI({
						message: "<img src='" + url_loading + "' />",
						css: {
							backgroundColor: 'transaparant',
							border: 'none',
						}
					});
					console.log("nunggu");
				},	             
				success:function(data){
					// var result = JSON.parse(data);
					// if (result.hasil=='sukses') {
						
					// }else{
					// 	counter_sewing();
					// }
					var result = JSON.parse(data);
					if (result.status==200) {
						$.unblockUI();
						$("#alert_success").trigger("click", result.pesan);
						$('#refresh').trigger("click");
						$(".proses").addClass("hidden");
						$(".selesai").removeClass("hidden");

						$('#flag_qtyscan').val('0');
					}else if (result.status==500) {
						$("#alert_warning").trigger("click", result.pesan);
						$('#scan').val('');
						$.unblockUI();
						$('#refresh').trigger("click");
						$('#refresh').trigger("click");
					}else if (result.status==501) {
						$("#alert_warning").trigger("click", result.pesan);
						location.reload();
					}
						
				},
				error:function(data){
					// if (data.status==500) {
					// 	counter_sewing();
					// }                    
					if (data.status==500) {
						$.unblockUI();
						$("#alert_warning").trigger("click", 'info ict');
						$('#scan').val('');
					}
				}
			})
				
				// counter_sewing(); 
		// $.ajax({
		// 	url:'Folding/saveidle',
		// 	data:data,
		// 	type:'POST',
		// 	beforeSend: function () {
		// 		$('#qtyidle').val('0');
		// 		$.blockUI({
		// 			message: "<img src='" + url_loading + "' />",
		// 			css: {
		// 				backgroundColor: 'transaparant',
		// 				border: 'none',
		// 			}
		// 		});
		// 	},	            
		// 	success:function(data){
		// 		var result = JSON.parse(data);
		// 		if (result.status==200) {
		// 			$.unblockUI();
		// 			$("#alert_success").trigger("click", result.pesan);
		// 			$('#refresh').trigger("click");
		// 			$(".proses").addClass("hidden");
		// 			$(".selesai").removeClass("hidden");
		// 			$('#flag_qtyscan').val('0');
		// 		}else if (result.status==500) {
		// 			$("#alert_warning").trigger("click", result.pesan);
		// 			$('#scan').val('');
		// 			$.unblockUI();
		// 			$('#refresh').trigger("click");
		// 			$('#refresh').trigger("click");
		// 		}else if (result.status==501) {
		// 			$("#alert_warning").trigger("click", result.pesan);
		// 			location.reload();
		// 		}
		// 	},error:function (data) {
		// 		if (data.status==500) {
		// 		$.unblockUI();
		// 		$("#alert_warning").trigger("click", 'info ict');
		// 		$('#scan').val('');
		// 		}
		// 	}

		// });
	}
</script>

<script>
	function startTime() {
		    var today = new Date();
		    var h = today.getHours();
		    var m = today.getMinutes();
		    var s = today.getSeconds();
		    m = checkTime(m);
		    s = checkTime(s);
		    h = checkTime(h);
		    $('.time').text(h + ":" + m + ":" + s);
		    var t = setTimeout(startTime, 500);
		    //jika jam menunjukan pukul 6||12||15 maka halaman akan di reload
		    if((h == 06 || h == 13 || h == 15) && m == 00 && s == 00){
		    	location.href = location.href;
		    }
		}
		function checkTime(i) {
		    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		    return i;
		}
</script>

<a href="<?php echo base_url('assets/img/Spinner.gif') ?>" id="loading_gif" class="hidden"></a>
<button type="button" class="hidden" id="alert_info"></button>
<button type="button" class="hidden" id="alert_success"></button>
<button type="button" class="hidden" id="alert_error"></button>
<button type="button" class="hidden" id="alert_warning"></button>
</body>

</html>