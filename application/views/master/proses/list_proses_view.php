<style type="text/css">
    .actived{
        border-bottom: 1px dashed #000;
    }
</style>
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> LIST PROSES
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="xs-2">
                    <a href="<?=base_url('master_process/add_master') ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> Tambah</a>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <table id="table-proses" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="20px">NO</th>
                                    <th>NAMA PROSES</th>
                                    <th width="50px">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>                        
                </div>
            </div>
            
        </div>
    </div>
</div>

<script type="text/javascript">
    var table =$('#table-proses').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti"  : true,
        "ajax":{
         "url": "Master_process/listmaster",
         "dataType": "json",
         "type": "POST",
         "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                       },
    "columns": [
              {"data" : null, 'sortable' : false},
              { "data": "proses_name" },
              { "data": "action",'sortable' : false },
           ],
          fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    });
    function edit_proses(id,act){
    if (act==1 ) {
        $('.proses_'+id).prop('disabled',false).addClass('actived');
    }else if (act==2) {
        $('.proses_'+id).prop('disabled',true).removeClass('actived');
        var data = 'name='+$('.proses_'+id).val()+'&id='+id;

        $.ajax({
            url:'Master_process/edit_proses_submit',
            data:data,
            type:'POST',
            success:function(data){
                if(response == 'sukses'){
                  $.unblockUI();
                  $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                  $('#table-proses').DataTable.ajax.reload();
                }else{
                  $.unblockUI();
                  $('#table-proses').DataTable.ajax.reload()
                }
            },
            error:function(response){
                if (response.status=500) {
                  $.unblockUI();
                  $("#alert_info").trigger("click", 'Contact Administrator');
                }                    
            }
        })
    }else{
      $('.proses_'+id).prop('disabled',true).removeClass('actived');
    }
    $('.edit_'+id).toggle();
   }
</script>

