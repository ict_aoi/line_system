<div class="panel panel-default border-grey">
  	<div class="panel-heading">
    	<i class="fa fa-external-link-square"></i> TAMBAH PROSES
	</div>
  <br>

  <div class="panel-body">
    <div class="xs-2">
        <a href="<?=base_url('master_process');?>" class="btn btn-primary"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
        <input type="hidden" value="<?php echo base_url('master_process'); ?>" id="base_url">
    </div><br>
	   <div class="table-responsive">
	    <div class="col-md-12">
	      <table class="table table-striped table-bordered table-hover table-full-width">
	     	<thead>
		      <tr>
		       <th>No</th>
		       <th>Nama Proses</th>
		       <th>ACTION</th>
		      </tr>
	     	</thead>
		     <tbody id="tbody-items">
		     </tbody>
	    </table>
	    </div>
	    
	   </div>
	   <form class="frmsave" method="POST">
	    <input type="hidden" name="items" id="items" value="[]">
	    <div class="col-xs-6 col-md-2 col-md-push-12">
	      <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan <i class="fa fa-floppy-o"></i></button>
	    </div>
	 </form>
  </div>

</div>
<script type="x-tmpl-mustache" id="items-table">  
  {% #item %}
    <tr>
      <td style="text-align:center;">{% no %}</td>
      <td>
        <div id="item_{% _id %}">{% proses_name %}</div>
        <input type="text" class="form-control hidden input-sm item-input" id="itemInputId_{% _id %}" 
          value="{% id %}" data-id="{% _id %}"
        >
        <span class="text-danger hidden" id="errorInput_{% _id %}">
          Role Already Selected.
        </span>
      </td>
      
      <td width="80px">        
		    <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="fa fa-trash-o"></i></button>
      </td>
    </tr>
  {%/item%}

   <tr>
    <td width="20px">
      #
    </td>
    <td colspan="2">
      <input type="hidden" id="defect_id">
      <br>
      <div class="row">
        <div class="col-xs-9">
          <input type="text" id="prosesName" class="form-control input-new" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Proses" >
        </div>
        <div class="col-xs-2">
          <button type="button" id="addItem" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
        </div>
      </div>
      <br>      
    </td>
    
  </tr>
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/master_proses.js"></script>