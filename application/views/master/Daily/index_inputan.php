<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          
			<input type="hidden" id="line_id" name="line_id" value="<?php echo $line;?>">
            <div style="overflow-x:auto;overflow-y:auto">
				<table id="table-edit-daily" class="table table-striped table-bordered table-hover table-full-width" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="text-center">STYLE</th>
							<th class="text-center">WORKING HOURS</th>
							<th class="text-center">ACTION</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        
         var url = '<?php echo base_url(); ?>';
         var table =$('#table-edit-daily').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             	"url": url+"/Daily/list_inputan",
             	"dataType": "json",
             	"type": "POST",
				"data":function(data) {
					data.line   = $('#line_id').val();
					data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
				// '</?php echo $this->security->get_csrf_token_name(); ?>' : '</?php echo $this->security->get_csrf_hash(); ?>' 
				}
			},
        	"columns": [
                  { "data": "style" },
                  { "data": "hours" },
                  { "data": "action",'sortable' : false },
			],
            //   fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
    });
    function edit_daily_inputan(id,jam) {
      
      	var url = '<?php echo base_url(); ?>';

	  	// var modal = $('#myModal_ > div > div');
		// $('#myModal_').modal('show');
		// modal.children('.modal-header').children('.modal-title').html('Edit Inputan');
		// modal.parent('.modal-dialog').addClass('modal-lg');
		// modal.children('.modal-body').html('<center>Loading..</center>');
		// modal.children('.modal-body').load('Daily/load_edit_inputan?id='+id+'&jam='+jam);
	
      $("#myModal").modal('show');
      var url_loading = $('#loading_gif').attr('href');
      $('.modal-title').text("Edit Inputan");
      $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
      $('.modal-body').load('Daily/load_edit_inputan?id='+id+'&jam='+jam);
      	return false;
    }
    
</script>
