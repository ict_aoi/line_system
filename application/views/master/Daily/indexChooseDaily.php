<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">

              <form class="search_style" method="POST">
              <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pilih LINE</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadline()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="line_id" id="line_id">
                        <input type="text" class="form-control" name="line_name" id="line_name" onclick="return loadline()" readonly="readonly" placeholder="PILIH LINE">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>
                    </div>
                    </td>
                  </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-search"></i> Cari</button>
          </form>
            </div>
        </div>
    </div>
    <input type="hidden" name="style_id" id="style_id">
    <input type="hidden" name="order_qty" id="order_qty" value="0">
    <div id="result" style="height: 400px; overflow: scroll;">
        <table id="table-daily" class="table table-striped table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">STYLE</th>
                        <th class="text-center">TOTAL QTY</th>
                        <!-- <th class="text-center">STATUS</th> -->
                        <th width="20px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

    </div>

    <!-- MODAL -->
    <div id="dailyModal" role="dialog" class="modal fade">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="panel no-border">
            <div class="panel-heading">
              <div class="pull-right">
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><i class="fa fa-times" style="font-size:24px;"></i></button>
              </div>
              <div class="panel-title">Daily Input</div>
              <div class="export-options-outsider-container export-options pull-right"></div>
              <div class="clearfix"></div>
            </div>

            <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputStyle" class="col-md-4 control-label required">STYLE</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" id="inputStyle" name="style" required="" readonly>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputQtyOrder" class="col-md-4 control-label required">Order Qty</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" id="inputQtyOrder" name="order_qty" required="" readonly="">
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                      <label for="inputChangeOver" class="col-md-4 control-label required">ChangeOver (CO)</label>
                        <div class="col-md-6">
                          <select class="form-control select2" id="inputChangeOver" name="change_over" required="">
                            <option value="">-- Pilih CO --</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="REPEAT">REPEAT</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputGsdSmv" class="col-md-4 control-label required">Gsd Smv</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right" id="inputGsdSmv" name="gsd_smv" required="" onkeypress="return isNumberDot(event);">
                          </div>
                      </div>
                    <div class="form-group">
                      <hr class="col-sm-11">
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputPresentSewer" class="col-md-4 control-label required">Present Sewer</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right" id="inputPresentSewer" name="present_sewer" required="" onkeypress="return isNumberKey(event);">
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputWorkingHours" class="col-md-4 control-label required">Working Hours</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right" id="inputWorkingHours" name="working_hours" required="" onkeypress="return isNumberDot(event);">
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputTargetOutput" class="col-md-4 control-label required">Target Output</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right" id="inputTargetOutput" name="target_output" required="" onkeypress="return isNumberDot(event);">
                          </div>
                      </div>
                    </div>

                  </div>
                </div>
            </div><!-- END PANEL BODY -->

            <div class="panel-footer text-center">
              <button type="button" class="btn btn-primary" onclick="submitDaily()" id="btnSimpanDaily">Simpan</button>
              <button type="button" data-dismiss="modal" class="btn btn-danger">Kembali</button>
            </div>

          </div>
        </div>
      </div>
    </div>
        <!-- GUEST MODAL -->

</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/dailyChoose.js"></script>