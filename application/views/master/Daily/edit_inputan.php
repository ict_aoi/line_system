<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          
			<input type="hidden" id="detail_id" name="detail_id" value="<?php echo $id;?>">
			<div class="form-group">
				<div class="col-md-12">
				<label class="col-md-6 control-label">Working Hours</label>
					<div class="col-md-6">
					<input type="text" class="form-control text-right" id="jam_kerja" name="jam_kerja" value="<?php echo $jam;?>" required>
					</div>
				</div>
			</div>
			<br><br>
			<button class="btn btn-primary col-md-12" id="edit_inputan"><i class="fa fa-floppy-o"> Simpan</i></button>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        
		var url = '<?php echo base_url(); ?>';

		$('#edit_inputan').click(function () {
			var detail = $('#detail_id').val();
			var jam_kerja = $('#jam_kerja').val();
			$.ajax({
	            url:'Daily/update_inputan',            
	            data: {detail_id:detail, jam:jam_kerja },
	            type:'POST',
	            beforeSend: function () {
		          $.blockUI({
		              message: "<img src='" + url_loading + "' />",
		              css: {
		                backgroundColor: 'transaparant',
		                border: 'none',
		              }
		            });
		          },	            
	            success:function(response){
					$.unblockUI();
					$("#alert_success").trigger("click", "Update Berhasil");
					$('#myModal').modal('hide');			
					window.location.href=location.href;   
	            },
	            error:function(response){
					$.unblockUI();
					$("#alert_warning").trigger("click", "Karton sudah di revisi ");                        
	            }
	        })
		});

    });
    
</script>
