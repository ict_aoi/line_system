
<!-- <div class="panel panel-default"> -->
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Daily</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Home</a></li>
        <li><i class="fa fa-bars"></i>Input Daily</li>
      </ol>
    </div>
  </div>
<!-- </div> -->
<div class="row">

<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          <div class="col-md-6">

              <!-- <form class="search_style" method="POST"> -->
              <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pilih LINE</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadline()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="line_id" id="line_id">
                        <input type="text" class="form-control" name="line_name" id="line_name" onclick="return loadline()" readonly="readonly" placeholder="PILIH LINE">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>
                    </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              <button class="btn btn-primary btn-lg btn-block" id="search"><i class="fa fa-search"></i> Cari</button>
          <!-- </form> -->
          </div>        

          <!-- <div class="col-md-6">
            <button class="btn btn-warning btn-lg btn-block" id="input_jam"><i class="fa fa-clock-o"></i> Input Jam</button>
          </div> -->
        </div>
    </div>
     <div class="panel panel-default">
      <div class="panel-heading">
       <h6 class="panel-title">
         <div class="col-md-2">
           Detail Style
         </div>
         <div class="col-sm-3 pull-right">
          <div class="input-group">              
          </div>
         </div>
       </h6>
      </div>
        <div class="panel-body">        
        <div class="clearfix">  </div>
          <div class="col-md-6">
            <label>Input Jam Mulai</label>
							<!-- <div class='input-group date' id='datetimepicker3'>
									<input type="text" class="form-control" name="jam_mulai" id="jam_mulai" placeholder="Klik Logo Jam Untuk Input">
									<span class="input-group-addon">
											<span class="glyphicon glyphicon-time"></span>
									</span>
							</div> -->
							<input type="text" class="form-control" name="jam_mulai" id="jam_mulai" placeholder="Klik untuk input jam mulai">
						<!-- </div>        
						<div class="col-md-6"> -->
						<br>
							<!-- <label>&nbsp</label> -->
							<button class="btn btn-primary btn-block" id="jam_submit"><i class="fa fa-clock-o"></i> Submit</button>
          </div>
					<div class="col-md-6">
						<br>
						<br>
						<br>
						<br>
						<button class="btn btn-warning btn-block" id="edit_input"><i class="fa fa-pencil"></i> Inputan</button>
					</div>
          <div class="row"></div>
          <br>
          <div class="col-md-12">
            
            <div id="result"></div>
           
          </div>
        <!-- </div> --></div>
    </div>
    <div class="hidden tambah_pop">
      <form method="POST" class="submittambah" >
        <div class="panel no-border">

            <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputStyle" class="col-md-4 control-label required">STYLE</label>
                          <div class="col-md-8">
                            <input type="hidden" class="line_id_hid" name="line_id">
                            <input type="text" class="form-control style" id="style" name="style" required="" readonly>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputQtyOrder" class="col-md-4 control-label required">Order Qty</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control order" id="inputQtyOrder" name="order_qty" required="" readonly="">
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                      <label for="inputChangeOver" class="col-md-4 control-label required">ChangeOver (CO)</label>
                        <div class="col-md-6 select_co">
                          <select class="form-control select2" id="inputChangeOver" name="change_over" required="">
                            <option value="">-- Pilih CO --</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="REPEAT">REPEAT</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputGsdSmv" class="col-md-4 control-label required">Gsd Smv</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right gsd_smv" id="inputGsdSmv" name="gsd_smv" required="" onkeypress="return isNumberDot(event);">
                          </div>
                      </div>
                    <div class="form-group">
                      <hr class="col-sm-11">
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputPresentSewer" class="col-md-4 control-label required">Present Sewer</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right present_sewer" id="inputPresentSewer" name="present_sewer" required="" onkeypress="return isNumberKey(event);">
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputPresentSewer" class="col-md-4 control-label required">Present IPF</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right present_folding" name="present_folding" required="" onkeypress="return isNumberKey(event);">
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputWorkingHours" class="col-md-4 control-label required">Working Hours</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right working_hours" id="inputWorkingHours" name="working_hours" required="" onkeypress="return isNumberDot(event);">
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label for="inputTargetOutput" class="col-md-4 control-label required">Komitmen PCS</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control text-right target_output" id="inputTargetOutput" name="target_output" required="" placeholder="0" onkeypress="return isNumberDot(event);">
                          </div>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="clearfix"></div>
                <!-- <div class="form-group">
                  <div class="col-md-4 center-block">
                    <button type="button" class="btn btn-primary" onclick="submitDaily()" id="btnSimpanDaily">Simpan</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Kembali</button>
                  </div>
                </div> -->
                <br>
                <div class="col-md-12 text-center"> 
                   <button type="submit" class="btn btn-primary" id="btnSimpanDaily"><i class="fa fa-floppy-o"> Simpan</i></button>
                   <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"> Close</i></button>
                </div>
               
                  <!-- <button type="button" data-dismiss="modal" class="btn btn-danger">Kembali</button> -->
            </div><!-- END PANEL BODY -->

            

          </div>
        </div>
      </form>
    </div>
</div>
<button type="button" class="hidden" id="refresh"></button>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/dailyChoose.js"></script>

<a href="<?php echo base_url('assets/img/Spinner.gif') ?>" id="loading_gif" class="hidden"></a>
<script>
  $("#jam_mulai").AnyTime_picker(
    { format: "%H:%i", labelTitle: "Jam",
      labelHour: "Jam", labelMinute: "Menit" } 
  );

  // $('#jam_mulai').datetimepicker({
  //   format: 'LT'
  // });

  var url_loading = $('#loading_gif').attr('href');
  $('#jam_submit').click(function () {
    var line_id = $('#line_id').val();
    var jam_mulai = $('#jam_mulai').val();
    
    var data = 'line_id='+line_id+'&jam='+jam_mulai;
               
		if (!line_id) {
			$("#alert_error").trigger("click", 'line tidak boleh kosong');
		}else{
          
        var datas = 'line_id='+line_id+'&jam='+jam_mulai;
        // var data = 'po='+$('#po').val()+'&line_id='+$('#line_id').val(); 
        $.ajax({
          url:'Daily/submit_jam_kerja',
          data:datas,
          type:'POST',
          beforeSend: function () {
    
            $.blockUI({
              message: "<img src='" + url_loading + "' />",
              css: {
                backgroundColor: 'transaparant',
                border: 'none',
              }
            });
          },                  
          success:function(data){
              var result = JSON.parse(data);
              if (result.status==200) {
                $.unblockUI();
                $("#alert_success").trigger("click", 'Simpan Data Berhasil');
              }else if (result.status==500){
                $.unblockUI();
                $("#alert_error").trigger("click", 'Simpan Gagal');
              }
							else if (result.status==100){
                $.unblockUI();
                $("#alert_error").trigger("click", 'Data Sudah Diinput');
              }
                
          },
          error:function(data){
              // if (data.status==500) {
              //   $("#alert_error").trigger("click", 'Info ICT');
              // }                    
          }
      });
		}
  });
</script>
