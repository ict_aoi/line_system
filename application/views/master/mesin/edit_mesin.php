<style type="text/css">
 
/* .modal-lg {
     width: 1000px;
    } */
</style>


<!-- <form class="sinkron" method="POST"> -->
    <!-- <div class="row"> -->
        <form class="edit_machine_sew" method="POST">    
            <input type="hidden" name="mesin_id" value="<?php echo $mesin['mesin_id'];?>" autocomplete="off" class="form-control">
            <label>Kode Mesin</label>
            <input type="text" name="kode" value="<?php echo $mesin['nama_mesin'];?>" autocomplete="off" class="form-control" required>
            <br>
            <label>Nama Adidas</label>
            <input type="text" name="adidas" value="<?php echo $mesin['adidas_name'];?>" autocomplete="off" class="form-control" required>
            <br>
            <label>Nama Grup Mesin</label>
            <input type="text" name="grup_mesin" value="<?php echo $mesin['machine_group_name'];?>" autocomplete="off" class="form-control" required>
            <br>
            <label>Nama Kategori Mesin</label>
            <input type="hidden" name="kategori_mesin_hidden" id="kategori_mesin_hidden" value="<?php echo $mesin['kategori_mesin'];?>" autocomplete="off" class="form-control">
			<select class="form-control select" id="kategori_mesin" name="kategori_mesin" required>
                <option value="">-- Pilih --</option>
                <option value="SPECIAL">SPECIAL</option>
                <option value="NORMAL">NORMAL</option>
            </select>
            <!-- <input type="text" name="kategori_mesin" value="<?php //echo $mesin['kategori_mesin'];?>" autocomplete="off" class="form-control" required> -->
            <br>
			<label>Machine Delay (%)</label>
			<input type="number" name="machine_delay" placeholder="Masukan Machine Delay (%)" autocomplete="off" class="form-control" required>
			<br>
            <div class="pull-right">
                <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> UPDATE</button>
            </div>
        </form>
    <!-- </div> -->
<!-- </form> -->
<script>
    $(document).ready(function(){
		var kategori = $("#kategori_mesin_hidden").val();
		$("#kategori_mesin").val(kategori);

        $('.edit_machine_sew').submit(function(e){
            e.preventDefault();
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>master_mesin/edit_mesin',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    else if(result.status==3){
                        $("#alert_warning").trigger("click", result.pesan);
                    }
                }
            })
        return false;
        });
    });
</script>
