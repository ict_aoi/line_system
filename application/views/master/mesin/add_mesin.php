<form class="add_machine_sew" method="POST">
    <label>Kode Mesin</label>
    <input type="text" name="kode" placeholder="Masukan Kode Mesin" autocomplete="off" class="form-control" required>
    <br>
    <label>Nama Adidas</label>
    <input type="text" name="adidas" placeholder="Masukan Nama Adidas" autocomplete="off" class="form-control" required>
    <br>
    <label>Nama Grup Mesin</label>
    <input type="text" name="grup_mesin" placeholder="Masukan Nama Grup Mesin" autocomplete="off" class="form-control" required>
    <br>
    <label>Nama Kategori Mesin</label>
    <select class="form-control select" id="kategori_mesin" name="kategori_mesin" required>
        <option value="">-- Pilih --</option>
        <option value="SPECIAL">SPECIAL</option>
        <option value="NORMAL">NORMAL</option>
    </select>
    <!-- <input type="text" name="kategori_mesin" placeholder="Masukan Nama Kategori Mesin" autocomplete="off" class="form-control" required> -->
    <br>
    <label>Machine Delay (%)</label>
    <input type="number" name="machine_delay" placeholder="Masukan Machine Delay (%)" autocomplete="off" class="form-control" required>
    <br>
    <div class="pull-right">
        <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> SIMPAN</button>
    </div>
</form>
<script>
    $(document).ready(function(){
        $('.add_machine_sew').submit(function(e){
            e.preventDefault();
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>master_mesin/save_mesin',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    else if(result.status==3){
                        $("#alert_warning").trigger("click", result.pesan);
                    }
                }
            })
        return false;
        });
    });
</script>
