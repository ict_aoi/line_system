<div class="col-md-4">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list" aria-hidden="true"></i> LIST STYLE
    </div>
    <div class="panel-body">
        <div class="input-group">
          <input type="hidden" id="base_url" value="<?=base_url('master_workflow')?>">
            <input type="text" class="form-control" name="style" id="style" readonly="readonly" value="<?=$style['style'] ?>" placeholder="PILIH STYLE">            
        </div>
        <label></label>
        <div class="input-group">
          <label for="">INPUT GSD SMV</label>
          <input type="number" class="form-control" name="gsd" id="gsd" placeholder="Masukan GSD SMV" value="<?=$style['gsd_smv'] ?>" step="any"  required> 
        </div>
        
    </div> 
    </div>

</div>
</div>
<div class="col-md-8">
    <div class="panel panel-default border-grey">
      <div class="panel-heading">
        <i class="fa fa-th-list" aria-hidden="true"></i> LIST WORKFLOW
    </div>
      <br>
      <br>
      
      <div class="panel-body">
       <div class="table-responsive">
        <div class="col-md-12">
         
        </div>
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover table-full-width">
         <thead>
          <tr>
           <th>No</th>
           <th style="width:400px;">Nama Proses</th>
           <th style="width:50px;">Last Proses</th>
           <th style="width:100px;">Cycle Time</th>
           <th style="width:50px;">Critical Proses</th>
           <th style="width:130px;">ACTION</th>
          </tr>
         </thead>
         <tbody id="tbody-items">
         </tbody>
        </table>
        </div>
        <div class="row">
					<div class="col-xs-12 pull-right">
            <label for=""><h4>INPUT CYCLE TIME</h4></label>
            <input type="number" class="form-control" name="cycle" id="cycle" placeholder="Masukan Cycle Time" step="any" required> 
          </div>
          <div class="col-xs-12 pull-right">
						<div class="row">
							<div class="col-xs-6">
								<label for=""><h4>SELECT PROSES</h4></label><br>
								<input type="checkbox" id="cklastproses"><span> Last Proses</span>
							</div>
							<div class="col-xs-6">
								<label for=""><h4>SELECT KATEGORI PROSES</h4></label><br>
								<input type="checkbox" id="ckcritical"><span> Critical Proses</span>
							</div>
						</div>
						<br>
            <select class="proses_choses form-control" id="prosesName" name="proses" onchange="return addProses()">
              <option value="" id="user_id" selected="selected">Pilih Proses</option>
              <?php 
                $master = $this->db->get('master_proses');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_proses_id."' name='".$master->proses_name."'>".$master->proses_name."</option>";
                }
              ?>
            </select>
          </div>
        </div>
    <br><br><br><br><br>
       </div>
       <form class="frmupdate" method="POST">
        <input type="hidden" name="items" id="items" value="<?php echo htmlspecialchars($record); ?>">
        <input type="hidden" name="style-hid" id="style_hid" value="<?=$style['style'] ?>">
        <input type="hidden" name="gsd-hid" id="gsd-hid" value="<?=$style['gsd_smv'] ?>">
      
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary btn-lg btn-block">update</button>
        </div>
     </form>
      </div>

     </div>
</div>
      

<script type="x-tmpl-mustache" id="items-table">  
  {% #item %}
    <tr>
      <td style="text-align:center;">{% no %}</td>
      <td>
        <div id="item_{% _id %}">{% proses_name %}</div>
        <input type="text" class="form-control input-md hidden" id="itemInputId_{% _id %}" 
          value="{% id %}" data-id="{% _id %}"
        <span class="text-danger hidden" id="errorInput_{% _id %}">
        </span>
      </td>
      <td>
      
        <input type="checkbox" class="checkbox_item"  disabled id="check_{% _id %}" data-id="{% _id %}" {% #lastproses %} checked="checked" {% /lastproses %}>
      
      </td>
      
      <td>
        <input type="number" class="form-control" name="cycle" id="in_cycle_{% _id %}" value="{% cycle_time %}" step="any" disabled="disabled" required> 
        <!-- <div id="cycle_{% _id %}">{% cycle_time %}</div> -->
      </td>      
			
			<td>
      
				<input type="checkbox" class="checkbox_item"  disabled="disabled"  id="cekcrit_{% _id %}" data-id="{% _id %}" {% #cekcrit %} checked="checked" {% /cekcrit %}>
			
			</td>

      <td>
            <button type="button" id="edit_cycle_{% _id %}" data-id="{% _id %}" class="btn btn-warning btn-edit-item"><i class="fa fa-pencil"></i></button>
            
            <button type="button" style="display:none;" id="save_cycle_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-save-item"><i class="fa fa-save"></i></button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="fa fa-trash-o"></i></button>
      </td>
    </tr>
  {%/item%}

   <tr>
    <td width="20px">
      #
    </td>
    <td>
      <input type="hidden" id="prosesId">
      <br>      
    </td>

    <td>
    </td>
    
    <td>
      <input type="hidden" id="cycleId">
      <br>      
    </td>

		<td>
		</td>

    <td>     
    </td>
    
    
  </tr>
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/resource/master_workflow.js"></script>
<!-- <input type="text" id="prosesName" class="form-control input-new" placeholder="Nama Proses"> -->
