<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
      <div class="panel-heading">
            <i class="fa fa-wrench"></i> LIST MASTER WORKFLOW
      </div>
		<br>
        <div class="col-xs-12">
        	<a href="<?=base_url('master_workflow/addworkflow'); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah </a>
            <!-- <button id="tambah" class="btn btn-primary" onclick="return tambah()">Tambah</button> -->
        </div>
        <br>
        <div class="panel-body">
            <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">NO</th>
                        <th>STYLE</th>
                        <th width="50px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
   	</div>
</div>
</div>
<script type="text/javascript">
    var table =$('#table-list').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti"  : true,
        "ajax":{
         "url": "Master_workflow/listmaster_ajax",
         "dataType": "json",
         "type": "POST",
         "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                       },
    "columns": [
              {"data" : null, 'sortable' : false},
              { "data": "style" },
              { "data": "action",'sortable' : false },
           ],
          fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/master_style.js"></script>