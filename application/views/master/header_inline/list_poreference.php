<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-po" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">NO</th>
                        <th>PO BUYER</th>
                        <th>STYLE</th>
                        <th>ARTICLE</th>
                        <th width="20px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
         var table =$('#table-po').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "listpoajax",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "poreference" },
                  { "data": "style" },
                  { "data": "article" },
                  { "data": "action",'sortable' : false },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
      
        $('#table-po_filter input').unbind();
        $('#table-po_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        }); 
    });

    function select(po,style,article) {
      var url_loading = $('#loading_gif').attr('href');
      $('#poreference').val(po);
      $('#style').val(style);
      $('#article').val(article);
      var data = 'style='+style+'&po='+po+'&article='+article;
      $.ajax({
            url:'getdetailstyle',
            data:data,
            type:'GET',
            success:function(data){
              if (data=='gagal') {
                $("#alert_error").trigger("click", 'QTY PO ini sudah di alokasikan semua,silahkan pilih po lain');
                $('#poreference').val('');
                $('#style').val('');
              }else{
                var result = JSON.parse(data);
                $('#datestartschedule').val(result.master_mo.datestartschedule);
                $('#datefinishschedule').val(result.master_mo.datefinishschedule);
                /*$('#exportdate').val(result.master_mo.kst_statisticaldate);*/
                $('#orderqty').val(result.sumqtyorder);
                $('#result').html(result.draw);
              }
            }
        })
      $('#myModal_').modal('hide');
    }

    
</script>