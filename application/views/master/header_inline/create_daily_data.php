<div class="panel panel-default border-grey">
	<div class="panel-body">  
		<form class="create_daily" method="POST">
			<input type="hidden" name="inline_header_id" value="<?=$header['inline_header_id'] ?>">
			<label>STYLE</label>
		    <input type="text" name="style"  id="style" class="form-control" value="<?=$header['style'] ?>" readonly="readonly">
		    <label>PO BUYER</label>
		    <input type="text" name="style"  id="poreference"  class="form-control" value="<?=$header['poreference'] ?>" readonly="readonly">
		    <label>TODAY</label>
		    <input type="text" name="today"  id="today" value="<?=date('Y-m-d') ?>" class="form-control" required" readonly="readonly" >   
		    <label>PRESENT SEWER</label>
		    <input type="number" name="present"  id="present" value="<?=$linedetail['present_sewer'] ?>" autocomplete="off" class="form-control" required>
		    <label>WORKING HOURS</label>
		    <input type="number" name="working_hours"  id="working_hours" value="<?=$linedetail['working_hours'] ?>" autocomplete="off" class="form-control" required>
		    <div class="clearfix"></div><br>
		    <button type="submit" name="save" class="btn btn-success btn-lg btn-block"><i class="fa fa-save"></i> Save</button>
		</form>
	</div>
</div>
<script>
	$(function() {
 //    var datepicker = $('#today');

	// if (datepicker.length > 0) {
	//      datepicker.datepicker({
	//       format: "yyyy-mm-dd",
	//       startDate: new Date()
	//     });
	// }
	$('.create_daily').submit(function() {
		var data = new FormData(this);
            
        $.ajax({
            url:'Header_inline/createdaily_submit',
            type: "POST",
            data: data,
            contentType: false,       
            cache: false,          
            processData:false,
            success: function(response){
                if (response=='sukses') {
                    $('#myModal').modal('hide');
                    $("#alert_success").trigger("click", 'Good Job');
                    $('#table-list').DataTable().ajax.reload();
                }else{
                    $("#alert_error").trigger("click", 'Data Sudah Ada, Cek Kembali Inputan');
                }
            },
	          error:function(response){
	            if (response.status=500) {
	              $("#alert_info").trigger("click", 'Failed');
	            }
	        }

        });
		return false;
	})
  });
</script>