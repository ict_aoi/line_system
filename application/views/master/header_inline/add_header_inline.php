<form class="add_header" method="POST">
    <label>LINE</label>
    <div class="input-group">
        <div class="input-group-btn">
            <button type="button" class="btn btn-info" onclick="return loadline()"><i class="fa fa-list"></i></button>
        </div>
        <input type="hidden" name="line_id" id="line_id">
        <input type="text" class="form-control" name="line" id="line" onclick="return loadline()" readonly="readonly" placeholder="PILIH LINE">
        <div class="input-group-btn">
            <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
        </div>                
    </div>
    <label>PO BUYER</label>
    <div class="input-group">
        <div class="input-group-btn">
            <button type="button" class="btn btn-info" onclick="return search()"><i class="fa fa-list"></i></button>
        </div>
        <input type="text" class="form-control" name="poreference" id="poreference" onclick="return search()" readonly="readonly" placeholder="PILIH PO BUYER">
        <div class="input-group-btn">
            <button type="button" class="btn btn-default btn-icon" id="clear"><span class="text-danger"><span class="fa fa-times"></span></span></button>
        </div>                
    </div>
    <label>STYLE</label>
    <input type="text" name="style"  id="style" placeholder="Masukan STYLE" autocomplete="off" class="form-control" required readonly="readonly">
    <label>DATE START SCHEDULE</label>
    <input type="text" name="datestartschedule"  id="datestartschedule" autocomplete="off" class="form-control" required readonly="readonly">   
    <label>DATE FINISH SCHEDULE</label>
    <input type="text" name="datefinishschedule"  id="datefinishschedule" autocomplete="off" class="form-control" required readonly="readonly">
    <label>EXPORT DATE</label>
    <input type="text" name="exportdate"  id="exportdate" autocomplete="off" class="form-control" required readonly="readonly">  
    <label>ORDER QTY</label>
    <input type="text" name="orderqty"  id="orderqty" autocomplete="off" class="form-control" required readonly="readonly">
    <label>START DATE ACTUAL</label>
    <input type="text" name="datestartactual"  id="datestartactual" placeholder="Pilih Tanggal" autocomplete="off" class="form-control" required>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-1">
            NIM
        </label>
        <div class="col-sm-4">
            <input type="text" name="nim" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
        </div>
        <label class="col-sm-2 control-label" for="form-field-1">
            NIM
        </label>
        <div class="col-sm-4">
            <input type="text" name="nim" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
        </div>
        <label class="col-sm-2 control-label" for="form-field-1">
            NIM
        </label>
        <div class="col-sm-4">
            <input type="text" name="nim" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
        </div>
        <label class="col-sm-2 control-label" for="form-field-1">
            NIM
        </label>
        <div class="col-sm-4">
            <input type="text" name="nim" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
        </div>
    </div>
    <!-- <label>TARGET QTY</label>
    <input type="number" name="targetqty"  id="targetqty" autocomplete="off" class="form-control" required> -->
    <label>CHANGE OVER CATEGORY</label>
        <?php echo form_dropdown('CO', array('A' => 'A', 'B' => 'B','C'=>'C','D'=>'D','REPEAT'=>'REPEAT'), null, "class='form-control'"); ?>      
    <div class="form-group">
        <label class="col-sm-2 control-label">

        </label> 
    </div>
    <button type="submit" name="save" class="btn btn-success btn-lg btn-block"><i class="fa fa-save"></i> Save</button>
</form>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/inline_header.js"></script>