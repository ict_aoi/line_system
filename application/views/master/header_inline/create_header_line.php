
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa fa-bars"> Create Header Line</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Data Master</a></li>
        <li><i class="fa fa-bars"></i>Header Line</li>
      </ol>
    </div>
  </div>
</div>
<style type="text/css">
    .actived{
        border-bottom: 1px dashed #000;
    }
</style>
<div class="panel panel-default border-grey"> 
    <div class="panel-body"> 
      <form class="add_header" method="POST">
        <div class="form-group"> 
            <div class="row"> 
              <div class="col-md-6">
                <label>LINE</label>
                <div class="input-group">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-info" onclick="return loadline()"><i class="fa fa-list"></i></button>
                    </div>
                    <input type="hidden" name="line_id" id="line_id">
                    <input type="text" class="form-control" name="line" id="line" onclick="return loadline()" readonly="readonly" placeholder="PILIH LINE">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                    </div>                
                </div>
                <span class="label label-danger">*Required</span><br>
              </div> 
     
              <div class="col-md-6"> 
                 <label>PO BUYER</label>
                <div class="input-group">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-info" onclick="return search()"><i class="fa fa-list"></i></button>
                    </div>
                    <input type="text" class="form-control" name="poreference" id="poreference" onclick="return search()" readonly="readonly" placeholder="PILIH PO BUYER">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default btn-icon" id="clear"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                    </div>                
                </div>
                <span class="label label-danger">*Required</span><br>             
              </div> 
            </div> 
        </div>         
        <div class="form-group"> 
            <div class="row">      
              <div class="col-md-6">
                <label>STYLE</label>
              <input type="text" name="style"  id="style" placeholder="Masukan STYLE" autocomplete="off" class="form-control" required readonly="readonly">
              </div>  
              <div class="col-md-6">
                <label>ARTICLE</label>
                <input type="text" name="article"  id="article" autocomplete="off" class="form-control" required readonly="readonly">
            </div>
            </div> 
        </div>
        <div class="form-group"> 
            <div class="row">      
              <div class="col-md-6">
                <label>ORDER QTY</label>
                <input type="text" name="orderqty"  id="orderqty" autocomplete="off" class="form-control" required readonly="readonly">
            </div>
            </div> 
        </div>            
        <br>
        <div class="form-group" class="col-md-12">
            <label for="">SIZE DETAIL</label>
            <div id="result" class="col-md-12">
  
            </div>
        </div>
        <div class="form-group"> 
            <div class="row"> 
              <div class="col-md-6">
                <label>START DATE ACTUAL</label>
                <input type="text" name="startdateactual"  id="datestartactual" placeholder="Pilih Tanggal" autocomplete="off" class="form-control" required>
                <span class="label label-danger">*Required</span><br>
              </div> 
     
              <div class="col-md-6"> 
                 <label>CHANGE OVER CATEGORY</label>
                 <?php echo form_dropdown('co', array('A' => 'A', 'B' => 'B','C'=>'C','D'=>'D','REPEAT'=>'REPEAT'), null, "class='form-control'"); ?>
                 <span class="label label-danger">*Required</span><br>               
              </div> 
            </div> 
        </div>    
        <div class="form-group">
            <label class="col-sm-2 control-label">

            </label> 
        </div>
        <button type="submit" name="save" class="btn btn-success btn-lg btn-block"><i class="fa fa-save"></i> Save</button>
    </form>
    <input type="hidden" id="base_url" value="<?=base_url('header_inline')?>">
    </div> 
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/resource/inline_header.js"></script>