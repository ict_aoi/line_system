<style type="text/css">
    .actived{
        border-bottom: 1px dashed #000;
    }
</style>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table-size" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">NO</th>
                        <th>SIZE</th>
                        <th>QTY</th>
                        <th width="100px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no=1; 
                        foreach ($records->result() as $key => $record) {
                            $_temp = '"'.$record->inline_header_id.'","'.$record->qty.'","'.$record->size.'"';
                           echo "<tr>";
                               echo "<td>".$no++."</td>";
                               echo "<td>".$record->size."</td>";
                               echo '<td><input type="text" value="'.$record->qty.'" id="qtyordered_'.$record->size.'" class="form-control" style="background-color: inherit" disabled></td>';
                               echo "<td><a onclick='return editqty($_temp,1)' href='javascript:void(0)' class='btn btn-primary edit_".$record->size."'><i class='fa fa-pencil-square-o'></i></a><a onclick='return editqty($_temp,2)' style='display: none' href='javascript:void(0)' class='btn btn-success edit_".$record->size."'><i class='fa fa-save'></i></a><a onclick='return editqty($_temp,3)' style='display: none' href='javascript:void(0)' class='btn btn-warning edit_".$record->size."'><i class='fa fa-times'></i></a><a onclick='return delete_size($_temp)' href='javascript:void(0)' class='btn btn-danger'><i class='fa fa-trash-o'></i></a></td>";
                           echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    function editqty(id,qty,size,act) {
       
        if (act==1) {
            $('#qtyordered_'+size).prop('disabled',false).addClass('actived');
        }else if(act==2){
            var qtyinput = parseInt($('#qtyordered_'+size).val());
            
            if (qtyinput>parseInt(qty)) {
                $("#alert_error").trigger("click", 'qty tidak boleh melebihi order');
                $('#qtyordered_'+size).prop('disabled',false).addClass('actived');
                $('.edit_'+size).toggle();
            }else{
              var data = 'qty='+$('#qtyordered_'+size).val()+'&id='+id+'&size='+size;
              
               $('#qtyordered_'+size).prop('disabled',true).removeClass('actived');

               var data = 'qty='+$('#qtyordered_'+size).val()+'&id='+id+'&size='+size;
               $.ajax({
                    url:'Header_inline/edit_qty_submit',
                    data:data,
                    type:'POST',
                    success:function(data){
                        if(data == 200){
                          $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                        }else if (data=500) {
                          $("#alert_error").trigger("click", 'QTY kurang dari QC');
                          $('#qtyordered_'+size).prop('disabled',false).addClass('actived');
                        }else{
                          $('#myModal').modal('hide');
                        }
                    },
                    error:function(response){
                        if (response.status==500) {
                          $.unblockUI();
                          $("#alert_info").trigger("click", 'Contact Administrator');
                        }                    
                    }
                }) 
               
            }

        }else{
            $('#qtyordered_'+size).prop('disabled',true).removeClass('actived');
            $('#qtyordered_'+size).val(qty);
        }
       $('.edit_'+size).toggle();
    }
    function delete_size(id,qty,size) {
        swal({
          title: "Apakah anda yakin?",
          text: "Size "+size+" akan di hapus?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((wildelete) => {
          if (wildelete) {
            $.ajax({
                url:'Header_inline/deleteaction?id='+id+'&size='+size,
                success:function(value){
                    if(value=='200'){
                        $("#alert_success").trigger("click", 'Hapus data Berhasil');
                        $('#myModal').modal('hide');
                        $('#table-list').DataTable().ajax.reload();
                    }else{
                        $("#alert_error").trigger("click", 'Hapus data gagal');
                        $('#myModal').modal('hide');
                    }
                }

            });
            
          }
        });
    }    
</script>