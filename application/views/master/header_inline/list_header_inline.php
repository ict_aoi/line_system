<style type="text/css">
 .sorting, .sorting_asc, .sorting_desc {
    background : none;
}
.modal-xxl {
     width: 1000px;
    }
</style>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
    <br>
        <div class="col-sm-2">
          <a href="<?=base_url('Header_inline/addHeader') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <!-- <button id="tambah" class="btn btn-primary" onclick="return addHeader()"><i class="fa fa-plus"></i> Tambah</button> -->
        </div>
        <br>
        <div class="panel-body">
            <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">NO</th>
                        <th width="200px">PO BUYER</th>
                        <th>STYLE</th>
                        <th>ARTICLE</th>
                        <th width="150px">LINE NAME</th>
                        <th width="150px">QTY ORDER</th>
                        <th width="150px">DETAIL</th>
                        <th width="200px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    var table =$('#table-list').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti"  : true,
        "ajax":{
         "url": "Header_inline/list_header",
         "dataType": "json",
         "type": "POST",
         "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                       },
    "columns": [
              {"data" : null, 'sortable' : false},
              { "data": "poreference" },
              { "data": "style" },
              { "data": "article" },
              { "data": "line_name" },
              { "data": "qtyorder" },
              { "data": "detail" },
              { "data": "action" },
           ],
          fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    });
    function addHeader() {
      var modal = $('#myModal > div > div');
      $('#myModal').modal('show');
      modal.children('.modal-header').children('.modal-title').html('Tambah Header');
      modal.parent('.modal-dialog').addClass('modal-xxl');
      modal.children('.modal-body').load('Header_inline/addHeader');
      return false;
    }
    function detail(id,po,style) {
      var modal = $('#myModal > div > div');
      $('#myModal').modal('show');
      modal.children('.modal-header').children('.modal-title').html('Detail PO');
      modal.parent('.modal-dialog').addClass('modal-lg');
      modal.children('.modal-body').load('Header_inline/list_detail_po?po='+po,'&id='+id);
      return false;
    }
    function Close(id) {
      swal({
      title: "Apakah anda yakin?",
      text: "PO BUYER Akan di Non Aktifkan?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((wildelete) => {
      if (wildelete) {
        $.ajax({
          url:'Header_inline/inactivesubmit?id='+id,
          success:function(value){
            if(value=='sukses'){
              $("#alert_success").trigger("click", 'Inactive Berhasil');
              $('#table-list').DataTable().ajax.reload();
            }else{
              $("#alert_error").trigger("click", 'Inactive gagal karna po sudah jalan di line');
            }
          },
          error:function(response){
            if (response.status==500) {
              $("#alert_info").trigger("click", 'Contact ICT');
            }
          }

        });
        
      }
    });
    }
    function daily(id,po,style) {
      var url_loading = $('#loading_gif').attr('href');
      $("#myModal").modal('show');
      $('.modal-title').text("Daily Data "+po);
      $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
      $('.modal-body').load('Header_inline/loadcreatedaily?id='+id,);
      return false;
    }
</script>