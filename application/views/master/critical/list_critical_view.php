<style type="text/css">
    .actived{
        border-bottom: 1px dashed #000;
    }
</style>
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> LIST Critical
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="xs-2">
                 <button id="tambah" class="btn btn-success" onclick="return tambah()">Tambah</button>
                 <!-- <a href = "<?= base_url('Master_critical/add_critical');?>">
                 <button id="tambah" class="btn btn-success" >Tambah</button> </a> -->
                </div>
                <div class="row">
                    <div class="panel-body">
                        <table id="table-critical" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="20px">No</th>
                                    <th>Nama Product</th>
                                
                                    <th width="50px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>                        
                </div>
            </div>
            
        </div>
    </div>
</div>

<button type="button" class="hidden" id="refresh"></button>


<script type="text/javascript">

$(document).ready(function () {
        var table =$('#table-critical').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
            "url": "Master_critical/listcritical",
            "dataType": "json",
            "type": "POST",
            "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                        },
        "columns": [
                {"data" : null, 'sortable' : false},
                { "data": "product_name" },
                { "data": "action",'sortable' : false },
            ],
            fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
        $('#table-critical_filter input').unbind();
        $('#table-critical_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-critical').DataTable().ajax.reload();
        });

    });
   
 

 
  

 function tambah(){       
        $("#myModal").modal('show');
        $('.modal-title').text("Add Critical");
        $('.modal-body').html('<center>Loading..</center>');
        $('.modal-body').load('Master_critical/add_critical');
        return false;
    }


    
</script>


