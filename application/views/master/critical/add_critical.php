<form class="add_critical" method="POST">
   
    <label >Nama Product</label>
    <select class="form-control select" id="product" name="product" style = "width:100%;" required>
        
    </select>
    <br><br>
    <label>Nama Proses</label>
   
    <select class="form-control js-example-basic-single" name="proses" id = "proses" style = "width:100%;" required>
       
    </select>
     <br>
     <label> Critical Proses </label>
     <div class="form-group">
                     
                          <div class="form-radio">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="radioYa" value="true" checked> Ya </label>
                            </div> 
                            <div class="form-radio">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="radioTidak" value="false" > Tidak </label>
                                
                            </div> 
                            
                            
                          </div>
    <div class="pull-right">
        <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> SIMPAN</button>
    </div>
</form>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
  <link href="<?php echo base_url();?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
  <script src="<?php echo base_url();?>assets/plugins/select2/select2.min.js"></script>

<link href="<?= base_url('assets/plugins/select2/select2.min.css');?>" rel="stylesheet" />
<script src="<?= base_url('assets/plugins/select2/select2.min.js');?>"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2({
        dropdownParent: $('#myModal'),
        placeholder: " -- Select Your Proses -- ",
        ajax: {
          url: 'Master_critical/search',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.proses_name,
                        id: item.master_proses_id + ',' + item.proses_name
                    }
                })
            };
          },
          cache: true
        }
    });
});

$(document).ready(function() {
    $('.select').select2({
        dropdownParent: $('#myModal'),
        placeholder: " -- Select Your Product -- ",
        ajax: {
          url: 'Master_critical/search_product',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama_product,
                        id: item.id + ',' + item.nama_product
                    }
                })
            };
          },
          cache: true
        }
    });
});

    $(document).ready(function(){
        
        $('.add_critical').submit(function(e){
            e.preventDefault();
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Master_critical/save_critical',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    else if(result.status==3){
                        $("#alert_warning").trigger("click", result.pesan);
                    }
                }
            })
        return false;
        });

    
    }); 


   
</script>
