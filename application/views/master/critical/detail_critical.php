<style type="text/css">
    .actived{
        border-bottom: 1px dashed #000;
    }
</style>
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> LIST Critical
        </div>
        <div class="panel-body">
            <div class="form-group">
                <input type = "hidden" id = "product_id" value = "<?php echo $product_id; ?>" >
                <div class="row">
                    <div class="panel-body">
                        <table id="table-detail" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="20px">No</th>
                                    <th>Proses Name</th>
                                    <th>Critical Proses</th>
                                    <th width="50px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>                        
                </div>
            </div>
            
        </div>
    </div>
</div>

<button type="button" class="hidden" id="refresh"></button>


<script type="text/javascript">

$(document).ready(function () {
        var product_id = $("#product_id").val();
        
        var table =$('#table-detail').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
            "url": "<?php echo base_url();?>Master_critical/list_group_product",
            "dataType": "json",
            "type": "POST",
            "data":function(data) { 
                                        
                data.product_id = $('#product_id').val();
                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                                    } },
        "columns": [
                {"data" : null, 'sortable' : false},
                { "data": "proses_name" },
                { "data": "is_critical" },
                { "data": "action",'sortable' : false },
            ],
            fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
        $('#table-detail_filter input').unbind();
        $('#table-detail_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-detail').DataTable().ajax.reload();
        });

    });
   
    function edit(id) {
        $("#myModal").modal('show');
        $('.modal-title').text("Edit Critical");
        $('.modal-body').html('<center>Loading..</center>');
        $('.modal-body').load('<?php echo base_url();?>master_critical/edit?id='+id);
        // modal.children('.modal-body').load('master_mesin/edit?id='+id);
        return false;
    }

    function delete_critical(id) {
        swal({
            title: "Apakah anda yakin?",
            text: "Data akan di hapus?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((wildelete) => {
            if (wildelete) {
                $.ajax({
                    url:'<?php echo base_url();?>master_critical/delete_critical?id='+id,
                    success:function(value){
                        if(value=='1'){
                            $("#alert_success").trigger("click", 'Hapus data Berhasil');                        
                            $('#refresh').trigger("click");
                        }else{
                            $("#alert_error").trigger("click", 'Hapus data gagal');
                        }
                    }

                });
                
            }
        });
        return false;
    }




    
</script>


