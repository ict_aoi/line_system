<form class="edit_critical" method="POST">
   
    <label>Nama Product</label>
    <input type="hidden" name="master_critical_id" id = "master_critical_id" value="<?php echo $critical['master_critical_id'];?>" autocomplete="off" class="form-control">
    
    <input type="hidden" name="master_product_name" id = "master_product_name" value="<?php echo $critical['product_name'];?>" autocomplete="off">
    <input type="hidden" name="master_proses_name" id = "master_proses_name" value="<?php echo $critical['proses_name'];?>" autocomplete="off">
    <input type="hidden" name="master_proses_id" id = "master_proses_id" value="<?php echo $critical['proses_id'];?>" autocomplete="off">
            
    <select class="form-control select" id="product" name="product" required>
        <option value="">-- Pilih --</option>
        
        <?php
         foreach ($product as $p) {

            echo "<option value='".$p->nama_product."'>".$p->nama_product."</option>";
          }
         ?>
    </select>
    <br>
    <label>Nama Proses</label>
    <select class="form-control select" id="proses" name="proses" required>
        <option value="">-- Pilih --</option>
        
        <?php
         foreach ($proses as $p) {

            echo "<option value='".$p->master_proses_id.',' .$p->proses_name."'>".$p->proses_name."</option>";
          }
         ?>
    </select>
     <br>
     <label> Critical Proses </label>
     <div class="form-group">
                     
                          <div class="form-radio">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="radioYa" value="true" checked> Ya </label>
                            </div> 
                            <div class="form-radio">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="radioTidak" value="false" > Tidak </label>
                                
                            </div> 
                            
                            
                          </div>
    <div class="pull-right">
        <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> UPDATE</button>
    </div>
</form>
<script>
    $(document).ready(function(){

        var product   = $('#master_product_name').val();
        var proses    = $('#master_proses_name').val();
        var proses_id = $('#master_proses_id').val();
        $('#proses').val(proses_id+','+proses); 
        $('#product').val(product);
        
        $('.edit_critical').submit(function(e){
            e.preventDefault();
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Master_critical/update_critical',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    else if(result.status==3){
                        $("#alert_warning").trigger("click", result.pesan);
                    }
                }
            })
        return false;
        });
    }); 
</script>
