<style type="text/css">
    .actived{
        border-bottom: 1px dashed #000;
    }
</style>
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> LIST Product
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="xs-2">
                    <button id="tambah" class="btn btn-success" onclick="return tambah()">Tambah</button>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <table id="table-product" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="20px">No</th>
                                    <th>Nama Product</th>
                                    <th width="50px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>                        
                </div>
            </div>
            
        </div>
    </div>
</div>

<button type="button" class="hidden" id="refresh"></button>

<script type="text/javascript">

$(document).ready(function () {
        var table =$('#table-product').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
            "url": "Master_product/listmaster",
            "dataType": "json",
            "type": "POST",
            "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                        },
        "columns": [
                {"data" : null, 'sortable' : false},
                { "data": "nama_product" },
                { "data": "action",'sortable' : false },
            ],
            fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
        $('#table-product_filter input').unbind();
        $('#table-product_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-product').DataTable().ajax.reload();
        });

    });
   
    function tambah(){       
        $("#myModal").modal('show');
        $('.modal-title').text("Add Product");
        $('.modal-body').html('<center>Loading..</center>');
        $('.modal-body').load('master_product/add_product');
        return false;
    }

    function edit(id) {
        $("#myModal").modal('show');
        $('.modal-title').text("Edit Product");
        $('.modal-body').html('<center>Loading..</center>');
        $('.modal-body').load('master_product/edit?id='+id);
        // modal.children('.modal-body').load('master_mesin/edit?id='+id);
        return false;
    }

    function delete_product(id) {
        swal({
            title: "Apakah anda yakin?",
            text: "Data akan di hapus?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((wildelete) => {
            if (wildelete) {
                $.ajax({
                    url:'master_product/delete_product?id='+id,
                    success:function(value){
                        if(value=='1'){
                            $("#alert_success").trigger("click", 'Hapus data Berhasil');                        
                            $('#refresh').trigger("click");
                        }else{
                            $("#alert_error").trigger("click", 'Hapus data gagal');
                        }
                    }

                });
                
            }
        });
        return false;
    }

  

    
</script>

