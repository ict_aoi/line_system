<form class="add_product_category" method="POST">
    <label>Nama Product</label>
    <input type="text" name="nama_product" placeholder="Masukan Nama Product" autocomplete="off" class="form-control" required>
    <br>
    <div class="pull-right">
        <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> SIMPAN</button>
    </div>
</form>
<script>
    $(document).ready(function(){
        $('.add_product_category').submit(function(e){
            e.preventDefault();
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>master_product/save_product',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    else if(result.status==3){
                        $("#alert_warning").trigger("click", result.pesan);
                    }
                }
            })
        return false;
        });
    });
</script>
