<div class="col-md-4">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list" aria-hidden="true"></i> LIST STYLE
    </div>
    <div class="panel-body">
        <div class="input-group">
          <input type="hidden" id="base_url" value="<?=base_url('master_workflow')?>">
            <input type="text" class="form-control" name="style" id="style" readonly="readonly" value="<?=$style['style'] ?>" placeholder="PILIH STYLE">            
        </div> 
    </div>

</div>
</div>
<div class="col-md-8">
    <div class="panel panel-default border-grey">
      <div class="panel-heading">
        <i class="fa fa-th-list" aria-hidden="true"></i> LIST WORKFLOW
    </div>
      <br>
      <br>
      
      <div class="panel-body">
       <div class="table-responsive">
        <div class="col-md-12">
         
        </div>
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover table-full-width">
         <thead>
          <tr>
           <th>No</th>
           <th>Nama Proses</th>
           <th>ACTION</th>
          </tr>
         </thead>
         <tbody id="tbody-items">
         </tbody>
        </table>
        </div>
        <div class="col-xs-12 pull-right">
      <label for=""><h4>SELECT PROSES</h4></label>
      <select class="proses_choses form-control" id="prosesName" name="proses" onchange="return addProses()">
        <option value="" id="user_id">Pilih Proses</option>
        <?php 
          $master = $this->db->get('master_proses');
          foreach ($master->result() as $key => $master) {
            echo "<option value='".$master->master_proses_id."' name='".$master->proses_name."'>".$master->proses_name."</option>";
          }
        ?>
      </select>
    </div>
    <br><br><br><br><br>
       </div>
       <form class="frmsave" method="POST">
        <?php echo $record ; ?>
        <input type="text" name="items" id="items" value="[]">
        <input type="text" name="style-hid" id="style_hid" value="<?=$style['style'] ?>">
      
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
        </div>
     </form>
      </div>

     </div>
</div>
      

<script type="x-tmpl-mustache" id="items-table">  
  {% #item %}
    <tr>
      <td style="text-align:center;">{% no %}</td>
      <td>
        <div id="item_{% _id %}">{% proses_name %}</div>
        <input type="text" class="form-control input-md hidden" id="itemInputId_{% _id %}" 
          value="{% id %}" data-id="{% _id %}"
        <span class="text-danger hidden" id="errorInput_{% _id %}">
        </span>
      </td>
      
      <td width="80px">
            <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="fa fa-trash-o"></i></button>
      </td>
    </tr>
  {%/item%}

   <tr>
    <td width="20px">
      #
    </td>
    <td colspan="2">
      <input type="hidden" id="prosesId">
      <br>      
    </td>
    
  </tr>
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/resource/master_workflow.js"></script>
<!-- <input type="text" id="prosesName" class="form-control input-new" placeholder="Nama Proses"> -->