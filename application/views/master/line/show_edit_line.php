
<div class="panel panel-default border-grey"> 
    <div class="panel-body">
    <form class="frmcange" method="POST"> 
      <div class="form-group"> 
        <div class="row"> 
          <div class="col-md-6">
          <input type="hidden" id="base_url" value="<?=base_url('master_line');?>"> 
            <label>NAMA LINE</label>
            <input type="hidden" name="master_line_id" value="<?=$record['master_line_id'] ?>"> 
            <input type="text" name="line_name" value="<?=$record['line_name'] ?>" class="form-control">
            <label>Nomor Mesin</label>
            <input type="number" name="mesin_id" value="<?=$record['mesin_id'] ?>" class="form-control"> 
          </div> 
        </div> 
      </div> 
      <div class="clearfix"></div>
      <div class="form-group"> 
        <div class="row"> 
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary pull-right">UPDATE</button> 
          </div>
          </form> 
        </div>        
      </div> 
    </div> 
  </div>
  <script type="text/javascript">
    $(function () {
      $('.frmcange').submit(function () {
        var data = new FormData(this);
        $.ajax({
            url:'<?=base_url();?>Master_line/editlinesubmit',
            type: "POST",
            data: data,
            contentType: false,       
            cache: false,          
            processData:false, 
            success: function(response){
                var result = JSON.parse(response);
                if(result.status == 200){
                  $("#alert_success").trigger("click", result.pesan);
                  $('#myModal_').modal('hide');
                  $('#table-list').DataTable().ajax.reload();
                }else{
                  $('#myModal_').modal('hide');
                  $("#alert_info").trigger("click", result.pesan);
                  $('#table-list').DataTable().ajax.reload();
                }
            }
          });
        return false;
      });
    });
  </script>
  <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/resource/change.js"></script> -->
