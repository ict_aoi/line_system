<style type="text/css">
    .actived{
        border-bottom: 1px dashed #000;
    }
</style>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa fa-bars"> Master Line</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Master Data</a></li>
        <li><i class="fa fa-bars"></i>List Line</li>
      </ol>
    </div>
  </div>
</div>
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> STYLE
        </div>
        <div class="panel-body">
            <form class="pilih" method="POST">
            <div class="form-group">
                <div class="row">
                    <div class="panel-body">
                        <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="20px">NO</th>
                                    <th>LINE NAME</th>
                                    <th>Nomor Mesin</th>
                                    <th>Factory</th>
                                    <th width="200px">ACTION</th>
                                </tr>
                            </thead>
                            <tbody id="proseslist">
                            </tbody>
                        </table>
                    </div>                        
                </div>
            </div>
            <div class="col-xs-1">
                <button type="submit" class="btn btn-small btn-primary"><i class="fa fa-check"></i> Simpan</button>
            </div>
            </form>
            <button class="btn btn-small btn-primary" onclick="add_item()"><i class="fa fa-plus"></i> Tambah</button> 
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=Base_url();?>assets/plugins/mousetrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/master_line.js"></script>
