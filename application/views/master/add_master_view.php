
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> STYLE
        </div>
        <div class="panel-body">
            <form class="pilih" method="POST">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="kode_style" id="kode_style" placeholder="Pilih Style" readonly>
                        <input type="hidden" value="<?php echo base_url('Master_process/'); ?>" id="base_url">
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-default" onclick="return search()">. . .</button>
                    </div>
                    <br>
                    <br>
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover table-full-width dataTable">
                        <!--elemet sebagai target append-->
                        <thead>
                            <tr> 
                                <td width="20px">#</td>
                                <td>Nama Process</td>
                                <td width="50px">Action</td>
                            </tr>
                        </thead>
                        <tbody id="proseslist">
                                <td></td>
                                <td></td>
                                <td></td>
                        </tbody>
                    </table>
                    </div>
                        
                </div>
            </div>
            <div class="col-xs-2">
                <button type="submit" class="btn btn-small btn-primary"><i class="fa fa-check"></i> Simpan</button>
            </div>
            </form>
            <button class="btn btn-small btn-primary" onclick="add_item()"><i class="fa fa-plus"></i> Tambah</button> 
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/master_style.js"></script>
