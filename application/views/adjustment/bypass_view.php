<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">

        <br>
        <div class="col-md-6">
                <label>PO BUYER</label>
                <input type="text" name="po" id="po" placeholder="Masukan PO BUYER" autocomplete="off" class="form-control po" required>
                <label>LINE ID</label>
                <input type="text" name="line_id" id="line_id" placeholder="Masukan Line ID" autocomplete="off" class="form-control line" required>
                
                <button type="button" id="by_packing" style="height: 75px;font-size:40px"  class="btn btn-success btn-lg btn-block" > PACKING BYPASS</button>
                <button type="button" id="by_qc" style="height: 75px;font-size:40px"  class="btn btn-warning btn-lg btn-block"> FOLDING BYPASS</button>

        </div>
        <br>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
</div>
<a href="<?php echo base_url('assets/img/Spinner.gif') ?>" id="loading_gif" class="hidden"></a>
<button type="button" class="hidden" id="refresh"></button>
<script type="text/javascript">
     var url_loading = $('#loading_gif').attr('href');
  $(function(){
    $('#by_packing').click(function () {
      swal({
        title: "PERHATIAN!!!",
        text: "Apakah Anda yakin bypass packing?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((wilsave) => {
        if (wilsave) {
          var data = 'po='+$('#po').val()+'&line_id='+$('#line_id').val(); 
          $.ajax({
                url:'<?=base_url('adjustment/bypass_packing') ?>',
                data:data,
                type:'POST',
                beforeSend: function () {
          
                  $.blockUI({
                    message: "<img src='" + url_loading + "' />",
                    css: {
                      backgroundColor: 'transaparant',
                      border: 'none',
                    }
                  });
                },                  
                success:function(data){
                    var result = JSON.parse(data);
                    if (result.status==1) {
                      $.unblockUI();
                      $("#alert_success").trigger("click", result.pesan);
                    }else if (result.status==3){
                      $.unblockUI();
                      $("#alert_error").trigger("click", result.pesan);
                    }
                     
                },
                error:function(data){
                    // if (data.status==500) {
                    //   $("#alert_error").trigger("click", 'Info ICT');
                    // }                    
                }
            });
        }
      });
     /* */
    });
    $('#by_qc').click(function () {
      swal({
        title: "PERHATIAN!!!",
        text: "Apakah Anda yakin bypass QC?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((wilsave) => {
        if (wilsave) {
            var data = 'po='+$('#po').val()+'&line_id='+$('#line_id').val(); 
          $.ajax({
                url:'<?=base_url('adjustment/bypass_qc') ?>',
                data:data,
                type:'POST',
                beforeSend: function () {
          
                  $.blockUI({
                    message: "<img src='" + url_loading + "' />",
                    css: {
                      backgroundColor: 'transaparant',
                      border: 'none',
                    }
                  });
                },                  
                success:function(data){
                    var result = JSON.parse(data);
                    if (result.status==500) {
                      $.unblockUI();
                      $("#alert_success").trigger("click", result.pesan);
                    }else if (result.status==200){
                      $.unblockUI();
                      $("#alert_error").trigger("click", result.pesan);
                    }else if (result.status==100){
                      $.unblockUI();
                      $("#alert_error").trigger("click", result.pesan);
                    }
                     
                },
                error:function(data){
                    // if (data.status==500) {
                    //   $("#alert_error").trigger("click", 'Info ICT');
                    // }                    
                }
            });
        }
      });
    });
  });
</script>