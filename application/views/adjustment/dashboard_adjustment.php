<style>
#chartdiv {
  width: 100%;
  height: 500px;
}
</style>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        
        
        <div class="panel-body">
        <!-- <div class="row"> -->
        <div class="alert alert-success fade in">
            <div class="col-md-3">
                <a href="<?=base_url('Adjustment/adjustqc')?>" class="btn btn-primary btn-lg btn-block"><i class="fa fa-edit"></i> REVISI</a>
            </div>
            <div class="col-md-9">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                </button>
                <strong>INFORMASI</strong><br> Tombol <b>REVISI</b> untuk <b>HAPUS</b> inputan.</b>
            </div>
            <br><br>
        </div>
        <h2 class="text-center"><b>10 LINE BANYAK HAPUS INPUT QC BULAN <?php echo date('Y-m');?></b></h2>
       

        
            <br>
            <div id="chartdiv"></div>
            <!-- <br><br><br><br><br><br>
            <br><br><br><br><br>  -->
            
        </div>
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
          <div class=" alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon-remove"></i>
            </button>
            <strong>INFORMASI</strong><br> Pencarian berdasarkan <b>LINE</b> dan <b>PO BUYER</b>, Setelah selesai input, tekan <b>Enter.</b>
          </div>
          <!-- <br>
          <br>
          <br>
          <br>
          <br> -->
            <table id="table-adjust" class="table table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>TANGGAL</th>
                        <th>NAMA LINE</th>
                        <th>PO BUYER</th>
                        <th>STYLE</th>
                        <th>SIZE</th>
                        <th>NAMA PEMOHON</th>
                        <th>QTY REVISI</th>
                        <th>ALASAN</th>
                        <th>DIHAPUS OLEH</th>
                    </tr>   
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div id="result">
        
    </div>
</div>
</div>
<!-- Resources -->
<script src = "<?=base_url('/assets/amcharts4/core.js');?>"></script>
<script src = "<?=base_url('/assets/amcharts4/charts.js');?>"></script>
<script src = "<?=base_url('/assets/amcharts4/themes/kelly.js');?>"></script>
<script src = "<?=base_url('/assets/amcharts4/themes/animated.js');?>"></script>

<script type="text/javascript">

    $(document).ready(function () {
         var table =$('#table-adjust').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "<?=base_url('Adjustment/dashboard_ajax');?>",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
             'line_id':$('#line_id').val(),
              }
                           },
        "columns": [
                  { "data": "tanggal"},
                  { "data": "line"},
                  { "data": "pobuyer"},
                  { "data": "style",'sortable' : false  },
                  { "data": "size",'sortable' : false  },
                  { "data": "pemohon",'sortable' : false  },
                  { "data": "revisi",'sortable' : false  },
                  { "data": "alasan",'sortable' : false  },
                  { "data": "hapus",'sortable' : false  },

               ],
            //    fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); },               
        });
        $('#table-adjust_filter input').unbind();
        $('#table-adjust_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });     
         
    });

    var url_loading = $('#loading_gif').attr('href');
    $(function () {
          $('.search_header').submit(function() {
            var line = $('#line_id').val();
            if(!line){
                $("#alert_warning").trigger("click", 'LINE TIDAK BOLEH KOSONG');
                $('#result').html(' ');
            }else{
                var data = new FormData(this);
                var url_loading = $('#loading_gif').attr('href');
                $.ajax({
                    url:'searchsubmit',
                    type: "POST",
                    data: data,
                    contentType: false,       
                    cache: false,          
                    processData:false,
                    beforeSend: function () {
                        $.blockUI({
                            message: "<img src='" + url_loading + "' />",
                            css: {
                                backgroundColor: 'transaparant',
                                border: 'none',
                            }
                        });
                    },
              success: function(response){
                  if (response=='gagal') {
                  $("#alert_warning").trigger("click", 'Data Tidak Ada');
                  $.unblockUI();
                  }else{
                    $('#result').html(response);
                    $.unblockUI();
                  }
              }
              ,
            error:function(response){
              if (response.status=500) {
                $("#alert_info").trigger("click", 'Cek Input Data');
                $.unblockUI();
              }
          }
          });
            }

            return false
       })
    });
    function loadline() {
        $("#myModal").modal('show');
        $('.modal-title').text("Pilih LINE");
        $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
        $('.modal-body').load('<?=base_url();?>adjustment/loadline');
        return false;
    }

        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_kelly);
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = [<?php echo $chart?>];

        // Create axes

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "line";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
        if (target.dataItem && target.dataItem.index & 2 == 2) {
            return dy + 25;
        }
        return dy;
        });

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "jumlah";
        series.dataFields.categoryX = "line";
        series.name = "jumlah";
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;

        }); // end am4core.ready()
</script>