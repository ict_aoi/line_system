<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          
          <div class="row">
            <div class="col-md-2">
              
                <a href="<?=base_url('Adjustment')?>" class="btn btn-primary btn-lg btn-block"><i class="fa fa-arrow-left"></i> Kembali</a>
              <!-- </div> -->
            </div>
          </div>
            <div class="col-md-12">
            <br>
              <form class="search_header" method="POST">
              <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pilih LINE</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadline()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="line_id" id="line_id">
                        <input type="text" class="form-control" name="line_name" id="line_name" onclick="return loadline()" readonly="readonly" placeholder="PILIH LINE">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>
                    </div>
                    </td>
                  </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-search"></i> Cari</button>
          </form>
            </div>
        </div>
    </div>
    <div id="result">
        
    </div>
</div>
</div>
<script type="text/javascript">

    var url_loading = $('#loading_gif').attr('href');
    $(function () {
          $('.search_header').submit(function() {
            var line = $('#line_id').val();
            if(!line){
                $("#alert_warning").trigger("click", 'LINE TIDAK BOLEH KOSONG');
                $('#result').html(' ');
            }else{
                var data = new FormData(this);
                var url_loading = $('#loading_gif').attr('href');
                $.ajax({
                    url:'searchsubmit',
                    type: "POST",
                    data: data,
                    contentType: false,       
                    cache: false,          
                    processData:false,
                    beforeSend: function () {
                        $.blockUI({
                            message: "<img src='" + url_loading + "' />",
                            css: {
                                backgroundColor: 'transaparant',
                                border: 'none',
                            }
                        });
                    },
              success: function(response){
                  if (response=='gagal') {
                  $("#alert_warning").trigger("click", 'Data Tidak Ada');
                  $.unblockUI();
                  }else{
                    $('#result').html(response);
                    $.unblockUI();
                  }
              }
              ,
            error:function(response){
              if (response.status=500) {
                $("#alert_info").trigger("click", 'Cek Input Data');
                $.unblockUI();
              }
          }
          });
            }

            return false
       })
    });
    function loadline() {
        $("#myModal").modal('show');
        $('.modal-title').text("Pilih LINE");
        $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
        $('.modal-body').load('<?=base_url();?>adjustment/loadline');
        return false;
    }
</script>