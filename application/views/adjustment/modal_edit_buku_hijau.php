<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    
    <div class="panel panel-default">
      <br>
        <div class="clearfix"></div>
        <div class="panel-body">
          <form class="editbukuhijau" method="POST">
            <input type="hidden" name="qc_endline_id" value="<?php echo $detail['qc_endline_id'] ?>">
            <input type="hidden" name="line_id" value="<?php echo $detail['line_id'] ?>">
            <div class="form-group"> 
                <div class="row"> 
                  <div class="col-md-6"> 
                    <label>PO BUYER</label>
                    <input type="text" name="poreference" value="<?=$detail['poreference'];?>"  class="form-control"  readonly> 
                  </div> 
                  <div class="col-md-6"> 
                    <label>STYLE</label>
                    <input type="text" name="style" value="<?=$detail['style'];?>"  class="form-control"  readonly>           
                  </div> 
                </div> 
            </div>
            <label>SIZE</label>
            <input type="text" name="size" value="<?=$detail['size'];?>"  class="form-control"  readonly> 
            <div class="form-group"> 
                <div class="row"> 
                  <div class="col-md-6"> 
                    <label>Total Output</label>
                    <input type="text" name="total_output" value="<?=$detail['max'] ?>"  class="form-control"  readonly>    
                  </div> 
                  <div class="col-md-6"> 
                    <label>Output Day</label>
                    <input type="text" name="output_day" id="txtOutputDay" value="<?=$detail['daily_output'];?>"  class="form-control" readonly>        
                  </div> 
                </div> 
            </div>  
            <label>Revisi Output</label>
            <input type="number" name="revisi" id="txtRevisi" class="form-control" required>
            <div class="form-group"> 
                <div class="row"> 
                  <div class="col-md-6"> 
                    <label>NIK PEMOHON</label>
                    <input type="text" name="nik_pemohon" id="nik" class="form-control" required>    
                  </div> 
                  <div class="col-md-6"> 
                    <label>NAMA</label>
                    <input type="text" name="nama" id="nama_pemohon" class="form-control" readonly>
                  </div> 
                </div> 
            </div>
            <label class="control-label">
              ALASAN
            </label>
            <div class="col-sm-12">
              <textarea type="text" id="alasan" name="alasan" placeholder="Masukan Alasan" required class="form-control" cols="50" rows="10"></textarea>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">

              </label> 
            </div>
            <button type="submit" name="save" class="btn btn-success btn-sm bsave"><i class="fa fa-save"></i> UPDATE</button>
        </form>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function(){
      var url_loading = $('#loading_gif').attr('href');
        $('#txtRevisi').change(function () {
          var day = parseInt($('#txtOutputDay').val());
          var revisi = parseInt($('#txtRevisi').val());
          if (revisi>day) {
            $("#alert_error").trigger("click", 'QTY Revisi tidak boleh lebih dari QTY Output hari ini');
            $('#txtRevisi').val('');
            $('#txtRevisi').focus();
          }
        });
        $('#nik').change(function(e) {
        var nik = $("#nik").val();
          $.ajax({
              type:'GET',
              url :'<?=base_url();?>cap/searchNik',
              data:'id='+nik,
              success:function(response){
                 var result = JSON.parse(response);
                 if (result.notif==1) {
                     $('#nama_pemohon').val(result.record.name);
                     $('#alasan').focus();
                 }else{
                    $("#alert_info").trigger("click", 'nik tidak terdaftar');
                    $('#nik').val('');
                    $('#nik').focus();
                 }
              },
                  error:function(response){
                    if (response.status==500) {
                      $.unblockUI();
                      $('#nik').val('');
                      $("#alert_info").trigger("click", 'Cek Inputan Anda');
                    }
                }
          });
      });
        $('.editbukuhijau').submit(function(){
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>adjustment/editbukuhijausubmit',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                  var result = JSON.parse(response)
                  if (result.status==200) {
                     $("#myModal_").modal('hide');
                     $("#alert_success").trigger("click", result.pesan);
                      $('#table-result').DataTable().ajax.reload()
                  }else{
                     $("#alert_info").trigger("click", result.pesan);
                  }
                }
        })
        return false;
    });
    });
</script>