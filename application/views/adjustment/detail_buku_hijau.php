<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>
<div class="row">
<div class="col-md-12 table_detail">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Detail Buku Hijau
             </div>
             <div class="col-md-2 style pull-right">
               <span class="style"></span>
             </div>
           </h6>
        </div>
        <div class="panel-body">
          <input type="hidden" id="line_id" value="<?php echo $line_id ?>">
           <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center">NO</th>
                            <th class="text-center">PO</th>
                            <th class="text-center">STYLE</th>
                            <th class="text-center">SIZE</th>
                            <th class="text-center">QTY ORDER</th>
                            <th class="text-center">Total QTY Input</th>
                            <th class="text-center">Counter Per Day</th>
                            <th class="text-center">BALANCE</th>
                            <th class="text-center">Defect Per Day</th>
                            <th class="text-center" width="50px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </table>
        </div>
        </div>
    </div>
    

</div>
</div>
<button type="button"  id="refresh"></button>
<script type="text/javascript">
    $(function () {
        var table =$('#table-result').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti"  : true,
        "ajax":{
         "url": "detail_buku_hijau_ajax",
         "dataType": "json",
         "type": "POST",
         "data":function(data) {
                        data.line_id   = $('#line_id').val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
    "columns": [
              {"data" : null, 'sortable' : false},
              { "data": "poreference" },
              { "data": "style" },
              { "data": "size" },
              { "data": "order",'sortable' : false },
              { "data": "counter_qc",'sortable' : false },
              { "data": "daily_output",'sortable' : false },
              { "data": "balance_per_day",'sortable' : false },
              { "data": "defect_perday",'sortable' : false },
              { "data": "action",'sortable' : false },
           ],
        fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    });
    $('#table-user_filter input').unbind();
      $('#table-user_filter input').bind('keyup', function(e) {
          if (e.keyCode == 13 || $(this).val().length == 0 ) {
              table.search($(this).val()).draw();
          }
          // if ($(this).val().length == 0 || $(this).val().length >= 3) {
          //     table.search($(this).val()).draw();
          // }
      });
    $('#refresh').bind('click', function () {
      $('#table-user').DataTable().ajax.reload();
    });
    });
    function pilih(qc_endline_id,poreference,size) {
        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Edit Buku Hijau <b>'+poreference+' <b> Size '+size);
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html('<center>Loading..</center>');
        modal.children('.modal-body').load('editbukuhijau?qc_id='+qc_endline_id);
        return false;  
    }
</script>