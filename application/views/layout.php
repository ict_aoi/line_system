<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Inline - System">
    <meta name="author" content="ICT">
    <meta name="keyword" content="Inline - System">
    <link rel="shortcut icon" href="<?php base_url() ?>assets/img/favicon.jpg">

    <title><?=$title;?> - Inline System </title>

    <!-- Bootstrap CSS -->
    <link href="<?=site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?=site_url('assets/css/anytime.min.css');?>" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?=Base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?=Base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/owl.carousel.css" type="text/css">
    <link href="<?=Base_url();?>assets/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/fullcalendar.css">
    <link href="<?=Base_url();?>assets/css/widgets.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=Base_url('assets/plugins/chosen/chosen.min.css');?>" type="text/css">
    <script src="<?=Base_url();?>assets/js/jquery.min.js"></script>

    <script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.rowspanizer.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/plugins/mustache.js"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/plugins/anytime.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/resource/notification.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/datepicker/css/bootstrap-datepicker.min.css');?>">
<script type="text/javascript" src="<?=base_url('assets/plugins/datepicker/js/bootstrap-datepicker.min.js');?>"></script>

  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
      <!--header start-->

      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>

            <!--logo start-->
            <a href="<?=base_url('home');?>" class="logo">LINE <span class="lite">System</span></a>
            <!--logo end-->

            <div class="top-nav notification-row">
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username"><?php echo ucwords($this->session->userdata('username'))?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="<?=base_url('role/accountsetting') ?>"><i class="icon_profile"></i> Account Setting</a>
                            </li>
                            <li>
                                <a href="<?=base_url('auth/logout');?>"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">
                  <?php
                    $role_id = $this->session->userdata('role_id');
                        $sql_menu = "SELECT * FROM menu WHERE menu_id in(select menu_id from role_user where level_id='$role_id') and is_main_menu='0' order by urut ASC";
                        $main_menu = $this->db->query($sql_menu)->result();
                    foreach ($main_menu as $main) {
                      $sql_menu = "SELECT * FROM menu WHERE menu_id in(select menu_id from role_user where level_id='$role_id') and is_main_menu in('$main->menu_id') order by urut";
                      $sub_menu = $this->db->query($sql_menu);
                      // $this->db->get_where('menu', array('is_main_menu'=>$main->menu_id));
                      if ($sub_menu->num_rows()>0) { ?>
                        <li class="sub-menu">
                            <a href="javascript:void(0)" class="">
                                <i class="<?php echo $main->icon; ?>"></i>
                                <span><?php echo ucwords($main->nama_menu);?></span>
                                <span class="menu-arrow arrow_carrot-right"></span>
                            </a>
                            <ul class="sub">
                            <?php
                            foreach ($sub_menu->result() as $sub) { ?>

                              <li>
                                <a class="" href="<?php echo base_url($sub->link);?>"><span class="title"><?php echo ucwords($sub->nama_menu);?></span></a>
                              </li>

                    <?php }
                          ?>
                          </ul>
                         </li>
                    <?php }else{ ?>
                          <li>
                              <a class="" href="<?=base_url($main->link);?>">
                                  <i class="<?php echo $main->icon; ?>"></i>
                                  <span><?php echo ucwords($main->nama_menu); ?></span>
                              </a>
                          </li>
                  <?php  }
                    }
                    ?>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

              <!-- page start-->
              <?php echo $contents ?>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section end -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body" style="overflow: auto;"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
<a href="<?php echo base_url('assets/img/Spinner.gif') ?>" id="loading_gif" class="hidden"></a>
<button type="button" class="hidden" id="alert_info"></button>
<button type="button" class="hidden" id="alert_success"></button>
<button type="button" class="hidden" id="alert_error"></button>
<button type="button" class="hidden" id="alert_warning"></button>
    <!-- javascripts -->

    <script src="<?=Base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?=Base_url();?>assets/plugins/chosen/chosen.jquery.min.js"></script>

    <script src="<?=Base_url();?>assets/plugins/autocomplete/jquery.easy-autocomplete.min.js"></script>
    <!-- nice scroll -->
    <script src="<?=Base_url();?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <!-- charts scripts -->
    <script src="<?=Base_url();?>assets/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="<?=Base_url();?>assets/js/owl.carousel.js" ></script>
    <!-- jQuery full calendar -->
    <script src="<?=Base_url();?>assets/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
    <script src="<?=Base_url();?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="<?=Base_url();?>assets/js/calendar-custom.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="<?=Base_url();?>assets/js/jquery.customSelect.min.js" ></script>

    <!--custome script for all page-->
    <script src="<?=Base_url();?>assets/js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="<?=Base_url();?>assets/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.autosize.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.placeholder.min.js"></script>
    <script src="<?=Base_url();?>assets/js/gdp-data.js"></script>
    <script src="<?=Base_url();?>assets/js/morris.min.js"></script>
    <script src="<?=Base_url();?>assets/js/sparklines.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.slimscroll.min.js"></script>
    <script>
      Mustache.tags = ['{%', '%}'];

      function isNumberKey(evt)
      {
      var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
        return true;
      }

      function isNumberDot(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
      }

    </script>
  </body>
</html>
