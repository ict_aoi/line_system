<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <br>
        <div class="col-sm-2">
            <button id="tambah" class="btn btn-success" onclick="return tambah()">Tambah</button>
        </div>
        <br>
        <div class="panel-body">
            <table id="table-user" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">NO</th>
                        <th>NAMA USER</th>
                        <th>NIK</th>
                        <th>NO HP</th>
                        <th>LEVEL</th>
                        <th>FACTORY</th>
                        <th width="50px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
</div>
<button type="button" class="hidden" id="refresh"></button>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/users.js"></script>