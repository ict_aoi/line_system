<form class="edit_menu" method="POST">
    <label>NIK</label>
    <input type="number" name="nik" value="<?=$record['nik'];?>" autocomplete="off" class="form-control" readonly >
    <label>Nama User</label>
    <input type="hidden" name="id" value="<?=$record['users_id'];?>">
    <input type="text" name="user" value="<?=$record['username'];?>"  class="form-control"  readonly>    
    <label>Factory</label>
    <input type="text" name="factory_name" value="<?=$factory_name;?>"  class="form-control factory_name" readonly>
    <input type="hidden" id="factory_id" name="factory" value="<?=$record['factory'];?>">
    <label>Departement</label>
    <input type="text" name="departement" value="<?=$record['departement'];?>"  class="form-control dept" readonly>
    <label>Password</label>
    <input type="password" name="password" placeholder="Masukan Password" autocomplete="off" class="form-control">
    <label>No HP</label>
    <input type="text" name="nohp" value="<?=$record['nohp'];?>"   class="form-control nohp">
    <label>Level</label>
    <select name="level" class="form-control" required>
        <option value="">Select Level</option>
        <?php
        $level = $this->db->get('level_user');
        foreach ($level->result() as $row){
            echo "<option value='$row->level_id'";
            echo $record['level_id']==$row->level_id?'selected':'';
            echo">$row->level_name</option>";
        }
        ?>
    </select>
    <div class="form-group">
        <label class="col-sm-2 control-label">

        </label> 
    </div>
    <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> UPDATE</button>
</form>
<script>
    $(document).ready(function(){
        $('.edit_menu').submit(function(){
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Role/edit_user',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    if (response=='sukses') {
                         $("#myModal").modal('hide');
                         $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                         $('#refresh').trigger("click");
                    }
                }
        })
        return false;
    });
    });
</script>