<form class="add_user" method="POST">
    <label>NIK</label>
    <input type="number" name="nik" placeholder="Masukan NIK" autocomplete="off" class="form-control nik" required>
    <label>Nama User</label>
    <input type="text" name="user" placeholder="Masukan Nama User"  class="form-control user" readonly>
    <label>Factory</label>
    <input type="text" name="factory_name"  class="form-control factory_name" required autocomplete="off" readonly>
    <input type="hidden" id="factory_id" name="factory">
    <label>Departement</label>
    <input type="text" name="departement"  class="form-control dept" readonly>
    <label>Password</label>
    <input type="password" name="password" id="pass" placeholder="Masukan Password" autocomplete="off" class="form-control" required>
    <label>No HP</label>
    <input type="text" name="nohp"  class="form-control nohp">
    <label>Level</label>
    <select name="level" class="form-control" required>
        <option value="">Select Level</option>
        <?php
        $level = $this->db->get('level_user');
        foreach ($level->result() as $row){
            echo "<option value='$row->level_id'>$row->level_name</option>";
        }
        ?>
    </select>
    
    <div class="form-group">
        <label class="col-sm-2 control-label">

        </label> 
    </div>
    <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> SIMPAN</button>
</form>
<script>
    $(document).ready(function(){
        $('.add_user').submit(function(){
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Role/save_user',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    if (response=='sukses') {
                         $("#myModal").modal('hide');
                         $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                         $('#refresh').trigger("click");
                    }else{
                        $("#alert_danger").trigger("click", 'Failed');
                    }
                }
        })
        return false;
    });
    $('.nik').change(function(e) {
        var url_loading = $('#loading_gif').attr('href');
        var nik = $(".nik").val();
          $.ajax({
              type:'GET',
              url :'<?=base_url();?>Role/searchNik',
              data:'id='+nik,
              success:function(response){
                 var result = JSON.parse(response);
                 if (result.notif==1) {
                     $('.user').val(result.record.name);
                     $('.factory_name').val(result.record.factory);
                     $('#factory_id').val(result.factory_id);
                     $('.dept').val(result.record.subdept_name);
                      document.getElementById("pass").focus();
                 }else if (result.notif==2) {
                     $("#alert_info").trigger("click", 'nik tidak terdaftar');
                 }else{
                     $("#alert_info").trigger("click", 'nik sudah ada ');
                 }
              },
                  error:function(response){
                    if (response.status==500) {
                      $.unblockUI();
                      $('#nik').val('');
                      $('#sewer').val('');
                      $('#sewer-hid').val('');
                      $("#alert_info").trigger("click", 'Cek Inputan Anda');
                    }
                }
          });
      });
    });
</script>