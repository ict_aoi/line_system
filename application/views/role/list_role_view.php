<div class="col-md-4">
    <!-- start: DYNAMIC TABLE PANEL -->
<body onload="load_level()"></body>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> LEVEL USER
        </div>
        <div class="panel-body">
            <div class="col-sm-1">
                <button id="tambah" class="btn btn-success btn-xs" onclick="return tambah()">Tambah</button>
            </div>
            <br>
            <div id="list_load"></div>
        </div>

    </div>
</div>

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> HAK AKSES MODULE
        </div>
        <div class="panel-body">
            <div id="tabel-role"></div>

        </div>
    </div>
</div>
<button type="button" class="hidden" id="refresh"></button>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/role.js"></script>