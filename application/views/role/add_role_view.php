<form class="add_role" method="POST">
    <label>Nama Role</label>
    <input type="text" name="level_name" placeholder="MASUKAN NAMA ROLE" id="form-field-1" class="form-control" required>
    <label>Nama Role</label>
    <textarea class="form-control" placeholder="Description" name="description" cols="50" rows="10"></textarea>
    <br>
    <button type="submit" name="submit" class="btn btn-danger btn-sm bsave" id="save"><i class="fa fa-save"></i> SIMPAN</button>
    <button type="submit" name="submit" class="btn btn-success">KEMBALI</button>
</form>
<script>
    $(document).ready(function(){
        $('.add_role').submit(function(){
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Role/save_level',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    if (response=='sukses') {
                         $("#myModal").modal('hide');
                         $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                         $('#refresh').trigger("click");
                    }else{
                        $("#alert_error").trigger("click", 'Failed');
                    }
                }
        })
        return false;
    });
    });
</script>