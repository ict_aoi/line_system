<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa fa-bars"> User Profile</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Account Setting</a></li>
        <li><i class="fa fa-bars"></i>Profile</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
    <div class="panel-body"> 
      <div class="form-group"> 
        <div class="row"> 
          <div class="col-md-6">
          <input type="hidden" id="base_url" value="<?=base_url('auth/logout');?>"> 
            <label>NIK</label> 
            <input type="text" name="nik" value="<?=$record['nik'] ?>" readonly="readonly" class="form-control"> 
          </div> 
 
          <div class="col-md-6"> 
            <label>NAME</label> 
            <input type="text" name="name" value="<?=$record['username'] ?>" readonly="readonly" class="form-control"> 
           
          </div> 
        </div> 
      </div> 

      <div class="form-group"> 
        <div class="row"> 
          <div class="col-md-6"> 
            <label>DIVISION</label> 
            <input type="text" name="divisi" value="<?=$record['level_name'] ?>" readonly="readonly" class="form-control"> 
          </div> 
 
          <div class="col-md-6"> 
            <label>FACTORY</label> 
            <input type="text" name="name" value="<?=$record['factory_name'] ?>" readonly="readonly" class="form-control"> 
           
          </div> 
        </div> 
      </div> 
 
      <div class="form-group"> 
        <div class="row">
          <form class="frmcange" method="POST"> 
          <div id="password_new_error" class="col-md-6"> 
            <label>NEW PASSWORD</label> 
            <input type="password" name="password_new" id="password_new" placeholder="Enter new password" class="form-control"> 
            <span id="password_new_danger" class="label label-danger">*Required</span> 
     
          </div> 
 
          <div id="password_confirm_error" class="col-md-6"> 
            <label>REPEAT PASSWORD</label> 
            <input type="password" name="password_confirm" id="password_confirm" placeholder="Repeat new password" class="form-control"> 
            <span id="password_confirm_danger" class="label label-danger">*Required</span> 
            <span id="message"></span> 
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary pull-right">UPDATE</button> 
          </div>
          </form> 
        </div>        
      </div> 
    </div> 
  </div>
  <script type="text/javascript" src="<?php echo base_url();?>assets/resource/change.js"></script>
