<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Detail Sewing <?=$nama_line;?>
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
          <input type="hidden" id="line" value="<?=$line;?>">
          <div class="clearfix"></div><br>
          <div class="box-body">
    
          <div class="clearfix"></div>
          <div class="table-responsive"><br>
            <table width="100%" class="table table-striped table-bordered table-hover dataTables" id="table-result">
                <thead>
                    <tr>
                        <th width="100px" class="text-center">LINE</th>
                        <th class="text-center">STYLE</th>                  
                        <th width="100px"class="text-center">STATUS</th>              
                        <th width="100px"class="text-center">ACTION</th>              
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
    </div>
    
</div>
</div>

<!-- </div>
</div> -->

<script>
var url_loading = $('#loading_gif').attr('href');
    var table = $('#table-result').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Resewing/list_style_sewing",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
              //filter
             "data":function(data) {
                        // data.tanggal = $('#tanggal').val();
                        data.line    = $('#line').val();
                        // data.balance   = $('#balance').val();
                        // data.filter    = $("input[name=filters]:checked").val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
          //sesuai nested
          //sort dari db
                  { "data": "line" },
                  { "data": "style" },                 
                  { "data": "status" },
                  { "data": "action" ,'sortable' : false },
               ],
               
        });

    function resewing(line,style,target,sewing) {
        swal({
            title     : "PERHATIAN!!!",
            text      : "Apakah anda yakin untuk mengaktifkan kembali style?",
            icon      : "warning",
            buttons   : true,
            dangerMode: true,
        })
        .then((wilsave) => {
            if (wilsave) {
                var data = 'line='+line+'&style='+style+'&target='+target+'&sewing='+sewing;
                $.ajax({
                    url        : 'Resewing/reactive_sewing',
                    data       : data,
                    type       : "POST",
                    beforeSend : function () {				
                        $.blockUI({
                            message: "<img src='" + url_loading + "' />",
                            css: {
                                backgroundColor: 'transaparant',
                                border: 'none',
                            }
                        });
                    },
                    success: function(response){
                        var result = JSON.parse(response);
                        if (result.status==200) {
                            $('#myModal').modal('hide');
                            $("#alert_success").trigger("click", 'Style Kembali Aktif');
                            $('#refresh').trigger("click");	
                            location.href = location.href;
                        }else{
                            $("#alert_error").trigger("click", 'Reactive Gagal');
                            $('#refresh').trigger("click");	
                        }
                    }
                    ,error:function(data){
                        if (data.status==500) {
                            $.unblockUI();
                            $("#alert_warning").trigger("click", 'Reactive Gagal');
                            $('#refresh').trigger("click");	
                        }
                    }
                });   
            }
            $.unblockUI(); //buat menutup modal
        });   
        return false;
    }	


</script>