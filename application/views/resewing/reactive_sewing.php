<div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Reactive Style Sewing</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Home</a></li>
        <li><i class="fa fa-bars"></i>Reactive Style Sewing</li>
      </ol>
    </div>
  </div>
<!-- </div> -->
<div class="row">

<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-6">

              <!-- <form class="search_style" method="POST"> -->
              <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pilih LINE</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadline()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="line_id" id="line_id">
                        <input type="text" class="form-control" name="line_name" id="line_name" onclick="return loadline()" readonly="readonly" placeholder="PILIH LINE">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>
                    </div>
                    </td>
                  </tr>
                </tbody>
            </table>
            <button class="btn btn-primary btn-lg btn-block" id="search"><i class="fa fa-search"></i> Cari</button>
          <!-- </form> -->
          </div>        
        
        </div>
    </div>
</div>
</div>
  
  <div id="result">    
  </div>

<div class="myDIV"></div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){

      $('#clear_line').click(function() {
        table.draw();
      });
      
      $('#search').click(function() {          
          var line    = $('#line_id').val();

          if (!line) {
              $("#alert_warning").trigger("click", 'PILIH LINE TERLEBIH DAHULU');
          }else{
            var data = 'line='+line;
            $.ajax({
              url :'Resewing/search_sewing',
              type: "POST",
              data: data,
              beforeSend: function () {
                  $.blockUI({
                    message: "<img src='" + url_loading + "' />",
                    css: {
                      backgroundColor: 'transaparant',
                      border: 'none',
                    }
                  });
              },
              success: function(response){
                  $('#result').html(response);
                  $.unblockUI();

              },
              error:function(response){
                if (response.status==500) {
                  $("#alert_info").trigger("click", 'Cek Input Data');
                  $.unblockUI();
                }
              }
            });
          }

          
      });      
  });

function loadline() {
	var url_loading = $('#loading_gif').attr('href');
	$("#myModal").modal('show');
	$('.modal-title').text("Pilih LINE");
	$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
	$('.modal-body').load('resewing/showLine');
	return false;
}
</script>