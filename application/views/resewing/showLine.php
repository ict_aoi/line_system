<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          
            <table id="table-line" class="table table-striped table-bordered  dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <!-- <th class="text-center" width="20px">NO</th> -->
                        <th>NAMA LINE</th>
                    </tr>
                </thead>
                <tbody>
                    <div id="result"><?php echo $draw ?></div>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(function(){
        $('#table-line').DataTable({
            'paging':true,
              'lengthChange': true,
              'searching'   : true,
              'ordering'    : false,
              'info'        : true,
              'autoWidth'   : true
        });
      });
    function selectline(line_id,line_name) {
        $('#line_id').val(line_id);
        $('#line_name').val(line_name);
        $("#myModal").modal('hide');
        $('#refresh').trigger("click"); 
        return false;
    }
</script>