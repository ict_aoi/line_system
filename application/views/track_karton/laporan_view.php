<script src="<?=base_url('assets/plugins/datepickerrange/moment.min.js');?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/datepickerrange/daterangepicker.min.css');?>" />
<script src="<?=base_url('assets/plugins/datepickerrange/jquery.daterangepicker.min.js');?>"></script>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> TRACK Karton</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>TRACK Karton</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body">
    <div class=" alert alert-danger fade in">
      <button data-dismiss="alert" class="close close-sm" type="button">
          <i class="icon-remove"></i>
      </button>
      <strong>INFORMASI</strong><br> Masukkan PO Buyer dengan <b>lengkap</b><br> Jika <b>rasio</b>, kolom size <b>tidak perlu</b> diisi.
    </div>
    <div class="col-md-3">
      <div class="box box-success">
        <div class="box-header with-border">  
          <div class="box-title"></div>
            <div class="box-body">
              <div class="form-group">
                <label>PO Buyer</label>
                <div class="input-group">
                  <input class="form-control" name="po_buyer" id="po_buyer" autocomplete="off"/>
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-default btn-icon" id="clear_line_po"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <label>Size</label>
                <div class="input-group">
                  <input class="form-control" name="size" id="size" autocomplete="off"/>
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-default btn-icon" id="clear_line_size"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <label>Apakah Rasio?</label>
                <div class="input-group">
                  <select class="form-control select2" id="rasio" name="rasio" style="width:250px">
                      <option value="">-- Pilih --</option>
                      <option value="t">Rasio</option>
                      <option value="f">Tidak Rasio</option>
                  </select>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="pull-right">
              <button id="filter_D" type="button" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                
                <!-- <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
</div>
 
    <div id="result">
            
    </div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
    $('#clear_line_po').click(function() {
        $('#po_buyer').val('');
    });
    
    $('#clear_line_size').click(function() {
        $('#size').val('');
    });

          
    $('#filter_D').click(function() {
        // var tgl1     = $('#daterange1').val();
        // var tgl2     = $('#daterange2').val();
        var po_buyer = $('#po_buyer').val();
        var size     = $('#size').val();
        var rasio     = $('#rasio').val();
        // var laporan  = $('#laporan').val();


        // alert(tgl1 + " = " + tgl2 + " = " + po_buyer + " = " + laporan);
        if(!rasio){
          $("#alert_warning").trigger("click", 'Rasio tidak?');
        }
        else{
          if(rasio=='t'){
            $('#size').val('');
          }
          if (!po_buyer) {
            $("#alert_warning").trigger("click", 'PO Buyer Tidak Boleh Kosong');
          }else{
            var data = 'po='+po_buyer+'&size='+size+'&rasio='+rasio;
            $.ajax({
              url:'filter_find_karton',
                      type: "POST",
                      data: data,
                      beforeSend: function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                  }
                });
              },
              success: function(response){
                // console.log(response);
                      // if (response=='gagal') {
                      //   $("#alert_warning").trigger("click", 'Data Tidak Ada');
                      //   $.unblockUI();
                      // }else{
                        $('#result').html(response);
                        $.unblockUI();
                      // }

                    },
                    error:function(response){
                    if (response.status==500) {
                      $("#alert_info").trigger("click", 'Cek Input Data');
                      $.unblockUI();
                    }
                }
            });
          }
        }

        
    });

});
</script>