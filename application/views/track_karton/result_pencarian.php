<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->


<!-- <div class="panel panel-default border-grey"> 
<div class="panel-body"> -->
<!-- <div class="row">
<div class="col-md-12"> -->
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Track Karton
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        <div class="panel-body">
        
          <input type="hidden" id="po" value="<?=$po;?>">
          <input type="hidden" id="size" value="<?=$size;?>">
          <input type="hidden" id="rasio" value="<?=$rasio;?>">
          <div class="clearfix"></div><br>
          <div class="box-body">
    
          <div class="clearfix"></div>

          <header class="panel-heading tab-bg-info">
              <ul class="nav nav-tabs">
                  <li class="active">
                      <a data-toggle="tab" href="#sewing" onclick="changeTabUser('sewing')">SEWING</a>
                  </li>
                  <li class="">
                      <a data-toggle="tab" href="#packing" onclick="changeTabUser('packing')">PACKING</a>
                  </li>
              </ul>
          </header>
          <div class="panel-body">
              <div class="tab-content">
                  <input type="hidden" id="active_tab_user" value="">
                  <div id="sewing" class="tab-pane active">
                    <div style="height:100%; overflow-x:auto;overflow-y:auto">
                      <table width="100%" class="table table-striped table-bordered table-hover dataTables" id="table-result">
                          <thead>
                              <tr>
                                  <!-- <th class="text-center">DATE</th> -->
                                  <th class="text-center">BARCODE</th>                  
                                  <th class="text-center">PO BUYER</th>              
                                  <th class="text-center">SIZE</th>              
                                  <th class="text-center">STATUS</th>
                                  <th class="text-center">TOTAL SCAN</th>
                                  <th class="text-center">ISI KARTON</th>
                                  <!-- <th class="text-center">ACTION</th> -->
                              </tr>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>
                    </div>
                  </div>
                  <div id="packing" class="tab-pane">
                    <div style="height:100%; overflow-x:auto;overflow-y:auto">
                      <table width="100%" class="table table-striped table-bordered table-hover dataTables" id="table-result2">
                          <thead>
                              <tr>
                                  <!-- <th class="text-center">DATE</th> -->
                                  <th class="text-center">BARCODE</th>                  
                                  <th class="text-center">PO BUYER</th>              
                                  <th class="text-center">SIZE</th>           
                                  <th class="text-center">ISI KARTON</th>
                                  <th class="text-center">CURRENT DEPARTMENT</th>
                                  <!-- <th class="text-center">ACTION</th> -->
                              </tr>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>
                    </div>
                  </div>
              </div>
          </div>
          <!-- <div class="table-responsive"><br> -->
            
          </div>
        </div>
    </div>
<!--     
</div>
</div> -->

<!-- </div>
</div> -->

<script>

  $(document).ready( function () {  
    var url_loading = $('#loading_gif').attr('href');
    $('#table-result').DataTable().destroy();
				$('#table-result tbody').empty();
        var table = $('#table-result').DataTable({
          "processing": true,
          "serverSide": true,
          "orderMulti"  : true,
          "searching": false,
          sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
          "ajax":{
            "url": "find_karton_line_ajax",
            "dataType": "json",
            "type": "POST",
            "beforeSend": function () {
              $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transparant',
                  border: 'none',
                }
              });
            },
            "complete": function() {
              $.unblockUI();
            },
            fixedColumns: true,
            //filter
            "data":function(data) {
              data.pobuyer = $('#po').val();
              data.size    = $('#size').val();
              data.rasio   = $('#rasio').val();
              data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            },
          },
          "columns": [
            { "data": "barcode_id" },                 
            { "data": "poreference" },
            { "data": "size" },
            { "data": "status" },
            { "data": "total" },
            { "data": "inner" },
          ],
        });

    $('#active_tab_user').on('change',function()
    {
      var active_tab = $('#active_tab_user').val();
      if(active_tab == 'sewing')
      {
        $('#table-result').DataTable().destroy();
				$('#table-result tbody').empty();
        var table = $('#table-result').DataTable({
          "processing": true,
          "serverSide": true,
          "orderMulti"  : true,
          "searching": false,
          sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
          "ajax":{
            "url": "find_karton_line_ajax",
            "dataType": "json",
            "type": "POST",
            "beforeSend": function () {
              $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transparant',
                  border: 'none',
                }
              });
            },
            "complete": function() {
              $.unblockUI();
            },
            fixedColumns: true,
            //filter
            "data":function(data) {
              data.pobuyer = $('#po').val();
              data.size    = $('#size').val();
              data.rasio   = $('#rasio').val();
              data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            },
          },
          "columns": [
            { "data": "barcode_id" },                 
            { "data": "poreference" },
            { "data": "size" },
            { "data": "status" },
            { "data": "total" },
            { "data": "inner" },
          ],
        });
      }else if(active_tab == 'packing')
      {
        
        $('#table-result2').DataTable().destroy();
				$('#table-result2 tbody').empty();
        var table2 = $('#table-result2').DataTable({
          "processing": true,
          "serverSide": true,
          "orderMulti"  : true,
          "searching": false,
          sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
          "ajax":{
            "url": "find_karton_packing_ajax",
            "dataType": "json",
            "type": "POST",
            "beforeSend": function () {
              $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transparant',
                  border: 'none',
                }
              });
            },
            "complete": function() {
              $.unblockUI();
            },
            fixedColumns: true,
            //filter
            "data":function(data) {
              data.pobuyer = $('#po').val();
              data.size    = $('#size').val();
              data.rasio   = $('#rasio').val();
              data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            },
          },
          "columns": [
            { "data": "barcode_id" },                 
            { "data": "poreference" },
            { "data": "size" },
            { "data": "inner" },
            { "data": "status" },
          ],
        });
      }
    });
  });

  
  function changeTabUser(status)
  {
    $('#active_tab_user').val(status).trigger('change');
  }
      
  
  
</script>