<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    
    <div class="panel panel-default">
      <br>
        <div class="col-md-8 pull-right alert alert-success fade in">
          <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="icon-remove"></i>
          </button>
          <strong>INFORMASI</strong><br> Masukan PO BUYER Untuk Lihat Status PO .<br>Ketik PO BUYER Lengkap / 4 Digit PO terakhir klik cari/tekan enter untuk melihat data <br>
          Jika data tidak ada kemungkinan belum ada inputan untuk PO tersebut.
        </div>
        <div class="clearfix"></div>
        <div class="panel-body">
          <div class="col-md-5">
                <div class="input-group  text-center">
                <input type="text" class="col-md-5 form-control" placeholder="Masukan PO BUYER" id="pobuyer" name="poreference">
                <span class="input-group-btn">
                  <button class="btn btn-primary submit" type="submit">Cari</button>
                </span>
              </div>
          </div>
          <div class="clearfix"></div><br><br>
          <div id="hasil"></div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
  $(function () {
    $('.submit').click(function() {
      var poreference = $('#pobuyer').val();
      if (poreference!='') {
        loaddata();
      }
    });
    $('#pobuyer').change(function() {
      var poreference = $('#pobuyer').val();
      if (poreference!='') {
        loaddata();
      }
    });
    function loaddata() {     
      var data = 'poreference='+$('#pobuyer').val();

          $.ajax({
              url:'<?=base_url('Qc/statusposubmit') ?>',
              data:data,
              type:'POST',
              beforeSend: function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                  }
                });
              },              
              success:function(data){
                  $.unblockUI();
                  $('#hasil').html(data);                  
              },
              error:function(response){
                  if (response.status==500) {
                    $.unblockUI();
                  }                    
              }
          })

    }
    $('#pobuyer').focus();
    loaddata();
  });
  function selectstatus(header_id,size_id,style,po,size) {
    $('#id').val(header_id);
    $('#poreference').val(po);
    $('#style').val(style);
    $('#size').val(size);
    $('#size_id').val(size_id);
    $("#myModal").modal('hide');
    $('.search_header').trigger("submit");
  }
  
</script>
