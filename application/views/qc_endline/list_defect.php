<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            
            <form class="adddefect" method='POST'>
                <div class="row">
                    <div class="col-md-3">
                        <button class="btn btn-primary btn-lg kirim" type="submit">KIRIM</button>
                    </div>
                    <div class="col-md-2">
                        <label style="font-size: 20px; font-weight: bold;"><input style="width: 30px; height: auto;" type="checkbox" name="checkgrade" id="checkgradeB" value="B">B Grade</label>
                    </div>
                    <div class="col-md-2">
                        <label style="font-size: 20px; font-weight: bold;"><input style="width: 30px; height: auto;" type="checkbox" name="checkgrade" id="checkgradeC" value="C">C Grade</label>
                    </div>
                </div>

                <input type="hidden" name="id" value="<?=$id?>">
                <input type="hidden" name="style" value="<?=$style?>">
                <input type="hidden" name="size_id" value="<?=$size_id?>">
                <input type="hidden" name="trans_id" value="<?=$trans_id?>">
                <font size="5">
            <table id="table-defect" class="table table-striped table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">#</th>
                        <th>DEFECT NAME</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php  
                        foreach ($defect as $key => $d) {
                            echo "<tr>";
                                echo "<td><input type='checkbox' name='defect_id[]' id='def_".$d->defect_id."' value='".$d->defect_id."' onClick='ckChange(this)'></td>";
                                echo "<td style='padding:0px; overflow:hidden; cursor:pointer'><label for='def_".$d->defect_id."'>".$d->defect_code."  ".$d->defect_jenis."</label></td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </font>
            </form>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#table-defect').DataTable({
            'paging':true,
              'lengthChange': true,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : true
        });
        $('.adddefect').submit(function () {
            var data = new FormData(this);
            $.ajax({
                url:'Qc/adddefectsubmit',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false,
                success: function(response){
                    var result = JSON.parse(response);
                    
                    if (result.status==200) {
                        $("#myModal").modal('hide');
                    }else{
                        $("#alert_warning").trigger("click", 'Gagal');
                    }
                    $('#refresh').trigger("click");
                },
                  error:function(response){
                    if (response.status==500) {
                      $("#alert_info").trigger("click", 'Failed');
                    }
                }

            });
            return false;
        });

        $('#checkgradeB').click(function(){
            if ($(this).is(':checked')==true) {
                $('#checkgradeC').prop('checked',false);
            }
        });

        $('#checkgradeC').click(function(){
            if ($(this).is(':checked')==true) {
                $('#checkgradeB').prop('checked',false);
            }
        });
    });
    function ckChange(ckType){
        var ckName = document.getElementsByName(ckType.name);
        var checked = document.getElementById(ckType.id);

        if (checked.checked) {
          for(var i=0; i < ckName.length; i++){

              if(!ckName[i].checked){
                  ckName[i].disabled = true;
              }else{
                  ckName[i].disabled = false;
              }
          } 
        }
        else {
          for(var i=0; i < ckName.length; i++){
            ckName[i].disabled = false;
          } 
        }    
    }
</script>