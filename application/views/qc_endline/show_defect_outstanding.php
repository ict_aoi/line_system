<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    
    <div class="panel panel-default">
        <div class="alert alert-success fade in">
      <button data-dismiss="alert" class="close close-sm" type="button">
          <i class="icon-remove"></i>
      </button>
      <strong><Informasi</strong><br> Klik PO Buyer Untuk Repair Garment .
    </div>
        <div class="panel-body">
            <table id="table-defectoutstanding" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center">NO.</th>
                            <th class="text-center">PO</th>
                            <th class="text-center">STYLE</th>
                            <th class="text-center">ARTICLE</th>
                            <th class="text-center">SIZE</th>
                            <th class="text-center">TOTAL REPAIRING</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no =1;
                           foreach ($listdefects->result() as $key => $listdefect) {
                               $temp = '"'.$listdefect->inline_header_id.'","'.$listdefect->header_size_id.'","'.$listdefect->style.'","'.$listdefect->poreference.'","'.$listdefect->size.'"';
                               echo "<tr onclick='return selectdefect($temp)'>
                                       <td>".$no++."</td>
                                       <td>".$listdefect->poreference."</td>
                                       <td>".$listdefect->style."</td>
                                       <td>".$listdefect->article."</td>
                                       <td>".$listdefect->size."</td>
                                       <td class='text-right'>".$listdefect->repairing."</td>
                                   </tr>";
                       
                           }
                       ?>
                    </tbody>
                </table>
            <!-- </div> -->

        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    function selectdefect(header_id,size_id,style,po,size) {
        $('#id').val(header_id);
        $('#poreference').val(po);
        $('#style').val(style);
        $('#size').val(size);
        $('#size_id').val(size_id);
        $("#myModal").modal('hide');
        $('.search_header').trigger("submit");
    }
</script>