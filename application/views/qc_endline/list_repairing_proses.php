
<div class="panel panel-default border-grey"> 
    <div class="panel-body">
	     
		<div class="panel-body">
            <table id="table-proses" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">NO</th>
                        <th>Kode Defect</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  
                        $no =1;
                        foreach ($listrepairing as $key => $list) {
                            $temp = '"'.$list->qc_endline_trans_id.'","'.$list->qc_endline_id.'"';
                            echo "<tr onclick='return selectdefect($temp)'>";
                                echo "<td>".$no++."</td>";
                                echo "<td>".$list->defect_id."</td>";
                                echo "<td>".$list->create_date."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    function selectdefect(id,endline_id) {

        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        // modal.children('.modal-title').text('Defect List');
        modal.parent('.modal-dialog').addClass('modal-md');
        modal.children('.modal-body').load('Qc/repairing_proses?id='+id+'&endline_id='+endline_id); 
        return false; 
    }
</script>