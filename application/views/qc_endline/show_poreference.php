<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>

<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
            <font size="5">
            <table id="table-po" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">NO</th>
                        <th>PO BUYER</th>
                        <th>STYLE</th>
                        <th>ARTICLE</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no=1; 
                        foreach ($listpo->result() as $list) {
                            $temp = '"'.$list->inline_header_id.'","'.$list->poreference.'","'.$list->style.'","'.$list->article.'"';
                            echo "<tr onclick='return selectpo($temp)'>";
                                echo "<td>".$no++."</td>";
                                echo "<td>".$list->poreference."</td>";
                                echo "<td>".$list->style."</td>";
                                echo "<td>".$list->article."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </font>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script>
 $(function(){
    $('#table-po').DataTable({
        'paging':true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
    });
  });
 function selectpo(id,po,style,article) {
    var url_loading = $('#loading_gif').attr('href');
    $('#id').val(id);
    $('#poreference').val(po);
    $('#style').val(style);
    $('#article').val(article);
	var modal = $('#myModal > div > div');
	$('#myModal').modal('show');
	modal.children('.modal-header').children('.modal-title').html('Pilih Size');
	modal.parent('.modal-dialog').addClass('modal-lg');
	modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
	modal.children('.modal-body').load('qc/loadsize?id='+id);
    return false;
 }
</script>