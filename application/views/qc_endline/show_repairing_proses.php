<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            
            <form class="defectrepair" method='POST'>
                <button class="btn btn-primary btn-lg kirim" type="submit">KIRIM</button>
                <input type="hidden" name="id" value="<?=$id?>">
                <input type="hidden" name="endline_id" value="<?=$endline_id?>">
                <font size="5">
            <table id="table-defect" class="table table-bordered table-full-width" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">#</th>
                        <th>DEFECT NAME</th>
                    </tr>
                </thead>
                <tbody>
                    <?php                          
                        foreach ($defect_in->result() as $key => $i) {
                            echo "<tr>";
                                echo "<td><input type='checkbox' name='defect_id[]' id='def_".$i->defect_id."' value='".$i->defect_id."' name='defect[]' checked></td>";
                                echo "<td style='padding:0px; overflow:hidden; cursor:pointer'><label for='def_".$i->defect_id."'>".substr($i->defect_id.". ".$i->defect_jenis,0,80)."</label></td>";
                            echo "</tr>";
                        }
                        foreach ($defect_not->result() as $key => $i) {
                            echo "<tr>";
                                echo "<td><input type='checkbox' name='adddefect_id[]' id='def_".$i->defect_id."' value='".$i->defect_id."' name='defect[]' disabled></td>";
                                echo "<td style='padding:0px; overflow:hidden; cursor:pointer'><label for='def_".$i->defect_id."'>".substr($i->defect_id.". ".$i->defect_jenis,0,80)."</label></td>";
                            echo "</tr>";
                        } 
                    ?>
                </tbody>
            </table>
        </font>
            </form>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#table-defect').DataTable({
            'paging':true,
              'lengthChange': true,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : true
        });
        $('.defectrepair').submit(function () {
            var data = new FormData(this);
            $.ajax({
                url:'Qc/submit_repairing',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false,
                success: function(response){
                    var result = JSON.parse(response);
                    
                    if (result.status==200) {
                         $("#myModal_").modal('hide');
                         $('#myModal').modal('hide');
                         $('#refresh').trigger("click"); 
                    }else{
                        $("#alert_warning").trigger("click", 'Gagal');
                    }
                    $('#refresh').trigger("click");
                },
                  error:function(response){
                    if (response.status==500) {
                      $("#alert_info").trigger("click", 'Failed');
                    }
                }

            });
            return false;
        })
    });
</script>