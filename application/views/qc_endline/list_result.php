<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box green-bg">
                        <i class="fa fa-thumbs-o-up"></i>
                    <div class="count"><h3 class="no-margin text-semibold" id="total_qcinspect" style="font-size: 28px;"><span class="total"></span></h3></div>
                    <div class="title"><span class="text-uppercase text-size-mini text-muted">Total QC</span></div>
                </div>       
            </div><!--/.col-->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box red-bg">
                    <i class="fa fa-thumbs-o-down"></i>
                    <div class="count"><h3 class="no-margin text-semibold" id="total_qcinspect" style="font-size: 28px;"><span class="totaldefect"></span></h3></div>
                    <div class="title"><span class="text-uppercase text-size-mini text-muted">Total Defect</span></div>                      
                </div><!--/.info-box-->         
            </div><!--/.col-->  
            <!-- <div style="overflow-x:auto;overflow-y:auto"> -->
            <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center">PO</th>
                            <th class="text-center">STYLE</th>
                            <th class="text-center">ARTICLE</th>
                            <th class="text-center">SIZE</th>
                            <th class="text-center">QTY ORDER</th>
                            <th class="text-center">Total QTY Input</th>
                            <th class="text-center">Counter Per Day</th>
                            <th class="text-center">BALANCE</th>
                            <th class="text-center">Defect Per Day</th>
                            <th class="text-center">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sum = 0;
                            $sumdefect = 0;
                            foreach ($results as $key => $value) {
                                $_temp = '"'.$value->qc_endline_id.'","'.$value->poreference.'","'.$value->size.'","'.$value->article.'"';
                                echo "<tr>
                                        <td>".$value->poreference."</td>
                                        <td>".$value->style."</td>
                                        <td>".$value->article."</td>
                                        <td>".$value->size."</td>
                                        <td class='text-right'>".$value->order."</td>
                                        <td class='text-right'>".$value->counter_qc."</td>
                                        <td class='text-right'>".$value->daily_output."</td>
                                        <td class='text-right'>".$value->balance_per_day."</td>
                                        <td class='text-right'>".$value->defect_perday."</td>";
                                    if ($value->defect_perday>0) {
                                        echo "<td class='text-center'>
                                                <div class='btn-group'>
                                                  <a onclick='return pilih($_temp)' class='btn btn-primary' href='javascript:void(0)'><i class='fa fa-eye'></i></a>
                                              </div>
                                              </td>";
                                    }else{
                                        echo "<td class='text-right'></td>";
                                    }
                                   echo  "</tr>";
                                    $sum+=$value->daily_output;
                                    $sumdefect+=$value->defect_perday;
                            }
                            echo "<input type='text' style='visibility: hidden' id='sumdaily' value='$sum'>";
                            echo "<input type='text' style='visibility: hidden' id='sumdefect' value='$sumdefect'>";
                        ?>
                    </tbody>
                </table>
            <!-- </div> -->

        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(function () {
        $('#table-result').DataTable({
            'paging':true,
              'lengthChange': true,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : true
        });
        var sumdaily=$('#sumdaily').val();
        var sumdefect=$('#sumdefect').val();
        
        $('.total').text(sumdaily);
        $('.totaldefect').text(sumdefect);
    });
    function pilih(qc_endline_id,poreference,size,article) {
        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Detail Defect PO <b>'+poreference+' <b> Article '+article+'<b> Size '+size);
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html('<center>Loading..</center>');
        modal.children('.modal-body').load('Qc/detaildefect?qc_id='+qc_endline_id);
        return false;  
    }
</script>