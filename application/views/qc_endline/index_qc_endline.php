<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<script type="text/javascript">
    $(function(){
        setInterval(function(){
            var d = new Date();
            var detik = d.getSeconds();
            var menit = d.getMinutes();
            var jam = d.getHours();
            $('.tgl > span').html(jam +':'+ nol(menit));
        },30000);
    })
    function nol(num){
        if(num < 10)
            return '0' + num;
        else
            return num;
    }
</script>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="col-md-6"><h3>QC Endline - <?php echo $line['line_name']; ?></h3></div>
            	<div class="col-md-6"><div class="pull-right tgl"><?=date('d-m-Y');?> <span><?=date('H:i');?></span></div></div>
                
              <form class="search_header" method="POST">
              <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>PO BUYER</th>
                        <th>SIZE</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadpo()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="style" id="style">
                        <input type="hidden" name="article" id="article">
                        <input type="text" class="form-control" name="poreference" id="poreference" onclick="return loadpo()" readonly="readonly" placeholder="PILIH PO BUYER">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_po"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>                
                    </div>   
                    </td>
                    <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" onclick="return loadsize()"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="text" class="form-control" name="size" style="left" id="size" onclick="return loadsize()" readonly="readonly" placeholder="PILIH SIZE">
                        <input type="hidden" id="size_id" name="size_id">
                    </td>
                  </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-search"></i> Cari</button>
          </form>
            </div>
        </div>
    </div>
    <div id="result">
        
    </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/qc_endline_chose.js"></script>