<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
   
        <div class="panel-body">
            <table id="table-detail" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="20px" class="text-center">Kode Defect</th>
                            <th class="text-center">Jenis Defect</th>
                            <th class="text-center">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($defects as $key => $defect): ?>
                          <tr>
                            <td><?=$defect->defect_id;?></td>
                            <td><?=$defect->defect_jenis;?></td>
                            <td><?=$defect->jumlah_defect;?></td>
                          </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            <!-- </div> -->

        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>