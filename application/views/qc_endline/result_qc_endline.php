<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<script src="<?=Base_url();?>assets/js/jquery.numpad.js"></script>
    <script src="<?=Base_url();?>assets/js/keyboard.js"></script>
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/jquery.numpad.css">
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/keyboard.css">
    <script src="<?php echo base_url();?>assets/plugins/blockUI/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/resource/notification.js"></script>
<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Endline Inspection
             </div>
             <div class="col-md-3 pull-right">
                <span class="style"></span> -
                <span class="article"></span>
             </div>
           </h6>
        </div>
        <div class="panel-body">
          <input type="hidden" id="id" value="<?php echo $id;?>">
          <input type="hidden" id="size_id" value="<?php echo $size_id;?>">
          <input type="hidden" id="qc_id">
          <input type="hidden" id="line_id" value="<?php echo $line; ?>">
          <input type="hidden" id="factory_id" value="<?php echo $factory; ?>">
          <br>
          <div class="col-md-6">
                <div class="btn-row">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary" id="view_result"><i class="fa fa-calculator"></i>Summary Cek</button>
                      <button type="button" class="btn btn-danger" id="defect_outstanding">Defect Outstanding</button>
                      <button type="button" onclick="return statuspo()" class="btn btn-success">Status PO</button>
                    </div>
                </div>
                <!-- <button class="btn btn-info" id="view_result">Summary Cek</button> -->
          </div>
          <div class="col-md-6 text-right">
            <div class="form-group"> 
              <div class="row"> 
                <div class="col-md-6">
                  <span><h3>PO BUYER : <b><?php echo $poreference; ?></b></h3>
                </div> 
              
              <div class="col-md-6"> 
                <span><h3>SIZE : <b><?php echo $size; ?></b></h3></span>
              
              </div> 
              </div> 
            </div>
            
            
          </div>
          <div class="clearfix"></div>
            <table class="table table-striped table-bordered table-hover table-full-width">
                <thead>
                    <tr>
                      <th width="200px" class="text-center">WIP</th>
                      <th class="text-center">REPAIRING</th>
                      <th class="text-center">CHECKED</th>
                      <th width="150px" class="text-center"><span id="balance-head">BALANCE</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="wft"></span></td>
                        <td class="text-center" style="font-size: 30px"><a onclick="return repairing()" href="javascript:void(0)"><span class="repairing"></span></a></td>
                        <td class="text-center" style="font-size: 30px"><span class="daily"></span></td>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="balance"></span></td>
                    </tr>
                    <tr>
                        <td class="text-center"><span class="total_repairing"></span></td>
                        <td class="text-center"><span class="all"></span></td>
                    </tr>
                    <tr>
                      <td colspan="2"><button type="button" id="good" style="height: 75px;font-size:40px"  class="btn btn-success btn-lg btn-block">Good</button></td>
                      <td colspan="2"><button type="button" id="defect" style="height: 75px;font-size:40px"  class="btn btn-danger btn-lg btn-block">Defect</button></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        
                        <div class="input-group">
                          <div class="input-group-btn">
                              <button type="submit" class="btn btn-xs btn-primary" id="change"><i class="fa fa-files-o"></i></button>
                          </div>
                         <!--  <input type="number" class="form-control hidden" name="qty" id="qty" placeholder="Masukan QTY"> -->   
                         <div class="input-group">
                            <input type="number" id="numpadButton" class="form-control qty hidden" placeholder="Masukan QTY" aria-describedby="numpadButton-btn">
                            <span class="input-group-btn">
                              <button class="btn btn-default keyboard hidden" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
                            </span>
                          </div>          
                      </div> 
                      <input type="hidden" id="flag" value="0">
                      </td>
                </tbody>    
            </table>
        </div>
    </div>
    
</div>
</div>
<button type="button" class="hidden" id="refresh"></button>
<button type="button" id="cek_seq"></button>
<a href="<?php echo base_url('Qc') ?>" id="logo" class="hidden"></a>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/qc_endline_result.js"></script>
<script type="text/javascript">

// $(document).ready(function() {

  
//     $('#cek_seq').one("click", function () {
//       var data    = 'id='+$('#id').val()+'&size_id='+$('#size_id').val();
        
//         //var data = 'line='+$('#line').val()+'&factory='+$('#factory_id').val();	
    
//         $.ajax({
//           url: 'qc/cek_sequence',
//           data:data,
//           type:'POST',	           
//           success:function(data){
//             var result = JSON.parse(data);
            
//             $('.all').text(result.counter_qc);
//             $('.daily').text(result.counter_daily);
            
//           },
//           error:function(){
//             // window.location.replace(base_url);
//           }
//         });
//     });

//   $(document).idle({
        
//         onIdle: function(){

//           $( "#cek_seq" ).trigger("click");
          
//         },
//         idle: 60000
//       });
//     });


  // $(document).idle({
        
  //       onIdle: function(){
      
  //         var data    = 'id='+$('#id').val()+'&size_id='+$('#size_id').val();
      
  //         //var data = 'line='+$('#line').val()+'&factory='+$('#factory_id').val();	
      
  //         $.ajax({
  //           url: 'qc/cek_sequence',
  //           data:data,
  //           type:'POST',	           
  //           success:function(data){
  //             var result = JSON.parse(data);
              
  //             $('.all').text(result.counter_qc);
  //             $('.daily').text(result.counter_daily);
              
  //           },
  //           error:function(){
  //             // window.location.replace(base_url);
  //           }
  //         });
          
  //         // counter_sewing();      
          
  //       },
  //       idle: 3000
  //     });
  //   });
  
  
</script>

