
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Supply Time
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
          <input type="hidden" id="dari" value="<?=$dari;?>">
          <input type="hidden" id="sampai" value="<?=$sampai;?>">
          <input type="hidden" id="line_id" value="<?=$line_id;?>">
          <div class="clearfix"></div>
          <div class="box-body">
            <div class="clearfix"></div>
            <div class="table-responsive"><br>
                <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
                        <th class="text-center">LINE</th>
                        <th class="text-center">JAM</th>
                        <th class="text-center">TOTAL QTY</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
    
</div>
</div>

<script>
    var url_loading = $('#loading_gif').attr('href');
    var table = $('#table-result').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "orderMulti"  : true,
        "searching": false,
        sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        "ajax":{
            "url"       : "report_supply_time_ajax",
            "dataType"  : "json",
            "type"      : "POST",
            "beforeSend": function () {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                backgroundColor: 'transparant',
                border: 'none',
                }
            });
            },
            "complete": function() {
            $.unblockUI();
            },
            fixedColumns: true,
            "data":function(data) {
                    data.dari         = $('#dari').val();
                    data.sampai       = $('#sampai').val();
                    data.line_id      = $('#line_id').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    },
                        },
            "columns": [
                { "data": "line_name" },
                { "data": "jam"},
                { "data": "qty"},
            ] 
    });


</script>