<script src="<?=base_url('assets/plugins/datepickerrange/moment.min.js');?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/datepickerrange/daterangepicker.min.css');?>" />
<script src="<?=base_url('assets/plugins/datepickerrange/jquery.daterangepicker.min.js');?>"></script>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> PRODUCTION OUTPUT</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Production Output PerJam</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <div class="panel-body">  
   <div class="col-md-8">
    <form action="<?php echo base_url('report/downloadhourlyoutput')?>" method="POST">
      <div class="form-group" id="two-inputs"> 
          <div class="row"> 
            <div class="col-md-4">
              <label>Dari</label> 
              <input id="daterange1" class="form-control" name="dari" placeholder="Tanggal Awal" required>
            </div> 
   
            <div class="col-md-4"> 
              <label>Sampai</label> 
              <input id="daterange2" class="form-control" name="sampai" placeholder="Tanggal Akhir" required>                
            </div> 
          </div>
          <br>

      </div>
      <div class="form-group">
        <div class="col-md-3">
          <select class="form-control" name="filter" required>
            <option value="">-- Pilih --</option>
            <option value="0">Global</option>
            <option value="1">Data Entry Per PO</option>
            <option value="2">Data Entry TLS Inline</option>
          </select>
        </div>
      </div>
      <button class="btn btn-info" type="submit" id="download"><i class="clip-file-excel"></i> Download</button>
    </form>

</div>
</div>
</div>
<script type="text/javascript">
  $(function () {
      $('#two-inputs').dateRangePicker(
        {
          
          getValue: function()
          {
            if ($('#daterange1').val() && $('#daterange2').val() )
              return $('#daterange1').val() + ' to ' + $('#daterange2').val();
            else
              return '';
          },
          setValue: function(s,s1,s2)
          {
            $('#daterange1').val(s1);
            $('#daterange2').val(s2);
            autoclose: true
          }

        });    
  });
</script>
