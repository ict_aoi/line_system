<script src="<?=base_url('assets/plugins/datepickerrange/moment.min.js');?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/datepickerrange/daterangepicker.min.css');?>" />
<script src="<?=base_url('assets/plugins/datepickerrange/jquery.daterangepicker.min.js');?>"></script>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> DISTRIBUTION RECEIVE</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Distribution Receive</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-3">
      <div class="box box-success">
        <div class="box-header with-border">
          <div class="box-title">Pilih Tanggal Laporan</div>
            <div class="box-body">
              <div class="form-group">
                <div id="two-inputs"> 
                  <div class="input-group">
                    <label>Dari</label> 
                    <input id="daterange1" class="form-control" name="dari" placeholder="Tanggal Awal" required>
                      
                    <label>Sampai</label> 
                    <input id="daterange2" class="form-control" name="sampai" placeholder="Tanggal Akhir" required>                
                  </div> 
                </div>
                <!-- <br> -->
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        
        
        <div class="box-title">Input PO Buyer</div>
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <input class="form-control" name="po_buyer" id="po_buyer" autocomplete="off"/>
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default btn-icon" id="clear_line_po"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                </div>
              </div>
              
              <div class="clearfix"></div>
              </div>
            </div>
          </div>
        <div class="box-title">Pilih Jenis Laporan</div>
          <div class="box-body">
            <div class="form-group">
              <!-- <div class="col-md-5"> -->
                <select class="form-control select2" id="laporan" name="laporan">
                  <option value="">-- Pilih --</option>
                  <option value="0">Report Summary Detail</option>
                  <option value="1">Report Summary PO</option>
                  <option value="2">Report Summary Scanned</option>
                </select>
              <!-- </div>  -->
              <br>
              <div class="text-right">
                <button id="filter_D" type="button" class="btn btn-primary"><i class="fa fa-search"></i> Filter</button>
                <button type="button" class="btn btn-primary distri-download"><i class="fa fa-file-excel-o"></i> Download</button>
              </div>          
              
              
            </div>
          </div>
        </div>   
      </div>
    </div>
  </div> 
</div>
 
    <div id="result">
            
    </div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
    
    $('#clear_line_po').click(function() {
        $('#daterange1').val('');
        $('#daterange2').val('');
        $('#po_buyer').val('');
        table.draw();
    });

    $('#tanggal').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $('#two-inputs').dateRangePicker(
        {
          
          getValue: function()
          {
            if ($('#daterange1').val() && $('#daterange2').val() )
              return $('#daterange1').val() + ' to ' + $('#daterange2').val();
            else
              return '';
          },
          setValue: function(s,s1,s2)
          {
            $('#daterange1').val(s1);
            $('#daterange2').val(s2);
            autoclose: true
          }

        });  
      
    $('#filter_D').click(function() {
        var tgl1     = $('#daterange1').val();
        var tgl2     = $('#daterange2').val();
        var po_buyer = $('#po_buyer').val();
        var laporan  = $('#laporan').val();


        // alert(tgl1 + " = " + tgl2 + " = " + po_buyer + " = " + laporan);
        if (!laporan) {
            $("#alert_warning").trigger("click", 'JENIS LAPORAN TIDAK BOLEH KOSONG');
        }else{
          var data = 'dari='+tgl1+'&sampai='+tgl2
                    +'&po='+po_buyer+'&laporan='+laporan;
          $.ajax({
            url:'filterSummaryDetail',
                    type: "POST",
                    data: data,
                    beforeSend: function () {
              $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transaparant',
                  border: 'none',
                }
              });
            },
            success: function(response){
              // console.log(response);
                    // if (response=='gagal') {
                    //   $("#alert_warning").trigger("click", 'Data Tidak Ada');
                    //   $.unblockUI();
                    // }else{
                      $('#result').html(response);
                      $.unblockUI();
                    // }

                  },
                  error:function(response){
                  if (response.status==500) {
                    $("#alert_info").trigger("click", 'Cek Input Data');
                    $.unblockUI();
                  }
              }
          });
        }

        
    });

    $('.search_header').submit(function() {
          var dari        = $('#daterange1').val();
          var sampai      = $('#daterange1').val();
          var pobuyer     = $('#po_buyer').val();
          var laporan     = $('#laporan').val();
          var url_loading = $('#loading_gif').attr('href')
      if (!laporan) {
        $("#alert_warning").trigger("click", 'JENIS LAPORAN TIDAK BOLEH KOSONG');
      }else{
        var data = new FormData(this);
        
      }
      return false;
    });
    

        $('#tanggal').change(function(event) {
          table.draw();
          if (event.keyCode == 13 && $(this).val() != '') {
              table.draw();
            }
        });

  });

  $('.distri-download').on('click', function(event) {
          var laporan  = $('#laporan').val();
          var dari  = $('#daterange1').val();
          var po  = $('#po_buyer').val();


          // console.log(laporan);
          if (!laporan) {
              $("#alert_warning").trigger("click", 'JENIS LAPORAN TIDAK BOLEH KOSONG');
          }
          else{
            
            if(!dari && !po){                  
              // event.preventDefault(); //return false. berhenti disitu
              $("#alert_warning").trigger("click", 'PO ATAU TANGGAL TIDAK BOLEH KOSONG');
            }
            else{
              
              var pobuyer = $('#po_buyer').val();
              var dari    = $('#daterange1').val();
              var sampai  = $('#daterange2').val();
              
              if(laporan==1)
              {
                // console.log("ini "+laporan);
                // event.preventDefault(); //return false. berhenti disitu
                window.open(base_url+'Report/downloadDistribution_po?dari='+dari+'&sampai='+sampai+'&pobuyer='+pobuyer,'_blank');
                // location.reload();
              }
              else if(laporan==0){
                // console.log("itu"+laporan);
                // event.preventDefault(); //return false. berhenti disitu
                window.open(base_url+'Report/downloadDistribution?dari='+dari+'&sampai='+sampai+'&pobuyer='+pobuyer,'_blank');
                // location.reload();
              }
              else if(laporan==2){
                window.open(base_url+'Report/downloadDistribution_scanned?dari='+dari+'&sampai='+sampai+'&pobuyer='+pobuyer,'_blank');
              }
            }
            
          /*if (tanggal != '' || balance!='' || pobuyer!=''||(line_from!=''&line_to!='')) {

            window.open(base_url+'Report/downloadproduction?filter='+filter+'&tanggal='+tanggal+'&balance='+balance+'&pobuyer='+pobuyer+'&line_from='+line_from+'&line_to='+line_to,'_blank');
          }else{
            $("#alert_error").trigger("click", 'Terjadi Kesalahan');
          }*/
          }


        });
</script>