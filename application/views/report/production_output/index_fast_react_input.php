<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> PRODUCTION OUTPUT</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>For Fast React</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <div class="panel-body">  
   <div class="col-md-8">
    <form action="<?php echo base_url('report/downloadfastReactInput')?>" id="react_input_form" method="POST">
      <div class="row"> 
        <h3>Download Template Untuk Upload ke Fast React </h3>
        
        <!---<h4>Menu Download ini hanya download di hari H</h4>--->
        
        <div class="form-group col-md-3 div-tanggal">
          <label for="tanggal" class="control-label">Pilih Tanggal</label>
          <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" value="" required>
        </div>
        <div class="col-md-5">
        </div>
        <br>
        <button class="btn btn-info" type="submit" id="download"><i class="clip-file-excel"></i> Download</button> 
      </div>
    </form>
</div>
</div>
</div>
<script type="text/javascript">
  $(function(){
        $('#tanggal').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });
  });
</script>