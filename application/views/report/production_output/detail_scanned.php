<style type="text/css">
 
.modal-lg {
    width: 1000px;
    /* overflow-y:auto; */
    /* overflow: auto !important;  */
}
/* #myModal_ { overflow-y: auto !important; } */
/* . { */
    /* overflow: auto !important; */
    /* overflow-x:auto;  */
/* } */


</style>
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
        
            <input type="hidden" id="poref" value="<?=$poreference;?>">
            <input type="hidden" id="po_size" value="<?=$size;?>">
            <input type="hidden" id="po_style" value="<?=$style;?>">
            <input type="hidden" id="po_article" value="<?=$article;?>">
            
            <div class="box-body">
            <!-- <div class="table-responsive"> -->
                <div style="overflow-x:auto;overflow-y:auto">
                    <table id="table-detail" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">BARCODE</th>
                                <th class="text-center">PO</th>
                                <th class="text-center">STYLE</th>
                                <th class="text-center">ARTICLE</th>
                                <th class="text-center">SIZE</th>
                                <th class="text-center">QTY</th>
                                <th class="text-center">CUT</th>
                                <th class="text-center">STIKER</th>
                                <th class="text-center">COMPONENT NAME</th>
                                <th class="text-center">SCANNED BY</th>
                                <th class="text-center">DATE</th>
                                <th class="text-center">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            <!-- </div> -->
            </div>
        </div>
    </div>
<script>
// $(document).ready(function () {

    var table =$('#table-detail').DataTable(
        {
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti"  : true,
        "searching" : false,
        // sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        "ajax":{
         "url": "detail_scanned_ajax",
         "dataType": "json",
         "type": "POST",
         "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                data.pobuyer = $('#poref').val();
                data.size    = $('#po_size').val();
                data.style   = $('#po_style').val();
                data.article = $('#po_article').val();
                // console.log(data.pobuyer);
                // console.log(data.size);
                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
              },
                   },
        "columns": [
                //   { "data" : null, 'sortable' : false},
                  { "data": "barcode_id"},
                  { "data": "poreference",'sortable' : false },
                  { "data": "style",'sortable' : false },
                  { "data": "article",'sortable' : false },
                  { "data": "size",'sortable' : false },
                  { "data": "qty",'sortable' : false },
                  { "data": "cut_num"},
                  { "data": "stiker",'sortable' : false },
                  { "data": "component_name" },
                  { "data": "operator",'sortable' : false },
                  { "data": "create_date" },
                  { "data": "status" },
              ]
    });
    // $('#table-detail_filter input').unbind();
    // $('#table-detail_filter input').bind('keyup', function(e) {
    //     if (e.keyCode == 13 || $(this).val().length == 5 ) {
    //         table.search($(this).val()).draw();
    //     }
    //     // if ($(this).val().length == 0 || $(this).val().length >= 3) {
    //     //     table.search($(this).val()).draw();
    //     // }
    // });

    // $('.tampilkan').click(function () {
    //     table.draw();
    // });
//   });
</script>