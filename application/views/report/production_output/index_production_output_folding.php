
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> PRODUCTION OUTPUT</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Production Output Folding</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-5">
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <div class="box-title">Pilih Tanggal Laporan</div> -->
					<div class="box-body">
						<div class="form-group">
							<div class=" alert alert-info fade in">
								<button data-dismiss="alert" class="close close-sm" type="button">
										<i class="icon-remove"></i>
								</button>
								<strong>INFORMASI</strong><br> Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir
							</div>
							<!-- <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required> -->
							<label>Input Tanggal Awal</label>
							<br>
							<input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off" required>
							<br>
							<label>Input Tanggal Akhir</label>
							<br>
							<input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off" required>
							<br>
							<div class="text-right">
								<div class="pull-left">
									<button type="button" class="btn btn-primary submit-download"><i class="fa fa-file-excel-o"></i> Download</button>
									<button class='btn btn-md btn-danger' id='reset_filter'><i class='fa fa-times'></i> Reset Filter</button>
								</div>
								<button class='btn btn-md btn-warning' id='pencarian'><i class='fa fa-search'></i> Search</button>
							</div>
						</div>
					</div>
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group"> 
        <div class="row"> 
          <div class="col-md-6">
            <label>FILTER</label>
            <div class="clearfix"></div> 
            <input type="radio" name="filters" id="filter_line" value="by_line"> <label for="filter_line" class="control-label">LINE</label>
            <div class="clearfix"></div>
            <input type="radio" name="filters" id="filter_pobuyer" value="by_pobuyer"> <label for="filter_pobuyer" class="control-label">PO BUYER</label>
            <div class="clearfix"></div>
            <input type="radio" name="filters" id="filter_balance" value="by_balance"> <label for="filter_balance" class="control-label">BALANCE</label>
          </div> 
        </div> 
      </div> 
  </div>
  <div class="col-md-4">
      <div class="form-group">
        <div class="row">
          <div class="col-md-6 div-pobuyer hidden">
            <label>Input PO BUYER</label>
            <input type="text" id="pobuyer" class="form-control" placeholder="Masukan Po Buyer">
          </div>
        </div>
      </div>
      <div class="form-group">
         <div class="form-group col-md-4 div-line hidden">
            <label for="line" class="control-label rangeline">Range Line :</label>
            <select data-placeholder="Line From" class="form-control line_choses" name="line_from" id="line_from">
              <option value=""></option>
              <?php
                $this->db->order_by('master_line_id', 'asc');
                $this->db->where('factory_id', $this->session->userdata('factory'));
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
            <br>
            <label for="">To</label>
            <select data-placeholder="Line To" class="form-control line_choses" name="line_to" id="line_to">
              <option value=""></option>
              <?php
                $this->db->order_by('master_line_id', 'asc');
                $this->db->where('factory_id', $this->session->userdata('factory'));
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
          </div>
      </div>
      <div class="form-group div-balance hidden">
          <div class="col-md-5">
            <select class="form-control select2" id="balance" name="balance">
              <option value="">-- Pilih --</option>
              <option value="0">All</option>
              <option value="1">Over</option>
              <option value="2">Balance</option>
              <option value="3">Out Standing</option>
              <!-- <option value="D">D</option>
              <option value="REPEAT">REPEAT</option> -->
            </select>
          </div> 
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="box-body">
    <div style="overflow-x:auto;overflow-y:auto">
       <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <th class="text-center">LINE</th>
                  <th class="text-center">DATE</th>                  
                  <th class="text-center">PO BUYER</th>              
                  <th class="text-center">ARTICLE</th>              
                  <th class="text-center">STYLE</th>              
                  <th class="text-center">SIZE</th>
                  <th class="text-center">QTY ORDER</th>
                  <th class="text-center">TOT. OUTPUT</th>
                  <th class="text-center">OUTPUT DAY</th>
                  <th class="text-center">BALANCE</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
    </div>
  </div>
  </div> 
</div>
<div class="myDIV"></div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
			$('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#tanggal2').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });
      
      $(".line_choses").chosen({width: "100%"});

      $('[name=filters]').change(function(event) {
        var filter = $(this).val();
        
        if (filter == 'by_line') {
          $('.div-line').removeClass('hidden');
          $('.div-pobuyer').addClass('hidden');
          $('.div-balance').addClass('hidden');
        }else if (filter == 'by_pobuyer') {
          $('.div-pobuyer').removeClass('hidden');
          $('.div-line').addClass('hidden');
          $('.div-balance').addClass('hidden');
        }else if (filter == 'by_balance') {
          $('.div-balance').removeClass('hidden');
          $('.div-line').addClass('hidden');
          $('.div-pobuyer').addClass('hidden');
        }

      });
      // function reset() {
      //   $('#balance').val('');
      //   $('#pobuyer').val('');
      //   $('#tanggal').val('');
      //   $('#line_from').val('');
      //   $('#line_to').val('');
      // }
			
      function reset() {
        $('.select2').val('');
        $('#pobuyer').val(' ');
        $('#line_from').val('');
        $('#line_to').val('');
      }
      var url_loading = $('#loading_gif').attr('href');
      var table = $('#table-result').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "production_output_folding_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                        data.dari 		 = $('#tanggal1').val();
                        data.sampai 	 = $('#tanggal2').val();
                        data.line_from = $('#line_from').val();
                        data.line_to   = $('#line_to').val();
                        data.pobuyer   = $('#pobuyer').val();
                        data.balance   = $('#balance').val();
                        data.filter    = $("input[name=filters]:checked").val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
                  { "data": "line" },
                  { "data": "date" },                 
                  { "data": "poreference" },
                  { "data": "article",'sortable' : false },
                  { "data": "style",'sortable' : false },
                  { "data": "size",'sortable' : false },
                  { "data": "order",'sortable' : false },
                  { "data": "total_folding",'sortable' : false },
                  { "data": "daily_output",'sortable' : false },
                  { "data": "balance_folding",'sortable' : false },
               ],
               
        });

        // $('#tanggal').change(function(event) {
        //   table.draw();
        //   if (event.keyCode == 13 && $(this).val() != '') {
        //       table.draw();
        //     }
        // });
        // $('#line_to').change(function(event) {
        //   var line_from = $('#line_from').val();
        //   if ($(this).val() < line_from) {
        //       $("#alert_error").trigger("click", 'Terjadi Kesalahan Range Line');
        //     }else{
        //       table.draw();
        //     }
        // });
       
        // $('#pobuyer').on('keyup', function(event) {
        //   event.preventDefault();
        //   if (event.keyCode == 13) {
        //     table.draw();
        //   }
        // });

        // $('#balance').change(function(event) {
        //   if ($(this).val() != '') {
        //     table.draw();
        //   }
        // });
        
        $('.submit-download').on('click', function(event) {
          event.preventDefault();
          var balance   = $('#balance').val();
          var pobuyer   = $('#pobuyer').val();
          var dari      = $('#tanggal1').val();
          var sampai    = $('#tanggal2').val();
          var line_from = $('#line_from').val();
          var line_to   = $('#line_to').val();
          var filter    = $("input[name=filters]:checked").val();

          if (dari != '' || sampai != '' || balance!='' || pobuyer!=''||(line_from!=''&line_to!='')) {

            window.open(base_url+'Report/downloadfolding?filter='+filter+'&dari='+dari+'&sampai='+sampai+'&balance='+balance+'&pobuyer='+pobuyer+'&line_from='+line_from+'&line_to='+line_to,'_blank');
          }else{
            $("#alert_error").trigger("click", 'Terjadi Kesalahan');
          }

        });

				$('#reset_filter').click(function() {
          $('#tanggal1').val("");
          $('#tanggal2').val("");
          // $('#line_from').val("");
          // $('#line_to').val("");
          // $('#balance').val("");
          $('#pobuyer').val("");
          // $("input[name=filters]:checked").val("");

          $('.div-pobuyer').addClass('hidden');
          $('.div-balance').addClass('hidden');
          $('.div-line').addClass('hidden');

          $('#line_from').prop('selectedIndex',0);
          $('#line_to').prop('selectedIndex',0);
          $('.select2').val('');
          // $('.line_choses').val('');

          $('input[name=filters]').prop('checked', false);
        });

        $('#pencarian').click(function() {
          var dari      = $('#tanggal1').val();
          var sampai    = $('#tanggal2').val();
          var line_from = $('#line_from').val();
          var line_to   = $('#line_to').val();
          var balance   = $('#balance').val();
          var pobuyer   = $('#pobuyer').val();
          var filter    = $("input[name=filters]:checked").val();

					console.log(pobuyer);
          if(filter == 'by_line'){
            if(dari>sampai){
              $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
            }
            else{
              if(line_from > line_to){
                $("#alert_warning").trigger("click", 'CEK LINE AWAL DAN LINE AKHIR');
              }
              else{
                if(line_from=='' || line_to==''){
                  $("#alert_warning").trigger("click", 'CEK LINE KOSONG');
                }
                else{
                  table.draw();
                }
              }
            }
          }else if(filter == 'by_pobuyer'){
            if(dari>sampai){
              $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
            }else{
              if(pobuyer == ''){
                $("#alert_warning").trigger("click", 'CEK PO KOSONG');
              }else{
                table.draw();
              }
            }
          }else if(filter == 'by_balance'){
            if(dari>sampai){
              $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
            }else{
              if(balance == ''){
                $("#alert_warning").trigger("click", 'CEK BALANCE KOSONG');
              }else{
                table.draw();
              }
            }
          }else{
						if(!dari || !sampai){
							$("#alert_warning").trigger("click", 'INPUT TANGGAL TERLEBIH DAHULU');
						}
						else{
							if(dari>sampai){
								$("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
							}
							else{
								table.draw();
							}
						}
          } 
        });

  });
</script>

