<div class="panel panel-default">
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header"><i class="fa fa-database"> REPORT PENGAJUAN AD</i></h3>
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url('home'); ?>">Report</a></li>
				<li><i class="fa fa-bars"></i>Report Pengajuan AD</li>
			</ol>
		</div>
	</div>
</div>
<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="panel-group" id="accordion">
					<label>Input Tanggal Awal</label>
					<br>
					<input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off">
					<br>
					<label>Input Tanggal Akhir</label>
					<br>
					<input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off">
					<br>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" id='collapse1' href="#collapseOne">Pencarian Berdasarkan Line</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse">
							<div class="panel-body">
								<input class="form-control" type="text" id="line_id">
							</div>
						</div>
					</div>
					<br>
					<button id='submit-download' href='javascript:void(0)' class='btn btn-md btn-primary'><i class='fa fa-file-text'></i> Download Report</button>
					<button class='btn btn-md btn-danger' id='clear_line'><i class='fa fa-cross'></i> Clear</button>
					<div class="pull-right">
						<button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button>
					</div>
				</div>
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default border-grey">
	<div class="panel-body">

		<!-- <input type="hidden" id="dari" value="<?= $dari; ?>">
				<input type="hidden" id="sampai" value="<?= $sampai; ?>">
				<input type="hidden" id="line_id" value="<?= $line_id; ?>"> -->
		<div class="clearfix"></div>
		<div class="box-body">
			<div class="clearfix"></div>
			<div class="table-responsive"><br>
				<table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
					<thead>
						<th class="text-center">TGL PERMINTAAN</th>
						<th class="text-center">JAM PERMINTAAN</th>
						<th class="text-center">STYLE</th>
						<th class="text-center">PO</th>
						<th class="text-center">NAMA QC</th>
						<th class="text-center">NAMA LINE</th>
						<th class="text-center">STATUS</th>
						<th class="text-center">ACTION</th>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">
	var url_loading = $('#loading_gif').attr('href');
	var base_url = "<?php echo base_url(); ?>";
	$(function() {
		$('#tanggal1').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true
		});
		
		$('#tanggal2').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true
		});

		$('#clear_line').click(function() {
			$('#tanggal1').val(null);
			$('#tanggal2').val(null);
			$('#line_id').val(null);
			table.draw();
		});

		$('#pencarian').click(function() {
			var dari   = $('#tanggal1').val();
			var sampai = $('#tanggal2').val();
			var line   = $('#line_id').val();

			if(!line){
				if(dari=='' || sampai == ''){
					$("#alert_info").trigger("click", 'Cek Input Tanggal');
				}
			}
			else{
				$('#table-result').DataTable().ajax.reload();
			}
			
		});

		$('#collapse1').click(function(event) {
			$('#line_id').val('');
		});

		$('#submit-download').on('click', function(event) {
			var dari    = $('#tanggal1').val();
			var sampai  = $('#tanggal2').val();
			var line_id = $('#line_id').val();

			window.open(base_url + 'Report/download_report_pengajuan_ad?dari=' + dari + '&sampai=' + sampai + '&line_id=' + line_id);
		});

    var table = $('#table-result').DataTable(
    {
			"processing": true,
			"serverSide": true,
			"orderMulti"  : true,
			sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
			"ajax":{
					"url"       : "report_pengajuan_ad_ajax",
					"dataType"  : "json",
					"type"      : "POST",
					fixedColumns: true,
					"data":function(data) {
									data.dari         = $('#tanggal1').val();
									data.sampai       = $('#tanggal2').val();
									data.line_id      = $('#line_id').val();
									data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
									},
			},
			"columns": [
					{ "data": "tanggal_permintaan" },
					{ "data": "jam_permintaan", "sortable":false },
					{ "data": "style" },
					{ "data": "poreference" },
					{ "data": "name" },
					{ "data": "line_name", "sortable":false },
					{ "data": "status" },
					{ "data": "action", "sortable":false },
			] 
    });
		
		$('#table-result_filter input').unbind();
		$('#table-result_filter input').bind('keyup', function(e) {
			if (e.keyCode == 13 || $(this).val().length == 0) {
				table.search($(this).val()).draw();
			}
			// if ($(this).val().length == 0 || $(this).val().length >= 3) {
			//     table.search($(this).val()).draw();
			// }
		});
	});

	function siap_action(pengajuan_id) {
        swal({
            title     : "PERHATIAN!!!",
            text      : "Apakah anda yakin melakukan aksi berikut?",
            icon      : "warning",
            buttons   : true,
            dangerMode: true,
        })
        .then((wilsave) => {
            if (wilsave) {
                var data = 'id='+pengajuan_id;
                $.ajax({
                    url        : 'pengajuan_ad_siap',
                    data       : data,
                    type       : "POST",
                    beforeSend : function () {				
                        $.blockUI({
                            message: "<img src='" + url_loading + "' />",
                            css: {
                                backgroundColor: 'transaparant',
                                border: 'none',
                            }
                        });
                    },
                    success: function(response){
                        var result = JSON.parse(response);
                        if (result.status==200) {
                            // $('#myModal').modal('hide');
                            $("#alert_success").trigger("click", 'AD telah siap');
                            $('#table-result').DataTable().ajax.reload();
                            // location.href = location.href;
                        }else{
                            $("#alert_error").trigger("click", 'Proses Gagal');
                    				$('#table-result').DataTable().ajax.reload();
                        }
                    }
                    ,error:function(data){
                        if (data.status==500) {
                            $.unblockUI();
                            $("#alert_warning").trigger("click", 'Proses Gagal');
                            $('#table-result').DataTable().ajax.reload();
                        }
                    }
                });   
            }
            $.unblockUI(); //buat menutup modal
        });   
        return false;
    }	
</script>
