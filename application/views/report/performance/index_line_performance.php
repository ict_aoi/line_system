
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> WFT</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>WFT</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-5">
    <div class="box box-success">
      <div class="box-header with-border">
        <div class="box-title">Pilih Tanggal Laporan</div>
          <div class="box-body">
            <div class="form-group">
              <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required>
              <div class="clearfix"></div><br>
              <div class="text-right">
                <button type="button" class="btn btn-primary submit-downloadstyle"><i class="fa fa-file-excel-o"></i> Download Style</button>
                <button type="button" class="btn btn-warning submit-download"><i class="fa fa-file-excel-o"></i> Download</button>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>

  
  <div class="clearfix"></div>
  <div class="box-body">
    <div style="overflow-x:auto;overflow-y:auto">
       <table id="table-performance" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <th class="text-center" width="150px">DATE</th>
                  <th class="text-center"width="100px">LINE</th>
                  <th class="text-center"width="100px">JENIS</th>
                  <th class="text-center" colspan="9" width="100px">JAM</th>
                  <th class="text-center"width="80px">TOTAL</th>
              </tr>
              <tr>
                <th></th>
                <th></th>
                <th></th>
                <th width="50px" class="text-center">8</th>
                <th width="50px" class="text-center">9</th>
                <th width="50px" class="text-center">10</th>
                <th width="50px" class="text-center">11</th>
                <th width="50px" class="text-center">12</th>
                <th width="50px" class="text-center">13</th>
                <th width="50px" class="text-center">14</th>
                <th width="50px" class="text-center">15</th>
                <th width="50px" class="text-center">16</th>
                <th></th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
    </div>
  </div>
  </div> 
</div>
<div class="myDIV"></div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });
      
      var url_loading = $('#loading_gif').attr('href');
      var table = $('#table-performance').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "listfwft_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                        data.tanggal   = $('#tanggal').val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
                  { "data": "date" },
                  { "data": "line" },
                  { "data": "total_good",'sortable' : false },
                  { "data": "defectinline",'sortable' : false },
                  { "data": "defectendline",'sortable' : false },
                  { "data": "wft",'sortable' : false },
               ],
               
        });

        $('#tanggal').change(function(event) {
          table.draw();
          if (event.keyCode == 13 && $(this).val() != '') {
              table.draw();
            }
        });
        
        
        $('.submit-downloadstyle').on('click', function(event) {
          event.preventDefault();
          
          var tanggal   = $('#tanggal').val();
          
          window.open(base_url+'Report/downloadwftstyle?tanggal='+tanggal,'_blank');

        });

        $('.submit-download').on('click', function(event) {
          event.preventDefault();
          
          var tanggal   = $('#tanggal').val();
          
          window.open(base_url+'Report/downloadwft?tanggal='+tanggal,'_blank');

        });
      
  });
</script>