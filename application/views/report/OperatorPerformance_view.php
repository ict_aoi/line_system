<script src="<?=base_url('assets/plugins/datepickerrange/moment.min.js');?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/datepickerrange/daterangepicker.min.css');?>" />
<script src="<?=base_url('assets/plugins/datepickerrange/jquery.daterangepicker.min.js');?>"></script>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Operator Performance</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-table"></i>Operator Performance</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <!-- <form id="form_download"> -->
  <div class="panel-body">
    <div class="row col-md-12">
      <div class="panel-footer">
      <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_date" value="by_date" checked="checked"> <label for="filter_date" class="control-label">Per Tanggal</label>
      </div>
      <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_nik" value="by_nik"> <label for="filter_nik" class="control-label">Per NIK</label>
      </div>
      <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_grade" value="by_grade"> <label for="filter_grade" class="control-label">Per Grade</label>
      </div>
      <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_line" value="by_line"> <label for="filter_line" class="control-label">LINE</label>
      </div>
      <div class="form-group col-md-3 div-tanggal">
        <div class="box-header with-border">
          <div class="box-title">Pilih Tanggal Laporan</div>
            <div class="box-body">
              <div class="form-group">
                <div id="two-inputs"> 
                  <div class="input-group">
                    <label>Dari</label> 
                    <input id="daterange1" class="form-control" name="dari" placeholder="Tanggal Awal" required>
                      
                    <label>Sampai</label> 
                    <input id="daterange2" class="form-control" name="sampai" placeholder="Tanggal Akhir" required>                
                  </div> 
                </div>
                <!-- <br> -->
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
      </div>
      <div class="form-group col-md-3 div-nik hidden">
        <label for="nik_sewer" class="control-label">Nik Sewer :</label>
        <input class="form-control" name="nik_sewer" id="nik_sewer" autocomplete="off" value="">
      </div>
      <div class="clearfix"></div>
      <div class="form-group div-grade hidden">
            <div class="col-md-3">
              <select class="form-control select2" id="inputGrade" name="grade">
                <option value="">-- Pilih Grade --</option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <!-- <option value="D">D</option>
                <option value="REPEAT">REPEAT</option> -->
              </select>
            </div>
      </div>
      <!-- <div class="form-group"> -->
         <div class="form-group col-md-4 div-line hidden">
            <label for="line" class="control-label rangeline">From :</label>
            <select data-placeholder="Line From" class="form-control line_choses" name="line_from" id="line_from">
              <option value=""></option>
              <?php
                $this->db->order_by('master_line_id', 'asc');
                $this->db->where('factory_id', $this->session->userdata('factory'));
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
            <br>
            <label for="">TO</label>
            <select data-placeholder="Line To" class="form-control line_choses" name="line_to" id="line_to">
              <option value=""></option>
              <?php
                $this->db->order_by('master_line_id', 'asc');
                $this->db->where('factory_id', $this->session->userdata('factory'));
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
          </div>
      <!-- </div> -->
      <div class="clearfix"></div>
      <div class="row">
        <div class="form-group col-md-3">
            <button type="button" class="btn btn-primary submit-tampilkan"><i class="fa fa-search"></i> Tampilkan </button>
            <button type="button" class="btn btn-success submit-download"><i class="fa fa-file-excel-o"></i> Download </button>
        </div>
        <div class="form-group col-md-1">
            
        </div>
      </div>
     
    </div>
    
    </div>
<!-- </form> -->

    <div class="col-md-5">
    <div class="box box-success">
      <div class="box-header with-border">


      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="box-body">

    <table id="table-inspect" class="table table-full-width dataTable" cellspacing="0" width="100%">
        <thead style="background-color: #f0f0f0;">
          <tr>
              <th class="" width="">Tanggal</th>
              <th class="" width="">Line</th>
              <th class="" width="">Name Operator</th>
              <th class="" width="">Name Proses</th>
              <th class="">Total Score</th>
              <th class="">Point Average</th>
              <th class="">Grade</th>
              <th class="">Jenis Defect</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
  </div>

  </div>
</div>
<div class="myDIV"></div>
<script type="text/javascript">
var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });
      $('#tanggal').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#two-inputs').dateRangePicker(
          {
            
            getValue: function()
            {
              if ($('#daterange1').val() && $('#daterange2').val() )
                return $('#daterange1').val() + ' to ' + $('#daterange2').val();
              else
                return '';
            },
            setValue: function(s,s1,s2)
            {
              $('#daterange1').val(s1);
              $('#daterange2').val(s2);
              autoclose: true
            }

          });  
      $(".line_choses").chosen({width: "100%"});

      $('[name=filters]').change(function(event) {
        var filter = $(this).val();
        reset();
        if (filter == 'by_date') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-nik').addClass('hidden');
          $('.div-grade').addClass('hidden');
          $('.div-line').addClass('hidden');
        }else if (filter == 'by_nik') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-nik').removeClass('hidden');
          $('.div-grade').addClass('hidden');
          $('.div-line').addClass('hidden');
        }else if (filter == 'by_grade') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-nik').addClass('hidden');
          $('.div-grade').removeClass('hidden');
          $('.div-line').addClass('hidden');
        }else if (filter == 'by_line') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-line').removeClass('hidden');
          $('.div-nik').addClass('hidden');
          $('.div-grade').addClass('hidden');
        }


      });

      function reset() {
        $('#inputGrade').val('');
        $('#nik_sewer').val('');
        $('#line_from').val('');
        $('#line_to').val('');
        // $('#tanggal').val('');

        $('#table-inspect > tbody').empty();
      }

      var url_loading = $('#loading_gif').attr('href');
      var filters = $("input[name=filters]:checked").val();
      var tanggal = $("#tanggal").val();
      //
      var table =$('#table-inspect').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "list_inspection_operator",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
             "scrollY": 200,
              "scrollX": true,
              "data":function(data) {
                  data.datefrom  = $('#daterange1').val();
                  data.dateto    = $('#daterange2').val();
                  data.grade     = $('#inputGrade').val();
                  data.nik_sewer = $('#nik_sewer').val();
                  data.line_from = $('#line_from').val();
                  data.line_to   = $('#line_to').val();
                  data.filter    = $("input[name=filters]:checked").val()
                  data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },

                               },
              "columns": [
                  { "data": "date",'sortable' : false  },
                  { "data": "line_name" },
                  { "data": "sewer_nik",'sortable' : false  },
                  { "data": "proses_name",'sortable' : false  },
                  { "data": "total_score",'sortable' : false  },
                  { "data": "point_avg",'sortable' : false  },
                  { "data": "grade",'sortable' : false  },
                  { "data": "jenisDefect",'sortable' : false  },
               ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull )
                  {

                  },

        });

      $('#tanggal').change(function(event) {
        if (filters == 'by_date') {
          table.draw();
          if (event.keyCode == 13 && $(this).val() != '') {
            table.draw();
          }
        }else if (filters == 'by_nik') {
          if ($('#nik_sewer').val() != '') {
            table.draw();
          }
        }else if (filters == 'by_grade') {
          if ($('#inputGrade').val() != '') {
            table.draw();
          }
        }
      });

      $('#nik_sewer').on('keyup', function(event) {
        event.preventDefault();
        if (event.keyCode == 13) {
          table.draw();
        }
      });

      $('#inputGrade').change(function(event) {
        if ($(this).val() != '') {
          table.draw();
        }
      });

      $('#line_to').change(function(event) {
          var line_from = $('#line_from').val();
          if ($(this).val() < line_from) {
              $("#alert_error").trigger("click", 'Terjadi Kesalahan Range Line');
            }else{
              table.draw();
            }
        });

      $('.submit-tampilkan').on('click',function () {
        if (filters == 'by_line') {
          var line_from = $('#line_from').val();
          var line_to = $('#line_to').val();
          if (line_to < line_from) {
            $("#alert_error").trigger("click", 'Terjadi Kesalahan Range Line');
          }
        }
        table.draw();
      });
      $('.submit-download').on('click', function(event) {
        event.preventDefault();
        var grade     = $('#inputGrade').val();
        var nik_sewer = $('#nik_sewer').val();
        var datefrom  = $('#daterange1').val();
        var dateto    = $('#daterange2').val();
        var line_from = $('#line_from').val();
        var line_to   = $('#line_to').val();
        var filter    = $("input[name=filters]:checked").val();

        if (datefrom != '' || nik_sewer!='' || grade!=''||(line_from!=''&line_to!='')) {

          window.open(base_url+'Report/download_operator_performance?filter='+filter+'&datefrom='+datefrom+'&dateto='+dateto+'&nik_sewer='+nik_sewer+'&grade='+grade+'&line_from='+line_from+'&line_to='+line_to,'_blank');
        }else{
          $("#alert_error").trigger("click", 'Terjadi Kesalahan');
        }

      });

  });
</script>