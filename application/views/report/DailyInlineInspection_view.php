
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Daily Inspection</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Daily Inspection</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <!-- <form id="form_download"> -->
  <div class="panel-body">
    <div class="row col-md-9">
      <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_date" value="by_date" checked="checked"> <label for="filter_date" class="control-label">Per Tanggal</label>
      </div>
      <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_nik" value="by_nik"> <label for="filter_nik" class="control-label">Per NIK</label>
      </div>
      <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_grade" value="by_grade"> <label for="filter_grade" class="control-label">Per Grade</label>
      </div>
      <!-- <div class="form-group col-md-3">
          <input type="radio" name="filters" id="filter_line" value="by_line"> <label for="filter_line" class="control-label">Per Line</label>
      </div> -->
      <div class="form-group col-md-3 div-tanggal">
        <label for="tanggal" class="control-label">Pilih Tanggal</label>
          <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" value="">
      </div>
      <div class="form-group col-md-3 div-nik hidden">
        <label for="nik_sewer" class="control-label">Nik Sewer :</label>
        <input class="form-control" name="nik_sewer" id="nik_sewer" autocomplete="off" value="">
      </div>
      <!-- <div class="form-group col-md-3 div-line hidden">
        <label for="line_id" class="control-label">Line ID :</label>
        <input class="form-control" name="line_id" id="line_id" autocomplete="off" value="">
      </div> -->
      <!-- <div class="form-group div-grade hidden">
            <div class="col-md-3">
              <select class="form-control select2" id="inputRound" name="round">
                <option value="">-- Pilih Round --</option>
                <option value="1">Round 1</option>
                <option value="2">Round 2</option>
                <option value="3">Round 3</option>
                <option value="4">Round 4</option>
                <option value="5">Round 5</option>
              </select>
            </div>
      </div> -->

    </div>
    <hr class="col-md-12">
    <div class="row col-md-12">
      <div class="form-group div-color hidden">
            <div class="col-md-3">
              <select class="form-control select2" id="inputColor" name="color">
                <option value="">-- Pilih Color --</option>
                <option value="green">Green</option>
                <option value="yellow">Yellow</option>
                <option value="red">Red</option>
              </select>
            </div>
      </div>

    </div>
    <div class="panel-footer text-right">
      <button type="button" class="btn btn-success submit-download"><i class="fa fa-file-excel-o"></i> Download </button>
    </div>
<!-- </form> -->

    <div class="col-md-5">
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <div class="form-group hidden" id="filter_by_date">
          <div class="box-title">Pilih Tanggal Laporan</div>
            <form action="<?=base_url('report/index_inspection');?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" value="<?php echo $tanggal; ?>" required>
                  <div class="clearfix"></div><br>
                  <span class="input-group-btn">
                      <button class="btn btn-success col-md-3"><i class="fa fa-search"></i> Cari </button>
                      <?php
                          if ($tanggal <> '')
                          {
                              ?>
                              <span class="clearfix"></span><br>
                              <a href="<?=base_url('Report/dailydownload?tanggal='.$tanggal) ?>" class="btn btn-primary col-md-3" ><i class="fa fa-file-excel-o"> Download</i></a>
                              <?php
                          }
                      ?>
                  </span>
                </div>
                <div class="form-group">

                </div>
              </div>
            </form>
        </div> -->

      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="box-body">

    <table id="table-inspect" class="table table-full-width dataTable" cellspacing="0" width="100%">
        <thead style="background-color: #f0f0f0;">
          <tr>
              <th class="text-center" width="">Nama Line</th>
              <th class="text-center" width="">Process</th>
              <th class="text-center">NIK</th>
              <th class="text-center">Operator Name</th>
              <th class="text-center" colspan="5">Defect Code</th>
          </tr>
          <tr>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th class="text-center" width="">R1</th>
              <th class="text-center" width="">R2</th>
              <th class="text-center" width="">R3</th>
              <th class="text-center" width="">R4</th>
              <th class="text-center" width="">R5</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
  </div>
  </div>
</div>
<div class="myDIV"></div>
<script type="text/javascript">
var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('[name=filters]').change(function(event) {
        var filter = $(this).val();
        reset();
        if (filter == 'by_date') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-nik').addClass('hidden');
          $('.div-grade').addClass('hidden');
          $('.div-color').addClass('hidden');
          // $('.div-line').addClass('hidden');
        }else if (filter == 'by_nik') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-nik').removeClass('hidden');
          // $('.div-line').addClass('hidden');
          // $('.div-grade').addClass('hidden');
          $('.div-color').addClass('hidden');
        }else if (filter == 'by_grade') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-nik').addClass('hidden');
          // $('.div-line').addClass('hidden');
          // $('.div-grade').removeClass('hidden');
          $('.div-color').removeClass('hidden');
        }

      });

      /*$('#inputRound').change(function(event) {
        if ($(this).val() != '') {
          $('.div-color').removeClass('hidden');
        }else{
          $('.div-color').addClass('hidden');
        }
      });*/

      function reset() {
        $('#inputColor').val('');
        $('#inputRound').val('');
        $('#nik_sewer').val('');
        $('#table-inspect > tbody').empty();
      }

      var url_loading = $('#loading_gif').attr('href');
      //
      var table =$('#table-inspect').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "list_inspection",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
             "scrollY": 200,
              "scrollX": true,
              "data":function(data) {
                  data.tanggal = $('#tanggal').val();
                  data.nik_sewer = $('#nik_sewer').val();
                  data.round = $('#inputRound').val();
                  data.color = $('#inputColor').val();
                  data.filter = $("input[name=filters]:checked").val()
                  data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
              // "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
              //    'tanggal':$('#tanggal').val(),
              //     }
                               },
              "columns": [
                  { "data": "line_name" },
                  { "data": "proses_name" },
                  { "data": "sewer_nik" },
                  { "data": "sewer_name" },
                  { "data": "round1",'sortable' : false },
                  { "data": "round2",'sortable' : false },
                  { "data": "round3",'sortable' : false },
                  { "data": "round4",'sortable' : false },
                  { "data": "round5",'sortable' : false }
               ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull )
                  {

                  },

        });
        $('#table-inspect_filter input').unbind();
        $('#table-inspect_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-inspect').DataTable().ajax.reload();
        });

      $('#tanggal').change(function(event) {
        table.draw();
        if (event.keyCode == 13 && $(this).val() != '') {
          table.draw();
        }
      });

      $('#nik_sewer').on('keyup', function(event) {
        event.preventDefault();
        if (event.keyCode == 13) {
          table.draw();
        }
      });

      $('#inputColor').change(function(event) {
        if ($(this).val() != '') {
          table.draw();
        }
      });

      $('.submit-download').on('click', function(event) {
        event.preventDefault();
        var color = $('#inputColor').val();
        var round = $('#inputRound').val();
        var nik_sewer = $('#nik_sewer').val();
        var tanggal = $('#tanggal').val();
        var filter = $("input[name=filters]:checked").val();

        if (tanggal != '' || nik_sewer!='' || round!='' || color != '') {

          window.open(base_url+'Report/download_line_performance?filter='+filter+'&tanggal='+tanggal+'&nik_sewer='+nik_sewer+'&round='+round+'&color='+color,'_blank');
        }else{
          $("#alert_error").trigger("click", 'Terjadi Kesalahan');
        }

      });

  });
</script>