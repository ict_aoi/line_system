
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Measurement</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Measurement Endline</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <div class="panel-body">
      <div class="panel-body">
        <div class="row col-md-4">
        
          <div class="form-group">
            <div class=" alert alert-danger fade in">
              <button data-dismiss="alert" class="close close-sm" type="button">
                  <i class="icon-remove"></i>
              </button>
              <strong>INFORMASI</strong><br> Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir
            </div>
            <!-- <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required> -->
            <label>Input Tanggal Awal</label>
            <br>
            <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off" required>
            <br>
            <label>Input Tanggal Akhir</label>
            <br>
            <input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off" required>
            <br>
          </div>
        </div>
        <div class="row col-md-8">
          <div class="form-group col-md-3">
              <input type="radio" name="filters" id="filter_date" value="by_date" checked="checked"> <label for="filter_date" class="control-label">Tanggal</label>
          </div>
          <div class="form-group col-md-3">
              <input type="radio" name="filters" id="filter_line" value="by_line"> <label for="filter_line" class="control-label">Range Line</label>
          </div>
          <!-- <div class="form-group col-md-3 div-tanggal">
            <label for="tanggal" class="control-label">Pilih Tanggal</label>
              <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" value="<?php echo $tanggal; ?>">
          </div> -->
          <div class="form-group col-md-3 div-line hidden">
            <label for="line" class="control-label">Range Line :</label>
            <select data-placeholder="line from" class="form-control line_choses" name="line_from" id="line_from">
              <option value=""></option>
              <?php
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
            <select data-placeholder="line to" class="form-control line_choses" name="line_to" id="line_to">
              <option value=""></option>
              <?php
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
           <!--  <input class="form-control" name="line_from" id="line_from" autocomplete="off" value="" placeholder="from">
            <input class="form-control" name="line_to" id="line_to" autocomplete="off" value="" placeholder="to"> -->
          </div>

        </div>
        <hr class="col-md-12">
        <div class="panel-footer text-right">
          <div class="pull-left">
            <button class='btn btn-md btn-warning' id='pencarian'><i class='fa fa-search'></i> Search</button>
          </div>
          <button type="button" class="btn btn-success submit-download"><i class="fa fa-file-excel-o"></i> Download </button>
        </div>
       <div class="box box-success">
       </div>
  </div>
  <div class="clearfix"></div>
  <div class="box-body">

    <table id="table-inspect" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
        <thead>
          <tr>
              <th class="">DATE</th>
              <th class="">LINE</th>
              <th class="">STYLE</th>
              <th class="text-center" colspan="5">WFT</th>
          </tr>
          <tr>
              <th></th>
              <th></th>
              <th></th>
              <th class="text-center" width="40px">I</th>
              <th class="text-center" width="40px">II</th>
              <th class="text-center" width="40px">III</th>
              <th class="text-center" width="40px">IV</th>
              <th class="text-center" width="40px">V</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
  </div>
  </div>
</div>
<div class="myDIV"></div>
<script type="text/javascript">
var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#tanggal2').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('[name=filters]').change(function(event) {
        var filter = $(this).val();
        reset();
        if (filter == 'by_date') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-line').addClass('hidden');
        }else if (filter == 'by_line') {
          // $("#alert_error").trigger("click", 'Terjadi Kesalahan');
          $('.div-tanggal').removeClass('hidden');
          $('.div-line').removeClass('hidden');
        }

      });


      function reset() {
        // $('#line_from').val('');
        // $('#line_to').val('');
        // $('#tanggal').val('');

        $('#table-inspect > tbody').empty();
      }

      var url_loading = $('#loading_gif').attr('href');

      var table =$('#table-inspect').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "list_measurementendline_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
             "scrollY": 200,
             "scrollX": true,
                    "data":function(data) {
                        data.dari = $('#tanggal1').val();
                        data.sampai = $('#tanggal2').val();
                        data.line_from = $('#line_from').val();
                        data.line_to = $('#line_to').val();
                        data.filter = $("input[name=filters]:checked").val()
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columnDefs": [
          { className: 'text-right', targets: [3, 4, 5, 6, 7] },
        ],
        "columns": [
                  { "data": "create_date" },
                  { "data": "line_name" },
                  { "data": "style",'sortable' : false },
                  { "data": "i",'sortable' : false },
                  { "data": "ii",'sortable' : false },
                  { "data": "iii",'sortable' : false },
                  { "data": "iv",'sortable' : false },
                  { "data": "v",'sortable' : false }
               ],

        });

      // $('#tanggal').change(function(event) {
      //   if ($("input[name=filters]:checked").val() != 'by_line') {
      //     table.draw();
      //     if (event.keyCode == 13 && $(this).val() != '') {
      //       table.draw();
      //     }
      //   }else{
      //     $('#line_to').trigger('change');
      //   }

      // });

      $(".line_choses").chosen({width: "100%"});

      // $('#line_to').keyup(function(event) {
      //   var line_from = $('#line_from').val();
      //   if (event.keyCode == 13) {
      //       if ($(this).val() < line_from) {
      //         $("#alert_error").trigger("click", 'Terjadi Kesalahan Range Line');
      //       }else{
      //         table.draw();
      //       }
      //   }
      // });

      // $('#line_to').change(function(event) {
      //   var line_from = $('#line_from').val();
      //   if ($(this).val() < line_from) {
      //       $("#alert_error").trigger("click", 'Terjadi Kesalahan Range Line');
      //     }else{
      //       table.draw();
      //     }
      // });

      $('.submit-download').on('click', function(event) {
        event.preventDefault();
        var line_from = $('#line_from').val();
        var line_to   = $('#line_to').val();
        var dari      = $('#tanggal1').val();
        var sampai    = $('#tanggal2').val();
        var filter    = $("input[name=filters]:checked").val();

        if (dari != '' || sampai != '' || (line_from!='' && line_to!='') || filter != '') {
          window.open(base_url+'Report/download_measurement_endline?filter='+filter+'&dari='+dari+'&sampai='+sampai+'&line_from='+line_from+'&line_to='+line_to,'_blank');
        }else{
          $("#alert_error").trigger("click", 'Terjadi Kesalahan');
        }

      });

      $('#pencarian').click(function() {
        var dari   = $('#tanggal1').val();
        var sampai = $('#tanggal2').val();
        
        var line_from = $('#line_from').val();
        var line_to = $('#line_to').val();

        var filter    = $("input[name=filters]:checked").val();

        if(filter == 'by_date'){
          if(dari>sampai){
            $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
          }
          else{
            table.draw();
          }
        }else if(filter == 'by_line'){
          if(dari>sampai){
            $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
          }
          else{
            if(line_from > line_to){
              $("#alert_warning").trigger("click", 'CEK LINE AWAL DAN LINE AKHIR');
            }
            else{
              if(line_from=='' || line_to==''){
                $("#alert_warning").trigger("click", 'CEK LINE KOSONG');
              }
              else{
                table.draw();
              }
            }
          }
        }
      });

  });
</script>