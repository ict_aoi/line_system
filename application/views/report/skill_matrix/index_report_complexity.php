<div class="panel panel-default">
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header"> <i class="fa fa-database"> Skill Based</i></h3>
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url('home'); ?>">Report</a></li>
				<li><i class="fa fa-bars"></i>Operation Complexity</li>
			</ol>
		</div>
	</div>
</div>
<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="col-md-5">
			<div class="box box-success">
				<div class="box-header with-border">
					<!-- <div class="box-title">Pilih Tanggal Laporan</div> -->
					<div class="box-body">
						<!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#import1">
									IMPORT DATA
							</button> -->

						<div class="form-group">
							<!-- <div class="col-md-6">

								<div class=" alert alert-info fade in">
									<button data-dismiss="alert" class="close close-sm" type="button">
										<i class="icon-remove"></i>
									</button>
									<strong>INFORMASI</strong><br> Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir
								</div>
								<label>Input Tanggal Awal</label>
								<br>
								<input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off">
								<br>
								<label>Input Tanggal Akhir</label>
								<br>
								<input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off">
							</div> -->
								<div class=" alert alert-danger fade in">
									<strong>FILTER</strong><br> <b>NIK</b> wajib diisi.
								</div>
								<label>INPUT NIK</label>
								<input class="form-control" name="nik" id="nik" autocomplete="off">
								
								<!-- <select class="form-control" id="prosesName" name="proses">
									<option value="" id="user_id">Pilih Proses</option> -->
									<?php 

									// $master = $this->db->query("SELECT * FROM master_proses order by proses_name");
									// foreach ($master->result() as $key => $master) {
									// 	echo "<option value='" . $master->master_proses_id . "' name='" . $master->proses_name . "'>" . $master->proses_name . "</option>";
									// }
									?>
								<!-- </select><br> -->

								<!-- <select class="proses_choses form-control" id="prosesName" name="proses" onchange="return addProses()">
									<option value="" id="user_id" selected="selected">Pilih Proses</option>
									<?php 
										// $master = $this->db->get('master_proses');
										foreach ($m_proses->result() as $key => $m_proses) {
										echo "<option value='".$m_proses->proses_id."' name='".$m_proses->proses_name."'>".$m_proses->proses_name."</option>";
										}
									?>
								</select><br><br>
								<label>Line</label>
								<br>
								<select class="form-control" id="lineName" name="line">
									<option value="" id="user_id">Pilih Line</option>
									<?php
									foreach ($ml->result() as $key => $ml) {
										echo "<option value='" . $ml->master_line_id . "' name='" . $ml->line_name . "'>" . $ml->line_name . "</option>";
									}
									?>
								</select><br>
								<label>Mesin</label>
								<br>
								<select class="form-control" id="mesinName" name="mesin">
									<option value="" id="user_id">Pilih Mesin</option>
									<?php
									foreach ($mesin->result() as $key => $mesin) {
										echo "<option value='" . $mesin->mesin_id . "' name='" . $mesin->nama_mesin . "'>" . $mesin->nama_mesin . "</option>";
									}
									?>
								</select> -->
							<div class="clearfix"></div><br>
							
							<button type="button" class="btn btn-warning submit-download"><i class="fa fa-file-excel-o"></i> Download</button>
							<!-- <button id='submit-download' href='javascript:void(0)' class='btn btn-md btn-primary'><i class='fa fa-file-text'></i> Download Report</button> -->
							<div class="pull-right">
								<button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="clearfix"></div>
		</div>
	</div>


	<div id="result">
	</div>

	<div class="myDIV"></div>

	<!-- Modal -->
	<div class="modal fade" id="import1" tabindex="-1" role="dialog" aria-labelledby="import1Label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="import1Label">Import Data</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
					<form method="post" action="<?php echo base_url("/Qc_matrix/upload_matrix"); ?>" enctype="multipart/form-data">
						<!-- 
            -- Buat sebuah input type file
            -- class pull-left berfungsi agar file input berada di sebelah kiri
            -->
						<input type="file" name="file">

						<!--
            -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
            -->


						<!-- <form method="post" action="<?php //echo site_url() 
																							?>Csv/importcsv" enctype="multipart/form-data">
              <input type="file" name="userfile" ><br><br>
              <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
          </form> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

					<input type="submit" class="btn btn-warning" name="import" value="Import">
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var url_loading = $('#loading_gif').attr('href');
		var base_url = "<?php echo base_url(); ?>";
		$(function() {
			
			$(".proses_choses").chosen();
			
			$('#tanggal1').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true
			});

			$('#tanggal2').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true
			});

			$('#pencarian').click(function() {
				// var tgl1   = $('#tanggal1').val();
				// var tgl2   = $('#tanggal2').val();
				// var line   = $('#lineName').val();
				// var mesin  = $('#mesinName').val();
				// var proses = $('#prosesName').find('option:selected').attr("value");

				var nik   = $('#nik').val();

				// if (!tgl1 || !tgl2) {
				// 	$("#alert_warning").trigger("click", 'TANGGAL TIDAK BOLEH KOSONG');
				// } else if (tgl1 > tgl2) {
				// 	$("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
				// } else {
				// var data = 'line=' + line + '&mesin=' + mesin + '&proses=' + proses;
				var data = 'nik=' + nik;
					$.ajax({
						url: 'pencarian_matrix',
						type: "POST",
						data: data,
						beforeSend: function() {
							// $.blockUI({
							// 	message: "<img src='" + url_loading + "' />",
							// 	css: {
							// 		backgroundColor: 'transaparant',
							// 		border: 'none',
							// 	}
							// });
						},
						success: function(response) {
							// console.log(response);
							// if (response=='gagal') {
							//   $("#alert_warning").trigger("click", 'Data Tidak Ada');
							//   $.unblockUI();
							// }else{
							$('#result').html(response);
							// $.unblockUI();
							// }

						},
						error: function(response) {
							if (response.status == 500) {
								$("#alert_info").trigger("click", 'Cek Input Data');
								// $.unblockUI();
							}
						}
					});
				// }


			});

			$('.submit-download').on('click', function(event) {
				event.preventDefault();

				// var tanggal   = $('#tanggal').val();
				// var dari   = $('#tanggal1').val();
				// var sampai = $('#tanggal2').val();
				var line   = $('#lineName').val();
				var mesin  = $('#mesinName').val();
				var proses = $('#prosesName').find('option:selected').attr("value");

				// if(dari == null || dari == null){
				// 	$("#alert_warning").trigger("click", 'DIISI SEMUA TANGGALNYA, JANGAN SATU AJA');
				// }
				// if (dari > sampai) {
				// 	$("#alert_warning").trigger("click", 'TANGGAL AWAL TIDAK BOLEH LEBIH BESAR DARI TANGGAL AKHIR');
				// } else {
					window.open(base_url + 'Qc_matrix/export_report_complexity_level?dari=' + dari + '&sampai=' + sampai + '&line=' + line+ '&mesin=' + mesin + '&proses=' + proses, '_blank');
				// }
			});

		});
	</script>
