
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> APT</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>APT</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <div class="box-title">Pilih Tanggal Laporan</div> -->
          <div class="box-body">
            <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#import1">
                IMPORT DATA
            </button> -->
            
            <div class="form-group">
              <div class=" alert alert-info fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                </button>
                <strong>INFORMASI</strong><br> Input Tanggal <b>Tidak Wajib</b><br>Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir
              </div>
							<div class="col-md-6">
							
								<!-- <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required> -->
								<label>Input Tanggal Awal</label>
								<br>
								<input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off" required>
								<br>
								<label>Input Tanggal Akhir</label>
								<br>
								<input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off" required>
								<br>

							</div>
							<div class="col-md-6">
								<label>Input Style</label>
								<select class="style_choses form-control" id="styleName" name="style" onchange="return addStyle()"> -->
									<option value="" id="user_id" selected="selected">Pilih Style</option>
									<?php 
										foreach ($m_style->result() as $key => $m_style) {
											echo "<option value='".$m_style->style."' name='".$m_style->style."'>".$m_style->style."</option>";
										}  
									?>
								</select><br><br>
								<label>Line</label>
								<br>
								<select class="line_choses form-control" id="lineName" name="line">
									<option value="" id="user_id">Pilih Line</option>
									<?php
									foreach ($ml->result() as $key => $ml) {
										echo "<option value='" . $ml->master_line_id . "' name='" . $ml->line_name . "'>" . $ml->line_name . "</option>";
									}
									?>
								</select><br>
							</div>
              <div class="clearfix"></div><br>
                <!-- <button type="button" class="btn btn-primary submit-downloadstyle"><i class="fa fa-file-excel-o"></i> Download Style</button> -->
              <!-- <div class="pull-right"> -->
                <button type="button" class="btn btn-warning submit-download col-md-12"><i class="fa fa-file-excel-o"></i> Download</button>
                <!-- <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button> -->
              <!-- </div> -->
            </div>
          </div>
      </div>
    </div>
  </div>

  
  <div class="clearfix"></div>
    <!-- <div class="box-body">
      <div style="overflow-x:auto;overflow-y:auto">
        <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="text-center" width="150px">DATE</th>
                    <th class="text-center"width="100px">LINE</th>
                    <th class="text-center"width="100px">TOTAL GOOD</th>
                    <th class="text-center" colspan="2" width="100px">TOTAL DEFECT</th>
                    <th class="text-center"width="80px">WFT </th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th width="80px" class="text-center">INLINE</th>
                  <th width="80px" class="text-center">ENDLINE</th>
                  <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
    </div> -->
  </div> 
</div>


<div id="result">    
  </div>

<div class="myDIV"></div>

<!-- Modal -->
<div class="modal fade" id="import1" tabindex="-1" role="dialog" aria-labelledby="import1Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="import1Label">Import Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
          <!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
          <form method="post" action="<?php echo base_url("/Qc_matrix/upload_matrix"); ?>" enctype="multipart/form-data">
            <!-- 
            -- Buat sebuah input type file
            -- class pull-left berfungsi agar file input berada di sebelah kiri
            -->
            <input type="file" name="file">
            
            <!--
            -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
            -->
            

          <!-- <form method="post" action="<?php //echo site_url() ?>Csv/importcsv" enctype="multipart/form-data">
              <input type="file" name="userfile" ><br><br>
              <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
          </form> -->
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
          <input type="submit" class="btn btn-warning" name="import" value="Import">
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
			$('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#tanggal2').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });
		
			$(".style_choses").chosen();

			$(".line_choses").chosen();

      $('#pencarian').click(function() {
          var tgl1     = $('#tanggal1').val();
          var tgl2     = $('#tanggal2').val();
						
					var line  = $('#lineName').find('option:selected').attr("value");
					var style = $('#styleName').find('option:selected').attr("value");

					console.log(line);
					console.log(style);
					

          // if (!tgl1||!tgl2) {
          //   $("#alert_warning").trigger("click", 'TANGGAL TIDAK BOLEH KOSONG');
          if(tgl1>tgl2){
            $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
          }else{
            var data = 'line='+line+'&style='+style+'&tgl1='+tgl1+'&tgl2='+tgl2;
            $.ajax({
              url:'pencarian_matrix',
              type: "POST",
              data: data,
              beforeSend: function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                  }
                });
              },
              success: function(response){
                // console.log(response);
                      // if (response=='gagal') {
                      //   $("#alert_warning").trigger("click", 'Data Tidak Ada');
                      //   $.unblockUI();
                      // }else{
                        $('#result').html(response);
                        $.unblockUI();
                      // }

                    },
                    error:function(response){
                    if (response.status==500) {
                      $("#alert_info").trigger("click", 'Cek Input Data');
                      $.unblockUI();
                    }
                }
            });
					}
      });

      $('.submit-download').on('click', function(event) {
        event.preventDefault();
        
        var dari    = $('#tanggal1').val();
        var sampai  = $('#tanggal2').val();
				
				var line  = $('#lineName').find('option:selected').attr("value");
				var style = $('#styleName').find('option:selected').attr("value");
        
        if (dari>sampai) {
          $("#alert_warning").trigger("click", 'TANGGAL AWAL TIDAK BOLEH LEBIH BESAR DARI TANGGAL AKHIR');
        }else{
          window.open(base_url+'Qc_matrix/export_report_skill_matrix?line='+line+'&style='+style+'&dari='+dari+'&sampai='+sampai,'_blank');
        }
      });
      
  });
</script>
