
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Line Balancing</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Line Balancing</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <div class="box-title">Pilih Tanggal Laporan</div> -->
          <div class="box-body">
            <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#import1">
                IMPORT DATA
            </button> -->
            
            <div class="form-group">
              <div class=" alert alert-info fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                </button>
                <!-- <strong>INFORMASI</strong><br> Input Tanggal <b>Tidak Wajib</b><br>Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir -->
              </div>
							<div class="col-md-6">		
						
								<label>LINE</label>
								<br>
								<select class="line_choses form-control" id="lineName" name="line">
									<option value="" id="user_id">Pilih Line</option>
									<?php
									foreach ($ml->result() as $key => $ml) {
										echo "<option value='" . $ml->master_line_id . "' name='" . $ml->line_name . "'>" . $ml->line_name . "</option>";
									}
									?>
								</select><br>

							</div>
							<div class="col-md-6">
								<label>STYLE</label>
								<select class="style_choses form-control" id="styleName" name="style" onchange="return addStyle()"> -->
									<option value="" id="user_id" selected="selected">Pilih Style</option>
									<?php 
										foreach ($m_style->result() as $key => $m_style) {
											echo "<option value='".$m_style->style."' name='".$m_style->style."'>".$m_style->style."</option>";
										}  
									?>
								</select><br><br>		
							</div>
              <div class="clearfix"></div><br>
                <!-- <button type="button" class="btn btn-primary submit-downloadstyle"><i class="fa fa-file-excel-o"></i> Download Style</button> -->
              <!-- <div class="pull-right"> -->
                <button type="button" class="btn btn-danger pencarian col-md-12"> Pencarian</button>
                <!-- <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button> -->
              <!-- </div> -->
            </div>
          </div>
      </div>
    </div>
  </div>

  
  <div class="clearfix"></div>
    <div class="box-body">
      <div style="overflow-x:auto;overflow-y:auto">
        <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
            <thead>
                <tr>
									<th class="text-center" width="100px">LINE</th>
									<th class="text-center" width="100px">NIK IE</th>
									<th class="text-center" width="100px">NAMA IE</th>
									<th class="text-center" width="100px">STYLE</th>
									<th class="text-center" width="100px">TGL MULAI</th>
									<th class="text-center" width="100px">TGL SELESAI</th>
									<th class="text-center" width="100px">ACTION</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
    </div>
  </div> 
</div>

<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
			$(".style_choses").chosen();

			$(".line_choses").chosen();

			var table = $('#table-result').DataTable({
				"processing": true,
				"serverSide": true,
				"orderMulti": true,
				"searching" : false,
				// sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
				"ajax":{
					"url": "data_line_balancing",
					"dataType": "json",
					"type": "POST",
					"beforeSend": function () {
							$.blockUI({
								message: "<img src='" + url_loading + "' />",
								css: {
									backgroundColor: 'transparant',
									border: 'none',
								}
							});
						},
						"complete": function() {
							$.unblockUI();
						},
						fixedColumns: true,
					"data":function(data) {
											data.line   = $('#lineName').val();
											data.style  = $('#styleName').val();
											data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
					},
				},
				"columns": [
					{ "data": "line_name", 'sortable' : true, 'orderable' : true },
					{ "data": "nik_ie" },
					{ "data": "nama_ie",'sortable' : false },
					{ "data": "style",'sortable' : true, 'orderable' : true },
					{ "data": "create_date",'sortable' : true, 'orderable' : true },
					{ "data": "finish_date",'sortable' : true, 'orderable' : true },
					{ "data": "action",'sortable' : false },
				],	
			});
			
			var dtable = $('#table-result').dataTable().api();
			dtable.draw();

			$('.pencarian').on('click', function(event) {
        event.preventDefault();
				dtable.draw();
			});
		
  });

	function report_lb(style, line, id) {
		window.open(base_url+'Qc_matrix/line_balancing?style='+style+'&line='+line+'&id='+id,'_blank');	
	}

</script>
