
<div class="row">
<div class="col-md-12">
<!-- 
	profil -->
	<div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Profil Sewer
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
		  <input type="hidden" id="nik" value="<?=$nik;?>">
          <div class="box-body">
		  <?php
		  foreach($profil as $p){
		  ?>
		  <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 ">NIK</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value = "<?php echo $p->nik; ?>" readonly/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 ">Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value = "<?php echo $p->name; ?>" readonly />
                            </div>
                          </div>
                        </div>
          </div>
		  <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 ">Department Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value = "<?php echo $p->department_name; ?>" readonly/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 ">Sub Department Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value = "<?php echo $p->subdept_name; ?>" readonly />
                            </div>
                          </div>
                        </div>
          </div>
		  <div class="row">
                        <!-- <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 ">Position</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value = "<?php echo $p->position; ?>" readonly/>
                            </div>
                          </div>
                        </div> -->
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 ">Factory</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value = "<?php echo $p->factory; ?>" readonly/>
                            </div>
                          </div>
                        </div>
                     
          </div>
		  <?php }
		  ?>
    
          <div class="clearfix"></div>
          
								
        </div>
    </div>
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Skill Base
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
          <!-- <input type="hidden" id="line" value="<?=$line;?>">
          <input type="hidden" id="mesin" value="<?=$mesin;?>">
          <input type="hidden" id="proses" value="<?=$proses;?>"> -->
          <!-- <div class="clearfix"></div><br> -->
		  <input type="hidden" id="nik" value="<?=$nik;?>">
          <div class="box-body">
    
          <div class="clearfix"></div>
          <div class="table-responsive"><br>
            <!-- <div style="overflow-x:auto;overflow-y:auto"> -->
                <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
											<th class="text-center">LINE</th>
											<th class="text-center">OPERATION COMPLEXITY</th>
											<th class="text-center">PROCESS</th>
											<th class="text-center">CATEGORY PROCESS</th>
											<th class="text-center">PERFORMANCE SCORE</th>
											<th class="text-center">QUALITY SCORE</th>
											<th class="text-center">STAR</th>
											<!-- <th class="text-center">Bintang</th> -->
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            <!-- </div> -->
          </div>
        </div>
    </div>
<!-- 
    line terakhir -->
    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Previous Line
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
		  <input type="hidden" id="nik" value="<?=$nik;?>">
          <div class="box-body">
    
          <div class="clearfix"></div>
          <div class="table-responsive"><br>
            <!-- <div style="overflow-x:auto;overflow-y:auto"> -->
                <table id="table-line" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
											
                      <th class="text-center">LINE</th>
                      <th class="text-center">TGL</th>
											<!-- <th class="text-center">Bintang</th> -->
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            <!-- </div> -->
          </div>
        </div>
    </div>
    
</div>
</div>

<!-- </div>
</div> -->

<script>
     var url_loading = $('#loading_gif').attr('href');
      var table = $('#table-result').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            // sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
							"url": "data_matrix",
							"dataType": "json",
							"type": "POST",
							"beforeSend": function () {
									$.blockUI({
										message: "<img src='" + url_loading + "' />",
										css: {
											backgroundColor: 'transparant',
											border: 'none',
										}
									});
								},
								"complete": function() {
									$.unblockUI();
								},
								fixedColumns: true,
							"data":function(data) {
													// data.line   = $('#line').val();
													// data.mesin  = $('#mesin').val();
													// data.proses = $('#proses').val();
													data.nik   = $('#nik').val();
													data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
							},
						},
					"columns": [
						{ "data": "line_name" },
						{ "data": "operation_complexity",'sortable' : false },
						{ "data": "proses_name",'sortable' : false },
						{ "data": "category_process",'sortable' : false },
						{ "data": "performance_score",'sortable' : false },
						{ "data": "quality_score",'sortable' : false },
						// { "data": "star",'sortable' : false, 'orderable' : true },
						{ "data": "bintang",'sortable' : true, 'orderable' : true },
					],
					
               
        });

        var table_line = $('#table-line').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            // sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
							"url": "data_lastline",
							"dataType": "json",
							"type": "POST",
							"beforeSend": function () {
									$.blockUI({
										message: "<img src='" + url_loading + "' />",
										css: {
											backgroundColor: 'transparant',
											border: 'none',
										}
									});
								},
								"complete": function() {
									$.unblockUI();
								},
								fixedColumns: true,
							"data":function(data) {
													// data.line   = $('#line').val();
													// data.mesin  = $('#mesin').val();
													// data.proses = $('#proses').val();
													data.nik   = $('#nik').val();
													data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
							},
						},
					"columns": [
						{ "data": "line_name" },
            { "data": "date" }
						// { "data": "date",'sortable' : false },
					
					],
					
               
        });


</script>
