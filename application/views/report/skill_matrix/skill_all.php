<div class="panel panel-default">
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header"> <i class="fa fa-database"> Skill Sewing</i></h3>
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?= base_url('home'); ?>">Report</a></li>
				<li><i class="fa fa-bars"></i>Skill Sewing</li>
			</ol>
		</div>
	</div>
</div>
<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="col-md-5">
			<label><b>Tanggal Awal</b></label>
			<input type="date" name="start" id="start" class="form-control" required="">
		</div>
		<div class="col-md-5">
			<label><b>Tanggal Akhir </b></label>
			<input type="date" name="end" id="end" class="form-control" required="">
		</div>
		<div class="col-lg-2">
			<button id="export" class="btn btn-success" style="margin-top: 25px;">Export</button>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#export').click(function(event){
		event.preventDefault();

		var start = $('#start').val();
		var end = $('#end').val();



		var url = "<?php echo base_url('Qc_matrix/export_skill');?>";

		window.open(url+'?start='+start+'&end='+end,'_blank');
	});
});
</script>