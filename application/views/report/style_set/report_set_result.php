
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               REPORT SET
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
          <input type="hidden" id="dari" value="<?=$dari;?>">
          <input type="hidden" id="sampai" value="<?=$sampai;?>">
          <input type="hidden" id="pobuyer" value="<?=$pobuyer;?>">
          <div class="clearfix"></div>
          <div class="box-body">
            <div class="clearfix"></div>
            <div class="table-responsive"><br>
                <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
                        <th class="text-center">DATE</th>
                        <th class="text-center">STYLE</th>
                        <th class="text-center">ARTICLE</th>
                        <th class="text-center">PO BUYER</th>
                        <th class="text-center">SIZE</th>
                        <th class="text-center">QTY ORDER</th>
                        <!-- <th class="text-center">BALANCE</th> -->
                        <th class="text-center">QTY OUTPUT</th>
                        <th class="text-center">LINE</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
    
</div>
</div>

<script>
    var url_loading = $('#loading_gif').attr('href');
    var table = $('#table-result').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "orderMulti"  : true,
        "searching": false,
        sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        "ajax":{
            "url"       : "report_set_ajax",
            "dataType"  : "json",
            "type"      : "POST",
            "beforeSend": function () {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                backgroundColor: 'transparant',
                border: 'none',
                }
            });
            },
            "complete": function() {
            $.unblockUI();
            },
            fixedColumns: true,
            "data":function(data) {
                    data.dari         = $('#dari').val();
                    data.sampai       = $('#sampai').val();
                    data.poreference  = $('#pobuyer').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    },
                        },
            "columns": [
                { "data": "date" },
                { "data": "style"},
                { "data": "article",'sortable'  : false },
                { "data": "poreference"},
                { "data": "size" },
                { "data": "order",'sortable'  : false },
                // { "data": "balance",'sortable'  : false },
                { "data": "output",'sortable'  : false },
                { "data": "line",'sortable'  : false },
            ] 
    });


</script>