
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa-database"> REPORT SET</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>REPORT SET</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="row">
      <div class="col-md-4">
          <div class="panel-group" id="accordion">
              <label>Input Tanggal Awal</label>
              <br>
              <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off">
              <br>
              <label>Input Tanggal Akhir</label>
              <br>
              <input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off">
              <br>
              <div class="panel panel-default">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" id='collapse1' href="#collapseOne">Pencarian Berdasarkan PO</a>
                      </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse">
                      <div class="panel-body">
                          <input class="form-control" type="text" id="pobuyer">        
                      </div>
                  </div>
              </div>
              <br>
              <button id='submit-download' href='javascript:void(0)' class='btn btn-md btn-primary'><i class='fa fa-file-text'></i> Download Report</button>
              <div class="pull-right">
                  <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button>
              </div>
          </div>
      </div>
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
      </div>
    </div>
  </div>
</div>
  
  <div id="result">    
  </div>

<div class="myDIV"></div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#tanggal2').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#clear_line').click(function() {
        $('#tanggal1').val('');
        $('#tanggal2').val('');
        $('#pobuyer').val('');
        table.draw();
      });
      
      $('#pencarian').click(function() {
          var dari    = $('#tanggal1').val();
          var sampai  = $('#tanggal2').val();
          var pobuyer = $('#pobuyer').val();

            var data = 'dari='+dari+'&sampai='+sampai+'&poreference='+pobuyer;
            $.ajax({
                url       : 'report_set_search',
                type      : "POST",
                data      : data,
                beforeSend: function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                  }
                });
              },
              success: function(response){
                $('#result').html(response);
              },
                    error:function(response){
                    if (response.status==500) {
                      $("#alert_info").trigger("click", 'Cek Input Data');
                      $.unblockUI();
                    }
                }
            });
          // }

          
      });

      $('#collapse1').click(function(event) {
          $('#pobuyer').val('');
      });

      $('#submit-download').on('click', function(event) {
        var dari    = $('#tanggal1').val();
        var sampai  = $('#tanggal2').val();
        var pobuyer = $('#pobuyer').val();

        window.open(base_url+'Report/download_report_set?dari='+dari+'&sampai='+sampai+'&pobuyer='+pobuyer);
      });
      
  });
</script>