<table border="1px">
	<thead>
      <tr>
          <th class="text-center">DATE</th>
          <th class="text-center">LINE</th>
          <th class="text-center">PO BUYER</th>              
          <th class="text-center">ARTICLE</th>                 
          <th class="text-center">STYLE</th>              
          <th class="text-center">SIZE</th>
          <th class="text-center">QTY ORDER</th>
          <th class="text-center">TOT. OUTPUT</th>
          <th class="text-center">OUTPUT DAY</th>
          <th class="text-center">BALANCE</th>
      </tr>          
    </thead>
    <tbody>
    	<?php foreach ($report->result() as $key => $r): ?>
        <tr>
          <td><?php echo $r->date;?></td>
          <td><?php echo $r->line_name;?></td>
          <td><?php echo $r->poreference;?></td>
          <td><?php echo $r->article;?></td>
          <td><?php echo $r->style;?></td>
          <td><?php echo $r->size;?></td>
          <td><?php echo $r->order;?></td>
          <td><?php echo $r->total_folding;?></td>
          <td><?php echo $r->daily_output;?></td>
          <td><?php echo $r->balance_folding;?></td>
        </tr>
      <?php endforeach ?>
    </tbody>
</table>