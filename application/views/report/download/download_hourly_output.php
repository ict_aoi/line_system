<table border="1px">
	<thead>
      <tr>
          <th class="text-center" rowspan="2">ACTIVITY NAME</th>
          <th class="text-center" rowspan="2">LINE ID</th>
          <th class="text-center" rowspan="2">LINE NAME</th>
          <th class="text-center" rowspan="2">DATE</th>
          <th class="text-center" colspan="13">JAM</th>
      </tr>
      <tr>
          <!-- <th></th>
          <th></th>
          <th></th>
          <th></th> -->
          <th class="text-center" width="">I</th>
          <th class="text-center" width="">II</th>
          <th class="text-center" width="">III</th>
          <th class="text-center" width="">IV</th>
          <th class="text-center" width="">V</th>
          <th class="text-center" width="">VI</th>
          <th class="text-center" width="">VII</th>
          <th class="text-center" width="">VIII</th>
          <th class="text-center" width="">IX</th>
          <th class="text-center" width="">X</th>
          <th class="text-center" width="">XI</th>
          <th class="text-center" width="">XII</th>
          <th class="text-center" width="">XIII</th>
      </tr>          
    </thead>
    <tbody>
      <?php
        foreach ($getLineSummary->result() as $key => $sum) {
          echo "<tr>";
            echo "<td>".$sum->activity_name."</td>";
            echo "<td>".$sum->line_id."</td>";
            echo "<td>".$sum->line_name."</td>";
            echo "<td>".$sum->date."</td>";
            echo "<td>".$sum->jam7."</td>";
            echo "<td>".$sum->jam8."</td>";
            echo "<td>".$sum->jam9."</td>";
            echo "<td>".$sum->jam10."</td>";
            echo "<td>".$sum->jam11."</td>";
            echo "<td>".$sum->jam12."</td>";
            echo "<td>".$sum->jam13."</td>";
            echo "<td>".$sum->jam14."</td>";
            echo "<td>".$sum->jam15."</td>";
            echo "<td>".$sum->jam16."</td>";
            echo "<td>".$sum->jam17."</td>";
            echo "<td>".$sum->jam18."</td>";
            echo "<td>".$sum->jam19."</td>";
          echo "</tr>";
        }

      ?>
    </tbody>
</table>