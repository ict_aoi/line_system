<table border="1px">
	<thead>
      <tr>
          <th class="text-center">DATE</th>
          <th class="text-center">LINE</th>
          <th class="text-center">ROUND</th>
          <th class="text-center">STYLE</th>
          <th class="text-center">KODE DEFECT</th>
          <th class="text-center">TOTAL DEFECT</th>
          <th class="text-center">TOTAL RANDOM</th>
      </tr>          
    </thead>
    <tbody>
    	<?php foreach ($getdataentryinline->result() as $key => $get): ?>
          <tr>
            <td><?php echo $get->date;?></td>
            <td><?php echo $get->line_name;?></td>
            <td><?php echo $get->round;?></td>
            <td><?php echo $get->style;?></td>
            <td><?php echo $get->defect_id;?></td>
            <td><?php echo $get->total_per_defect;?></td>
            <td><?php echo $get->total_random;?></td>
          </tr>
      <?php endforeach ?>
    </tbody>
</table>