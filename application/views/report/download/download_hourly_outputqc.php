<table border="1px">
	<thead>
      <tr>
          <th class="text-center">LINE</th>
          <th class="text-center">DATE</th>
          <th class="text-center">JAM KE</th>
          <th class="text-center">STYLE</th>
          <th class="text-center">PO BUYER</th>
          <th class="text-center">SIZE</th>
          <th class="text-center">OUTPUT</th>
      </tr>          
    </thead>
    <tbody>
      <?php foreach ($getLineSummary->result() as $key => $sum): ?>
        <tr>
          <td><?php echo $sum->line_name; ?></td>
          <td><?php echo $sum->date; ?></td>
          <td><?php echo $sum->jam; ?></td>
          <td><?php echo $sum->style; ?></td>
          <td style="mso-number-format:'\@'"><?php echo $sum->poreference; ?></td>
          <td><?php echo $sum->size; ?></td>
          <td><?php echo $sum->output; ?></td>
        </tr>
      <?php endforeach ?>
    	
    </tbody>
</table>