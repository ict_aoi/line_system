
<?php
  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report Tls Endline By Style '.$dari.' sd '.$sampai.'.xls"');
 $sumGood =0; $sumDeft = 0;
?>
<table border="1px">
  <thead>
      <tr>
          <th class="text-center" rowspan="2">DATE</th>          
          <th class="text-center" rowspan="2">STYLE</th>
          <th class="text-center" rowspan="2">LINE</th>
          <th class="text-center" colspan="170">DEFECT CODE</th>
          <th class="text-center" rowspan="2">Cumm. Defect</th>
          <th class="text-center" rowspan="2">First Good</th>
          <th class="text-center" rowspan="2">QTY Inspect</th>
          <th class="text-center" rowspan="2">Defect %</th>
      </tr>
      <tr>
        <?php foreach ($defect as $key => $dfc): ?>
        
          <th><?php echo "'".$dfc->defect_code ?></th>
        <?php endforeach ?>
      </tr> 
              
    </thead>
    <tbody>
        <?php foreach ($data as $dt) {
            $sumGood += $dt['total_good'];
            $sumDeft += $dt['total_defect'];
        ?>
          <tr>
              <td><?php echo date_format(date_create($dt['date']),'m/d/Y'); ?></td>
              <td><?php echo $dt['style']; ?></td>
              <td><?php echo $dt['line_name']; ?></td>
              <?php foreach ($defect as $defl) { ?>
                  <td><?php echo $dt['defect_'.$defl->defect_id] ?></td>
              <?php } ?>

              <td><?php echo $dt['total_defect'] ?></td>
              <td><?php echo $dt['total_good'] ?></td>
              <td><?php echo $dt['total_good']+$dt['total_defect'] ?></td>
              <td><?php echo number_format((float)(($dt['total_defect']/($dt['total_good']+$dt['total_defect']))*100), 2, '.', '') ?></td>
          </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
          <td colspan="3"> TOTAL</td>
            <?php foreach ($defect as $dft) {
            ?>
                <td><?php echo $totdef['defect_'.$dft->defect_id] ?></td>
            <?php
                }         
            ?>
          <td>
            <?php echo $sumDeft; ?>
          </td>
          <td>
            <?php echo $sumGood; ?>
          </td>
          <td>
            <?php echo ($sumDeft+$sumGood); ?>
          </td>
          <td>
              <?php echo number_format((float)(($sumDeft/($sumGood+$sumDeft))*100), 2, '.', '') ?>
          </td>
            
        </tr>
    </tfoot>

</table>
