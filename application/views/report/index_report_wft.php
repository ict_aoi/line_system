
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Report WFT</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Wft</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-5">
    <div class="box box-success">
      <div class="box-header with-border">
        <div class="box-title">Pilih Tanggal Laporan</div>
        <form action="<?=base_url('report/report_wft');?>" method="POST">
          <div class="box-body">
            <div class="form-group">
              <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" value="<?php echo $tanggal; ?>" required>
              <div class="clearfix"></div><br>
              <span class="input-group-btn">
                  <button class="btn btn-success col-md-3"><i class="fa fa-search"></i> Cari </button>
                  <?php
                      if ($tanggal <> '')
                      {
                          ?>
                          <span class="clearfix"></span><br>
                          <a href="<?=base_url('Report/downloadwft?tanggal='.$tanggal) ?>" class="btn btn-primary col-md-3" ><i class="fa fa-file-excel-o"> Download</i></a>
                          <?php
                      }
                  ?>
                
              </span>
            </div>
            <div class="form-group">
              
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="box-body">
    
    <table id="table-inspect" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
        <thead>
          <tr>
              <th class="text-center" width="80px">DATE</th>
              <th class="text-center" width="100px">LINE</th>
              <th class="text-center" width="150px">STYLE</th>
              <th class="text-center" width="80px">WFT </th>
          </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
  </div>
  </div> 
</div>
<div class="myDIV"></div>
<script type="text/javascript">
  $(function(){
      $('#tanggal').datepicker({
          format: "yyyy-mm-dd"
      });

      var table =$('#table-inspect').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "listfwft_ajax",
             "dataType": "json",
             "type": "POST",
             "scrollY": 200,
        "scrollX": true,
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
             'tanggal':$('#tanggal').val(),
              }
                           },
        "columns": [
                  { "data": "date" },
                  { "data": "line" },
                  { "data": "style",'sortable' : false },
                  { "data": "wft",'sortable' : false }
               ],
               
        }); 
      
  });
</script>