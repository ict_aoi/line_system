<div class="panel panel-default">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"> <i class="fa fa-database"> Change Over</i></h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
                <li><i class="fa fa-bars"></i>Change Over</li>
            </ol>
        </div>
    </div>
</div>
<div class="panel panel-default border-grey">
    <div class="panel-body">
        <div class="col-md-5">
            <div class="box box-success">
                <div class="box-header with-border">
                    <!-- <div class="box-title">Pilih Tanggal Laporan</div> -->
                    <div class="box-body">
                        <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#import1">
                IMPORT DATA
            </button> -->

                        <div class="form-group">
                            <div class=" alert alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Input Tanggal <b>harus</b> diisi
                            </div>
                            <!-- <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required> -->
                            <label>Input Tanggal</label>
                            <br>
                            <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off" required>
                            <br>
                            <div class="form-group">
                                <select class="form-control" id="filter" name="filter" required>
                                    <option value="">-- Pilih --</option>
                                    <option value="1">COPT</option>
                                    <option value="2">COT</option>
                                </select>
                            </div>
                            <div class="clearfix"></div><br>
                            <!-- <button type="button" class="btn btn-primary submit-downloadstyle"><i class="fa fa-file-excel-o"></i> Download Style</button> -->

                            <div class="pull-right">
                                <button type="button" class="btn btn-warning submit-download"><i
                                        class="fa fa-file-excel-o"></i> Download</button>
                                <!-- <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="result">
</div>

<div class="myDIV"></div>

<script type="text/javascript">
var url_loading = $('#loading_gif').attr('href');
var base_url = "<?php echo base_url();?>";
$(function() {
    $('#tanggal1').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $('.submit-download').on('click', function(event) {
        event.preventDefault();

        var tgl1 = $('#tanggal1').val();
        var filter = $('#filter option:selected').val();
        console.log(filter);

        if (!tgl1) {
            $("#alert_warning").trigger("click", 'TANGGAL TIDAK BOLEH KOSONG');
            return;
        }
        if (filter == '') {
            $("#alert_warning").trigger("click", 'SILAHKAN PILIH FILTER DOWNLOAD');
            return;
        }

        if (filter == 1) {
            window.open(base_url + 'report/report_copt?tanggal1=' + tgl1, '_blank');
        }
        if (filter == 2) {
            window.open(base_url + 'report/report_co_ajax?tanggal1=' + tgl1, '_blank');
        }
    });

});
</script>