
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Measurement</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>Measurement Inline</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey">
  <div class="panel-body">
      <div class="box-body">
        <div class="row col-md-4">
        
          <div class="form-group">
            <div class=" alert alert-danger fade in">
              <button data-dismiss="alert" class="close close-sm" type="button">
                  <i class="icon-remove"></i>
              </button>
              <strong>INFORMASI</strong><br> Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir
            </div>
            <!-- <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required> -->
            <label>Input Tanggal Awal</label>
            <br>
            <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off" required>
            <br>
            <label>Input Tanggal Akhir</label>
            <br>
            <input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off" required>
            <br>
          </div>
        </div>
        <div class="row col-md-8">
          <div class="form-group col-md-3">
              <input type="radio" name="filters" id="filter_date" value="by_date" checked="checked"> <label for="filter_date" class="control-label">Tanggal</label>
          </div>
          <div class="form-group col-md-3">
              <input type="radio" name="filters" id="filter_line" value="by_line"> <label for="filter_line" class="control-label">Range Line</label>
          </div>
          <!-- <div class="form-group col-md-3 div-tanggal">
            <label for="tanggal" class="control-label">Pilih Tanggal</label>
              <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" value="<?php echo $tanggal; ?>">
          </div> -->

          
          <div class="form-group col-md-3 div-line hidden">
            <label for="line" class="control-label">Range Line :</label>
            <select data-placeholder="line from" class="form-control line_choses" name="line_from" id="line_from">
              <option value=""></option>
              <?php
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
            <br>
            <br>
            <select data-placeholder="line to" class="form-control line_choses" name="line_to" id="line_to">
              <option value=""></option>
              <?php
                $master = $this->db->get('master_line');
                foreach ($master->result() as $key => $master) {
                  echo "<option value='".$master->master_line_id."' name=''>".$master->line_name."</option>";
                }
              ?>
            </select>
              <!-- <input class="form-control" name="line_from" id="line_from" autocomplete="off" value="" placeholder="from" onkeypress="return isNumberKey(event);">
              <input class="form-control" name="line_to" id="line_to" autocomplete="off" value="" placeholder="to" onkeypress="return isNumberKey(event);"> -->
          </div>
        </div>
        <hr class="col-md-12">
        <div class="panel-footer text-right">
          <div class="pull-left">
            <button class='btn btn-md btn-warning' id='pencarian'><i class='fa fa-search'></i> Search</button>
          </div>
          <button type="button" class="btn btn-success submit-download"><i class="fa fa-file-excel-o"></i> Download </button>
        </div>
    <!-- <div class="col-md-5"> -->
    <div class="box box-success">
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="box-body">

    <table id="table-inspect" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
        <thead>
          <tr>
              <th class="">DATE</th>
              <th class="">LINE</th>
              <th class="">NIK</th>
              <th class="">Operator Name</th>
              <th class="" width="">Process</th>
              <th class="text-center" colspan="5">Defect Code</th>
          </tr>
          <tr>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th class="text-center" width="">R1</th>
              <th class="text-center" width="">R2</th>
              <th class="text-center" width="">R3</th>
              <th class="text-center" width="">R4</th>
              <th class="text-center" width="">R5</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
  </div>
  </div>
</div>
<div class="myDIV"></div>
<script type="text/javascript">
var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#tanggal2').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('[name=filters]').change(function(event) {
        var filter = $(this).val();
        reset();
        if (filter == 'by_date') {
          $('.div-tanggal').removeClass('hidden');
          $('.div-line').addClass('hidden');
        }else if (filter == 'by_line') {
          // $("#alert_error").trigger("click", 'Terjadi Kesalahan');
          $('.div-tanggal').removeClass('hidden');
          $('.div-line').removeClass('hidden');
        }

      });


      function reset() {
        // $('#line_from').val('');
        // $('#line_to').val('');
        // $('#tanggal').val('');

        $('#table-inspect > tbody').empty();
      }

      var url_loading = $('#loading_gif').attr('href');

      var table =$('#table-inspect').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "list_measurementinline_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
             "scrollY": 200,
             "scrollX": true,
                    "data":function(data) {
                      // console.log(data.line_from);
                      // console.log(data.line_to);
                        data.dari = $('#tanggal1').val();
                        data.sampai = $('#tanggal2').val();
                        data.line_from = $('#line_from').val();
                        data.line_to = $('#line_to').val();
                        data.filter = $("input[name=filters]:checked").val()
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
                  { "data": "date" },
                  { "data": "line_name" },
                  { "data": "sewer_nik",'sortable' : false },
                  { "data": "sewer_name",'sortable' : false },
                  { "data": "proses_name",'sortable' : false },
                  { "data": "round1",'sortable' : false },
                  { "data": "round2",'sortable' : false },
                  { "data": "round3",'sortable' : false },
                  { "data": "round4",'sortable' : false },
                  { "data": "round5",'sortable' : false }
               ],

        });

      // $('#tanggal').change(function(event) {
      //   if ($("input[name=filters]:checked").val() != 'by_line') {
      //     table.draw();
      //     if (event.keyCode == 13 && $(this).val() != '') {
      //       table.draw();
      //     }
      //   }else{
      //     $('#line_to').trigger('change');
      //   }

      // });

      $(".line_choses").chosen({width: "100%"});

      /*$('#line_to').keyup(function(event) {
        var line_from = $('#line_from').val();
        if (event.keyCode == 13) {
            if ($(this).val() < line_from) {
              $("#alert_error").trigger("click", 'Terjadi Kesalahan Range Line');
            }else{
              table.draw();
            }
        }
      });*/
      //  $('#line_to').change(function(event) {
      //   var line_from = $('#line_from').val();
      //   if ($(this).val() < line_from) {
      //       $("#alert_error").trigger("click", 'Terjadi Kesalahan Range Line');
      //     }else{
      //       table.draw();
      //     }
      // });

      $('.submit-download').on('click', function(event) {
        event.preventDefault();
        var line_from = $('#line_from').val();
        var line_to   = $('#line_to').val();
        var dari      = $('#tanggal1').val();
        var sampai    = $('#tanggal2').val();
        var filter    = $("input[name=filters]:checked").val();

        if (dari != '' || sampai != '' || (line_from!='' && line_to!='') || filter != '') {
          window.open(base_url+'Report/download_measurement?filter='+filter+'&dari='+dari+'&sampai='+sampai+'&line_from='+line_from+'&line_to='+line_to,'_blank');
        }else{
          $("#alert_error").trigger("click", 'Terjadi Kesalahan');
        }

      });

      $('#pencarian').click(function() {
        var dari   = $('#tanggal1').val();
        var sampai = $('#tanggal2').val();
        
        var line_from = $('#line_from').val();
        var line_to = $('#line_to').val();

        var filter    = $("input[name=filters]:checked").val();

        if(filter == 'by_date'){
          if(dari>sampai){
            $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
          }
          else{
            table.draw();
          }
        }else if(filter == 'by_line'){
          if(dari>sampai){
            $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
          }
          else{
            if(line_from > line_to){
              $("#alert_warning").trigger("click", 'CEK LINE AWAL DAN LINE AKHIR');
            }
            else{
              if(line_from=='' || line_to==''){
                $("#alert_warning").trigger("click", 'CEK LINE KOSONG');
              }
              else{
                table.draw();
              }
            }
          }
        }
      });

  });
</script>