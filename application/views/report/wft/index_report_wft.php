<div class="panel panel-default">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"> <i class="fa fa-database"> WFT</i></h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
                <li><i class="fa fa-bars"></i>WFT</li>
            </ol>
        </div>
    </div>
</div>
<div class="panel panel-default border-grey">
    <div class="panel-body">
        <div class="col-md-5">
            <div class="box box-success">
                <div class="box-header with-border">
                    <!-- <div class="box-title">Pilih Tanggal Laporan</div> -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class=" alert alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal
                                Akhir
                            </div>
                            <!-- <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required> -->
                            <label>Input Tanggal Awal</label>
                            <br>
                            <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off" required>
                            <br>
                            <label>Input Tanggal Akhir</label>
                            <br>
                            <input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off" required>
                            <br>
                            <div class="clearfix"></div><br>
                            <!-- <button type="button" class="btn btn-primary submit-downloadstyle"><i class="fa fa-file-excel-o"></i> Download Style</button>
                <button type="button" class="btn btn-warning submit-download"><i class="fa fa-file-excel-o"></i> Download</button> -->
                            <div class="form-group">
                                <div class="col-md-8">
                                    <select class="form-control" name="filter" id="filter" required>
                                        <option value="">-- Pilih --</option>
                                        <option value="0">Line</option>
                                        <option value="1">Style</option>
                                        <option value="2">PO</option>
                                    </select>
                                </div>
                                <div class="pull-right">
                                    <button class='btn btn-md btn-success' id='pencarian'><i
                                            class='fa fa-file-excel-o'></i>
                                        Download</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<div id="result">
</div>

<div class="myDIV"></div>
<script type="text/javascript">
var url_loading = $('#loading_gif').attr('href');
var base_url = "<?php echo base_url();?>";
$(function() {
    $('#tanggal1').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $('#tanggal2').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $('#pencarian').click(function() {
        var tgl1 = $('#tanggal1').val();
        var tgl2 = $('#tanggal2').val();
        var filter = $('#filter option:selected').val();;

        if (!tgl1 || !tgl2) {
            $("#alert_warning").trigger("click", 'TANGGAL TIDAK BOLEH KOSONG');
            return;
        }
        if (tgl1 > tgl2) {
            $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
            return;
        }

        if (filter == '') {
            $("#alert_warning").trigger("click", 'SILAHKAN PILIH FILTER DOWNLOAD');
            return;
        }

        if (filter == 0) {
            window.open(base_url + 'Report/downloadwft?dari=' + tgl1 + '&sampai=' + tgl2,
                '_blank');
        }
        if (filter == 1) {
            window.open(base_url + 'Report/downloadwftstyle?dari=' + tgl1 + '&sampai=' + tgl2,
                '_blank');
        }
        if (filter == 2) {
            window.open(base_url + 'Report/downloadWftPo?dari=' + tgl1 + '&sampai=' + tgl2,
                '_blank');
        }

    });


});
</script>