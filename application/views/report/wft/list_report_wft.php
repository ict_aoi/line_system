
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               WFT
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
          <input type="hidden" id="tgl1" value="<?=$tgl1;?>">
          <input type="hidden" id="tgl2" value="<?=$tgl2;?>">
          <div class="clearfix"></div><br>
          <div class="box-body">
    
          <div class="clearfix"></div>
          <div class="table-responsive"><br>
            <div style="overflow-x:auto;overflow-y:auto">
                <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="150px">DATE</th>
                            <th class="text-center"width="100px">LINE</th>
                            <th class="text-center"width="100px">TOTAL GOOD</th>
                            <th class="text-center" colspan="2" width="100px">TOTAL DEFECT</th>
                            <th class="text-center"width="80px">WFT </th>
                        </tr>
                        <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th width="80px" class="text-center">INLINE</th>
                        <th width="80px" class="text-center">ENDLINE</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
    
</div>
</div>

<!-- </div>
</div> -->

<script>
     var url_loading = $('#loading_gif').attr('href');
      var table = $('#table-result').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "listfwft_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                        data.dari     = $('#tanggal1').val();
                        data.sampai   = $('#tanggal2').val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
                  { "data": "date" },
                  { "data": "line" },
                  { "data": "total_good",'sortable' : false },
                  { "data": "defectinline",'sortable' : false },
                  { "data": "defectendline",'sortable' : false },
                  { "data": "wft",'sortable' : false },
               ],
               
        });


</script>