
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> TLS</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>TLS INLINE</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-5">
      <div class="box box-success">
        <div class="box-header with-border">
          <div class="box-title">Pilih Tanggal Laporan</div>
          <div class="box-body">
            <div class="form-group">
              <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" value="<?php echo $tanggal; ?>" required>
              <div class="clearfix"></div><br>
              <div class="text-right">
                <!-- <button type="button" class="btn btn-primary submit-download-style"><i class="fa fa-file-excel-o"></i> Download By Style</button>
                <button type="button" class="btn btn-success submit-download-line"><i class="fa fa-file-excel-o"></i> Download By line</button>
                <button type="button" class="btn btn-warning submit-download-entry"><i class="fa fa-file-excel-o"></i> Download Data Entry</button> -->
              </div>
            </div>
            <div class="col-md-6">
              <button type="button" class="btn btn-primary btn-block submit-download-style"><i class="fa fa-file-excel-o"></i> Download By Style</button>
              <br>
              <button type="button" class="btn btn-success btn-block submit-download-line"><i class="fa fa-file-excel-o"></i> Download By Line</button>  
            </div>
            <div class="col-md-6">
              <button type="button" class="btn btn-warning btn-block submit-download-entry"><i class="fa fa-file-excel-o"></i> Download Data Entry</button>
              <br>
              <button type="button" class="btn btn-default btn-block submit-download-week"><i class="fa fa-file-excel-o"></i> Download Data Last Week</button>    
            </div>
            <!-- <div class="form-group"> 
            </div> -->
          </div>

        </div>
      </div>
    </div>
    <div class="col-md-7">
      <div class=" alert alert-danger fade in">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <strong>INFORMASI</strong><br> <b>Download Data Last Week</b> di update setiap <b>Hari Senin</b>, berisikan data <b>seminggu</b> yang lalu.
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="box-body">
      <div style="overflow-x:auto;overflow-y:auto">
        <table id="table-tls" class="table table-full-width dataTable" cellspacing="0" width="100%">
            <thead>
              <tr>
                  <th class="text-center" rowspan="2">DATE</th>
                  <th class="text-center" rowspan="2">LINE</th>
                  <th class="text-center" rowspan="2">PO BUYER</th>
                  <th class="text-center" rowspan="2">STYLE</th>
                  <th class="text-center" rowspan="2">TOTAL RANDOM</th>
                  <th class="text-center" rowspan="2">TOTAL DEFECT</th>
                  <th class="text-center" rowspan="2">RFT</th>
                  <th class="text-center" colspan="30">DEFECT CODE</th>
              </tr>
              <tr>
                <?php foreach ($defect_id as $key => $defect): ?>
                  <th><?php echo $defect->defect_id ?></th>
                <?php endforeach ?>
              </tr>          
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
    </div>
  </div> 
</div>
<div class="myDIV"></div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });
      var url_loading = $('#loading_gif').attr('href');
      var table = $('#table-tls').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "rftreport_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                        data.tanggal = $('#tanggal').val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
                  { "data": "create_date" },
                  { "data": "line" },
                  { "data": "poreference",'sortable' : false },
                  { "data": "style",'sortable' : false },
                  { "data": 'total_checked','sortable' : false },
                  { "data": "count_defect",'sortable' : false },
                  { "data": "rft",'sortable' : false },
                  { 'data': 'defect1','sortable' : false },
                  { 'data': 'defect2','sortable' : false },
                  { 'data': 'defect3','sortable' : false },
                  { 'data': 'defect4','sortable' : false },
                  { 'data': 'defect5','sortable' : false },
                  { 'data': 'defect6','sortable' : false },
                  { 'data': 'defect7','sortable' : false },
                  { 'data': 'defect8','sortable' : false },
                  { 'data': 'defect9','sortable' : false },
                  { 'data': 'defect10','sortable' : false },
                  { 'data': 'defect11','sortable' : false },
                  { 'data': 'defect12','sortable' : false },
                  { 'data': 'defect13','sortable' : false },
                  { 'data': 'defect14','sortable' : false },
                  { 'data': 'defect15','sortable' : false },
                  { 'data': 'defect16','sortable' : false },
                  { 'data': 'defect17','sortable' : false },
                  { 'data': 'defect18','sortable' : false },
                  { 'data': 'defect19','sortable' : false },
                  { 'data': 'defect20','sortable' : false },
                  { 'data': 'defect21','sortable' : false },
                  { 'data': 'defect22','sortable' : false },
                  { 'data': 'defect23','sortable' : false },
                  { 'data': 'defect24','sortable' : false },
                  { 'data': 'defect25','sortable' : false },
                  { 'data': 'defect26','sortable' : false },
                  { 'data': 'defect27','sortable' : false },
                  { 'data': 'defect28','sortable' : false },
                  { 'data': 'defect29','sortable' : false },
                  { 'data': 'defect30','sortable' : false },
               ],
               
      });

        $('#tanggal').change(function(event) {
          table.draw();
          if (event.keyCode == 13 && $(this).val() != '') {
              table.draw();
            }
        });
        /*download rft by style*/
        $('.submit-download-style').on('click', function(event) {
            var url_loading = $('#loading_gif').attr('href');
            event.preventDefault();
            var tanggal = $('#tanggal').val();

            if (tanggal != '') {

              window.open(base_url+'Report/downloadrft_bystyle?tanggal='+tanggal,'_blank');
            }else{
              $("#alert_error").trigger("click", 'Terjadi Kesalahan');
            }

        });
        /*download rft by style*/
        $('.submit-download-line').on('click', function(event) {
           var url_loading = $('#loading_gif').attr('href');
            event.preventDefault();
            var tanggal = $('#tanggal').val();

            if (tanggal != '') {

              window.open(base_url+'Report/downloadrft_byline?tanggal='+tanggal,'_blank');
            }else{
              $("#alert_error").trigger("click", 'Terjadi Kesalahan');
            }

        });
        $('.submit-download-entry').on('click', function(event) {
           var url_loading = $('#loading_gif').attr('href');
            event.preventDefault();
            var tanggal = $('#tanggal').val();

            if (tanggal != '') {

              window.open(base_url+'Report/downloaddataentryinline?tanggal='+tanggal,'_blank');
            }else{
              $("#alert_error").trigger("click", 'Terjadi Kesalahan');
            }

        });

        $('.submit-download-week').on('click', function(event) {
           var url_loading = $('#loading_gif').attr('href');
            event.preventDefault();
            window.open(base_url+'report/download_tls_inline_week','_blank');
        });

        $('#clear_line').click(function() {
          table.draw();
        });
      
  });
</script>