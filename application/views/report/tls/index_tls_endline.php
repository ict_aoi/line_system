
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> TLS</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>TLS ENDLINE</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="col-md-5">
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <div class="box-title">Pilih Tanggal Laporan</div> -->
          <div class="box-body">
            <div class="form-group">
              <div class=" alert alert-danger fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                </button>
                <strong>INFORMASI</strong><br> Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir
              </div>
              <!-- <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required> -->
              <label>Input Tanggal Awal</label>
              <br>
              <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off" required>
              <br>
              <label>Input Tanggal Akhir</label>
              <br>
              <input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off" required>
              <br>

              <div class="clearfix"></div><br>
              <!-- <div class="text-right"> -->
              <button type="button" class="btn btn-primary submit-download-style"><i class="fa fa-file-excel-o"></i> Download By Style</button>
              <button type="button" class="btn btn-success submit-download-entry"><i class="fa fa-file-excel-o"></i> Download Data Entry</button>
              <!-- </div> -->
              <div class="pull-right">
                <button class='btn btn-md btn-warning' id='pencarian'><i class='fa fa-search'></i> Search</button>
              </div>
            </div>
            <div class="form-group">
              
            </div>
          </div>

      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="box-body">
    <div style="overflow-x:auto;overflow-y:auto">
    <table id="table-tls" class="table table-full-width dataTable" cellspacing="0" width="100%">
        <thead>
          <tr>
              <th class="text-center" rowspan="2">DATE</th>
              <th class="text-center" rowspan="2">LINE</th>
              <th class="text-center" rowspan="2">STYLE</th>              
               <th class="text-center" rowspan="2">TOTAL GOOD</th>
               <th class="text-center" rowspan="2">TOTAL DEFECT</th>
              <th class="text-center" colspan="30">DEFECT CODE</th>
          </tr>
          <tr>
           <?php foreach ($defect_id as $key => $defect): ?>
            <th><?php echo $defect->defect_id ?></th>
          <?php endforeach ?> 
          </tr>          
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
  </div>
  </div> 
</div>
<div class="myDIV"></div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#tanggal2').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      // $('#tanggal').datepicker({
      //     format: "yyyy-mm-dd",
      //     autoclose: true
      // });
      
      var url_loading = $('#loading_gif').attr('href');
      var table = $('#table-tls').DataTable({
            "processing": true,
            "serverSide": true,
            "orderMulti"  : true,
            "searching": false,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "tls_endlline_ajax",
             "dataType": "json",
             "type": "POST",
             "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                        data.dari = $('#tanggal1').val();
                        data.sampai = $('#tanggal2').val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
                           },
        "columns": [
                  { "data": "date" },
                  { "data": "line" },
                  { "data": "style",'sortable' : false },
                  { "data": "total_good",'sortable' : false },
                  { "data": "total_defect",'sortable' : false },
                  { 'data': 'defect1','sortable' : false },
                  { 'data': 'defect2','sortable' : false },
                  { 'data': 'defect3','sortable' : false },
                  { 'data': 'defect4','sortable' : false },
                  { 'data': 'defect5','sortable' : false },
                  { 'data': 'defect6','sortable' : false },
                  { 'data': 'defect7','sortable' : false },
                  { 'data': 'defect8','sortable' : false },
                  { 'data': 'defect9','sortable' : false },
                  { 'data': 'defect10','sortable' : false },
                  { 'data': 'defect11','sortable' : false },
                  { 'data': 'defect12','sortable' : false },
                  { 'data': 'defect13','sortable' : false },
                  { 'data': 'defect14','sortable' : false },
                  { 'data': 'defect15','sortable' : false },
                  { 'data': 'defect16','sortable' : false },
                  { 'data': 'defect17','sortable' : false },
                  { 'data': 'defect18','sortable' : false },
                  { 'data': 'defect19','sortable' : false },
                  { 'data': 'defect20','sortable' : false },
                  { 'data': 'defect21','sortable' : false },
                  { 'data': 'defect22','sortable' : false },
                  { 'data': 'defect23','sortable' : false },
                  { 'data': 'defect24','sortable' : false },
                  { 'data': 'defect25','sortable' : false },
                  { 'data': 'defect26','sortable' : false },
                  { 'data': 'defect27','sortable' : false },
                  { 'data': 'defect28','sortable' : false },
                  { 'data': 'defect29','sortable' : false },
                  { 'data': 'defect30','sortable' : false },
               ],
               
        });

        $('#pencarian').click(function() {
          var dari   = $('#tanggal1').val();
          var sampai = $('#tanggal2').val();

          if(dari>sampai){
            $("#alert_warning").trigger("click", 'CEK TANGGAL AWAL DAN AKHIR');
          }
          else{
            table.draw();
          }
        });
        // $('#tanggal').change(function(event) {
        //   table.draw();
        //   if (event.keyCode == 13 && $(this).val() != '') {
        //       table.draw();
        //     }
        // });
        /*download rft by style*/
        $('.submit-download-style').on('click', function(event) {
            var url_loading = $('#loading_gif').attr('href');
            event.preventDefault();

            var dari   = $('#tanggal1').val();
            var sampai = $('#tanggal2').val();

            console.log(dari);
            console.log(sampai);
            if (dari != '' && sampai != '') {

              window.open(base_url+'report/downloadrftendline_bystyle?dari='+dari+'&sampai='+sampai,'_blank');
            }else{
              $("#alert_error").trigger("click", 'Terjadi Kesalahan');
            }

        });
        /*download data entry*/
        $('.submit-download-entry').on('click', function(event) {
           var url_loading = $('#loading_gif').attr('href');
            event.preventDefault();
            var dari   = $('#tanggal1').val();
            var sampai = $('#tanggal2').val();

            // console.log(dari);
            // console.log(sampai);
            if (dari != '' && sampai != '') {
              // window.open(base_url+'report/download_dataentryendline?dari='+dari+'&sampai='+sampai,'_blank');
              window.open(base_url+'report/download_dataentryendline?dari='+dari+'&sampai='+sampai);
            }else{
              $("#alert_error").trigger("click", 'Terjadi Kesalahan');
            }

        });
      
  });
</script>