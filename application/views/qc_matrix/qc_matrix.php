<!-- <style>
#myModal {
  overflow-x: hidden;
  overflow-y: auto;
}
</style> -->
<style>
  /* Set Background Image for body and basic font-color for the page */
  /*
  Styling Page Header with Stopwatch Image in header
  vertical-align text and img to bottom
  */

  #page-header {
    text-align: center;
    font-size: 7vw;
    border-bottom: none;
  }

  #page-header span {
    line-height: 7vw;
    text-transform: uppercase;
    vertical-align: bottom;
  }

  #page-header img {
    height: 7vw;
    padding-bottom: 0;
    margin: 0 3px;
    padding: 0;
    vertical-align: bottom;
  }


  /* Background of .jumbotron (= container for timers and controls) transparent */

  .jumbotron {
    background-color: transparent;
  }


  /* Styling Timers */

  #time,
  #lap {
    text-align: center;
  }

  #time {
    font-size: 8vw;
  }

  #timeMsec {
    font-size: 6vw;
  }

  #lap {
    font-size: 3vw;
  }

  #lapMsec {
    font-size: 2vw;
  }


  /* Positioning Buttons in '.controls'-div */

  .controls {
    text-align: center;
    padding-top: 20px;
  }


  /* Styling Buttons */

  .btn-controls {
    display: inline-block;
    margin: 10px;
    height: 130px;
    width: 130px;
    border: 1px solid #fff;
    border-radius: 100%;
    font-size: 24px;
    font-weight: bold;
    color: #fff;
  }

  #startStop {
    background-color: rgb(0, 204, 153);
  }

  #startStop:hover {
    background-color: rgba(0, 153, 115, 1);
  }

  #split {
    background-color: rgba(186, 74, 94, .3);
  }

  #reset {
    color: rgba(51, 51, 51, 1);
    background-color: rgba(189, 188, 186, 1);
  }

  #reset:hover {
    background-color: rgb(51, 51, 51);
    color: #fff;
  }


  /* Table hidden, so it can be shown when 'split'-Button is clicked */

  #tables {
    display: none;
    font-size: 18px;
  }
</style>

<div class="panel panel-default border-grey" >
      <div class="panel-heading">
       <h6 class="panel-title">
         <div class="col-md-2">
           Actual Production Time
         </div>
         <div class="col-sm-3 pull-right">
          <div class="input-group">
          </div>

            <!-- <div class="row">
              <button type="button" class="btn btn-primary" id="btnBack"><i class="fa fa-arrow-circle-left"></i> Back </button>
              <button type="button" class="btn btn-primary" id="btnNext"><i class="fa fa-arrow-circle-right"></i> Next </button>
            </div> -->
         </div>
       </h6>
      </div>
			
			<br>
			<input type="hidden" name="batch_id" value="<?=$batch?>"  id="batch_id">
			<?php
				if($status_batch == 200){
					// echo '<button id="start_job" class="col-md-12 btn btn-warning btn-lg btn-block">Start Job</button>';
				}
				else{
					echo '<div class="alert alert-success fade in">
					<button data-dismiss="alert" class="close close-sm" type="button">
						<i class="icon-remove"></i>
					</button>
					<strong>Informasi</strong><br> Batch Job saat ini aktif pada <b> STYLE '.$main_style.' , '.$line. ' </b> 
					Silahkan melanjutkan APT.
				</div>';
				}
			?>
			
      <br>
      <div class="panel-body">
			
				<div class="table-responsive">
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover table-full-width">
            <thead>
              <tr>
                <th>Style</th>
                <th width="250px">Nama Proses</th>
                <th>Nama Mesin</th>
                <th>NIK</th>
                <th>Nama Sewer</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><input type="text" class="form-control" name="style" value="<?=$main_style?>" id="style" required readonly></input></td>
                <td>
                  <div class="input-group">
                  <div class="input-group-btn">
                      <button type="button" class="btn btn-info" onclick="return loadproses()"><i class="fa fa-list" /></button>
                  </div>
                  <input type="text" class="form-control" name="proses" style="left" id="proses" onclick="return loadproses()" readonly="readonly" placeholder="PILIH PROSES">
                </td>
                <td>
                  <div class="input-group">
                  <div class="input-group-btn">
                      <button type="button" class="btn btn-info" onclick="return loadproses()"><i class="fa fa-list" /></button>
                  </div>
                  <input type="text" class="form-control" name="mesin" style="left" id="mesin" onclick="return loadproses()" readonly="readonly" placeholder="PILIH MESIN ">
              
                </td>
                <td><input type="text" class="form-control" name="nik" id="nik" required autocomplete="off"></td>
                <td><input type="text" class="form-control" name="sewer" id="sewer" required readonly></td>
              </tr>
              <tr>
                <th>
                  JENIS
                </th>
                <th></th>
                <!-- <td colspan='2'>
                  <select class="form-control select" id="tc" name="tc" required>
                      <option value="">-- Pilih --</option>
                      <option value="t">ASSEMBLY</option>
                      <option value="f">PRE ASSEMBLY</option>
                  </select>
                </td> -->
                <th>
                  WIP
                </th>
                <th></th>
                <th>
                  Stitches Burst
                </th>
                <!-- <td colspan='2'>
                  <input type="text" class="form-control" name="wip" id="wip" required>
                </td> -->
              </tr>
              <tr>
                <td>
                  <select class="form-control select" id="tc" name="tc" required>
                      <option value="">-- Pilih --</option>
                      <option value="t">ASSEMBLY</option>
                      <option value="f">PRE ASSEMBLY</option>
                  </select>
                </td>
                <td></td>
                <td>
                  <input type="number" class="form-control" name="wip" id="wip" required>
                </td>
                <td></td>
                <td>
                  <input type="number" class="form-control" name="burst" id="burst" required>
                </td>
              </tr>
            </tbody>
          </table>
         </div>
        </div>
       <!-- <div class="col-md-12">
          <div class="input-group">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-info" onclick=""><i class="fa fa-list" /></button>
                </div>
                <input type="text" class="form-control" style="left" id="grade" onclick="" readonly="readonly" placeholder="">
           </div>
        <br><br>
        </div> -->
       <form class="frmsave" method="POST">

        <!-- <input type="hidden" name="lastproses" id="lastproses-hid"> -->
        <!-- <input type="hidden" name="grade" id="grade-hid"> -->
        <!-- <input type="hidden" value="<$round; ?>" id="round-hid" name="round"> -->
        <input type="hidden" value="<?=$record['inline_header_id']?>" name="header_id" id="header_id">
        <input type="hidden" value="<?=$record['poreference']?>" name="poreference">
        <input type="hidden" value="<?=$record['line_id']?>" name="line_id" id="line_id">
        <input type="hidden" name="style" value="<?=$record['style'] ?>">
        <input type="hidden" name="sewer" id="sewer-hid">
        <input type="hidden" name="wip" id="wip-hid">
        <input type="hidden" name="tc" id="tc-hid">
        <input type="hidden" name="burst" id="burst-hid">
        <input type="hidden" name="proses" id="proses-hid" value="0">
        <input type="hidden" name="mesin" id="mesin-hid" value="0">
        <!-- <input type="hidden" name="items" id="items" value="[]"> -->
        <!-- <input type="hidden" name="items_grade" id="items_grade" value="">
        <input type="hidden" name="grade_before" id="grade_before" value="0"> -->
        <div class="row">
					<div class="col-xs-12 text-center">
					<br>
						<!-- <input type="text" class="btn btn-danger disabled" name="smv_spt" id="smv_spt" value=""> -->
						<span><b>SMV SPT</b></span><br>
						<span class="btn btn-danger disabled" id="smv_spt"><b>-</b></span>
						<br>
					</div>
				</div>

        <div class="col-xs-12">
          <div id="stopwatch">
            <!-- Stopwatch Timers -->
            <div id="time">
              <!-- <span id="timeHour">00</span>:<span id="timeMinute">00</span>: -->
              <span id="timeSec">00</span>.<span id="timeCsec">00</span>
            </div>
            <div id="lap">
              <!-- <span id="lapHour">00</span>:<span id="lapMinute">00</span>: -->
              <span id="lapSec">00</span>.<span id="lapCsec">00</span>
            </div>
            <!-- </div> -->
            <!-- Stopwatch Controls -->
            <div class="container">
              <div class="controls">
                <button type="button" id="startStop" class="btn-controls">
                  <span class="glyphicon glyphicon-play" aria-hidden="true"></span>
                  <span class="sr-only">Start</span>
                </button>
                <button type="button" id="split" class="btn-controls">Split</button>
                <button type="button" id="reset" class="btn-controls">Reset</button>
              </div>
            </div>

            <!-- </div> -->
            <!-- Lap Times -->
            <div id="lapTable" class="container">
              <table class="table" id="tables">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Interval</th>
                    <th>Total</th>
                    <th>Time Recorded</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <!-- <tbody id="laptimes"> -->
                  <!-- print lapTimeDetails here -->
                <!-- </tbody> -->
								<tbody id="tbody-items">
          			</tbody>
              </table>
            </div>  
          </div>
        </div>
        

        <br>
        <div class="col-md-12">
					<input type="hidden" name="items" id="items" value="[]">
          <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>      
				
          <!-- <button id="simpan" class="btn btn-primary btn-lg btn-block">Simpan</button> -->
        </div>
     </form>
      </div>
			
			<?php
				if($status_batch == 200){
				}
				else{
					echo '<button id="close_job" class="col-md-12 btn btn-warning btn-lg btn-block">Close Job</button>';
				}
			?>
     </div>
<!-- <button type="button" class="hidden" id="refresh"></button>
<button type="button" class="hidden" id="reset"></button> -->
<!-- <a href="</?php echo base_url('qc_matrix') ?>" id="logo" class="hidden"></a> -->
<a href="<?php echo base_url(); ?>/qc_matrix" id="logo" class="hidden"></a>

<script type="text/javascript" src="<?php echo base_url();?>assets/resource/qc_matrix.js"></script>


<script type="x-tmpl-mustache" id="items-table">  
  {% #item %}
    <tr>
      <td style="text-align:center;">{% no %}</td>
      <td>
        <div id="item_interval_{% _id %}">{% interval %}</div>
      </td>
      <td>
				<div id="item_total_{% _id %}">{% waktu %}</div>
      </td>  
      <td>
				<div id="item_record_{% _id %}">{% record %}</div>
      </td>  
      <td>
            <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="fa fa-trash-o"></i></button>
      </td>
    </tr>
  {%/item%}

   <tr>
	 <td width="20px">
      #
    </td>
    <td>
    </td>

    <td>
    </td>
    
    <td>
    </td>

		<td>
		</td>
    
  </tr>
</script>


<script>

  var jalan = 0;
  var listWaktu = [];
    // $('#simpan').click(function() {
      
    //   if(listWaktu.length >= 3)
    //   {
    //     console.log(listWaktu);
    //   }
    //   else{
    //     alert("fuck");
    //   }

    // });

  // $(function() {
    // Variables:
    var mode = 0; // App mode: 0 when watch has not been started
    var timeCounter = 0; // Time counter
    var lapCounter = 0; // Lap time counter
    // var lapNumber = 0; // Number of Laps
    var loop; // Variable for setInterval
    var date; // Variable for current date and time

    // Hours, Minutes, Seconds, Milliseconds for time and lap time:
    var timeHours, timeMinutes, timeSeconds, timeCsec;
    var lapHours, lapMinutes, lapSeconds, lapCsec;
    // Disable split-Button:
    $('#split').prop('disabled', true);
    // When start-Button is clicked:
    $('#startStop').click(function() {
      var nik = $('#nik').val();
      var proses = $('#proses').val();
      var mesin = $('#mesin').val();
      var wip = $('#wip').val();
      var tc = $('#tc').val();
      var burst = $('#burst').val();
      if (nik=='' || proses=='' || mesin =='' || wip == '' || tc == '') {
        $("#alert_error").trigger("click", 'Tolong Inputan Di Lengkapi Semua');
      }else{
        if (mode === 0) {
          // Set app mode to 1 (on)
          mode = 1;
          jalan = 1;
          // Change start-Button to stop-Button
          $('#startStop').html('<span class="glyphicon glyphicon-stop" aria-hidden="true"></span><span class="sr-only">Stop</span>');
          $('#startStop').css('background-color', 'rgb(0, 153, 204)');
          $('#startStop').hover(function() {
            $(this).css('background-color', 'rgba(0, 115, 153, 1)');
          }, function() {
            $(this).css('background-color', 'rgb(0, 153, 204)');
          });
          // Change split-Button to enabled
          $('#split').css('background-color', 'rgba(186, 74, 94, 1)');
          $('#split').hover(function() {
            $(this).css('background-color', 'rgba(152, 58, 75, 1)');
          }, function() {
            $(this).css('background-color', 'rgba(186, 74, 94, 1)');
          });
          $('#split').prop('disabled', false);
          // Change reset-Button to disabled
          $('#reset').css({
            'background-color': 'rgba(51, 51, 51, 0.3)',
            'color': '#fff'
          });
          $('#reset').prop('disabled', true);
          // start time and lap counters
          startCounters();
        } else {
          // Set app mode to 0 (off)
          mode = 0;
          jalan = 0;
          // Change stop-Button to start-Button
          $('#startStop').html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span><span class="sr-only">Stop</span>');
          $('#startStop').css("background-color", "rgb(0, 204, 153)");
          $('#startStop').hover(function() {
            $(this).css('background-color', 'rgba(0, 153, 115, 1)');
          }, function() {
            $(this).css('background-color', 'rgb(0, 204, 153)');
          });
          // Change split-Button to disabled
          $('#split').css('background-color', 'rgba(186, 74, 94, 0.3)');
          $('#split').prop('disabled', true);
          // Change reset-Button to enabled
          $('#reset').css({
            'background-color': 'rgba(189, 188, 186, 1)',
            'color': 'rgba(51, 51, 51, 1)'
          });
          $('#reset').hover(function() {
            $(this).css({
              'background-color': 'rgba(51, 51, 51, 1)',
              'color': '#fff'
            });
          }, function() {
            $(this).css({
              'background-color': 'rgba(189, 188, 186, 1)',
              'color': 'rgba(51, 51, 51, 1)'
            });
          });
          $('#reset').prop('disabled', false);
          // Stop counters
          clearInterval(loop);
        }
      }
    });

    // When reset-Button is clicked:
    $('#reset').click(function() {
      // empty table and hide it:
        $('#laptimes').empty();
        $('#tables').css('display', 'none');
        // reset timers:
        timeCounter = 0;
        lapCounter  = 0;
				updateTime();
				items     = [];
				// listWaktu = [];
    });

    // When split-Button is clicked:
    $('#split').click(function() {
      // if mode = 1 (on)
      var nik = $('#nik').val();
      var proses = $('#proses').val();
      var mesin = $('#mesin').val();
      var wip = $('#wip').val();
      var tc = $('#tc').val();
      var burst = $('#burst').val();
      if (nik=='' || proses=='' || mesin =='' || wip == '' || tc == '') {
        $("#alert_error").trigger("click", 'Tolong Inputan Di Lengkapi Semua');
      }else{
        if (mode = 1) {
          // stop loop
          clearInterval(loop);
          // reset lapCounter
          lapCounter = 0;
          // Get current time and date:
          // Create a date object with new Date() constructor and assign it as value to 'date'
          date = new Date();
          // Display table for lap details:
          $('#tables').css('display', 'table');
          // print lap details:
          addLapDetails(date);
          // restart counters:
          startCounters();
        }
      }
    })

    // Functions:
    // starts time and lap counters and updates time on webpage
    function startCounters() {
      loop = setInterval(function() {
        timeCounter++;
        lapCounter++;
        updateTime();
      }, 10)
    }
    // converts counters to hours, minutes, seconds and milliseconds
    // and updates the times on the webpage
    function updateTime() {
      // Conversion ms -> h:min:s:ms
      // -> /Playground/time-format-converter.js
      // Here: conversion to Centiseconds
      // as 1 ms resolution might result in accuracy issues
      timeCsec = timeCounter % 100;
      // timeHours = Math.floor((timeCounter / 100) / 3600);
      // timeMinutes = Math.floor(((timeCounter / 100) % 3600) / 60);
      timeSeconds = Math.floor(((timeCounter / 100) % 3600));

      // Update times on webpage:
      // $('#timeHour').text(format(timeHours));
      // $('#timeMinute').text(format(timeMinutes));
      $('#timeSec').text(format(timeSeconds));
      $('#timeCsec').text(format(timeCsec));

      // Same applies to laptimes:
      lapCsec = lapCounter % 100;
      // lapHours = Math.floor((lapCounter / 100) / 3600);
      // lapMinutes = Math.floor(((lapCounter / 100) % 3600) / 60);
      lapSeconds = Math.floor(((lapCounter / 100) % 3600));

      // Update lap times on webpage:
      // $('#lapHour').text(format(lapHours));
      // $('#lapMinute').text(format(lapMinutes));
      $('#lapSec').text(format(lapSeconds));
      $('#lapCsec').text(format(lapCsec));
    }

    // Format numbers to 2 digits:
    function format(number) {
      if (number < 10) {
        return '0' + number;
      } else {
        return number;
      }
    };

    // Prints lap details into laptime div:
    function addLapDetails(clock) {
      var listDetail = [];
      // lapNumber++;
      // Lap#, Interval, Total, Time Recorded
      var interval =
        // '<span>' + format(lapHours) + '</span>' +
        // ':<span>' + format(lapMinutes) + '</span>' +
        '<span>' + format(lapSeconds) + '</span>' +
        '.<span>' + format(lapCsec) + '</span>';
      var total =
        // '<span>' + format(timeHours) + '</span>' +
        // ':<span>' + format(timeMinutes) + '</span>' +
        '<span>' + format(timeSeconds) + '</span>' +
        '.<span>' + format(timeCsec) + '</span>';

      // Convert current time to date-time-string:
      var date = clock.toLocaleDateString('fr-CA');
      var recorded = clock.toLocaleTimeString('en-US', { hour12: false });

			// Create table rows from variables:

			// var index = lapNumber-1;
      // var lapTimeDetails =
      //   '<tr> <td># ' + lapNumber + '</td>' +
      //   '<td>' + interval + '</td>' +
      //   '<td>' + total + '</td>' +
      //   '<td>' + date + ' ' + recorded + '</td>' +
			// 	'<td> <button type="button" id="delete_waktu_'+index+'" class="btn btn-danger btn-delete_waktu" onclick="return deleteWaktu('+index+')" ><i class="fa fa-trash-o" /></button> </td></tr>';
      // // Print lap details to the beginning of the list:  
      // $(lapTimeDetails).prependTo('#laptimes');

			
			var sum_waktu = format(timeSeconds) + '.' + format(timeCsec);
			var waktu     = format(lapSeconds) + '.' + format(lapCsec);
			var tanggal   = date + ' ' + recorded;
      // listDetail.push(lapNumber);
      // listDetail.push(waktu);
      // listDetail.push(tanggal);
			// listWaktu.push(listDetail);
			tambahitem(waktu,sum_waktu,tanggal);
		}
		
		
		$('.btn-delete_waktu').on('click', deleteWaktu);
		
		
		function deleteWaktu(){
			var id = parseInt($(this).data('id'), 10);
				
			console.log(listWaktu[id]);
			
		  items.splice(id, 1);
			// $('#in_cycle_'+id).prop("disabled", false);
			// $('#cekcrit_'+id).prop("disabled", false);
			// $('#edit_cycle_'+id).hide();
			// $('button').toggle(true);  //shows button
			// $('#save_cycle_'+id).css("display","inline");
			render();
		}
	
		function echo() {
			console.log(listWaktu);
			console.log("s");
		}



	var items = JSON.parse($('#items').val());

	function render() {
    getIndex();
    // setBoolean();
    $('#items').val(JSON.stringify(items));
    var tmpl = $('#items-table').html();

    // console.log(tmpl);
    
    Mustache.parse(tmpl);
    var data = { item: items };
    var html = Mustache.render(tmpl, data);
    $('#tbody-items').html(html);
    bind();
   
	}
	
	
  function getIndex(){
    for (idx in items) {
      items[idx]['_id'] = idx;
      items[idx]['no'] = parseInt(idx) + 1;
    }
	}
	
  function bind(){
    // $('#prosesId').on('change', tambahitem);
    $('.btn-delete-item').on('click', deleteWaktu);
    // $('.btn-edit-item').on('click', editCycleItem);
    // $('.btn-save-item').on('click', saveCycleItem);

    // $('.input-new').keypress(function (e) {
    //   if (e.keyCode == 13) {
    //     e.preventDefault();
    //     tambahitem();
    //     document.getElementById("prosesName").focus();
    //   }
    // });
	}

  function tambahitem(a,b,c) {
		var input = {
			'interval': a,
			'waktu': b,
			'record': c,
		}; 
		items.push(input);
		render();
  }

	
  // }); 
</script>
