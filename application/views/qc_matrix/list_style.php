<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="col-md-2">
      <br>

    </div>
    <div class="panel panel-default">
      <input type="hidden" value="<?php echo $record ?>" id="line_id">
        <div class="panel-body">
            <div style="overflow-x:auto;overflow-y:auto">
                <table id="table-po" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="20px">NO</th>
                            <th>STYLE</th>
                            <th width="20px">ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
         var table =$('#table-po').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Qc_matrix/list_style_ajax",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
             'line_id':$('#line_id').val(), },

                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "style" },
                  { "data": "action",'sortable' : false },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
    });
    function pilih(style) {
      $('#style').val(style);
      $('#proses').focus();
      $('#myModal').modal('hide');
      $('.search_header').trigger("submit");
    }
</script>