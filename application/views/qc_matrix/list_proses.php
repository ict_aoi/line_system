<!-- <style>
#myModal {
  overflow-x: hidden;
  overflow-y: auto;
}
</style> -->
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <div class="col-md-8 pull-right alert alert-success fade in">
              <button data-dismiss="alert" class="close close-sm" type="button">
                  <i class="icon-remove"></i>
              </button>
              <strong>INFORMASI</strong><br> Untuk Menampilkan Pencarian Proses Tekan Enter  <br>
            </div>      
              <table id="table-defect" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th class="text-center" width="20px">NO</th>
                          <th>PROSES NAME</th>
                          <th width="20px">ACTION</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            <div class="row text-center">
              <br>
              <input type="hidden" name="list_proses_array" id="list_proses_array">
              <input type="hidden" name="nama_proses_array" id="nama_proses_array">
              <a onclick='return pilih()' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Pilih</a>
              <br>
              &nbsp
            </div>
              
          </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>

<script type="text/javascript">

      var list_proses = new Array();
      var list_nama = new Array();
    $(document).ready(function () {

          var table =$('#table-defect').DataTable({
            "processing": true,
						"serverSide": true,
						"paging": false,
						// "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
              "url": "Qc_matrix/listproses",
              "dataType": "json",
              "type": "POST",
              "data":{
                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                'style':$('#style').val(),'line_id':$('#line_id').val(),'round':$('#round').val()
              }
            },
            "columns": [
                      {"data" : null, 'sortable' : false, 'orderable' : true},
                      { "data": "prosesname",'sortable' : false, 'orderable' : true },
                      { "data": "action",'sortable' : false },
                  ],
                  fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
                  ,
          });
          $('#table-defect_filter input').unbind();
          $('#table-defect_filter input').bind('keyup', function(e) {
              if (e.keyCode == 13 || $(this).val().length == 0 ) {
                  table.search($(this).val()).draw();
              }
              /*if ($(this).val().length == 0 || $(this).val().length >= 4) {
                  table.search($(this).val()).draw();
              }*/
          });
    });

    function selectedApproval()
    {
      $("input[type=checkbox]:checked").each(function (){
        // console.log(this.value);
        list_proses.push(this.id);
        list_nama.push(this.value);
      });
      $('#list_proses_array').val(list_proses);
      $('#nama_proses_array').val(list_nama);
    }

    function pilih() {

      selectedApproval()

      var list_proses_html = $('#list_proses_array').val();
      var list_nama_html   = $('#nama_proses_array').val();

      // console.log(list_proses);
      // console.log(list_nama);
      if (!list_proses_html) {
        $("#alert_warning").trigger("click", 'Tidak ada item yang dipilih');
      }
      
      var url_loading = $('#loading_gif').attr('href');

      var nama = '';
      list_nama.forEach(function (item, index){
        nama = nama + item + " + ";
      });
      // nama = nama.replace(",", " + ");
      // console.log(nama);

      var string = ""+nama.slice(0, -3)+"";
      // console.log("ini string = "+string);
      //penting
      $('#proses').val(string);
      $('#list_proses').text(string);

      // console.log($('#proses').val());
      $('#proses-hid').val(list_proses);

      $("#myModal").modal('show');
      var url_loading = $('#loading_gif').attr('href');
      $('.modal-title').text("Pilih Mesin");
      $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
      $('.modal-body').load('qc_matrix/mesin_view?id='+list_proses);
      $('#refresh').trigger("click");
      $('#reset').trigger("click");

    

    }

    // function getcolors(header_id, sewer) {
    //   var url_loading = $('#loading_gif').attr('href');
    //   var data = 'idHeader='+header_id+'&nik='+sewer+'&proses_id='+$('#proses-hid').val();

    //     $.ajax({
    //         url:'Qc_inspect/getColor',
    //         data:data,
    //         type:'GET',
    //         dataType: "JSON",
    //         beforeSend: function () {
    //           $.blockUI({
    //               message: "<img src='" + url_loading + "' />",
    //               css: {
    //                 backgroundColor: 'transparant',
    //                 border: 'none',
    //               }
    //             });
    //           },
    //         success:function(response){
    //           $.unblockUI();
    //             $('.panel-heading').css('background-color',response['color']);
    //             $('#grade_before').val(response['grade']).trigger('change');
    //         },
    //         error:function(response){
    //             if (response.status==500) {
    //               $.unblockUI();
    //               $("#alert_warning").trigger("click", 'Kontak ICT');
    //             }
    //         },
    //         timeout: 15000
    //     })

    //   $("#myModal").modal('hide');
    //   // return false;
    // }

</script>
