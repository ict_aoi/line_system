<!-- <style>
#myModal {
  overflow-x: hidden;
  overflow-y: auto;
}
</style> -->
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
          <div style="overflow-x:auto;overflow-y:auto">
            <div class="col-md-8 pull-right alert alert-info fade in">
              <button data-dismiss="alert" class="close close-sm" type="button">
                  <i class="icon-remove"></i>
              </button>
              <strong>INFORMASI</strong><br> Untuk Menampilkan Pencarian Mesin Tekan Enter  <br>     
              <input type="hidden" value="<?=$proses_id; ?>" id="proses_id" name="proses_id">
            </div>
              <table id="table-mesin" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th class="text-center" width="20px">NO</th>
                          <th>NAMA MESIN</th>
                          <th width="20px">ACTION</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
          </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
         var table =$('#table-mesin').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Qc_matrix/listmesin",
             "dataType": "json",
             "type": "POST",
             "data":{
              '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
              'style':$('#style').val(),
              'line_id':$('#line_id').val(),
              'round':$('#round').val(),
              'proses_id':$('#proses_id').val(),
            }
                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "nama_mesin",'sortable' : false },
                  { "data": "action",'sortable' : false },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
              ,
        });
         $('#table-defect_filter input').unbind();
          $('#table-defect_filter input').bind('keyup', function(e) {
              if (e.keyCode == 13 || $(this).val().length == 0 ) {
                  table.search($(this).val()).draw();
              }
              /*if ($(this).val().length == 0 || $(this).val().length >= 4) {
                  table.search($(this).val()).draw();
              }*/
          });
    });
    function pilih(proses_id,id,name) {
      var url_loading = $('#loading_gif').attr('href');
      // var sewer       = $('#sewer-hid').val();
      var header_id = $('#header_id').val();
      var line_id   = $('[name=line_id]').val();


      // $('#lastproses-hid').val(last);
      $('#mesin').val(name);
      $('#mesin-hid').val(id);

      // $.ajax({
      //       url:'Qc_matrix/loadsewerproses',
      //       data:{proses_id:proses_id, line_id:line_id},
      //       type:'GET',
      //       dataType: "JSON",
      //       beforeSend: function () {
      //         $.blockUI({
      //             message: "<img src='" + url_loading + "' />",
      //             css: {
      //               backgroundColor: 'transparant',
      //               border: 'none',
      //             }
      //           });
      //         },
      //       success:function(response){
      //         $.unblockUI();

              // if (response.length >=1) {

                  $("#myModal").modal('show');
                  var url_loading = $('#loading_gif').attr('href');
                  $('.modal-title').text("Pilih Nama Sewer");
                  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
                  $('.modal-body').load('qc_matrix/viewsewerproses?proses_id='+proses_id+'&header_id='+header_id+'&line_id='+line_id);
                  $('#refresh').trigger("click");
                  $('#reset').trigger("click");

                  return false;

              // }else if (response.length == 0) {
              //   $('#nik').val('');
              //   $('#sewer').val('');
              //   $("#myModal").modal('hide');
              //   $('#nik').focus();
              //   $('#refresh').trigger("click");
              //   $('#reset').trigger("click");
              //   return false;
              //   // getcolors(header_id,value['sewer_nik']);
              // }
    //         }
    //     })

    }

    // function getcolors(header_id, sewer) {
    //   var url_loading = $('#loading_gif').attr('href');
    //   var data = 'idHeader='+header_id+'&nik='+sewer+'&proses_id='+$('#proses-hid').val();

    //     $.ajax({
    //         url:'Qc_inspect/getColor',
    //         data:data,
    //         type:'GET',
    //         dataType: "JSON",
    //         beforeSend: function () {
    //           $.blockUI({
    //               message: "<img src='" + url_loading + "' />",
    //               css: {
    //                 backgroundColor: 'transparant',
    //                 border: 'none',
    //               }
    //             });
    //           },
    //         success:function(response){
    //           $.unblockUI();
    //             $('.panel-heading').css('background-color',response['color']);
    //             $('#grade_before').val(response['grade']).trigger('change');
    //         },
    //         error:function(response){
    //             if (response.status==500) {
    //               $.unblockUI();
    //               $("#alert_warning").trigger("click", 'Kontak ICT');
    //             }
    //         },
    //         timeout: 15000
    //     })

    //   $("#myModal").modal('hide');
    //   // return false;
    // }

</script>
