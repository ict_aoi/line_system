<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h6 class="panel-title">
			<div class="col-md-2">
				Actual Production Time
			</div>
		</h6>
	</div>

	<br>
	<!-- <button id="start" class="col-md-12 btn btn-warning btn-lg btn-block">Start Job</button> -->
	<br>
	<div class="panel-body">
		<div class=" alert alert-danger fade in">
			<button data-dismiss="alert" class="close close-sm" type="button">
				<i class="icon-remove"></i>
			</button>
			<strong>PERINGATAN</strong><br> 
			Anda mempunyai pekerjaan belum selesai di  <b> <?= $batch['line_name']; ?>, STYLE <?= $batch['style']; ?> </b>, 
			Silahkan untuk diselesaikan terlebih dahulu
		</div>
		<!-- <form class="frmsave" method="POST"> -->
			<input type="hidden" value="<?= $batch['id']; ?>" name="batch_id" id="batch_id">
			<button type="button" id="close_job" class="btn btn-warning btn-lg btn-block">Closed Job</button>
		<!-- </form> -->
	</div>
</div>

<script>

	var url_loading = $('#loading_gif').attr('href');
	$(document).ready(function() {
		$('#close_job').click(function() {
			// console.log(panjang);
			var batch_id = $('#batch_id').val(); 
			var data = 'batch_id='+batch_id;

			swal({
				title: "PERHATIAN!!!",
				text: "Apakah Anda Yakin Untuk Close Batch?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((wilsave) => {
				if (wilsave) {
					$.ajax({
						url: 'qc_matrix/close_matrix_job',
						type: "GET",
						data: data,
						// data: {
						//   data : data,
						//   array : listWaktu,
						// },
						contentType: false,
						cache: false,
						processData: false,
						beforeSend: function() {
							$.blockUI({
								message: "<img src='" + url_loading + "' />",
								css: {
									backgroundColor: 'transaparant',
									border: 'none',
								}
							});
							// console.log(data);
						},
						success: function(value) {
							var base_url = '<?=base_url();?>'+'/qc_matrix';
							if(value=='success'){

								$("#alert_success").trigger("click", 'Batch Job Closed');	
								window.location.replace(base_url);
								$.unblockUI();
							}
							else {
								$.unblockUI();
								$("#alert_error").trigger("click", 'Info ICT');
							}
						},
						error: function(data) {
							$.unblockUI();
							$("#alert_info").trigger("click", 'Contact Administrator');
						},
						// timeout: 15000
					});
				}
			});
			return false;
		})
	});
</script>
<!-- <a href="</?php echo base_url('qc_matrix') ?>" id="logo" class="hidden"></a>

<script type="text/javascript" src="</?php echo base_url(); ?>assets/resource/qc_matrix.js"></script> -->
