<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Inline - System">
    <meta name="author" content="ICT">
    <meta name="keyword" content="Inline - System">
    <link rel="shortcut icon" href="<?php base_url() ?>assets/img/favicon.jpg">

    <title><?=$title;?> - Inline System </title>

    <!-- Bootstrap CSS -->
    <link href="<?=site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?=Base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?=Base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/owl.carousel.css" type="text/css">
    <link href="<?=Base_url();?>assets/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <!-- <link rel="stylesheet" href="<?=Base_url();?>assets/css/fullcalendar.css"> -->
    <link href="<?=Base_url();?>assets/css/widgets.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <script src="<?=Base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.idle.min.js"></script>
    <script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.rowspanizer.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/plugins/mustache.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/resource/notification.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/datepicker/css/bootstrap-datepicker.min.css');?>">
    <script type="text/javascript" src="<?=base_url('assets/plugins/datepicker/js/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?=Base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.numpad.js"></script>
    <script src="<?=Base_url();?>assets/js/keyboard.js"></script>
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/jquery.numpad.css">
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/keyboard.css">

  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="sidebar-closed">
      <!--header start-->

      <header class="header dark-bg">

            <!--logo start-->
            <?php if ($this->session->userdata('logged_in')=='qc'): ?>
              <a href="<?=base_url('Qc');?>" class="logo">LINE <span class="lite">System</span></a>
              <?php else: ?>
                <a href="<?=base_url('Folding');?>" class="logo">LINE <span class="lite">System</span></a>
            <?php endif ?>

            <!--logo end-->

            <div class="top-nav notification-row">
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    <?php if ($this->session->userdata('logged_in')=='folding'): ?>
                      <!-- menu request karton -->
                      <li id="task_notificatoin_bar" class="dropdown">
                       <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                           <span data-icon="&#x63"></span>
                       </a>
                       <ul class="dropdown-menu extended tasks-bar">
                           <div class="notify-arrow notify-arrow-blue"></div>
                           <li>
                             <a href="<?php echo 'https://pdc.bbi-apparel.com/bypass-request-carton/'.($this->session->userdata('line_id')) ?>">
                                 <span class="label label-success"><i class="fa fa-paper-plane"></i></span>
                                 Request Karton
                             </a>
                           </li>
                           <li>
                             <a href="<?php echo base_url('RequestKarton/report_receivekarton') ?>">
                                 <span class="label label-primary"><i class="fa fa-files-o"></i></span>
                                 Laporan Terima Karton
                             </a>
                           </li>
                           <li>
                             <a href="<?php echo base_url('RequestKarton/packing_list') ?>">
                                 <span class="label label-danger"><i class="fa fa-list-alt"></i></span>
                                 Packing List
                             </a>
                           </li>
                       </ul>
                                         </li>
                      <li id="task_notificatoin_bar" class="dropdown">

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span data-icon="&#xe02f"></i>
                        </a>
                        <ul class="dropdown-menu extended tasks-bar">
                            <div class="notify-arrow notify-arrow-blue"></div>
                            <li>
                                <?php
                                  if ($this->session->userdata('nik_adm')!=0) {
                                    echo '<a href="'.base_url('distribusi').'" >
                                              <span class="label label-primary"><i class="fa fa-check-square-o"></i></span>
                                              Receive Distribusi
                                          </a>';
                                  }else{
                                    echo '<a href="javascript:void(0)" id="login_distribution" >
                                              <span class="label label-primary"><i class="fa fa-check-square-o"></i></span>
                                              Receive Distribusi
                                          </a>';
                                  }
                                ?>
                            </li>
                            <li>
                                <?php
                                  if ($this->session->userdata('nik_adm')!=0) {
                                    echo '<a href="'.base_url('Distribusi/pindah_line').'" >
                                              <span class="label label-danger"><i class="icon_pin"></i></span>
                                              Pindah Loading
                                          </a>';
                                  }else{
                                    echo '<a href="javascript:void(0)" id="login_pindah" >
                                              <span class="label label-danger"><i class="icon_pin"></i></span>
                                               Pindah Loading
                                          </a>';
                                  }
                                ?>
                            </li>
                            <li>
                            <a href="<?php echo base_url('Distribusi/Report_Receive') ?>" id="login_pindah">
                                <span class="label label-success"><i class="fa fa-files-o"></i></span>
                                Laporan Receive Distribusi
                            </a>
                        </li>
                        </ul>
                    </li>
                    <?php endif ?>

                    <li class="dropdown">

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username"><?php echo ucwords($this->session->userdata('name'))?>(<?php echo $this->session->userdata('factory_name');?>)</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li>
                                <?php if ($this->session->userdata('logged_in')=='qc'): ?>
                                  <a href="<?=base_url('Auth/logout_qc');?>"><i class="icon_key_alt"></i> Log Out</a>
                                  <?php else: ?>
                                    <a href="<?=base_url('Auth/logout_folding');?>"><i class="icon_key_alt"></i> Log Out</a>
                                <?php endif ?>

                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>
      <!--main content start-->

      <section id="main-content">
          <section class="wrapper">

              <!-- page start-->
              <?php echo $contents ?>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section end -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body" style="overflow: auto;"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
<div class="hidden login_pop">
  <form  method="POST" action='<?=base_url('distribusi');?>' >
    <label>NIK</label>
    <input type="text" name="nik" placeholder="Scan NIK disini" class="form-control" autofocus autocomplete="off" required>
    <label></label>
    <button class="btn btn-success btn-flat btn-block submit">Login</button>
  </form>
  <div class="view_">

  </div>
</div>
<div class="hidden login_pindah_pop">
  <form  method="POST" action='<?=base_url('Distribusi/pindah_line');?>' >
    <label>NIK</label>
    <input type="text" name="nik"placeholder="Scan NIK disini" autofocus class="form-control" autocomplete="off" required>
    <label></label>
    <button class="btn btn-success btn-flat btn-block submit">Login</button>
  </form>
</div>
<a href="<?php echo base_url('assets/img/Spinner.gif') ?>" id="loading_gif" class="hidden"></a>
<button type="button" class="hidden" id="alert_info"></button>
<button type="button" class="hidden" id="alert_success"></button>
<button type="button" class="hidden" id="alert_error"></button>
<button type="button" class="hidden" id="alert_warning"></button>
    <!-- javascripts -->
    <!-- nice scroll -->
    <!-- <script src="<?=Base_url();?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.nicescroll.js" type="text/javascript"></script> -->
    <!-- charts scripts -->
    <script src="<?=Base_url();?>assets/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="<?=Base_url();?>assets/js/owl.carousel.js" ></script>
    <script src="<?=Base_url();?>assets/js/jquery.rateit.min.js"></script>


    <!--custome script for all page-->
    <!-- <script src="<?=Base_url();?>assets/js/scripts.js"></script> -->
    <script src="<?=Base_url();?>assets/js/jquery.autosize.min.js"></script>
    <script src="<?=Base_url();?>assets/js/morris.min.js"></script>
    <!-- <script src="<?=Base_url();?>assets/js/jquery.slimscroll.min.js"></script> -->
    <script>
      Mustache.tags = ['{%', '%}'];
      $('#login_distribution').click(function() {
        $('.view_').html('');
        var val = $('.login_pop').html();
        $('#myModal').modal({backdrop: 'static', keyboard: false});
        $('.modal-dialog').addClass('modal-md');
        $('.modal-title').text('Login Terima Loading');
        $('.modal-body').html(val);
      });
      $('#login_pindah').click(function() {
        $('.view_').html('');
        var val = $('.login_pindah_pop').html();
        $('#myModal').modal({backdrop: 'static', keyboard: false});
        $('.modal-dialog').addClass('modal-md');
        $('.modal-title').text('Login Pindah Loading');
        $('.modal-body').html(val);
      });
    </script>
  </body>
</html>
