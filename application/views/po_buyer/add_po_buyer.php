<form class="add_pobuy" method="POST">
    <label>PO BUYER</label>
    <input type="text" name="po" placeholder="Masukan PO BUYER" autocomplete="off" class="form-control po" required>
    
    <button type="submit" name="save" class="btn btn-danger btn-sm bsave"><i class="fa fa-save"></i> SIMPAN</button>
</form>
<script>
    $(document).ready(function(){
        $('.add_pobuy').submit(function(e){
            e.preventDefault();
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Cron/save_po',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    else if(result.status==3){
                        $("#alert_warning").trigger("click", result.pesan);
                    }
                }
            })
        return false;
        });
    });
</script>