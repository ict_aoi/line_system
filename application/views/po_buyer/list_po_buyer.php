<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <br>
        <div class="col-sm-2">
            <button id="tambah" class="btn btn-success" onclick="return tambah()">Tambah</button>
        </div>
        <br>
        <div class="panel-body">
            <table id="table-po" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <!-- <th width="20px">NO</th> -->
                        <th>PO BUYER</th>
                        <th>TANGGAL SINKRON</th>
                        <th>LC DATE</th>
                        <th>CREATE DATE</th>
                        <th>UPDATE DATE</th>
                        <th>DELETE DATE</th>
                        <th>PO BARU</th>
                        <th width="50px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
</div>
<button type="button" class="hidden" id="refresh"></button>
<script type="text/javascript">
    $(document).ready(function () {
        var table =$('#table-po').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
            "url": "list_po",
            "dataType": "json",
            "type": "POST",
            "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                        },
        "columns": [
                // { "data": null, 'sortable' : false},
                { "data": "poreference"},
                { "data": "requirement_date" },
                { "data": "kst_lcdate" , 'sortable' : false},
                { "data": "create_date" },
                { "data": "update_date" },
                { "data": "delete_date" },
                { "data": "po_new" },
                { "data": "action" },
            ],
            // fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
        $('#table-po_filter input').unbind();
        $('#table-po_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-po').DataTable().ajax.reload();
        });
    });
    function tambah(){       
        $("#myModal").modal('show');
        $('.modal-title').text("Add PO Buyer");
        $('.modal-body').html('<center>Loading..</center>');
        $('.modal-body').load('add_po');
        return false;
    }

    function reduce(po) {
        var url_loading = $('#loading_gif').attr('href');
        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Reduce PO');
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:170px;margin-right=3000px;width=100px'>");
        modal.children('.modal-body').load('reduce_po?po='+po);
        return false;
    }

    function reroute(po) {
        var url_loading = $('#loading_gif').attr('href');
        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Reroute PO');
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:170px;margin-right=3000px;width=100px'>");
        modal.children('.modal-body').load('reroute_po?po='+po);
        return false;
    }
    function hapus(id) {
        swal({
        title: "Apakah anda yakin?",
        text: "Data akan di hapus?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((wildelete) => {
        if (wildelete) {
            $.ajax({
                url:'delete_user?id='+id,
                success:function(value){
                    if(value=='sukses'){
                        $("#alert_success").trigger("click", 'Hapus data Berhasil');
                        $('#table-user').DataTable().ajax.reload();
                    }else{
                        $("#alert_error").trigger("click", 'Hapus data gagal');
                    }
                }

            });
            
        }
        });
    }
</script>