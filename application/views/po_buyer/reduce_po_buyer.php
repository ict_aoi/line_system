<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>


<!-- <form class="sinkron" method="POST"> -->
    <label>PO BUYER</label>
    <input type="text" id="po" value="<?php echo $po_buyer;?>" readonly>
    <!-- <input type="text" name="po" value="<$po_buyer;?>" autocomplete="off" class="form-control" readonly > -->

    <div class="row">    
        <div class="col-md-6">
            <table id="table-distribusi" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>STYLE</th>
                        <th>PO BUYER</th>
                        <th>SIZE</th>
                        <th>STATUS</th>
                        <th>QTY</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                            $no = 1;
                            foreach ($distribusi as $d) {
                        ?>
                            <td><?php echo $d->style ?></td>
                            <td><?php echo $d->poreference ?></td>
                            <td><?php echo $d->size ?></td>
                            <td><?php echo $d->status ?></td>
                            <td><?php echo $d->total ?></td>
                    </tr>
                        <?php
                            }
                        ?>
                </tbody>
            </table>
        </div>

        <div class="col-md-6">
            <table id="table-erp" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>STYLE</th>
                        <th>PO BUYER</th>
                        <th>SIZE</th>
                        <th>QTY</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                            $no = 1;
                            foreach ($erp as $e) {
                        ?>
                            <td><?php echo $e->style ?></td>
                            <td><?php echo $e->poreference ?></td>
                            <td><?php echo $e->size ?></td>
                            <td><?php echo number_format($e->qtyordered,0,',',',') ?></td>
                    </tr>
                        <?php
                            }
                        ?>
                </tbody>
            </table>
        </div>
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">

        </label> 
    </div>
    <button type="btn" id="sinkron-btn" class="btn btn-success"><i class="fa fa-retweet"></i> SINKRON</button>
    <button type="btn" id="cancel-btn" class="btn btn-danger"><i class="fa fa-trash-o"></i> CANCEL ORDER</button>
<!-- </form> -->
<script>
    $(document).ready(function(){
        $('#sinkron-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po='+$('#po').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/sinkron_po',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    // else if(result.status==3){
                    //     $("#alert_warning").trigger("click", result.pesan);
                    // }


                    // if (response=='sukses') {
                    //      $("#myModal").modal('hide');
                    //      $("#alert_success").trigger("click", 'Sinkron Berhasil');
                    //      $('#refresh').trigger("click");
                    // }
                }
            })
            return false;
        });
        $('#cancel-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po='+$('#po').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/cancel_po',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                    // else if(result.status==3){
                    //     $("#alert_warning").trigger("click", result.pesan);
                    // }


                    // if (response=='sukses') {
                    //      $("#myModal").modal('hide');
                    //      $("#alert_success").trigger("click", 'Sinkron Berhasil');
                    //      $('#refresh').trigger("click");
                    // }
                }
            })
            return false;
        });
    });
</script>