<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>


<!-- <form class="sinkron" method="POST"> -->
    <div class="row">
        <div class="col-md-6">
            <label>PO BUYER LAMA</label>
            <input type="text" id="po_lama" value="<?php echo $po_buyer;?>"class="form-control" readonly>
        </div>
        <div class="col-md-6">
            <label>PO BUYER BARU</label>
            <input type="text" id="po_baru" autocomplete="off" class="form-control">
        </div>
    </div>
    <div class="row">    
        <!-- <div class="col-md-6"> -->
            <table id="table-distribusi" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>STYLE</th>
                        <th>PO BUYER</th>
                        <th>SIZE</th>
                        <th>STATUS</th>
                        <th>QTY</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                            $no = 1;
                            foreach ($distribusi as $d) {
                        ?>
                            <td><?php echo $d->style ?></td>
                            <td><?php echo $d->poreference ?></td>
                            <td><?php echo $d->size ?></td>
                            <td><?php echo $d->status ?></td>
                            <td><?php echo $d->total ?></td>
                    </tr>
                        <?php
                            }
                        ?>
                </tbody>
            </table>
        <!-- </div> -->
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">

        </label> 
    </div>
    <button type="btn" id="update-btn" class="btn btn-danger"><i class="fa fa-upload"></i> REROUTE</button>
<!-- </form> -->
<script>
    $(document).ready(function(){
        $('#update-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po_lama='+$('#po_lama').val()+'&po_baru='+$('#po_baru').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/update_po_baru',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                }
            })
            return false;
        });
        $('#cancel-btn').click(function(){
            // var po = $("#po").val();
            var data = 'po='+$('#po').val();
            
            $.ajax({
                url:'<?=base_url();?>Cron/cancel_po',
                type: "POST",
                data: data,
                success: function(response){

                    var result = JSON.parse(response);

                    if(result.status==1){
                        $("#myModal").modal('hide');
                        $("#alert_success").trigger("click", result.pesan);
                        $('#refresh').trigger("click");
                    }
                    else if(result.status==2) {
                        $("#alert_error").trigger("click", result.pesan);
                    }
                }
            })
            return false;
        });
    });
</script>