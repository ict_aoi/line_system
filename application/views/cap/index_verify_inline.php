<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
      <div class="panel-heading">
            <i class="fa fa-wrench"></i> List Verify Inline
      </div>
        <br>
        <div class="panel-body">
            <table id="table-capinline" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">NO</th>
                        <th width="80px">LINE</th>
                        <th width="30px">ROUND</th>
                        <th width="80px">NAMA SEWER</th>
                        <th width="100px" class="text-center">NAMA PROSES</th>
                        <th class="text-center">NAMA DEFECT</th>
                        <th class="text-center">PENYEBAB UTAMA</th>
                        <th class="text-center">AKAR MASALAH</th>
                        <th class="text-center">TINDAKAN</th>
                        <th width="50px">ACTION</th>
                        <th width="30px">STATUS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="<?php echo base_url('assets/img/Spinner.gif') ?>" id="loading_gif" class="hidden"></a>
</div>
<script type="text/javascript">

	var url_loading = $('#loading_gif').attr('href');

    $(document).ready(function () {
         var table =$('#table-capinline').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
             "url": "showVerifyinline",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "line" },
                  { "data": "round" },
                  { "data": "nama_sewer",'sortable' : false },
                  { "data": "proses_name",'sortable' : false },
                  { "data": "nama_defect",'sortable' : false },
                  { "data": "major_couse",'sortable' : false },
                  { "data": "root_couse",'sortable' : false },
                  { "data": "action",'sortable' : false },
                  { "data": "aksi",'sortable' : false },
                  { "data": "status",'sortable' : false },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
        $('#table-user_filter input').unbind();
        $('#table-user_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-capinline').DataTable().ajax.reload();
        });
    });
    function verify(id_detail) {
      $.ajax({
            type:'GET',
            url :'verifyInline',
			data:'id_detail='+id_detail,
			beforeSend: function () {
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
						backgroundColor: 'transparant',
						border: 'none',
					}
				});
			},
            success:function(response){
				
				var dtable = $('#table-capinline').dataTable().api();
                if (response==1) {
					
					$.unblockUI();
					// $('#table-capinline').DataTable().ajax.reload();
					dtable.draw();
                }else{
					
					$.unblockUI();
                    $("#alert_info").trigger("click", 'CAP Expired');
					// $('#table-capinline').DataTable().ajax.reload();
					dtable.draw();
                }
            }
        });
        
    }
    function perbaiki(id_detail) {
      var url_loading = $('#loading_gif').attr('href');
      var modal = $('#myModal > div > div');
      $('#myModal').modal('show');
      modal.children('.modal-header').children('.modal-title').html('Perbaiki CAP');
      modal.parent('.modal-dialog').addClass('modal-lg');
      modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
      modal.children('.modal-body').load('showrepairinline?id='+id_detail);
      return false;
    }


</script>
