<script src="<?=Base_url();?>assets/plugins/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.countdown.min.js"></script>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa fa-bars"> CORRECTIVE ACTION</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">CAP ENDLINE</a></li>
        <li><i class="fa fa-bars"></i>Create Cap Endline</li>
      </ol>
    </div>
  </div>
</div>
<div class="col-lg-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> LIST LINE TOP 3
        </div>
        <div class="panel-body">
            <input type="hidden" id="time" value="<?php echo $record['sewing_deadline_date'] ?>">
            <div class="row">
              <div class="col-md-2 pull-right">
                <div id="getting-started"></div>
            </div>
            </div>
            <!-- <div id="result"></div> -->
            <div class="form-group">
                <div class="row">
                  <div class="col-md-4">

                    <label>Dari Line</label>
                    <select name='fromline' class='form-control fromline' value=''>
                      <option value='' class='form-control'>Pilih</option>
                    <?php foreach ($line->result() as $key => $l): ?>
                      <option value="<?=$l->no_urut?>"><?php echo $l->line_name;?></option>
                    <?php endforeach ?>
                    </select>

                  </div>

                  <div class="col-md-4">
                    <label>Sampai Line</label>
                    <select name='fromline' class='form-control toline' value=''>
                      <option value='' class='form-control'>Pilih</option>
                    <?php foreach ($line->result() as $key => $l): ?>
                      <option value="<?=$l->no_urut?>"><?php echo $l->line_name;?></option>
                    <?php endforeach ?>
                    </select>

                  </div>
                  

                </div>
                
              </div>
             
              <div class="row">
                <div class="col-md-2">
                    <button type="button" class="btn btn-success tampil">Tampilkan</button>
                  </div>
              </div>
            <div style="overflow-x:auto;overflow-y:auto">
            <table id="table-top" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">No</th>
                        <th width="20px">LINE</th>
                        <th width="20px">WFT</th>
                        <th width="20px">Jam</th>
                        <th>Nama Defect</th>
                        <th>Total</th>
                        <th width="50px">ACTION</th>
                        <th>Status</th>

                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
          </div>
        </div>

    </div>
</div>

<!-- MODAL -->
    <div id="capModal" role="dialog" class="modal fade">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="panel no-border">
            <div class="panel-heading">
              <div class="pull-right">
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><i class="fa fa-times" style="font-size:24px;"></i></button>
              </div>
              <div class="panel-title"><i class="fa fa-external-link-square"></i> CREATE CORRECTIVE ACTION</div>
              <div class="export-options-outsider-container export-options pull-right"></div>
              <div class="clearfix"></div>
            </div>

            <div class="panel-body">
              <div class="pull-center"><div id="getting-started-modal" class="label label-danger"></div></div>
                <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                        <!-- <div class="panel-heading"> -->

                        <!-- </div> -->
                          <div id="result">
                              <form class="form-horizontal cap_create" method="POST">
                                <div class="form-group">
                                      <label class="col-sm-2 control-label" for="form-field-2" >

                                      </label>
                                  <div class="col-sm-9">
                                    <input type="hidden" id="id_detail"  name="id_detail">
                                    <input type="hidden" id="defect_id" name="defect_id" ">
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-4">
                                          <label>STYLE</label>
                                          <input type="text" name="style" id="style" class="form-control" readonly>
                                        </div>

                                        <div class="col-md-4">
                                          <label>LINE</label>
                                          <input type="text" name="line_name" id="line_name" class="form-control" readonly>

                                        </div>
                                        <div class="col-md-4">
                                          <label>JAM</label>
                                          <input type="text" name="jam" id="jam" class="form-control" readonly>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="form-field-3" >
                                    JUDUL
                                  </label>
                                  <div class="col-sm-9">
                                    <input type="text" name="title" id="title" class="form-control" readonly>
                                  </div>
                                </div>
                                <div class="form-group">
                                      <label class="col-sm-2 control-label" for="form-field-2" >

                                      </label>
                                  <div class="col-sm-9">
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label>NIK</label>
                                          <input type="text" name="nik" id="nik" class="form-control" disabled>
                                        </div>

                                        <div class="col-md-6">
                                          <label>NAMA</label>
                                          <input type="text" name="nama" id="nama" class="form-control" readonly>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="form-field-4" >
                                    KATEGORI MASALAH
                                  </label>
                                 <div class="col-sm-9">
                                    <?php echo form_dropdown('major_couse', array(''=>'--Pilih--','man' => 'Man', 'machine' => 'Machine','method'=>'Method','material'=>'Material'), null, "class='form-control major_couse' id='major_couse' required disabled"); ?>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="form-field-3" >
                                    AKAR MASALAH
                                  </label>
                                  <div class="col-sm-9">
                                    <textarea type="text" name="root_couse" id="root_couse" placeholder="Masukan Akar Masalah" required class="form-control root_couse" cols="50" rows="" disabled></textarea>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="form-field-3" >
                                    ACTION
                                  </label>
                                  <div class="col-sm-9">
                                    <textarea type="text" name="action" id="action" placeholder="Masukan Action" required class="form-control action" cols="50" rows="" disabled></textarea>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block save" id="save" disabled><i class="fa fa-save"></i> SIMPAN</button>
                                </div>
                              </form>
                          </div>
                          </div>

                      </div>
                  </div>
                </div>
            </div><!-- END PANEL BODY -->

            <!-- <div class="panel-footer text-center">
              <button type="button" class="btn btn-primary" onclick="submitDaily()" id="btnSimpanDaily">Simpan</button>
              <button type="button" data-dismiss="modal" class="btn btn-danger">Kembali</button>
            </div> -->

          </div>
        </div>
      </div>
    </div>
<!-- MODAL -->


<button type="button" class="hidden" id="refresh"></button>

<script type="text/javascript">
    var getting_value = 1;

     var time = $('#time').val();
      var dt = new Date();

      var dd = dt.getDate();
      if (dd<10) {
          dd='0'+dd;
      }
      var now = dt.getFullYear()+"-"+(dt.getMonth() + 1)+"-"+dd+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
  $(function () {

      $("#getting-started, #getting-started-modal").countdown($('#time').val(), function(event) {
          $(this).text(event.strftime('%H:%M:%S'));
      });

      $('#getting-started').on('finish.countdown', function() {
        getting_value = 0;
      });


    if ($('#getting-started').text() == '00:00:00') {
      getting_value = 0;
    }

    $('#refresh').click(function() {
        load_wft();
    });

    /*function load_wft() {
      $.ajax({
        type:'GET',
        url :'loadwft',
        success:function(html){
            var result = JSON.parse(html);
            $('#result').html(result.draw);

        }
    });
  }
  load_wft();*/
  /*data table serverside*/
  var table =$('#table-top').DataTable({
      "processing": true,
      "serverSide": true,
      // "order": [],
      "orderMulti"  : true,
      sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
      "ajax":{
       "url": "showwft_ajax",
       "dataType": "json",
       "type": "POST",
       "data":function(data) {
                  data.fromline = $('.fromline').val();
                  data.toline    = $('.toline').val();
                  data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },

        },
  "columns": [
            {"data" : null, 'sortable' : false},
            { "data": "line" },
            { "data": "wft" },
            { "data": "jam" },
            { "data": "defect_jenis" },
            { "data": "total" },
            { "data": "aksi" },
            { "data": "status" },
         ],
      fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
  });
  /*$('#table-user_filter input').unbind();
    $('#table-user_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0 ) {
            table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
    });*/
  /*end datatable serverside*/
  $('.tampil').click(function () {
    var fromline = $('.fromline').val();
    var toline = $('.toline').val();
    
    if (fromline=='') {
      $("#alert_info").trigger("click", 'Pilih Line Dari Terlebih dahulu');
    }
    if (toline=='') {
      $("#alert_info").trigger("click", 'Pilih Line Sampai Terlebih dahulu');
    }
    
    if (parseInt(toline)<parseInt(fromline)) {
      $("#alert_info").trigger("click", 'Line Akhir tidak boleh lebih Kecil dari Line Awal');
    }else{
      table.draw();
    }
  });
  $('.cap_create').submit(function () {
    var data       = new FormData(this);
    var url_loading = $('#loading_gif').attr('href');
    if (getting_value == 0) {
      $("#alert_info").trigger("click", 'CAP Expired');
        $('#table-top').DataTable().ajax.reload();
        nonaktif();

        return false;
    }
    if (Date.parse(now)<Date.parse(time)) {
        $.ajax({
          url:'saveCapEndline',
          type: "POST",
          data: data,
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function () {
          $.blockUI({
              message: "<img src='" + url_loading + "' />",
              css: {
                backgroundColor: 'transparant',
                border: 'none',
              }
            });
          },
          success: function(data){
            if (data==1) {
                $.unblockUI();
                $("#alert_success").trigger("click", 'sukses');
                $('#table-top').DataTable().ajax.reload();
                nonaktif();
            }

          }
          ,error:function(response){
            if (response.status==500) {
              $.unblockUI();
              $("#alert_info").trigger("click", 'Contact Administrator');
              $('#table-top').DataTable().ajax.reload();
              nonaktif();
            }

           }
        });
    }else{
        $("#alert_info").trigger("click", 'CAP Expired');
        $('#table-top').DataTable().ajax.reload();
        nonaktif();
    }
    return false;
  });
  /*search nik*/

  $('#nik').change(function(e) {
        var url_loading = $('#loading_gif').attr('href');
        var nik = $("#nik").val();
          $.ajax({
              type:'GET',
              url :'searchNik',
              data:'id='+nik,
              success:function(response){
                 var result = JSON.parse(response);
                 if (result.notif==1) {
                     $('#nama').val(result.record.name);
                     aktif();
                      document.getElementById("major_couse").focus();

                 }else{
                    $("#alert_info").trigger("click", 'nik tidak terdaftar');
                 }
              },
                  error:function(response){
                    if (response.status==500) {
                      $.unblockUI();
                      $("#alert_info").trigger("click", 'contact ict');
                    }
                }
          });
      });
  /*end search nik*/
  });

  function create(id_cap,id_detail) {
      var data = 'id_cap='+id_cap+'&id_detail='+id_detail;

      if (Date.parse(now)<Date.parse(time)) {
        $.ajax({
            type:'GET',
            url :'showCreate_cap',
            data:data,
            success:function(response){
                $('#capModal').modal({backdrop: 'static'});
                var result = JSON.parse(response);
                if (result.notif==1) {
                    $('#nik').prop('disabled',false);
                    // document.getElementById("nik").focus();
                    $('#style').val(result.record.style);
                    $('#title').val(result.record.defect_jenis);
                    $('#id_detail').val(result.record.cap_endline_detail_id);
                    $('#defect_id').val(result.record.defect_id);
                    $('#line_name').val(result.record.line_name);
                    $('#jam').val(result.record.jam);
                    $('#table-top').DataTable().ajax.reload();
                }
            }
        });

    }else{
        $("#alert_info").trigger("click", 'CAP Expired');
        $('#refresh').trigger("click");
        nonaktif();
    }

  }

  $('#capModal').on('shown.bs.modal', function() {
      $('#nik').focus();
  });

  function aktif() {
       $('.major_couse').prop('disabled',false);
       $('.root_couse').prop('disabled',false);
       $('.action').prop('disabled',false);
       $('.save').prop('disabled',false);
  }
  function nonaktif() {
        $('#nik').prop('disabled');
        $('.major_couse').prop('disabled');
        $('.root_couse').prop('disabled');
        $('.action').prop('disabled');
        $('.save').prop('disabled');
        $('#style').val(' ');
        $('#title').val(' ');
        $('#nik').val(' ');
        $('#nama').val(' ');
        $('#id_detail').val(' ');
        $('#defect_id').val(' ');
        $('#line_name').val(' ');
        $('#jam').val(' ');
        $('.major_couse').val(' ');
        $('#root_couse').val(' ');
        $('#action').val(' ');

        $('#capModal').modal('hide');
  }

  $('.fromline, .toline').change(function(event) {
    var fromline = $(this).val();
    var toline = $('.toline').val();

    if (fromline != '') {
      if (toline != '') {
        $('#form_search').submit();
      }
    }
  });

  $(".fromline, .toline").chosen({width: "100%"});

</script>
