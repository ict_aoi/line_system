
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa-database"> CAP</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('home');?>">Report</a></li>
        <li><i class="fa fa-bars"></i>CAP</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="row">
      <div class="col-md-4">
          <div class="panel-group" id="accordion">
              <label>Input Tanggal Awal</label>
              <br>
              <input class="form-control" name="tanggal1" id="tanggal1" autocomplete="off">
              <br>
              <label>Input Tanggal Akhir</label>
              <br>
              <input class="form-control" name="tanggal2" id="tanggal2" autocomplete="off">
              <br>
              <label>Input Jenis Laporan</label>
              <br>
              <select class="form-control select2" id="laporan" name="laporan">
                <option value="">-- Pilih --</option>
                <option value="0">CAP Inline</option>
                <option value="1">CAP Endline</option>
              </select>
              <!-- <input class="form-control" type="date" id="tgl"> -->
              <br>
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" id='collapse1' href="#collapseOne">Pencarian Berdasarkan LINE</a>
                          </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse">
                          <div class="panel-body">
                              <input class="form-control" type="text" id="search_line">        
                          </div>
                      </div>
                  </div>
              <br>
                  <!-- <div class="panel panel-default">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" id='collapse2' href="#collapseTwo">Pencarian Berdasarkan Style</a>
                          </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                          <div class="panel-body">
                              <input class="form-control" type="text" id="style">        
                          </div>
                      </div>
                  </div>
              <br> -->
              
              <!-- onclick='return download()'
                <button id="download_report" class='btn btn-md btn-warning'><i class='fa fa-th-list'></i> Download Report</button> -->
              <button id='submit-download' href='javascript:void(0)' class='btn btn-md btn-primary'><i class='fa fa-file-text'></i> Download Report</button>
              <div class="pull-right">
                  <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button>
              </div>
          </div>
      </div>
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
      </div>
    </div>
    <!-- <div class="col-md-5">
      <div class="box box-success">
        <div class="box-header with-border">
          <div class="box-title">Input Tanggal</div>
            <div class="box-body">
              <div class="form-group">
                <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required>
                <div class="clearfix"></div><br>
                <div class="text-right">
                  <button type="button" class="btn btn-primary submit-downloadstyle"><i class="fa fa-file-excel-o"></i> Download Style</button>
                  <button type="button" class="btn btn-warning submit-download"><i class="fa fa-file-excel-o"></i> Download</button>
                  <button class='btn btn-md btn-success' id='pencarian'><i class='fa fa-search'></i> Search</button>
                </div>
              </div>
            </div>
        </div>
      </div> -->
  </div>
</div>
  
  <div id="result">    
  </div>

<div class="myDIV"></div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  $(function(){
      $('#tanggal1').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#tanggal2').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#clear_line').click(function() {
        $('#tanggal1').val('');
        $('#tanggal2').val('');
        $('#search_line').val('');
        table.draw();
      });
      
      $('#pencarian').click(function() {
          var tgl1     = $('#tanggal1').val();
          var tgl2     = $('#tanggal2').val();
          var line    = $('#search_line').val();
          var laporan = $('#laporan').val();

          if (!laporan) {
              $("#alert_warning").trigger("click", 'JENIS LAPORAN TIDAK BOLEH KOSONG');
          }else{
            var data = 'tanggal1='+tgl1+'&tanggal2='+tgl2+'&line='+line
                      +'&laporan='+laporan;
            $.ajax({
              url:'filter_cap',
                      type: "POST",
                      data: data,
                      beforeSend: function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                  }
                });
              },
              success: function(response){
                // console.log(response);
                      // if (response=='gagal') {
                      //   $("#alert_warning").trigger("click", 'Data Tidak Ada');
                      //   $.unblockUI();
                      // }else{
                        $('#result').html(response);
                        $.unblockUI();
                      // }

                    },
                    error:function(response){
                    if (response.status==500) {
                      $("#alert_info").trigger("click", 'Cek Input Data');
                      $.unblockUI();
                    }
                }
            });
          }

          
      });

      $('#collapse1').click(function(event) {
          $('#search_line').val('');
      });

      $('#submit-download').on('click', function(event) {
        
        var laporan     = $('#laporan').val();
        var dari        = $('#tanggal1').val();
        var sampai      = $('#tanggal2').val();
        var search_line = $('#search_line').val();

        if (!laporan) {
            $("#alert_warning").trigger("click", 'JENIS LAPORAN TIDAK BOLEH KOSONG');
        }
        else{            
          if(laporan==0)
          {
            // window.open(base_url+'Cap/download_inline?tanggal='+tanggal+'&search_line='+search_line);
            window.open(base_url+'Cap/download_inline?dari='+dari+'&sampai='+sampai+'&search_line='+search_line);
            // event.preventDefault(); //return false. berhenti disitu
            // location.reload();
          }
          else if(laporan==1){
            // console.log("itu"+laporan);
            // event.preventDefault(); //return false. berhenti disitu
            window.open(base_url+'Cap/download_endline?dari='+dari+'&sampai='+sampai+'&search_line='+search_line);
            // window.open(base_url+'Report/downloadDistribution?dari='+dari+'&sampai='+sampai+'&pobuyer='+pobuyer);
            // location.reload();
          }
        }
      });
      
  });
</script>