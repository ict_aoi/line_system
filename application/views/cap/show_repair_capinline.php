<form class="perbaikicap" method="POST">
    <label>Alasan</label>
    <input type="hidden" value="<?=$record?>" name="id">
    <textarea class="form-control" placeholder="Description" name="description" cols="50" rows="10"></textarea>
    <br>
    <button type="submit" name="submit" class="btn btn-danger btn-sm bsave" id="save"><i class="fa fa-save"></i> SIMPAN</button>
</form>
<script>
    $(document).ready(function(){
        $('.perbaikicap').submit(function(){
            var data = new FormData(this);
            
            $.ajax({
                url:'SubmitRepairCapInline',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    var result = JSON.parse(response);
                    if (result.status==200) {
                         $("#myModal").modal('hide');
                         $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                         $('#table-capinline').DataTable().ajax.reload();
                    }else{
                        $("#alert_error").trigger("click", 'Failed');
                    }
                }
        })
        return false;
    });
    });
</script>