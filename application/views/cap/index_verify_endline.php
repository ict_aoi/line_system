<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
      <div class="panel-heading">
            <i class="fa fa-wrench"></i> List Verify WFT
      </div>
        <br>
        <div class="panel-body">
            <table id="table-wft" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">NO</th>
                        <th width="80px">LINE</th>
                        <th width="80px">JAM</th>
                        <th width="80px">WFT</th>
                        <th class="text-center">NAMA DEFECT</th>
                        <th width="80px" class="text-center">TOTAL</th>
                        <th class="text-center">PENYEBAB UTAMA</th>
                        <th class="text-center">ROOT CAUSE</th>
                        <th class="text-center">ACTION</th>
                        <th width="80px" class="text-center">DEADLINE QC</th>
                        <th width="50px">ACTION</th>
                        <th width="50px">STATUS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
         var table =$('#table-wft').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
             "url": "showVerifyEndline",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "line" },
                  { "data": "jam" },
                  { "data": "wft" },
                  { "data": "jenis_defect" },
                  { "data": "total" },
                  { "data": "major_couse" },
                  { "data": "root_couse" },
                  { "data": "action" },
                  { "data": "deadlineqc" },
                  { "data": "aksi" },
                  { "data": "status" },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
        $('#table-user_filter input').unbind();
        $('#table-user_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-user').DataTable().ajax.reload();
        });
    });
    function verify(id_detail) {
      $.ajax({
            type:'GET',
            url :'verifyEndline',
            data:'id_detail='+id_detail,
            success:function(response){
                if (response==1) {
                    $('#table-wft').DataTable().ajax.reload();
                }else{
                    $("#alert_info").trigger("click", 'CAP Expired');
                    $('#table-wft').DataTable().ajax.reload();
                }
            }
        });
        
    }
    function perbaiki(id_detail) {
      var url_loading = $('#loading_gif').attr('href');
      var modal = $('#myModal > div > div');
      $('#myModal').modal('show');
      modal.children('.modal-header').children('.modal-title').html('Perbaiki CAP');
      modal.parent('.modal-dialog').addClass('modal-lg');
      modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
      modal.children('.modal-body').load('showrepairendline?id='+id_detail);
      return false;
    }
</script>