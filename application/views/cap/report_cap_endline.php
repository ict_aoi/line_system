
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               CAP Endline
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
          <input type="hidden" id="tgl1" value="<?=$tgl1;?>">
          <input type="hidden" id="tgl2" value="<?=$tgl2;?>">
          <input type="hidden" id="line" value="<?=$line;?>">
          <input type="hidden" id="laporan" value="<?=$laporan;?>">
          <div class="clearfix"></div><br>
          <div class="box-body">
    
          <div class="clearfix"></div>
          <div class="table-responsive"><br>
            <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                <thead>
                    <th class="text-center">DATE</th>
                    <th class="text-center">LINE</th>
                    <th class="text-center">JAM</th>
                    <th class="text-center">WFT</th>
                    <th class="text-center">NAMA DEFECT</th>
                    <th class="text-center">MAJOR CAUSE</th>
                    <th class="text-center">ROOT CAUSE</th>
                    <th class="text-center">ACTION</th>
                    <th class="text-center">DIBUAT OLEH</th>
                    <th class="text-center">DUE DATE</th>
                    <th class="text-center">STATUS</th>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
    </div>
    
</div>
</div>

<!-- </div>
</div> -->

<script>
    var url_loading = $('#loading_gif').attr('href');
    var table = $('#table-result').DataTable({
        "processing": true,
        "serverSide": true,
        "orderMulti"  : true,
        "searching": false,
        sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        "ajax":{
            "url": "summary_endline",
            "dataType": "json",
            "type": "POST",
            "beforeSend": function () {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                backgroundColor: 'transparant',
                border: 'none',
                }
            });
            },
            "complete": function() {
            $.unblockUI();
            },
            fixedColumns: true,
            "data":function(data) {
                    data.tanggal1     = $('#tgl1').val();
                    data.tanggal2     = $('#tgl2').val();
                    data.search_line  = $('#line').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    },
                        },
            "columns": [
                { "data": "date" },
                { "data": "line" },
                { "data": "jam",'sortable' : false },
                { "data": "wft",'sortable' : false },
                { "data": "defect_id",'sortable' : false },
                { "data": "major_couse",'sortable' : false },
                { "data": "root_couse",'sortable' : false },
                { "data": "correct_action",'sortable' : false },
                { "data": "dibuat_oleh",'sortable' : false },
                { "data": "dibuat_kapan",'sortable' : false },
                { "data": "status" },
            ],
            
    });


</script>