<!DOCTYPE html>
  <html>
    <head>
      <meta http-equiv="cache-control" content="max-age=0" />
      <meta http-equiv="cache-control" content="no-cache" />
      <meta http-equiv="expires" content="0" />
      <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
      <meta http-equiv="pragma" content="no-cache" />
      <title>Login QC Endline | Inline</title>
      <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/icon.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/style_login.css"  media="screen,projection"/>      
      
      <script src="<?php echo base_url();?>assets/plugins/blockUI/sweetalert.min.js"></script>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
    
      <div class="container">
        <div class="row">

          <div class="col s12 m12 l12">
            <form class="col s12 center-div login">
              <!-- <input type="hidden" name="dst" value="$(link-orig)"> -->
              
              <div class="box">
                <div class="row no-margin-btm">
                  <div class="col s12">
                    <img src="<?=base_url();?>assets/img/logo1.png" class="responsive-img center-block logo">
                  </div>
                </div>
                <div class="box-input">
                  <div class="row no-margin-btm">
                    <span class="center-it">Line System Login</span><br>
                    <span class="center-it"><b>QC Endline <?php echo $line_name ?></b></span>
                  </div>
                  <div class="row no-margin-btm">
                    <div class="input-field col s12">
                      <input type="number" name="nik" id="nik" type="text" placeholder="Isikan NIK" autofocus required autocomplete="off ">
                    </div>
                  </div>
                  <input type="hidden" id="line" name="line" value="<?php echo $this->uri->segment(3);?>">
                  <div class="row no-margin-btm">
                    <div class="input-field col s12">
                      <button class="btn waves-effect waves-light center-block btn-login" type="submit">Login
                      </button>
                    </div>
                  </div>
                </div>
                <div class="row no-margin-btm">
                  <div class="col s12">
                    <div class="footer">
                      <h6 class="center-align">POWERED BY ICT-Development</h6>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div id="modal" class="modal">
        <div class="modal-content">
          <h4>Pilih Line</h4>
          <p>Line tidak terdeteksi, Pilih Line untuk login</p>
          <form class="col s12 center-div login">
            <input type="hidden" name="nik" id="nik2">
            <div class="input-field col s12 result">
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn waves-effect waves-light center-block btn-login" type="submit">Login</button>
        </div>
      </form>
      </div>
      <script src="<?=Base_url();?>assets/js/jquery.min.js"></script>
      <script src="<?=Base_url();?>assets/js/materialize.min.js"></script>
        <script>
         $(document).ready(function() {
           
            $('.login').submit(function(){
                  var data = new FormData(this);
                  $.ajax({
                      url:'<?=base_url();?>auth/qc_login/',
                      type: "POST",
                      data: data,
                      contentType: false,       
                      cache: false,          
                      processData:false, 
                      success: function(response){
                        var result= JSON.parse(response);
                          if (result.status==200) {
                             window.location.href ="qc"; 
                          }else if (result.status==500){
                            swal({
                                title: "Oops...",
                                text: result.pesan,
                                showConfirmButton: false,
                                icon: "error",
                                timer: 1000
                            });
                            $('#nik').focus();
                          }else if (result.status==300) {
                            console.log(result.draw);
                            $('#nik2').val(result.nik); 
                            $('.result').html(result.draw);
                            $('#modal').modal();
                            $('#modal').modal('open');
                            
                            $("#selectedTest").formSelect();
                          }
                      }
              })
              return false;
          });
        });
      </script>
    </body>
  </html>