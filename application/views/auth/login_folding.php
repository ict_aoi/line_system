<!DOCTYPE html>
  <html>
    <head>
      <meta http-equiv="cache-control" content="max-age=0" />
      <meta http-equiv="cache-control" content="no-cache" />
      <meta http-equiv="expires" content="0" />
      <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
      <meta http-equiv="pragma" content="no-cache" />
      <title>Login Folding | Inline</title>
      <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/icon.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/style_login.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
    
      <div class="container">
        <div class="row">

          <div class="col s12 m12 l12">
            <form class="col s12 center-div" action="<?=base_url('auth/folding/'.$line);?>" method="POST">
              <!-- <input type="hidden" name="dst" value="$(link-orig)"> -->
              
              <div class="box">
                <div class="row no-margin-btm">
                  <div class="col s12">
                    <img src="<?=base_url();?>assets/img/logo1.png" class="responsive-img center-block logo">
                  </div>
                </div>
                <div class="box-input">
                  <div class="row no-margin-btm">
                    <span class="center-it">Line System Login</span><br>
                    <span class="center-it"><b>Folding </b><?php echo $line_name ?></span>

                  </div>
                  <div class="row no-margin-btm">
                    <div class="input-field col s12">
                      <input type="number" name="nik" id="nik" type="text" placeholder="Isikan NIK" autofocus required autocomplete="off ">
                    </div>
                  </div>
                  <div class="row no-margin-btm">
                    <div class="input-field col s12">
                      <button class="btn waves-effect waves-light center-block btn-login" type="submit">Login
                      </button>
                    </div>
                  </div>
                </div>
                <div class="row no-margin-btm">
                  <div class="col s12">
                    <div class="footer">
                      <h6 class="center-align">POWERED BY ICT-Development</h6>
                    <!--  <img src="img/logo-gmedia.png" class="responsive-img center-block logo-footer"> -->
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>