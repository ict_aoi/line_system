<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Inline - System">
    <meta name="author" content="ICT">
    <meta name="keyword" content="Inline - System">
    <link rel="shortcut icon" href="<?php base_url() ?>assets/img/favicon.jpg">

    <title>Login Page | Inline</title>

    <!-- Bootstrap CSS -->    
    <link href="<?=Base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?=Base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="<?=Base_url();?>assets/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?=Base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="<?=Base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style-responsive.css" rel="stylesheet" />
    <style type="text/css">
        .login-img3-body{
            width: 100%;
            height: auto;
        }
    </style>
</head>

  <body class="login-img3-body">

    <div class="container">        
      <form class="login-form" action="<?=base_url('auth/login');?>" method="POST">        
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" class="form-control" placeholder="NIK" name="nik" autofocus required>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span><?php echo form_error('password');?>
                <input type="password" class="form-control" placeholder="Password" name="password" required>
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me" name="remember"> Remember me
                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>

        </div>
      </form>

    </div>


  </body>
</html>
