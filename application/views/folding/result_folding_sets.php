<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->

<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Folding
             </div>
             <div class="col-md-3 pull-right">
               <span class="style"><?=$style;?></span><span>-</span>
                <span class="article"><?=$article;?></span>
             </div>
           </h6>
        </div>
        <div class="panel-body">
          <input type="hidden" id="header_id" value="<?=$header_id;?>">
          <input type="hidden" id="sizeid" value="<?=$size_id;?>">
          <input type="hidden" id="remark" value="<?=$remark;?>">
          <!-- <input type="hidden" id="switch" value="<?=$switch;?>"> -->
          <!-- <input type="hidden" id="change_hid" value="<?=$change;?>"> -->
          <input type="hidden" id="barcode_id" value="<?=$barcode_id;?>">
          <input type="hidden" id="scan_id" value="<?=$scan_id;?>">
          <input type="hidden" id="style_hid" value="<?=$style;?>">
          <input type="hidden" id="barcodePackage" value="">
          <!-- <input type="hidden" id="folding_output" value="<?php //$max_output;?>"> -->
          <div class="clearfix"></div><br>
          <div class="form-group"> 
            <div class="row"> 
              <div class="col-md-2">
                <span><h5>PO BUYER </h5></span>
                <span class="poreference"><h2><b><?=$poreference;?></b></h2></span>
              </div> 
              <div class="col-md-2">
                <h5>SIZE :</h5>
                <span class="size"><h2><b><?=$size;?></b></h5></span>
              </div> 
            </div> 
          </div>
          <div class="col-md-3">
            <input type="hidden" id="qtyidle" value="0">
            <label>Status :</label> 
            <span class="proses hidden"><span class="status label label-primary">Proses Simpan Sedang Berjalan</span></h3></span>
            <span class="selesai hidden"><span class="status label label-danger">Simpan Selesai</span></h3></span>
           <!-- <h3><span class=""><b><?=$status;?></b></span></h3> -->
          </div>
          <div class="clearfix"></div>
            <table class="table table-striped table-bordered table-hover table-full-width">
                <thead>
                    <tr>
                      <th width="200px" class="text-center">WIP</th>
                      <th class="text-center">CHECKED</th>
                      <th width="150px" class="text-center">BALANCE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="wft"></span></td>
                        <!-- <td class="text-center" style="font-size: 30px">0</td> -->
                        <td class="text-center" style="font-size: 30px"><span class="countertoday"></span></td>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="balance"></span></td>
                    </tr>
                    <tr>
                        <!-- <td class="text-center">0</td> -->
                        <td class="text-center"><span class="counter_folding"></span></td>
                    </tr>
                    <tr>
                      <td colspan="4">
                        <!-- <div class="input-group">                           -->
                          <input type="text" id="scan" class="form-control hidden" style="left" autofocus onClick="this.select();" placeholder="SCAN BARCODE DI SINI">
                          <button type="button" id="good" style="height: 75px;font-size:40px"  class="btn btn-success btn-lg btn-block">+</button>
                      </td>
                       
                        
                    </tr>
                </tbody>    
            </table>
        </div>
    </div>
    
</div>
</div>

<button type="button" class="hidden" id="refresh"></button>
<button type="button" class="hidden" id="loadlist"></button>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/folding_result_sets.js"></script>
