<style type="text/css">
 
.modal-lg {
     width: 800px;
    }
</style>

<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group"> 
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="info-box green-bg">
                          <i class="fa fa-dropbox"></i>
                      <div class="count"><h3 class="no-margin text-semibold" style="font-size: 28px;"><span class="totaloutput"></span></h3></div>
                      <div class="title"><span class="text-uppercase text-size-mini text-muted">Total Output Folding</span></div>
                  </div>       
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box blue-bg">
                            <i class="fa fa-cubes"></i>
                        <div class="count"><h3 class="no-margin text-semibold" style="font-size: 28px;"><span class="totalkomplete"></span></h3></div>
                        <div class="title"><span class="text-uppercase text-size-mini text-muted">Total Karton Komplete</span></div>
                    </div>       
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-xs-6">
                  <label for="po">Masukkan PO</label>
                  <div class="input-group">
                        <input type="text" id="numpadButton" class="form-control pobuyer" placeholder="Masukan PO" aria-describedby="numpadButton-btn">
                        <span class="input-group-btn">
                            <button class="btn btn-default" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
                        </span>
                    </div>
                  <!-- <input class="form-control" name="pobuyer" id="pobuyer" autocomplete="off" required> -->
                  <div class="clearfix"></div><br>
                  <div class="text-right">
                    <button type="button" class="btn btn-primary tampil"><i class="fa fa-file-excel-o"></i> Tampilkan</button>
                  </div>
                </div>
              </div>
            </div>



            <!-- <div class="row">
              <div style="overflow-y: auto; height:400px">
                <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">NO</th>
                      <th class="text-center">PO</th>
                      <th class="text-center">STYLE</th>
                      <th class="text-center">SIZE</th>
                      <th class="text-center">TOTAL KARTON</th>
                      <th class="text-center">TOTAL QTY</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div> 
            </div> -->
        </div>
    </div>
</div>
</div>


<div id="result">
</div>
<script type="text/javascript">
$(document).ready(function () {
    // $('#tanggal').datepicker({
    //     format: "yyyy-mm-dd",
    //     autoclose: true
    // });
    // $.ajax({
    //     type:'POST',
    //     url :'<?=base_url();?>Folding/show_output_folding',
    //     // data:'',
    //     success:function(response){
    //         var result = JSON.parse(response);
    //         $('.totaloutput').text(result.data.totaloutput);
    //         $('.totalkomplete').text(result.data.totalkomplete);
    //         // table.draw();
    //     },
    //     error:function(response){
    //       if (response.status==500) {
            
    //       }
    //   }
    // });
    
  var url_loading = $('#loading_gif').attr('href');
  var po   = $('.pobuyer').val();
  // console.log(po);
  // // if(po!=''){
  //   var table =$('#table-result').DataTable({
  //       "stateSave": true,
  //       "processing": true,
  //       "serverSide": true,
  //       "searching": false,
  //       // "order": [],
  //       "orderMulti"  : true,
  //       "ajax":{
  //        "url": "folding_output_ajax",
  //        "dataType": "json",
  //        "type": "POST",
  //        "beforeSend": function () {
  //               $.blockUI({
  //                 message: "<img src='" + url_loading + "' />",
  //                 css: {
  //                   backgroundColor: 'transparant',
  //                   border: 'none',
  //                 }
  //               });
  //             },
  //             "complete": function() {
  //               $.unblockUI();
  //             },
  //             fixedColumns: true,
  //            "data":function(data) {
  //               data.pobuyer   = $('.pobuyer').val();
  //               data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
  //             },
  //                  },
  //       "columns": [           
  //                 { "data" : null, 'sortable' : false},
  //                 { "data": "poreference",'sortable' : false },
  //                 { "data": "style",'sortable' : false },
  //                 { "data": "size"},
  //                 { "data": "total_karton",'sortable' : false },
  //                 { "data": "total_qty",'sortable' : false },
  //                 { "data": "action",'sortable' : false },
  //             ],
  //             fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
  //   });
    
  // // }
    
  //     $('#table-user_filter input').unbind();
  //     $('#table-user_filter input').bind('keyup', function(e) {
  //         if (e.keyCode == 13 || $(this).val().length == 0 ) {
  //             table.search($(this).val()).draw();
  //         }
  //         // if ($(this).val().length == 0 || $(this).val().length >= 3) {
  //         //     table.search($(this).val()).draw();
  //         // }
  //     });

      $('.tampil').click(function() {
        var po = $(".pobuyer").val();

        if (!po) {
            $("#alert_warning").trigger("click", 'PO TIDAK BOLEH KOSONG');
        }else{
          var data = 'pobuyer='+po;

          $.ajax({
            url:'filter_output_folding',
                    type: "POST",
                    data: data,
                    beforeSend: function () {
              $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transaparant',
                  border: 'none',
                }
              });
            },
            success: function(response){
              // console.log(response);
                    // if (response=='gagal') {
                    //   $("#alert_warning").trigger("click", 'Data Tidak Ada');
                    //   $.unblockUI();
                    // }else{
                      $('#result').html(response);
                      $.unblockUI();
                    // }

                  },
                  error:function(response){
                  if (response.status==500) {
                    $("#alert_info").trigger("click", 'Cek Input Data');
                    $.unblockUI();
                  }
              }
          });
        }

        
    });

      
    

    // $('.tampil').click(function () {
    //   // if ($('#tanggal').val()!='') {
        
    //     var po = $(".pobuyer").val();
    //     table.draw();
         
    //   // }else{
    //   //   $("#alert_error").trigger("click", 'Tanggal tidak boleh kosong');
    //   // }
    // });

});


// function detail(po,size) {
//     var url_loading = $('#loading_gif').attr('href');
//     var modal = $('#myModal_ > div > div');
//     $('#myModal_').modal('show');
//     modal.children('.modal-header').children('.modal-title').html('Report OutputFolding');
//     modal.parent('.modal-dialog').addClass('modal-lg');
//     modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
//     modal.children('.modal-body').load('detail_folding_output?po='+po+'&size='+size);
// }


</script>