<script type="x-tmpl-mustache" id="items-table">

  {% #item %}
    <tr>
      <td style="text-align:center;">{% no %}</td>
      <td>
        <input type="hidden" id="barcode_id" value="{% barcode_id %}">
        <div id="style_{% _id %}">{% style %}</div>
      </td>
      <td>
          <div id="poreference_{% _id %}">{% poreference %}</div>
      </td>
      <td>
        <div id="size_{% _id %}">{% size %}</div>
      </td>
      <td>
          <div id="article_{% _id %}">{% article %}</div>
      </td>
      <td>
          <div id="qty_{% _id %}">{% qty %}</div>
      </td>
      <td>
          <div id="cut_num{% _id %}">{% cut_num %}</div>
      </td>
      <td>
          <div id="startend{% _id %}">{% startend %}</div>
      </td>
      <td>
          <div id="component_name_{% _id %}">{% component_name %}</div>
      </td>

      <td width="50px">
        <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="fa fa-trash-o"></i></button> 
      </td>
    </tr>
  {%/item%}

   <tr>
    <td width="20px">
      #
    </td>
    <td colspan="8">
      <input type="text" class="form-control" autofocus id="scanbandle" placeholder="Scan Barcode Bandle disini">
    </td>


  </tr>
</script>