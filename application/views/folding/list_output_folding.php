<style type="text/css">
 
.modal-lg {
     width: 1000px;
    }
</style>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Output Folding
             </div>
             <!-- <div class="col-md-2 pull-right">
               
             </div> -->
           </h6>
        </div>
        
        <div class="panel-body">
        
          <input type="hidden" id="pobuyer" value="<?=$pobuyer;?>">
          <div class="clearfix"></div><br>
          <div class="box-body">
    
          <div class="clearfix"></div>
          <div class="table-responsive"><br>
            <!-- <div style="overflow-x:auto;overflow-y:auto"> -->
                <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">NO</th>
                      <th class="text-center">PO</th>
                      <th class="text-center">STYLE</th>
                      <th class="text-center">SIZE</th>
                      <th class="text-center">TOTAL KARTON</th>
                      <th class="text-center">TOTAL GARMENT</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if($master==0){
                    }
                    else{
                      $i=1;
                      foreach($master as $m) {
                        $temp = '"'.$m->poreference.'","'.$m->size.'"';?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $m->poreference;?></td>
                            <td><?php echo $m->style;?></td>
                            <td><?php echo $m->size;?></td>
                            <td><?php echo $m->total_karton;?></td>
                            <td><?php echo $m->total;?></td>
                            <td><?php echo "<center><a onclick='return detail($temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-align-justify'></i> Detail</a></center>";?></td>
                        </tr>
                    <?php }
                    }?>
                  </tbody>
                </table>
            <!-- </div> -->
          </div>
        </div>
    </div>
    
</div>
</div>


<!-- MODAL -->

<!-- <div class="modal fade" id="detail-output" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="panel-body">
              
              <input type="hidden" id="poref" value="</?=$poreference;?>">
              <input type="hidden" id="po_size" value="</?=$size;?>"> 
              
              <input type="hidden" id="poref">
              <input type="hidden" id="po_size">
              <div class="row">
                  <div class="col-md-4">
                      <div class="input-group">
                          <input type="text" id="numpadButton" class="form-control karton" placeholder="Masukan 5 digit karton terakhir" aria-describedby="numpadButton-btn">
                          <span class="input-group-btn">
                              <button class="btn btn-default" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
                          </span>
                      </div>
                      <div class="text-right">
                          <button type="button" class="btn btn-primary tampilkan"><i class="fa fa-file-excel-o"></i> Tampilkan</button>
                      </div>
                  </div>
                  <div class="col-md-4"></div>
                  <div class="col-md-4"></div>
              </div>
              <table id="table-detail" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th class="text-center">NO</th>
                          <th class="text-center">PO</th>
                          <th class="text-center">STYLE</th>
                          <th class="text-center">SIZE</th>
                          <th class="text-center">QTY SIZE</th>
                          <th class="text-center">QTY KARTON</th>
                          <th class="text-center">BARCODE KARTON</th>
                          <th class="text-center">LINE</th>
                          <th class="text-center">TANGGAL TERIMA</th>
                          <th class="text-center">TANGGAL COMPLETE</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
          </div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div> -->
<!-- END MODAL -->
<!-- </div>
</div> -->

<script>
  var url_loading = $('#loading_gif').attr('href');
     
  var table =$('#table-result').DataTable();
  
  function detail(po,size) {
    var url_loading = $('#loading_gif').attr('href');
    var modal = $('#myModal_ > div > div');
    $('#myModal_').modal('show');
    modal.children('.modal-header').children('.modal-title').html('Report OutputFolding');
    modal.parent('.modal-dialog').addClass('modal-lg');
    modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
    modal.children('.modal-body').load('detail_folding_output?po='+po+'&size='+size);
  }

  // function detail(po,size) {
  //   var url_loading = $('#loading_gif').attr('href');
  //   // var modal = $('#detail-output > div > div');
  //   // , keyboard: false
  //   $('#detail-output').modal({backdrop: 'static'});
  //   // $('#detail-output').modal('show');
  //   // modal.children('.modal-header').children('.modal-title').html('Report OutputFolding');
  //   // modal.parent('.modal-dialog').addClass('modal-lg');

  //   $('#poref').val(po);
  //   $('#po_size').val(size);
    
  //   var table =$('#table-detail').DataTable({
  //       "destroy": true,
  //       "processing": true,
  //       "serverSide": true,
  //       // "order": [],
  //       "orderMulti"  : true,
  //       "ajax":{
  //        "url": "folding_detail_ajax",
  //        "dataType": "json",
  //        "type": "POST",
  //        "beforeSend": function () {
  //               $.blockUI({
  //                 message: "<img src='" + url_loading + "' />",
  //                 css: {
  //                   backgroundColor: 'transparant',
  //                   border: 'none',
  //                 }
  //               });
  //             },
  //             "complete": function() {
  //               $.unblockUI();
  //             },
  //             fixedColumns: true,
  //            "data":function(data) {
  //               data.pobuyer = $('#poref').val();
  //               data.size    = $('#po_size').val();
  //               data.karton  = $('.karton').val();
  //               // console.log(data.pobuyer);
  //               // console.log(data.size);
  //               data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
  //             },
  //                  },
  //       "columns": [
  //                 { "data" : null, 'sortable' : false},
  //                 { "data": "poreference",'sortable' : false },
  //                 { "data": "style",'sortable' : false },
  //                 { "data": "size",'sortable' : false },
  //                 { "data": "qty_size",'sortable' : false },
  //                 { "data": "qty_karton",'sortable' : false },
  //                 { "data": "barcode_package" },
  //                 { "data": "nama_line",'sortable' : false },
  //                 { "data": "create_on" },
  //                 { "data": "complete_on" },
  //             ],
  //             fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
  //   });
  //   $('#table-detail_filter input').unbind();
  //   $('#table-detail_filter input').bind('keyup', function(e) {
  //       if (e.keyCode == 13 || $(this).val().length == 5 ) {
  //           table.search($(this).val()).draw();
  //       }
  //       // if ($(this).val().length == 0 || $(this).val().length >= 3) {
  //       //     table.search($(this).val()).draw();
  //       // }
  //   });

  //   $('.tampilkan').click(function () {
  //       table.draw();
  //   });
  //   // modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
  //   // modal.children('.modal-body').load('detail_folding_output?po='+po+'&size='+size);
  // }

    //  var table =$('#table-result').DataTable({
    //     "stateSave": true,
    //     "processing": true,
    //     "serverSide": true,
    //     "searching": false,
    //     // "order": [],
    //     "orderMulti"  : true,
    //     "ajax":{
    //      "url": "folding_output_ajax",
    //      "dataType": "json",
    //      "type": "POST",
    //      "beforeSend": function () {
    //             $.blockUI({
    //               message: "<img src='" + url_loading + "' />",
    //               css: {
    //                 backgroundColor: 'transparant',
    //                 border: 'none',
    //               }
    //             });
    //           },
    //           "complete": function() {
    //             $.unblockUI();
    //           },
    //           fixedColumns: true,
    //          "data":function(data) {
    //             data.pobuyer   = $('.pobuyer').val();
    //             data.<?php// echo $this->security->get_csrf_token_name(); ?> = "<?php //echo $this->security->get_csrf_hash(); ?>";
    //           },
    //                },
    //     "columns": [           
    //               { "data" : null, 'sortable' : false},
    //               { "data": "poreference",'sortable' : false },
    //               { "data": "style",'sortable' : false },
    //               { "data": "size"},
    //               { "data": "total_karton",'sortable' : false },
    //               { "data": "total_qty",'sortable' : false },
    //               { "data": "action",'sortable' : false },
    //           ],
    //           fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    // });
    



</script>

