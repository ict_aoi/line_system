<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->

<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Folding - Sets
             </div>
             <div class="col-md-4 pull-right">
               <span class="style"><?=$p['style'];?></span>-
               <span class="article"><?=$p['article'];?></span>
             </div>
           </h6>
        </div>
        <div class="panel-body">
          <input type="hidden" id="header_id" value="<?=$p['header_id'];?>">
          <input type="hidden" id="sizeid" value="<?=$p['size_id'];?>">
          <input type="hidden" id="rasio" value="<?=$rasio;?>">
          <input type="hidden" id="remark" value="<?=$p['remark'];?>">
          <input type="hidden" id="switch" value="0">
          <input type="hidden" id="scan_id" value="<?=$p['scan_id'];?>">
          <input type="hidden" id="style_hid" value="<?=$p['style'];?>">
          <input type="hidden" id="barcode_garment_hid">
          <input type="hidden" id="barcodePackage" value="<?=$barcodePackage;?>">
          <input type="hidden" id="source_data" value="<?=$p['source_data'];?>">
          <input type="hidden" id="counterscan_rasio" value="">
          <input type="hidden" id="qty" value="0">
          <input type="hidden" id="barcode_garment" value="0">
          <div class="clearfix"></div><br>
          <div class="form-group">
            <div class="row">
              <div class="col-md-2 pull-right">
                <?php  
                  $department_name = $this->session->userdata('department_name');
                 
                  if ($department_name=='ICT'||$department_name=='CI/IE' ||$department_name=='IE') {
                    echo '<a class="btn btn-danger" href="javascript:void(0)" onclick="modal_revisibarcode()" id="btn-revisi" title="Revisi Barcode Garment"> <span class="fa fa-exchange"></span> Revisi Barcode Garment</a>';
                  }
                ?>
                
                
              </div> 
            </div> 
            <div class="row">
               
              <div class="col-md-2">
                <span><h5>PO BUYER </h5></span>
                <span class="poreference"><h5><b><?=$p['poreference'];?></b></span></h5>
              </div> 
              <?php if($rasio==1){
              ?>
                <div class="col-md-3">
                  <span><h5>ISI KARTON </h5></span>
                  <div class="alert alert-info">
                    <b><span class="list_size"></span><b>
                  </div>
                </div> 
              <?php } 
              ?>
              <div class="col-md-2 pull-right">
                <h5>SIZE :</h5>
                <h5>
                  <select class="form-control size" id="size" required>
                    <?php  
                      foreach ($y as $key => $y) {
                       
                       echo '<option data-qty="'.(int)$y[1].'" value="'.$y[0].'">'.$y[0].'</option>';
                      }
                    ?>
                  </select>
                </h5>
              </div> 
            </div> 
          </div>
          <div class="form-group"> 
            <div class="row"> 
              <div class="col-md-3">
                <label>Tgl Export : </label> 
                <h3><span class="tglexport"><b><?php echo date('d-m-Y', strtotime($p['po_summary']['datepromised']));?></b></span></h3>
              </div> 
              <div class="col-md-3">
                <label>Qty Karton :</label> 
               <h3><span class="qty_karton"><b><?=$p['inner_pack'];?></span><?=$p['remark'] ;?></b></h3>
              </div> 
              <div class="col-md-3">
                <label>Total Karton :</label> 
               <h3><span class="total_karton"><b><?=$p['totalKarton'];?></span><span>/<?=$p['pkg_count'];?></span></b></h3>
              </div> 
              <div class="col-md-3">
                <label>Status :</label> 
                <div class="status">
                  <?=$status;?>
                </div>
               <!-- <h3><span class=""><b><?=$status;?></b></span></h3> -->
              </div>
            </div> 
          </div>
          <div class="clearfix"></div>
            <table class="table table-striped table-bordered table-hover table-full-width">
                <thead>
                    <tr>
                      <th width="200px" class="text-center name1">WIP</th>
                      <th class="text-center name2">WIP</th>
                      <th width="250px" class="text-center">QTY SAVE GARMENT</th>
                      <th width="250px" class="text-center">QTY SCAN</th>
                      <th width="150px" class="text-center">BALANCE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="wip1"></span></td>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="wip2"></span></td>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="qty_save"></span></td>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="qty_scan">0</span></td>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="balance"></span></td>
                    </tr>
                    <tr>
                        <!-- <td class="text-center">0</td> -->
                        <!-- <td class="text-center"><span class="counter_folding"></span></td> -->
                    </tr>
                    <tr>
                      <td colspan="5">
                        <!-- <div class="input-group">                           -->
                          <input type="text" id="scan" class="form-control" style="left" autofocus onClick="this.select();" placeholder="SCAN BARCODE DI SINI">
                          <br>
                        <!-- </div> -->
                      </td>
                       
                        
                    </tr>
                </tbody>    
            </table>
            <div class="col-md-12 ">
                <button id="simpan" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"> Simpan</i></button>
            </div>
        </div>
    </div>
    
</div>
</div>

<button type="button" class="hidden" id="refresh"></button>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/folding_result_sets_scan.js"></script>
