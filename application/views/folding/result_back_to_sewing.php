<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->

<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default border-grey">
        <div class="panel-heading">
           <h6 class="panel-title">
             <div class="col-md-2">
               Bongkaran Packing
             </div>
             <div class="col-md-2 style pull-right">
               <span class="style"></span>
             </div>
           </h6>
        </div>
        <div class="panel-body">
          <input type="hidden" id="barcodePackage" value="<?=$barcodePackage;?>">
          <div class="clearfix"></div><br>
          <div class="form-group"> 
            <div class="row"> 
              <div class="col-md-2">
                <span><h5>PO BUYER </h5> <?=$poreference ?></span>
                
              </div> 
              <div class="col-md-2">
                 <span><h5>PO BUYER </h5> <?php foreach ($size as $key => $s) {
                   echo $s;
                 };?></span>
              </div> 
            </div> 
          </div>
          <div class="form-group"> 
            <div class="row"> 
              <div class="col-md-6">
                <button type="button" id="sewing_in" style="height: 75px;font-size:40px"  class="btn btn-primary btn-lg btn-block" <?=$disable_in ?>>Sewing In</button>
                
              </div> 
              <div class="col-md-6">
                <button type="button" id="sewing_complete" style="height: 75px;font-size:40px"  class="btn btn-success btn-lg btn-block" <?=$disable_out ?>>Sewing Komplit</button>
              </div> 
            </div> 
          </div>
          </div>
          <div class="clearfix"></div>
            <input type="hidden" id="scan">
        </div>
    </div>
    
</div>
</div>
<script type="text/javascript">
var url_loading = $('#loading_gif').attr('href');
  $(function(){
    $('#sewing_in').click(function () {
      swal({
        title: "PERHATIAN!!!",
        text: "Apakah Anda yakin karton Akan di Terima Line?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((wilsave) => {
        if (wilsave) {
          var data = 'barcode_id='+$('#barcodePackage').val(); 
          $.ajax({
                url:'<?=base_url('folding/submit_sewing_in') ?>',
                data:data,
                type:'POST',
                beforeSend: function () {
          
                  $.blockUI({
                    message: "<img src='" + url_loading + "' />",
                    css: {
                      backgroundColor: 'transaparant',
                      border: 'none',
                    }
                  });
                },                  
                success:function(data){
                    var result = JSON.parse(data);
                    if (result.status==200) {
                      $.unblockUI();
                      $("#alert_success").trigger("click", result.pesan);
                      $('#sewing_complete').prop('disabled',false);
                      $('#sewing_in').prop('disabled',true);
                    }else if (result.status==500){
                      $.unblockUI();
                      $('#sewing_in').prop('disabled',false);
                      $('#sewing_complete').prop('disabled',true);
                      $("#alert_error").trigger("click", result.pesan);
                    }
                     
                },
                error:function(data){
                    if (data.status==500) {
                      $("#alert_error").trigger("click", 'Info ICT');
                    }                    
                }
            });
        }
      });
     /* */
    });
    $('#sewing_complete').click(function () {
      swal({
        title: "PERHATIAN!!!",
        text: "Apakah Anda yakin karton Akan di Komplit?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((wilsave) => {
        if (wilsave) {
          var data = 'barcode_id='+$('#barcodePackage').val(); 
          $.ajax({
                url:'<?=base_url('folding/submit_sewing_complete') ?>',
                data:data,
                type:'POST',
                beforeSend: function () {
          
                  $.blockUI({
                    message: "<img src='" + url_loading + "' />",
                    css: {
                      backgroundColor: 'transaparant',
                      border: 'none',
                    }
                  });
                },                  
                success:function(data){
                    var result = JSON.parse(data);
                    if (result.status==200) {
                      $.unblockUI();
                      $("#alert_success").trigger("click", result.pesan);
                      $('#sewing_complete').prop('disabled',true);
                      $('#sewing_in').prop('disabled',true);
                    }else if (result.status==500){
                      $.unblockUI();
                      $('#sewing_in').prop('disabled',true);
                      $('#sewing_complete').prop('disabled',true);
                      $("#alert_error").trigger("click", result.pesan);
                    }
                     
                },
                error:function(data){
                    if (data.status==500) {
                      $("#alert_error").trigger("click", 'Info ICT');
                    }                    
                }
            });
        }
      });
    });
  });
</script>