<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table-size" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">NO</th>
                        <th>SIZE</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no=1; 
                        foreach ($list_size->result() as $list) {
                            $temp = '"'.$list->size.'","'.$list->header_size_id.'","'.$list->poreference.'","'.$list->inline_header_id.'","'.$list->style.'"';
                            echo "<tr onclick='return selectsize($temp)'>";
                                echo "<td>".$no++."</td>";
                                echo "<td>".$list->size."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script>
 $(function(){
    $('#table-size').DataTable({
        'paging':true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
    });
  });
 
 function selectsize(sized,size_id,po,header_id,style) {
    var url_loading = $('#loading_gif').attr('href');
    $('#size').val(sized);
    $('#size_id').val(size_id);
    $("#myModal").modal('hide');
        var size = $('#size').val();
        var url_loading = $('#loading_gif').attr('href')
        if (!po && !size) {
            $("#alert_warning").trigger("click", 'PO BUYER TIDAK BOLEH KOSONG');
        }else{
            var data = 'id='+header_id+'&poreference='+po+'&size_id='+size_id+'&style='+style;

            $.ajax({
                url:'Folding/searchsubmitfolding',
                type: "POST",
                data: data,
                beforeSend: function () {
                    $.blockUI({
                        message: "<img src='" + url_loading + "' />",
                        css: {
                            backgroundColor: 'transaparant',
                            border: 'none',
                        }
                    });
                },
                success: function(response){
                  if (response=='gagal') {
                    $("#alert_warning").trigger("click", 'Data Tidak Ada');
                    $.unblockUI();
                  }else{
                    $('#result').html(response);
                    $.unblockUI();
                  }
                },
                error:function(response){
                  if (response.status==500) {
                    $("#alert_info").trigger("click", 'Cek Input Data');
                    $.unblockUI();
                  }
              }
            });
        }    
    return false;
 }
</script>