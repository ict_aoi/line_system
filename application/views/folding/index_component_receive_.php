<div class="panel panel-default border-grey">
 <div class="panel-body"> 
  <div class="box-body">
    <div style="overflow-x:auto;overflow-y:auto">
       <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <th class="text-center" width="20px">NO</th>
                  <?php if ($trust==1): ?>
                    <th class="text-center" width="50px">BARCODE</th>  
                  <?php endif ?>
                  
                  <th class="text-center" width="50px">NAMA KOMPONEN</th>
                  <th class="text-center" width="80px">TANGGAL TERIMA</th>
                  <th class="text-center" width="80px">DI TERIMA</th>
                  <th class="text-center" class="text-center"width="80px">TANGGAL PINDAH</th>
                  <th class="text-center" width="80px">DI PINDAH</th>
                  <th class="text-center" width="80px">STATUS</th>
              </tr>
          </thead>
          <tbody>
            <?php echo $draw ?>

          </tbody>
      </table>
    </div>
  </div>
</div>
</div>