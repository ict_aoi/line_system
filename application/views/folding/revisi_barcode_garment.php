<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <form class="barcode_submit" method="POST">
            <label for="changebarcode">Barcode Lama</label>
            <input name="oldbarcode" class="form-control changebarcode" value="<?=$barcode_garment ?>" readonly>
            <input type="hidden" name="header_id" value="<?=$header_id;?>">
            <input type="hidden" name="flag_barcode" value="<?=$flag_barcode;?>">
            <input type="hidden" name="size_id" value="<?=$size_id;?>">
            <label>Scan Barcode Baru</label>
            <input type="number" name="newbarcode" class="form-control newbarcode" required autofocus="true">
            <button id="submit" class="btn btn-primary"><i class="fa fa-floppy-o"> Simpan</i></button>
          </form>
        </div>
    </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
      $('.barcode_submit').submit(function(){
            var data = new FormData(this);
            var newbarcode = $('.newbarcode').val();
            
            $.ajax({
                url:'Folding/updatebarcode',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(data) {
                  var result = JSON.parse(data);
                  if (result.status == 200) {
                      $('#barcode_garment').val(result.barcode);
                      $("#alert_success").trigger("click", result.pesan);
                      $("#myModal").modal('hide');
                  } else if (result.status == 500) {
                      $("#alert_warning").trigger("click", result.pesan);
                  }
                },
                error: function(data) {
                    if (data.status == 500) {
                        $.unblockUI();
                        $("#alert_warning").trigger("click", 'info ict');
                    }
                }
            })
        return false;
        });
    });
</script>