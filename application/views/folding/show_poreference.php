<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="col-md-2">
      <br>

    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table-po" class="table table-bordered table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">NO</th>
                        <th>PO BUYER</th>
                        <th>STYLE</th>
                        <th>ARTICLE</th>
                        <th width="20px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                  <?php  
                    $no=1;

                    foreach ($records->result() as $key => $record) {
                      $temp = '"'.$record->inline_header_id.'","'.$record->poreference.'","'.$record->style.'"';
                      echo "<tr>";
                        echo "<td>".$no++."</td>";
                        echo "<td>".$record->poreference."</td>";
                        echo "<td>".$record->style."</td>";
                        echo "<td>".$record->article."</td>";
                        echo "<td><center><a onclick='return selectpo($temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center></td>";
                      echo "</tr>";
                    }
                  ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
      $('#table-po').DataTable({
          'paging':true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
      });
         /*var table =$('#table-po').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax":{
             "url": "Folding/loadporeference_ajax",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
             'line_id':$('#line_id').val(), },

                           },
        "columns": [
                  {"data" : null, 'sortable' : false},
                  { "data": "poreference",'sortable' : false },
                  { "data": "style",'sortable' : false },
                  { "data": "action",'sortable' : false },
               ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });*/
    });
    function selectpo(id,po,style) {
    var url_loading = $('#loading_gif').attr('href');
    $('#id').val(id);
    $('#poreference').val(po);
    $('#style').val(style);
    $("#myModal").modal('show');    
    $('.modal-title').text("Pilih Size");
    $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
    $('.modal-body').load('folding/showsize?po='+po+'&id='+id);
    return false;
 }
</script>