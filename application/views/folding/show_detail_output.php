<style type="text/css">
 
.modal-lg {
     width: 1200px;
    }
</style>


<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
        
            <input type="hidden" id="poref" value="<?=$poreference;?>">
            <input type="hidden" id="po_size" value="<?=$size;?>">
            <div class="row">
                <!-- <div class="col-md-4">
                    <div class="input-group">
                        <input type="text" id="numpadButton" class="form-control karton" placeholder="Masukan 5 digit karton terakhir" aria-describedby="numpadButton-btn">
                        <span class="input-group-btn">
                            <button class="btn btn-default" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
                        </span>
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary tampilkan"><i class="fa fa-file-excel-o"></i> Tampilkan</button>
                    </div>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4"></div> -->
            </div>
            <table id="table-detail" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <!-- <th class="text-center">NO</th> -->
                        <th class="text-center">TANGGAL COMPLETE</th>
                        <th class="text-center">PO</th>
                        <th class="text-center">STYLE</th>
                        <th class="text-center">SIZE</th>
                        <th class="text-center">TOTAL KARTON (ctn)</th>
                        <th class="text-center">TOTAL GARMENT (pcs)</th>
                      
                        <!-- <th class="text-center">NO</th>
                        <th class="text-center">PO</th>
                        <th class="text-center">STYLE</th>
                        <th class="text-center">SIZE</th>
                        <th class="text-center">QTY GARMENT</th>
                        <th class="text-center">QTY KARTON</th>
                        <th class="text-center">BARCODE KARTON</th>
                        <th class="text-center">LINE</th>
                        <th class="text-center">TANGGAL TERIMA</th>
                        <th class="text-center">TANGGAL COMPLETE</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        if($detail==0){
                        }
                        else{
                        $i=1;
                        foreach($detail as $m) {
                            $temp = '"'.$m->poreference.'","'.$m->size.'"';?>
                            <tr>
                                <td><?php echo $m->complete_at;?></td>
                                <td><?php echo $m->poreference;?></td>
                                <td><?php echo $m->style;?></td>
                                <td><?php echo $m->size;?></td>
                                <td><?php echo $m->total_karton;?></td>
                                <td><?php echo $m->total;?></td>
                            </tr>
                    <?php }
                    }?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
<script>
$(document).ready(function () {

    var table =$('#table-detail').DataTable();
        // {
    //     "processing": true,
    //     "serverSide": true,
    //     // "order": [],
    //     "orderMulti"  : true,
    //     "searching" : false,
    //     "ajax":{
    //      "url": "folding_detail_ajax",
    //      "dataType": "json",
    //      "type": "POST",
    //      "beforeSend": function () {
    //             $.blockUI({
    //               message: "<img src='" + url_loading + "' />",
    //               css: {
    //                 backgroundColor: 'transparant',
    //                 border: 'none',
    //               }
    //             });
    //           },
    //           "complete": function() {
    //             $.unblockUI();
    //           },
    //           fixedColumns: true,
    //          "data":function(data) {
    //             data.pobuyer = $('#poref').val();
    //             data.size    = $('#po_size').val();
    //             data.karton  = $('.karton').val();
    //             // console.log(data.pobuyer);
    //             // console.log(data.size);
    //             data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
    //           },
    //                },
    //     "columns": [
    //               { "data" : null, 'sortable' : false},
    //               { "data": "poreference",'sortable' : false },
    //               { "data": "style",'sortable' : false },
    //               { "data": "size",'sortable' : false },
    //               { "data": "qty_size",'sortable' : false },
    //               { "data": "qty_karton",'sortable' : false },
    //               { "data": "barcode_package" },
    //               { "data": "nama_line",'sortable' : false },
    //               { "data": "create_on" },
    //               { "data": "complete_on" },
    //           ],
    //           fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    // });
    // $('#table-detail_filter input').unbind();
    // $('#table-detail_filter input').bind('keyup', function(e) {
    //     if (e.keyCode == 13 || $(this).val().length == 5 ) {
    //         table.search($(this).val()).draw();
    //     }
    //     // if ($(this).val().length == 0 || $(this).val().length >= 3) {
    //     //     table.search($(this).val()).draw();
    //     // }
    // });

    // $('.tampilkan').click(function () {
    //     table.draw();
    // });
  });
</script>