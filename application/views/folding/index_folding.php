<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<script type="text/javascript">
    $(function(){
        setInterval(function(){
            var d = new Date();
            var detik = d.getSeconds();
            var menit = d.getMinutes();
            var jam = d.getHours();
            $('.tgl > span').html(jam +':'+ nol(menit));
        },30000);
    })
    function nol(num){
        if(num < 10)
            return '0' + num;
        else
            return num;
    }
</script>
<style type="text/css">
 
.modal-lg {
     width: 850px;
    }
</style>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="form-group"> 
                    <div class="row"> 
                      <div class="col-md-6">
                        <label></label> 
                        <h3>Folding - <?php echo $line['line_name']; ?></h3> 
                      </div> 
             
                      <div class="col-md-6"> 
                        <label></label> 
                        <div class="pull-right tgl"><?=date('d-m-Y');?> <span><?=date('H:i');?></span></div>
                       
                      </div> 
                    </div> 
                </div> 
                
                <div class="col-md-4 pull-right">
                    <div class="btn-row">
                        <div class="btn-group">
                            <!-- <button type="button" class="btn btn-primary" id="openReport">Tambah</button> -->
                            <a href="<?php echo base_url('Folding/showOutputFolding'); ?>">
                                <span class="btn btn-success"><i class="fa fa-book"></i> Laporan Folding Output</span>
                            </a>
                            <!-- <button type="button" class="btn btn-success" onclick="return outputfolding()" id="outputfolding"><i class="fa fa-calculator"></i>Report OutputFolding</button> -->
                        </div>
                    </div>
                </div>
               <div class="col-md-4">
                    
                    <!-- <button class="btn btn-info" id="view_result">Summary Cek</button> --> 
              </div>  
              <form class="search_header hidden" method="POST">
                  <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>PO BUYER</th>
                            <th>SIZE</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-info" onclick="return loadpo()"><i class="fa fa-list"></i></button>
                            </div>
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" name="style" id="style">
                            <input type="text" class="form-control" name="poreference" id="poreference" onclick="return loadpo()" readonly="readonly" placeholder="PILIH PO BUYER">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default btn-icon" id="clear_po"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                            </div>                
                        </div>   
                        </td>
                        <td>
                          <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-info" onclick="return loadsize()"><i class="fa fa-list"></i></button>
                            </div>
                            <input type="text" class="form-control" name="size" style="left" id="size" onclick="return loadsize()" readonly="readonly" placeholder="PILIH SIZE">
                            <input type="hidden" id="size_id" name="size_id">
                        </td>
                      </tr>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-search"></i> Cari</button>
              </form>
              <input type="text" class="form-control" autofocus onClick="this.select();" id="scan_karton" placeholder="Scan Barcode Karton disini" autocomplete="off">
              <br>
              <div class="row">
                  <div class="form-group">        
                      <div class="col-md-12">
                      <button type="submit" class="btn btn-primary col-md-2" id="change"><i class="fa fa-retweet"></i></button>
                      </div>
                  </div>
                  <input type="hidden" id="flag" value="0">
                  <input type="hidden" id="flag_qtyscan" value="0">
              </div> 
                    
            </div>
        </div>
    </div>
    <div id="result">
        
    </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/folding_chose.js"></script>