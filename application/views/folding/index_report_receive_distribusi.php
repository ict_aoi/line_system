<style type="text/css">
 
.modal-lg {
     width: 900px;
    }
</style>
<div class="panel panel-default">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"> <i class="fa fa-database"> Laporan Receive Loading</i></h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=base_url('folding');?>">home</a></li>
        <li><i class="fa fa-bars"></i>Laporan Receive Loading</li>
      </ol>
    </div>
  </div>
</div>
<div class="panel panel-default border-grey"> 
  <div class="panel-body"> 
    <div class="box box-success">

      <div class="box-header with-border">
      <div class="col-md-3">
        <div class="box-title">Pilih Tanggal Laporan</div>
          <div class="box-body">
            <div class="input-group">
              <input class="form-control" name="tanggal" id="tanggal" autocomplete="off" required>
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="clear"><i class="fa fa-times"></i></button>
              </span>
            </div>
            <div class="clearfix"></div><br>
            <div class="input-group">
              <input type="text" id="numpadButton" class="form-control poreference" placeholder="Masukan PO BUYER" aria-describedby="numpadButton-btn">
              <span class="input-group-btn">
                <button class="btn btn-default" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
              </span>
            </div>
            <div class="clearfix"></div><br>
            <div class="result"></div>
            <div class="clearfix"></div><br>
            <div class="text-right">
              <button type="button" class="btn btn-primary tampilkan"><i class="fa fa-file-excel-o"></i> Tampilkan</button>
            </div>
          </div>
        </div>
      <!-- </div> -->
      <div class="col-md-6">
        
            <div class="hasil_summ"> </div>
          </div>
        </div>
      </div>
      </div>
  <!-- </div> -->
  <div class="clearfix"></div>
  <div class="box-body">
    <div style="overflow-x:auto;overflow-y:auto">
       <table id="table-result" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <th class="text-center" width="20px">NO</th>
                  <th class="text-center">STYLE</th>
                  <th class="text-center" width="100px">PO BUYER</th>
                  <th class="text-center" width="80px">SIZE</th>
                  <th class="text-center" width="80px">ARTICLE</th>
                  <th class="text-center" width="50px">QTY</th>
                  <th class="text-center" width="50px">CUTING NUMBER</th>
                  <th class="text-center" width="50px">NO STICKER</th>
                  <th class="text-center" width="80px">TANGGAL</th>
                  <th width="80px">STATUS</th>
                  <th width="80px">DETAIL</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
    </div>
  </div>
  </div> 
</div>
<script type="text/javascript">
  var url_loading = $('#loading_gif').attr('href');
  var base_url = "<?php echo base_url();?>";
  
  $(function(){
      $('#tanggal').datepicker({
          format    : "yyyy-mm-dd",
          autoclose : true
      });

      var url_loading = $('#loading_gif').attr('href');
      var table       = $('#table-result').DataTable({
            "processing"  : true,
            "serverSide"  : true,
            "orderMulti"  : true,
            "searching"   : false,
            "order"       : [[ 0, "desc" ]],
            sDom          : '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
            "ajax"        : {
                  "url"        : "<?=base_url();?>Distribusi/report_receive_ajax",
                  "dataType"   : "json",
                  "type"       : "POST",
                  "beforeSend" : function () {
                    $.blockUI({
                      message : "<img src='" + url_loading + "' />",
                      css     : {
                        backgroundColor : 'transparant',
                        border          : 'none',
                      }
                    });
                  },
                  "complete"  : function() {
                      $.unblockUI();
                  },
                  fixedColumns: true,
                  "data"      : function(data) {
                      data.tanggal     = $('#tanggal').val();
                      data.poreference = $('.poreference').val();
                      data.pilih_size  = $('#pilih_size').val();
                      data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                  },
            },
            "columns"   : [
                  { "data" : null, 'sortable' : false},
                  { "data" : "style" },
                  { "data" : "poreference" },
                  { "data" : "size" },
                  { "data" : "article" },
                  { "data" : "qty" },
                  { "data" : "cut_num" },
                  { "data" : "sticker" },
                  { "data" : "create_date" },
                  { "data" : "status" },
                  { "data" : "detail",'sortable' : false  },
               ],
               fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }               
        });
      
        // var summary = $('#table-summary').DataTable();      
      
        $('.tampilkan').click(function () {
          table.draw();
          var data = 'poreference='+$('.poreference').val()+'&size='+$('#pilih_size').val();
          $.ajax({
            url   : 'report_summary_ajax',
            type  : 'post',
            data  : data,
            beforeSend  : function () {
              $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transaparant',
                  border: 'none',
                }
              });
            },
            success   : function (data) {
              var result = JSON.parse(data);              
              $.unblockUI();
              $('.hasil_summ').html(result.draw);
            }
          });
        });

        $('#clear').click(function () {
          $('#tanggal').val('');
        });

        $('.poreference').change(function () {
          var data = 'poreference='+$('.poreference').val();
          $.ajax({
            url         : 'showSize',
            type        : 'post',
            data        : data,
            beforeSend  : function () {
              $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transaparant',
                  border: 'none',
                }
              });
            },
            success     : function (data) {
              var result = JSON.parse(data);
              $.unblockUI();
              $('.result').html(result.draw);
            }
          });
        });
  });
  
  function detail(po,size,cut_num,start_num,start_end,style,distribusi_id,article) {
      var url_loading = $('#loading_gif').attr('href');
      var modal       = $('#myModal_ > div > div');
      $('#myModal_').modal('show');
      modal.children('.modal-header').children('.modal-title').html('Detail');
      modal.parent('.modal-dialog').addClass('modal-lg');
      modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
      modal.children('.modal-body').load('<?=base_url();?>Distribusi/detailComponent?poreference='+po+'&size='+encodeURIComponent(size)+'&cut_num='+cut_num+'&start_num='+start_num+'&start_end='+start_end+'&style='+style+'&distribusi_id='+distribusi_id+'&article='+encodeURIComponent(article));
      return false;
  }
</script>