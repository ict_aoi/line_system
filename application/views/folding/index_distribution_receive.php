<style type="text/css">
 
.modal-lg {
     width: 1200px;
    }
</style>
<script type="text/javascript">
    $(function(){
        setInterval(function(){
            var d       = new Date();
            var detik   = d.getSeconds();
            var menit   = d.getMinutes();
            var jam     = d.getHours();
            $('.tgl > span').html(jam +':'+ nol(menit));
        }, 30000);
    })

    function nol(num){
        if(num < 10)
            return '0' + num;
        else
            return num;
    }
</script>
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
            <a class="btn btn-primary" href="<?=base_url('Folding');?>" title="Kembali Ke halaman Folding"><span class="arrow_carrot-left_alt2"></span> Kembali</a>            
            <div class="col-md-12">
                <div class="form-group"> 
                    <div class="row"> 
                      <div class="col-md-6">
                        <h3>Receive Loading - <?php echo $line['line_name']; ?></h3>
                      </div>
                        <div class="col-md-6">
                            <div class="pull-right tgl"><?=date('d-m-Y');?> <span><?=date('H:i');?></span>
                            </div>
                        </div> 
                    </div> 
                </div>
                <div class="form-group"> 
                    <div class="row"> 
                        <div class="col-md-4">  
                            <div class="btn-row">
                                <div class="btn-group">
                                  <button type="button" onclick="return showreportreceive()" class="btn btn-success" id="outputfolding"><i class="fa fa-calculator"></i>Laporan Receive Distribusi</button>
                                </div>
                            </div> 
                        </div>
                      <div class="col-md-4 pull-right">
                            <span class="nama"><?php echo $nama; ?></span>-<span class="nik"><?php echo $this->session->userdata('nik_adm');; ?></span> 
                     </div>
                     
                    </div> 
                </div>                              
                <div style="overflow-y: auto; height:450px">
                <table class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="20px">NO</th>
                            <th class="text-center">STYLE</th>
                            <th class="text-center" width="100px">PO BUYER</th>
                            <th class="text-center" width="80px">SIZE</th>
                            <th class="text-center" width="80px">ARTICLE</th>
                            <th class="text-center" width="50px">QTY</th>
                            <th class="text-center" width="50px">CUTING NUMBER</th>
                            <th class="text-center" width="50px">NO STICKER</th>
                            <th class="text-center">KOMPONEN</th>
                            <th width="20px" width="80px">ACTION</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-items">
                    </tbody>
                </table>
            </div>
            </div>
            <form class="frmsave" method="POST">
            
            <input type="hidden"  name="items" id="items" value="[]">
            <input type="hidden"  name="nik" id="nik" value="<?php echo $this->session->userdata('nik_adm');;?>">
            
            <div class="col-md-12 ">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>
            </form>
        </div>
    </div>
    <?php  
        $this->load->view('folding/_itemsdetailloading');
    ?>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/receive_distribution.js?<?php echo time();?>"></script>
<script type="text/javascript" >
    function showreportreceive() {
        var url_loading = $('#loading_gif').attr('href');
        var modal       = $('#myModal > div > div');
        
        $('#myModal').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Report Receive Distribusi');
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
        modal.children('.modal-body').load('<?=base_url();?>Distribusi/ReportReceiveModal');
        return false;
    }
</script>
