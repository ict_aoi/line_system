
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Monitor</title>
	<script type="text/javascript">
	$(function(){
		setInterval(function(){ 
			var d = new Date();
			if((d.getMinutes() == 10 && d.getSeconds() == 0)){
				location.href = location.href;
			}

			$.ajax({
				url:"<?=base_url('display/data_line/'.$line.'/'.$factory.'');?>",
				cache:false,
				success:function(data){
					$('body').html(data);
				}
			});

		},65000);
	});
</script>
</head>
<body style="overflow: hidden;">
<?php

	echo dashboard_main($line,$factory);
?>
</body>
</html>