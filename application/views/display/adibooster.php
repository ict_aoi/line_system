<!DOCTYPE html>
<html>
<head>
	<title>EFFICIENCY & WFT</title>
	
    <link href="<?=site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <script src="<?=Base_url();?>assets/js/jquery.min.js"></script>
	<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
	<script src="https://www.amcharts.com/lib/3/serial.js"></script>
	<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
	<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />	
    <script src="<?=Base_url();?>assets/js/jquery.min.js"></script>
	<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
	<script type="text/javascript">
		$(function(){
			var url_adibooster_graph = $('#url_adibooster_graph').attr('href'); 
			setInterval(function(){ 
				var d = new Date();
				if((d.getMinutes() == 30 && d.getSeconds() == 0) || (d.getMinutes() == 0 && d.getSeconds() == 0))
					location.href = location.href;

				$.ajax({
					url: url_adibooster_graph,
					cache:false,
					success:function(data){
						$('.view').html(data);
					}
				});

			},5000);
		});
	</script>
	<style type="text/css">
		body,
		html{
			height: 100%;
		}
		#chartdiv{
			border: 0px solid #000;
			height: 100%
		}
		.btns{
			position: fixed;
			top: 10px;
			left: 10px;
		}
	</style>
</head>
<body>
		<div class="view" style="height: 100%; border: 0px solid #000">
			<!-- <?php $this->load->view('display',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto)); ?> -->
		</div>
		<div class="btns">
		<div class="btn-group">
			<a href="<?=base_url('display/adibooster_graph/'.$showlinefrom.'/'.$showlineto.'');?>" id="url_adibooster_graph" class="hidden"></a>
			<a href="<?=base_url('display/adibooster_eff/'.$showlinefrom.'/'.$showlineto.'');?>" class="btn btn-primary btn-lg">EFFICIENCY</a>
			<a href="<?=base_url('display/adibooster_wft/'.$showlinefrom.'/'.$showlineto.'');?>" class="btn btn-success btn-lg">WFT</a>
		</div>
		</div>
</body>
</html>