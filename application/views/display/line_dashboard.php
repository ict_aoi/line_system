<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Inline - System">
    <meta name="author" content="ICT">
    <meta name="keyword" content="Inline - System">
    <link rel="shortcut icon" href="<?php base_url() ?>assets/img/favicon.jpg">

    <title><?=$line_name?> - Inline System </title>

    <!-- Bootstrap CSS -->    
    <link href="<?=site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?=Base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?=Base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/owl.carousel.css" type="text/css">
    <link href="<?=Base_url();?>assets/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <!-- <link rel="stylesheet" href="<?=Base_url();?>assets/css/fullcalendar.css"> -->
    <link href="<?=Base_url();?>assets/css/widgets.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=Base_url('assets/plugins/chosen/chosen.min.css');?>" type="text/css">
    <script src="<?=Base_url();?>assets/js/jquery.min.js"></script>

    <script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.rowspanizer.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/plugins/mustache.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/resource/notification.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/datepicker/css/bootstrap-datepicker.min.css');?>">
	<script type="text/javascript" src="<?=base_url('assets/plugins/datepicker/js/bootstrap-datepicker.min.js');?>"></script>
    
  </head>


<script>
	function startTime() {
		    var today = new Date();
		    var h = today.getHours();
		    var m = today.getMinutes();
		    var s = today.getSeconds();
		    m = checkTime(m);
		    s = checkTime(s);
		    h = checkTime(h);
		    $('.time').text(h + ":" + m + ":" + s);
		    var t = setTimeout(startTime, 500);
		    //jika jam menunjukan pukul 6||12||15 maka halaman akan di reload
		    if((h == 06 || h == 13 || h == 15) && m == 00 && s == 00){
		    	location.href = location.href;
		    }
		}
	function checkTime(i) {
		if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		return i;
	}
</script>


<style>
	table.borderless td,table.borderless th{
		border: none !important;
	}

	/* CSS Document */

	marquee {
		margin-top: 5px;
		width: 100%;
	}

	.runtext-container {
	/* background-color:#ffffff; */
	/* *background-color:#ccf;
	background-image:-moz-linear-gradient(top,#ccf,#fff);
	background-image:-webkit-gradient(linear,0 0,0 100%,from(#ccf),to(#fff));
	background-image:-webkit-linear-gradient(top,#ccf,#fff);
	background-image:-o-linear-gradient(top,#ccf,#fff);
	background-image:linear-gradient(to bottom,#ccf,#fff);
	background-repeat:repeat-x; */
		/* border: 4px solid #000000; */
		/* box-shadow:0 5px 20px rgba(0, 0, 0, 0.9); */

	height: 50px;
	overflow-x: hidden;
	overflow-y: visible;
	/* margin: 0 60px 0 30px; */
	padding:0 3px 0 3px;
	}

	.main-runtext {margin: 0 auto;
	overflow: visible;
	position: relative;
	height: 30px;
	}

	.runtext-container .holder {
	position: relative;
	overflow: visible;
	display:inline;
	float:left;

	}

	.runtext-container .holder .text-container {
		display:inline;
	}

	.runtext-container .holder a{
		text-decoration: none;
		font-weight: bold;
		color:#0072CA;
		text-shadow:0 -1px 0 rgba(0,0,0,0.25);
		line-height: -0.5em;
		font-size:50px;
		height: 30px;
		line-height: 30px;
		text-align: center;
	}

	span {
		display: inline-block;
		vertical-align: middle;
		line-height: normal;
	}

	.runtext-container .holder a:hover{
		text-decoration: none;
		color:#6600ff;
	}
</style>


<body onload="startTime()" style="overflow: hidden;">
<!-- <div > -->
<div class="panel panel-default border-grey">
  <!-- <form id="form_download"> -->
 <div class="panel-body">
   <!-- <div class="col-xs text-center"> -->
   <!-- <div class="col-xs"> -->
        <!-- <button type="button" class="btn btn-primary btn-lg">Large button</button> -->
 		<!-- <div class="col-xs-12"><h1>DASHBOARD</h1></div><br> -->
		<!-- <div class="row"> -->
			<!-- <div class="panel"> -->
				<!-- <div class="cold-md-4"> -->
					<!-- <div class="btn btn-warning btn-lg"> LINE 2 <?php //echo $line_name;?></div> -->
				<!-- </div>
				<div class="cold-md-4">
				</div>
				<div class="cold-md-4">
				</div> -->
			<!-- </div> -->
		<!-- </div> -->
 		<!-- <div class="col-xs btn btn-warning btn-lg"> 21<?php //echo $line_name;?></div> -->
        
 	<!-- </div>
     <br> -->
    <div class="row alert" style="background-color: #0072CA">
		<!-- <div class="col-xs-6 col-md-2"> -->
			<!-- <div class="btn btn-warning btn-xl"> LINE 2 <?php //echo $line_name;?></div> -->
			<!-- <div class="alert alert-success text-center"> -->
				<!-- <button type="button" class="btn btn-danger"><h1><strong>LINE 12</strong></h1></button> -->
						<!-- <h2>LINE 12</h2> -->
						<!-- <h1><strong>LINE 12</strong></h1> -->
						<!-- <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p> -->
			<!-- </div> -->
		<!-- </div> -->
		<div class="col-xs-6 col-md-12">
 			<div class="row ">
			 	<div class="col-md-2">
				 	<table id="table-result" class="table borderless table-full-width">
						<tr>
							<td class="text-center">
								<br>
								<button type="button" class="btn" style="background-color: #75e3c8"><h1 style="color: #125a49"><b><?php echo $line_name;?></b></h1></button>
								<div class="time" style="color: #FFFFFF; font-size: 30px;"><?php echo date("H:i:s"); ?></div>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-md-10">
					<div class="table-responsive">
					<!-- <br> -->
						<table id="table-result" class="table table-full-width" cellspacing="0" width="100%">
							<tr>
								<!-- <td class="text-center"rowspan="3"width="100" >								
									<button type="button" class="btn" style="background-color: #75e3c8"><h1 style="color: #125a49"><b><?php echo $line_name;?></b></h1></button>
									<div class="time" style="color: #FFFFFF; font-size: 30px;"><?php //echo date("H:i:s"); ?></div>
								</td> -->
								<td class=" alert-link"width="250" style="border: 0px; color: #FFFFFF; font-size: 25px;">LAST UPDATED</td>
								<td class=" alert-link"width="500" style="border: 0px; color: #FFFFFF; font-size: 25px;">&nbsp<?php echo $last_update?></td>
								<td class=" alert-link"width="250" style="border-top: 3px solid red; border-bottom: 3px solid red; border-left: 3px solid red; color: #FFFFFF; font-size: 25px;">TARGET HARIAN</td>
								<td class=" alert-link"width="150" style="border-top: 3px solid red; border-bottom: 3px solid red; border-right: 3px solid red; color: #FFFFFF; font-size: 25px;"> <?php echo $today_target_harian;?></td>
								<!-- <td class="text-center"rowspan="3">
									<div class="time" width="200" style="border: 0px; color: #FFFFFF; font-size: 80px;"><?php //echo date("H:i:s"); ?></div>
								</td> -->
							</tr>
							<tr>
								<td class=" alert-link" width="200" style="border: 0px; color: #FFFFFF; font-size: 25px;" >STYLE</td>
								<td class=" alert-link" width="250" style="border: 0px; color: #FFFFFF; font-size: 25px;"><?php echo $style;?></td>
								<td class=" alert-link" width="200" style="border: 0px; color: #FFFFFF; font-size: 25px;">CUM DAY</td>
								<td class=" alert-link" width="50" style="border: 0px; color: #FFFFFF; font-size: 25px;"> <?php echo $cum_day;?></td>
							</tr>
							<tr>
								<td class=" alert-link" width="200" style="border: 0px; color: #FFFFFF; font-size: 25px;">SMV</td>
								<td class=" alert-link" width="250" style="border: 0px; color: #FFFFFF; font-size: 25px;"> <?php echo $smv;?></td>
								<td class=" alert-link" width="200" style="border: 0px; color: #FFFFFF; font-size: 25px;">MAN POWER</td>
								<td class=" alert-link" width="50" style="border: 0px; color: #FFFFFF; font-size: 25px;"> <?php echo $operator;?></td>
							</tr>						
						</table>
					</div>
				</div>
				
 			
	 		</div>
 	    </div>
 	</div>
	<!-- <div class="col-xs">
		<div class="row text-center">
			<br><br><br><br>
			<h3><b>Last Update : 11-06-2019<?php 	//echo date('Y-m-d H:i:s',strtotime($last_update)); ?></b></h3>
		</div>
	</div> -->
	<div class="col-xs-12">
		<!-- <br> -->
		<!-- <div class="col-md-6 pull-right"></div> -->
		<table class="table">
			<thead class="primary">
				<tr>
					<th colspan="3" class="col-xs-4 text-center info" style="font-size: 30px; border: 10px solid white; background-color: #57A2F8">
						<div class="runtext-container">
							<div class="main-runtext">
								<marquee direction="" onmouseover="this.stop();" onmouseout="this.start();">

									<div class="holder">

										<div class="text-container">
											<span>
												&nbsp; &nbsp; &nbsp; <a data-fancybox-group="gallery" class="fancybox" href="images/runtext/Electric_Lighting_Act.jpg" title="THE ELECTRIC LIGHTING ACT: section 35"><?php echo $pobuyer_text; ?></a>
											</span>
										</div>

										
										<div class="text-container">
											<span>
											&nbsp; &nbsp; &nbsp; <a data-fancybox-group="gallery" class="fancybox" href="images/runtext/Electric_Lighting_Act.jpg" title="THE ELECTRIC LIGHTING ACT: section 35"><?php echo $pobuyer_text; ?></a>
											</span>
										</div>

									</div>

								</marquee>
							</div>
						</div>
						
						<!-- <b style="color:white;">PO : <?php //echo $po_buyer;?> , DELIVERY DATE : <?php //echo $po_date;?></b> -->
					</th>
				</tr>
				<tr class="">
					<th class="col-xs-4 text-center" style="border: 10px solid white;"><h3><b></b></h3></th>
                    <th class="col-xs-4 text-center warning" style="font-size: 25px; border: 10px solid white;"><b style="color:black;">PENCAPAIAN (JAM <?php echo $pukul;?>)</b></th>
					<th class="col-xs-4 text-center warning" style="font-size: 25px; border: 10px solid white;"><b style="color:black;">AKUMULASI (JAM <?=$jam_pertama;?> - <?=$jam_terakhir;?>)</b></th>
                    <!-- <th class="col-xs-4 text-center success" style="font-size: 20px;"><h3><b>KUMULATIF</b></h3></th> -->
				</tr>
			</thead>
			<tbody>
				<tr style="border: 10px solid white;">
					<td class="warning" style="vertical-align: middle; font-size: 25px; border: 10px solid white;"><b style="color:black;">TARGET OUTPUT(pcs)</b></td>
                    <td class="text-center success" style="vertical-align: middle; font-size: 30px; border: 10px solid white;"><b style="color:black;"><?php echo $target_jam;?></b></td>
                    <td class="text-center success" style="vertical-align: middle; font-size: 30px; border: 10px solid white;"><b style="color:black;"><?php echo $target_akumulatif;?></b></td>
                    <!-- <td class="text-center success" style="vertical-align: middle; font-size: 30px;"><?php //echo //$today_pencapaian;?></td> -->
				</tr>
				<tr>
					<td class="warning" style="vertical-align: middle; font-size: 25px; border: 10px solid white;"><b style="color:black;">SEWING OUTPUT(pcs)</b></td>
					
                    <td class="text-center success" style="vertical-align: middle; font-size: 30px; border: 10px solid white;"><b style="color:black;"><?php echo $sewing_jam;?></b></td>
                    <td class="text-center success" style="vertical-align: middle; font-size: 30px; border: 10px solid white;"><b style="color:black;"><?php echo $sewing_min_1;?></b></td>
                    <!-- <td class="text-center success" style="vertical-align: middle; font-size: 30px;"><?php //echo //$today_pencapaian;?></td> -->
				</tr>
				<tr>
					<td class="warning" style="vertical-align: middle; font-size: 25px; border: 10px solid white;"><b style="color:black;">QC OUTPUT(pcs)</b></td>
                    <td class="text-center success" style="vertical-align: middle; font-size: 30px; border: 10px solid white;"><b style="color:black;"><?php echo $qc_jam;?></b></td>
					<td class="text-center success" style="vertical-align: middle; font-size: 30px; border: 10px solid white;"><b style="color:black;"><?php echo $qc_min_1;?></b></td>
                    <!-- <td class="text-center" style="vertical-align: middle; font-size: 40px;"><?php //echo $cum_pencapaian;?></td> -->
				</tr>
				<!-- <tr>
					<td style="vertical-align: middle; font-size: 40px;"><b>WFT(%)</b></td>
                    <td class="text-center" style="vertical-align: middle; font-size: 40px;"><?php //echo $wft;?></td>
				</tr> -->
                
			</tbody>
		</table>

        <!-- <table class="table table-bordered">
            <tr>
                <td colspan="9">
                INI BERITA 
                </td>
            </tr>
        </table> -->

		<div class="row">
			<!-- <div class="col-md-12 text-center">		 -->
				<div class="row alert" style="background-color: #75e3c8">
					<table class="table table-full-width">
						<tr>
							<td width="700" class="text-center" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 3px solid white; border-right: 0px solid white;">
								<a href='#' style="font-size: 30px; color:#125a49"><b>TARGET EFF(%)</b></a>
							</td>
							<!-- <td width="100">
								<a href='#' style="font-size: 30px; color:#125a49"><b>:</b></a>
							</td> -->
							<td width="300" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 0px solid white; border-right: 0px solid white;">
								<a href='#' style="font-size: 35px; color:#125a49"><b><?php echo $target_eff; ?></b></a>
							</td>
							
							<td width="100" style="border-top: 0px solid white; border-bottom: 0px solid white; border-left: 3px solid white; border-right: 3px solid white;">
								<a href='#' style="font-size: 30px; color:#125a49"><b></b></a>
							</td>

							<td width="700" class="text-center" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 0px solid white; border-right: 0px solid white;">
								<a href='#' style="font-size: 30px; color:#125a49"><b>TARGET WFT(%)</b></a>
							</td>
							<td width="300" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 0px solid white; border-right: 3px solid white;">
								<a href='#' style="font-size: 35px; color:#125a49"><b>3,0</b></a>
							</td>
						</tr>
					<!-- </table> -->
					<!-- <a href='#' style="font-size: 70px; color:#125a49"><b><?php //echo $text_berjalan; ?></b></a>	 -->
				<!-- </div> -->
				<!-- <div class="row alert" style="background-color: #75e3c8"> -->
					<!-- <table class="table table-border"> -->
						<tr>
							<td class="text-center" style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 3px solid white; border-right: 0px solid white;">
								<a href='#' style="font-size: 30px; color:#125a49"><b>ACTUAL EFF(%)</b></a>
							</td>
							<!-- <td>
								<a href='#' style="font-size: 30px; color:#125a49"><b>:</b></a>
							</td> -->
							<td style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 0px solid white; border-right: 0px solid white;">
								<a href='#' style="font-size: 35px; color:#125a49"><b><?php echo $actual_eff; ?></b></a>
							</td>
							
							<td style="border-top: 0px solid white; border-bottom: 0px solid white; border-left: 3px solid white; border-right: 3px solid white;">
								<a href='#' style="font-size: 30px; color:#125a49"><b></b></a>
							</td>
							<td class="text-center" style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 0px solid white; border-right: 0px solid white;">
								<a href='#' style="font-size: 30px; color:#125a49"><b>ACTUAL WFT(%)</b></a>
							</td>
							<!-- <td>
								<a href='#' style="font-size: 30px; color:#125a49"><b>:</b></a>
							</td> -->
							<td style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 0px solid white; border-right: 3px solid white;">
								<a href='#' style="font-size: 35px; color:#125a49"><b><?php echo $wft; ?></b></a>
							</td>
						</tr>
					</table>
				</div>			
				<h4><?php echo $this->input->ip_address(); ?></h4>
				<input type="hidden" name="flag_refresh" id="flag_refresh" value="0">	
			<!-- </div> -->
		</div>
  </div>
</div>
<!-- </div> -->
</body>

<script>
	$(function(){
		// setInterval(function(){ 
		// 	// var d = new Date();
		// 	// if((d.getMinutes() == 10 && d.getSeconds() == 0)){
		// 		location.href = location.href;
		// 	// }
		// },65000);
		getRand();
	});

	function getRand() {
		var min = 50,max = 60;
		var flag_refresh = $("#flag_refresh").val();
		// var url_line_performance = $("#url_line_performance").val();
		var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
		// alert('Wait for ' + rand + ' seconds');
		if (flag_refresh==0) {
			$("#flag_refresh").val(1);
		} else {
			window.location.href=location.href;
		}
		
		setTimeout(getRand, rand * 1000);

		// setTimeout(getRand, 300000);
	}
</script>
