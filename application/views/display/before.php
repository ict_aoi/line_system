<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Inline - System">
    <meta name="author" content="ICT">
    <meta name="keyword" content="Inline - System">
    <link rel="shortcut icon" href="<?php base_url() ?>assets/img/favicon.jpg">

    <title><?=$line_name?> - Inline System </title>

    <!-- Bootstrap CSS -->    
    <link href="<?=site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?=Base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?=Base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Base_url();?>assets/css/owl.carousel.css" type="text/css">
    <link href="<?=Base_url();?>assets/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <!-- <link rel="stylesheet" href="<?=Base_url();?>assets/css/fullcalendar.css"> -->
    <link href="<?=Base_url();?>assets/css/widgets.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?=Base_url();?>assets/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=Base_url('assets/plugins/chosen/chosen.min.css');?>" type="text/css">
    <script src="<?=Base_url();?>assets/js/jquery.min.js"></script>

    <script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?=Base_url();?>assets/js/jquery.rowspanizer.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/blockUI/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?=Base_url();?>assets/plugins/mustache.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/resource/notification.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/datepicker/css/bootstrap-datepicker.min.css');?>">
	<script type="text/javascript" src="<?=base_url('assets/plugins/datepicker/js/bootstrap-datepicker.min.js');?>"></script>
    
  </head>

<script>
	$(function(){
		setInterval(function(){ 
			// var d = new Date();
			// if((d.getMinutes() == 10 && d.getSeconds() == 0)){
				location.href = location.href;
			// }
		},65000);
	});
	function startTime() {
		    var today = new Date();
		    var h = today.getHours();
		    var m = today.getMinutes();
		    var s = today.getSeconds();
		    m = checkTime(m);
		    s = checkTime(s);
		    h = checkTime(h);
		    $('.time').text(h + ":" + m + ":" + s);
		    var t = setTimeout(startTime, 500);
		    //jika jam menunjukan pukul 6||12||15 maka halaman akan di reload
		    // if((h == 06 || h == 13 || h == 15) && m == 00 && s == 00){
		    // 	location.href = location.href;
		    // }
		}
		function checkTime(i) {
		    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		    return i;
		}
</script>
<style type="text/css">
	.strip{
		width: 100%;
		height: 40px;
		margin-bottom: 30px;
		background: #1A2732;
	}
	.strip:last-child{
		margin-bottom: 0px;
	}
	.tri-strip{
		border: 0px solid #000;
		line-height: 180px;
	}
	.line{
	font-weight: bold;
	font-size: 95px;
	line-height: 180px;
	color:#1A2732;
	position: relative;
	left: 40px;
}
.row{
	padding: 10px;
}
.pt{
	text-align: center;
	width: 100%;
	border: 0px solid #000;
	font-size: 40px;
	font-weight: bold;
	position: fixed;
	left: 0px;
	color:#1A2732;
	bottom: 40px;
}
</style>
<div class="row">
	
	<div class="pt">
		<body onload="startTime()">
		<div class="time"><?php echo date("H:i:s"); ?></div>
		<span><?php echo $factory_name ?></span>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="col-xs-4">
				<div class="tri-strip">
					<div class="strip"></div>
					<div class="strip"></div>
					<div class="strip"></div>
				</div>
			</div>
		</div>
		<div class="col-xs-3 text-center line"><span><?php echo $line_name ?></span></div>
			<div class="col-xs-4 pull-right">
				<div class="tri-strip">
					<div class="strip"></div>
					<div class="strip"></div>
					<div class="strip"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

