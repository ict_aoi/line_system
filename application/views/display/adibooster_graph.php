<div id="chartdiv" style="min-height: 650px"></div>                               
<script type="text/javascript">
  var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "titles": [{
        "text": "DAILY EFFICIENCY",
        "size": 15
    }, {
      "text": "LAST UPDATE <?=date('H:i:s');?>",
      "bold": false
    }],
    "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 150
    },
    "dataProvider": [                    
            /*<?php
            foreach($data['line'] as $a => $line){
                if($data['eff'][$a] < $data['tgt'][$a])
                    $color = '#FF0000';
                else
                    $color =  '#00FF00';
            ?>
            {
                "date": "<?=$line;?>",
                "efficiency": <?=$data['eff'][$a];?>,
                "target_eff": <?=$data['tgt'][$a];?>,
                "color":"<?=$color;?>"   
            },
            <?php } ?>*/

            ],

    "valueAxes": [{
        "id": "distanceAxis",
        "minimum":0,
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left",
        "title": "EFFICIENCY (%)"
    }],
    "graphs": [{
        "alphaField": "alpha",
        "balloonText": "ACTUAL: [[value]] ",
        "labelText": "[[value]]%",
        "labelPosition": "top",
        "fontSize":14,
        "labelRotation": 0,
        "dashLengthField": "dashLength",
        "fillAlphas": 0.6,
        "legendValueText": ": [[value]] %",
        "title": "ACTUAL",
        "type": "column",
        "valueField": "efficiency",
        "valueAxis": "distanceAxis",
        "colorField": "color"
    }, {
        "balloonText": "TARGET: [[value]]",
        "bullet": "diamond",
        "bulletBorderAlpha": 1,
        "useLineColorForBulletBorder": true,
        "bulletColor": "#FFFFFF",
        "dashLengthField": "dashLength",
        "labelText": "[[value]]%",
        "labelPosition": "top",
        "legendValueText": ": [[value]] %",
        "fontSize":14,
        "title": "TARGET",
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueField": "target_eff",
        "valueAxis": "latitudeAxis"
    }],
    "chartCursor": {
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
         "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
    },
    "categoryField": "date",
  "categoryAxis": {
    "gridPosition": "start",
    "fontSize":12,
    "gridAlpha": 0,
    "tickPosition": "start",
    "tickLength": 20
  },
    "export": {
      "enabled": false
     }
});
</script>