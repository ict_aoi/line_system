
<style type="text/css">
	body,
	html{
	}
	table{
		margin: 0px;
		padding: 0px;
		border: 1px;
		color:black;
		font-weight: bolder;
	} 
	.col-xs-1,
	.col-xs-2,
	.col-xs-3{
		border:0px solid #000;
		margin: 0px;
	}
	.col-xs-1,
	.col-xs-2,
	.col-xs-12{
		padding: 0px;
	}
	table{
		margin:0px;
	}
	.area{
		background: #EAEAEA;
		border: 0px solid #000;
		padding-bottom: 0px;
	}
	.title{
		font-weight: bold;
		font-size: 20px;
		text-align: center;
		border-bottom: 0px solid #000;
		background: #0000FF;
		line-height: 30px;
		color: #FAFAFA;
	}
	.title2{
		border-bottom: 1px solid #FFF;
		font-weight: bold;
		background: #0000FF;
		color: #FFF;
		line-height: 38px;
		font-size: 20px;
	}
	.value2{
		line-height: 38px;
		border-bottom: 1px solid #FFF;
		font-weight: bold;
		background: #EAEAEA;
		font-size: 20px;
	}
	.line-number{
		line-height: 120px;
		font-weight: bold;
		font-size: 30px;
		background: #fff44f;
		text-align: center;
		border-bottom: 6px solid #00f;

	}
	.line > .title{
		background: #0000FF;
		color: #FAFAFA;
	}
	td{
		font-size: 20px;
	}
	.count{
		font-size: 43px;
		text-align: center;
	}
</style>


<div class="panel panel-default border-grey">
  <!-- <form id="form_download"> -->
 <div class="panel-body">
   <div class="col-xs-1 line">
 		<div class="col-xs-12 title">LINE</div>
 		<div class="col-xs-12 line-number"><?php echo $line_name;?></div>
 	</div>
 	<div class="col-xs-3">
 		<div class="row">
 			<div class="col-xs-6 title2">DATE</div>
 			<div class="col-xs-6 value2"><?=date("d-m-Y");?></div>
 			<div class="col-xs-6 title2">STYLE</div>
 			<div class="col-xs-6 value2"><?php echo $style;?></div>
 			<div class="col-xs-6 title2">ORDER QTY</div>
 			<div class="col-xs-6 value2"><?=$detail['order_qty'];?></div>
 			<div class="col-xs-6 title2">GSD SMV</div>
 			<div class="col-xs-6 value2"><?=$detail['gsd_smv'];?></div>
 		</div>
 	</div>
 	<div class="col-xs-3">
 		<div class="row">
 			<div class="col-xs-6 title2">TARGET/HOUR</div>
 			<div class="col-xs-6 value2"><?php echo round($target_hour) ?></div>
 			<div class="col-xs-6 title2">CUM DAY</div>
 			<div class="col-xs-6 value2"><?=$detail['cumulative_day'];?></div>
 			<div class="col-xs-6 title2">OPERATOR</div>
 			<div class="col-xs-6 value2"><?=$detail['present_sewer'];?></div>
 		</div>
 	</div>
	<div class="col-xs-3 pull-right">
		<div class="row">
			<br><br><br><br>
			<h3><b>Last Update : <?php 	echo date('Y-m-d H:i:s',strtotime($last_update)); ?></b></h3>
		</div>
	</div>
	<div class="col-xs-12">
		<br>
		<div class="col-md-6 pull-right"></div>
		<table class="table table-bordered">
			<thead class="bg-primary">
				<tr>
					<th colspan="2" class="col-xs-2">TIME</th>
					<?php foreach ($jam as $key => $jam): ?>
						<th class="text-center col-xs-1"><?=$jam['name'];?></th>	
					<?php endforeach ?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td rowspan="3" class="col-xs-1" style="vertical-align: middle;">OUTPUT</td>
					<td class="col-xs-1" style="vertical-align: middle;">SEWING (Pcs)</td>
					<?php echo $sumSewing;?><!-- 
					<td class='count' style='background:#CCC'></td> -->
				</tr>
				<tr>
					<td style="vertical-align: middle;">QC (Pcs)</td>
					<?php echo $sumqcendline;?>
				</tr>
				<tr>
					<td style="vertical-align: middle;">FOLDING (Pcs)</td>
					<?php echo $sumfolding;?>
				</tr>
				<tr>
					<td rowspan="2" style="vertical-align: middle;">EFFICIENCY</td>
					<td style="vertical-align: middle;">TARGET (%)</td>
					<?php echo $target;?>
				</tr>
				<tr>
					<td style="vertical-align: middle;">ACTUAL (%)</td>
					<?php echo $actual;?>
				</tr>
				<tr>
					<td rowspan="2" style="vertical-align: middle;">DEFECT</td>
					<td style="vertical-align: middle;">INLINE (Pcs)</td>
					<?php echo $sum_inline;?>
				</tr>
				<tr>
					<td style="vertical-align: middle;">ENDLINE (Pcs)</td>
					<?php echo $sum_endline;?>
				</tr>
				<tr>
					<td rowspan="2" style="vertical-align: middle;">WFT</td>
					<td style="vertical-align: middle;">TARGET (%)</td>
					<?php echo $wft_target;?>
				</tr>
				<tr>
					<td style="vertical-align: middle;">ACTUAL (%)</td>
					<?php echo $wft_actual;?>
				</tr>
			</tbody>
		</table>
		</div>
  </div>
</div>
