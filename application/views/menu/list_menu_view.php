<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <br>
        <div class="col-sm-2">
            <button id="tambah" class="btn btn-success" onclick="return tambah()">Tambah</button>
        </div>
        <br>
        <div class="panel-body">
            <table id="table-menu" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">NO</th>
                        <th>NAMA MENU</th>
                        <th>LINK</th>
                        <th>IS MAIN MENU</th>
                        <th width="50px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>
</div>
<button type="button" class="hidden" id="refresh"></button>
<script type="text/javascript" src="<?php echo base_url();?>assets/resource/menu.js"></script>