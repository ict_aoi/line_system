<form class="add_menu" method="POST">
    <label>Nama Menu</label>
    <input type="text" name="nama_menu" placeholder="MASUKAN NAMA MENU" class="form-control" required>
    <label>LINK</label>
    <input type="text" name="link" placeholder="masukan link" class="form-control" required>
    <label>Icon</label>
    <input type="text" name="icon" placeholder="Masukan Kode Icon" class="form-control">
    <label>Urut</label>
    <input type="number" name="urut" placeholder="Urut Menu" class="form-control">
    <label>IS MAIN MENU</label>
    <select name="is_main_menu" class="form-control">
        <option value="0">MAIN MENU</option>
        <?php
        $this->db->where('is_main_menu','0');
        $menu = $this->db->get('menu');
        foreach ($menu->result() as $row){
            echo "<option value='$row->menu_id'>$row->nama_menu</option>";
        }
        ?>
    </select>
    <div class="form-group">
        <label class="col-sm-2 control-label">

        </label>
        
        
    </div>
    <button type="submit" name="submit" class="btn btn-danger btn-sm bsave" id="save"><i class="fa fa-save"></i> SIMPAN</button>
    <button type="submit" name="submit" class="btn btn-success">KEMBALI</button>
</form>
<script>
    $(document).ready(function(){
        $('.add_menu').submit(function(){
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Menu/save_menu',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    if (response=='sukses') {
                         $("#myModal").modal('hide');
                         $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                         $('#refresh').trigger("click");
                    }
                }
        })
        return false;
    });
    });
</script>