<form class="edit_menu" method="POST">
    <input type="hidden" name="menu_id" value="<?=$record['menu_id'];?>">
    <label>Nama Menu</label>
    <input type="text" name="nama_menu" placeholder="MASUKAN NAMA MENU" id="form-field-1" class="form-control" value="<?=$record['nama_menu'];?>" required>
    <label>LINK</label>
    <input type="text" name="link" placeholder="masukan link" id="form-field-1" class="form-control" value="<?=$record['link'];?>" required>
    <label>Icon</label>
    <input type="text" name="icon" placeholder="Masukan Kode Icon" id="form-field-1" class="form-control" value="<?=$record['icon'];?>">
    <label>Urut</label>
    <input type="number" name="urut" placeholder="Urut Menu" class="form-control" value="<?=$record['urut'];?>">
    <label>IS MAIN MENU</label>
    <select name="is_main_menu" class="form-control">
        <option value="0">MAIN MENU</option>
        <?php
        $this->db->where('is_main_menu','0');
        $menu = $this->db->get('menu');
        foreach ($menu->result() as $row){
            echo "<option value='$row->menu_id'";
            echo $record['is_main_menu']==$row->menu_id?'selected':'';
            echo">$row->nama_menu</option>";
        }
        ?>
    </select>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-1">

        </label>
        
        
    </div>
    <button type="submit" name="submit" class="btn btn-danger btn-sm bsave" id="save"><i class="fa fa-save"></i> Edit</button>
    <button type="submit" name="submit" class="btn btn-success">KEMBALI</button>
</form>
<script>
    $(document).ready(function(){
        $('.edit_menu').submit(function(){
            var data = new FormData(this);
            
            $.ajax({
                url:'<?=base_url();?>Menu/edit_menu',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false, 
                success: function(response){
                    if (response=='sukses') {
                         $("#myModal").modal('hide');
                         $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                         $('#refresh').trigger("click");
                    }
                }
        })
        return false;
    });
    });
</script>