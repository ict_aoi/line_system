<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function chek_session()
{
    $CI= & get_instance();
    $session=$CI->session->userdata('logged_in');
    if($session!=1)
    {
        redirect('auth/login');
    }
}
function chek_session_login()
{
    $CI= & get_instance();
    $session=$CI->session->userdata('logged_in');
    if($session==1)
    {
        redirect('home');
    }
}
function bCrypt($pass,$cost){
      $chars='./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
 
      // Build the beginning of the salt
      $salt=sprintf('$2a$%02d$',$cost);
 
      // Seed the random generator
      mt_srand();
 
      // Generate a random salt
      for($i=0;$i<22;$i++) $salt.=$chars[mt_rand(0,63)];
 
     // return the hash
    return crypt($pass,$salt);
}
function cmb_dinamis($name, $table, $field, $pk, $extra = null,$order=null,$val='',$selected = null,$ex=null) {
    $ci = & get_instance();

    $cmb = "<select name='$name' class='form-control $extra' value='$val' $ex>";
            $ci->db->order_by($order);
    $data = $ci->db->get($table)->result();
        $cmb .="<option value='' class='form-control'>Pilih</option>";
    foreach ($data as $row) {
        $cmb .="<option value='" . $row->$pk . "'";
        $cmb .= $val == $row->$pk ? 'selected' : '';
        $cmb .=">" . ucwords($row->$field) . "</option>";
    }
    $cmb .= "</select>";
    return $cmb;
}
function cmb_dinamis2($name, $table, $field, $pk, $selected = null, $extra = null) {
    $ci = & get_instance();

    $cmb = "<select name='$name' class='form-control' $extra>";
    $data = $ci->db->get($table)->result();
        $cmb .="<option value='' class='form-control'>Pilih</option>";
    foreach ($data as $row) {
        $cmb .="<option value='" . $row->$pk . "'";
        $cmb .= $selected == $row->$pk ? 'selected' : '';
        $cmb .=">" . ucwords($row->$field) . "</option>";
    }
    $cmb .= "</select>";
    return $cmb;
}
function hakakses($seqment,$tuj,$title,$des)
{
  $CI        =& get_instance();
  $role      = $CI->uri->segment($seqment);
  $level     = $CI->session->userdata('role_id');

  $permision = $CI->db->get_where('menu',array('link'=>$role))->result()[0]->menu_id;

  $role_user = $CI->db->get_where('role_user', array('menu_id'=>$permision,'level_id'=>$level));

  if ($role_user->num_rows()==0) {
    return redirect('error_404','refresh');
  }else{
    $CI->template->set('title',$title);
    $CI->template->set('desc_page',$des);
    $CI->template->load('layout',$tuj);
  }
  
}
function chek_session_qc()
{
    $CI= & get_instance();
    $factory = $CI->session->userdata('factory');
    $line = $CI->session->userdata('mesin_id');
    $log = $CI->session->userdata('logged_in');

    if ($log=='qc') {
      
      if($factory==NULL||$line==NULL)
      {
          header('location:'.base_url('Auth/qc/'.$line));
      }
    }else{
      header('location:'.base_url('Auth/qc/'.$line));
    }
    
}
function chek_session_login_qc()
{
    $CI= & get_instance();
    $factory = $CI->session->userdata('factory');
    $line = $CI->session->userdata('mesin_id');
    $log     = $CI->session->userdata('logged_in');
    if ($log=='qc') {
      if($factory!=NULL&&$line!=NULL)
      {
          redirect('Qc');
      }
    }
}
function chek_session_folding()
{
    $CI= & get_instance();
    $factory = $CI->session->userdata('factory');
    $line = $CI->session->userdata('mesin_id');
    $log = $CI->session->userdata('logged_in');

    if ($log=='folding') {
      if($factory==NULL||$line==NULL)
      {
          header('location:'.base_url('auth/folding/'.$line));
      }
    }else{
      header('location:'.base_url('auth/folding/'.$line));
    }
    
}
function chek_session_login_folding()
{
   $CI      = & get_instance();
   $factory = $CI->session->userdata('factory');
   $line    = $CI->session->userdata('mesin_id');
   $log     = $CI->session->userdata('logged_in');
   if ($log=='folding') {
     if($factory!=NULL&&$line!=NULL)
      {
          redirect('folding');
      }
   }
    
}
function last_date_ofthe_month($month='', $year='')
{
  if(!$year)   $year   = date('Y');
  if(!$month)  $month  = date('m');
  $date = $year.'-'.$month.'-01';

  $next_month = strtotime('+ 1 month', strtotime($date));

  $last_date  = date('d', strtotime('-1 minutes',  $next_month));
  return $last_date;

}
function week_of_today()
{

  $month = date('m');
  $month = str_pad($month,2,'0',STR_PAD_LEFT);
  $today = date('Y-m-d');

  $minggu = 0;
  $week_end = 0;

  $last_date =  last_date_ofthe_month();

  for($i = 1; $i<=$last_date; $i++)
  {
    $i = str_pad($i,2,'0',STR_PAD_LEFT);
    $date =  date("Y-{$month}-{$i}");
    $day  =  date('D', strtotime($date));

    if($day == 'Sat')
    {
      $minggu = $minggu + 1;
    }
    if($date == $today)
    {
      $minggu = $minggu + 1;
      break;

    }
  }
  return $minggu;

}

function display_main($line=0,$factory=0)
{
  $CI =& get_instance();
  return $CI->data_line($line,$factory);
}

function dashboard_main($line=0,$factory=0)
{
  $CI =& get_instance();
  return $CI->data_line($line,$factory);
}



?>