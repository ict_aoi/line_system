<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Button extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Button_model');
	}

	public function index()
	{
		
	}
	function sewing($mesin=0,$factory=0){

		$line = $this->db->get_where('master_line', array('mesin_id'=>$mesin,'active'=>'t','factory_id'=>$factory))->row_array()['master_line_id'];
		// $quer = $this->db->get_where('master_line', array('mesin_id'=>$mesin,'active'=>'t','factory_id'=>$factory))->row();

		// $line = $quer->master_line_id;
		// $line_name = $quer->line_name;
		$line_name = $this->Button_model->cekLineName($line);
		
		$factory   = $this->Button_model->getFactory($factory);
		
		$active    = $this->Button_model->styleactive($line,$factory['factory_id']);

		$cek_style = $this->Button_model->cek_master_order($active['style']);
		
		$masker = $cek_style['value'];

		$data = array(
			'line_name'  =>$line_name, 
			'factory_id' =>$factory, 
			'active'     =>$active
		);

		if($masker == 'MASKER'){
			$this->load->view('button/sewing_masker',$data);
		}
		else{
			$this->load->view('button/sewing',$data);
		}
	}
	
	public function update_counter_sewing($line)
	{
		// if (!$this->input->is_cli_request()) {
		// 	redirect('auth','refresh');
		// }
		$cekBeda = $this->Button_model->cekBeda($line);
		foreach ($cekBeda->result() as $key => $cek) {
			$update_counter = "select * from update_counter_sewing('$cek->style',$cek->line_id)";
			$this->db->query($update_counter);
		}
	}

    public function sewing_status($line=0,$factory=0)
    {

		$line    = $this->input->post('line');
		$factory = $this->input->post('factory');
		$idle    = $this->input->post('idle');
		$style   = $this->db->get_where('status_sewing', array('line_id'=>$line,'factory_id'=>$factory))->row_array()['style'];

		// $header  = $this->Button_model->getHeader($line,$factory,$style);

		if($idle==1){
			$this->update_counter_sewing($line);
		}		

		$counter = $this->Button_model->counterSewingdaily($line,$factory,$style);
		
		// $balance = $counter-$header->row_array()['targetqty'];
		// var_dump($counter);
		$data = array(
			// 'balance' => $balance, 
			'counter' => $counter 
		);
		echo json_encode($data);
    }

    
    public function select_style_sewing($mesin=0,$factory=0)
    {	
    	$line = $this->db->get_where('master_line', array('mesin_id'=>$mesin,'active'=>'t','factory_id'=>$factory));
    	$data = $this->Button_model->getHeader($line->row_array()['master_line_id'],$factory);

    	
    	$this->load->view('button/select_style_sewing_view',array('mesin'=>$mesin,'factory'=>$factory,'record'=>$data,'line'=>$line->row_array()['line_name']));
    }
    public function select_styleactive_sewing($style,$line=0,$factory=0)
    {
    	$this->db->where(array('line_id'=>$line,'factory_id'=>$factory));
    	$cekStatus = $this->db->get('status_sewing');
    	$mesin_id = $this->db->get_where('master_line', array('master_line_id'=>$line,'active'=>'t','factory_id'=>$factory))->row_array()['mesin_id'];
    	if($cekStatus->num_rows() == 0){
    		$detail = array(
    			'line_id'    => $line,
    			'style'      => $style,
    			'factory_id' => $factory,
    			'ip'         => $this->input->ip_address()
    		);
    		$this->db->insert('status_sewing',$detail);
    	}else{
    		$this->db->where(array('line_id'=>$line,'factory_id'=>$factory));
    		$this->db->update('status_sewing',array('style'=>$style, 'ip' => $this->input->ip_address()));
    	}
    	header('location:'.base_url('button/sewing/'.$mesin_id.'/'.$factory));
    }
	// public function sewing_up($mesin=0,$factory=0)
	public function sewing_up($post=NULL)
    {
		$post = $this->input->post();
		if ($post!=NULL) {
			$line    = trim($post['line']);
			$factory = trim($post['factory']);
			$qty     = trim($post['qtyidle']);
			$style   = $this->db->get_where('status_sewing', array('line_id'=>$line,'factory_id'=>$factory))->row_array()['style'];
			
			$counter = $this->Button_model->counterSewing($line,$factory,$style);

			$c       = $counter+1;

			
			if ($line!='0') {
				for ($i=1;$i<=$qty;$i++){
					$detail = array(
						'style'       => $style,
						'line_id'     => $line,
						'factory_id'  => $factory,
						'counter_pcs' => $c,
						'ip'          => $this->input->ip_address()
					); 		
					$this->db->insert('sewing_pcs', $detail);

					$c++;
				}

				// $header  = $this->Button_model->getHeader($line,$factory,$style);

				// $balance = $c-$header->row_array()['targetqty'];
				// $counter_daily = $this->Button_model->counterSewingdaily($line,$factory,$style);

				
				// $this->db->trans_rollback();
				$status = 200;
				$pesan  = "OK";
			}else{
				$status = 200;
				$pesan  = "Insert Data Gagal";
			}
			$data = array(
				'status' => $status, 
				'pesan' => $pesan 
			);
			echo json_encode($data);

    	// $data = array(
		// 	// 'balance' => $balance, 
		// 	// 'counter' => $counter_daily,
		// 	'hasil'   => $hasil

		// );
		// echo json_encode($data);
    	}
	}
}

/* End of file Button.php */
/* Location: ./application/controllers/Button.php */
