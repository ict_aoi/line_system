<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('cookie');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		chek_session_login();
		$this->load->view('auth/login');

	}
	public function login()
	{
		$nik = $this->input->post('nik',TRUE);
		$password = $this->input->post('password',TRUE);

		$_cek_nik = $this->db->get_where('users',array('nik'=>$nik,'active'=>1));
		if ($_cek_nik->num_rows()>0) {
			$userdetail = $_cek_nik->result()[0];
			if ($userdetail->password == crypt($password,$userdetail->password)) {
				$login_data = array(
			        'nik'  => $nik,
			        'username'	=> $userdetail->username,		        			        
			        'role_id'	=> $userdetail->level_id,		        			        
			        'factory'	=> $userdetail->factory,		        			        
			        'logged_in' => TRUE,
				);
				$this->session->set_userdata($login_data);
				// $this->session->userdata('item');
				
				$ceklogin = $this->db->get_where('users',array('nik'=>$nik,'level_id'=>'d14965ea-299e-4984-9c1b-ab82721ba72c'));
				
				if ($ceklogin->num_rows()==0) {
					redirect('home','refresh');
										
				}else{
					redirect('qc_inspect','refresh');
				}				
			}else{
				redirect('auth','refresh');
			}
		}else{
			redirect('auth','refresh');
		}

	}
	public function logout(){
		$this->session->sess_destroy();
		delete_cookie('username'); delete_cookie('password');
		redirect('auth','refresh');
	}
	public function qc_login()
	{
		// chek_session_login_qc();
		
		if ($this->input->post('nik')!='') {
			$this->db->where('nik', $this->input->post('nik'));
			$cekNik = $this->db->get('master_sewer');
			if ($cekNik->num_rows()>0) {
				$detail = $cekNik->row_array();
					$factory = $this->db->get_where('master_factory',array('factory_name'=>$detail['factory']))->row_array();
				if ($this->input->post('line')!='') {

					$this->db->where('active', 't');
					$this->db->where('factory_id', $factory['factory_id']);
					$this->db->where('mesin_id', $this->input->post('line'));
					$line_id = $this->db->get('master_line');

					if ($line_id->num_rows()>0) {
						$login_data = array(
							'nik'          => $detail['nik'],
							'name'         => $detail['name'],
							'line_id'      => $line_id->row_array()['master_line_id'],
							'mesin_id'     =>$this->input->post('line'),
							'logged_in'    => 'qc',
							'factory'      =>$factory['factory_id'], 
							'factory_name' =>$factory['factory_name'] 
						);
						
						$this->session->set_userdata($login_data);
						$status = '200';
						$pesan = "NIK tidak terdaftar";
						$data = array('status' => $status, 'pesan'=>$pesan);
						echo json_encode($data);
					}else{
						$status = '500';
						$pesan = "Nomor Line Tutup";
						$data = array('status' => $status, 'pesan'=>$pesan);
						echo json_encode($data);
					}
					
				}else{
					$status = '300';
					$nik =$detail['nik'];
					$factory = $factory['factory_id'];
					$draw='';
					$draw = '<select id="selectedTest" name="line"><option value="" disabled selected>Klik disini Untuk Pilih Line</option>';
	                      $this->db->order_by('mesin_id', 'asc');
	                      $this->db->where('active', 't');
	                      $this->db->where('factory_id',$factory);
	                      $master = $this->db->get('master_line');
	                      foreach ($master->result() as $key => $master) {
	                        $draw.=  "<option value='".$master->mesin_id."'name=''>".$master->line_name."</option>";
	                      }
                    $draw.=  '</select>';
					$data = array('status' => $status,'nik'=>$nik,'draw'=>$draw);
					echo json_encode($data);
				}

				

			}else{
				$status = '500';
				$pesan = "NIK tidak terdaftar";
				$data = array('status' => $status, 'pesan'=>$pesan);
				echo json_encode($data);
			}
		}
	}
	public function qc($line=NULL)
	{

		chek_session_login_qc();
		$ip = substr($this->input->ip_address(), 0,10);
		if ($ip=='192.168.56'||$ip=='192.168.57') {
			$factory=2;
		}else if ($ip=='192.168.16') {
			$factory=1;
		}else{
			$factory='';
		}
		if ($factory!='') {
			$this->db->where('factory_id', $factory);
		}
		$this->db->where('active', 't');
		$this->db->where('mesin_id', $line);
		$line_name = $this->db->get('master_line');
		$data = array(
			'line_name'    => $line_name->row_array()['line_name']
		);
		$this->load->view('auth/login2', $data);
	}
	public function logout_qc(){
		$line = $this->session->userdata('mesin_id');
		$this->session->sess_destroy();
		header('location:'.base_url('auth/qc/'.$line));
	}
	public function folding($line=NULL)
	{
		chek_session_login_folding();
		$this->db->where('active', 't');

		$ip = substr($this->input->ip_address(), 0,10);
		if ($ip=='192.168.56'||$ip=='192.168.57') {
			$factory=2;
		}else if ($ip=='192.168.16') {
			$factory=1;
		}else{
			$factory='';
		}
		
		if ($factory!='') {
			$this->db->where('factory_id', $factory);
		}

		$this->db->where('mesin_id', $line);
		$line_name = $this->db->get('master_line');
		if ($line_name->num_rows()>0) {
			$nik = $this->input->post('nik');
			$this->db->where('nik', $nik);
			$cekNik = $this->db->get('master_sewer');
			if ($cekNik->num_rows()>0) {
				$detail = $cekNik->row_array();
				$factory = $this->db->get_where('master_factory',array('factory_name'=>$detail['factory']))->row_array();
				$this->db->where('factory_id', $factory['factory_id']);
				$this->db->where('mesin_id', $line);
				$line_id = $this->db->get('master_line')->row_array()['master_line_id'];
				
				$login_data = array(
					'nik'             => $detail['nik'],
					'name'            => $detail['name'],
					'department_name' => $detail['department_name'],
					'line_id'         => $line_id,
					'mesin_id'        => $line,
					'logged_in'       => 'folding',
					'factory'         => $factory['factory_id'], 
					'factory_name'    => $factory['factory_name'],
					'nik_adm'         => 0 
				);
				
				$this->session->set_userdata($login_data);
				redirect('folding','refresh');
			}else{
				$data = array(
					'line'    => $line_name->row_array()['mesin_id'],
					'line_name'    => $line_name->row_array()['line_name'],
				);
				$this->load->view('auth/login_folding', $data);
			}
		}else{
			$data = array(
				'line'    => $line_name->row_array()['mesin_id'],
				'line_name'    => $line_name->row_array()['line_name'],
			);
			$this->load->view('auth/login_folding', $data);
		}
	}
	public function logout_folding(){
		$line = $this->session->userdata('mesin_id');
		$this->session->sess_destroy();
		header('location:'.base_url('auth/folding/'.$line));
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */