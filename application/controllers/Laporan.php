<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		chek_session();
		$this->load->model('Cekpermision_model');
		$this->load->model('LaporanModel');
	}

	public function index()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$this->template->set('title', 'Laporan');
			$this->template->set('desc_page', 'Laporan');
			$this->template->load('layout', 'laporan/laporan_view');
		}
	}

	public function list_laporan()
	{
		$columns = array(
			0  => 'no_urut',
			1  => 'no_urut',
			2  => 'style',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');


		$dari    = $this->input->post('dari');
		$sampai  = $this->input->post('sampai');


		if ($dari != '') {
			$dari = date('Y-m-d', strtotime($dari));
		} else {
			$dari = date('Y-m-d');
		}

		if ($sampai != '') {
			$sampai = date('Y-m-d', strtotime($sampai));
		} else {
			$sampai = date('Y-m-d');
		}

		$factory         = $this->input->post('factory');

		if ($factory == null) {
			$factory = '1';
		} else {
			$factory = $factory;
		}

		$search_po    = $this->input->post('po');
		$search_style = $this->input->post('style');

		$totalData = $this->LaporanModel->allposts_daily_count($factory, $dari, $sampai);

		$totalFiltered = $totalData;

		if (empty($search_po) && empty($search_style)) {
			$dash = $this->LaporanModel->allposts_daily($limit, $start, $order, $dir, $factory, $dari, $sampai);
		} else {

			$dash =  $this->LaporanModel->posts_daily_search($limit, $start, $order, $dir, $search_po, $search_style, $factory, $dari, $sampai);

			// var_dump($dash);
			// die();
			$totalFiltered = $this->LaporanModel->posts_daily_search_count($search_po, $search_style, $factory, $dari, $sampai);
		}

		$data = array();
		$nomor_urut = 0;
		if (!empty($dash)) {
			foreach ($dash as $dash) {
				// $qty_order   = $dash->order;
				// $out_qc = $dash->out_qc;
				// $bundle = $dash->total;
				// $wip_qc = (int)$bundle - (int)$out_qc;

				// $out_qc      = $dash->out_qc;
				// $out_folding = $dash->out_folding;
				// $wip_folding = (int)$out_qc - (int)$out_folding;

				// $loading = (int)$bundle - ((int)$out_qc + (int)$wip_qc);

				// $wip_folding = (int)$out_qc - (int)$out_folding;

				$nestedData['date']         = $dash->update_date;
				$nestedData['factory_name'] = $dash->factory_name;
				$nestedData['line_name']    = $dash->line_name;
				$nestedData['style']        = $dash->style;
				$nestedData['poreference']  = $dash->poreference;
				$nestedData['size']         = $dash->size;
				$nestedData['wip_qc']       = $dash->wip_sewing;
				$nestedData['wip_folding']  = $dash->wip_folding;
				$nestedData['output']       = $dash->folding_output;

				$temp = '"' . $dash->poreference . '","' . $dash->line_id . '","' . $dash->size . '","' . $dash->style . '","' . $dash->wip_sewing . '"';
				// $nestedData['action']      = "hai";
				$nestedData['action']      = "<center><a onclick='return detail($temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-th-list'></i> Detail</a> </center>";

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}


	public function get_detail($post = 0)
	{
		$post = $this->input->get();

		$po     = $post['po'];
		$line   = $post['line'];
		$size   = $post['size'];
		$style  = $post['style'];
		$bundle = $post['bundle'];

		$component = $this->LaporanModel->get_component($po, $size, $style, $line);

		$data = array(
			'component'    => $component,
			'bundle'       => $bundle,
		);

		// print_r($data);
		// die();
		$this->load->view('laporan/detail_view', $data);
	}

	public function download($post = NULL)
	{
		$post = $this->input->post();
		$columns = array(
			0  => 'no_urut',
			1  => 'no_urut',
		);

		$limit = NULL;
		$start = NULL;
		$order = NULL;
		$dir   = NULL;
		$draw  = $this->input->post('draw');


		$search_dari    = $_GET['dari'];
		$search_sampai  = $_GET['sampai'];
		$search_po      = $_GET['po'];
		$search_factory = $_GET['factory'];
		$search_style   = $_GET['style'];



		if ($search_dari != '') {
			$dari = date('Y-m-d', strtotime($search_dari));
		} else {
			$dari = date('Y-m-d');
		}

		if ($search_sampai != '') {
			$sampai = date('Y-m-d', strtotime($search_sampai));
		} else {
			$sampai = date('Y-m-d');
		}


		// $this->input->post('style');

		// if($search_po == 'a'){
		// 	$search_po = null;
		// }

		if ($search_factory == null) {
			$search_factory = '1';
		} else {
			$search_factory = $search_factory;
		}

		if (empty($search_po) && empty($search_style)) {
			$dash = $this->LaporanModel->allposts_daily($limit, $start, $order, $dir, $search_factory, $dari, $sampai);
		} else {

			$dash =  $this->LaporanModel->posts_daily_search($limit, $start, $order, $dir, $search_po, $search_style, $search_factory, $dari, $sampai);
		}

		$data = array();
		$excel = "'\@'";
		if (!empty($dash)) {
			foreach ($dash as $dash) {
				$nestedData['date']         = $dash->update_date;
				$nestedData['factory_name'] = $dash->factory_name;
				$nestedData['line_name']    = $dash->line_name;
				$nestedData['style']        = $dash->style;
				$nestedData['poreference']  = $dash->poreference;
				$nestedData['size']         = $dash->size;
				$nestedData['wip_qc']       = $dash->wip_sewing;
				$nestedData['wip_folding']  = $dash->wip_folding;
				$nestedData['output']       = $dash->folding_output;

				$query = $this->db->query("SELECT DISTINCT component_name from distribusi_detail_view 
				where poreference = '$dash->poreference' and size ='$dash->size' and style = '$dash->style' and line_id='$dash->line_id'")->result();

				if ($query == NULL) {
					$baris = '<tr>
					<td class="text-center">' . $nestedData['date'] . '</td>
					<td class="text-center">' . $nestedData['factory_name'] . '</td>
					<td class="text-center">' . $nestedData['line_name'] . '</td>
					<td class="text-center">' . $nestedData['style'] . '</td>
					<td style="mso-number-format:' . $excel . '" class="text-center">' . $nestedData['poreference'] . '</td>
					<td class="text-center">' . $nestedData['size'] . '</td>
					<td class="text-center">' . $nestedData['wip_qc'] . '</td>
					<td class="text-center">' . $nestedData['wip_folding'] . '</td>
					<td class="text-center">' . $nestedData['output'] . '</td>
					<td class="text-center">-</td>
					<td class="text-center">' . $dash->wip_sewing . '</td>
					</tr>';
					$data[] = $baris;
				} else {
					foreach ($query as $q) {
						$nama_component = $q->component_name;

						$baris = '<tr>
						<td class="text-center">' . $nestedData['date'] . '</td>
						<td class="text-center">' . $nestedData['factory_name'] . '</td>
						<td class="text-center">' . $nestedData['line_name'] . '</td>
						<td class="text-center">' . $nestedData['style'] . '</td>
						<td style="mso-number-format:' . $excel . '" class="text-center">' . $nestedData['poreference'] . '</td>
						<td class="text-center">' . $nestedData['size'] . '</td>
						<td class="text-center">' . $nestedData['wip_qc'] . '</td>
						<td class="text-center">' . $nestedData['wip_folding'] . '</td>
						<td class="text-center">' . $nestedData['output'] . '</td>
						<td class="text-center">' . $nama_component . '</td>
						<td class="text-center">' . $dash->wip_sewing . '</td>
						</tr>';
						$data[] = $baris;
					}
				}
			}
		}

		$json_data = array(
			"data" => $data,
		);
		$this->load->view("laporan/report_view", $json_data);
	}
}
