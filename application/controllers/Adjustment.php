<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adjustment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$CI = &get_instance();
		chek_session();
		
		$this->load->model(array('Cekpermision_model','AdjustmentModel','QcEndlineModel','FoldingModel','Button_model'));
		date_default_timezone_set('Asia/Jakarta');
		$this->fg = $CI->load->database('fg',TRUE);
	}

	public function chartSering()
    {
		$factory = $this->session->userdata('factory');
        $row = $this->AdjustmentModel->count_sering($factory);

        $output = '';
        foreach ($row as $r) {
            $output .= "{ line: '$r->line_name', jumlah:".$r->count."},";    
        }
        return $output;
    }


	public function index()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		
		$data = array(
			'chart' => $this->chartSering(),
		);
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Dashboard Revisi Input QC');
            $this->template->set('desc_page','Dashboard Revisi Input QC');
            $this->template->load('layout','adjustment/dashboard_adjustment',$data);
        }
	}

    public function dashboard_ajax()
    {
        $columns = array(
                            0 =>'tanggal',
							1 =>'no_urut',
							2 =>'poreference',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');
        $date  = date('Y-m-d');
        $line_id    = $this->input->post('line_id');


        $totalData = $this->AdjustmentModel->allposts_count_dash();

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->AdjustmentModel->allposts_dash($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->AdjustmentModel->posts_search_dash($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->AdjustmentModel->posts_search_count_dash($search);
        }
        $data = array();
        $nomor_urut = 0;

        if(!empty($master))
        {
            foreach ($master as $master)
            {
                // $nestedData['no']         = (($draw-1) * 10) + (++$nomor_urut);
				
                $nestedData['tanggal'] = $master->tanggal;
                $nestedData['line']    = $master->line_name;
                $nestedData['pobuyer'] = $master->poreference;
                $nestedData['style']   = $master->style;
                $nestedData['size']    = $master->size;
                $nestedData['pemohon'] = $master->nik_pemohon."<br>(".$master->name.")";
                $nestedData['revisi']  = $master->qty_revisi;
                $nestedData['alasan']  = strtoupper($master->alasan);
                $nestedData['hapus']   = $master->create_by."<br>(".$master->username.")";
                $data      []          = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	
	public function adjustqc()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Master Header Inline');
            $this->template->set('desc_page','Master Header Inline');
            $this->template->load('layout','adjustment/index_pilih_line');
        }
	}
	public function loadline()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('adjustment/modal_line');
        }
	}
	public function searchsubmit($post=NULL)
	{
		$post = $this->input->post();

		if ($post!=NULL) {
			
			/*$detailBukuHijau = $this->AdjustmentModel->getDetailBukuHijau($post['line_id'],$factory);*/
			$data = array('line_id' => $post['line_id'] );
			$this->load->view('adjustment/detail_buku_hijau', $data);
		}
	}
	public function editbukuhijau($get='')
	{
		$get = $this->input->get();
		if ($get!=NULL) {
			$detailBukuHijauParam = $this->AdjustmentModel->getDetailBukuHijauParam($get['qc_id'])->row_array();
			

			$data = array(
				'detail' => $detailBukuHijauParam
			);
			$this->load->view('adjustment/modal_edit_buku_hijau', $data);
		}
	}
	public function editbukuhijausubmit($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$qc_id       = $post['qc_endline_id'];
			$poreference = $post['poreference'];
			$style       = $post['style'];
			$size        = $post['size'];
			$line_id     = $post['line_id'];
			$qty_revisi  = $post['revisi'];

			$max_qc = $this->AdjustmentModel->getMax($qc_id);

			$folding = $this->AdjustmentModel->counter_folding($poreference, $style, $size, $line_id)->row_array();

			// var_dump($folding['counter']);
			// die();
			$max_folding = (int)($folding['counter']==null?0:$folding['counter']) + (int)($folding['counter_sets']==null?0:$folding['counter_sets']);

			$legal_qty = (int)$max_qc - (int)$max_folding;

			if($qty_revisi <= $legal_qty){
				$this->db->trans_start();

				$this->deleteQCtrans($post);
				$this->InsertAdjusmentQc($post);

				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
					$pesan = "Simpan data gagal";
				}else{
					$this->db->trans_complete();
					$status = 200;
					$pesan = "Data Sudah Tersimpan";
				}
			}
			else{
				$status = 500;
				$pesan = "Qty tidak bisa direvisi, karena sudah di input folding.";
			}

			$data = array(
				'status' => $status, 
				'pesan' => $pesan, 
			);
			echo json_encode($data);
		}
	}
	private function deleteQCtrans($post)
	{		
		$max = $this->AdjustmentModel->getMax($post['qc_endline_id']);
		$revisi = $max-$post['revisi'];
		$detailDelete = $this->AdjustmentModel->detailDelete($post['qc_endline_id'],$revisi);
		foreach ($detailDelete->result() as $key => $detail) {
			$trans_id[]=$detail->qc_endline_trans_id;
		}
		$this->AdjustmentModel->deleteQCtrans($trans_id);
		$repair        = $this->QcEndlineModel->getCounterDefect($post['qc_endline_id'],$trans_id);;
		$total_repair  = $this->QcEndlineModel->getTotalDefect($post['qc_endline_id'],$trans_id);
		$counter_qc    = $this->AdjustmentModel->getMax($post['qc_endline_id']);
		
		$this->db->where('qc_endline_id', $post['qc_endline_id']);
		$this->db->update('qc_endline', array('counter_qc'=>$counter_qc,'repairing'=>$repair,'total_repairing'=>$total_repair));
	}
	private function InsertAdjusmentQc($post)
	{
		$detail = array(
			'id'               => $this->uuid->v4(),
			'qc_endline_trans' =>$post['qc_endline_id'], 
			'poreference'      =>$post['poreference'], 
			'style'            =>$post['style'], 
			'size'             =>$post['size'], 
			'nik_pemohon'      =>$post['nik_pemohon'], 
			'qty_revisi'       =>$post['revisi'], 
			'alasan'           =>$post['alasan'], 
			'line_id'          =>$post['line_id'], 
			'factory_id'       =>$this->session->userdata('factory'), 
			'create_by'        =>$this->session->userdata('nik') 
		);
		$factory = $this->session->userdata('factory');
		
		$this->db->insert('adjustment_delete_qc', $detail);

		// $hariini = date('Y-m-d');
		// $cekshft = $this->db->query("SELECT shift FROM daily_jam_kerja WHERE line_id = '$line_id' and factory_id = '$factory_id'
		// 					date(create_date) = '$hariini'")->row_array();
		// $shift = $cekshft['shift'];
		
		$cek_cron = $this->db->query("SELECT * from schedulers where keterangan = 'tarik_hari' 
		and status = 'ongoing' or status = 'queue' and factory_id = '$factory'")->num_rows();
	
	

		
		if($cek_cron != null){

		}
		else{
				
			$data_schedule = array(
				'id'         => $this->uuid->v4(),
				'job'        => 'SYNC_PRODUCTION',
				'status'     => 'queue',
				'created_at' => date('Y-m-d H:i:s'),
				'keterangan' => 'tarik_hari',
				'factory_id'=> $factory
				// 'shift' => $shift
			);
			
			$id_schedule = $data_schedule['id'];
			$this->db->insert('schedulers', $data_schedule);
			
		}

	}
	// serverside detail buku hijau
	public function detail_buku_hijau_ajax()
	{
		$columns = array( 
                            0 =>'poreference', 
                            1 =>'style',
                            2 =>'size',
                        );

		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');
		$date    = date('Y-m-d');
		$line_id = $this->input->post('line_id');
		$factory = $this->session->userdata('factory');

        $totalData = $this->AdjustmentModel->allposts_count_detail($date,$line_id,$factory);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $adjustment = $this->AdjustmentModel->allposts_detail($limit,$start,$order,$dir,$date,$line_id,$factory);
        }
        else {
            $search = $this->input->post('search')['value'];
            

            $adjustment =  $this->AdjustmentModel->posts_search_detail($limit,$start,$search,$order,$dir,$date,$line_id,$factory);

            $totalFiltered = $this->AdjustmentModel->posts_search_count_detail($search,$date,$line_id,$factory);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($adjustment))
        {
            foreach ($adjustment as $adjustment)
            {
            	
                $_temp                  = '"'.$adjustment->qc_endline_id.'","'.$adjustment->poreference.'","'.$adjustment->size.'"';
                
				$nestedData['no']              = (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['poreference']     = $adjustment->poreference;
				$nestedData['style']           = $adjustment->style;
				$nestedData['size']            = $adjustment->size;
				$nestedData['order']           = $adjustment->order;
				$nestedData['counter_qc']      = $adjustment->counter_qc;
				$nestedData['daily_output']    = $adjustment->daily_output;
				$nestedData['balance_per_day'] = $adjustment->balance_per_day;
				$nestedData['defect_perday']   = $adjustment->defect_perday;                
                $nestedData['action']   = "<a onclick='return pilih($_temp)' class='btn btn-primary' href='javascript:void(0)'><i class='fa fa-pencil-square-o'></i></a>";
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
	// end server side detail buku hijau

	public function bypass()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
				
			$this->template->set('title','BYPASS FOLDING');
			$this->template->set('desc_page','BYPASS FOLDING');
			$this->template->load('layout','adjustment/bypass_view');
			// $this->template->load('layout','po_buyer/list_po_buyer');
        }        
	}

	public function bypass_packing($post=NULL)
    {
		$post = $this->input->post();
		
		
        if ($post!='') {
			$pobuyer = $post['po'];
			$line_id = $post['line_id'];

			$barcode = $this->AdjustmentModel->getBarcodeFg($pobuyer);
			// var_dump($barcode);
			// die();
			$this->db->trans_start();
			foreach ($barcode as $bar) {

				$query = $this->fg->query("SELECT * from package_movements where barcode_package = '$bar->barcode_id' and department_from = 'preparation'
				and department_to = 'sewing' and status_to = 'onprogress' and status_from = 'completed'");
				if($query->num_rows()==0){
					// update table package 
					$this->fg->where('barcode_id',$bar->barcode_id);
					$this->fg->update('package', array('updated_at'=>date('Y-m-d H:i:s'),'current_department'=>'sewing','current_status'=>'onprogress','status_sewing'=>'onprogress'));

					// update table package movement
					$this->fg->where('deleted_at is NULL',NULL,FALSE);
					$this->fg->where(array('barcode_package'=>$bar->barcode_id,'is_canceled'=>'f'));
					$this->fg->update('package_movements', array('deleted_at'=>date('Y-m-d H:i:s')));

					// insert table package movement
					$line_id = $this->AdjustmentModel->getLineFg($line_id)['id'];
					$user_id = ($this->session->userdata('factory')==2?44:45);
					$detail_movement = array(
						'barcode_package' =>$bar->barcode_id , 
						'department_from' =>'preparation', 
						'department_to'   =>'sewing', 
						'status_to'       =>'onprogress', 
						'status_from'     =>'completed', 
						'user_id'         => $user_id, 
						'description'     =>'accepted on checkin sewing', 
						'created_at'      =>date('Y-m-d H:i:s'), 
						'line_id'         =>$line_id, 
						'ip_address'      =>$this->input->ip_address() 
					);
					$this->fg->insert('package_movements', $detail_movement);
				}

				$query2 = $this->fg->query("SELECT * from package_movements where barcode_package = '$bar->barcode_id' and department_from = 'sewing'
				and department_to = 'sewing' and status_to = 'onprogress' and status_from = 'completed'");
				if($query2->num_rows()==0){
					$this->complete_karton($bar->barcode_id,$line_id);
				}
				
			}

			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
				// $status = 500;
				// $pesan  = "Insert Data gagal";
			}else{
				$this->db->trans_complete();
				// $status = 200;
				// $pesan  = "OK";
			}

			$status = 1;
			$pesan = "SUKSES";
		}else{
			$status = 3;
			$pesan = "PO KOSONG";
		}			

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
	}

	private function complete_karton($barcode_package,$line)
	{
		
		
		//if ($package['barcode_id']!=NULL) {
			// update table package 
			$line_id = $this->AdjustmentModel->getLineFg($line)['id'];
			$user_id = ($this->session->userdata('factory')==2?44:45);
			$this->fg->where('barcode_id',$barcode_package);
			$this->fg->update('package', array('updated_at'=>date('Y-m-d H:i:s'),'current_department'=>'sewing','current_status'=>'completed','status_sewing'=>'completed'));

			// update table package movement
			$this->fg->where('deleted_at is NULL',NULL,FALSE);
			$this->fg->where(array('barcode_package'=>$barcode_package,'is_canceled'=>'f'));
			$this->fg->update('package_movements', array('deleted_at'=>date('Y-m-d H:i:s')));

			// insert table package movement
			$detail_movement = array(
				'barcode_package' =>$barcode_package , 
				'department_from' =>'sewing', 
				'department_to'   =>'sewing', 
				'status_from'     =>'onprogress', 
				'status_to'       =>'completed', 
				'user_id'         => $user_id, 
				'description'     =>'set as completed on sewing', 
				'created_at'      =>date('Y-m-d H:i:s'), 
				'line_id'         =>$line_id,
				'ip_address'      =>$this->input->ip_address() 
			);
			$this->fg->insert('package_movements', $detail_movement);

			$this->db->where('barcode_id',$barcode_package);
			$this->db->update('folding_package_barcode', array('status'=>'completed','complete_by'=>$this->session->userdata('nik'),'complete_at'=>date('Y-m-d H:i:s'),'backto_sewing'=>'f'));
		//}
	}
	

	public function bypass_qc($post=NULL)
	{
		$post = $this->input->post();
		
		
        if ($post!='') {
			$pobuyer = $post['po'];
			$line_id = $post['line_id'];
			$styleid = $post['style'];

			$folding = $this->AdjustmentModel->getFolding($pobuyer,$line_id,$styleid);

			// var_dump($folding);
			// die();

			$sizz = '';

			foreach ($folding as $fold ) {
				$qty             = $fold->balance;
				$id              = $fold->inline_header_id;
				$poreference     = $fold->poreference;
				$size            = $fold->size;
				$counter_folding = $this->FoldingModel->counter_folding($id,$size);
				$status          = 'allocation';
				$style_id        = $fold->style;

				// var_dump($style);
				// die();

				$sizz .= $qty.",";
				// $this->db->trans_start();
				for ($i=1;$i<=$qty;$i++){
					$counter_folding+=1;
					$detail = array(
						'folding_id'       => $this->uuid->v4(),
						'inline_header_id' => $id,
						'barcode_package'  => '',
						'barcode_garment'  => '',
						'counter_folding'  => $counter_folding,
						'status_folding'   => $status,
						'scan_id'          => '',
						'style'            => $style_id,
						'size'             => $size,
						'poreference'      => $poreference,
						'factory_id'       => $this->session->userdata('factory'),
						'remark'           => 'SETS',
						'create_by'        => $this->session->userdata('nik'),
						'ip'               => $this->input->ip_address()
					);
					$this->db->insert('folding', $detail);
				}

				// var_dump($i);
			
				if ($this->db->trans_status() == FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
					$pesan  = "Insert Data gagal";
				}else{
					$this->db->trans_complete();
					$status = 200;
					$pesan  = "OK";
				}
			}

			// var_dump($sizz);
			// 	die();
		}else{
			$status = 100;
			$pesan = "PO KOSONG";
		}	
		$data = array(
			'status' => $status, 
			'pesan' => $pesan 
		);
		echo json_encode($data);
	}

	public function bypass_output_qc()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
				
			$this->template->set('title','BYPASS QC');
			$this->template->set('desc_page','BYPASS QC');
			$this->template->load('layout','adjustment/qc/index_bypass');
			// $this->template->load('layout','po_buyer/list_po_buyer');
        }        
	}

	public function bypass_allin($post=NULL)
	{
		$post= $this->input->post();

		if ($post!=NULL) {

			$po_bypass   = $post['po'];
			$size_bypass = $post['size'];
			$line_bypass = $post['line'];
			$date_bypass = $post['tgl'];
			$qty_bypass  = $post['qty'];

			$cek_header = $this->db->query("SELECT * from inline_header 
			where poreference = '$po_bypass' and line_id = '$line_bypass'")->row_array();

			if($cek_header != null){
				$ih_id      = $cek_header['inline_header_id'];
				$style_id   = $cek_header['style'];
				$factory_id = $cek_header['factory_id'];
	
				$cek_detail = $this->db->query("SELECT * from inline_header_size 
				where inline_header_id = '$ih_id' and size = '$size_bypass'")->row_array();
				
				$ih_detail = $cek_detail['header_size_id'];
				
				$cek_qc = $this->db->query("SELECT * FROM qc_output where poreference = '$po_bypass' 
				and size = '$size_bypass' and line_id = '$line_bypass'")->row_array();

				$available = $cek_qc['available'];
				if($qty_bypass <= $available){
					$qty = $qty_bypass;
				}
				else{
					$qty = 0;
				}
	
				// $qty     = $post['qty'];
				$id      = $ih_id;
				$size_id = $ih_detail;
				$style   = $style_id;
				$factory = $factory_id;

				$cekinputqc = $this->QcEndlineModel->cekInputQc($id,$size_id);

				
				// echo "<pre>";
				// var_dump($style_id);
				// var_dump($style);
				// var_dump($cekinputqc);
				// echo "</pre>";
				// die;
				$this->db->trans_start();

				$this->bypass_sewing($style,$line_bypass,$factory,$qty,$date_bypass);

				if ($cekinputqc==0) {
					$this->insert_endline($id,$size_id,$style,$date_bypass);
	
					$this->insert_endlinetrans_masal($id,$size_id,$style,$qty,$date_bypass);
					$this->insertStatusQc($id,$size_id,$date_bypass);
	
				}else{
					$this->insert_endlinetrans_masal($id,$size_id,$style,$qty,$date_bypass);
					$this->insertStatusQc($id,$size_id,$date_bypass);
				}
				
	
				if ($this->db->trans_status() == FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
					$pesan = "ROLLBACK";
				}else{
					$this->db->trans_complete();
					$status = 200;
					$pesan = "SUKSES";
				}
			}
			else{
				$status = 404;
				$pesan = "ERROR";
			}			

			$data = array(
				'status'     => $status, 
				'pesan'      => $pesan, 
			);
			echo json_encode($data);
			
			
		}
	}

	private function bypass_sewing($style,$line,$factory,$qty,$tgl)
	{
		$counter = $this->Button_model->counterSewing($line,$factory,$style);

		$c       = $counter+1;

		for ($i=1;$i<=$qty;$i++){
			$detail = array(
				'style'       => $style,
				'line_id'     => $line,
				'factory_id'  => $factory,
				'counter_pcs' => $c,
				'create_date' => $tgl,
			); 			
			$this->db->insert('sewing_pcs', $detail);
		}	

		$update_counter = "select * from update_counter_sewing('$style',$line)";
		$this->db->query($update_counter);
	}

	private function insert_endline($id,$size_id,$style,$tgl)
	{
		$nik     = $this->session->userdata('nik');
		$factory = $this->session->userdata('factory');
		$data = array(
			'qc_endline_id'    => $this->uuid->v4(),
			'inline_header_id' => $id,
			'header_size_id'   => $size_id,
			'create_by'        => $nik,
			'style'            => $style,
			'factory_id'       => $factory,
			'create_date'      => $tgl
		);
		$this->db->insert('qc_endline', $data);
	}

	private function insert_endlinetrans_masal($id,$size_id,$style,$qty,$tgl)
	{
		$getendline      = $this->QcEndlineModel->getEndline($id,$size_id);
		$endlinetrans_id = $getendline['qc_endline_id'];
		$counter_trans   = $this->QcEndlineModel->countPcs($endlinetrans_id);
		$counter_qc      = ($counter_trans==0?0:$counter_trans);
		$factory         = $this->session->userdata('factory');
		$nik             = $this->session->userdata('nik');
		for ($i=1;$i<=$qty;$i++){
			$counter_qc+=1;
            $detail = array(
				'qc_endline_trans_id' => $this->uuid->v4(),
				'qc_endline_id'       => $getendline['qc_endline_id'],
				'style'               => $style,
				'factory_id'          => $factory,
				'counter_pcs'         => $counter_qc,
				'inline_header_id'    => $id,
				'create_by'           => $nik,
				'create_date'         => $tgl,
				'sys'                 => 'manual-'.$qty,
				'ip'                  => $this->input->ip_address()
			);
            $this->db->insert('qc_endline_trans', $detail);
        }
        $counter_max = $this->QcEndlineModel->countPcs($endlinetrans_id);

		$this->db->where('qc_endline_id', $endlinetrans_id);
		$this->db->update('qc_endline', array('counter_qc'=>$counter_max));
		// $cektrans = $this->QcEndlineModel->cekCountertrans($endlinetrans_id);
			
		// if ($cektrans>0) {
		// 	$update_counter = "select * from update_counter('$endlinetrans_id')";
		// 	$this->db->query($update_counter);

		// 	$update_counter_qc = "select * from update_counter_qc('$endlinetrans_id')";
		// 	$this->db->query($update_counter_qc);
		// }
	}

	private function insertStatusQc($id,$size,$tgl)
	{
		/*$Header = $this->QcEndlineModel->cekHeader($id,$size)->row_array();
		$factory = $this->session->userdata('factory');
		
		$this->db->where('Date(update_at)', date('Y-m-d'));
		$this->db->where(array('style'=>$Header['style'],'line_id'=>$Header['line_id']));
		$cekStatusEndline = $this->db->get('status_qc_endline');
		if ($cekStatusEndline->num_rows()==0) {
			$detailStatus = array(
				'id'         => $this->uuid->v4(),
				'style'      =>$Header['style'], 
				'line_id'    =>$Header['line_id'], 
				'update_at'  =>date('Y-m-d H:i:s'), 
				'factory_id' =>$factory 
			);
			
			$this->db->insert('status_qc_endline', $detailStatus);
		}*/
		$Header  = $this->QcEndlineModel->cekHeader($id,$size)->row_array();
		$factory = $this->session->userdata('factory');

		if($Header['style']==null){
			$inline = $this->db->query("SELECT * FROM inline_header where inline_header_id = '$id'")->row_array();

			$style = $inline['style'];
		}
		else{
			$style = $Header['style'];
		}
		$this->db->where('Date(update_at)',$tgl);
		$this->db->where(array('line_id'=>$Header['line_id'],'factory_id'=>$factory));
		$cekStatusEndline = $this->db->get('status_qc_endline');
		if ($cekStatusEndline->num_rows()==0) {
			$detailStatus = array(
				'id'         => $this->uuid->v4(),
				'style'      => $style,
				'line_id'    => $Header['line_id'],
				'update_at'  => $tgl,
				'factory_id' => $factory
			);
			
			$this->db->insert('status_qc_endline', $detailStatus);
		}else{
			$this->db->where('Date(update_at)', date('Y-m-d'));
			$this->db->where(array('line_id'=>$Header['line_id'],'factory_id'=>$factory));
			$this->db->update('status_qc_endline', array('style'=>$style,'update_at'=>$tgl));
		}
		
		
	}
}

/* End of file Adjustment.php */
/* Location: ./application/controllers/Adjustment.php */
