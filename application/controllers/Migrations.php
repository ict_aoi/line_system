<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrations extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library("migration");
	}

	public function index()
	{

	    if ($this->migration->version(17) === FALSE) {
    		show_error($this->migration->error_string());
    	}else {
    		echo 'Migrations Success';
    	}
	}

}

/* End of file migrations.php */
/* Location: ./application/controllers/migrations.php */