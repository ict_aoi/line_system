<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Folding extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$CI = &get_instance();
		date_default_timezone_set('Asia/Jakarta');
		chek_session_folding();
		$this->load->model(array('FoldingModel','QcEndlineModel'));
		$this->fg = $CI->load->database('fg',TRUE);
		$this->pdc_new = $CI->load->database('pdc_new',TRUE);
		$this->load->library('curl');
	}

	public function index()
	{
		$line_id = $this->session->userdata('line_id');
		$line = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();
		
		$data = array('line' =>$line , );
		$this->template->set('title',$line['line_name'].' - Folding');
        $this->template->set('desc_page','Folding');
		$this->template->load('layout2','folding/index_folding',$data);
	}
	public function loadporeference()
	{
		$data['records'] = $this->FoldingModel->listoutstanding();
		$this->load->view('folding/show_poreference',$data);
	}
	public function loadporeference_ajax()
	{
		$columns = array(
                            0 =>'startdateactual',
                            1 =>'poreference',
                        );
		
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $totalData = $this->FoldingModel->allposts_count_headerfolding();

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->FoldingModel->allposts_headerfolding($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->FoldingModel->posts_search_headerfolding($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->FoldingModel->posts_search_count_headerfolding($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
            	
            	$outstanding = $this->FoldingModel->listoutstanding($master->inline_header_id);
            	foreach ($outstanding->result() as $key => $out) {
            		$temp = '"'.$out->inline_header_id.'","'.$out->poreference.'","'.$out->style.'"';

                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['poreference'] = $out->poreference;
                $nestedData['style'] = $out->style;
                $nestedData['startdateactual'] = $out->startdateactual;
                $nestedData['action'] = "<center><a onclick='return selectpo($temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
                $data[] = $nestedData;
            	}
                

            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function showsize($get=NULL)
	{
		$get = $this->input->get();

		if ($get!=NULL) {
			$po = $get['po'];
			$id = $get['id'];
			$data['list_size']	= $this->FoldingModel->listsize($po,$id);
			$this->load->view('folding/show_size',$data);			
		}
	}
	public function searchsubmitfolding($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {
			$header_id = $post['id'];
			$size_id = $post['size_id'];

			$cekHeader = $this->FoldingModel->cek_qc_output($header_id,$size_id);
			if ($cekHeader->num_rows()>0) {
				$style = $post['style'];

				// $switch = $this->ceksets($style);
				$remark = $this->remark($style);
				// $change = ($cekHeader->num_rows()>0?1:0);
					
				/*$this->fg->where('po_number', $post['poreference']);
				$remark = $this->fg->get('po_summary')->row_array()['remark'];*/

				$poreference        = $cekHeader->row_array()['poreference'];
				$size               = $cekHeader->row_array()['size'];
				$article            = $cekHeader->row_array()['article'];
				$ListBarcodePackage = $this->FoldingModel->getListBarcodePackage($poreference,$size);
				$qc_output          = $cekHeader->row_array()['qc_output'];
				$folding_header     = $this->FoldingModel->GetHeader($header_id,$size,$style);
				$counter            = ($folding_header->row_array()['counter']==null?0:$folding_header->row_array()['counter']);
				$counter_sets       = ($folding_header->row_array()['counter_sets']==null?0:$folding_header->row_array()['counter_sets']);
				$total              = (int)$qc_output - ( (int)$counter + (int)$counter_sets );
				
				
				$data = array(
					'header_id'   => $header_id,
					'remark'      => $remark,
					'article'     => $article,
					// 'switch'   => $switch,
					// 'change'   => $change,
					'style'       => $post['style'],
					'size_id'     => $size_id,
					'poreference' => $poreference,
					'size'        => $size,
					'scan_id'     => '',
					'draw'        => '',
					'barcode_id'  => '',
					'max_output'  => $total
				);
				$this->load->view('folding/result_folding_sets',$data);
			}
		}
	}

	public function getToken() {
        $login_url = 'http://mid.bbigarment.co.id/api/login';
        $login_data = json_encode([
            'username' => '221000020',
            'password' => 'Pass@123'
        ]);

        $this->curl->create($login_url);
        $this->curl->http_header('Content-Type', 'application/json');
        $this->curl->post($login_data);
        $result = $this->curl->execute();		

		return json_decode($result);        
    }

    public function getCarton($token, $barcode, $user) {
        // $token = $this->getToken();
		// var_dump($token, $barcode); die();
        if ($token === null) {
            echo "Error: Unable to retrieve token.";
            return;
        }

        $carton_url = 'http://mid.bbigarment.co.id/api/pdc/getcarton';

		$this->curl->create($carton_url);

		// Set each HTTP header individually
		$this->curl->http_header('barcode: ' . $barcode);
		$this->curl->http_header('user: '.$user);
		$this->curl->http_header('Authorization: Bearer ' . $token);
		$this->curl->http_header('Accept: application/json');

		$result = $this->curl->execute();

		return json_decode($result);
    }

	public function searchsubmitbyscan($post=NULL)
	{
		$post 	= $this->input->post();
		$factory_id = $this->session->userdata('factory');
		
		if($factory_id == 1){
			$user = 'lineaoi1';
		} else if($factory_id == 2){
			$user = 'lineaoi2';
		} else{
			$user = 'lineaoi3';
		}

		if ($post!=NULL) {
			$BarcodePackage = $this->FoldingModel->getBarcodePackage($post['scan_karton']);
			// $package = $this->FoldingModel->getPackage($post['scan_karton']);
			
			$token = $this->getToken();
			if($token != NULL){
				
				$token = $token->data->token;
				
				$package = $this->getCarton($token, $post['scan_karton'], $user);
				if($package != NULL){
					$package = $package->data;
				} else{
					echo '2';
					return false;
				}								
			}	
			
			if($BarcodePackage->row_array() == null){				
				$this->checkin_karton($post['scan_karton'], $package->source_data);
				$this->insertPackageNew($package);
				$this->insertPackageBarcode($post['scan_karton'],$package->scan_id);
				$BarcodePackage = $this->FoldingModel->getBarcodePackage($post['scan_karton']);
			}			
			
			//cek status belum pernah di scan / back to sewing / atau sudah onprogress
			if (($BarcodePackage->row_array()['status']=='onprogress'||$BarcodePackage->row_array()['status']==NULL)||(
				$BarcodePackage->row_array()['backto_sewing']!='t'&&$BarcodePackage->row_array()['status']=='back to sewing')) {
				
				//cek apakah sudah pernah scan atau belum	
				// var_dump($BarcodePackage->num_rows());die();
				if ($BarcodePackage->num_rows()==0) {
					//cek di fgms udah complete belum
					if ($package->num_rows()!=0) {
						$status_karton_packing = $package->current_dep;

						if($status_karton_packing == 'preparation' || $status_karton_packing == 'sewing')
						{
							$cetakkarton 		 = $package->current_dep=='preparation' &&$package->current_status=='completed';
							$sewingcheckin 		 = $package->current_dep=='sewing' &&$package->current_status=='onprogress';
							$sewingcheckcomplete = $package->current_dep=='sewing' &&$package->current_status=='completed';
							if ($cetakkarton||$sewingcheckin) {								
								$manufacturing_size = '';
								foreach($package->detail as $dt){
									$manufacturing_size .= ' '.$dt->size;
								}

								$cek_rasio = $package->is_rasio;							
								if($cek_rasio == true){
									$size 	= explode(' ', trim($manufacturing_size));								
									$size1 	= array();
									$qty 	= array();
									foreach ($package->detail as $key => $s) {									
										$y[]	= [$s->size, $s->qty];
										$x		= $s->size;									
										array_push($size1, $x);
									}
									$rasio =1;
								}else{
									$size = explode(' ', trim($manufacturing_size));
									$size1 = array();
									foreach ($size as $key => $s) {									
										if($size != ''){
											array_push($size1, $s);
										}
									}
									$qty[]   = $package->inner_pack;
									$y[]     = array_merge($size1,$qty);
									$rasio   = 0;
								}
								
								$status = ($this->FoldingModel->getBarcodePackage($post['scan_karton'])->row_array()['status']=='completed'?'<h3><span class="status label label-danger">Completed</span></h3>':'<h3><span class="status label label-primary">On Progress</span></h3>');
								// $header  = $this->FoldingModel->cekHeader($package->row_array()['po_number'],$size1,$package->row_array()['buyer_item']);
								$header  = $this->FoldingModel->cekHeader($package->po_number,$size1,$package->detail[0]->article_no);

								
								if ($header->num_rows()>0) {
																	
									$this->db->trans_start();
								
									// $this->checkin_karton($post['scan_karton'], $package->source_data);
									// $this->insertPackage($package->row_array());
									// $this->insertPackageBarcode($post['scan_karton'],$package->row_array()['scan_id']);
									
									
									// if ($this->db->trans_status() == FALSE)
									// {
									// 	$this->db->trans_rollback();
									// }else{
									// 	$this->db->trans_complete();
									// }
									$p = $this->result_folding($package,$header);
									$data = array(
										'p'              => $p, 
										'size'           => $size1, 
										'status'         => $status,  
										'rasio'          => $rasio,  
										'y'              => $y, 
										'barcodePackage' => $post['scan_karton']
									);
									if ($p['remark']=='SETS') {

										$this->load->view('folding/result_folding_sets_scan',$data);
									}else{

										$cek_style = $this->FoldingModel->cek_master_order($p['style']);
									
										$masker = $cek_style['value'];
										if($masker == 'MASKER'){
											$this->load->view('folding/result_folding_masker',$data);
										}
										else{
											$this->load->view('folding/result_folding',$data);
										}
									}
									
								}else{							
									echo "1";
								}
							}else if($sewingcheckcomplete){
								echo "6";
							}
							else{	
								echo "3";
							}
						}
						else{
							echo "5";
						}
					}else{
						
						echo "2";
					}
				}else{
					if ($package != NULL) {						
						$status_karton_packing = $package->current_dep;												
						
						if($status_karton_packing == 'preparation' || $status_karton_packing == 'sewing')
						{
							$manufacturing_size = '';
							foreach($package->detail as $dt){
								$manufacturing_size .= ' '.$dt->size;
							}

							$cek_rasio = $package->is_rasio;							
							if($cek_rasio == true){
								$size 	= explode(' ', trim($manufacturing_size));								
								$size1 	= array();
								$qty 	= array();
								foreach ($package->detail as $key => $s) {									
									$y[]	= [$s->size, $s->qty];
									$x		= $s->size;									
									array_push($size1, $x);
								}
								$rasio =1;
							}else{
								$size = explode(' ', trim($manufacturing_size));
								$size1 = array();
								foreach ($size as $key => $s) {									
									if($size != ''){
										array_push($size1, $s);
									}
								}
								$qty[]   =$package->inner_pack;
								$y[]     =array_merge($size1,$qty);
								$rasio   =0;
							}														
							
							$status  = ($BarcodePackage->row_array()['status']=='completed'?'<h3><span class="status label label-danger">Completed</span></h3>':'<h3><span class="status label label-primary">On Progress</span></h3>');
							$header  = $this->FoldingModel->cekHeader($package->po_number,$size1,$package->detail[0]->article_no);
							
							if ($header->num_rows()>0) {
								$p = $this->result_folding($package,$header);
									$data = array(
										'p'              => $p, 
										'size'           => $size1, 
										'status'         => $status,  
										'rasio'          => $rasio,  
										'y'              => $y, 
										'barcodePackage' => $post['scan_karton']
									);
								if ($p['remark']=='SETS') {

									$this->load->view('folding/result_folding_sets_scan',$data);
								}else{
									
									$cek_style = $this->FoldingModel->cek_master_order($p['style']);
									
									$masker = $cek_style['value'];
									if($masker == 'MASKER'){
										$this->load->view('folding/result_folding_masker',$data);
									}
									else{
										$this->load->view('folding/result_folding',$data);
									}
								}
								
								
							}else{							
								echo "1";
							}
						}
						else{
							echo "5";
						}
					}
					else{
						echo "5";
					}
				}
			}elseif ($BarcodePackage->row_array()['backto_sewing']=='t'||$BarcodePackage->row_array()['status']=='back to sewing') {
				$package = $this->FoldingModel->getPackage($post['scan_karton']);
				$size1[] = $package->row_array()['manufacturing_size'];

				$disable_out = ($BarcodePackage->row_array()['backto_sewing']=='t'&&$BarcodePackage->row_array()['status']=='back to sewing'?'disabled':'');
				$disable_in = ($BarcodePackage->row_array()['backto_sewing']=='t'&&$BarcodePackage->row_array()['status']=='sewing in'?'disabled':'');
				$data = array(
					'barcodePackage' => $post['scan_karton'], 
					'poreference' => $package->row_array()['po_number'], 
					'size' => $size1,
					'disable_out' => $disable_out,
					'disable_in' => $disable_in
				);
				$this->load->view('folding/result_back_to_sewing',$data);
			}else{							
				echo "4";
			}
			
		}
	}
	private function result_folding($package,$header)
	{
		
		$styles          = $header->row_array()['style'];
		$sets            = explode('-', $styles)[0];
		
		$switch          = $this->ceksets($styles);
		$remark          = $this->remark($styles);
		$style           = ($remark=='SETS'?$sets:$styles);
	
		$poreference     = $header->row_array()['poreference'];
		$size            = $header->row_array()['size'];
		$header_id       = $header->row_array()['inline_header_id'];
		$size_id         = $header->row_array()['header_size_id'];
		$article         = $header->row_array()['article'];
		$scan_id         = $package->scan_id;
		// $draw         = $this->tablePakcageFolding($scan_id,$remark,$package,$poreference,$size);
		// $folding_package = $this->FoldingModel->foldingPackage($scan_id);
		$totalKarton     = $this->FoldingModel->totalKarton($scan_id);
		$po_summary      = $this->FoldingModel->po_summary($poreference);
		$counterscan     = $this->FoldingModel->counterScan($scan_id,$poreference,$size);

		// var_dump($header->row_array()['poreference'], $package->scan_id); die();
		
		$data = array(
			'header_id'   => $header_id, 
			'remark'      => $remark, 
			'switch'      => $switch,
			'style'       => $style, 
			'scan_id'     => $scan_id,
			'poreference' => $poreference, 
			'size'        => $size, 
			'article'     => $article, 
			'po_summary'  => $po_summary, 
			'counterscan' => $counterscan, 
			'totalKarton' => $totalKarton, 
			'inner_pack'  => $package->inner_pack, 
			'pkg_count'   => $package->pkg_count, 
			'source_data' => $package->source_data, 
			// 'draw'     => $draw, 
			'size_id'     => $size_id
		);


		return $data;
	}
	private function ceksets($style)
	{
		$withlist = array('BOTTOM','BOT','-OUT','-OUTER','TOP','IN');
		$cek = array();
		foreach($withlist as $key=>$result){
		    if(strpos($style, $result) != false){
		      $cek = 1;
		      break;   
		    } else{
		      $cek = 0; 
		      if (sizeof($withlist)==$key) {
		      	break;
		      }      
		    }

		}
		return $cek;
	}
	private function remark($style)
	{
		// $withlist = array('_','TOP','BOTTOM','BOT','-OUT','-OUTER');
		$withlist = array('TOP','BOTTOM','BOT','-OUT','-OUTER');
		$cek = array();
		foreach($withlist as $key=>$result){
		    if(strpos($style, $result) != false){
		      $cek = 'SETS';   
		      break;
		    } else{
		      $cek = 'PCS';
		      if (sizeof($withlist)==$key) {
		      	break;
		      }
		    }
		    

		}
		return $cek;
	}
	public function loaddata($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {

			$id              = $post['id'];
			$barcode_package = $post['barcode_id'];
			$scan_id         = $post['scan_id'];
			$poreference     = trim($post['poreference']);
			$size            = trim($post['size']);
			$style           = $post['style'];
			$rasio           = $post['rasio'];
			$article         = $post['article'];
			$data            = $this->foldingdetail($id,$barcode_package,$scan_id,$poreference,$size,$rasio,$style,$article);
			
			echo json_encode($data);
		}
	}
	public function loadDataSetsScan($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {
			
			$header_id       = trim($post['header_id']);
			$poreference     = trim($post['poreference']);
			$size            = $post['size'];
			$barcode_package = $post['barcode_id'];
			$scan_id         = $post['scan_id'];
			$style           = $post['style'];
			$rasio           = $post['rasio'];
			$article         = $post['article'];
			
			$getStyles       = $this->FoldingModel->getStyle($poreference,$size);
			$names           = array();
			$wip_1           = 0;
			$wip_2           = 0;
			/*if ($getStyles->num_rows()==1) {
				$wip_1 =$this->FoldingModel->wipStyleSets($poreference,$size,$getStyles->result()[0]->style);
			}

			if ($getStyles->num_rows()>1) {
				$wip_1 = $this->FoldingModel->wipStyleSets($poreference,$size,$getStyles->result()[0]->style);
				$wip_2 = $this->FoldingModel->wipStyleSets($poreference,$size,$getStyles->result()[1]->style);
			}*/
			foreach ($getStyles->result() as $key => $get) {
				
				$names[] = preg_split("/[-_]+/", $get->style)[1];
				
			}
			
			// $order             = $this->FoldingModel->getOrder($poreference,$size,$style)->row_array()['qty_ordered'];
			$counterscan       = $this->FoldingModel->counterScan($barcode_package);
			
			// $counter_folding   = $this->FoldingModel->counterFoldingSets($style,$poreference,$size)/2;
			$getBarcodeGarment = $this->FoldingModel->getBarcodeGarment($style,$poreference,$size,$article);
			$barcode_garment   =($getBarcodeGarment->num_rows()==0?0:$getBarcodeGarment->row_array()['barcode_id']);
			$temp = '"'.$poreference.'","'.$size.'","'.$article.'"';

			
			
			// $balance         = $counter_folding-$order;

			// $karton_size     = $this->FoldingModel->counterPackage($header_id,$size,$barcode_package);
			
			$data = array(
				'names'             => $names, 
				/*'wip_1'             => $wip_1, 
				'wip_2'             => $wip_2,*/ 
				'counterscan'       => $counterscan, 
				// 'balance'           => $balance, 
				'barcode_garment'   => $barcode_garment,
				'wip_1'             => "<center><a onclick='return loadhidden($temp)' href='javascript:void(0)' class='btn btn-success'>TAMPILKAN</a></center>",
				'wip_2'             => "<center><a onclick='return loadhidden($temp)' href='javascript:void(0)' class='btn btn-success'>TAMPILKAN</a></center>",
				'balance'         => "<center><a onclick='return loadhidden($temp)' href='javascript:void(0)' class='btn btn-warning'>TAMPILKAN</a></center>", 
				// 'counterscan_rasio' => $counterscan_rasio,
				//tambah
				// 'list_size' 		=> $text
				// 'isiKarton'			=> $karton_size
			);
			
			echo json_encode($data);
		}
	}
	public function loadhiddenset($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {
			$getStyles       = $this->FoldingModel->getStyle($post['po'],$post['size']);
			$names           = array();
			$wip_1           = 0;
			$wip_2           = 0;
			if ($getStyles->num_rows()==1) {
				$wip_1 =$this->FoldingModel->wipStyleSets($post['po'],$post['size'],$getStyles->result()[0]->style);
			}

			if ($getStyles->num_rows()>1) {
				$wip_1 = $this->FoldingModel->wipStyleSets($post['po'],$post['size'],$getStyles->result()[0]->style);
				$wip_2 = $this->FoldingModel->wipStyleSets($post['po'],$post['size'],$getStyles->result()[1]->style);
			}

			$counter_folding   = $this->FoldingModel->counterFoldingSets($post['style'],$post['po'],$post['size'])/2;
			$order             = $this->FoldingModel->getOrder($post['po'],$post['size'],$post['style'])->row_array()['qty_ordered'];

			$balance         = $counter_folding-$order;

			$data = array(
				'wip_1'             => $wip_1, 
				'wip_2'             => $wip_2, 
				'balance'           => $balance, 
			);
			
			echo json_encode($data);
		}
	}

	public function loadhidden($post=NULL)
	{
		$post = $this->input->post();
		$ih = $post['ih'];
		$size = $post['size'];
		if ($post!=NULL) {
			$wip_1           = 0;
			$query_qc = $this->db->query("SELECT * FROM qc_output
			 where inline_header_id = '$ih' and size = '$size'")->row_array();
			$qc = $query_qc['qc_output'];

			$query_folding = $this->db->query("SELECT * FROM folding_header
			 where inline_header_id = '$ih' and size = '$size'")->row_array();
			$folding = isset($query_folding['counter']) ? $query_folding['counter'] : 0;

			//stok packing
			$data_free = $this->db->query("SELECT sum(qty) as sum from wip_free_stock where 
			inline_header_id = '$ih' and size = '$size'")->row_array();

			$wip = ((int)$qc+(int)$data_free['sum']) - (int)$folding;
			
			// $wip = (int)$qc - (int)$folding;
			
			$order = $query_qc['qty_order'];
			$balance         = (int)$folding - ((int)$order+(int)$data_free['sum']) ;

			$data = array(
				'wip_1'             => $wip, 
				'balance'           => $balance, 
			);
			
			echo json_encode($data);
		}
	}

	public function loadRasio($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {
			$poreference     = trim($post['poreference']);
			$barcode_package = $post['barcode_id'];
			$rasio           = $post['rasio'];
			$size            = $post['size'];

			$text ='Size : ';
			// $counterscan_rasio = 0;
			if ($rasio==1) {
				// $counterscan_rasio = $this->FoldingModel->counterScanRasio($barcode_package,$poreference,$size);

				//tambah
				$list_size = $this->FoldingModel->list_size($barcode_package,$poreference);

				foreach ($list_size as $ls) {
					$total = ((int)$ls->total);

					$text.= ''.$ls->size.'='.$total.' ,  ';
				}
			}
		}

		$data = array(
			// 'counterscan_rasio' => $counterscan_rasio, 
			'list_size' 		=> $text
		);
		
		echo json_encode($data);
	}

	public function loadRasioSets($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {
			$poreference     = trim($post['poreference']);
			$size            = trim($post['size']);
			$barcode_package = $post['barcode_id'];
			$rasio           = $post['rasio'];

			$text ='Size : ';
			$counterscan_rasio = 0;
			if ($rasio==1) {
				$counterscan_rasio = $this->FoldingModel->counterScanRasioSets($barcode_package,$poreference,$size);
			
				//tambah
				$list_size = $this->FoldingModel->list_size($barcode_package,$poreference);

				foreach ($list_size as $ls) {
					$total = ((int)$ls->total)/2;

					$text.= ''.$ls->size.'='.$total.' ,  ';
				}
			}
		}

		$data = array(
			'counterscan_rasio' => $counterscan_rasio, 
			'list_size' 		=> $text
		);
		
		echo json_encode($data);
	}

	public function loadDataSets($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {

			$id              = $post['id'];
			$size            = $post['size'];
			$barcode_package = $post['barcode_id'];
			$scan_id         = $post['scan_id'];
			$data            = $this->detail($id,$size);
			
			echo json_encode($data);
		}
	}
	private function detail($header_id,$size)
	{
		$where = array(
			'inline_header_id' =>$header_id , 
			'size' =>$size  
		);
		$this->db->where($where);
		$qc_endline      = $this->db->get('qc_output')->row_array();
		
		$counter =  $this->FoldingModel->counter_folding($header_id,$size);
		$counter_folding = $counter['counter']+$counter['counter_sets'];
		
		$balance         = $counter_folding-$qc_endline['qty_order'];

		$wft             = $qc_endline['qc_output']-$counter_folding;
		$detail = array(
			// 'countertoday'    => $countertoday, 
			'balance'         => $balance, 
			'counter_folding' => $counter_folding,  
			'wft'             => $wft
		);
		return $detail;

	}

	public function load_wip($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$header_id = $post['header_id'];
			$size      = $post['size'];

			$data_folding = $this->FoldingModel->load_folding($header_id, $size);

			$data_free = $this->db->query("SELECT * from wip_free_stock where 
			inline_header_id = '$header_id' and size = '$size'")->row_array();


			if($data_folding->num_rows()!=NULL){
				$data    = $data_folding->row_array();
				$balance = (int)$data['folding_output'] - ((int)$data['qty_order'] + (int)$data_free['qty']);
				// $wft     = (int)$data['qc_output'] - (int)$data['folding_output'];
				$wft     = ((int)$data['qc_output'] + (int)$data_free['qty']) - (int)$data['folding_output'];
			}
			else{
				$balance = 0;
				$wft     = 0;
			}
		}
		
		$data = array(
			'balance'           => $balance, 
			'wft'               => $wft
		);

		echo json_encode($data);
	}

	private function foldingdetail($header_id,$barcode_package=NULL,$scan_id=NULL,$poreference,$size,$rasio,$style,$article)
	{
		
		// $where = array(
		// 	'inline_header_id' =>$header_id , 
		// 	'size'   =>$size  
		// );

		// $this->db->where($where);
		// $qc_endline        = $this->db->get('qc_output')->row_array();
		
		// $counter_folding   = $this->FoldingModel->counter_folding($header_id,$size)['counter'];

		// $counterscan_rasio = 0;

		// $data_folding = $this->FoldingModel->load_folding($header_id, $size);

		// if($data_folding->num_rows()!=NULL){
		// 	$data    = $data_folding->row_array();
		// 	$balance = (int)$data['folding_output'] - (int)$data['qty_order'];
		// 	$wft     = (int)$data['qc_output'] - (int)$data['folding_output'];
		// }
		// else{
		// 	$balance = 0;
		// 	$wft     = 0;
		// }
		
	
		// $balance           = $counter_folding-$qc_endline['qty_order'];
		
		$getBarcodeGarment = $this->FoldingModel->getBarcodeGarment($style,$poreference,$size,$article);
		$barcode_garment   = ($getBarcodeGarment->num_rows()==0?0:$getBarcodeGarment->row_array()['barcode_id']);
		$counterscan       = $this->FoldingModel->counterScan($barcode_package);
		// $wft               = $qc_endline['qc_output']-$counter_folding;
		
		$totalKarton       = ($scan_id!=NULL?$this->FoldingModel->totalKarton($scan_id):0);
		
		
		// $karton_size     = $this->FoldingModel->counterPackage($header_id,$size,$barcode_package);
		$counterscan_rasio = 0;
		if ($rasio==1) {
			$counterscan_rasio = $this->FoldingModel->counterScanRasio($barcode_package,$poreference,$size);
		}
		
		$_temp                  = '"'.$header_id.'","'.$size.'"';

		$detail = array(
			'barcode_garment' => $barcode_garment,
			'counterscan'     => $counterscan,
			'totalKarton'     => $totalKarton,
			'wft'             => "<center><a onclick='return loadhidden($_temp)' href='javascript:void(0)' class='btn btn-success'>TAMPILKAN</a></center>",
			'balance'         => "<center><a onclick='return loadhidden($_temp)' href='javascript:void(0)' class='btn btn-warning'>TAMPILKAN</a></center>",
			'counterscan_rasio' => $counterscan_rasio, 	
			// 'balance'           => $balance, 
			// 'wft'               => $wft,
			//tambah
			// 'list_size' 		=> $text
			// 'isiKarton'			=> $karton_size
		);
		return $detail;
	}

	public function searchkarton($post=NULL)
	{
		$post = $this->input->post();

		if ($post!=NULL) {
			$draw='';
			$scan            = $post['scan'];
			$id              = $post['id'];
			$size_id         = $post['size_id'];
			$size            = $this->QcEndlineModel->qtyOrderSize($id,$post['size_id'])['size'];
			$package         = $this->FoldingModel->package($post['scan']);
			$folding_package = $this->FoldingModel->foldingPackage($package['scan_id'])['inner_pack'];
			$counter_package = $this->FoldingModel->counterPackage($id,$size,$post['scan']);

			$detail = $this->package_detail($scan,$id);
			$cekHeader = $this->QcEndlineModel->cekHeader($id,$size_id)->row_array();
			
			if ($cekHeader['poreference']==$detail['package_detail']['po_number']&&$cekHeader['size']==$detail['package_detail']['manufacturing_size']) {
				
				$cetakkarton = $detail['package']['current_department']=='preparation' &&$detail['package']['current_status']=='completed';
				$sewingcheckin = $detail['package']['current_department']=='sewing' &&$detail['package']['current_status']=='onprogress';
				$cekKomplete = $detail['package']['current_department']=='sewing' &&$detail['package']['current_status']=='completed';
				if ($cetakkarton || $sewingcheckin) {
						// validasi checkin karton
						$this->db->trans_start();
						
						$this->checkin_karton($scan);
						$this->insertPackage($detail['package_detail']);
						$this->insertPackageBarcode($scan,$detail['package_detail']['scan_id']);

						// get table folding_package
						$draw = $this->tablePakcageFolding($detail);
						
					
					if ($this->db->trans_status() == FALSE)
					{
						$this->db->trans_rollback();
						$status = 500;
						$pesan  = "Insert Data gagal";
					}else{
						$this->db->trans_complete();
						$status = 200;
						$pesan  = "OK";
					}
				}else if($cekKomplete) {
					$status = 500;
					$pesan = "karton sudah complete";
				}else{
					$status = 500;
					$pesan = "Status karton masih preparation on progress(blm print) info packing";
					
				}
			}
			else{
				$status = 500;
				$pesan = "Karton Tidak Sesuai Size Yang di pilih";
				
			}

			$data = array(
				'barcode_id' => $detail['package']['barcode_id'],  
				'scan_id'    => $detail['package_detail']['scan_id'],
				'draw'      =>  $draw, 
				'status'     => $status, 
				'pesan'      => $pesan, 
			);
			echo json_encode($data);			
		}
	}
	public function submit_sewing_in($post=NULL)
	{
		$post= $this->input->post('barcode_id');
		if ($post!='') {

			$this->checkin_karton($post);
			$this->db->where('barcode_id', $post);
			$this->db->update('folding_package_barcode', array('status'=>'sewing in'));
			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
				$status = 500;
				$pesan  = "Sewing in gagal";
			}else{
				$this->db->trans_complete();
				$status = 200;
				$pesan  = "OK";
			}
			$data = array(
				'status'     => $status, 
				'pesan'      => $pesan, 
			);
			echo json_encode($data);
		}
	}
	public function submit_sewing_complete($post=NULL)
	{
		$post= $this->input->post('barcode_id');
		if ($post!='') {

			$this->complete_karton($post);


			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
				$status = 500;
				$pesan  = "Sewing in gagal";
			}else{
				$this->db->trans_complete();
				$status = 200;
				$pesan  = "OK";
			}
			$data = array(
				'status'     => $status, 
				'pesan'      => $pesan, 
			);
			echo json_encode($data);
		}
	}
	private function checkin_karton($scan, $source_data)
	{
		$token = $this->getToken();		
		if($token != NULL){
			
			$token = $token->data->token;
						
			$carton_url = 'http://mid.bbigarment.co.id/api/pdc/checkin';
		
			$this->curl->create($carton_url);

			$factory_id = $this->session->userdata('factory');
				
			if($factory_id == 1){
				$user = 'lineaoi1';
			} else if($factory_id == 2){
				$user = 'lineaoi2';
			} else{
				$user = 'lineaoi3';
			}

			$line_name =$this->db->get_where('master_line', array('master_line_id'=>$this->session->userdata('line_id')))->row_array()['line_name'];

			$this->pdc_new->where('name', $line_name);
			$line_pdc = $this->pdc_new->get('master_line')->row_array();
	
			// Set each HTTP header individually		
			$checkin_data = [
				'user' 			=> $user,
				'barcode_id' 	=> $scan,
				'line_id' 		=> ($line_pdc != null) ? $line_pdc['id'] : $this->session->userdata('line_id'),
				'source_data' 	=> $source_data
			];		
			$this->curl->http_header('Authorization: Bearer ' . $token);				
			$this->curl->post($checkin_data);
			$result = $this->curl->execute();

			// var_dump($result, $checkin_data); die();
			
		}		
	}
	// private function checkin_karton($scan, $source)
	// {
	// 	$package = $this->FoldingModel->package($scan);
		 

	// 	if ($package['barcode_id']!=NULL) {
	// 		$this->fg->where(array('barcode_id'=>$scan,'current_department'=>'sewing','current_status'=>'onprogress'));
	//          $status_checkin = $this->fg->get('package')->num_rows();
	//          if ($status_checkin==0) {
	//          	// update table package 
	// 			$this->fg->where('barcode_id',$scan);
	// 			$this->fg->update('package', array('updated_at'=>date('Y-m-d H:i:s'),'current_department'=>'sewing','current_status'=>'onprogress','status_sewing'=>'onprogress'));

	// 			// update table package movement
	// 			$this->fg->where('deleted_at is NULL',NULL,FALSE);
	// 			$this->fg->where(array('barcode_package'=>$scan,'is_canceled'=>'f'));
	// 			$this->fg->update('package_movements', array('deleted_at'=>date('Y-m-d H:i:s')));

	// 			// insert table package movement
	// 			$line_id = $this->FoldingModel->getLineFg()['id'];
	// 			$user_id = ($this->session->userdata('factory')==2?44:45);
	// 			$detail_movement = array(
	// 				'barcode_package' =>$scan , 
	// 				'department_from' =>'preparation', 
	// 				'department_to'   =>'sewing', 
	// 				'status_to'       =>'onprogress', 
	// 				'status_from'     =>'completed', 
	// 				'user_id'         => $user_id, 
	// 				'description'     =>'accepted on checkin sewing', 
	// 				'created_at'      =>date('Y-m-d H:i:s'), 
	// 				'line_id'         =>$line_id, 
	// 				'ip_address'      =>$this->input->ip_address() 
	// 			);
	// 			$this->fg->insert('package_movements', $detail_movement);
	//          }
			
	// 	}
	// }
	private function package_detail($scan,$id)
	{
		$header = $this->FoldingModel->header($id);
			
		$package = $this->FoldingModel->package($scan);

		$package_detail = $this->FoldingModel->package_detail($package['scan_id']);

		$po_summary = $this->FoldingModel->po_summary($header['poreference']);

		$counterscan = $this->FoldingModel->counterScan($scan,$header['poreference'],$package_detail['manufacturing_size']);
		
		$detail  = array(
			'header'         =>$header, 
			'package'        =>$package, 
			'package_detail' =>$package_detail, 
			'counterscan'    =>$counterscan, 
			'po_summary'     =>$po_summary 
		);
		return $detail;

	}
	private function tablePakcageFolding($scan_id,$remark,$package,$poreference,$size)
	{
		$folding_package = $this->FoldingModel->foldingPackage($scan_id);
		$totalKarton     = $this->FoldingModel->totalKarton($scan_id);
		$po_summary = $this->FoldingModel->po_summary($poreference);
		$counterscan = $this->FoldingModel->counterScan($scan_id,$poreference,$size);
		
	    $draw =''; 
	   	
		$draw.='<div class="alert alert-block alert-info fade in">
	          <button data-dismiss="alert" class="close close-sm" type="button">
	              <i class="icon-remove"></i>
	          </button>
	          <div class="form-group"> 
		        <div class="row"> 
		          <div class="col-xs-8">
		          
		            <label>Tgl Export : </label> 
		            <h3><span class="tglexport">'.date('d-m-Y', strtotime($po_summary['datepromised'])).'</span></h3>
		          </div> 
		 
		          <div class="col-xs-4"> 
		            <label>Qty Karton :</label> 
		            <h3><span class="qty_karton">'.$package->row_array()['inner_pack'].$remark.'</span> </h3>
		           
		          </div> 
		        </div> 
		      </div>
		      <div class="form-group"> 
		        <div class="row"> 
		          <div class="col-xs-8">
		          
		            <label>Qty Scan : </label> 
		            <h3><span class="qty_scan">'.$counterscan.'</span> PCS</h3>
		          </div> 
		 
		          <div class="col-xs-4"> 
		            <label>Total Karton :</label> 
		            <h3><span class="total_karton">'.$totalKarton.'</span><span>'.'/'.$package->row_array()['pkg_count'].'</span></h3>
		           
		          </div> 
		        </div> 
		      </div>
		      <div class="form-group"> 
		        <div class="row"> 
		          <div class="col-md-4">
		            <div id="hasil"></div>
		          </div> 
		        </div> 
		      </div>  
	      </div>';

	      return $draw;
	}
	public function foldingUpSets($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {
			$this->db->trans_start();
			$cekHeader = $this->FoldingModel->GetHeader($post['id'],trim($post['size']),trim($post['style']));
			$counter_folding = $this->FoldingModel->counterFoldingMax($post['id'],trim($post['size']))+1;
			$countSets       = $this->FoldingModel->counter_folding($post['id'],trim($post['size']))['counter_sets']+1;

			if ($cekHeader->num_rows()>0) {
				$this->insertFolding($post,$counter_folding,$countSets,$cekHeader->row_array()['id']);
			}else{
				$header_id = $this->uuid->v4();
				$this->InsertFoldingHeaderButton($post,$countSets,$header_id);
				$this->insertFolding($post,$counter_folding,$countSets,$header_id);

			}
			

			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
				$status = 500;
				$pesan  = "Simpan Data Gagal";
			}else{
				$this->db->trans_complete();
				$status = 200;
				$pesan  = "OK";
			}

			$data = array(
				'status' => $status, 
				'pesan' => $pesan 
			);
			echo json_encode($data);

		}
	}
	public function foldingUpSetsScan($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			
			$getBarcodeGarment = $this->FoldingModel->getBarcodeGarment($post['poreference'],$post['size']);

			if ($getBarcodeGarment->num_rows()==0||$getBarcodeGarment->row_array()['barcode_garment']==$post['scan']) {
				$this->db->trans_start();
				

				

				$this->updateSets($post);
				if ($post['complete']==1) {
					$this->complete_karton($post['barcode_package'], $post['source_data']);
				}
				// $wip = $this->wipSets($post['poreference'],$post['size']);
				$status = $this->db->get_where('folding_package_barcode', array('barcode_id'=>$post['barcode_package']))->row_array()['status'];
				$draw='';
				if ($status=='completed') {
					$draw.='<h3><span class="status label label-danger">Completed</span></h3>';
				}else if ($status=='onprogress') {
					$draw.='<h3><span class="status label label-primary">OnProgress</span></h3>';
				}

				if ($this->db->trans_status() == FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
					
					$pesan  = "Insert Data gagal";					
				}else{
					$this->db->trans_complete();
					$status = 200;
					$pesan  = "OK";
				}
				$data = array(
					'status' => $status, 
					'pesan'  => $pesan,
					'draw'   => $draw
					// 'wip_1'  => $wip['wip_1'],
					// 'wip_2'  => $wip['wip_2'] 
				);
				echo json_encode($data);	
			}else{
				$status = 500;
				$pesan  = "Barode Garment salah,silahkan cek kembali";
				$data = array(
					'status' => $status, 
					'pesan'  => $pesan,
				);
				echo json_encode($data);
			}
		}
	}
	public function foldingUpSetsScanMasal($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			
			// if ($post['complete']==1) {
				$this->db->trans_start();
				
				$poreference     = trim($post['poreference']);
				$size            = trim($post['size']);
				$scan_id         = $post['scan_id'];
				$inputan         = $post['qty'];
				$in_header       = $post['id'];
				$barcode_package = $post['barcode_package'];
				$source_data 	 = $post['source_data'];
				
				$this->db->limit(1);
				$this->db->order_by('sum(counter_sets)', 'asc');
				$this->db->group_by('style,poreference,size');
				$this->db->where('poreference', $poreference);
				$this->db->where('size', $size);
				$this->db->select('style,poreference,size,sum(counter_sets) as counter_sets');
				$cekOutputSets = $this->db->get('folding_header')->row_array();
				$counter = (int)$cekOutputSets['counter_sets']-(int)$post['qty'];
				
				
				$draw='';
				
				if($counter >= 0){
					if ($post['flag_barcode']==0) {
						$this->InsertBarcodeGarment($post);

					}
					$cek = $this->updateSets($post);

					$status = $this->db->get_where('folding_package_barcode', array('barcode_id'=>$post['barcode_package']))->row_array()['status'];
					
					if ($status=='completed') {
						$draw.='<h3><span class="status label label-danger">Completed</span></h3>';
					}else if ($status=='onprogress') {
						$draw.='<h3><span class="status label label-primary">OnProgress</span></h3>';
					}
					if ($cek==1) {
						if ($this->db->trans_status() == FALSE)
						{
							$this->db->trans_rollback();
							$status = 500;
							$pesan  = "Simpan Data gagal";					
						}else{
							$this->db->trans_complete();
							$status = 200;
							$pesan  = "OK";
						}
					}else{
						$status = 500;
						$pesan  = "Simpan Data gagal";	
					}
				}
				else{
					$status = 504;
					$pesan  = "Simpan Data gagal, Cek WIP";		
				}
				
				
				$data = array(
					'status' => $status, 
					'pesan'  => $pesan,
					'draw'   => $draw
					// 'wip_1'  => $wip['wip_1'],
					// 'wip_2'  => $wip['wip_2'] 
				);
				echo json_encode($data);	

			// }
		}
	}
	public function folding_up($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {
			$remark          = $post['remark'];
			$id              = $this->input->post('id');
			// $header          = $this->FoldingModel->header($id);
			$poreference     = $post['poreference'];
			$size            = $post['size'];
			$draw 			 = '';
			if ($remark=='PCS') {
				// $package         = $this->FoldingModel->package($post['barcode_package']);
				$getBarcodeGarment = $this->FoldingModel->getBarcodeGarment($poreference,$size);
				if ($getBarcodeGarment->num_rows()==0||$getBarcodeGarment->row_array()['barcode_garment']==$post['scan']) {
					$this->db->trans_start();
					$this->insertFolding($post);

					// $data = $this->foldingdetail($id,$post['size_id'],$post['barcode_package'],$package['scan_id']);
					// $totalGarment = $this->FoldingModel->counterPackageBarcode($post['barcode_package']);

					if ($post['complete']==1) {
						$this->complete_karton($post['barcode_package']);
					}
					$status = $this->db->get_where('folding_package_barcode', array('barcode_id'=>$post['barcode_package']))->row_array()['status'];

					$draw='';
					if ($status=='completed') {
						$draw.='<h3><span class="status label label-danger">Completed</span></h3>';
					}else if ($status=='onprogress') {
						$draw.='<h3><span class="status label label-primary">OnProgress</span></h3>';
					}
					if ($this->db->trans_status() == FALSE)
					{
						$this->db->trans_rollback();
						$status = 500;
						$pesan  = "Insert Data gagal";
					}else{
						$this->db->trans_complete();
						$status = 200;
						$pesan  = "OK";
					}
				}else{
					$status = 500;
					$pesan  = "Barode Garment salah,silahkan cek kembali";
				}
			}else{
				if ($post['barcode_package']!=NULL) {
					$package           = $this->FoldingModel->package($post['barcode_package']);
					$folding_package   = $this->FoldingModel->foldingPackage($package['scan_id'])['inner_pack'];
					$counter_package   = $this->FoldingModel->counterPackageBarcode($post['barcode_package']);
					$getBarcodeGarment = $this->FoldingModel->getBarcodeGarment($poreference,$size);

					if ($getBarcodeGarment->num_rows()==0||$getBarcodeGarment->row_array()['barcode_garment']==$post['scan']) {
						
						if ((int)$counter_package<(int)$folding_package*2) {

							/*$getUnallocation =  $this->FoldingModel->getUnallocation($poreference,$size)->num_rows();
							
							if ($getUnallocation!=0) {*/
								$this->db->trans_start();

								$this->insertFolding($post);

								/*$getIdUnallocation =  $this->FoldingModel->getUnallocation($poreference,$size,1)->row_array()['folding_id'];
								
								$this->db->where('folding_id', $getIdUnallocation);
								$this->db->update('folding', array('barcode_package'=>$post['barcode_package'],'barcode_garment'=>$post['scan'],'scan_id'=>$package['scan_id'],'status_folding'=>'allocation'));*/

								/*$totalGarment = $this->FoldingModel->counterPackage($id,$size,$post['barcode_package']);*/
								$totalGarmentSets = $this->FoldingModel->counterPackageBarcode($post['barcode_package']);

								// $totalGarmentSets = $totalGarment*2;
								$foldingPackageSets = $folding_package*2;

								if ($totalGarmentSets==$foldingPackageSets) {
									$this->complete_karton($post['barcode_package']);
									$this->db->where('barcode_package', $post['barcode_package']);
									$this->db->update('folding', array('barcode_garment'=>$getBarcodeGarment->row_array()['barcode_garment']));
									$draw.='<h3><span class="status label label-danger">Completed</span></h3>';
								}else{
									$draw.='<h3><span class="status label label-primary">OnProgress</span></h3>';
								}
								
								/*$notif='1';
								$data = array('notif' => $notif,'draw'=>$draw );
								echo json_encode($data);*/
								if ($this->db->trans_status() == FALSE)
								{
									$this->db->trans_rollback();
									$status = 500;
									$pesan  = "Insert Data gagal";
								}else{
									$this->db->trans_complete();
									$status = 200;
									$pesan  = "OK";
								}
							/*}else{
								
								$status = 500;
								$pesan  = "QTY TOP & BOTTOM tidak sama";
							}*/
								
						}else{
							/*$data['notif'] = "2";
							echo json_encode($data);*/
							$status = 500;
							$pesan  = "QTY Karton Complete";
						}
					}else{
						$status = 500;
						$pesan  = "Barode Garment salah,silahkan cek kembali";
					}
				}else{
					$this->db->trans_start();

					$this->insertFolding($post);

					if ($this->db->trans_status() == FALSE)
					{
						$this->db->trans_rollback();
						$status = 500;
						$pesan  = "Insert Data gagal";
					}else{
						$this->db->trans_complete();
						$status = 200;
						$pesan  = "OK";
					}
					
				}				
			}
			$data = array(
				'status' => $status, 
				'draw' => $draw, 
				'pesan' => $pesan 
			);
			echo json_encode($data);					
		}
	}
	private function insertFolding($post,$counter_folding,$countSets,$folding_header_id)
	{
		$id              = $this->input->post('id');
		$poreference     = trim($post['poreference']);
		$size            = trim($post['size']);

		$status          = ($post['remark']!='SETS'?'allocation':'unallocation');
		$style = trim($post['style']);

		$foldingDetail = array(
			'folding_id'         => $this->uuid->v4(),
			'inline_header_id'   => $id,
			'folding_header_id'  => $folding_header_id,
			// 'barcode_package' => $barcode_package,
			// 'barcode_garment' => $scan,
			'counter_folding'    => $counter_folding,
			'status_folding'     => $status,
			// 'scan_id'         => $scan_id,
			'style'              => $style,
			'size'               => $size,
			'poreference'        => $poreference,
			'factory_id'         => $this->session->userdata('factory'),
			'remark'             => $post['remark'],
			'create_by'          => $this->session->userdata('nik'),
			'ip'                 => $this->input->ip_address()
		);

		$this->db->insert('folding', $foldingDetail);

		$this->db->where('style', $style);
		$this->db->where('size', $size);
		$this->db->where('inline_header_id', $id);
		$this->db->update('folding_header', array('counter_sets'=>$countSets));
	}
	private function wipSets($poreference,$size)
	{
		$getStyles = $this->FoldingModel->getStyle($poreference,$size);

		$wip_1 =0;
		$wip_2 =0;

		if ($getStyles->num_rows()==1) {
			$wip_1 = $this->FoldingModel->wipStyleSets($poreference,$size,$getStyles->result()[0]->style)->num_rows();
		}

		if ($getStyles->num_rows()>1) {
			$wip_1 = $this->FoldingModel->wipStyleSets($poreference,$size,$getStyles->result()[0]->style)->num_rows();
			$wip_2 = $this->FoldingModel->wipStyleSets($poreference,$size,$getStyles->result()[1]->style)->num_rows();
		}
		$data = array(
			'wip_1' => $wip_1, 
			'wip_2' => $wip_2, 
		);
		return $data;
	}
	private function updateSets($post)
	{
		$folding_id        = array();
		$folding_header_id = array();
		$poreference       = trim($post['poreference']);
		$size              = trim($post['size']);
		$style             = trim($post['style']);
		$getMinSets        = $this->FoldingModel->getMinSets($poreference,$size);
		foreach ($getMinSets->result() as $key => $get) {
		
			$folding_ids = $this->FoldingModel->getUnallocation($get->style,$get->poreference,$get->size,$post['qty']);
			
			foreach ($folding_ids->result() as $key => $id) {
				$folding_id[] = $id->folding_id;
				$folding_header_id[] = $id->folding_header_id;
			}
		}
		$cek = count($folding_id);
		
		if ($cek==$post['qty']*2) {

			$this->db->group_by('folding_header_id');
			$this->db->where_in('folding_id', $folding_id);
			$this->db->select('count(counter_folding) as counter , folding_header_id');
			$f_count = $this->db->get('folding')->result();

			foreach ($f_count as $f) {
				$f_header = $this->db->query("SELECT * from folding_header where id = '$f->folding_header_id'")->row();

				$counter = (int)$f_header->counter + (int)$f->counter;
				$counter_sets = (int)$f_header->counter_sets - (int)$f->counter;

				$this->db->query("UPDATE folding_header
				SET counter = $counter, counter_sets = $counter_sets,style_sets='$style'
				WHERE id = '$f->folding_header_id'");
			}


			$this->db->where_in('folding_id', $folding_id);
			$this->db->update('folding', array('barcode_package'=>$post['barcode_package'],'scan_id'=>$post['scan_id'],'status_folding'=>'allocation','scan_date'=>date('Y-m-d H:i:s')));
			
			$count = $this->FoldingModel->counterScan($post['barcode_package']);
			$counter_barcode = (int)$count+(int)$post['qty'];

			$this->db->where('barcode_id', $post['barcode_package']);
			$this->db->update('folding_package_barcode', array('counter'=>$counter_barcode));

			if ($post['complete']==1) {
				$this->complete_karton($post['barcode_package'], $post['source_data']);
			}
			return 1;
			
		}else{
			return 2;
		}
	
		
	}
	private function insertPackage($package_detail)
	{
		$cekPackage = $this->db->get_where('folding_package',array('scan_id'=>$package_detail['scan_id']))->num_rows();

		if ($cekPackage==0) {
			$detail = array(
				'package_id'  =>$this->uuid->v4(),
				'scan_id'     =>$package_detail['scan_id'], 
				'poreference' =>$package_detail['po_number'], 
				'item_qty'    =>$package_detail['item_qty'], 
				'inner_pack'  =>$package_detail['inner_pack'], 
				'size'        =>$package_detail['manufacturing_size'], 
				'pkg_count'   =>$package_detail['pkg_count'] 
			);
			$this->db->insert('folding_package', $detail);
		}
		
	}
	private function insertPackageNew($package_detail)
	{
		$cekPackage = $this->db->get_where('folding_package',array('scan_id'=>$package_detail->scan_id))->num_rows();
		// var_dump($cekPackage); die();

		if ($cekPackage==0) {
			$detail = array(
				'package_id'  =>$this->uuid->v4(),
				'scan_id'     =>$package_detail->scan_id, 
				'poreference' =>$package_detail->po_number, 
				'item_qty'    =>$package_detail->inner_pack, 
				'inner_pack'  =>$package_detail->inner_pack, 
				'size'        =>$package_detail->detail[0]->size, 
				'pkg_count'   =>$package_detail->pkg_count
			);
			$this->db->insert('folding_package', $detail);
		}
		
	}

	
	private function insertPackageBarcode($barcode_id,$scan_id)
	{
		$cekBarcode = $this->db->get_where('folding_package_barcode',array('barcode_id'=>$barcode_id))->num_rows();
		
		if ($cekBarcode==0) {
			$user = $this->session->userdata('nik');
			$detail = array(
				'package_barcode_id' =>$this->uuid->v4(),
				'scan_id'            =>$scan_id, 
				'barcode_id'         =>$barcode_id, 
				'create_by'          =>$user, 
				'line_id'            =>$this->session->userdata('line_id'), 
				'status'             =>'onprogress' 
			);
			$this->db->insert('folding_package_barcode', $detail);
		}
	}
	// private function insertPackageBarcode($barcode_id,$scan_id)
	// {
	// 	$cekBarcode = $this->db->get_where('folding_package_barcode',array('barcode_id'=>$barcode_id))->num_rows();
		
	// 	if ($cekBarcode==0) {
	// 		$user = $this->session->userdata('nik');
	// 		$detail = array(
	// 			'package_barcode_id' =>$this->uuid->v4(),
	// 			'scan_id'            =>$scan_id, 
	// 			'barcode_id'         =>$barcode_id, 
	// 			'create_by'          =>$user, 
	// 			'line_id'            =>$this->session->userdata('line_id'), 
	// 			'status'             =>'onprogress' 
	// 		);
	// 		$this->db->insert('folding_package_barcode', $detail);
	// 	}
	// }
	
	private function complete_karton($barcode_package, $source_data)
	{
		// $package = $this->FoldingModel->package($barcode_package);
		// var_dump($package); die();

		$token = $this->getToken();
		if($token != NULL){
			
			$token = $token->data->token;	

			// if ($package['barcode_id']!=NULL) {
				$carton_url = 'http://mid.bbigarment.co.id/api/pdc/checkout';
	
				$this->curl->create($carton_url);
	
				// Set each HTTP header individually
				$factory_id = $this->session->userdata('factory');
				
				if($factory_id == 1){
					$user = 'lineaoi1';
				} else if($factory_id == 2){
					$user = 'lineaoi2';
				} else{
					$user = 'lineaoi3';
				}

				$line_name =$this->db->get_where('master_line', array('master_line_id'=>$this->session->userdata('line_id')))->row_array()['line_name'];

				$this->pdc_new->where('name', $line_name);
				$line_pdc = $this->pdc_new->get('master_line')->row_array();

				$checkout_data = [
					'user' 			=> $user,
					'barcode_id' 	=> $barcode_package,
					'line_id' 		=> ($line_pdc != null) ? $line_pdc['id'] : $this->session->userdata('line_id'),
					'source_data' 	=> $source_data
				];		
				$this->curl->http_header('Authorization: Bearer ' . $token);				
				$this->curl->post($checkout_data);
				$result = $this->curl->execute();
	
				// update table folding package 			
				$this->db->where('barcode_id',$barcode_package);
				$this->db->update('folding_package_barcode', array('status'=>'completed','complete_by'=>$this->session->userdata('nik'),'complete_at'=>date('Y-m-d H:i:s'),'backto_sewing'=>'f'));
			// }
			
		}
		
	}
	// private function complete_karton($barcode_package)
	// {
	// 	$package = $this->FoldingModel->package($barcode_package);
		
	// 	if ($package['barcode_id']!=NULL) {
	// 		// update table package 
	// 		$line_id = $this->FoldingModel->getLineFg()['id'];
	// 		$user_id = ($this->session->userdata('factory')==2?44:45);
	// 		$this->fg->where('barcode_id',$barcode_package);
	// 		$this->fg->update('package', array('updated_at'=>date('Y-m-d H:i:s'),'current_department'=>'sewing','current_status'=>'completed','status_sewing'=>'completed'));

	// 		// update table package movement
	// 		$this->fg->where('deleted_at is NULL',NULL,FALSE);
	// 		$this->fg->where(array('barcode_package'=>$barcode_package,'is_canceled'=>'f'));
	// 		$this->fg->update('package_movements', array('deleted_at'=>date('Y-m-d H:i:s')));

	// 		// insert table package movement
	// 		$detail_movement = array(
	// 			'barcode_package' =>$barcode_package , 
	// 			'department_from' =>'sewing', 
	// 			'department_to'   =>'sewing', 
	// 			'status_from'     =>'onprogress', 
	// 			'status_to'       =>'completed', 
	// 			'user_id'         => $user_id, 
	// 			'description'     =>'set as completed on sewing', 
	// 			'created_at'      =>date('Y-m-d H:i:s'), 
	// 			'line_id'         =>$line_id,
	// 			'ip_address'      =>$this->input->ip_address() 
	// 		);
	// 		$this->fg->insert('package_movements', $detail_movement);

	// 		$this->db->where('barcode_id',$barcode_package);
	// 		$this->db->update('folding_package_barcode', array('status'=>'completed','complete_by'=>$this->session->userdata('nik'),'complete_at'=>date('Y-m-d H:i:s'),'backto_sewing'=>'f'));
	// 	}
	// }
	public function showPackage($post=NULL)
	{
		$post = $this->input->post();
		
		if ($post!=NULL) {
			$detail = $this->package_detail($post['barcode_id'],$post['id']);
			
			$draw = $this->tablePakcageFolding($detail);
			$data = array(
				'barcode_id' => $post['barcode_id'],  
				'scan_id'    => $detail['package_detail']['scan_id'], 
				'draw'=>$draw
			);
			echo json_encode($data);
		}
	}
	public function showOutputFolding()
	{
		$date = date('Y-m-d');
		
		$line_id       = $this->session->userdata('line_id');
		$line          = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();
		// $totaloutput   = $this->FoldingModel->totalOutputFolding($date)->row_array()['daily_output'];
		// $totalkomplete = $this->FoldingModel->totalCompleteCarton($date)->num_rows();
		
		$data = array(
			'line'          => $line,
			// 'totaloutput'   => $totaloutput,
			// 'totalkomplete' => $totalkomplete,
		);

		$this->template->set('title',$line['line_name'].' - Folding');
        $this->template->set('desc_page','Folding');
		$this->template->load('layout2','folding/show_outputfolding',$data);

		// $this->load->view('folding/show_outputfolding',$data);
	}

	public function filter_output_folding($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$pobuyer = $post['pobuyer'];
			$output  = $this->FoldingModel->output_karton_no_ajax($pobuyer);

			if(empty($output)){
				$master = 0;
			}
			else{
				$master = $output;
			}
			$data = array (
				'pobuyer' => $pobuyer,
				'master'  => $master
			);
			
			$this->load->view("folding/list_output_folding",$data);

			// echo "sukses";
		}
	}

	function show_output_folding()
	{

		// $date = date('Y-m-d');
		// $data['totaloutput'] = $this->FoldingModel->totalOutputFolding($date)->row_array()['output'];
		// $data['totalkomplete'] = $this->FoldingModel->totalCompleteCarton($date)->num_rows();
		
		// $json_data = array(
		// 	"data"            => $data
		// );

		// echo json_encode($json_data);
		echo json_encode('0');
	}
	public function output_ajax()
	{
		$columns = array(
                            0 =>'line_id',
                            1 =>'date',
                            2 =>'poreference',
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        $date = $this->input->post('tanggal');


	        // $date = date('Y-m-d');

	        $totalData = $this->FoldingModel->allposts_count_outputfolding($date);

	        $totalFiltered = $totalData;
	       
	        if(empty($this->input->post('search')['value'])&&$date=='')
	        {
	            $master = $this->FoldingModel->allposts_outputfolding($limit,$start,$order,$dir);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $master =  $this->FoldingModel->posts_search_outputfolding($limit,$start,$search,$order,$dir,$date);

	            $totalFiltered = $this->FoldingModel->posts_search_count_outputfolding($search,$date);
	        }
	        $data = array();
	        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {
	            	$date = date('Y-m-d');
	            	$carton_complete = $this->FoldingModel->CartonSend($master->inline_header_id,$master->poreference,$master->size,$master->style);
	            	$total_karton = $this->FoldingModel->countCarton($master->inline_header_id,$master->poreference,$master->size,$master->style);
	            	$total_karton_day = $this->FoldingModel->CartonSend($master->inline_header_id,$master->poreference,$master->size,$master->style,$date);
					$nestedData['date']            = $master->date;
					$nestedData['line']            = $master->line_name;
					$nestedData['poreference']     = $master->poreference;
					$nestedData['style']           = $master->style;
					$nestedData['size']            = $master->size;
					$nestedData['order']           = $master->order;
					$nestedData['total_folding']   = $master->total_folding;
					$nestedData['daily_output']    = $master->daily_output;
					$nestedData['balance_folding'] = $master->balance_folding;
					$nestedData['complete_day'] = $total_karton_day;
					$nestedData['carton_complete'] = $carton_complete."/".$total_karton;			
					
	                $data[] = $nestedData;
	            }
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
	}

	// public function folding_output_ajax()
	// {
	// 	$columns = array(
	// 		0 =>'poreferenee',
	// 		1 =>'style',
	// 		2 =>'size',


	// 		// PO, STYLE, SIZE, JUMLAH KARTON KOMPLIT, QTY SCAN, DETAIL
	// 	);
	// 	$limit = $this->input->post('length');
	// 	$start = $this->input->post('start');
	// 	$order = $columns[$this->input->post('order')[0]['column']];
	// 	$dir   = $this->input->post('order')[0]['dir'];
	// 	$draw  = $this->input->post('draw');

	// 	$pobuyer = $this->input->post('pobuyer');

	// 	if(!empty($pobuyer)){
	// 		$totalData = $this->FoldingModel->output_karton_count($pobuyer);

	// 		$totalFiltered = $totalData;
	
	// 		$master = $this->FoldingModel->output_karton_all($limit,$start,$order,$dir,$pobuyer);	
	// 	}else{
	// 		$totalData = 0;
	// 		$totalFiltered = 0;
	// 		$master = null;
	// 	}
		
	// 	$data = array();

	// 	$nomor_urut = 0;
	// 	if(!empty($master))
	// 	{	
	// 		foreach ($master as $master)
	// 		{
	// 			$temp = '"'.$master->poreference.'","'.$master->size.'"';

	// 			$nestedData['poreference']  = $master->poreference;
	// 			$nestedData['style']        = $master->style;
	// 			$nestedData['size']         = $master->size;
	// 			$nestedData['total_karton'] = $master->total_karton;
	// 			$nestedData['total_qty']    = $master->total;
	// 			$nestedData['action']       = "<center><a onclick='return detail($temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-align-justify'></i> Detail</a></center>";
				
	// 			$data[] = $nestedData;
	// 		}
	// 	}

	// 	$json_data = array(
	// 			"draw"            => intval($this->input->post('draw')),
	// 			"recordsTotal"    => intval($totalData),
	// 			"recordsFiltered" => intval($totalFiltered),
	// 			"data"            => $data
	// 			);

	// 	echo json_encode($json_data);
	// }

	public function detail_folding_output($get=NULL)
	{
		$get = $this->input->get();

		if ($get!=NULL) {
			$po   = $get['po'];
			$size = $get['size'];
			$detail  = $this->FoldingModel->detail_karton_no_ajax($po,$size);

			$data = array(
				'poreference' => $po,
				'size'        => $size,
				'detail'      => $detail
			);

			// var_dump($data);
			// die();
			// $data['list_karton']	= $this->FoldingModel->output_folding_detail($po,$size);
			$this->load->view('folding/show_detail_output',$data);			
		}
	}

	public function folding_detail_ajax()
	{
		$columns = array(
			0 => 'barcode_package',
			8 => 'create_on',
			9 => 'complete_on'



			// PO, STYLE, SIZE, JUMLAH KARTON KOMPLIT, QTY SCAN, DETAIL
		);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$pobuyer = $this->input->post('pobuyer');
		$size    = $this->input->post('size');
		// $search  = $this->input->post('karton');
		// $search  = $this->input->post('search')['value'];
		// var_dump($pobuyer);
		// var_dump($size);
		// var_dump($karton);
		// die();

		$totalData = $this->FoldingModel->output_folding_detail_count($pobuyer,$size);

		$master = $this->FoldingModel->output_folding_detail_all($limit,$start,$order,$dir,$pobuyer,$size);

		$totalFiltered = $totalData;

		$data = array();
		$nomor_urut = 0;
		if(!empty($master))
		{	
			foreach ($master as $master)
			{
				$nestedData['poreference']     = $master->poreference;
				$nestedData['style']           = $master->style;
				$nestedData['size']            = $master->size;
				$nestedData['qty_size']        = $master->total;
				$nestedData['qty_karton']      = $master->inner_pack;
				$nestedData['barcode_package'] = $master->barcode_package;
				$nestedData['nama_line']       = $master->line_name;
				$nestedData['create_on']       = $master->create_on;
				$nestedData['complete_on']     = $master->complete_on;
				$data[]						   = $nestedData;
			}
		}

		$json_data = array(
				"draw"            => intval($this->input->post('draw')),
				"recordsTotal"    => intval($totalData),
				"recordsFiltered" => intval($totalFiltered),
				"data"            => $data
				);

		echo json_encode($json_data);
	}

	public function searchoutput($tanggal=NULL)
	{
		$tanggal =$this->input->get('tanggal');
		if ($tanggal!=NULL) {

			$totaloutput = $this->FoldingModel->totalOutputFolding($tanggal)->row_array()['output'];
			$totalkomplete = $this->FoldingModel->totalCompleteCarton($tanggal)->num_rows();
			$data = array(
				'totaloutput' => $totaloutput, 
				'totalkomplete' => $totalkomplete
			);
			echo json_encode($data);
		}
	}
	public function foldingupmasal($post=NULL)
	{
		$post = $this->input->post();
		// var_dump($post); die();
		if ($post!=NULL) { 
			$poreference     = trim($post['poreference']);
			$size            = trim($post['size']);
			$scan_id         = $post['scan_id'];
			$inputan         = $post['qty'];
			$style           = $post['style'];
			$article         = $post['article'];
			$in_header       = $post['id'];
			$barcode_package = $post['barcode_package'];
			$source_data 	 = $post['source_data'];

			$getCounter = $this->FoldingModel->cekValidasi($poreference,$size,$style,$in_header);
			if($getCounter->result()!=NULL){
				foreach($getCounter->result() as $getc){
					$counter = $getc->counter;
					$tambah = (int)$counter + (int)$inputan;

					$data_free = $this->db->query("SELECT sum(qty) as sum from wip_free_stock where 
					inline_header_id = '$getc->inline_header_id' and size = '$getc->size'")->row_array();

					$outputan_qc = ((int)$getc->qc_output + (int)$data_free['sum']);

					// if($tambah<=$getc->qc_output){
					if($tambah<=$outputan_qc){
						$cekKarton = $this->FoldingModel->foldingPackage($scan_id)['inner_pack'];
						$isiKarton = $this->FoldingModel->counterScanSets($barcode_package,$poreference);
						$isi = (int)$isiKarton + (int)$inputan;
						$scan_status = "aman";
						// if($isi <= (int)$cekKarton){
						// 	$scan_status = "aman";
						// }
						// else{
						// 	$scan_status = "bermasalah";	
						// }
					}
					else{
						$scan_status = "bermasalah";
					}
				}
			}
			else{
				$scan_status = "bermasalah";
			}
			

			$draw='';
			if($scan_status == "aman"){

				$this->db->trans_start();

				$cekHeader = $this->FoldingModel->GetHeader($post['id'],trim($post['size']),$post['style']);
				// var_dump($cekHeader->row_array()); die();
				$counter_folding = ($cekHeader->row_array() != null)? $cekHeader->row_array()['counter'] : 0;

				if ($cekHeader->num_rows()==0) {
					$folding_header_id = $this->uuid->v4();
					$this->InsertFoldingHeaderScan($post,$folding_header_id);
					$this->InserFoldingDetail($post,$counter_folding,$folding_header_id);
				}else{
					$folding_header_id = $cekHeader->row_array()['id'];
					$this->InserFoldingDetail($post,$counter_folding,$folding_header_id);
				}
				if ($post['flag_barcode']==0) {
					$this->InsertBarcodeGarment($post);

				}
				if ($post['complete']==1) {
					$this->complete_karton($post['barcode_package'], $source_data);
				}


				$status = $this->db->get_where('folding_package_barcode', array('barcode_id'=>$post['barcode_package']))->row_array()['status'];

				// $draw='';
				if ($status=='completed') {
					$draw.='<h3><span class="status label label-danger">Completed</span></h3>';
				}else if ($status=='onprogress') {
					$draw.='<h3><span class="status label label-primary">OnProgress</span></h3>';
				}
				if ($this->db->trans_status() == FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
					$pesan  = "Insert Data Gagal";
				}else{
					$this->db->trans_complete();
					$status = 200;
					$pesan  = "OK";
				}
			}
			else{
				$this->db->trans_rollback();
				$status = 504;
				$pesan  = "Insert Data Gagal, Cek WIP QC";		
			}
			$data = array(
				'status' => $status,
				'draw'   => $draw,
				'pesan'  => $pesan
			);
			echo json_encode($data);
		}
	}
	private function InsertBarcodeGarment($post)
	{
		$barcode_garment_id = $this->uuid->v4();
		$detail = array(
			'id'          => $barcode_garment_id,
			'barcode_id'  => trim($post['scan']), 
			'style'       => trim($post['style']), 
			'poreference' => trim($post['poreference']), 
			'article'     => trim($post['article']), 
			'size'        => trim($post['size']), 
			'create_by'   => $this->session->userdata('nik')
 		);
		$this->db->insert('barcode_garment', $detail);

		$this->db->where('inline_header_id', $post['id']);
		$this->db->where('size', $post['size']);
		$this->db->update('folding_header', array('barcode_garment_id'=>$barcode_garment_id));
	}
	private function InserFoldingDetail($post,$counter_folding,$folding_header_id)
	{
		$qty             = $post['qty'];
		$id              = $post['id'];
		$poreference     = trim($post['poreference']);
		$size            = trim($post['size']);
		$barcode_package = $post['barcode_package'];
		$status          = 'allocation';
		$scan            = $post['scan'];
		for ($i=1;$i<=$qty;$i++){
			$counter_folding+=1;
            $detail = array(
				'folding_id'        => $this->uuid->v4(),
				'inline_header_id'  => $id,
				'barcode_package'   => $barcode_package,
				'counter_folding'   => $counter_folding,
				'status_folding'    => $status,
				'scan_id'           => trim($post['scan_id']),
				'style'             => trim($post['style']),
				'size'              => $size,
				'poreference'       => $poreference,
				'factory_id'        => $this->session->userdata('factory'),
				'remark'            => $post['remark'],
				'create_by'         => $this->session->userdata('nik'),
				'folding_header_id' => $folding_header_id,
				'scan_date'         => date('Y-m-d H:i:s'),
				'ip'                => $this->input->ip_address()
			);
            $this->db->insert('folding', $detail);
        }
        $this->db->where('id', $folding_header_id);
        $this->db->update('folding_header', array('counter'=>$counter_folding,'update_at'=>date('Y-m-d H:i:s')));

        $counterscan = $this->FoldingModel->counterScan($barcode_package);

        $counter_barcode = (int)$counterscan+(int)$qty;
        $this->db->where('barcode_id', $barcode_package);
        $this->db->update('folding_package_barcode', array('counter'=>$counter_barcode));
	}
	private function InsertFoldingHeaderScan($post,$folding_header_id)
	{
		$detailInsert = array(
			'id'               => $folding_header_id, 
			'inline_header_id' => $post['id'], 
			'line_id'          => $this->session->userdata('line_id'), 
			'factory_id'       => $this->session->userdata('factory'), 
			'poreference'      => trim($post['poreference']), 
			'size'             => trim($post['size']), 
			'style'            => trim($post['style']), 
			'style_sets'       => trim($post['style']), 
			'counter'          => $post['qty'], 
			'remark'           => $post['remark'], 
			'update_at'        => date('Y-m-d H:i:s') 
		);
		$this->db->insert('folding_header', $detailInsert);
	}
	
	public function updateCountBarcode($bar)
	{
		$counterBarcode = $this->FoldingModel->counterScan($post['barcode_package']);
		$this->db->where('barcode_id', $post['barcode_package']);
		$this->db->update('folding_package_barcode', array('counter'=>$counterBarcode+$qty));
	}
	
	public function create_header()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getStatus = $this->FoldingModel->getStatusPO();
		foreach ($getStatus->result() as $key => $get) {
			$cekHeader = $this->FoldingModel->GetHeader($get->inline_header_id,$get->size,$get->style);
			if ($cekHeader->num_rows()==0) {
				$remark = $this->remark($get->style);
				$id     = $this->uuid->v4();
				$detailInsert = array(
					'id'               => $id, 
					'inline_header_id' => $get->inline_header_id, 
					'line_id'          => $get->line_id, 
					'factory_id'       => $get->factory_id, 
					'poreference'      => $get->poreference, 
					'size'             => $get->size, 
					'style'            => $get->style, 
					'counter'          => $get->total_folding, 
					'remark'           => $remark 
				);
				$this->db->insert('folding_header', $detailInsert);
				
				$this->db->where('inline_header_id', $get->inline_header_id);
				$this->db->where('size', $get->size);
				$this->db->update('folding', array('folding_header_id'=>$id));
			}else{
				$this->db->where('inline_header_id', $get->inline_header_id);
				$this->db->where('size', $get->size);
				$this->db->where('style', $get->style);
				$this->db->update('folding_header', array('counter'=>$get->total_folding,'update_at'=>date('Y-m-d H:i:s')));
			}
		}
	}
	public function count_barcode()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getCounters = $this->FoldingModel->getCounterBarcode();
		foreach ($getCounters->result() as $key => $get) {
			$this->db->where('barcode_id', $get->barcode_package);
			$this->db->update('folding_package_barcode', array('counter'=>$get->total));
		}
	}
	public function count_sets()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getCountersets = $this->FoldingModel->getCounterSets();
		foreach ($getCountersets->result() as $key => $get) {
			$cekHeader = $this->FoldingModel->GetHeader($get->inline_header_id,$get->size,$get->style);
			if ($cekHeader->num_rows()>0) {
				$this->db->where('style', $get->style);
				$this->db->where('size', $get->size);
				$this->db->where('inline_header_id', $get->inline_header_id);
				$this->db->update('folding_header',array('counter_sets'=>$get->count));
			}else{
				$remark = $this->remark($get->style);
				$id     = $this->uuid->v4();
				$detailInsert = array(
					'id'               => $id, 
					'inline_header_id' => $get->inline_header_id, 
					'line_id'          => $get->line_id, 
					'factory_id'       => $get->factory_id, 
					'poreference'      => $get->poreference, 
					'size'             => $get->size, 
					'style'            => $get->style, 
					'counter_sets'     => $get->count, 
					'remark'           => $remark 
				);
				$this->db->insert('folding_header', $detailInsert);
				
				$this->db->where('inline_header_id', $get->inline_header_id);
				$this->db->where('size', $get->size);
				$this->db->update('folding', array('folding_header_id'=>$id));
			}
		}

	}
	public function getcountsetsbeda()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getCountersets = $this->FoldingModel->getCounterSetsBeda();
		if ($getCountersets->num_rows()>0) {
			foreach ($getCountersets->result() as $key => $get) {
			$cekHeader = $this->FoldingModel->GetHeader($get->inline_header_id,$get->size,$get->style);
				if ($cekHeader->num_rows()>0) {
					$this->db->where('style', $get->style);
					$this->db->where('size', $get->size);
					$this->db->where('inline_header_id', $get->inline_header_id);
					$this->db->update('folding_header',array('counter_sets'=>$get->count));
				}else{
					$remark = $this->remark($get->style);
					$id     = $this->uuid->v4();
					$detailInsert = array(
						'id'               => $id, 
						'inline_header_id' => $get->inline_header_id, 
						'line_id'          => $get->line_id, 
						'factory_id'       => $get->factory_id, 
						'poreference'      => $get->poreference, 
						'size'             => $get->size, 
						'style'            => $get->style, 
						'counter_sets'     => $get->count, 
						'remark'           => $remark 
					);
					$this->db->insert('folding_header', $detailInsert);
					
					$this->db->where('inline_header_id', $get->inline_header_id);
					$this->db->where('size', $get->size);
					$this->db->update('folding', array('folding_header_id'=>$id));
				}
			}
		}
		
	}
	public function saveidle($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {

			$header_id = $post['id'];
			$size_id   = $post['size_id'];
			$size      = $post['size'];
			$style     = $post['style'];
			$qty_input = trim($post['qtyidle']);

			$cek_qc       = $this->FoldingModel->cek_qc_output($header_id,$size_id);
			$qc_output    = $cek_qc->row_array()['qc_output'];
			$cek_folding  = $this->FoldingModel->GetHeader($header_id,$size,$style);
			$counter      = ($cek_folding->row_array()['counter']==null?0:$cek_folding->row_array()['counter']);
			$counter_sets = ($cek_folding->row_array()['counter_sets']==null?0:$cek_folding->row_array()['counter_sets']);
			
			$total        = (int)$qc_output - ( (int)$counter + (int)$counter_sets );

			if($qty_input<=$total){
				$cekHeader   = $this->FoldingModel->GetHeader($post['id'],trim($post['size']),trim($post['style']));
				$counterSets = $this->FoldingModel->getallocation($post['id'],trim($post['size']),'unallocation')->num_rows();
	
				if ($cekHeader->num_rows()==0) {
					$folding_header_id = $this->uuid->v4();
	
					$this->InsertFoldingHeaderButton($post,$counterSets,$folding_header_id);
					$this->foldingupSaveIdle($post,$folding_header_id,$counterSets);
	
				}else{
					$folding_header_id = $cekHeader->row_array()['id'];
					$this->foldingupSaveIdle($post,$folding_header_id,$counterSets);
				}
				if ($this->db->trans_status() == FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
					$pesan  = "Insert Data gagal";
				}else{
					$this->db->trans_complete();
					$status = 200;
					$pesan  = "OK";
				}
			}
			else{
				$status = 500;
				$pesan  = "Qty lebih dari WIP QC";
			}

			
			$data = array(
				'status' => $status, 
				'pesan' => $pesan 
			);
			echo json_encode($data);
		}
	}
	private function InsertFoldingHeaderButton($post,$counter,$id)
	{
		$countersets = (int)$counter+(int)$post['qtyidle'];
		
		$detailInsert = array(
			'id'               => $id, 
			'inline_header_id' => $post['id'], 
			'line_id'          => $this->session->userdata('line_id'), 
			'factory_id'       => $this->session->userdata('factory'), 
			'poreference'      => trim($post['poreference']), 
			'size'             => trim($post['size']), 
			'style'            => trim($post['style']), 
			'counter_sets'     => $countersets, 
			'remark'           => $post['remark'] 
		);
		$this->db->insert('folding_header', $detailInsert);
	}
	private function foldingupSaveIdle($post,$folding_header_id,$countersets)
	{
		$counter_folding = $this->FoldingModel->counterFoldingMax($post['id'],trim($post['size']));
		$id              = $this->input->post('id');
		$poreference     = trim($post['poreference']);
		$size            = trim($post['size']);
		$qty             = trim($post['qtyidle']);		
		$status          = ($post['remark']!='SETS'?'allocation':'unallocation');
		$style           = trim($post['style']);

		$this->db->trans_start();
		for ($i=1;$i<=$qty;$i++){
			$counter_folding+=1;
            $detail = array(
				'folding_id'         => $this->uuid->v4(),
				'inline_header_id'   => $id,
				'folding_header_id'  => $folding_header_id,
				// 'barcode_package' => $barcode_package,
				// 'barcode_garment' => $scan,
				'counter_folding'    => $counter_folding,
				'status_folding'     => $status,
				// 'scan_id'         => $scan_id,
				'style'              => $style,
				'size'               => $size,
				'poreference'        => $poreference,
				'factory_id'         => $this->session->userdata('factory'),
				'remark'             => $post['remark'],
				'create_by'          => $this->session->userdata('nik'),
				'ip'                 => $this->input->ip_address()
			);
            $this->db->insert('folding', $detail);
	    }
	    $counter = $countersets+$qty;

	    $this->db->where('id', $folding_header_id);
	    $this->db->update('folding_header', array('counter_sets'=>$counter));  
	}
	public function updatecounterbeda()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getCounterBeda = $this->FoldingModel->getCounterBeda();
		
		foreach ($getCounterBeda->result() as $key => $get) {

			$this->db->where('id', $get->id);
			$this->db->update('folding_header', array('counter'=>$get->counter_alocation));
		}
	}
	public function updatecounterbedasets()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getCounterBeda = $this->FoldingModel->getCounterBedasets();
		
		foreach ($getCounterBeda->result() as $key => $get) {

			$this->db->where('id', $get->id);
			$this->db->update('folding_header', array('counter_sets'=>$get->counter_alocation));
		}
	}
	
	public function auto_complete_karton($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$scan_id         = $post['scan_id'];
			$barcode_package = $post['barcode_package'];
			$source_data = $post['source_data'];
			$draw='';

			// $karton = $this->FoldingModel->cekKarton($scan_id,$barcode_package);
			// $cekKarton = $karton->result();

			// $cekKarton = $this->FoldingModel->foldingPackage($scan_id)['inner_pack'];
			$isiKarton = $this->FoldingModel->cekKarton($scan_id,$barcode_package)['counter'];
			$kapasitas = $this->FoldingModel->cekKarton($scan_id,$barcode_package)['inner_pack'];

			if((int)$isiKarton == (int)$kapasitas){
				$this->complete_karton($barcode_package, $source_data);
				$draw.='<h3><span class="status label label-danger">Completed</span></h3>';
			}else{
				$draw.='<h3><span class="status label label-primary">OnProgress</span></h3>';
			}
			$status = 100;
			$pesan  = "OK";		
		}
		else{
			$status = 500;
			$pesan  = "Insert Data gagal";		
		}
		
		
		$data = array(
			'status' => $status, 
			'pesan'  => $pesan,
			'draw'   => $draw
		);
		echo json_encode($data);
	}
	public function updatebarcodegarment($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$this->db->where('header_size_id', $post['size_id']);
			$this->db->where('inline_header_id', $post['id']);
			$header_detail = $this->db->get('header_line');

			$this->db->where('barcode_id', $post['newbarcode']);
			$barcode_detail = $this->db->get('barcode_garment');
			if ($barcode_detail->num_rows()==0) {
				$this->db->where('header_size_id', $post['size_id']);
				$this->db->where('article', $header_detail->row_array()['article']);
				$this->db->where('style', $header_detail->row_array()['style']);
				$this->db->update('barcode_garment', array('barcode_id'=>$post['new_barcode'],'update_at'=>date('Y-m-d'),'update_by'=>$this->session->userdata('nik')));
			}else{
				$status = 500;
				$pesan  = "Barcode ".$post['newbarcode']." Sudah terdaftar di Style ".$barcode_detail->row_array()['style']." Article".$barcode_detail->row_array()['article']." dan Size ".$barcode_detail->row_array()['size']." Info ke ICT";
			}
			$data = array(
				'barcode' => $post['newbarcode'], 
				'status' => $status, 
				'pesan'  => $pesan
			);
			echo json_encode($data);
		}
	}
	public function showmodalrevisibarcode($get=NULL)
    {
		$get = $this->input->get();
		
    	if ($get['barcode_garment']!=0) {
    		$data = array(
				'barcode_garment' => $get['barcode_garment'],
				'flag_barcode'    => $get['flag_barcode'],
				'header_id'       => $get['header_id'],
				'size_id'         => $get['size_id']
    		);
    		$this->load->view('folding/revisi_barcode_garment',$data);
		}
		else if($get['barcode_garment']!=null){
			$data = array(
				'barcode_garment' => $get['barcode_garment'],
				'flag_barcode'    => $get['flag_barcode'],
				'header_id'       => $get['header_id'],
				'size_id'         => $get['size_id']
    		);
    		$this->load->view('folding/revisi_barcode_garment',$data);
		}
		else{
    		echo "1";
    	}
        
    }
    public function updatebarcode($post=NULL)
    {
    	$post = $this->input->post();
    	
    	if ($post['flag_barcode']!='0') {
    		$this->db->where('size', $post['size_id']);
    		$this->db->where('inline_header_id', $post['header_id']);
			$header_detail = $this->db->get('header_line')->row_array();

			// $this->db->where('style', $header_detail['style']);
			$this->db->where('poreference', $header_detail['poreference']);
			$this->db->where('size', $header_detail['size']);
			$this->db->where('article', $header_detail['article']);
    		$this->db->where('barcode_id', $post['newbarcode']);
    		$cekbarcode = $this->db->get('barcode_garment');
    		if ($cekbarcode->num_rows()==0) {
    			// $this->db->where('style', $header_detail['style']);
				$this->db->where('poreference', $header_detail['poreference']);
				$this->db->where('size', $header_detail['size']);
				$this->db->where('article', $header_detail['article']);
    			$this->db->update('barcode_garment', array('barcode_id'=>trim($post['newbarcode']),'update_at'=>date('Y-m-d H:i:s'),'update_by'=>$this->session->userdata('nik')));
    			$status = 200;
    			$pesan = "update barcode berhasil";

    		}else{
    			$status = 500;
				$pesan  = "Barcode ".$post['newbarcode']." Sudah terdaftar di Style ".$cekbarcode->row_array()['style']." PO ".$cekbarcode->row_array()['poreference']." Article ".$cekbarcode->row_array()['article']." dan Size ".$cekbarcode->row_array()['size']." Info ke ICT";
    		}
    		$data = array(
				'barcode' => $post['newbarcode'], 
				'status' => $status, 
				'pesan'  => $pesan
			);
			echo json_encode($data);
    	}else{
    		$data = array(
				'barcode' => $post['newbarcode'], 
				'status' => 200, 
				'pesan'  => "update barcode berhasil"
		);
		echo json_encode($data);
    	}
    	
	}
	
	
	public function cekout_wip_folding($post=NULL)
	{
		$post= $this->input->post('barcode_id');
		if ($post!='') {
			
			$this->db->where("barcode_package",$post);
			$this->db->select("inline_header_id, poreference, size, remark");
			$this->db->distinct();
			$query       = $this->db->get("folding");
			$num_folding = $query->num_rows();
			
			
			if($num_folding>1){				
				$folding     = $query->result();
				if($folding['0']->remark == 'PCS'){
					$this->db->trans_start();
					$this->db->query("DELETE from folding where barcode_package = '$post'");

					foreach ($folding as $key => $value) {					
						$this->db->query("SELECT * from update_folding('$value->inline_header_id', '$value->poreference', '$value->size');");
					}
					
					$this->db->query("SELECT * from update_header_folding('$value->poreference', '$value->size');");
				}
			}else{			
				$folding     = $query->row();
				if($folding->remark == 'PCS'){
					$this->db->trans_start();
					$this->db->query("DELETE from folding where barcode_package = '$post'");
					$this->db->query("SELECT * from update_folding('$folding->inline_header_id', '$folding->poreference', '$folding->size');");			
					$this->db->query("SELECT * from update_header_folding('$folding->poreference', '$folding->size');");	
				}
			}

			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
				$status = 500;
				$pesan  = "Checkout Gagal";
			}else{
				$this->db->trans_complete();
				$status = 200;
				$pesan  = "OK";
			}
			$data = array(
				'status'     => $status, 
				'pesan'      => $pesan, 
			);
			echo json_encode($data);


			// $this->checkin_karton($post);
			// $this->db->where('barcode_id', $post);
			// $this->db->update('folding_package_barcode', array('status'=>'sewing in'));
			// if ($this->db->trans_status() == FALSE)
			// {
			// 	$this->db->trans_rollback();
			// 	$status = 500;
			// 	$pesan  = "Sewing in gagal";
			// }else{
			// 	$this->db->trans_complete();
			// 	$status = 200;
			// 	$pesan  = "OK";
			// }
			// $data = array(
			// 	'status'     => $status, 
			// 	'pesan'      => $pesan, 
			// );
			// echo json_encode($data);
		}
	}


}

/* End of file Folding.php */
/* Location: ./application/controllers/Folding.php */
