<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_line extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		chek_session();
		$this->load->model(array('Master_line_model','Cekpermision_model'));
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Master line');
            $this->template->set('desc_page','Master Line');
            $this->template->load('layout','master/line/list_line_view');
        }

	}
	public function list_line()
	{
		$columns = array(
                            0 =>'mesin_id',
                            1 =>'line_name'
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');
        $factory = $this->session->userdata('factory');


        $totalData = $this->Master_line_model->allposts_count_list($factory);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->Master_line_model->allposts_list($limit,$start,$order,$dir,$factory);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->Master_line_model->posts_search_list($limit,$start,$search,$order,$dir,$factory);

            $totalFiltered = $this->Master_line_model->posts_search_count_list($search,$factory);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $key=>$master)
            {
                $this->db->where('factory_id', $master->factory_id);
                $factory = $this->db->get('master_factory');
                foreach ($factory->result() as $key => $f) {
                   $factory_name = $f->factory_name;
                }

                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                /*$nestedData['line_name'] = '<input name="nama_input['.$key.']" class="form-control proses_'.$master->master_line_id.'" value="'.$master->line_name.'" onkeyup="this.value = this.value.toUpperCase()"  disabled />';*/
                $nestedData['line_name'] = $master->line_name;
                $nestedData['mesin_id']  = $master->mesin_id;
                $nestedData['factory']   = $factory_name;
                if ($master->active=='t') {
                    $nestedData['action'] = "<center><a onclick='return edit($master->master_line_id)' href='javascript:void(0)' class='btn btn-primary'><i class='fa fa-pencil-square-o'></i></a><a onclick='return hapus($master->master_line_id)' href='javascript:void(0)' class='btn btn-danger'><i class='fa fa-eye-slash'></i></a></center>";
                }else{
                    $nestedData['action'] = "<center><a onclick='return active($master->master_line_id)' href='javascript:void(0)' class='btn btn-danger'><i class='fa fa-eye'></i> Non Active</a></center>";
                }
                

                /*$nestedData['action'] = "<center><a onclick='return edit_proses($master->master_line_id,1)' href='javascript:void(0)' class='edit_".$master->master_line_id." btn btn-primary'><i class='fa fa-pencil-square-o'></i> Edit</a><a onclick='return edit_proses($master->master_line_id,2)' style='display: none' href='javascript:void(0)' class='edit_".$master->master_line_id."  btn btn-success'><i class='fa fa-save'></i> Update</a></center>";*/
                $data[] = $nestedData;

            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function savesubmit($line_name=0)
	{
		$line_name=$this->input->post('line_name');
        $factory = $this->session->userdata('factory');
		$i =0;
		if (!empty($line_name)) {
           foreach ($line_name as $key => $line) {
            $data[$i]['line_name']      = strtoupper($line);
            $data[$i]['factory_id']      = $factory;
            $i++;
            }
            $this->db->insert_batch('master_line', $data);
            echo "sukses";
        }else{
            echo "gagal";
        }
	}
    public function editlinesubmit($post=NULL)
    {
        $post = $this->input->post();
        
        if ($post!=NULL) {
            $this->db->where('active', 't');
            $this->db->where('factory_id', $this->session->userdata('factory'));
            $this->db->where('line_name', strtoupper($post['line_name']));
            $ceknamaline = $this->db->get('master_line');

            $this->db->where('active', 't');
            $this->db->where('factory_id', $this->session->userdata('factory'));
            $this->db->where('mesin_id', $post['mesin_id']);
            $cekMesinId = $this->db->get('master_line');
          
            if ($ceknamaline->num_rows()==0||$cekMesinId->num_rows()==0) {
                $line_before = $this->db->get_where('master_line',array('master_line_id'=>$post['master_line_id']))->row_array()['line_name'];
                $this->db->trans_start();
                if ($ceknamaline->num_rows()==0) {
                    $this->db->where('master_line_id',$post['master_line_id']);
                    $this->db->update('master_line', array('line_name'=>strtoupper($post['line_name'])));

                    $this->db->where('line_id', $post['master_line_id']);
                    $this->db->where('delete_at is NULL');
                    $cekhistory = $this->db->get('history_line');

                    if ($cekhistory->num_rows()>0) {
                         $this->db->where('history_id', $cekhistory->row_array()['history_id']);
                        $this->db->update('history_line', array('delete_at'=>date('Y-m-d H:i:s'),'delete_by'=>$this->session->userdata('nik')));
                    }
                    $detail_history = array(
                        'history_id' => $this->uuid->v4(), 
                        'line_id' => $post['master_line_id'], 
                        'line_name_from' => $line_before, 
                        'line_name_to' => strtoupper($post['line_name']), 
                        'create_by' => $this->session->userdata('nik'), 
                    );
                    $this->db->insert('history_line', $detail_history);
                }
                if ($cekMesinId->num_rows()==0) {
                    $this->db->where('master_line_id',$post['master_line_id']);
                    $this->db->update('master_line', array('mesin_id'=>$post['mesin_id']));
                }
                if ($this->db->trans_status() == FALSE)
                {
                    $this->db->trans_rollback();
                    $status =500;
                    $pesan  ="Update data gagal";
                }else{
                    $this->db->trans_complete();
                    $status =200;
                    $pesan ="update berhasil";
                }
                
            }else{
                $status =500;
                $pesan ="nama line / Nomor mesin sudah ada";
            }
          
            $data = array('status' => $status,'pesan'=>$pesan );
            echo json_encode($data);
        }
    }
    public function showedit($get=NULL)
    {
       $get = $this->input->get();
       if ($get!='') {
            $this->db->where('master_line_id', $get['id']);
            $data['record'] = $this->db->get('master_line')->row_array();
            $this->load->view('master/line/show_edit_line', $data);
       }
    }
    public function delete_line($get=NULL)
    {
        $get = $this->input->get();
        if ($get!=NULL) {
            $this->db->trans_start();
            $line_name = $this->db->get_where('master_line', array('master_line_id'=>$get['id']))->row_array()['line_name'];
            $this->db->where('master_line_id', $get['id']);
            $this->db->update('master_line', array('active'=>'f','line_name'=>$line_name."-TUTUP"));

            $this->db->where('line_id', $get['id']);
            $this->db->where('delete_at is NULL');
            $cekhistory = $this->db->get('history_line');

            if ($cekhistory->num_rows()>0) {
                 $this->db->where('history_id', $cekhistory->row_array()['history_id']);
                $this->db->update('history_line', array('delete_at'=>date('Y-m-d H:i:s'),'delete_by'=>$this->session->userdata('nik')));
            }
            $detail_history = array(
                'history_id' => $this->uuid->v4(), 
                'line_id' => $get['id'], 
                'line_name_from' => $line_name, 
                'line_name_to' => $line_name."-TUTUP", 
                'create_by' => $this->session->userdata('nik'), 
            );
            $this->db->insert('history_line', $detail_history);

            if ($this->db->trans_status() == FALSE)
            {
                $this->db->trans_rollback();
                $status =500;
                $pesan  ="Update data gagal";
            }else{
                $this->db->trans_complete();
                $status =200;
                $pesan ="Inactive berhasil";
            }
            $data = array('status' => $status,'pesan'=>$pesan );
            echo json_encode($data);
        }
    }
    public function active_line($get=NULL)
    {
        $get = $this->input->get();
        if ($get!=NULL) {
            $line = $this->db->get_where('master_line', array('master_line_id'=>$get['id']));
            $cek = $this->db->get_where('master_line', array($line->row_array()['mesin_id'],'factory_id'=>$this->session->userdata('factory')));
            if ($cek->num_rows()==0) {
                $this->db->trans_start();
                

                $line_name = str_replace('-TUTUP', '', $line->row_array()['line_name']);

                $this->db->where('master_line_id', $get['id']);
                $this->db->update('master_line', array('active'=>'t','line_name'=>$line_name));

                $this->db->where('line_id', $get['id']);
                $this->db->where('delete_at is NULL');
                $cekhistory = $this->db->get('history_line');

                if ($cekhistory->num_rows()>0) {
                     $this->db->where('history_id', $cekhistory->row_array()['history_id']);
                    $this->db->update('history_line', array('delete_at'=>date('Y-m-d H:i:s'),'delete_by'=>$this->session->userdata('nik')));
                }
                $detail_history = array(
                    'history_id'     => $this->uuid->v4(), 
                    'line_id'        => $get['id'], 
                    'line_name_from' => $line->row_array()['line_name'], 
                    'line_name_to'   => $line_name, 
                    'create_by'      => $this->session->userdata('nik'), 
                );
                $this->db->insert('history_line', $detail_history);

                if ($this->db->trans_status() == FALSE)
                {
                    $this->db->trans_rollback();
                    $status =500;
                    $pesan  ="Update data gagal";
                }else{
                    $this->db->trans_complete();
                    $status =200;
                    $pesan ="Inactive berhasil";
                }
                
            }else{
                $status =500;
                $pesan  ="masih ada mesin id yang aktif";
            }
            $data = array('status' => $status,'pesan'=>$pesan );
            echo json_encode($data);
        }
    }

}

/* End of file master_line.php */
/* Location: ./application/controllers/master_line.php */