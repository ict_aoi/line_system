<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inna extends CI_Controller {

	function __construct()
    {
  		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		chek_session();
		$this->load->model('Cekpermision_model');
		$this->load->model('InnaModel');
		
    }

    public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Laporan');
            $this->template->set('desc_page','Laporan');
            $this->template->load('layout','inna/laporan_view');
        }

	}

	public function list_laporan()
    {
        $columns = array( 
							0  => 'date',
							1  => 'no_urut',
							2  => 'style',
							3  => 'poreference',
							4  => 'size',
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

  
		// $date         = $this->input->post('tgl');

  		// if ($date!='') {
  		// 	$date = date('Y-m-d',strtotime($date));
  		// }else{
  		// 	$date = date('Y-m-d');
		// }
		
		$term         = $this->input->post('term');

  		if ($term==null) {
  			$term = 't';
  		}else{
  			$term = $term;
		}

		$search_po    = $this->input->post('po');
		// $search_style = $this->input->post('style');

        $totalData = $this->InnaModel->allposts_daily_count($term);
            
        $totalFiltered = $totalData; 
            
        if(empty($search_po))
        {            
            $dash = $this->InnaModel->allposts_daily($limit,$start,$order,$dir,$term);
        }
        else {
			
            $dash =  $this->InnaModel->posts_daily_search($limit,$start,$order,$dir,$search_po,$term);

			// var_dump($dash);
			// die();
            $totalFiltered = $this->InnaModel->posts_daily_search_count($search_po,$term);
		}
		
        $data = array();
		$nomor_urut = 0;
		if($term == 't'){
			$remark = 'TERINTEGRASI';
		}
		else{
			$remark = 'BELUM TERINTEGRASI';
		}
        if(!empty($dash))
        {
            foreach ($dash as $dash)
            {
				$nestedData['date']        = $dash->date;
				$nestedData['style']       = $dash->style;
				$nestedData['poreference'] = $dash->poreference;
				$nestedData['size']        = $dash->size;
				$nestedData['qty']         = $dash->sum;
				$nestedData['line']        = $dash->line_name;
				$nestedData['remark']      = $remark;

				// $temp = '"'.$dash->poreference.'","'.$dash->line_id.'","'.$dash->size.'","'.$dash->style.'","'.$dash->wip_sewing.'"';
				// // $nestedData['action']      = "hai";
				// $nestedData['action']      = "<center><a onclick='return detail($temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-th-list'></i> Detail</a> </center>";
                
				$data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }
	

	public function download($post=NULL)
    {
		$post = $this->input->post();
        $columns = array( 
			0  => 'date',
			1  => 'no_urut',
			2  => 'style',
			3  => 'poreference',
			4  => 'size',
        );

        $limit = NULL;
        $start = NULL;
        $order = NULL;
        $dir   = NULL;
        $draw  = $this->input->post('draw');

		$search_po      = $_GET['po'];
		$search_term    = $_GET['term'];

		if ($search_term==null) {
			$search_term = 't';
		}else{
			$search_term = $search_term;
	  }


        $totalData = $this->InnaModel->allposts_daily_count($search_term);
            
        $totalFiltered = $totalData; 
            
        if(empty($search_po))
        {            
            $dash = $this->InnaModel->allposts_daily($limit,$start,$order,$dir,$search_term);
        }
        else {
			
            $dash =  $this->InnaModel->posts_daily_search($limit,$start,$order,$dir,$search_po,$search_term);

            $totalFiltered = $this->InnaModel->posts_daily_search_count($search_po,$search_term);
		}
		
        $data = array();
		$nomor_urut = 0;
		$excel = "'\@'";
		if($search_term == 't'){
			$term = 'TERINTEGRASI';
		}
		else{
			$term = 'BELUM TERINTEGRASI';
		}
        if(!empty($dash))
        {
            foreach ($dash as $dash)
            {
				$nestedData['date']         = $dash->date;
				$nestedData['style']        = $dash->style;
				$nestedData['poreference']  = $dash->poreference;
				$nestedData['size']         = $dash->size;
				$nestedData['qty']          = $dash->sum;
				$nestedData['line']         = $dash->line_name;
				$nestedData['term']         = $term;
                
                $baris = '<tr>
                <td class="text-center">'.$nestedData['date'].'</td>
                <td class="text-center">'.$nestedData['line'].'</td>
                <td class="text-center">'.$nestedData['style'].'</td>
                <td style="mso-number-format:'.$excel.'" class="text-center">'.$nestedData['poreference'].'</td>
                <td class="text-center">'.$nestedData['size'].'</td>
                <td class="text-center">'.$nestedData['qty'].'</td>
                <td class="text-center">'.$nestedData['term'].'</td>
                </tr>';
                $data[] = $baris;
        	}
        }
          
        $json_data = array(
                    "data" => $data,
					);
		$this->load->view("inna/report_view",$json_data);
	}

}
