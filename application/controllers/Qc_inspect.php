<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_inspect extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		chek_session();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('Qc_inspect_model','Cekpermision_model'));
	}

	public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','QC Inspect');
            $this->template->set('desc_page','QC Inspect');
            $this->template->load('layout','qc/qc_inspect_chose');
        }

	}
	public function loadline()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('qc/list_line');
        }
	} 
	public function loadpobuyer()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $line_id['record'] = $_GET['id'];
            $this->load->view('qc/list_poreference',$line_id);
        }

	}
    public function loadsewerproses()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        $proses_id = $_GET['proses_id'];
        // $header_id = $_GET['header_id'];
        $line_id = $_GET['line_id'];
        $date = date('Y-m-d');

        $cek = $this->Qc_inspect_model->getSewerNik($proses_id,$line_id)->result_array();

        echo json_encode($cek);

    }

    public function viewsewerproses()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        $proses_id = $_GET['proses_id'];
        // $header_id = $_GET['header_id'];
        $line_id = $_GET['line_id'];
        $date = date('Y-m-d');
        
        $data['sewer'] = $this->Qc_inspect_model->getSewerNik($proses_id,$line_id);


        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('qc/list_sewer',$data);
        }

    }
	public function savesubmit($style=NULL,$header_id=NULL,$proses=NULL,$qc_nik=NULL,$factory=NULL,$last=NULL)
	{
        $post      = $this->input->post();

        
        $last      = $post['lastproses'];
        $grade     = $post['grade'];
        $style     = $post['style'];
        $header_id = $post['header_id'];
        $sewer     = $post['sewer'];
        $proses    = $post['proses'];
        $qc_nik    = $this->session->userdata('nik');
        $factory   = $this->session->userdata('factory');
        $date      = date('Y-m-d');
        $maxheader      = $this->Qc_inspect_model->maxheader($post['line_id']);

        if ($maxheader=='') {
            $data_round = array(
                'line_id'    => $post['line_id'],
                'factory_id' => $factory,
                'round'      => '1'
            );
            
             $this->db->insert('round_status', $data_round);
        }

        $round = $this->Qc_inspect_model->maxheader($post['line_id'])[0]->round;

        $_cek = $this->Qc_inspect_model->getDetail($proses,$round,$sewer,$date,$post['line_id']);


        if (!empty($style)||!empty($header_id)||!empty($proses)) {
            if ($last=='t') {
               if ($_cek==0) {
                    $i =0;
                    $json = str_replace(array("\t","\n"), "", $_POST["items"]);
                    $data = json_decode($json);
                    foreach ($data as $key => $item) {
                        $detail[$i]['inline_detail_id'] =$this->uuid->v4();
                        $detail[$i]['inline_header_id'] =$header_id;
                        $detail[$i]['round']            =$round;
                        $detail[$i]['master_grade_id']  =$grade;
                        $detail[$i]['factory_id']       =$factory;
                        $detail[$i]['master_proses_id'] =$proses;
                        $detail[$i]['sewer_nik']        =$sewer;
                        $detail[$i]['qc_nik']           =$qc_nik;
                        $detail[$i]['style']            =$style;
                        $detail[$i]['line_id']          =$post['line_id'];
                        $detail[$i]['defect_id']        =$item->defect_id;
                        $detail[$i]['size']             =$item->size;
                        $i++;
                    }
                    $rounds = $round+1;
                    if ($round=='5') {
                        $date   = date("Y-m-d", time() + 86400);

                        $data = array(
                            'line_id' => $post['line_id'],
                            'round' => 1,
                            'factory_id' => $factory,
                            'create_date' => $date
                            );
                    }else{
                        $data = array(
                            'line_id'    => $post['line_id'],
                            'factory_id' => $factory,
                            'round'      => $rounds
                        );
                    }

                    $this->db->insert_batch('inline_detail', $detail);
                    $this->db->insert('round_status', $data);
                    
                    // insert cap inline
                    $this->insert_capinline($date,$post['line_id'],$round,$proses,$sewer);

                    // end insert

                    $notif = "sukses";
                }else{
                    $notif = "gagal";
                }
            }else{
                if ($_cek==0) {
                    $i =0;
                    $json = str_replace(array("\t","\n"), "", $_POST["items"]);
                    $data = json_decode($json);
                    foreach ($data as $key => $item) {
                        $detail[$i]['inline_detail_id'] =$this->uuid->v4();
                        $detail[$i]['inline_header_id'] =$header_id;
                        $detail[$i]['round']            =$round;
                        $detail[$i]['master_grade_id']  =$grade;
                        $detail[$i]['factory_id']       =$factory;
                        $detail[$i]['master_proses_id'] =$proses;
                        $detail[$i]['sewer_nik']        =$sewer;
                        $detail[$i]['qc_nik']           =$qc_nik;
                        $detail[$i]['style']            =$style;
                        $detail[$i]['line_id']          =$post['line_id'];
                        $detail[$i]['defect_id']        =$item->defect_id;
                        $detail[$i]['size']             =$item->size;
                        $i++;
                    }
                   
                    $this->db->insert_batch('inline_detail', $detail);
                    $this->insert_capinline($date,$post['line_id'],$round,$proses,$sewer);
                    $notif = "sukses";
                }else{
                    $notif = "gagal";
                }
            }
        }else{
            $notif = "gagal";
        }
        $roundss = $this->Qc_inspect_model->maxheader($post['line_id'])[0]->round;
        $data = array(
            'notif' => $notif,
            'round' => $roundss,
            'nik'   => $sewer
        );
        echo json_encode($data);

	}
    private function insert_capinline($date,$line_id,$round,$proses,$sewer)
    {
        $this->db->where(array('create_date'=> $date,'line_id' => $line_id,'color'=>'red','round'=>$round,'master_proses_id'=>$proses,'sewer_nik'=>$sewer));
        $cap_inline = $this->db->get('listinspection_view');
        $cap_id = array();
        $_line  = array();
        $detail = array();
        $date   = date('Y-m-d');
        foreach ($cap_inline->result() as $key => $cap) {
           $_cap_id = $this->uuid->v4();
           $line_id = $cap->line_id;

           $defect_jenis= str_replace('No Defect', '',$cap->defect_jenis);
           
           $detail = array(
            'cap_inline_id' => $_cap_id, 
            'line_id'       => $line_id, 
            'factory_id'    => $cap->factory_id, 
            'line_name'     => $cap->line_name, 
            'round'         => $cap->round, 
            'sewer_nik'     => $cap->sewer_nik, 
            'sewer_name'    => $cap->sewer_name, 
            'qc_name'       => $cap->qc_name, 
            'proses_name'   => $cap->proses_name, 
            'defect_id'     => $cap->defect_id,
            'defect_jenis'  => $defect_jenis
        ); 
            $cap_id [] = $_cap_id;
            $_line []  = $line_id;                      
        }
        if (count($detail)!=0) {
           $this->db->insert('cap_inline', $detail);
        }

        if (count($_line)!=0) {
            $this->db->where_not_in('line_id', $_line);
            $this->db->where_not_in('cap_inline_id', $cap_id);
        }
        $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
        $this->db->where('status', 'cap verify');
        $this->db->update('cap_inline', array('status'=>'close'));
    }
	public function listline()
    {
        $columns = array(
                            0 =>'master_line_id',
                            1 =>'line_name',
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');


        $totalData = $this->Qc_inspect_model->allposts_count_line();

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->Qc_inspect_model->allposts_line($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->Qc_inspect_model->posts_search_line($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Qc_inspect_model->posts_search_count_line($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
                $_temp = '"'.$master->master_line_id.'","'.trim($master->line_name).'"';
                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['line_name'] = ucwords($master->line_name);
                $nestedData['action'] = "<center><a onclick='return select($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
    }
    public function listpoajax()
    {
    	$columns = array(
                            0 =>'poreference',
                            1 =>'style',
                        );
        $limit   = $this->input->post('length');
        $start   = $this->input->post('start');
        $order   = $columns[$this->input->post('order')[0]['column']];
        $dir     = $this->input->post('order')[0]['dir'];
        $draw    = $this->input->post('draw');
        $factory = $this->session->userdata('factory');
        $line_id =$this->input->post('line_id');

        $totalData = $this->Qc_inspect_model->allposts_count_list($factory,$line_id);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->Qc_inspect_model->allposts_list($limit,$start,$order,$dir,$factory,$line_id);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->Qc_inspect_model->posts_search_list($limit,$start,$search,$order,$dir,$factory,$line_id);

            $totalFiltered = $this->Qc_inspect_model->posts_search_count_list($search,$factory,$line_id);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
            	$_temp = '"'.trim($master->poreference).'","'.$master->style.'"';
                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['poreference'] = ucwords($master->poreference);
                $nestedData['style'] = ucwords($master->style);
                $nestedData['action'] = "<center><a onclick='return pilih($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
    }
    public function searchsubmit($line=0,$po=NULL)
    {

        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $line        = $this->input->post('line_id',TRUE);
            $poreference = $this->input->post('poreference',TRUE);
            $style       = $this->input->post('style',TRUE);
            $cek         = $this->Qc_inspect_model->cekheader($line,$poreference,$style);


            if ($cek->num_rows()>0) {
                $data['record'] = $cek->row_array();
                $factory        = $this->session->userdata('factory');
                $detail_id      = $cek->result()[0]->inline_header_id;
                $date           = date('Y-m-d');
                $maxheader      = $this->Qc_inspect_model->maxheader($line);
                // var_dump ($maxheader);
                // die();

                if ($maxheader=='') {
                    $data['round'] = 1;
                }else{
                    $date = date('Y-m-d');
                    if ($maxheader[0]->create_date<$date) {
                        $detailround = array(
                            'line_id' => $line,
                            'round' => 1,
                            'factory_id' => $factory
                        );
                        $this->db->insert('round_status', $detailround);
                        $data['round'] = 1;
                    }else{
                        $data['round'] = $maxheader[0]->round;
                    }

                }
                $this->load->model('master_workflow_model');
                $data['style'] = $this->master_workflow_model->getworkflow($style);
                $data['size'] = $this->Qc_inspect_model->poSizeQc($line,$poreference,$factory);

                $this->load->view('qc/qc_inspect',$data);

            }else{
                echo "gagal";
            }
        }


    }
    public function searchnik($nik=NULL)
    {
        $nik  = $_GET['id'];
        $_cek = $this->db->get_where('master_sewer',array('nik'=>$nik));
    	if ($_cek->num_rows()>0) {
    		echo $_cek->result()[0]->name;
    	}else{
    		echo "gagal";
    	}
    }
    public function defect_view()
    {
        $this->load->view('qc/list_defect');
    }
    public function list_defect()
    {
        $columns = array(
                            0 =>'defect_id',
                            1 =>'defect_jenis',
                            2 =>'grade_id',
                        );
        $limit   = $this->input->post('length');
        $start   = $this->input->post('start');
        $order   = $columns[$this->input->post('order')[0]['column']];
        $dir     = $this->input->post('order')[0]['dir'];
        $draw    = $this->input->post('draw');

        $totalData = $this->Qc_inspect_model->allposts_count_defect();

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->Qc_inspect_model->allposts_defect($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->Qc_inspect_model->posts_search_defect($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Qc_inspect_model->posts_search_count_defect($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
                $_temp = '"'.$master->defect_id.'","'.trim($master->defect_jenis).'","'.$master->grade_id.'"';
                $nestedData['no']= $master->defect_code;
                $nestedData['defect'] = ucwords($master->defect_jenis);
                $nestedData['grade'] = $master->grade_id;
                $nestedData['action'] = "<center><a onclick='return pilih($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
    }
    public function listinspection()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            // $sum = 
            $date      = date('Y-m-d');
            // $detail_id      = $this->input->get('id');
            
            // $data['detail'] = $this->Qc_inspect_model->headerdetail($detail_id,$date);
            

            $this->load->view('qc/list_inspection');
        }
    }
    public function proses_view()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('qc/list_proses');
        }
    }
    public function listproses()
    {
       $columns = array(
                            0 =>'master_proses_id',
                            1 =>'proses_name',
                            2 =>'jumlah',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');
        $style = $this->input->post('style');
        $line_id = $this->input->post('line_id');
        $round = $this->input->post('round');
        

        $totalData = $this->Qc_inspect_model->allposts_count_proses($style);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->Qc_inspect_model->allposts_proses($limit,$start,$order,$dir,$style);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->Qc_inspect_model->posts_search_proses($limit,$start,$search,$order,$dir,$style);

            $totalFiltered = $this->Qc_inspect_model->posts_search_count_proses($search,$style);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
                $_temp = '"'.$master->lastproses.'","'.$master->master_proses_id.'","'.trim($master->proses_name).'"';
                /*$detail_inline = $this->Qc_inspect_model->detailInlineProses($line_id,$round,$master->style,$master->master_proses_id);*/

                /*$jumlah = ($detail_inline->row_array()['exists']=='t'?5:0);*/

                /*$jumlah=0;
                foreach ($detail_inline->result() as $detail) {
                    $jumlah = $detail->jumlah;
                }*/
                /*$jumlah = ($detail_inline==0?0:5);*/

                $nestedData['no']         = (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['prosesname'] = ucwords($master->proses_name);
                $nestedData['lastproses'] = $master->lastproses;
                /*$nestedData['jumlah']     = $jumlah;*/
                $nestedData['action']     = "<center><a onclick='return pilih($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
                $data[]                   = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
    }

    public function gradeview()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('qc/list_grade');
        }
    }

    public function inspection_view()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('qc/list_inspection');
        }
    }

    public function inspection_summary($line)
    {
        $date       = date('Y-m-d');
        $line_id    = $line;
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $total = $this->Qc_inspect_model->sum_defect($date,$line_id);

            if($total == null){
                $hitung = 0;                
            
                $data = array (
                    'detail' => 0,
                    "sum"    => 0,
                    'persen' => 0,
                );

            }else{
                $hitung = ($total->persen / $total->total_random) * 100;            
                $hasil = number_format((float)$hitung, 2, '.', '');
            
                $data = array (
                    'detail' => $this->Qc_inspect_model->detail_defect($date,$line_id),
                    'sum'    => $this->Qc_inspect_model->sum_defect($date,$line_id),
                    'persen' => $hasil,
                );
            }

            $this->load->view('qc/list_summary',$data);
        }
    }


    public function listinspection_ajax()
    {
        $columns = array(
                            0 =>'',
                            1 =>'round',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');
        $date  = date('Y-m-d');
        $line_id    = $this->input->post('line_id');


        $totalData = $this->Qc_inspect_model->allposts_count_inspect($date,$line_id);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->Qc_inspect_model->allposts_inspect($limit,$start,$order,$dir,$date,$line_id);
        }
        else { 
            $search = $this->input->post('search')['value'];

            $master =  $this->Qc_inspect_model->posts_search_inspect($limit,$start,$search,$order,$dir,$date,$line_id);

            $totalFiltered = $this->Qc_inspect_model->posts_search_count_inspect($search,$date,$line_id);
        }
        $data = array();
        $nomor_urut = 0;

        if(!empty($master))
        {
            foreach ($master as $master)
            {
                $nestedData['no']         = (($draw-1) * 10) + (++$nomor_urut);
                
                $nestedData['prosesname'] = strtolower($master->proses_name."<br>").ucwords($master->sewer_name."(".$master->sewer_nik.")");
                $nestedData['round1']      = "<input type='text' style='background-color:".$master->grade_round1.";border: 0px;text-align: right;width: inherit' value='".$master->round1."' disabled>";
                $nestedData['round2']      = "<input type='text' style='background-color:".$master->grade_round2.";border: 0px;text-align: right;width: inherit' value='".$master->round2."' disabled>";
                $nestedData['round3']      = "<input type='text' style='background-color:".$master->grade_round3.";border: 0px;text-align: right;width: inherit' value='".$master->round3."' disabled>";
                $nestedData['round4']      = "<input type='text' style='background-color:".$master->grade_round4.";border: 0px;text-align: right;width: inherit' value='".$master->round4."' disabled>";
                $nestedData['round5']      = "<input type='text' style='background-color:".$master->grade_round5.";border: 0px;text-align: right;width: inherit' value='".$master->round5."' disabled>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
    }
    public function getColor($header_id=NULL,$proses_id=0)
    {

        $header_id = $this->input->get('idHeader');
        $nik = $this->input->get('nik');
        $proses = $this->input->get('proses_id');
       
        $getWarna = $this->Qc_inspect_model->getColor($nik,$proses);
        $json_data = array(
                            'color' => '#f5f5f5',
                            'grade' => 0
                        );
        if ($getWarna!=NULL) {
            $color = $getWarna[0]->color;
            $grade = $getWarna[0]->master_grade_id;

            $json_data = array(
                                'color' => $getWarna[0]->color,
                                'grade' => $getWarna[0]->master_grade_id
                            );
        }
        
        echo json_encode($json_data);
    }
    public function list_detail_inspection($get=NULL)
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $get = $this->input->get();
            if ($get!=NULL) {
                $this->db->where(array('round'=>$get['round'],'sewer_nik'=>$get['nik'],'master_proses_id'=>$get['proses_id']));
                $this->db->join('master_defect', 'master_defect.defect_id = listinspection_view.defect_id', 'left');
                $data['record']= $this->db->get('listinspection_view');

                $this->load->view('qc/list_detail_inspection', $data);
            }
        }
	}
	
	public function pengajuan_ad()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','QC Inspect');
            $this->template->set('desc_page','QC Inspect');
            $this->template->load('layout','qc/pengajuan_ad');
        }

	}

	public function listpengajuan()
    {
        $columns = array( 
			0 =>'created_at', 
			1 =>'tanggal_permintaan',
			3 =>'style',
			4 =>'poreference',
			6 =>'status',
		);

        $limit   = $this->input->post('length');
        $start   = $this->input->post('start');
        $order   = $columns[$this->input->post('order')[0]['column']];
        $dir     = $this->input->post('order')[0]['dir'];
        $draw    = $this->input->post('draw');
        $factory = $this->session->userdata('factory');

  
        $totalData = $this->Qc_inspect_model->allposts_count_ad($factory);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Qc_inspect_model->allposts_ad($limit,$start,$order,$dir,$factory);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master = $this->Qc_inspect_model->posts_search_ad($limit,$start,$search,$order,$dir,$factory);

            $totalFiltered = $this->Qc_inspect_model->posts_search_count_ad($search,$factory);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
				$_temp       = '"'.$master->pengajuan_id.'"';
                // $nestedData['no']        = (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['tanggal_permintaan'] = ucwords($master->tanggal_permintaan);
                $nestedData['jam_permintaan']     = ucwords(date('H:m',strtotime($master->jam_permintaan)));
                $nestedData['style']              = ucwords($master->style);
                $nestedData['poreference']        = ucwords($master->poreference);
				$nestedData['line_name']          = ucwords($master->line_name);
				
				// if($master->status='belum siap'){
				// 	$nestedData['status']             = "<center><button class='btn btn-danger' readonly>
				// 	".ucwords($master->status)."</button></center>";
				// }
				// else{
				// 	$nestedData['status']             = "<center><button class='btn btn-success' readonly>
				// 	".ucwords($master->status)."</button></center>";
				// }
				
				$nestedData['status']             = ucwords($master->status);
                // $nestedData['action']            = "<center><a onclick='return select($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
                $data[]                   = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }

	public function loadpengajuan()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
			
			$factory = $this->session->userdata('factory');
			$ml      = $this->db->query("SELECT * FROM master_line where active='t' and factory_id = '$factory' order by no_urut");
			$data['ml']    = $ml;
            $this->load->view('qc/form_pengajuan',$data);
        }
	}

	public function simpan_pengajuan()
	{
		
		$nik               = $this->session->userdata('nik');
		$factory           = $this->session->userdata('factory');
		$tanggal_pengajuan = $this->input->post('tanggal_pengajuan');
		$waktu_pengajuan   =  date("H:i:s");
		$style_pengajuan   = $this->input->post('style_pengajuan');
		$po_pengajuan      = $this->input->post('po_pengajuan');
		$line_pengajuan    = $this->input->post('line_pengajuan');
        // var_dump($waktu_pengajuan);
        // die();
		
		$query = $this->db->query("SELECT * FROM pengajuan_ad where tanggal_permintaan = '$tanggal_pengajuan' and jam_permintaan = '$waktu_pengajuan' and style = '$style_pengajuan' and poreference = '$po_pengajuan' and line_id = '$line_pengajuan' and created_by = '$nik' and factory_id = '$factory'");

		if($query->num_rows()<1){
			$detail = array(
				'pengajuan_id'       => $this->uuid->v4(),
				'tanggal_permintaan' => $tanggal_pengajuan,
				'jam_permintaan'     => $waktu_pengajuan,
				'style'              => strtoupper($style_pengajuan),
				'poreference'        => strtoupper($po_pengajuan),
				'line_id'            => $line_pengajuan,
				'status'             => 'belum siap',
				'created_at'         => date('Y-m-d H:i:s'),
				'created_by'         => $nik,
				'factory_id'         => $factory,
			); 
			
			$this->db->insert('pengajuan_ad', $detail);
			$status = '200';	
		}
		else{
			$status = '400';
		}

		$data = array(
			'status' => $status, 
		);

		echo json_encode($data);
	}
}

/* End of file qc_inspect.php */
/* Location: ./application/controllers/qc_inspect.php */
