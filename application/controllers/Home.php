<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		chek_session();
		$this->load->model('Cekpermision_model');
	}

	public function index()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Home');
            $this->template->set('desc_page','Home');
            $this->template->load('layout','home/home_view');
        }
	}

	
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */