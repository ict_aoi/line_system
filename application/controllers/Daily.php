<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daily extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		chek_session();
		$this->load->model(array('DailyModel','Cekpermision_model'));
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{

		$cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Daily Input');
            $this->template->set('desc_page','Daily Input');
            $this->template->load('layout','master/Daily/index_daily');
            // $this->template->load('layout','master/daily/indexChooseDaily');
		}
	}
	public function showLine()
	{
		$draw='';
		$no = 1;
		$this->db->order_by('no_urut');
		$this->db->where('factory_id', $this->session->userdata('factory'));
		$this->db->where('active', 't');
		$line = $this->db->get('master_line');
		foreach ($line->result() as $key => $l) {
			$_temp = '"'.$l->master_line_id.'","'.$l->line_name.'"';
			$draw .="<tr onclick='return selectline($_temp)'>".
						"<td>".$l->line_name."</td>".
			       '</tr>';
		}
		$data = array('draw' => $draw );
		$this->load->view('master/Daily/showline', $data);
	}
	public function searchStyle($post=NULL)
	{
		$post = $this->input->post();

		$data['styles'] = NULL;

		if ($post!=NULL) {
			$getStyle = $this->DailyModel->getStyle($post['line_id'])->result_array();

			$data['styles'] = $getStyle;

		}

		echo json_encode($data['styles']);
	}

	public function dailyStyleByDate()
	{
		// $post = $this->input->post();
		$factory = $this->session->userdata('factory');

		$style_id = $this->input->post('style_id');
		$line_id = $this->input->post('line_id');

		$data['result_style'] = [];
		$data['detail_style'] = [];

		$numStyleHeader = $this->DailyModel->getStyleHeader($style_id, $line_id, $factory);
		$numStyleByDate = $this->DailyModel->getStyleByDate($style_id, $line_id, $factory);

		if ($numStyleHeader->num_rows() > 0) {
			$data['result_style'] = $numStyleHeader->row_array();
		}

		if ($numStyleByDate->num_rows() > 0) {
			$data['detail_style'] = $numStyleByDate->row_array();
		}

		echo json_encode($data);
	}

	public function submitDaily($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$factory       = $this->session->userdata('factory');
			$style         = $post['style'];
			$line_id       = $post['line_id'];
			$order_qty     = $post['order_qty'];
			$change_over   = $post['change_over'];
			$gsd_smv       = $post['gsd_smv'];
			$present_sewer = $post['present_sewer'];
			$working_hours = $post['working_hours'];
			$target_output = $post['targetoutput'];
			$date_now      = date('Y-m-d');

			$getDailyView = $this->DailyModel->getDailyView($line_id)->row_array();
			
			$this->db->trans_start(); 
			if (date('Y-m-d',strtotime($getDailyView['create_date']))!=$date_now) {
				
				if ($getDailyView['style']==$style&&$getDailyView['change_over']==$change_over) {

					$daily_header_id = $this->uuid->v4();
					
					$this->insertDetail($post,$getDailyView['daily_header_id']);

					$this->db->where('daily_header_id', $getDailyView['daily_header_id']);
					$co = $this->db->get('daily_detail');

					$this->db->where('daily_header_id', $getDailyView['daily_header_id']);
					$this->db->update('daily_header', array('cumulative_day'=>$co->num_rows()-1));
				}else{
					$daily_header_id = $this->uuid->v4();
					
					$this->insertheader($post,$daily_header_id);
					$this->insertDetail($post,$daily_header_id);
				} 
			}else{
				$getStyleByDate= $this->DailyModel->getHeaderView($style,$line_id,$factory,$change_over);
					
				
				if ($getStyleByDate->num_rows()>0) {
					
					$this->updatedaily($post,$getStyleByDate->row_array()['daily_header_id'],$getStyleByDate->row_array()['daily_detail_id']);

				}else{

					$daily_header_id = $this->uuid->v4();
					
					$this->insertheader($post,$daily_header_id);
					$this->insertDetail($post,$daily_header_id);
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
		        $status = 500;
			}else{
				$this->db->trans_complete();
				$status = 200;
			}

			$data['status'] = $status;


			echo json_encode($data);

		}
	}
	/*public function submitDaily()
	{
		$post          = $this->input->post();
		$factory       = $this->session->userdata('factory');
		
		$style         = $post['style'];
		$line_id       = $post['line_id'];
		$order_qty     = $post['order_qty'];
		$change_over   = $post['change_over'];
		$gsd_smv       = $post['gsd_smv'];
		$present_sewer = $post['present_sewer'];
		$working_hours = $post['working_hours'];
		$target_output = $post['targetoutput'];


		$date_now = date('Y-m-d H:i:s');


		$this->db->trans_start();

			$numStyleHeader = $this->DailyModel->numStyleHeader($style,$change_over,$line_id, $factory);
			$numStyleByDate = $this->DailyModel->getStyleByDate($style, $line_id, $factory)->num_rows();

			
			$date = date('Y-m-d');
			$days_ago = date('Y-m-d', strtotime('-4 days', strtotime($date)));

			if ($numStyleHeader->num_rows()==0) {

				$headerBefore = $this->DailyModel->headerBefore($line_id,$days_ago,$style);
				foreach ($headerBefore->result() as $key => $b) {
					$daily_header_id[] = $b->daily_header_id;
				}
				if (isset($daily_header_id)) {
					$this->db->where_in('daily_header_id', $daily_header_id);
					$this->db->update('daily_header', array('job_status'=>FALSE));
				}
				
				$post_header = array(
							'daily_header_id' => $this->uuid->v4(),
							'style'           => $style,
							'line_id'         => $line_id,
							'factory_id'      => $factory,
							'start_date'      => $date_now,
							'order_qty'       => $order_qty,
							'change_over'     => $change_over,
							'gsd_smv'         => $gsd_smv
							);

				$insert_header = $this->db->insert('daily_header', $post_header);

				$getDailyHeaderId = $this->DailyModel->getDailyHeaderId($style, $line_id, $factory);
				

				$this->db->insert('daily_detail',
									[
										'daily_detail_id' => $this->uuid->v4(),
										'daily_header_id' => $getDailyHeaderId,
										'present_sewer' => $present_sewer,
										'working_hours' => $working_hours,
										'target_output' => $target_output,
										'create_date' => $date_now

									]
								);

			}else{
				//$this->db->where('daily_header_id',$numStyleHeader->row_array()['daily_header_id']);
				$this->db->update('daily_header',
									['gsd_smv' => $gsd_smv],
									['daily_header_id' => $numStyleHeader->row_array()['daily_header_id']]
								);

				// $getDailyHeaderId = $this->DailyModel->getDailyHeaderId($style, $line_id, $factory);
				
				if ($numStyleByDate > 0) {
					$this->db->update('daily_detail',
											['present_sewer'   => $present_sewer, 'working_hours' => $working_hours,'target_output' => $target_output],
											['daily_header_id' => $numStyleHeader->row_array()['daily_header_id'], 'date(create_date)' => date('Y-m-d')]
									);
				}else{
					$this->db->insert('daily_detail',
									[
										'daily_detail_id' => $this->uuid->v4(),
										'daily_header_id' => $numStyleHeader->row_array()['daily_header_id'],
										'present_sewer'   => $present_sewer,
										'working_hours'   => $working_hours,
										'target_output' => $target_output,
										'create_date'     => $date_now

									]
								);
					$this->db->where('daily_header_id', $numStyleHeader->row_array()['daily_header_id']);
				$co = $this->db->get('daily_detail');
				
				$this->db->where('daily_header_id', $numStyleHeader->row_array()['daily_header_id']);
				$this->db->update('daily_header', array('cumulative_day'=>$co->num_rows()-1));
				}
			}

		

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
	        $status = 500;
		}else{
			$this->db->trans_complete();
			$status = 200;
		}

		$data['status'] = $status;


		echo json_encode($data);
	}*/
	private function inactive_job()
	{
		$this->db->where_in('daily_header_id', $daily_header_id);
		$this->db->update('daily_header', array('job_status'=>FALSE));
	}
	private function insertheader($post,$daily_header_id)
	{
		
		$detail_header = array(
			'daily_header_id' => $daily_header_id,
			'style'           => $post['style'],
			'factory_id'      => $this->session->userdata('factory'),
			'line_id'         => $post['line_id'],
			'start_date'      => date('Y-m-d H:i:s'),
			'order_qty'       => $post['order_qty'],
			'change_over'     => $post['change_over'],
			'gsd_smv'         => $post['gsd_smv']
		);
		$this->db->insert('daily_header', $detail_header);

		$this->db->where('daily_header_id<>', $daily_header_id);
		$this->db->where('style', $post['style']);
		$this->db->where('line_id', $post['line_id']);
		$this->db->update('daily_header', array('job_status'=>'f'));
	}
	private function insertDetail($post,$daily_header_id)
	{
		$detail = array(
			'daily_detail_id' => $this->uuid->v4(),
			'daily_header_id' => $daily_header_id,
			'present_sewer'   => $post['present_sewer'],
			'working_hours'   => $post['working_hours'],
			'present_folding' => $post['present_folding'],
			'create_by'       => $this->session->userdata('nik'),
			'target_output'   => $post['target_output'],
			'create_date'     => date('Y-m-d H:i:s') 
		);
		$this->db->insert('daily_detail', $detail);
	}
	private function updatedaily($post,$daily_header_id,$detail_id)
	{
		
		$update_detail = array(
			'present_sewer'   => $post['present_sewer'],
			'present_folding' => $post['present_folding'],
			'update_date'     => date('Y-m-d H:i:s'),
			'update_by'       => $this->session->userdata('nik'),
			'target_output'   => $post['target_output'],
			'working_hours'   => $post['working_hours']
		);
		$this->db->where('daily_detail_id', $detail_id);
		$this->db->update('daily_detail', $update_detail);

		$update_header = array(
			'gsd_smv' => $post['gsd_smv'],
		);
		$this->db->where('daily_header_id', $daily_header_id);
		$this->db->update('daily_header', $update_header);
	}

	public function loaddata()
	{
		$line_id = $this->input->post('id');
		$draw             = '';
		$draw             .= '<table id="table-daily" class="table table-striped table-bordered table-hover table-full-width bg-success" cellspacing="0" width="100%">
			                <thead>
			                    <tr  bgcolor="#E6E6FA">
			                        <th width="50px" class="text-center">NO.</th>
			                        <th class="text-center">STYLE</th>
			                        <th class="text-center">GSD SMV</th>
			                        <th width="100px" class="text-center">TOTAL QTY</th>
			                        <th width="100px" class="text-center">Komitmen Line</th>
			                        <th width="80px" class="text-center">CHANGE OVER</th>
			                        <th width="80px" class="text-center">CUM DAY</th>
			                        <th width="80px" class="text-center">PRESENT SEWER</th>
			                        <th width="80px" class="text-center">PRESENT IPF</th>
			                        <th width="80px" class="text-center">WORKING HOURS</th>
			                        <th width="120px" class="text-center">ACTION</th>
			                    </tr>
			                </thead>
			                <tbody>';
		if ($line_id!='') {
			$no               = 1;
			$getStyles = $this->DailyModel->getStyle($line_id);
			if ($getStyles->num_rows()==0) {
				$draw.='<tr><td colspan="8">Tidak Ada Data</td></tr>';
			}else{
				foreach ($getStyles->result() as $key => $get) {

					$query = "select * from update_targetqty('$get->style',$get->line_id,$get->factory_id)";
					$this->db->query($query);

					$details         = $this->DailyModel->detailStyleDate($get->line_id,$get->style);
					$change_over     ='';
					$gsd     ='';
					$working         ='';
					$target_output   ='';
					$cum_day         ='';
					$present_sewer   ='';
					$present_folding ='';
					$id_temp         ='';
					foreach ($details->result() as $key => $detail) {
						$change_over     = $detail->change_over;
						$target_output   = $detail->target_output;
						$working         = $detail->working_hours;
						$present_sewer   = $detail->present_sewer;
						$present_folding = $detail->present_folding;
						$cum_day         = $detail->cumulative_day;
						$gsd         = $detail->gsd_smv;
						$id_temp         = '"'.$detail->daily_header_id.'"';
					}
					
					$gsd_smv= ($gsd==''?$get->gsd_smv:$gsd);
					$previous_co = $this->DailyModel->detailStyle($get->line_id,$get->style)->row_array()['change_over'];
					$temp = '"'.$get->style.'","'.$get->balance.'","'.$gsd_smv.'","'.$previous_co.'","'.$line_id.'","'.$working.'","'.$present_sewer.'","'.$present_folding.'","'.$target_output.'"';
					$draw.="<tr>".
								'<td>'.$no++.'</td>'.
								'<td>'.$get->style.'</td>'.
								'<td>'.$gsd_smv.'</td>'.
								'<td>'.$get->balance.'</td>'.
								'<td>'.$target_output.'</td>'.
								'<td>'.$change_over.'</td>'.
								'<td>'.$cum_day.'</td>'.
								'<td>'.$present_sewer.'</td>'.
								'<td>'.$present_folding.'</td>'.
								'<td>'.$working.'</td>';
							$draw .= "<td> <div class='btn-group'>";
								if ($details->num_rows()>0) {
									$draw .="<a onclick='return tambah($temp)'". "class='btn btn-primary' href='javascript:void(0)'><span data-icon='&#x6c'> Edit</i></a>";
								}
								if ($details->num_rows()==0) {
									$draw .="<a onclick='return tambah($temp)'". "class='btn btn-success' href='javascript:void(0)'><span data-icon='&#x50'> Tambah</i></a>".
								"</div>";
								}

								
							'<tr>';					
				}
			}
			
			$draw.='</tbody></table>';
		}else{
			$draw             .= '<tbody><tr><td colspan="11">Tidak Ada Data</td></tr>';
		}
		echo json_encode($draw);

	}
	public function submitDaily_v2($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {

			$date         = date('Y-m-d');
			$factory      = $this->session->userdata('factory');
			$getDailyView = $this->DailyModel->getDailyView($post['line_id'])->row_array();
			$this->db->trans_start(); 
			if (date('Y-m-d',strtotime($getDailyView['create_date']))!=$date) {
				if ($getDailyView['style']==$post['style']&&$getDailyView['change_over']==$post['change_over']) {

					$daily_header_id = $this->uuid->v4();
					
					$this->insertDetail($post,$getDailyView['daily_header_id']);

					$cum_day = $this->DailyModel->getCumDay($getDailyView['daily_header_id']);

					$this->db->where('daily_header_id', $getDailyView['daily_header_id']);
					$this->db->update('daily_header', array('cumulative_day'=>$cum_day));
				}else{
					$daily_header_id = $this->uuid->v4();
					
					$this->insertheader($post,$daily_header_id);
					$this->insertDetail($post,$daily_header_id);
				} 
			}else{
				$getStyleByDate= $this->DailyModel->getHeaderView($post['style'],$post['line_id'],$factory,$post['change_over']);
					
				
				if ($getStyleByDate->num_rows()>0) {
					
					$this->updatedaily($post,$getStyleByDate->row_array()['daily_header_id'],$getStyleByDate->row_array()['daily_detail_id']);

				}else{

					$daily_header_id = $this->uuid->v4();
					
					$this->insertheader($post,$daily_header_id);
					$this->insertDetail($post,$daily_header_id);
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
		        $status = 500;
			}else{
				$this->db->trans_complete();
				$status = 200;
			}

			$data['status'] = $status;


			echo json_encode($data);

		}else{
			redirect('auth','refresh');
		}
	}

	public function submit_jam_kerja($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$line    = $post['line_id'];
			$jam     = $post['jam'];
			// $shift = $post['shift'];
			$date    = date('Y-m-d');
			$user    = $this->session->userdata('nik');
			$factory = $this->session->userdata('factory');
			


			// $query_cek = $this->db->query("SELECT * from daily_jam_kerja where 
			// line_id = '$line' and jam_mulai = '$jam' and created_by = '$user' and factory_id = '$factory' and date(create_date) = '$date'")->row_array();

			// if($query_cek == null){

			// $query_cek = $this->db->query("SELECT * FROM daily_jam_kerja WHERE date(create_date) = '$date' and line_id = '$line'");

			// if($query_cek->num_rows() > 0){
			// 	$status = 100;
			// } else{

				
				$this->db->trans_start(); 
				$data = array(
					'daily_jam_id' => $this->uuid->v4(),
					'line_id'      => $line,
					// 'shift'		=> $shift,
					'jam_mulai'    => $jam,
					'created_by'   => $this->session->userdata('nik'),
					'factory_id'   => $this->session->userdata('factory'),
					'create_date'  => date('Y-m-d H:i:s')
				);
				$this->db->insert('daily_jam_kerja', $data);

				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
				}else{
					$this->db->trans_complete();
					$status = 200;
				}
			// }
			// }
			// else{
			// 	$status = 100;
			// }
			
			

			$data['status'] = $status;


			echo json_encode($data);

		}else{
			redirect('auth','refresh');
		}
	}

	public function load_inputan()
	{
		$data['line'] = $_GET['id'];
		$this->load->view('master/Daily/index_inputan',$data);
	}

    public function list_inputan()
    {
        $draw    = $this->input->post('draw');
        $factory = $this->session->userdata('factory');
        $line    = $this->input->post('line');
		
        $totalData     = $this->DailyModel->getDailyToday_count($line);
        $totalFiltered = $totalData;
        $master        = $this->DailyModel->getDailyToday_all($line);

        $data       = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
				$temp        = '"'.$master->daily_detail_id.'","'.$master->working_hours.'"';
							
				$nestedData['style']  = $master->style;
				$nestedData['hours']  = $master->working_hours;
				$nestedData['action'] = "<center><a onclick='return edit_daily_inputan($temp)' href='javascript:void(0)' class='btn btn-success'>Edit</a></center>";
				$data[]    		      = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
	
	public function load_edit_inputan()
	{
		$data['id']  = $_GET['id'];
		$data['jam'] = $_GET['jam'];
		$this->load->view('master/Daily/edit_inputan',$data);
	}

	public function update_inputan()
	{
		$detail_id = $this->input->post('detail_id');
		$jam       = $this->input->post('jam');

		
		$this->db->where('daily_detail_id', $detail_id);
		$this->db->update('daily_detail', array('working_hours'=>$jam));
		
		$data['status'] = 200;


		echo json_encode($data);
	}

}

/* End of file Daily.php */
/* Location: ./application/controllers/Daily.php */
