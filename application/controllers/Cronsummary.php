<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronsummary extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		ini_set('max_execution_time', 3000);
		$this->load->model(array('Cron_model','ReportModel'));
        date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		// php index.php Cronsummary/1
	}
	private function jam_kerja($waktu=0)
	{
		$jam['time'] = [
			0 => [
				'name' => '07:00 - 08:00',
				'awal' => '07:00:00',
				'akhir' => '08:00:00'
			], 
			1 => [
				'name' => '08:00 - 09:00',
				'awal' => '08:00:00',
				'akhir' => '09:00:00'
			]];

		/*$jam[0] = array(
			'name' => '07:00 - 08:00',
			'awal' => '07:00:00',
			'akhir' => '08:00:00'
		);
		$jam[1] = array(
			'name' => '08:00 - 09:00',
			'awal' => '08:00:00',
			'akhir' => '09:00:00'
		);
		$jam[2] = array(
			'name' => '09:00 - 10:00',
			'awal' => '09:00:00',
			'akhir' => '10:00:00'
		);
		$jam[3] = array(
			'name' => '10:00 - 11:00',
			'awal' => '10:00:00',
			'akhir' => '11:00:00'
		);
		$jam[4] = array(
			'name' => '11:00-12:00',
			'awal' => '11:00:00',
			'akhir' => '12:00:00'
		);
		$jam[5] = array(
			'name' => '12:45-13:45',
			'awal' => '12:00:00',
			'akhir' => '13:45:00'
		);
		$jam[6] = array(
			'name' => '13:45-14:45',
			'awal' => '13:45:00',
			'akhir' => '14:45:00'
		);
		$jam[7] = array(
			'name' => '14:45-15:45',
			'awal' => '14:45:00',
			'akhir' => '15:45:00'
		);
		$jam[8] = array(
			'name'  => '16:00-17:00',
			'awal'  => '15:45:00',
			'akhir' => '17:00:00'
		);
		$jam[9] = array(
			'name'  => '17:00-18:00',
			'awal'  => '17:00:00',
			'akhir' => '18:00:00'
		);
		$jam[10] = array(
			'name' => '18:30-19:15',
			'awal' => '18:00:00',
			'akhir' => '19:30:00'
		);
		$jam[11] = array(
			'name' => '19:30-20:30',
			'awal' => '19:30:00',
			'akhir' => '20:30:00'
		);
		$jam[12] = array(
			'name' => '19:30-20:30',
			'awal' => '19:30:00',
			'akhir' => '20:30:00'
		);
		$jam[13] = array(
			'name' => '20:30-21:30',
			'awal' => '19:30:00',
			'akhir' => '21:30:00'
		);*/

		return $jam;
	}
	public function download_summary_po()
	{
		$jam_kerja    = $this->jam_kerja();
		foreach ($jam_kerja as $key => $jam) {
			$awal  = date("Y-m-d").' '.$jam['awal'];
			$akhir = date("Y-m-d").' '.$jam['akhir'];
			
		}
	}
	public function tarik_perjam()
	{	
		// if (!$this->input->is_cli_request()) {
		// 	redirect('auth','refresh');
		// }
		// else{
			$factory = '1';
			$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'queue' and factory_id = '$factory'");

			if(($query->num_rows())!=null)
			{
				$this->tarik_hari();
			}
			else{
				$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'ongoing' and factory_id = '$factory'");

				if(($query->num_rows())!=null)
				{
	
				}
				else
				{
					$data_schedule = array(
						'id'        => $this->uuid->v4(),
						'job'       => 'SYNC_PRODUCTION',
						'status'    => 'ongoing',
						'created_at' => date('Y-m-d H:i:s'),
						'start_job' => date('Y-m-d H:i:s'),
						'keterangan' => 'tarik_perjam',
						'factory_id' => $factory
					);
					
					$id_schedule = $data_schedule['id'];
					$this->db->insert('schedulers', $data_schedule);

					$this->db->trans_start();
					//$jam_kerja    = $this->db->get('jam_kerja')->result_array();
					$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					
					$previousValue = null;

					
					foreach ($jam_kerja as $key => $jam) {
					
						$awal  = date("Y-m-d").' '.$jam['awal'];
						$akhir = date("Y-m-d").' '.$jam['akhir'];
						
						$time  = time();
						$now   = date("Y-m-d H:i:s",$time);
						if($jam['factory'] == $factory && $time > strtotime($awal) && $time < strtotime($akhir)){
	
							if (date('i',$time)=='00') {
								if($previousValue) {
									$this->db->where('jam_id', $previousValue);
									$previous = $this->db->get('jam_kerja')->row_array();
								
									$previousAwal  = date("Y-m-d").' '.$previous['awal'];
									$previousAkhir = date("Y-m-d").' '.$previous['akhir'];
	
									$this->sumSewing($factory,$previousAwal,$previousAkhir);
									//edit article
									$this->sumQcOutput($factory,$previousAwal,$previousAkhir); //done
									$this->sumFolding($factory,$previousAwal,$previousAkhir); //done
									$this->sumDefectEndline($factory,$previousAwal,$previousAkhir); //done
									$this->sumDefectInline($factory,$previousAwal,$previousAkhir); //done
	
								}
	
							}
							$this->sumSewing($factory,$awal,$akhir);
							$this->sumQcOutput($factory,$awal,$akhir);
							$this->sumFolding($factory,$awal,$akhir);
							$this->sumDefectEndline($factory,$awal,$akhir);
							$this->sumDefectInline($factory,$awal,$akhir);
	
						}
					
						$previousValue = $jam['jam_id'];
					}
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
						$status = 500;
					}else{
						$this->db->trans_complete();
						$status = 200;
					}
					
					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'  => 'done',
						'end_job' => date('Y-m-d H:i:s'),
					));
				}
			}
		 // }

		
	}

	public function tarik_perjam_2()
	{	
		// if (!$this->input->is_cli_request()) {
		// 	redirect('auth','refresh');
		// }
		// else{
			$factory = '2';
			$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'queue' and factory_id = '$factory'");

			if(($query->num_rows())!=null)
			{
				$this->tarik_hari_2();
			}
			else{
				$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'ongoing' and factory_id = '$factory'");

				if(($query->num_rows())!=null)
				{
	
				}
				else
				{
					$data_schedule = array(
						'id'        => $this->uuid->v4(),
						'job'       => 'SYNC_PRODUCTION',
						'status'    => 'ongoing',
						'created_at' => date('Y-m-d H:i:s'),
						'start_job' => date('Y-m-d H:i:s'),
						'keterangan' => 'tarik_perjam',
						'factory_id' => $factory
					);
					
					$id_schedule = $data_schedule['id'];
					$this->db->insert('schedulers', $data_schedule);

					$this->db->trans_start();
					//$jam_kerja    = $this->db->get('jam_kerja')->result_array();
					$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					
					$previousValue = null;

					
					foreach ($jam_kerja as $key => $jam) {
					
						$awal  = date("Y-m-d").' '.$jam['awal'];
						$akhir = date("Y-m-d").' '.$jam['akhir'];
						
						$time  = time();
						$now   = date("Y-m-d H:i:s",$time);
						if($jam['factory'] == $factory && $time > strtotime($awal) && $time < strtotime($akhir)){
	
							if (date('i',$time)=='00') {
								if($previousValue) {
									$this->db->where('jam_id', $previousValue);
									$previous = $this->db->get('jam_kerja')->row_array();
								
									$previousAwal  = date("Y-m-d").' '.$previous['awal'];
									$previousAkhir = date("Y-m-d").' '.$previous['akhir'];
	
									$this->sumSewing($factory,$previousAwal,$previousAkhir);
									//edit article
									$this->sumQcOutput($factory,$previousAwal,$previousAkhir); //done
									$this->sumFolding($factory,$previousAwal,$previousAkhir); //done
									$this->sumDefectEndline($factory,$previousAwal,$previousAkhir); //done
									$this->sumDefectInline($factory,$previousAwal,$previousAkhir); //done
	
								}
	
							}
							$this->sumSewing($factory,$awal,$akhir);
							$this->sumQcOutput($factory,$awal,$akhir);
							$this->sumFolding($factory,$awal,$akhir);
							$this->sumDefectEndline($factory,$awal,$akhir);
							$this->sumDefectInline($factory,$awal,$akhir);
	
						}
					
						$previousValue = $jam['jam_id'];
					}
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
						$status = 500;
					}else{
						$this->db->trans_complete();
						$status = 200;
					}
					
					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'  => 'done',
						'end_job' => date('Y-m-d H:i:s'),
					));
				}
			}
		// }

		
	}

	private function sumSewing($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->SumSewing($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$name= 'sewing';
			$cekLineSummary = $this->Cron_model->cekLineSummary($awal,$akhir,$sum->line_id,$sum->factory_id,$name,$sum->style);
			if ($cekLineSummary->num_rows()>0) {
				$this->db->where('summary_id', $cekLineSummary->row_array()['summary_id']);
				$this->db->where('factory_id', $factory);
				$this->db->delete('line_summary_po');
			}
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'sewing', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->count, 
				'style'         => $sum->style, 
				'date'          => $sum->date, 
				'factory_id'    => $sum->factory_id, 
				'line_name'     => $sum->line_name, 
				'line_id'       => $sum->line_id 
			);
			$this->db->insert('line_summary_po', $detail);
		}
		
	}
	private function sumQcOutput($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->sumQcEndlinePerPO($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
						

			$name= 'outputqc';
			$cekLineSummary = $this->Cron_model->cekLineSummary($awal,$akhir,$sum->line_id,$sum->factory_id,$name,$sum->style,$sum->poreference,$sum->size);
			if ($cekLineSummary->num_rows()>0) {
				$this->db->where('summary_id', $cekLineSummary->row_array()['summary_id']);
				$this->db->where('factory_id', $factory);
				$this->db->delete('line_summary_po');
			}
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'outputqc', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->daily_output, 
				'factory_id'    => $sum->factory_id, 
				'style'         => $sum->style, 
				'size'          => $sum->size, 
				'poreference'   => $sum->poreference, 
				'date'          => $sum->create_date, 
				'line_name'     => $sum->line_name, 
				'line_id'       => $sum->line_id,
				'article'		=> $sum->article
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	private function sumFolding($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->sumFolding($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$name= 'folding';
			$cekLineSummary = $this->Cron_model->cekLineSummary($awal,$akhir,$sum->line_id,$sum->factory_id,$name,$sum->style,$sum->poreference,$sum->size);
			if ($cekLineSummary->num_rows()>0) {
				$this->db->where('summary_id', $cekLineSummary->row_array()['summary_id']);
				$this->db->where('factory_id', $factory);
				$this->db->delete('line_summary_po');
			}
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'folding', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->count, 
				'factory_id'    => $sum->factory_id, 
				'style'         => $sum->style, 
				'size'          => $sum->size, 
				'poreference'   => $sum->poreference, 
				'date'          => $sum->date, 
				'line_name'     => $sum->line_name, 
				'line_id'       => $sum->line_id,
				'article'		=> $sum->article
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	private function sumDefectEndline($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->sumDefectEndline($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$name= 'defect_endline';
			$cekLineSummary = $this->Cron_model->cekLineSummary($awal,$akhir,$sum->line_id,$sum->factory_id,$name);
			if ($cekLineSummary->num_rows()>0) {
				$this->db->where('summary_id', $cekLineSummary->row_array()['summary_id']);
				$this->db->where('factory_id', $factory);
				$this->db->delete('line_summary_po');
			}
			$detail = array(
				'summary_id'    => $this->uuid->v4(),
				'activity_name' => 'defect_endline',
				'jam_awal'      => $awal,
				'jam_akhir'     => $akhir,
				'output'        => $sum->defect_endline,
				'factory_id'    => $sum->factory_id,
				'style'         => $sum->style,
				'line_name'     => $sum->line_name,
				'date'          => $sum->date,
				'line_id'       => $sum->line_id,
				'article'       => $sum->article
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	private function sumDefectInline($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->sumDefectInline($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$name= 'defect_inline';
			$cekLineSummary = $this->Cron_model->cekLineSummary($awal,$akhir,$sum->master_line_id,$sum->factory_id,$name);
			if ($cekLineSummary->num_rows()>0) {
				$this->db->where('summary_id', $cekLineSummary->row_array()['summary_id']);
				$this->db->where('factory_id', $factory);
				$this->db->delete('line_summary_po');
			}
			$detail = array(
				'summary_id'    => $this->uuid->v4(),
				'activity_name' => 'defect_inline',
				'jam_awal'      => $awal,
				'jam_akhir'     => $akhir,
				'output'        => $sum->defect_inline,
				'factory_id'    => $sum->factory_id,
				'style'         => $sum->style,
				'date'          => $sum->date,
				'line_name'     => $sum->line_name,
				'line_id'       => $sum->master_line_id
				// 'article'       => $sum->article
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	public function tarik_hari()
	{
		// if (!$this->input->is_cli_request()) {
		// 	redirect('auth','refresh');
		// }
		// else
		// {
			$factory = '1';
			$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'queue' and factory_id = '$factory'");

			if(($query->num_rows())!=null)
			{
				$query2  = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'ongoing' and factory_id = '$factory'");

				if(($query2->num_rows())!=null){
					
				}else{
					$id_schedule = $query->row_array()['id'];

					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'    => 'ongoing',
						'start_job' => date('Y-m-d H:i:s'),
					));
					
					//$jam_kerja    = $this->db->get('jam_kerja')->result_array();
					$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					$this->db->trans_start();
					$date       = date('Y-m-d');
					//  $date = '2021-02-10';
					$date_awal  = $date;
					$date_akhir = $date;

					for ($i=$date_awal; $i <= $date_akhir; $i++) { 
						$this->db->where('date', $i);
						$this->db->where('factory_id', $factory);
						$this->db->delete('line_summary_po');
						foreach ($jam_kerja as $key => $jam) {
							$awal  = $i.' '.$jam['awal'];
							$akhir = $i.' '.$jam['akhir'];

							
							$this->sumSewingHari($factory,$awal,$akhir,$i);
							$this->sumQcOutputHari($factory,$awal,$akhir,$i);
							$this->sumFoldingHari($factory,$awal,$akhir,$i);
							$this->sumDefectEndlineHarian($factory,$awal,$akhir,$i);
							$this->sumDefectInlineHarian($factory,$awal,$akhir,$i);
								
						}
						if ($this->db->trans_status() === FALSE)
						{
							$this->db->trans_rollback();
							$status = 500;
						}else{
							$this->db->trans_complete();
							$status = 200;
						}
					}
					
					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'  => 'done',
						'end_job' => date('Y-m-d H:i:s'),
					));
				}
			}
			else{
				$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'ongoing' and factory_id = '$factory'");

				if(($query->num_rows())!=null)
				{
					$data_schedule = array(
						'id'         => $this->uuid->v4(),
						'job'        => 'SYNC_PRODUCTION',
						'status'     => 'queue',
						'created_at' => date('Y-m-d H:i:s'),
						'keterangan' => 'tarik_hari',
						'factory_id' => $factory
					);
					
					$id_schedule = $data_schedule['id'];
					$this->db->insert('schedulers', $data_schedule);
				}
				else
				{
					$data_schedule = array(
						'id'         => $this->uuid->v4(),
						'job'        => 'SYNC_PRODUCTION',
						'status'     => 'ongoing',
						'created_at' => date('Y-m-d H:i:s'),
						'start_job'  => date('Y-m-d H:i:s'),
						'keterangan' => 'tarik_hari',
						'factory_id' => $factory
					);
					
					$id_schedule = $data_schedule['id'];
					$this->db->insert('schedulers', $data_schedule);
	
					//$jam_kerja    = $this->db->get('jam_kerja')->result_array();
					$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					
					$this->db->trans_start();
					$date       = date('Y-m-d');
					// $date = '2021-04-30';
					$date_awal  = $date;
					$date_akhir = $date;
	
					for ($i=$date_awal; $i <= $date_akhir; $i++) { 
						$this->db->where('date', $i);
						$this->db->where('factory_id', $factory);
						$this->db->delete('line_summary_po');
						foreach ($jam_kerja as $key => $jam) {
							// $date = date('Y-m-d');
							
							$awal  = $i.' '.$jam['awal'];
							$akhir = $i.' '.$jam['akhir'];
	
							
							$this->sumSewingHari($factory,$awal,$akhir,$i);
							$this->sumQcOutputHari($factory,$awal,$akhir,$i);
							$this->sumFoldingHari($factory,$awal,$akhir,$i);
							$this->sumDefectEndlineHarian($factory,$awal,$akhir,$i);
							$this->sumDefectInlineHarian($factory,$awal,$akhir,$i);
								
						}
						if ($this->db->trans_status() === FALSE)
						{
							$this->db->trans_rollback();
							$status = 500;
						}else{
							$this->db->trans_complete();
							$status = 200;
						}
					}
					
					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'  => 'done',
						'end_job' => date('Y-m-d H:i:s'),
					));
				}
			}
		// }
	}

	public function tarik_hari_2()
	{
		// if (!$this->input->is_cli_request()) {
		// 	redirect('auth','refresh');
		// }
		// else
		// {
			$factory = '2';
			$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'queue' and factory_id = '$factory'");

			if(($query->num_rows())!=null)
			{
				$query2  = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'ongoing' and factory_id = '$factory'");

				if(($query2->num_rows())!=null){
					
				}else{
					$id_schedule = $query->row_array()['id'];

					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'    => 'ongoing',
						'start_job' => date('Y-m-d H:i:s'),
					));
					
					//$jam_kerja    = $this->db->get('jam_kerja')->result_array();
					$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					$this->db->trans_start();
					$date       = date('Y-m-d');
					// $date = '2022-06-06';
					$date_awal  = $date;
					$date_akhir = $date;

					for ($i=$date_awal; $i <= $date_akhir; $i++) { 
						$this->db->where('date', $i);
						$this->db->where('factory_id', $factory);
						$this->db->delete('line_summary_po');
						foreach ($jam_kerja as $key => $jam) {
							$awal  = $i.' '.$jam['awal'];
							$akhir = $i.' '.$jam['akhir'];

							
							$this->sumSewingHari($factory,$awal,$akhir,$i);
					
							$this->sumQcOutputHari($factory,$awal,$akhir,$i);
						
							$this->sumFoldingHari($factory,$awal,$akhir,$i);
							
							$this->sumDefectEndlineHarian($factory,$awal,$akhir,$i);
							
							$this->sumDefectInlineHarian($factory,$awal,$akhir,$i);
							
								
						}
						if ($this->db->trans_status() === FALSE)
						{
							$this->db->trans_rollback();
							$status = 500;
						}else{
							$this->db->trans_complete();
							$status = 200;
						}
					}
					
					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'  => 'done',
						'end_job' => date('Y-m-d H:i:s'),
					));
				}
			}
			else{
				$query = $this->db->query("SELECT * FROM schedulers where job = 'SYNC_PRODUCTION' and status = 'ongoing' and factory_id = '$factory'");

				if(($query->num_rows())!=null)
				{
					$data_schedule = array(
						'id'         => $this->uuid->v4(),
						'job'        => 'SYNC_PRODUCTION',
						'status'     => 'queue',
						'created_at' => date('Y-m-d H:i:s'),
						'keterangan' => 'tarik_hari',
						'factory_id' => $factory
					);
					
					$id_schedule = $data_schedule['id'];
					$this->db->insert('schedulers', $data_schedule);
				}
				else
				{
					$data_schedule = array(
						'id'         => $this->uuid->v4(),
						'job'        => 'SYNC_PRODUCTION',
						'status'     => 'ongoing',
						'created_at' => date('Y-m-d H:i:s'),
						'start_job'  => date('Y-m-d H:i:s'),
						'keterangan' => 'tarik_hari',
						'factory_id' => $factory
					);
					
					$id_schedule = $data_schedule['id'];
					$this->db->insert('schedulers', $data_schedule);
	
					
					$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					
					$this->db->trans_start();
					$date       = date('Y-m-d');
					// $date = '2021-04-16';
					$date_awal  = $date;
					$date_akhir = $date;
	
					for ($i=$date_awal; $i <= $date_akhir; $i++) { 
						$this->db->where('date', $i);
						$this->db->where('factory_id', $factory);
						$this->db->delete('line_summary_po');
						foreach ($jam_kerja as $key => $jam) {
							// $date = date('Y-m-d');
							
							$awal  = $i.' '.$jam['awal'];
							$akhir = $i.' '.$jam['akhir'];
	
							
							$this->sumSewingHari($factory,$awal,$akhir,$i);
							$this->sumQcOutputHari($factory,$awal,$akhir,$i);
							$this->sumFoldingHari($factory,$awal,$akhir,$i);
							$this->sumDefectEndlineHarian($factory,$awal,$akhir,$i);
							$this->sumDefectInlineHarian($factory,$awal,$akhir,$i);
								
						}
						if ($this->db->trans_status() === FALSE)
						{
							$this->db->trans_rollback();
							$status = 500;
						}else{
							$this->db->trans_complete();
							$status = 200;
						}
					}
					
					$this->db->where(array('id'=>$id_schedule));
					$this->db->update('schedulers',array(
						'status'  => 'done',
						'end_job' => date('Y-m-d H:i:s'),
					));
				}
			}
		// }
	}

	private function sumSewingHari($factory,$awal,$akhir,$date)
	{
		$sum = $this->Cron_model->SumSewing($factory,$awal,$akhir);

		foreach ($sum->result() as $key => $sum) {
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'sewing', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->count, 
				'style'         => $sum->style, 
				'date'          => $sum->date, 
				'factory_id'    => $sum->factory_id, 
				'line_name'     => $sum->line_name, 
				'line_id'       => $sum->line_id 
			);
			$this->db->insert('line_summary_po', $detail);
		}
		
	}
	public function sumQcOutputHari($factory,$awal,$akhir,$date)
	{
		$sum = $this->Cron_model->sumQcEndlinePerPO($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'outputqc', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->daily_output, 
				'factory_id'    => $sum->factory_id, 
				'style'         => $sum->style, 
				'size'          => $sum->size, 
				'poreference'   => $sum->poreference, 
				'date'          => $sum->create_date, 
				'line_name'     => $sum->line_name, 
				'line_id'       => $sum->line_id 
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	private function sumFoldingHari($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->sumFolding($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'folding', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->count, 
				'factory_id'    => $sum->factory_id, 
				'style'         => $sum->style, 
				'size'          => $sum->size, 
				'poreference'   => $sum->poreference, 
				'date'          => $sum->date, 
				'line_name'     => $sum->line_name, 
				'line_id'       => $sum->line_id 
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	private function sumDefectEndlineHarian($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->sumDefectEndline($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'defect_endline', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->defect_endline, 
				'factory_id'    => $sum->factory_id, 
				'style'         => $sum->style, 
				'line_name'     => $sum->line_name, 
				'date'          => $sum->date, 
				'line_id'       => $sum->line_id 
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	private function sumDefectInlineHarian($factory,$awal,$akhir)
	{
		$sum = $this->Cron_model->sumDefectInline($factory,$awal,$akhir);
		foreach ($sum->result() as $key => $sum) {
			$detail = array(
				'summary_id'    => $this->uuid->v4(), 
				'activity_name' => 'defect_inline', 
				'jam_awal'      => $awal, 
				'jam_akhir'     => $akhir,
				'output'        => $sum->defect_inline, 
				'factory_id'    => $sum->factory_id, 				 
				'style'         => $sum->style, 
				'date'          => $sum->date, 
				'line_name'     => $sum->line_name, 
				'line_id'       => $sum->master_line_id 
			);
			$this->db->insert('line_summary_po', $detail);
		}
	}
	public function tarik_ulang()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh'); 
		}else{
			$factory=1;
			$date = '2021-09-06';
		$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					
		
			$this->db->trans_start();
			// $date = date('Y-m-d');
			// $date = '2021-09-03';
			$date_awal = $date;
			$date_akhir = $date;

			for ($i=$date_awal; $i <= $date_akhir; $i++) { 
				$this->db->where('date', $i);
				$this->db->where('factory_id', $factory);
				$this->db->delete('line_summary_po');
				foreach ($jam_kerja as $key => $jam) {
					// $date = date('Y-m-d');
					
					$awal  = $i.' '.$jam['awal'];
					$akhir = $i.' '.$jam['akhir'];

					
			    	$this->sumSewingHari($factory,$awal,$akhir,$i);
			    	$this->sumQcOutputHari($factory,$awal,$akhir,$i);
			    	$this->sumFoldingHari($factory,$awal,$akhir,$i);
			    	$this->sumDefectEndlineHarian($factory,$awal,$akhir,$i);
			    	$this->sumDefectInlineHarian($factory,$awal,$akhir,$i);
		    			
				}
				// $this->db->where('sinkron', 'f');
				// $this->db->update('adjustment_delete_qc', array('sinkron'=>'t'));
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
			        $status = 500;
				}else{
					$this->db->trans_complete();
					$status = 200;
				}
			}
		}
		
	}

	public function tarik_ulang_hari($factory,$date)
	{
		// if (!$this->input->is_cli_request()) {
		// 	redirect('auth','refresh'); 
		// }else{
			// $factory=1;
			// $date = '2021-09-06';
		$jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory'")->result_array();
					
		
			$this->db->trans_start();
			// $date = date('Y-m-d');
			// $date = '2021-09-03';
			$date_awal = $date;
			$date_akhir = $date;

			for ($i=$date_awal; $i <= $date_akhir; $i++) { 
				$this->db->where('date', $i);
				$this->db->where('factory_id', $factory);
				$this->db->delete('line_summary_po');
				foreach ($jam_kerja as $key => $jam) {
					// $date = date('Y-m-d');
					
					$awal  = $i.' '.$jam['awal'];
					$akhir = $i.' '.$jam['akhir'];

					
			    	$this->sumSewingHari($factory,$awal,$akhir,$i);
			    	$this->sumQcOutputHari($factory,$awal,$akhir,$i);
			    	$this->sumFoldingHari($factory,$awal,$akhir,$i);
			    	$this->sumDefectEndlineHarian($factory,$awal,$akhir,$i);
			    	$this->sumDefectInlineHarian($factory,$awal,$akhir,$i);
		    			
				}
				// $this->db->where('sinkron', 'f');
				// $this->db->update('adjustment_delete_qc', array('sinkron'=>'t'));
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
			        $status = 500;
				}else{
					$this->db->trans_complete();
					$status = 200;
				}
			}
		// }
		
	}
	public function updatecounterbeda()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getCounterBeda = $this->Cron_model->getCounterBeda();
		
		foreach ($getCounterBeda->result() as $key => $get) {

			$this->db->where('id', $get->id);
			$this->db->update('folding_header', array('counter'=>$get->counter_alocation));
		}
	}
	public function updatecounterbedasets()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$getCounterBeda = $this->Cron_model->getCounterBedasets();
		
		foreach ($getCounterBeda->result() as $key => $get) {

			$this->db->where('id', $get->id);
			$this->db->update('folding_header', array('counter_sets'=>$get->counter_alocation));
		}
	}

	public function update_sto()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$query = $this->db->query("SELECT f.* from qc_output f
		LEFT JOIN ( SELECT distinct poreference from inline_header where date(create_date) >= '2019-10-01' and factory_id = '2') x on x.poreference = f.poreference
		LEFT JOIN qc_endline qe on qe.qc_endline_id = f.qc_endline_id
		where f.qc_endline_id is not null and
		date(qe.create_date) >= '2019-07-01' and
		date(qe.create_date) < '2019-10-26'
		and f.factory_id = '1'
		-- and f.qc_endline_id = '12082000-1457-4ecd-9c63-ed2188cf82be'
		and f.update_date is null
		")->result();


		$this->db->trans_start();
		foreach($query as $que){
			$qc_id = $que->qc_endline_id;

			$waktu = $this->db->query("SELECT MAX(create_date) as tgl from qc_endline_trans where 
			qc_endline_id = '$qc_id'")->row();

			if($waktu->tgl == null){

			}
			else{
				$this->db->query("UPDATE qc_endline set update_date = '$waktu->tgl' 
				WHERE qc_endline_id = '$qc_id'");
			}

		}

		if ($this->db->trans_status() == FALSE)
		{
			$status = 500;
			$pesan = "Simpan Data gagal";
			$this->db->trans_rollback();
		}else{
			$status = 200;
			$pesan = "Berhasil";
			$this->db->trans_complete();
		}

	}

	public function tarik_tls()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh'); 
		}

		$factory = 1;
		$this->export_tls_inline($factory);
		
		$factory = 2;
		$this->export_tls_inline($factory);		
	}

	public function export_tls_inline($factory_id){
		// Load plugin PHPExcel nya
		include_once APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$tgl     = date('Y-m-d');
		$dari    = date('Y-m-d',strtotime('-7 days'));
		$sampai  = $tgl;
		$factory = $factory_id;

		// var_dump($dari);
		// var_dump($sampai);
		// die();

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('ICT Team')
					 ->setLastModifiedBy('ICT Team')
					 ->setTitle("Data TLS Inline".$dari." sd ".$sampai."")
					 ->setSubject("TLS Inline")
					 ->setDescription("Laporan TLS Inline ".$dari." sd ".$sampai."")
					 ->setKeywords("Data TLS Inline");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
		  'font' => array('bold' => true), // Set font nya jadi bold
		  'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Set text jadi ditengah secara horizontal (center)
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER      // Set text jadi di tengah secara vertical (middle)
		  ),
		  'borders' => array(
			'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
			'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
			'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
		  )
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
		  'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
		  ),
		  'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		  )
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA TLS Inline ".$dari." sd ".$sampai.""); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:U1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3

		
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
		$excel->getActiveSheet()->mergeCells('A2:A3'); // Set kolom A3 dengan tulisan "NO"
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('B2', "DATE");
		$excel->getActiveSheet()->mergeCells('B2:B3'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('C2', "LINE");
		$excel->getActiveSheet()->mergeCells('C2:C3');  // Set kolom B3 dengan tulisan "NIS"
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('D2', "PO BUYER");
		$excel->getActiveSheet()->mergeCells('D2:D3');  // Set kolom C3 dengan tulisan "NAMA"
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('E2', "STYLE");
		$excel->getActiveSheet()->mergeCells('E2:E3');  // Set kolom D3 dengan tulisan "JENIS KELAMIN"
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('F2', "R1 RANDOM");
		$excel->getActiveSheet()->mergeCells('F2:F3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('G2', "TOTAL DEFECT");
		$excel->getActiveSheet()->mergeCells('G2:G3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('H2', "RFT");
		$excel->getActiveSheet()->mergeCells('H2:H3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('I2', "DEFECT CODE");
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom A

		$excel->getActiveSheet()->mergeCells('I2:FV2');
		$excel->setActiveSheetIndex(0)->setCellValue('I3','1');
		$excel->setActiveSheetIndex(0)->setCellValue('J3','2');
		$excel->setActiveSheetIndex(0)->setCellValue('K3','3');
		$excel->setActiveSheetIndex(0)->setCellValue('L3','4');
		$excel->setActiveSheetIndex(0)->setCellValue('M3','5');
		$excel->setActiveSheetIndex(0)->setCellValue('N3','6');
		$excel->setActiveSheetIndex(0)->setCellValue('O3','7');
		$excel->setActiveSheetIndex(0)->setCellValue('P3','8');
		$excel->setActiveSheetIndex(0)->setCellValue('Q3','9');
		$excel->setActiveSheetIndex(0)->setCellValue('R3','10');
		$excel->setActiveSheetIndex(0)->setCellValue('S3','11');
		$excel->setActiveSheetIndex(0)->setCellValue('T3','12');
		$excel->setActiveSheetIndex(0)->setCellValue('U3','13');
		$excel->setActiveSheetIndex(0)->setCellValue('V3','14');
		$excel->setActiveSheetIndex(0)->setCellValue('W3','15');
		$excel->setActiveSheetIndex(0)->setCellValue('X3','16');
		$excel->setActiveSheetIndex(0)->setCellValue('Y3','17');
		$excel->setActiveSheetIndex(0)->setCellValue('Z3','18');
		$excel->setActiveSheetIndex(0)->setCellValue('AA3','19');
		$excel->setActiveSheetIndex(0)->setCellValue('AB3','20');
		$excel->setActiveSheetIndex(0)->setCellValue('AC3','21');
		$excel->setActiveSheetIndex(0)->setCellValue('AD3','22');
		$excel->setActiveSheetIndex(0)->setCellValue('AE3','23');
		$excel->setActiveSheetIndex(0)->setCellValue('AF3','24');
		$excel->setActiveSheetIndex(0)->setCellValue('AG3','25');
		$excel->setActiveSheetIndex(0)->setCellValue('AH3','26');
		$excel->setActiveSheetIndex(0)->setCellValue('AI3','27');
		$excel->setActiveSheetIndex(0)->setCellValue('AJ3','28');
		$excel->setActiveSheetIndex(0)->setCellValue('AK3','29');
		$excel->setActiveSheetIndex(0)->setCellValue('AL3','30');
		$excel->setActiveSheetIndex(0)->setCellValue('AM3','1.1');
		$excel->setActiveSheetIndex(0)->setCellValue('AN3','1.2');
		$excel->setActiveSheetIndex(0)->setCellValue('AO3','1.3');
		$excel->setActiveSheetIndex(0)->setCellValue('AP3','1.4');
		$excel->setActiveSheetIndex(0)->setCellValue('AQ3','1.5');
		$excel->setActiveSheetIndex(0)->setCellValue('AR3','1.6');
		$excel->setActiveSheetIndex(0)->setCellValue('AS3','1.7');
		$excel->setActiveSheetIndex(0)->setCellValue('AT3','1.8');
		$excel->setActiveSheetIndex(0)->setCellValue('AU3','1.9');
		$excel->setActiveSheetIndex(0)->setCellValue('AV3','1.1');
		$excel->setActiveSheetIndex(0)->setCellValue('AW3','1.11');
		$excel->setActiveSheetIndex(0)->setCellValue('AX3','1.12');
		$excel->setActiveSheetIndex(0)->setCellValue('AY3','1.13');
		$excel->setActiveSheetIndex(0)->setCellValue('AZ3','1.14');
		$excel->setActiveSheetIndex(0)->setCellValue('BA3','1.15');
		$excel->setActiveSheetIndex(0)->setCellValue('BB3','1.16');
		$excel->setActiveSheetIndex(0)->setCellValue('BC3','1.17');
		$excel->setActiveSheetIndex(0)->setCellValue('BD3','1.18');
		$excel->setActiveSheetIndex(0)->setCellValue('BE3','1.19');
		$excel->setActiveSheetIndex(0)->setCellValue('BF3','1.2');
		$excel->setActiveSheetIndex(0)->setCellValue('BG3','1.21');
		$excel->setActiveSheetIndex(0)->setCellValue('BH3','1.22');
		$excel->setActiveSheetIndex(0)->setCellValue('BI3','1.23');
		$excel->setActiveSheetIndex(0)->setCellValue('BJ3','1.24');
		$excel->setActiveSheetIndex(0)->setCellValue('BK3','1.25');
		$excel->setActiveSheetIndex(0)->setCellValue('BL3','1.26');
		$excel->setActiveSheetIndex(0)->setCellValue('BM3','1.27');
		$excel->setActiveSheetIndex(0)->setCellValue('BN3','2.1');
		$excel->setActiveSheetIndex(0)->setCellValue('BO3','2.2');
		$excel->setActiveSheetIndex(0)->setCellValue('BP3','2.3');
		$excel->setActiveSheetIndex(0)->setCellValue('BQ3','2.4');
		$excel->setActiveSheetIndex(0)->setCellValue('BR3','2.5');
		$excel->setActiveSheetIndex(0)->setCellValue('BS3','2.6');
		$excel->setActiveSheetIndex(0)->setCellValue('BT3','2.7');
		$excel->setActiveSheetIndex(0)->setCellValue('BU3','2.8');
		$excel->setActiveSheetIndex(0)->setCellValue('BV3','2.9');
		$excel->setActiveSheetIndex(0)->setCellValue('BW3','2.1');
		$excel->setActiveSheetIndex(0)->setCellValue('BX3','2.11');
		$excel->setActiveSheetIndex(0)->setCellValue('BY3','2.12');
		$excel->setActiveSheetIndex(0)->setCellValue('BZ3','2.13');
		$excel->setActiveSheetIndex(0)->setCellValue('CA3','2.14');
		$excel->setActiveSheetIndex(0)->setCellValue('CB3','2.15');
		$excel->setActiveSheetIndex(0)->setCellValue('CC3','2.16');
		$excel->setActiveSheetIndex(0)->setCellValue('CD3','2.17');
		$excel->setActiveSheetIndex(0)->setCellValue('CE3','2.18');
		$excel->setActiveSheetIndex(0)->setCellValue('CF3','2.19');
		$excel->setActiveSheetIndex(0)->setCellValue('CG3','2.2');
		$excel->setActiveSheetIndex(0)->setCellValue('CH3','2.21');
		$excel->setActiveSheetIndex(0)->setCellValue('CI3','2.22');
		$excel->setActiveSheetIndex(0)->setCellValue('CJ3','2.23');
		$excel->setActiveSheetIndex(0)->setCellValue('CK3','2.24');
		$excel->setActiveSheetIndex(0)->setCellValue('CL3','2.25');
		$excel->setActiveSheetIndex(0)->setCellValue('CM3','2.26');
		$excel->setActiveSheetIndex(0)->setCellValue('CN3','2.27');
		$excel->setActiveSheetIndex(0)->setCellValue('CO3','2.28');
		$excel->setActiveSheetIndex(0)->setCellValue('CP3','2.29');
		$excel->setActiveSheetIndex(0)->setCellValue('CQ3','2.3');
		$excel->setActiveSheetIndex(0)->setCellValue('CR3','2.31');
		$excel->setActiveSheetIndex(0)->setCellValue('CS3','2.32');
		$excel->setActiveSheetIndex(0)->setCellValue('CT3','2.33');
		$excel->setActiveSheetIndex(0)->setCellValue('CU3','2.34');
		$excel->setActiveSheetIndex(0)->setCellValue('CV3','2.35');
		$excel->setActiveSheetIndex(0)->setCellValue('CW3','2.36');
		$excel->setActiveSheetIndex(0)->setCellValue('CX3','2.37');
		$excel->setActiveSheetIndex(0)->setCellValue('CY3','2.38');
		$excel->setActiveSheetIndex(0)->setCellValue('CZ3','2.39');
		$excel->setActiveSheetIndex(0)->setCellValue('DA3','2.4');
		$excel->setActiveSheetIndex(0)->setCellValue('DB3','2.41');
		$excel->setActiveSheetIndex(0)->setCellValue('DC3','2.42');
		$excel->setActiveSheetIndex(0)->setCellValue('DD3','2.43');
		$excel->setActiveSheetIndex(0)->setCellValue('DE3','2.44');
		$excel->setActiveSheetIndex(0)->setCellValue('DF3','2.45');
		$excel->setActiveSheetIndex(0)->setCellValue('DG3','2.46');
		$excel->setActiveSheetIndex(0)->setCellValue('DH3','2.47');
		$excel->setActiveSheetIndex(0)->setCellValue('DI3','2.48');
		$excel->setActiveSheetIndex(0)->setCellValue('DJ3','2.49');
		$excel->setActiveSheetIndex(0)->setCellValue('DK3','2.5');
		$excel->setActiveSheetIndex(0)->setCellValue('DL3','2.51');
		$excel->setActiveSheetIndex(0)->setCellValue('DM3','2.52');
		$excel->setActiveSheetIndex(0)->setCellValue('DN3','2.53');
		$excel->setActiveSheetIndex(0)->setCellValue('DO3','2.54');
		$excel->setActiveSheetIndex(0)->setCellValue('DP3','2.55');
		$excel->setActiveSheetIndex(0)->setCellValue('DQ3','3.1');
		$excel->setActiveSheetIndex(0)->setCellValue('DR3','3.2');
		$excel->setActiveSheetIndex(0)->setCellValue('DS3','4.1');
		$excel->setActiveSheetIndex(0)->setCellValue('DT3','4.2');
		$excel->setActiveSheetIndex(0)->setCellValue('DU3','4.3');
		$excel->setActiveSheetIndex(0)->setCellValue('DV3','4.4');
		$excel->setActiveSheetIndex(0)->setCellValue('DW3','4.5');
		$excel->setActiveSheetIndex(0)->setCellValue('DX3','4.6');
		$excel->setActiveSheetIndex(0)->setCellValue('DY3','4.7');
		$excel->setActiveSheetIndex(0)->setCellValue('DZ3','4.8');
		$excel->setActiveSheetIndex(0)->setCellValue('EA3','4.9');
		$excel->setActiveSheetIndex(0)->setCellValue('EB3','4.1');
		$excel->setActiveSheetIndex(0)->setCellValue('EC3','4.11');
		$excel->setActiveSheetIndex(0)->setCellValue('ED3','5.1');
		$excel->setActiveSheetIndex(0)->setCellValue('EE3','5.2');
		$excel->setActiveSheetIndex(0)->setCellValue('EF3','5.3');
		$excel->setActiveSheetIndex(0)->setCellValue('EG3','5.4');
		$excel->setActiveSheetIndex(0)->setCellValue('EH3','5.5');
		$excel->setActiveSheetIndex(0)->setCellValue('EI3','5.6');
		$excel->setActiveSheetIndex(0)->setCellValue('EJ3','5.7');
		$excel->setActiveSheetIndex(0)->setCellValue('EK3','5.8');
		$excel->setActiveSheetIndex(0)->setCellValue('EL3','5.9');
		$excel->setActiveSheetIndex(0)->setCellValue('EM3','5.1');
		$excel->setActiveSheetIndex(0)->setCellValue('EN3','5.11');
		$excel->setActiveSheetIndex(0)->setCellValue('EO3','5.12');
		$excel->setActiveSheetIndex(0)->setCellValue('EP3','5.13');
		$excel->setActiveSheetIndex(0)->setCellValue('EQ3','5.14');
		$excel->setActiveSheetIndex(0)->setCellValue('ER3','5.15');
		$excel->setActiveSheetIndex(0)->setCellValue('ES3','5.16');
		$excel->setActiveSheetIndex(0)->setCellValue('ET3','5.17');
		$excel->setActiveSheetIndex(0)->setCellValue('EU3','5.18');
		$excel->setActiveSheetIndex(0)->setCellValue('EV3','5.19');
		$excel->setActiveSheetIndex(0)->setCellValue('EW3','5.2');
		$excel->setActiveSheetIndex(0)->setCellValue('EX3','5.21');
		$excel->setActiveSheetIndex(0)->setCellValue('EY3','5.22');
		$excel->setActiveSheetIndex(0)->setCellValue('EZ3','5.23');
		$excel->setActiveSheetIndex(0)->setCellValue('FA3','5.24');
		$excel->setActiveSheetIndex(0)->setCellValue('FB3','5.25');
		$excel->setActiveSheetIndex(0)->setCellValue('FC3','5.26');
		$excel->setActiveSheetIndex(0)->setCellValue('FD3','5.27');
		$excel->setActiveSheetIndex(0)->setCellValue('FE3','5.28');
		$excel->setActiveSheetIndex(0)->setCellValue('FF3','6.1');
		$excel->setActiveSheetIndex(0)->setCellValue('FG3','6.2');
		$excel->setActiveSheetIndex(0)->setCellValue('FH3','6.3');
		$excel->setActiveSheetIndex(0)->setCellValue('FI3','6.4');
		$excel->setActiveSheetIndex(0)->setCellValue('FJ3','6.5');
		$excel->setActiveSheetIndex(0)->setCellValue('FK3','6.6');
		$excel->setActiveSheetIndex(0)->setCellValue('FL3','6.7');
		$excel->setActiveSheetIndex(0)->setCellValue('FM3','6.8');
		$excel->setActiveSheetIndex(0)->setCellValue('FN3','6.9');
		$excel->setActiveSheetIndex(0)->setCellValue('FO3','6.1');
		$excel->setActiveSheetIndex(0)->setCellValue('FP3','6.11');
		$excel->setActiveSheetIndex(0)->setCellValue('FQ3','6.12');
		$excel->setActiveSheetIndex(0)->setCellValue('FR3','7.1');
		$excel->setActiveSheetIndex(0)->setCellValue('FS3','7.2');
		$excel->setActiveSheetIndex(0)->setCellValue('FT3','7.3');
		$excel->setActiveSheetIndex(0)->setCellValue('FU3','7.4');
		$excel->setActiveSheetIndex(0)->setCellValue('FV3','7.5');

		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ED')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ER')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ES')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ET')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FV')->setAutoSize(true);

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		// $excel->getActiveSheet()->getStyle('A2:A3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('B2:B3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('C2:C3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('D2:D3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('E2:E3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('F2:F3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('G2:G3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('H2:H3')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('I2:AL2')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('I3:AL3')->applyFromArray($style_col);
		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		// $siswa = $this->SiswaModel->view();

		// $tanggal =$this->input->get('tanggal');
		// $detail_report='';

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		$details = $this->ReportModel->downloadReportTls_ranged($dari,$sampai,$factory);
	
		
		$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4

		foreach ($details->result() as $key => $detail) {
			$rft  = (($detail->total_checked-$detail->count_defect)/$detail->total_checked)*100;
		// foreach($siswa as $data){ // Lakukan looping pada variabel siswa
		  $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		  $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $detail->create_date);
		  $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $detail->line_name);
		  $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $detail->poreference);
		  $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $detail->style);
		  $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $detail->total_checked);
		  $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $detail->count_defect);
		  $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($rft,1,',',',').'%');
		  $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $detail->defect1);
		$excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $detail->defect2);
		$excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $detail->defect3);
		$excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $detail->defect4);
		$excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $detail->defect5);
		$excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $detail->defect6);
		$excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $detail->defect7);
		$excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, $detail->defect8);
		$excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, $detail->defect9);
		$excel->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $detail->defect10);
		$excel->setActiveSheetIndex(0)->setCellValue('S'.$numrow, $detail->defect11);
		$excel->setActiveSheetIndex(0)->setCellValue('T'.$numrow, $detail->defect12);
		$excel->setActiveSheetIndex(0)->setCellValue('U'.$numrow, $detail->defect13);
		$excel->setActiveSheetIndex(0)->setCellValue('V'.$numrow, $detail->defect14);
		$excel->setActiveSheetIndex(0)->setCellValue('W'.$numrow, $detail->defect15);
		$excel->setActiveSheetIndex(0)->setCellValue('X'.$numrow, $detail->defect16);
		$excel->setActiveSheetIndex(0)->setCellValue('Y'.$numrow, $detail->defect17);
		$excel->setActiveSheetIndex(0)->setCellValue('Z'.$numrow, $detail->defect18);
		$excel->setActiveSheetIndex(0)->setCellValue('AA'.$numrow, $detail->defect19);
		$excel->setActiveSheetIndex(0)->setCellValue('AB'.$numrow, $detail->defect20);
		$excel->setActiveSheetIndex(0)->setCellValue('AC'.$numrow, $detail->defect21);
		$excel->setActiveSheetIndex(0)->setCellValue('AD'.$numrow, $detail->defect22);
		$excel->setActiveSheetIndex(0)->setCellValue('AE'.$numrow, $detail->defect23);
		$excel->setActiveSheetIndex(0)->setCellValue('AF'.$numrow, $detail->defect24);
		$excel->setActiveSheetIndex(0)->setCellValue('AG'.$numrow, $detail->defect25);
		$excel->setActiveSheetIndex(0)->setCellValue('AH'.$numrow, $detail->defect26);
		$excel->setActiveSheetIndex(0)->setCellValue('AI'.$numrow, $detail->defect27);
		$excel->setActiveSheetIndex(0)->setCellValue('AJ'.$numrow, $detail->defect28);
		$excel->setActiveSheetIndex(0)->setCellValue('AK'.$numrow, $detail->defect29);
		$excel->setActiveSheetIndex(0)->setCellValue('AL'.$numrow, $detail->defect30);
		$excel->setActiveSheetIndex(0)->setCellValue('AM'.$numrow, $detail->defect31);
		$excel->setActiveSheetIndex(0)->setCellValue('AN'.$numrow, $detail->defect32);
		$excel->setActiveSheetIndex(0)->setCellValue('AO'.$numrow, $detail->defect33);
		$excel->setActiveSheetIndex(0)->setCellValue('AP'.$numrow, $detail->defect34);
		$excel->setActiveSheetIndex(0)->setCellValue('AQ'.$numrow, $detail->defect35);
		$excel->setActiveSheetIndex(0)->setCellValue('AR'.$numrow, $detail->defect36);
		$excel->setActiveSheetIndex(0)->setCellValue('AS'.$numrow, $detail->defect37);
		$excel->setActiveSheetIndex(0)->setCellValue('AT'.$numrow, $detail->defect38);
		$excel->setActiveSheetIndex(0)->setCellValue('AU'.$numrow, $detail->defect39);
		$excel->setActiveSheetIndex(0)->setCellValue('AV'.$numrow, $detail->defect40);
		$excel->setActiveSheetIndex(0)->setCellValue('AW'.$numrow, $detail->defect41);
		$excel->setActiveSheetIndex(0)->setCellValue('AX'.$numrow, $detail->defect42);
		$excel->setActiveSheetIndex(0)->setCellValue('AY'.$numrow, $detail->defect43);
		$excel->setActiveSheetIndex(0)->setCellValue('AZ'.$numrow, $detail->defect44);
		$excel->setActiveSheetIndex(0)->setCellValue('BA'.$numrow, $detail->defect45);
		$excel->setActiveSheetIndex(0)->setCellValue('BB'.$numrow, $detail->defect46);
		$excel->setActiveSheetIndex(0)->setCellValue('BC'.$numrow, $detail->defect47);
		$excel->setActiveSheetIndex(0)->setCellValue('BD'.$numrow, $detail->defect48);
		$excel->setActiveSheetIndex(0)->setCellValue('BE'.$numrow, $detail->defect49);
		$excel->setActiveSheetIndex(0)->setCellValue('BF'.$numrow, $detail->defect50);
		$excel->setActiveSheetIndex(0)->setCellValue('BG'.$numrow, $detail->defect51);
		$excel->setActiveSheetIndex(0)->setCellValue('BH'.$numrow, $detail->defect52);
		$excel->setActiveSheetIndex(0)->setCellValue('BI'.$numrow, $detail->defect53);
		$excel->setActiveSheetIndex(0)->setCellValue('BJ'.$numrow, $detail->defect54);
		$excel->setActiveSheetIndex(0)->setCellValue('BK'.$numrow, $detail->defect55);
		$excel->setActiveSheetIndex(0)->setCellValue('BL'.$numrow, $detail->defect56);
		$excel->setActiveSheetIndex(0)->setCellValue('BM'.$numrow, $detail->defect57);
		$excel->setActiveSheetIndex(0)->setCellValue('BN'.$numrow, $detail->defect58);
		$excel->setActiveSheetIndex(0)->setCellValue('BO'.$numrow, $detail->defect59);
		$excel->setActiveSheetIndex(0)->setCellValue('BP'.$numrow, $detail->defect60);
		$excel->setActiveSheetIndex(0)->setCellValue('BQ'.$numrow, $detail->defect61);
		$excel->setActiveSheetIndex(0)->setCellValue('BR'.$numrow, $detail->defect62);
		$excel->setActiveSheetIndex(0)->setCellValue('BS'.$numrow, $detail->defect63);
		$excel->setActiveSheetIndex(0)->setCellValue('BT'.$numrow, $detail->defect64);
		$excel->setActiveSheetIndex(0)->setCellValue('BU'.$numrow, $detail->defect65);
		$excel->setActiveSheetIndex(0)->setCellValue('BV'.$numrow, $detail->defect66);
		$excel->setActiveSheetIndex(0)->setCellValue('BW'.$numrow, $detail->defect67);
		$excel->setActiveSheetIndex(0)->setCellValue('BX'.$numrow, $detail->defect68);
		$excel->setActiveSheetIndex(0)->setCellValue('BY'.$numrow, $detail->defect69);
		$excel->setActiveSheetIndex(0)->setCellValue('BZ'.$numrow, $detail->defect70);
		$excel->setActiveSheetIndex(0)->setCellValue('CA'.$numrow, $detail->defect71);
		$excel->setActiveSheetIndex(0)->setCellValue('CB'.$numrow, $detail->defect72);
		$excel->setActiveSheetIndex(0)->setCellValue('CC'.$numrow, $detail->defect73);
		$excel->setActiveSheetIndex(0)->setCellValue('CD'.$numrow, $detail->defect74);
		$excel->setActiveSheetIndex(0)->setCellValue('CE'.$numrow, $detail->defect75);
		$excel->setActiveSheetIndex(0)->setCellValue('CF'.$numrow, $detail->defect76);
		$excel->setActiveSheetIndex(0)->setCellValue('CG'.$numrow, $detail->defect77);
		$excel->setActiveSheetIndex(0)->setCellValue('CH'.$numrow, $detail->defect78);
		$excel->setActiveSheetIndex(0)->setCellValue('CI'.$numrow, $detail->defect79);
		$excel->setActiveSheetIndex(0)->setCellValue('CJ'.$numrow, $detail->defect80);
		$excel->setActiveSheetIndex(0)->setCellValue('CK'.$numrow, $detail->defect81);
		$excel->setActiveSheetIndex(0)->setCellValue('CL'.$numrow, $detail->defect82);
		$excel->setActiveSheetIndex(0)->setCellValue('CM'.$numrow, $detail->defect83);
		$excel->setActiveSheetIndex(0)->setCellValue('CN'.$numrow, $detail->defect84);
		$excel->setActiveSheetIndex(0)->setCellValue('CO'.$numrow, $detail->defect85);
		$excel->setActiveSheetIndex(0)->setCellValue('CP'.$numrow, $detail->defect86);
		$excel->setActiveSheetIndex(0)->setCellValue('CQ'.$numrow, $detail->defect87);
		$excel->setActiveSheetIndex(0)->setCellValue('CR'.$numrow, $detail->defect88);
		$excel->setActiveSheetIndex(0)->setCellValue('CS'.$numrow, $detail->defect89);
		$excel->setActiveSheetIndex(0)->setCellValue('CT'.$numrow, $detail->defect90);
		$excel->setActiveSheetIndex(0)->setCellValue('CU'.$numrow, $detail->defect91);
		$excel->setActiveSheetIndex(0)->setCellValue('CV'.$numrow, $detail->defect92);
		$excel->setActiveSheetIndex(0)->setCellValue('CW'.$numrow, $detail->defect93);
		$excel->setActiveSheetIndex(0)->setCellValue('CX'.$numrow, $detail->defect94);
		$excel->setActiveSheetIndex(0)->setCellValue('CY'.$numrow, $detail->defect95);
		$excel->setActiveSheetIndex(0)->setCellValue('CZ'.$numrow, $detail->defect96);
		$excel->setActiveSheetIndex(0)->setCellValue('DA'.$numrow, $detail->defect97);
		$excel->setActiveSheetIndex(0)->setCellValue('DB'.$numrow, $detail->defect98);
		$excel->setActiveSheetIndex(0)->setCellValue('DC'.$numrow, $detail->defect99);
		$excel->setActiveSheetIndex(0)->setCellValue('DD'.$numrow, $detail->defect100);
		$excel->setActiveSheetIndex(0)->setCellValue('DE'.$numrow, $detail->defect101);
		$excel->setActiveSheetIndex(0)->setCellValue('DF'.$numrow, $detail->defect102);
		$excel->setActiveSheetIndex(0)->setCellValue('DG'.$numrow, $detail->defect103);
		$excel->setActiveSheetIndex(0)->setCellValue('DH'.$numrow, $detail->defect104);
		$excel->setActiveSheetIndex(0)->setCellValue('DI'.$numrow, $detail->defect105);
		$excel->setActiveSheetIndex(0)->setCellValue('DJ'.$numrow, $detail->defect106);
		$excel->setActiveSheetIndex(0)->setCellValue('DK'.$numrow, $detail->defect107);
		$excel->setActiveSheetIndex(0)->setCellValue('DL'.$numrow, $detail->defect108);
		$excel->setActiveSheetIndex(0)->setCellValue('DM'.$numrow, $detail->defect109);
		$excel->setActiveSheetIndex(0)->setCellValue('DN'.$numrow, $detail->defect110);
		$excel->setActiveSheetIndex(0)->setCellValue('DO'.$numrow, $detail->defect111);
		$excel->setActiveSheetIndex(0)->setCellValue('DP'.$numrow, $detail->defect112);
		$excel->setActiveSheetIndex(0)->setCellValue('DQ'.$numrow, $detail->defect113);
		$excel->setActiveSheetIndex(0)->setCellValue('DR'.$numrow, $detail->defect114);
		$excel->setActiveSheetIndex(0)->setCellValue('DS'.$numrow, $detail->defect115);
		$excel->setActiveSheetIndex(0)->setCellValue('DT'.$numrow, $detail->defect116);
		$excel->setActiveSheetIndex(0)->setCellValue('DU'.$numrow, $detail->defect117);
		$excel->setActiveSheetIndex(0)->setCellValue('DV'.$numrow, $detail->defect118);
		$excel->setActiveSheetIndex(0)->setCellValue('DW'.$numrow, $detail->defect119);
		$excel->setActiveSheetIndex(0)->setCellValue('DX'.$numrow, $detail->defect120);
		$excel->setActiveSheetIndex(0)->setCellValue('DY'.$numrow, $detail->defect121);
		$excel->setActiveSheetIndex(0)->setCellValue('DZ'.$numrow, $detail->defect122);
		$excel->setActiveSheetIndex(0)->setCellValue('EA'.$numrow, $detail->defect123);
		$excel->setActiveSheetIndex(0)->setCellValue('EB'.$numrow, $detail->defect124);
		$excel->setActiveSheetIndex(0)->setCellValue('EC'.$numrow, $detail->defect125);
		$excel->setActiveSheetIndex(0)->setCellValue('ED'.$numrow, $detail->defect126);
		$excel->setActiveSheetIndex(0)->setCellValue('EE'.$numrow, $detail->defect127);
		$excel->setActiveSheetIndex(0)->setCellValue('EF'.$numrow, $detail->defect128);
		$excel->setActiveSheetIndex(0)->setCellValue('EG'.$numrow, $detail->defect129);
		$excel->setActiveSheetIndex(0)->setCellValue('EH'.$numrow, $detail->defect130);
		$excel->setActiveSheetIndex(0)->setCellValue('EI'.$numrow, $detail->defect131);
		$excel->setActiveSheetIndex(0)->setCellValue('EJ'.$numrow, $detail->defect132);
		$excel->setActiveSheetIndex(0)->setCellValue('EK'.$numrow, $detail->defect133);
		$excel->setActiveSheetIndex(0)->setCellValue('EL'.$numrow, $detail->defect134);
		$excel->setActiveSheetIndex(0)->setCellValue('EM'.$numrow, $detail->defect135);
		$excel->setActiveSheetIndex(0)->setCellValue('EN'.$numrow, $detail->defect136);
		$excel->setActiveSheetIndex(0)->setCellValue('EO'.$numrow, $detail->defect137);
		$excel->setActiveSheetIndex(0)->setCellValue('EP'.$numrow, $detail->defect138);
		$excel->setActiveSheetIndex(0)->setCellValue('EQ'.$numrow, $detail->defect139);
		$excel->setActiveSheetIndex(0)->setCellValue('ER'.$numrow, $detail->defect140);
		$excel->setActiveSheetIndex(0)->setCellValue('ES'.$numrow, $detail->defect141);
		$excel->setActiveSheetIndex(0)->setCellValue('ET'.$numrow, $detail->defect142);
		$excel->setActiveSheetIndex(0)->setCellValue('EU'.$numrow, $detail->defect143);
		$excel->setActiveSheetIndex(0)->setCellValue('EV'.$numrow, $detail->defect144);
		$excel->setActiveSheetIndex(0)->setCellValue('EW'.$numrow, $detail->defect145);
		$excel->setActiveSheetIndex(0)->setCellValue('EX'.$numrow, $detail->defect146);
		$excel->setActiveSheetIndex(0)->setCellValue('EY'.$numrow, $detail->defect147);
		$excel->setActiveSheetIndex(0)->setCellValue('EZ'.$numrow, $detail->defect148);
		$excel->setActiveSheetIndex(0)->setCellValue('FA'.$numrow, $detail->defect149);
		$excel->setActiveSheetIndex(0)->setCellValue('FB'.$numrow, $detail->defect150);
		$excel->setActiveSheetIndex(0)->setCellValue('FC'.$numrow, $detail->defect151);
		$excel->setActiveSheetIndex(0)->setCellValue('FD'.$numrow, $detail->defect152);
		$excel->setActiveSheetIndex(0)->setCellValue('FE'.$numrow, $detail->defect153);
		$excel->setActiveSheetIndex(0)->setCellValue('FF'.$numrow, $detail->defect154);
		$excel->setActiveSheetIndex(0)->setCellValue('FG'.$numrow, $detail->defect155);
		$excel->setActiveSheetIndex(0)->setCellValue('FH'.$numrow, $detail->defect156);
		$excel->setActiveSheetIndex(0)->setCellValue('FI'.$numrow, $detail->defect157);
		$excel->setActiveSheetIndex(0)->setCellValue('FJ'.$numrow, $detail->defect158);
		$excel->setActiveSheetIndex(0)->setCellValue('FK'.$numrow, $detail->defect159);
		$excel->setActiveSheetIndex(0)->setCellValue('FL'.$numrow, $detail->defect160);
		$excel->setActiveSheetIndex(0)->setCellValue('FM'.$numrow, $detail->defect161);
		$excel->setActiveSheetIndex(0)->setCellValue('FN'.$numrow, $detail->defect162);
		$excel->setActiveSheetIndex(0)->setCellValue('FO'.$numrow, $detail->defect163);
		$excel->setActiveSheetIndex(0)->setCellValue('FP'.$numrow, $detail->defect164);
		$excel->setActiveSheetIndex(0)->setCellValue('FQ'.$numrow, $detail->defect165);
		$excel->setActiveSheetIndex(0)->setCellValue('FR'.$numrow, $detail->defect166);
		$excel->setActiveSheetIndex(0)->setCellValue('FS'.$numrow, $detail->defect167);
		$excel->setActiveSheetIndex(0)->setCellValue('FT'.$numrow, $detail->defect168);
		$excel->setActiveSheetIndex(0)->setCellValue('FU'.$numrow, $detail->defect169);
		$excel->setActiveSheetIndex(0)->setCellValue('FV'.$numrow, $detail->defect170);

		  
		  // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		  $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('T'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('U'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('V'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('W'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('X'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Y'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Z'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AA'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AB'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AC'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AD'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AE'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AF'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AG'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AH'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AI'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AJ'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AK'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AL'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AN'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AO'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AP'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AQ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AR'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AS'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AT'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AU'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AV'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AW'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AX'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AY'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AZ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BA'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BB'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BC'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BD'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BE'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BF'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BG'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BH'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BI'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BJ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BK'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BL'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BN'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BO'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BP'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BQ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BR'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BS'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BT'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BU'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BV'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BW'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BX'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BY'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BZ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CA'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CB'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CC'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CD'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CE'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CF'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CG'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CH'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CI'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CJ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CK'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CL'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CN'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CO'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CP'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CQ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CR'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CS'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CT'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CU'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CV'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CW'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CX'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CY'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('CZ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DA'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DB'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DC'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DD'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DE'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DF'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DG'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DH'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DI'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DJ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DK'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DL'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DN'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DO'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DP'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DQ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DR'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DS'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DT'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DU'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DV'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DW'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DX'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DY'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('DZ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EA'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EB'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EC'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('ED'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EE'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EF'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EG'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EH'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EI'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EJ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EK'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EL'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EN'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EO'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EP'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EQ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('ER'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('ES'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('ET'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EU'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EV'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EW'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EX'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EY'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('EZ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FA'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FB'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FC'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FD'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FE'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FF'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FG'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FH'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FI'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FJ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FK'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FL'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FN'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FO'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FP'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FQ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FR'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FS'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FT'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FU'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('FV'.$numrow)->applyFromArray($style_row);


		  
		  $no++; // Tambah 1 setiap kali looping
		  $numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
		// $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom E
		// $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('AZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('BZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('CZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('DZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ED')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ER')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ES')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('ET')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EV')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EW')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EX')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EY')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('EZ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FA')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FB')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FC')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FD')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FE')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FF')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FG')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FH')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FI')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FJ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FK')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FL')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FM')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FN')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FO')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FP')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FQ')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FR')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FS')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FT')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FU')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('FV')->setAutoSize(true);

		
		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Laporan TLS Inline");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment; filename="DATA TLS Inline "'.$dari.'" sd "'.$sampai.'".xlsx"'); // Set nama file excel nya
		// header('Cache-Control: max-age=0');
		$filename = 'TLS_Inline_AOI_'.$factory.'.xlsx';
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        // $write->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
		$path = $_SERVER["DOCUMENT_ROOT"]."/var/www/html/line_system/assets/excel/";
		// $write->save($path.$fileName);
		$write->save(str_replace(__FILE__,''.$path.'/'.$filename,__FILE__));
		// $write->save('php://output');
	}

	public function export_inline_bulan($factory_id){
		// Load plugin PHPExcel nya
		include_once APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		// $tgl     = date('Y-m-d');
		// $dari    = date('Y-m-d',strtotime('-7 days'));
		
		$dari    = '2020-01-01';
		$sampai  = '2020-06-30';
		$factory = $factory_id;

		// var_dump($dari);
		// var_dump($sampai);
		// die();

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));
		echo "start from: ".date('Y-m-d H:i:s');
		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('ICT Team')
					 ->setLastModifiedBy('ICT Team')
					 ->setTitle("Data TLS Inline".$dari." sd ".$sampai."")
					 ->setSubject("TLS Inline")
					 ->setDescription("Laporan TLS Inline ".$dari." sd ".$sampai."")
					 ->setKeywords("Data TLS Inline");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
		  'font' => array('bold' => true), // Set font nya jadi bold
		  'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Set text jadi ditengah secara horizontal (center)
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER      // Set text jadi di tengah secara vertical (middle)
		  ),
		  'borders' => array(
			'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
			'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
			'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
		  )
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
		  'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
		  ),
		  'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		  )
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA TLS Inline ".$dari." sd ".$sampai.""); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:U1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3

		
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
		$excel->getActiveSheet()->mergeCells('A2:A3'); // Set kolom A3 dengan tulisan "NO"
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('B2', "DATE");
		$excel->getActiveSheet()->mergeCells('B2:B3'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('C2', "LINE");
		$excel->getActiveSheet()->mergeCells('C2:C3');  // Set kolom B3 dengan tulisan "NIS"
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('D2', "PO BUYER");
		$excel->getActiveSheet()->mergeCells('D2:D3');  // Set kolom C3 dengan tulisan "NAMA"
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('E2', "STYLE");
		$excel->getActiveSheet()->mergeCells('E2:E3');  // Set kolom D3 dengan tulisan "JENIS KELAMIN"
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('F2', "R1 RANDOM");
		$excel->getActiveSheet()->mergeCells('F2:F3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('G2', "TOTAL DEFECT");
		$excel->getActiveSheet()->mergeCells('G2:G3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('H2', "RFT");
		$excel->getActiveSheet()->mergeCells('H2:H3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('I2', "DEFECT CODE");
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom A

		$excel->getActiveSheet()->mergeCells('I2:AL2');
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "1");
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "2");
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "3");
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "4");
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "5");
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('N3', "6");
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('O3', "7");
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('P3', "8");
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('Q3', "9");
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('R3', "10");
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('S3', "11");
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('T3', "12");
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('U3', "13");
		$excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('V3', "14");
		$excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('W3', "15");
		$excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('X3', "16");
		$excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('Y3', "17");
		$excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('Z3', "18");
		$excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AA3', "19");
		$excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AB3', "20");
		$excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AC3', "21");
		$excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AD3', "2");
		$excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AE3', "23");
		$excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AF3', "24");
		$excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AG3', "25");
		$excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AH3', "26");
		$excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AI3', "27");
		$excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AJ3', "28");
		$excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AK3', "29");
		$excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AL3', "30");  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true); // Set width kolom A
		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A2:A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B2:B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C2:C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D2:D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E2:E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F2:F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G2:G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H2:H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I2:AL2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3:AL3')->applyFromArray($style_col);
		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		// $siswa = $this->SiswaModel->view();

		// $tanggal =$this->input->get('tanggal');
		// $detail_report='';

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		$details = $this->ReportModel->downloadReportTls_ranged($dari,$sampai,$factory);
	
		
		$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4

		foreach ($details->result() as $key => $detail) {
			$rft  = (($detail->total_checked-$detail->count_defect)/$detail->total_checked)*100;
		// foreach($siswa as $data){ // Lakukan looping pada variabel siswa
		  $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		  $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $detail->create_date);
		  $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $detail->line_name);
		  $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $detail->poreference);
		  $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $detail->style);
		  $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $detail->total_checked);
		  $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $detail->count_defect);
		  $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($rft,1,',',',').'%');
		  $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $detail->defect1);
		  $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $detail->defect2);
		  $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $detail->defect3);
		  $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $detail->defect4);
		  $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $detail->defect5);
		  $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $detail->defect6);
		  $excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $detail->defect7);
		  $excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, $detail->defect8);
		  $excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, $detail->defect9);
		  $excel->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $detail->defect10);
		  $excel->setActiveSheetIndex(0)->setCellValue('S'.$numrow, $detail->defect11);
		  $excel->setActiveSheetIndex(0)->setCellValue('T'.$numrow, $detail->defect12);
		  $excel->setActiveSheetIndex(0)->setCellValue('U'.$numrow, $detail->defect13);
		  $excel->setActiveSheetIndex(0)->setCellValue('V'.$numrow, $detail->defect14);
		  $excel->setActiveSheetIndex(0)->setCellValue('W'.$numrow, $detail->defect15);
		  $excel->setActiveSheetIndex(0)->setCellValue('X'.$numrow, $detail->defect16);
		  $excel->setActiveSheetIndex(0)->setCellValue('Y'.$numrow, $detail->defect17);
		  $excel->setActiveSheetIndex(0)->setCellValue('Z'.$numrow, $detail->defect18);
		  $excel->setActiveSheetIndex(0)->setCellValue('AA'.$numrow, $detail->defect19);
		  $excel->setActiveSheetIndex(0)->setCellValue('AB'.$numrow, $detail->defect20);
		  $excel->setActiveSheetIndex(0)->setCellValue('AC'.$numrow, $detail->defect21);
		  $excel->setActiveSheetIndex(0)->setCellValue('AD'.$numrow, $detail->defect22);
		  $excel->setActiveSheetIndex(0)->setCellValue('AE'.$numrow, $detail->defect23);
		  $excel->setActiveSheetIndex(0)->setCellValue('AF'.$numrow, $detail->defect24);
		  $excel->setActiveSheetIndex(0)->setCellValue('AG'.$numrow, $detail->defect25);
		  $excel->setActiveSheetIndex(0)->setCellValue('AH'.$numrow, $detail->defect26);
		  $excel->setActiveSheetIndex(0)->setCellValue('AI'.$numrow, $detail->defect27);
		  $excel->setActiveSheetIndex(0)->setCellValue('AJ'.$numrow, $detail->defect28);
		  $excel->setActiveSheetIndex(0)->setCellValue('AK'.$numrow, $detail->defect29);
		  $excel->setActiveSheetIndex(0)->setCellValue('AL'.$numrow, $detail->defect30);
		  
		  // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		  $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('T'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('U'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('V'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('W'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('X'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Y'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Z'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AA'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AB'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AC'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AD'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AE'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AF'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AG'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AH'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AI'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AJ'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AK'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AL'.$numrow)->applyFromArray($style_row);
		  
		  $no++; // Tambah 1 setiap kali looping
		  $numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
		// $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom E
		// $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true); // Set width kolom E
		
		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Laporan TLS Inline");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment; filename="DATA TLS Inline "'.$dari.'" sd "'.$sampai.'".xlsx"'); // Set nama file excel nya
		// header('Cache-Control: max-age=0');
		$filename = 'TLS_Inline_AOI 2_Bulan 1-6.xlsx';
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        // $write->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
		$path = $_SERVER["DOCUMENT_ROOT"]."/var/www/html/line_system/assets/excel/";
		// $write->save($path.$fileName);
		$write->save(str_replace(__FILE__,''.$path.'/'.$filename,__FILE__));
		// $write->save('php://output');
		echo "end on: ".date('Y-m-d H:i:s');
	}

}

/* End of file cronsummary.php */
/* Location: ./application/controllers/cronsummary.php */
