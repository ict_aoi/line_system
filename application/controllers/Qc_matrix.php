<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Qc_matrix extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		chek_session();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('Qc_matrix_model', 'Cekpermision_model', 'Master_workflow_model'));
	}

	public function index()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$this->template->set('title', 'QC Matrix');
			$this->template->set('desc_page', 'QC Matrix');
			$this->template->load('layout', 'qc_matrix/qc_matrix_chose');
		}
	}
	public function loadline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$this->load->view('qc_matrix/list_line');
		}
	}
	public function loadstyle()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$line_id['record'] = $_GET['id'];
			$this->load->view('qc_matrix/list_style', $line_id);
		}
	}

	public function listline()
	{
		$columns = array(
			0 => 'master_line_id',
			1 => 'line_name',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');


		$totalData = $this->Qc_matrix_model->allposts_count_line();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Qc_matrix_model->allposts_line($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Qc_matrix_model->posts_search_line($limit, $start, $search, $order, $dir);

			$totalFiltered = $this->Qc_matrix_model->posts_search_count_line($search);
		}
		$data = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $master) {
				$_temp = '"' . $master->master_line_id . '","' . trim($master->line_name) . '"';
				$nestedData['no'] = (($draw - 1) * 10) + (++$nomor_urut);
				$nestedData['line_name'] = ucwords($master->line_name);
				$nestedData['action'] = "<center><a onclick='return select($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}
	public function list_style_ajax()
	{
		$columns = array(
			// 0 =>'poreference',
			0 => 'style',
		);
		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');
		$factory = $this->session->userdata('factory');
		$line_id = $this->input->post('line_id');

		$totalData = $this->Qc_matrix_model->allposts_count_list($factory, $line_id);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Qc_matrix_model->allposts_list($limit, $start, $order, $dir, $factory, $line_id);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Qc_matrix_model->posts_search_list($limit, $start, $search, $order, $dir, $factory, $line_id);

			$totalFiltered = $this->Qc_matrix_model->posts_search_count_list($search, $factory, $line_id);
		}
		$data = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $master) {
				$_temp = '"' . $master->style . '"';
				$nestedData['no'] = (($draw - 1) * 10) + (++$nomor_urut);
				$nestedData['style'] = ucwords($master->style);
				$nestedData['action'] = "<center><a onclick='return pilih($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function searchsubmit($line = 0, $po = NULL)
	{

		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$line      = $this->input->post('line_id');
			$style     = $this->input->post('style');
			$factory   = $this->session->userdata('factory');
			$cek_batch = $this->Qc_matrix_model->cekBatch();
			$cek       = $this->Qc_matrix_model->cekheader($line, $style);
			// var_dump($line);
			// var_dump($style);
			// die();
			$ml        = $this->db->query("SELECT * FROM master_line where master_line_id = '$line'")->row();

			// $poreference = $this->input->post('poreference',TRUE);
			if ($cek_batch->num_rows() == null) {


				// $detail_id      = $cek->result()[0]->inline_header_id;
				$date           = date('Y-m-d');
				// $maxheader      = $this->Qc_matrix_model->maxheader($line);
				// var_dump ($maxheader);
				// die();

				$x = $this->Master_workflow_model->getworkflow($style);
				$data = array(
					'record'       => $cek->row_array(),
					'style '       => $x,
					'main_style'   => $style,
					'line'         => $ml->line_name,
					'factory'      => $factory,
					'status_batch' => 200,
					'batch'        => 0,
				);
				// $data['style'] = $this->master_workflow_model->getworkflow($style);

				$this->load->view('qc_matrix/qc_matrix', $data);
				// $status_batch = 200;
			} else {
				$batch = $cek_batch->row();
				if ($batch->style == $style && $batch->line_id == $line) {
					$date           = date('Y-m-d');
					// $maxheader      = $this->Qc_matrix_model->maxheader($line);
					// var_dump ($maxheader);
					// die();
					$x = $this->Master_workflow_model->getworkflow($style);
					$data = array(
						'record'       => $cek->row_array(),
						'style '       => $x,
						'main_style'   => $style,
						'line'         => $ml->line_name,
						'factory'      => $factory,
						'status_batch' => 300,
						'batch'        => $batch->id,
						'batch_style'  => $batch->style,
					);
					// $data['style'] = $this->master_workflow_model->getworkflow($style);

					$this->load->view('qc_matrix/qc_matrix', $data);
				} else {

					$data = array(
						'record'  => $cek->row_array(),
						'line'    => $ml->line_name,
						'factory' => $factory,
						'style'   => $style,
						'batch'   => $cek_batch->row_array(),
					);

					$this->load->view('qc_matrix/qc_matrix_batch', $data);
				}
			}
			// }else{
			//     echo "gagal";
			// }
		}
	}


	public function searchsubmit_ulang()
	{

		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$line      = $this->input->get('line_id');
			$style     = $this->input->get('style');
			$factory   = $this->session->userdata('factory');
			$cek_batch = $this->Qc_matrix_model->cekBatch();
			$cek       = $this->Qc_matrix_model->cekheader($line, $style);
			// var_dump($line);
			// var_dump($style);
			// die();
			$ml        = $this->db->query("SELECT * FROM master_line where master_line_id = '$line'")->row();

			// $poreference = $this->input->post('poreference',TRUE);
			if ($cek_batch->num_rows() == null) {


				// $detail_id      = $cek->result()[0]->inline_header_id;
				$date           = date('Y-m-d');
				// $maxheader      = $this->Qc_matrix_model->maxheader($line);
				// var_dump ($maxheader);
				// die();

				$x = $this->Master_workflow_model->getworkflow($style);
				$data = array(
					'record'       => $cek->row_array(),
					'style '       => $x,
					'main_style'   => $style,
					'line'         => $ml->line_name,
					'factory'      => $factory,
					'status_batch' => 200,
					'batch'        => 0,
				);
				// $data['style'] = $this->master_workflow_model->getworkflow($style);

				$this->load->view('qc_matrix/qc_matrix', $data);
				// $status_batch = 200;
			} else {
				$batch = $cek_batch->row();
				if ($batch->style == $style && $batch->line_id == $line) {
					$date           = date('Y-m-d');
					// $maxheader      = $this->Qc_matrix_model->maxheader($line);
					// var_dump ($maxheader);
					// die();
					$x = $this->Master_workflow_model->getworkflow($style);
					$data = array(
						'record'       => $cek->row_array(),
						'style '       => $x,
						'main_style'   => $style,
						'line'         => $ml->line_name,
						'factory'      => $factory,
						'status_batch' => 300,
						'batch'        => $batch->id,
						'batch_style'  => $batch->style,
					);
					// $data['style'] = $this->master_workflow_model->getworkflow($style);

					$this->load->view('qc_matrix/qc_matrix', $data);
				} else {

					$data = array(
						'record'  => $cek->row_array(),
						'line'    => $ml->line_name,
						'factory' => $factory,
						'style'   => $style,
						'batch'   => $cek_batch->row_array(),
					);

					$this->load->view('qc_matrix/qc_matrix_batch', $data);
				}
			}
			// }else{
			//     echo "gagal";
			// }
		}
	}


	public function proses_view()
	{
		$style_id = $_GET['id'];


		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		// $record = $this->Master_workflow_model->getworkflow($style_id);
		// var_dump($style_id);
		// var_dump($record);
		// die();
		// $data['record'] = json_encode($record);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			// $this->load->view('qc_matrix/list_proses',$data);
			$this->load->view('qc_matrix/list_proses');
		}
	}
	public function listproses()
	{
		$columns = array(
			0 => 'master_workflow_id',
			1 => 'proses_name',
			2 => 'jumlah',
		);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');
		$style = $this->input->post('style');
		$line_id = $this->input->post('line_id');
		$round = $this->input->post('round');


		$totalData = $this->Qc_matrix_model->allposts_count_proses($style);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Qc_matrix_model->allposts_proses($limit, $start, $order, $dir, $style);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Qc_matrix_model->posts_search_proses($limit, $start, $search, $order, $dir, $style);

			$totalFiltered = $this->Qc_matrix_model->posts_search_count_proses($search, $style);
		}
		$data = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $master) {
				$_temp = '"' . $master->master_proses_id . '","' . trim($master->proses_name) . '"';
				/*$detail_inline = $this->Qc_matrix_model->detailInlineProses($line_id,$round,$master->style,$master->master_proses_id);*/

				/*$jumlah = ($detail_inline->row_array()['exists']=='t'?5:0);*/

				/*$jumlah=0;
                foreach ($detail_inline->result() as $detail) {
                    $jumlah = $detail->jumlah;
                }*/
				/*$jumlah = ($detail_inline==0?0:5);*/

				$nestedData['no']         = (($draw - 1) * 10) + (++$nomor_urut);
				$nestedData['prosesname'] = ucwords($master->proses_name);
				// $nestedData['lastproses'] = $master->lastproses;
				/*$nestedData['jumlah']     = $jumlah;*/
				$nestedData['action']     =  "<input type='checkbox' name='is_checked[]' id='" . $master->master_proses_id . "' value='" . $master->proses_name . "' class='mycheckbox' data-status='0'>";
				// $nestedData['action']     =  "<button type='button' name='chooseProses' id='".$master->master_proses_id."' value='".$master->proses_name."' class='mycheckbox' data-status='0'>";
				// $nestedData['action']     = "<center><a onclick='return chooseItem($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
				$data[]                   = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function mesin_view()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
		$proses_id = $_GET['id'];
		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$data = array(
				'proses_id' => $proses_id,
			);

			$this->load->view('qc_matrix/list_mesin', $data);
		}
	}

	public function listmesin()
	{
		$columns = array(
			// 0 =>'no',
			0 => 'nama_mesin',
		);
		$limit     = $this->input->post('length');
		$start     = $this->input->post('start');
		$order     = $columns[$this->input->post('order')[0]['column']];
		$dir       = $this->input->post('order')[0]['dir'];
		$draw      = $this->input->post('draw');
		$proses_id = $this->input->post('proses_id');
		// $line_id = $this->input->post('line_id');
		// $round = $this->input->post('round');


		$totalData = $this->Qc_matrix_model->allposts_count_mesin();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Qc_matrix_model->allposts_mesin($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Qc_matrix_model->posts_search_mesin($limit, $start, $search, $order, $dir);

			$totalFiltered = $this->Qc_matrix_model->posts_search_count_mesin($search);
		}
		$data = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $master) {
				$_temp = '"' . $proses_id . '","' . $master->mesin_id . '","' . trim($master->nama_mesin) . '"';
				/*$detail_inline = $this->Qc_matrix_model->detailInlineProses($line_id,$round,$master->style,$master->master_proses_id);*/

				/*$jumlah = ($detail_inline->row_array()['exists']=='t'?5:0);*/

				/*$jumlah=0;
                foreach ($detail_inline->result() as $detail) {
                    $jumlah = $detail->jumlah;
                }*/
				/*$jumlah = ($detail_inline==0?0:5);*/

				$nestedData['no']         = (($draw - 1) * 10) + (++$nomor_urut);
				$nestedData['nama_mesin'] = ucwords($master->nama_mesin);
				/*$nestedData['jumlah']     = $jumlah;*/
				$nestedData['action']     = "<center><a onclick='return pilih($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";
				$data[]                   = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function loadsewerproses()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		$proses_id = $_GET['proses_id'];
		// $header_id = $_GET['header_id'];
		$line_id = $_GET['line_id'];
		$date = date('Y-m-d');


		$proses = str_replace(",", "','", $proses_id);

		$final = "'" . $proses . "'";
		// var_dump($proses_id); var_dump("<br>");
		// var_dump($proses);var_dump("<br>");
		// var_dump($final);var_dump("<br>");
		// die();

		$cek = $this->Qc_matrix_model->getSewerNik($final, $line_id)->result_array();

		echo json_encode($cek);
	}

	public function viewsewerproses()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		$proses_id = $_GET['proses_id'];
		// $header_id = $_GET['header_id'];
		$line_id = $_GET['line_id'];
		$date = date('Y-m-d');

		$proses = str_replace(",", "','", $proses_id);

		$final = "'" . $proses . "'";
		// var_dump($proses_id);
		// var_dump($proses);
		// die();

		$data['sewer'] = $this->Qc_matrix_model->getSewerNik($final, $line_id);


		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$this->load->view('qc_matrix/list_sewer', $data);
		}
	}
	public function savesubmit($style = NULL, $header_id = NULL, $proses = NULL, $qc_nik = NULL, $factory = NULL)
	{
		$post      = $this->input->post();
		// echo json_encode($post);
		// die();
		// $last      = $post['lastproses'];
		// $grade     = $post['grade'];
		$style     = $post['style'];
		$mesin     = $post['mesin'];
		$header_id = $post['header_id'];
		$sewer     = $post['sewer'];
		$wip       = $post['wip'];
		$tc        = $post['tc'];
		$burst     = $post['burst'];
		$proses    = $post['proses'];
		$items     = json_decode($this->input->post('items', TRUE));
		$ie        = $this->session->userdata('nik');
		$factory   = $this->session->userdata('factory');
		$date      = date('Y-m-d');
		

		$cek_batch = $this->db->query("SELECT * FROM skill_matrix_batch where nik_ie = '$ie' 
					and factory_id = '$factory' and finish_date is null")->row();

		

		$cek_header = $this->db->query("SELECT * FROM skill_matrix_header where nik_sewer = '$sewer' 
					and mesin_id = '$mesin' and proses_id = '$proses' and date(create_date) = '$date'")->row();
		
		if ($burst == '') {
			$burst = null;
		}

		if ($cek_batch == null) {
			$data_batch = array(
				'id' => $this->uuid->v4(),
				'line_id'          => $post['line_id'],
				'factory_id'       => $factory,
				'style'            => $style,
				'nik_ie'           => $ie,
				'create_date'      => date('Y-m-d H:i:s'),
			);

			$this->db->insert('skill_matrix_batch', $data_batch);

			$batch_id = $data_batch['id'];
		} else {
			$batch_id = $cek_batch->id;
		}

		if ($cek_header == null) {
			$data_header = array(
				'matrix_header_id' => $this->uuid->v4(),
				'line_id'          => $post['line_id'],
				'factory_id'       => $factory,
				'style'            => $style,
				'proses_id'        => $proses,
				'mesin_id'         => $mesin,
				'nik_sewer'        => $sewer,
				'nik_ie'           => $ie,
				'wip'              => $wip,
				'burst'            => $burst,
				'jenis_wip'        => $tc,
				'create_date'      => date('Y-m-d H:i:s'),
				'matrix_batch_id'  => $batch_id,
			);

			$this->db->insert('skill_matrix_header', $data_header);

			$matrix_header = $data_header['matrix_header_id'];
		} else {
			$matrix_header = $cek_header->matrix_header_id;
		}

		// var_dump($matrix_header);
		// die();
		$this->db->limit(1);
		$this->db->order_by('create_date', 'desc');
		$this->db->where('matrix_header_id', $matrix_header);
		$query  =   $this->db->get('skill_matrix_detail')->row_array();
		// $round = $this->Qc_matrix_model->maxheader($post['line_id'])[0]->round;
	

		$cek_round = isset($query['round']);

		if (!empty($style) || !empty($header_id) || !empty($proses) || !empty($mesin)) {
			// if ($last=='t') {

			if ($cek_round == null) {
				$i      = 0;
				$hitung = 0;
				if (!empty($items)) {
					foreach ($items as $key => $item) {
						$data[$i]['matrix_detail_id'] = $this->uuid->v4();;
						$data[$i]['matrix_header_id'] = $matrix_header;
						$data[$i]['round']            = $i + 1;
						$data[$i]['create_date']      = $item->record;
						$data[$i]['durasi']           = $item->interval;

						$hitung = $hitung + $item->interval;
						$i++;
					}
				}
				$this->db->insert_batch('skill_matrix_detail', $data);

				// $json = str_replace(array("\t","\n"), "", $_POST["waktu"]);
				// $data = json_decode($json);
				// $hitung=0;
				// foreach ($data as $key => $item) {
				//     // $detail[$i]['matrix_detail_id'] = $this->uuid->v4();
				//     // $detail[$i]['matrix_header_id'] = $matrix_header;
				//     // $detail[$i]['round']            = $i+1;
				//     // $detail[$i]['waktu']            = "(to_timestamp('$item->time_id', 'hh24:mi:ss:ms'))";
				//     // $detail[$i]['create_date']      = date('Y-m-d H:i:s');

				//     $matrix_detail_id = $this->uuid->v4();
				//     $matrix_header_id = $matrix_header;
				//     $round            = $item[0];
				//     $waktu            = $item[1];
				//     $create_date      = $item[2];

				// $hitung = $hitung + $waktu;
				// $i++;

				//     $this->db->query("INSERT INTO skill_matrix_detail
				//     (matrix_detail_id, matrix_header_id, round, waktu, create_date, durasi) 
				//     VALUES ('$matrix_detail_id', '$matrix_header_id', 
				//     $round, (to_timestamp('$waktu', 'ss:ms')), '$create_date', '$waktu')");
				// }

				if ($i == 0) {
					$rata  = 0;
					$hasil = 0;
				} else {
					$rata  = (float)$hitung / $i;
					$hasil = number_format($rata, 2);
				}

				$data_update = array(
					'rata_durasi' => $hasil,
				);
				$this->db->where('matrix_header_id', $matrix_header);
				$this->db->update('skill_matrix_header', $data_update);
				// $this->db->insert_batch('skill_matrix_detail', $detail);
				// $this->db->insert('round_status', $data);

				$notif = "sukses";
			} else {
				$notif = "gagal";
			}
		} else {
			$notif = "gagal";
		}

		$data = array(
			'notif' => $notif,
			// 'round' => $roundss,
			'nik'   => $sewer
		);
		echo json_encode($data);
	}
	// private function insert_capinline($date,$line_id,$round,$proses,$sewer)
	// {
	//     $this->db->where(array('create_date'=> $date,'line_id' => $line_id,'color'=>'red','round'=>$round,'master_proses_id'=>$proses,'sewer_nik'=>$sewer));
	//     $cap_inline = $this->db->get('listinspection_view');
	//     $cap_id = array();
	//     $_line  = array();
	//     $detail = array();
	//     $date   = date('Y-m-d');
	//     foreach ($cap_inline->result() as $key => $cap) {
	//        $_cap_id = $this->uuid->v4();
	//        $line_id = $cap->line_id;

	//        $defect_jenis= str_replace('No Defect', '',$cap->defect_jenis);

	//        $detail = array(
	//         'cap_inline_id' => $_cap_id, 
	//         'line_id'       => $line_id, 
	//         'factory_id'    => $cap->factory_id, 
	//         'line_name'     => $cap->line_name, 
	//         'round'         => $cap->round, 
	//         'sewer_nik'     => $cap->sewer_nik, 
	//         'sewer_name'    => $cap->sewer_name, 
	//         'qc_name'       => $cap->qc_name, 
	//         'proses_name'   => $cap->proses_name, 
	//         'defect_id'     => $cap->defect_id,
	//         'defect_jenis'  => $defect_jenis
	//     ); 
	//         $cap_id [] = $_cap_id;
	//         $_line []  = $line_id;                      
	//     }
	//     if (count($detail)!=0) {
	//        $this->db->insert('cap_inline', $detail);
	//     }

	//     if (count($_line)!=0) {
	//         $this->db->where_not_in('line_id', $_line);
	//         $this->db->where_not_in('cap_inline_id', $cap_id);
	//     }
	//     $this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
	//     $this->db->where('status', 'cap verify');
	//     $this->db->update('cap_inline', array('status'=>'close'));
	// }

	public function search_smv($id = NULL, $style = NULL)
	{
		$proses_id = $_GET['id'];
		$style     = $_GET['style'];
		// var_dump($proses_id);
		// var_dump($style);
		// $proses = explode(',',$proses_id);
		// $proses = str_replace(",","','",$proses_id);

		// var_dump($proses_id);
		// die();

		$cek = $this->db->query("SELECT * from master_workflow where style = '$style' and master_proses_id in ($proses_id)");
		// $this->db->where_in('master_proses_id',$proses);
		// $this->db->where('style',$style);
		// $_cek  = $this->db->get('master_workflow');
		// = $this->db->get_where('master_workflow',array('master_proses_id'=>$proses_id,'style'=>$style));
		$smv = 0;


		if ($cek->num_rows() > 0) {
			// echo $_cek->result()[0]->smv_spt;
			if ($cek->num_rows() > 1) {
				$cek_smv = $cek->result();
				foreach ($cek_smv as $value) {
					$smv = $smv + (float)$value->cycle_time;
				}
			} else {
				$smv = $smv + (float)$cek->result()[0]->cycle_time;
			}

			echo $smv;
		} else {
			echo "-";
		}
	}

	public function close_matrix_job()
	{

		$get      = $this->input->get();

		$batch_id = $get['batch_id'];

		$data_batch = array(
			'finish_date' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id', $batch_id);
		$this->db->update('skill_matrix_batch', $data_batch);
		// $this->db->insert('skill_matrix_batch', $data_batch);

		echo "success";
	}

	public function searchnik($nik = NULL)
	{
		$nik  = $_GET['id'];
		$_cek = $this->db->get_where('master_sewer', array('nik' => $nik));
		if ($_cek->num_rows() > 0) {
			echo $_cek->result()[0]->name;
		} else {
			echo "gagal";
		}
	}
	public function stopwatch()
	{
		$this->load->view('qc_matrix/stopwatch');
	}

	public function report_matrix()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$factory   = $this->session->userdata('factory');
			$ml        = $this->db->query("SELECT * FROM master_line where factory_id = '$factory' order by no_urut");
			$m_style   = $this->db->query("SELECT DISTINCT style from inline_header where factory_id = '$factory' and date(create_date) >= '2020-01-01'");

			$data['ml']      = $ml;
			$data['m_style'] = $m_style;
			$data['tanggal'] = $this->input->post('tanggal', TRUE);
			// $data['defect_id'] =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();
			$this->template->set('title', 'Report Matrix');
			$this->template->set('desc_page', 'Report Matrix');
			$this->template->load('layout', 'report/skill_matrix/index_report_matrix', $data);
		}
	}

	// public function pencarian_matrix($post=NULL)
	// {
	// 	$post 	= $this->input->post();

	// 	if ($post!=NULL) {

	// 		$tgl1    = $post['tanggal1'];
	// 		$tgl2    = $post['tanggal2'];

	// 		$data = array (
	// 			'tgl1'    => $tgl1,
	// 			'tgl2'    => $tgl2,
	// 		);

	// 		$this->load->view("report/skill_matrix/list_report_matrix",$data);

	// 		// echo "sukses";
	// 	}
	// }

	public function export_report_skill_matrix()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$factory = $this->session->userdata('factory');
		$nik     = $this->session->userdata('nik');
		
		$dari   = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		
		$query = $this->db->query("SELECT * from master_sewer where nik = '$nik'")->row_array();
		if ($query['department_name'] == 'CI/IE' && $query['job_desc'] == 'Staff') {
			$remark = 'single';
		} else {
			$remark = 'allin';
		}

		$line   = $this->input->get('line');
		$style  = $this->input->get('style');

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('ICT Team')
			->setLastModifiedBy('ICT Team')
			->setTitle("Data APT")
			->setSubject("Skill Matrix")
			->setDescription("Laporan APT")
			->setKeywords("Data APT");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Set text jadi ditengah secara horizontal (center)
				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER      // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER  // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA APT"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:P1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);  // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3


		$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
		// $excel->getActiveSheet()->mergeCells('A2:A3'); // Set kolom A3 dengan tulisan "NO"
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('B2', "DATE");
		// $excel->getActiveSheet()->mergeCells('B2:B3'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('C2', "LINE");
		// $excel->getActiveSheet()->mergeCells('C2:C3');  // Set kolom B3 dengan tulisan "NIS"
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('D2', "STYLE");
		// $excel->getActiveSheet()->mergeCells('D2:D3');  // Set kolom C3 dengan tulisan "NAMA"
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('E2', "NAMA PROSES");
		// $excel->getActiveSheet()->mergeCells('E2:E3');  // Set kolom D3 dengan tulisan "JENIS KELAMIN"
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('F2', "NAMA MESIN");
		// $excel->getActiveSheet()->mergeCells('F2:F3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('G2', "NIK OPERATOR");
		// $excel->getActiveSheet()->mergeCells('G2:G3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('H2', "NAMA OPERATOR");
		// $excel->getActiveSheet()->mergeCells('H2:H3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('I2', "NIK IE");
		// $excel->getActiveSheet()->mergeCells('I2:I3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('J2', "NAMA IE");
		// $excel->getActiveSheet()->mergeCells('J2:J3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('K2', "WIP");
		// $excel->getActiveSheet()->mergeCells('K2:K3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('L2', "JENIS WIP");
		// $excel->getActiveSheet()->mergeCells('L2:L3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('M2', "STICHING BURST");
		// $excel->getActiveSheet()->mergeCells('M2:M3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('N2', "ROUND");
		// $excel->getActiveSheet()->mergeCells('N2:N3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('O2', "CYCLE TIME");
		// $excel->getActiveSheet()->mergeCells('O2:O3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom A

		// $excel->setActiveSheetIndex(0)->setCellValue('P2', "WAKTU");
		// // $excel->getActiveSheet()->mergeCells('P2:P3');  // Set kolom E3 dengan tulisan "ALAMAT"
		// $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('P2', "DURASI");
		// $excel->getActiveSheet()->mergeCells('Q2:Q3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom A


		// Apply style header yang telah kita buat tadi ke masing-masing kolom header

		$excel->getActiveSheet()->getStyle('A2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('P2')->applyFromArray($style_col);
		// $excel->getActiveSheet()->getStyle('Q2')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		// $siswa = $this->SiswaModel->view();

		// $tanggal =$this->input->get('tanggal');
		// $detail_report='';

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		$details = $this->Qc_matrix_model->download_skill_matrix($line, $style, $factory, $remark, $dari, $sampai);


		$no = 0; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 3; // Set baris pertama untuk isi tabel adalah baris ke 4
		$smh = '';
		foreach ($details->result() as $key => $detail) {
			// $rft  = (($detail->total_checked-$detail->count_defect)/$detail->total_checked)*100;
			// foreach($siswa as $data){ // Lakukan looping pada variabel siswa
			if ($smh == $detail->matrix_header_id) {
			} else {
				$no++; // Tambah 1 setiap kali looping
			}
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $detail->tgl);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $detail->line_name);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $detail->style);
			// if ($detail->proses_name == null) {
			// 	// var_dump($detail->proses_name);
			// 	$nama = '';

			// 	$query = $this->db->query("SELECT * from master_proses where master_proses_id in ($detail->proses_id)")->result_array();
			// 	foreach ($query as $q) {
			// 		$nama .= "" . $q['proses_name'] . ", ";
			// 	}


			// 	$prosesname = rtrim($nama, ', ');
			// 	$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $prosesname);
			// } else {
			// 	$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $detail->proses_name);
			// }
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $detail->proses_name);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $detail->nama_mesin);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $detail->nik_sewer);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $detail->nama_sewer);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $detail->nik_ie);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $detail->nama_ie);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $detail->wip);
			if ($detail->jenis_wip == 't') {
				$jenis = 'ASSEMBLY';
			} else {
				$jenis = 'PRE ASSEMBLY';
			}
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $jenis);
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $detail->burst);
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $detail->round);
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $detail->cycle_time);
			$excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $detail->durasi);
			//   $excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, $detail->durasi);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
			//   $excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);

			$smh = $detail->matrix_header_id;
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom E
		// $excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom E

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Laporan Skill Matrix");
		$excel->setActiveSheetIndex(0);

		// Proses file excel
		// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="REPORT APT".xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		// $filename = 'Skill_Matrix_AOI_'.$factory.'.xlsx';
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		// $write->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
		// $path = $_SERVER["DOCUMENT_ROOT"]."/line_system/assets/excel/";
		// $write->save($path.$fileName);
		// $write->save(str_replace(__FILE__,''.$path.'/'.$filename,__FILE__));
		$write->save('php://output');
	}

	function upload_matrix()
	{
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		if ($ext != 'xlsx') {
			$status = 200;
		} else {
			$data = $_FILES['file'];
			include $_SERVER["DOCUMENT_ROOT"] . '/line_system/library/Classes/PHPExcel/IOFactory.php';
			$path = $_SERVER["DOCUMENT_ROOT"] . "/line_system/file/";
			if (
				$data["type"] != 'application/kset' &&
				$data["type"] != 'application/vnd.ms-excel' &&
				$data["type"] != 'application/xls' &&
				$data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
				$data["type"] != 'application/ms-excel'
			) {

				$status = 200;
			} else {
				$datas         = move_uploaded_file($data["tmp_name"], $path . $data['name']);
				$inputFileName = $path . $data['name'];
				$objPHPExcel   = PHPExcel_IOFactory::load($inputFileName);
				$sheetCount    = $objPHPExcel->getSheetCount();
				$sheet         = $objPHPExcel->getSheetNames();
				$sheetData     = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
				$data_excel    = array();
				foreach ($sheetData as $a => $b) {
					if ($a > 1) {
						$tgl          = $b['A'];
						$line         = $b['B'];
						$style        = $b['C'];
						$nama_proses  = $b['D'];
						$kode_mesin   = $b['E'];
						$nik_operator = $b['F'];
						$nik_ie       = $b['G'];
						$wip          = $b['H'];

						$array = array(
							'tgl'          => $tgl,
							'line'         => $line,
							'style'        => $style,
							'nama_proses'  => $nama_proses,
							'kode_mesin'   => $kode_mesin,
							'nik_operator' => $nik_operator,
							'nik_ie'       => $nik_ie,
							'wip'          => $wip,
						);
						$excel[] = $array;
					}
				}

				foreach ($excel as $a => $b) {
					// var_dump(!empty($b));
					if (!empty($b)) {

						// var_dump($b['tgl']);
						$colom1 = "'" . $b['tgl'] . "'";
						$colom2 = $b['line'];
						$colom3 = $b['style'];
						$colom4 = $b['nama_proses'];
						$colom5 = $b['nik_operator'];
						$colom6 = $b['nik_ie'];
						$colom7 = $b['wip'];

						$query = $this->db->query("SELECT
                            smh.matrix_header_id,
                            DATE ( smh.create_date ) AS tgl,
                            ml.line_name,
                            smh.STYLE,
                            mp.proses_name,
                            mm.nama_mesin,
                            smh.nik_sewer,
                            mss.NAME AS nama_sewer,
                            smh.nik_ie,
                            msi.NAME AS nama_ie,
                            smh.wip,
                            smh.jenis_wip,
                            smd.round,
                            smd.durasi 
                        FROM
                            skill_matrix_header smh
                            LEFT JOIN skill_matrix_detail smd ON smd.matrix_header_id = smh.matrix_header_id
                            LEFT JOIN master_line ml ON ml.master_line_id = smh.line_id
                            LEFT JOIN master_sewer mss ON mss.nik = smh.nik_sewer
                            LEFT JOIN master_sewer msi ON msi.nik = smh.nik_ie
                            LEFT JOIN master_proses mp ON mp.master_proses_id::varchar = smh.proses_id
                            LEFT JOIN master_mesin mm ON mm.mesin_id = smh.mesin_id 
                        WHERE
                            date(smh.create_date) = " . $colom1 . "
                            AND smh.factory_id = '2'
                            AND ml.line_name = '" . $colom2 . "'
                            AND smh.style = '" . $colom3 . "'
                            AND mp.proses_name = '" . $colom4 . "'
                            AND smh.nik_sewer = '" . $colom5 . "'
                            AND smh.nik_ie = '" . $colom6 . "'
                            AND smh.wip = '" . $colom7 . "'
                        ORDER BY
                            smh.create_date,
                            smd.round
                        ")->result();

						$kode_mesin = $b['kode_mesin'];
						// var_dump($query);
						foreach ($query as $q) {
							$header = $q->matrix_header_id;
							// var_dump($header);
							$this->db->where('matrix_header_id', $header);
							$this->db->update('skill_matrix_header', array('mesin_id' => $kode_mesin));
						}
					} else {
						$status = 200;
					}
				}
				$status = 100;
			}
		}

		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$data['tanggal'] = $this->input->post('tanggal', TRUE);
			$data['defect_id'] = $this->db->get_where('master_defect', array('defect_id<>' => 0))->result();
			$this->template->set('title', 'Report Matrix');
			$this->template->set('desc_page', 'Report Matrix');
			$this->template->load('layout', 'report/skill_matrix/index_report_matrix', $data);
		}
		// $this->load->view("ImportBoard",$datas);
	}

	public function report_complexity()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {

			$factory   = $this->session->userdata('factory');
			$ml        = $this->db->query("SELECT * FROM master_line where factory_id = '$factory' order by no_urut");
			$mesin     = $this->db->query("SELECT * FROM master_mesin order by mesin_id");
			$m_proses  = $this->db->query("SELECT DISTINCT smh.proses_id, mp.proses_name from skill_matrix_header smh
				LEFT JOIN master_proses mp on mp.master_proses_id::varchar = smh.proses_id
				WHERE proses_name is not null");
			$data['tanggal'] = $this->input->post('tanggal', TRUE);
			$data['ml']      = $ml;
			$data['mesin']   = $mesin;
			$data['m_proses']   = $m_proses;
			$this->template->set('title', 'Report Complexity');
			$this->template->set('desc_page', 'Report Complexity');
			$this->template->load('layout', 'report/skill_matrix/index_report_complexity', $data);
		}
	}

	public function pencarian_matrix($post = NULL)
	{
		$post 	= $this->input->post();

		if ($post != NULL) {

			// $line   = $post['line'];
			// $mesin  = $post['mesin'];
			// $proses = $post['proses'];
			 $nik = $post['nik'];
			
			$data = array(
				// 'line'   => $line,
				// 'mesin'  => $mesin,
				// 'proses' => $proses,
				'nik' => $nik
			);
			$data['profil'] = $this->Qc_matrix_model->sewer_profil($nik);

			$this->load->view("report/skill_matrix/list_report_matrix", $data);

			// echo "sukses";
		}
	}

	public function sewer_profil()
	{
		$nik = $this->input->post('nik');
		$data['profil'] = $this->Qc_matrix_model->sewer_profil($nik);
		echo json_encode($data);
	}

	public function data_matrix($post = NULL)
	{
		$columns = array(
			0 => 'line_name',
			8 => 'bintang'
		);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];

		// $limit = 0;
		// $start = 0;
		// $order = 0;
		// $dir   = 0;


		$factory = $this->session->userdata('factory');
		// $mesin   = $this->input->post('mesin');
		// $line    = $this->input->post('line');
		// $proses  = $this->input->post('proses');
		 $nik  = $this->input->post('nik');

		// if ($dari!='') {
		// 	$dari = date('Y-m-d',strtotime($dari));
		// }else{
		// 	$dari = 0;
		// }

		// if ($sampai!='') {
		// 	$sampai = date('Y-m-d',strtotime($sampai));
		// }else{
		// 	$sampai = 0;
		// }

		// $totalData = $this->Qc_matrix_model->allpost_count_data_matrix($factory, $line, $mesin, $proses);

		$totalData = $this->Qc_matrix_model->allpost_count_data_matrix($factory, $nik);
		$totalFiltered = $totalData;

		if (empty($search)) {
			$master = $this->Qc_matrix_model->allposts_data_matrix($limit, $start, $order, $dir, $factory, $nik);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Qc_matrix_model->posts_search_data_matrix($limit, $start, $search, $order, $dir, $factory, $nik);

			$totalFiltered = $this->Qc_matrix_model->posts_search_count_data_matrix($search, $factory, $nik);
		}
		$data = array();

		$nomor_urut = 0;
		if (!empty($master)) {

			foreach ($master as $master) {
				// if($master->proses_name == null){
				// 	$nama  = '';
				// 	$cycle = 0;

				// 	$query = $this->db->query("SELECT * from workflow_view where master_proses_id in ($master->proses_id)")->result_array();
				// 	foreach ($query as $q) {
				// 		$nama .= "".$q['proses_name'].", ";
				// 		$cycle = $cycle + $q['cycle_time'];
				// 	}
				// 	$prosesname= rtrim($nama, ', ');
				// }
				// else{
				// 	$prosesname = $master->proses_name;
				// 	// $cycle = $master->cycle_time;
				// }

				if ($master->bintang == 0) {
					$star = '☆';
				} else if ($master->bintang == 1) {
					$star = '★';
				} else if ($master->bintang == 2) {
					$star = '★★';
				} else if ($master->bintang == 3) {
					$star = '★★★';
				} else {
					$star = '☆';
				}

				$nestedData['line_name']            = $master->line_name;
				$nestedData['operation_complexity'] = $master->operation_complexity;
				$nestedData['proses_name']          = $master->proses_name;
				$nestedData['category_process']     = $master->kategori_proses;
				$nestedData['performance_score']    = $master->performance;
				$nestedData['quality_score']        = $master->inline;
				$nestedData['bintang']              = $star;
				$nestedData['star']                 = $master->bintang;

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}
	public function data_lastline($post = NULL)
	{
		$columns = array(
			0 => 'line_name',
			1 => 'date'
		);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];

		// $limit = 0;
		// $start = 0;
		// $order = 0;
		// $dir   = 0;


		$factory = $this->session->userdata('factory');
		// $mesin   = $this->input->post('mesin');
		// $line    = $this->input->post('line');
		// $proses  = $this->input->post('proses');
		 $nik  = $this->input->post('nik');

		// if ($dari!='') {
		// 	$dari = date('Y-m-d',strtotime($dari));
		// }else{
		// 	$dari = 0;
		// }

		// if ($sampai!='') {
		// 	$sampai = date('Y-m-d',strtotime($sampai));
		// }else{
		// 	$sampai = 0;
		// }

		// $totalData = $this->Qc_matrix_model->allpost_count_data_matrix($factory, $line, $mesin, $proses);

		$totalData = $this->Qc_matrix_model->allpost_count_last_line($factory, $nik);
		$totalFiltered = $totalData;

		if (empty($search)) {
			$master = $this->Qc_matrix_model->allposts_last_line($limit, $start, $order, $dir, $factory, $nik);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Qc_matrix_model->posts_search_last_line($limit, $start, $search, $order, $dir, $factory, $nik);

			$totalFiltered = $this->Qc_matrix_model->posts_search_count_last_line($search, $factory, $nik);
		}
		$data = array();

		$nomor_urut = 0;
		if (!empty($master)) {

			foreach ($master as $master) {
				// if($master->proses_name == null){
				// 	$nama  = '';
				// 	$cycle = 0;

				// 	$query = $this->db->query("SELECT * from workflow_view where master_proses_id in ($master->proses_id)")->result_array();
				// 	foreach ($query as $q) {
				// 		$nama .= "".$q['proses_name'].", ";
				// 		$cycle = $cycle + $q['cycle_time'];
				// 	}
				// 	$prosesname= rtrim($nama, ', ');
				// }
				// else{
				// 	$prosesname = $master->proses_name;
				// 	// $cycle = $master->cycle_time;
				// }
				$newDate     = date("d-m-Y", strtotime($master->date));
				$nestedData['line_name'] = $master->line_name;
				$nestedData['date']      = $newDate;
	
				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}
	public function export_report_complexity_level()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$factory = $this->session->userdata('factory');

		$dari   = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$line   = $this->input->get('line');
		$mesin  = $this->input->get('mesin');
		$proses = $this->input->get('proses');
		// $dari   = ($dari!=''?$dari:date('Y-m-d'));
		// $sampai = ($sampai!=''?$sampai:date('Y-m-d'));

		// $tgl     = date('2020-03-12'); //Y-m-d');
		// $dari    = date('2020-03-12'); //Y-m-d'); //,strtotime('-7 days'));
		// $sampai  = $tgl;
		// $factory = $factory_id;

		// var_dump($dari);
		// var_dump($sampai);
		// die();

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('ICT Team')
			->setLastModifiedBy('ICT Team')
			->setTitle("Data  Skill Based" . $dari . " sd " . $sampai . "")
			->setSubject(" Skill Based")
			->setDescription("Laporan  Skill Based " . $dari . " sd " . $sampai . "")
			->setKeywords("Data  Skill Based");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Set text jadi ditengah secara horizontal (center)
				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER      // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER  // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA SKILL BASED " . $dari . " sd " . $sampai . ""); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:K1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);  // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3


		$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
		// $excel->getActiveSheet()->mergeCells('A2:A3'); // Set kolom A3 dengan tulisan "NO"
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('B2', "NIK");
		// $excel->getActiveSheet()->mergeCells('C2:C3');  // Set kolom B3 dengan tulisan "NIS"
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('C2', "NAMA");
		// $excel->getActiveSheet()->mergeCells('D2:D3');  // Set kolom C3 dengan tulisan "NAMA"
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('D2', "OPERATION COMPLEXITY LEVEL");
		// $excel->getActiveSheet()->mergeCells('E2:E3');  // Set kolom D3 dengan tulisan "JENIS KELAMIN"
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('E2', "OPERATION COMPLEXITY");
		// $excel->getActiveSheet()->mergeCells('F2:F3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('F2', "PRODUCT TYPE");
		// $excel->getActiveSheet()->mergeCells('G2:G3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('G2', "CATEGORY PROCESS");
		// $excel->getActiveSheet()->mergeCells('H2:H3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom Am A

		$excel->setActiveSheetIndex(0)->setCellValue('H2', "LINE");
		// $excel->getActiveSheet()->mergeCells('N2:N3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('I2', "PERFORMANCE SCORE");
		// $excel->getActiveSheet()->mergeCells('N2:N3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('J2', "QUALITY SCORE");
		// $excel->getActiveSheet()->mergeCells('O2:O3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('K2', "TOTAL STAR PER PROCESS");
		// $excel->getActiveSheet()->mergeCells('P2:P3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom Aet width kolom A



		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K2')->applyFromArray($style_col);
		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		// $siswa = $this->SiswaModel->view();

		// $tanggal =$this->input->get('tanggal');
		// $master_report='';

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		$details = $this->Qc_matrix_model->operation_complexity_level($dari, $sampai, $factory, $line, $mesin, $proses);


		$no = 0; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 3; // Set baris pertama untuk isi tabel adalah baris ke 4
		$smh = '';
		foreach ($details->result() as $key => $detail) {

			$no++;
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
			//   $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $detail->tgl);
			//   $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $detail->line_name);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $detail->nik_sewer);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, strtoupper($detail->nama_sewer));
			if ($detail->proses_name == null) {
				// var_dump($detail->proses_name);
				$nama  = '';
				$cycle = 0;

				$query = $this->db->query("SELECT * from workflow_view where master_proses_id in ($detail->proses_id)")->result_array();
				foreach ($query as $q) {
					$nama .= "" . $q['proses_name'] . ", ";
					$cycle = $cycle + $q['cycle_time'];
				}


				$prosesname = rtrim($nama, ', ');
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $prosesname);
			} else {
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $detail->proses_name);
				$cycle = $detail->cycle_time;
			}

			$operation_complexity = '';
			$kategori_proses      = '';
			//   var_dump($detail->is_critical);
			//   die();
			if ($detail->is_critical == 't') {
				$kategori_proses = 'CRITICAL';
				if ($detail->kategori_mesin == 'SPECIAL') {
					if ($detail->cycle_time > 45) {
						$operation_complexity = 'A';
					} else if ($detail->cycle_time >= 30 && $detail->cycle_time <= 45) {
						$operation_complexity = 'A';
					} else if ($detail->cycle_time < 30) {
						$operation_complexity = 'B';
					} else {
						$operation_complexity = 'undefined';
					}
				} else if ($detail->kategori_mesin == 'NORMAL') {
					if ($detail->cycle_time > 45) {
						$operation_complexity = 'A';
					} else if ($detail->cycle_time >= 30 && $detail->cycle_time <= 45) {
						$operation_complexity = 'B';
					} else if ($detail->cycle_time < 30) {
						$operation_complexity = 'B';
					} else {
						$operation_complexity = 'undefined';
					}
				} else {
					$operation_complexity = 'undefined';
				}
			} else if ($detail->is_critical == 'f') {

				$kategori_proses = 'BASIC';
				if ($detail->kategori_mesin == 'SPECIAL') {
					if ($detail->cycle_time > 45) {
						$operation_complexity = 'A';
					} else if ($detail->cycle_time >= 30 && $detail->cycle_time <= 45) {
						$operation_complexity = 'B';
					} else if ($detail->cycle_time < 30) {
						$operation_complexity = 'B';
					} else {
						$operation_complexity = 'undefined';
					}
				} else if ($detail->kategori_mesin == 'NORMAL') {
					if ($detail->cycle_time > 45) {
						$operation_complexity = 'B';
					} else if ($detail->cycle_time >= 30 && $detail->cycle_time <= 45) {
						$operation_complexity = 'C';
					} else if ($detail->cycle_time < 30) {
						$operation_complexity = 'C';
					} else {
						$operation_complexity = 'undefined';
					}
				} else {
					$operation_complexity = 'undefined';
				}
			} else {
				$operation_complexity = 'undefined';
			}

			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $operation_complexity);

			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, '-');
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $kategori_proses);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $detail->line_name);

			//   $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $detail->rata_durasi);
			//   $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $detail->cycle);


			//   $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $detail->kategori_mesin);
			//   $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $kategori_proses);
			//   $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $operation_complexity);

			if ($detail->rata_durasi == 0) {
				$performance = 0;
			} else {
				$performance = $detail->cycle_time / $detail->rata_durasi;
			}
			$persen_performance = (number_format($performance, 2)) * 100;
			$hasil_performance  = $persen_performance . "%";

			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $hasil_performance);

			//   $detail = $this->db->query("SELECT min(master_grade_id) as terbaik
			//   FROM inlinedetail_view WHERE sewer_nik = '$detail->nik_sewer' and master_proses_id::VARCHAR = '$detail->proses_id'
			//   ")->row_array();

			if ($detail->terbaik == null) {
				$nilai_inline  = 'undefined';
				$persen_inline = 'undefined';
				$hasil_inline  = $persen_inline;
			} else {
				if ($detail->terbaik == '1') {
					$nilai_inline  = 'A';
					$persen_inline = 100;
					$hasil_inline  = $persen_inline . "%";
				} else if ($detail->terbaik == '2') {
					$nilai_inline  = 'B';
					$persen_inline = 60;
					$hasil_inline  = $persen_inline . "%";
				} else if ($detail->terbaik == '3') {
					$nilai_inline  = 'C';
					$persen_inline = 0;
					$hasil_inline  = $persen_inline . "%";
				} else {
					$nilai_inline  = 'undefined';
					$persen_inline = 'undefined';
					$hasil_inline  = $persen_inline;
				}
			}

			if ($operation_complexity == 'A' || $operation_complexity == 'B') {
				if ($persen_performance >= 90 && $persen_inline == 100) {
					$bintang = 3;
				} else if ($persen_performance >= 75 && $persen_inline == 100) {
					$bintang = 2;
				} else {
					$bintang = 0;
				}
			} else if ($operation_complexity == 'C') {
				if ($persen_performance >= 75 && $persen_inline == 100) {
					$bintang = 1;
				} else {
					$bintang = 0;
				}
			} else {
				$bintang = 0;
			}

			if ($bintang == 0) {
				$star = '☆';
			} else if ($bintang == 1) {
				$star = '★';
			} else if ($bintang == 2) {
				$star = '★★';
			} else if ($bintang == 3) {
				$star = '★★★';
			} else {
				$star = '☆';
			}

			//   $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $nilai_inline);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $hasil_inline);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $star);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			//   $smh = $detail->matrix_header_id;
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom E

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Laporan Skill Based");
		$excel->setActiveSheetIndex(0);

		// Proses file excel
		// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="REPORT SKILL BASED "' . $dari . '" sd "' . $sampai . '".xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		// $filename = 'Skill_Matrix_AOI_'.$factory.'.xlsx';
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		// $write->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
		// $path = $_SERVER["DOCUMENT_ROOT"]."/line_system/assets/excel/";
		// $write->save($path.$fileName);
		// $write->save(str_replace(__FILE__,''.$path.'/'.$filename,__FILE__));
		$write->save('php://output');
	}


	public function rata()
	{
		$query = $this->db->query("SELECT * from skill_matrix_header")->result();

		foreach ($query as $q) {
			$cek     = $this->db->query("SELECT * from skill_matrix_detail where matrix_header_id = '$q->matrix_header_id'")->result_array();
			$panjang = count($cek);
			$hitung  = 0;
			foreach ($cek as $c) {
				$hitung = $hitung + $c['durasi'];
			}
			if ($panjang == 0) {
				$rata  = 0;
				$hasil = 0;
			} else {
				$rata  = (float)$hitung / $panjang;
				$hasil = number_format($rata, 2);
			}

			$data = array(
				'rata_durasi' => $hasil,
			);
			$this->db->where('matrix_header_id', $q->matrix_header_id);
			$this->db->update('skill_matrix_header', $data);
		}
	}

	
	public function report_line_balancing()
	{
		// $cek_permision = $this->Cekpermision_model->cekpermision(2);

		// if ($cek_permision == 0) {
		// 	redirect('error_page', 'refresh');
		// } else {
			$factory   = $this->session->userdata('factory');
			$ml        = $this->db->query("SELECT * FROM master_line where factory_id = '$factory' order by no_urut");
			$m_style   = $this->db->query("SELECT DISTINCT style from inline_header where factory_id = '$factory' and date(create_date) >= '2020-01-01'");

			$data['ml']      = $ml;
			$data['m_style'] = $m_style;
			$data['tanggal'] = $this->input->post('tanggal', TRUE);
			// $data['defect_id'] =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();
			$this->template->set('title', 'Report Matrix');
			$this->template->set('desc_page', 'Report Matrix');
			$this->template->load('layout', 'report/skill_matrix/index_report_line_balancing', $data);
		// }
	}

	public function data_line_balancing($post = NULL)
	{
		$columns = array(
			0 => 'no_urut',
			1 => 'nik_ie',
			3 => 'style',
			4 => 'create_date',
			5 => 'finish_date'
		);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];

		$factory = $this->session->userdata('factory');
		$line    = $this->input->post('line');
		$style   = $this->input->post('style');
		$nik     = $this->session->userdata('nik');
		
		$query = $this->db->query("SELECT * from master_sewer where nik = '$nik'")->row_array();
		if ($query['department_name'] == 'CI/IE' && $query['job_desc'] == 'Staff') {
			$remark = 'single';
		} else {
			$remark = 'allin';
		}

		$totalData     = $this->Qc_matrix_model->allpost_count_data_line_balancing($factory, $remark, $line, $style);
		$totalFiltered = $totalData;
		$master        = $this->Qc_matrix_model->allposts_data_line_balancing($limit, $start, $order, $dir, $factory, $remark, $line, $style);
		
		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {

			foreach ($master as $master) {

				$temp = '"'.$master->style.'","'.$master->line_id.'","'.$master->id.'"';
				$nestedData['line_name']   = $master->line_name;
				$nestedData['nik_ie']      = $master->nik_ie;
				$nestedData['nama_ie']     = $master->name;
				$nestedData['style']       = $master->style;
				$nestedData['create_date'] = $master->create_date;
				$nestedData['finish_date'] = $master->finish_date;
				$nestedData['action']      = "<center><a onclick='return report_lb($temp)' href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-file'></i> Download</a> </center>";
				

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	// public function line_balancing($style, $line, $matrix_batch_id)
	public function line_balancing()
	{
		$style           = $this->input->get('style');
		$line            = $this->input->get('line');
		$matrix_batch_id = $this->input->get('id');
		
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excel = new PHPExcel();

		$excel->getProperties()->setCreator('ICT Team')
			->setLastModifiedBy('ICT Team')
			->setTitle("Data Line Balancing")
			->setSubject("Line Balancing")
			->setDescription("Laporan Line Balancing")
			->setKeywords("Data Line Balancing");
			
		$excel->getActiveSheet(0)->setTitle("Data");

		$style_col = array(
			'font' => array('bold' => true),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
			)
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "LINE BALANCING");
		$excel->getActiveSheet()->mergeCells('A1:Q1');
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "PRE ASSEMBLY");
		$excel->getActiveSheet()->mergeCells('A2:Q2');
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15);
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('B3', "MESIN");
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('C3', "PROSES");
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('D3', "OPERATOR");
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('E3', "RASIO PROCESS");
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('F3', "CAPACITY (pcs)");
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('G3', "AVAILABLE MINS");
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('H3', "WAITING RATE (menit)");
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('I3', "TARGET (pcs)");
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('J3', "TARGET 100% (pcs)");
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('K3', "ALLOCATED TIME (detik)");
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('L3', "HARMONIC WAVE (detik)");
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('M3', "PT (detik)");
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('N3', "UCL (detik)");
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('O3', "LCL (detik)");
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('P3', "SPT (detik)");
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('Q3', "PROCESS TIME");
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);

		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);

		
		// ASSEMBLY / TRUUUE
		$query = $this->db->query("SELECT * from skill_matrix_header
		where style = '$style' and line_id = '$line' and jenis_wip is false and matrix_batch_id = '$matrix_batch_id' ")->result();

		foreach ($query as $key => $value) {

			$sum_apt = $this->db->query("SELECT sum(rata_durasi) AS total_durasi from skill_matrix_header
			where style = '$style' and line_id = '$line' and jenis_wip is false and matrix_batch_id = '$matrix_batch_id'
			and nik_sewer ='$value->nik_sewer'")->row();

			$rasio            = $value->rata_durasi / $sum_apt->total_durasi;

			$data = array(
				'rasio' => $rasio,
			);

			$this->db->where('matrix_header_id', $value->matrix_header_id);
			$this->db->update('skill_matrix_header', $data);
		}


		// $worker = $this->db->query("SELECT nik_sewer, matrix_batch_id, smh.proses_id, wv.master_workflow_id from skill_matrix_header smh
		// LEFT JOIN workflow_view wv on wv.style = smh.style and wv.master_proses_id::varchar = smh.proses_id
		// where smh.style = '$style' and smh.line_id = '$line' and jenis_wip is false and matrix_batch_id = '$matrix_batch_id'
		// -- GROUP BY nik_sewer, matrix_batch_id
		// ORDER BY wv.master_workflow_id")->result();

		
		$apt = $this->db->query("SELECT smh.*, mm.nama_mesin, ms.name, wv.proses_name, wv.cycle_time, mm.nama_mesin, 
		mm.machine_delay, dv.order_qty, dv.present_sewer, dv.target_output
		from skill_matrix_header smh
		LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id
		LEFT JOIN daily_view dv on dv.style = smh.style and dv.line_id = smh.line_id and date(dv.create_date) = date(smh.create_date)
		LEFT JOIN workflow_view wv on wv.master_proses_id::varchar = smh.proses_id and wv.style = smh.style
		LEFT JOIN master_sewer ms on ms.nik = smh.nik_sewer
		where smh.style = '$style'
		and jenis_wip is false  and smh.line_id = '$line' 
		and matrix_batch_id = '$matrix_batch_id'
		ORDER BY wv.master_workflow_id")->result();


		$sum_sewer         = 0;
		$no                = 0;   // Untuk penomoran tabel, di awal set dengan 1
		$numrow            = 4;
		foreach ($apt as $key => $value) {
			// $apt = $this->db->query("SELECT smh.*, mm.nama_mesin, ms.name, wv.proses_name, wv.cycle_time, mm.nama_mesin, 
			// mm.machine_delay, dv.order_qty, dv.present_sewer, dv.target_output
			// from skill_matrix_header smh
			// LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id
			// LEFT JOIN daily_view dv on dv.style = smh.style and dv.line_id = smh.line_id and date(dv.create_date) = date(smh.create_date)
			// LEFT JOIN workflow_view wv on wv.master_proses_id::varchar = smh.proses_id and wv.style = smh.style
			// LEFT JOIN master_sewer ms on ms.nik = smh.nik_sewer
			// where smh.style = '$style'
			// and jenis_wip is false  and smh.line_id = '$line' 
			// and matrix_batch_id = '$work->matrix_batch_id'")->result();

			// $sum_apt = $this->db->query("SELECT sum(rata_durasi) AS total_durasi from skill_matrix_header
			// where style = '$style' and line_id = '$line' and jenis_wip is false and matrix_batch_id = '$work->matrix_batch_id'
			// and nik_sewer ='$work->nik_sewer'")->row();

			$no_op = $this->db->query("SELECT sum(rasio) AS no_operator from skill_matrix_header
			where style = '$style' and line_id = '$line' and jenis_wip is false and matrix_batch_id = '$value->matrix_batch_id' 
			and proses_id = '$value->proses_id'")->row();

			// foreach ($apt as $key => $value) {

				$no++;
				$cycle_per_second = 3600 / $value->rata_durasi;
				$rasio            = $value->rasio;
				$capacity         = $cycle_per_second * $rasio * (1 - ($value->machine_delay / 100));

				if ($capacity <= $value->target_output) {
					$available_mins = 0;
				} else {
					$available_mins = (($capacity - $value->target_output) * $value->rata_durasi) / 60;
				}

				if (($capacity - $value->target_output) < 0) {
					$waiting_rate = 0;
				} else {
					$waiting_rate = (($capacity - $value->target_output) / $value->target_output);
				}
				// var_dump($cycle_per_second);
				// var_dump($rasio);
				// var_dump(1-($value->machine_delay/100));
				// var_dump($capacity);
				// var_dump($value->target_output);
				// die();

				if ($value->proses_name == null) {
					// var_dump($detail->proses_name);
					$nama  = '';
					$spt = 0;
	
					$query = $this->db->query("SELECT * from workflow_view where master_proses_id in ($value->proses_id) and style = '$value->style'")->result_array();
					foreach ($query as $q) {
						$nama .= "" . $q['proses_name'] . ", ";
						$spt = $spt + $q['cycle_time'];
					}
	
					$prosesname = rtrim($nama, ', ');
					$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $prosesname);
				} else {
					// $cycle = $detail->cycle_time;				
					$spt = $value->cycle_time;				
					$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $value->proses_name);
				}
				

				$per_proses = $this->db->query("SELECT proses_id, rata_durasi, count(*) as jumlah from skill_matrix_header
				where style = '$style' and line_id = '$line' and jenis_wip is false and proses_id = '$value->proses_id'
				GROUP BY proses_id, rata_durasi");

				$harmon           = $per_proses->result();
				$n_harmonic       = $per_proses->num_rows();
				$pembagi_harmonic = 0;

				foreach ($harmon as $key => $h) {
					$pembagi_harmonic = $pembagi_harmonic + 1 / $h->rata_durasi;
				}

				$harmonic_wave = $n_harmonic / $pembagi_harmonic;
				$alocated_time = $harmonic_wave / $no_op->no_operator;
				$target_persen = 3600 / $alocated_time;
				$target        = round($target_persen * (1 - ($value->machine_delay / 100)));
				$sum_sewer     = $value->present_sewer;

				$data = array(
					'allocated_time' => $alocated_time,
					'harmonic_wave'  => $harmonic_wave,
				);
	
				$this->db->where('matrix_header_id', $value->matrix_header_id);
				$this->db->update('skill_matrix_header', $data);

				$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
				$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $value->nama_mesin);
				// $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $value->proses_name);
				$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $value->nik_sewer . ' - ' . $value->name);
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $rasio);
				$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $capacity);
				$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $available_mins);
				$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $waiting_rate);
				$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $target);
				$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, round($target_persen, 2, PHP_ROUND_HALF_UP));
				$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, round($alocated_time, 2));
				$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, round($harmonic_wave, 2));
				$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, 'PT');
				$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, 'UCL');
				$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, 'LCL');
				$excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $spt);
				$excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $value->rata_durasi);

				$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
				$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
				$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
				$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);

				$numrow++;
			// }
		}

		$sum_harmonic_wave = $this->db->query("SELECT sum(distinct harmonic_wave) AS total_harmonic from skill_matrix_header
		where matrix_batch_id = '$matrix_batch_id' and jenis_wip is false")->row();
		$pt=0;
		$ucl=0;
		$lcl=0;

		$last_numrow1 = $numrow-1;
		for($i=$numrow-1;$i>3;$i--){	
			$pt  = $sum_harmonic_wave->total_harmonic / $sum_sewer;
			$ucl = $pt / 0.85;
			$lcl = ($pt * 2) - $ucl;
			
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $i, round($pt, 2));
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $i, round($ucl, 2));
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $i, round($lcl, 2));
			
			$excel->getActiveSheet()->getStyle('M' . $i)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			$excel->getActiveSheet()->getStyle('N' . $i)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			$excel->getActiveSheet()->getStyle('O' . $i)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

			$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		}
		//=============================================================ASsEMBLYE TRUUU
		
		$numrow            = $numrow+1;
		
		$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "ASSEMBLY");
		$excel->getActiveSheet()->mergeCells('A'.$numrow.':Q'.$numrow);
		$excel->getActiveSheet()->getStyle('A'.$numrow)->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A'.$numrow)->getFont()->setSize(15);
		$excel->getActiveSheet()->getStyle('A'.$numrow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$numrow++;
		$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "NO");
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, "MESIN");
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, "PROSES");
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, "OPERATOR");
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, "RASIO PROCESS");
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, "CAPACITY (pcs)");
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, "AVAILABLE MINS");
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, "WAITING RATE (menit)");
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, "TARGET (pcs)");
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, "TARGET 100% (pcs)");
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, "ALLOCATED TIME (detik)");
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, "HARMONIC WAVE (detik)");
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, "PT (detik)");
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, "UCL (detik)");
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, "LCL (detik)");
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, "SPT (detik)");
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

		$excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, "PROCESS TIME");
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);

		$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_col);

		$numrow++;

		$query = $this->db->query("SELECT * from skill_matrix_header
		where style = '$style' and line_id = '$line' and jenis_wip is true and matrix_batch_id = '$matrix_batch_id' ")->result();

		foreach ($query as $key => $value) {

			$sum_apt = $this->db->query("SELECT sum(rata_durasi) AS total_durasi from skill_matrix_header
			where style = '$style' and line_id = '$line' and jenis_wip is true and matrix_batch_id = '$matrix_batch_id'
			and nik_sewer ='$value->nik_sewer'")->row();

			$rasio            = $value->rata_durasi / $sum_apt->total_durasi;

			$data = array(
				'rasio' => $rasio,
			);

			$this->db->where('matrix_header_id', $value->matrix_header_id);
			$this->db->update('skill_matrix_header', $data);
		}

		$sum_sewer         = 0;
		$no                = 0;   // Untuk penomoran tabel, di awal set dengan 1
		
		$apt = $this->db->query("SELECT smh.*, mm.nama_mesin, ms.name, wv.proses_name, wv.cycle_time, mm.nama_mesin, 
		mm.machine_delay, dv.order_qty, dv.present_sewer, dv.target_output
		from skill_matrix_header smh
		LEFT JOIN master_mesin mm on mm.mesin_id = smh.mesin_id
		LEFT JOIN daily_view dv on dv.style = smh.style and dv.line_id = smh.line_id and date(dv.create_date) = date(smh.create_date)
		LEFT JOIN workflow_view wv on wv.master_proses_id::varchar = smh.proses_id and wv.style = smh.style
		LEFT JOIN master_sewer ms on ms.nik = smh.nik_sewer
		where smh.style = '$style' and jenis_wip is true 
		and smh.line_id = '$line' and matrix_batch_id = '$matrix_batch_id'
		ORDER BY wv.master_workflow_id")->result();

		foreach ($apt as $key => $value) {
		
			$no_op = $this->db->query("SELECT sum(rasio) AS no_operator from skill_matrix_header
			where style = '$style' and line_id = '$line' and matrix_batch_id = '$value->matrix_batch_id' and jenis_wip is true 
			and proses_id = '$value->proses_id'")->row();

			// foreach ($apt as $key => $value) {

				$no++;
				$cycle_per_second = 3600 / $value->rata_durasi;
				$rasio            = $value->rasio;
				$capacity         = $cycle_per_second * $rasio * (1 - ($value->machine_delay / 100));

				if ($capacity <= $value->target_output) {
					$available_mins = 0;
				} else {
					$available_mins = (($capacity - $value->target_output) * $value->rata_durasi) / 60;
				}

				if (($capacity - $value->target_output) < 0) {
					$waiting_rate = 0;
				} else {
					$waiting_rate = (($capacity - $value->target_output) / $value->target_output);
				}

				if ($value->proses_name == null) {
					// var_dump($detail->proses_name);
					$nama  = '';
					$spt = 0;
	
					$query = $this->db->query("SELECT * from workflow_view where master_proses_id in ($value->proses_id) and style = '$value->style'")->result_array();
					foreach ($query as $q) {
						$nama .= "" . $q['proses_name'] . ", ";
						$spt = $spt + $q['cycle_time'];
					}
	
					$prosesname = rtrim($nama, ', ');
					$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $prosesname);
				} else {
					// $cycle = $detail->cycle_time;				
					$spt = $value->cycle_time;				
					$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $value->proses_name);
				}
				
				$per_proses = $this->db->query("SELECT proses_id, rata_durasi, count(*) as jumlah from skill_matrix_header
				where style = '$style' and line_id = '$line' and proses_id = '$value->proses_id' and jenis_wip is true 
				GROUP BY proses_id, rata_durasi");

				$harmon           = $per_proses->result();
				$n_harmonic       = $per_proses->num_rows();
				$pembagi_harmonic = 0;

				foreach ($harmon as $key => $h) {
					$pembagi_harmonic = $pembagi_harmonic + 1 / $h->rata_durasi;
				}

				$harmonic_wave = $n_harmonic / $pembagi_harmonic;
				$alocated_time = $harmonic_wave / $no_op->no_operator;
				$target_persen = 3600 / $alocated_time;
				$target        = round($target_persen * (1 - ($value->machine_delay / 100)));
				$sum_sewer     = $value->present_sewer;

				$data = array(
					'allocated_time' => $alocated_time,
					'harmonic_wave'  => $harmonic_wave,
				);
	
				$this->db->where('matrix_header_id', $value->matrix_header_id);
				$this->db->update('skill_matrix_header', $data);

				$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
				$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $value->nama_mesin);
				// $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $value->proses_name);
				$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $value->nik_sewer . ' - ' . $value->name);
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $rasio);
				$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $capacity);
				$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $available_mins);
				$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $waiting_rate);
				$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $target);
				$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, round($target_persen, 2, PHP_ROUND_HALF_UP));
				$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, round($alocated_time, 2));
				$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, round($harmonic_wave, 2));
				$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, 'PT');
				$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, 'UCL');
				$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, 'LCL');
				$excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $spt);
				$excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $value->rata_durasi);

				$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
				$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
				$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
				$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);

				$numrow++;
			// }
		}

		$sum_harmonic_wave = $this->db->query("SELECT sum(distinct harmonic_wave) AS total_harmonic from skill_matrix_header
		where matrix_batch_id = '$matrix_batch_id' and jenis_wip is true ")->row();
		$pt=0;
		$ucl=0;
		$lcl=0;

		for($i=$numrow-1;$i>$last_numrow1+3;$i--){
			$pt  = $sum_harmonic_wave->total_harmonic / $sum_sewer;
			$ucl = $pt / 0.85;
			$lcl = ($pt * 2) - $ucl;
			
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $i, round($pt, 2));
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $i, round($ucl, 2));
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $i, round($lcl, 2));
			
			$excel->getActiveSheet()->getStyle('M' . $i)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			$excel->getActiveSheet()->getStyle('N' . $i)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			$excel->getActiveSheet()->getStyle('O' . $i)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

			$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		}

		$excel->createSheet(1);
		$excel->setActiveSheetIndex(1)->setTitle("Graph1");
		$excel->setActiveSheetIndex(1)->setCellValue('A1', "PROSES");
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(1)->setCellValue('B1', "ALLOCATED TIME");
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(1)->setCellValue('C1', "PT");
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(1)->setCellValue('D1', "UCL");
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(1)->setCellValue('E1', "LCL");
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		$excel->getActiveSheet()->getStyle('A1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E1')->applyFromArray($style_col);
		
		$query_graph = $this->db->query("SELECT distinct proses_id, allocated_time, proses_name, master_workflow_id 
		from skill_matrix_header LEFT JOIN workflow_view on workflow_view.master_proses_id::VARCHAR = skill_matrix_header.proses_id 
		and workflow_view.style = skill_matrix_header.style where matrix_batch_id = '$matrix_batch_id' and jenis_wip is false 
		ORDER BY master_workflow_id")->result();

		$numrow2 = 2;
		foreach($query_graph as $qg){
			
			$excel->setActiveSheetIndex(1)->setCellValue('A' . $numrow2, $qg->proses_name);
			$excel->getActiveSheet()->getStyle('A' . $numrow2)->applyFromArray($style_row);
			
			$excel->setActiveSheetIndex(1)->setCellValue('B' . $numrow2, round($qg->allocated_time, 2));
			$excel->getActiveSheet()->getStyle('B' . $numrow2)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			
			$excel->setActiveSheetIndex(1)->setCellValue('C' . $numrow2, $pt);
			$excel->getActiveSheet()->getStyle('C' . $numrow2)->applyFromArray($style_row);
			
			$excel->setActiveSheetIndex(1)->setCellValue('D' . $numrow2, $ucl);
			$excel->getActiveSheet()->getStyle('D' . $numrow2)->applyFromArray($style_row);
			
			$excel->setActiveSheetIndex(1)->setCellValue('E' . $numrow2, $lcl);
			$excel->getActiveSheet()->getStyle('E' . $numrow2)->applyFromArray($style_row);
			$numrow2 += 1;
		}

		$last_data_1 = $numrow2-1;

		
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		$excel->setActiveSheetIndex(0);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->setActiveSheetIndex(0);


		// $query = $this->db->query("SELECT distinct style from skill_matrix_header
		// where style = 'S2125SMUW010' and line_id = '37' and matrix_batch_id = '$matrix_batch_id' ")->result_array();

		// $last_data = $numrow2-1;

		$dsl = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$B$1', NULL, 1),
			// 'Allocated'
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$C$1', NULL, 1),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$D$1', NULL, 1),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$E$1', NULL, 1),
		);

		$xal = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$A$2:$A$' . $last_data_1, NULL, $last_data_1),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$A$2:$A$' . $last_data_1, NULL, $last_data_1),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$A$2:$A$' . $last_data_1, NULL, $last_data_1),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph1!$A$2:$A$' . $last_data_1, NULL, $last_data_1),
		);

		// var_dump('Data!$K$3:$K' . $last_data);
		// var_dump($no - 1);
		// die();
		$dsv = array(
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph1!$B$2:$B$' . $last_data_1, NULL, $last_data_1),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph1!$C$2:$C$' . $last_data_1, NULL, $last_data_1),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph1!$D$2:$D$' . $last_data_1, NULL, $last_data_1),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph1!$E$2:$E$' . $last_data_1, NULL, $last_data_1),
		);

		$ds = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_LINECHART,
			PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
			range(0, count($dsv) - 1),
			$dsl,
			$xal,
			$dsv
		);

		
		$title  = new PHPExcel_Chart_Title('CONTROL CHART');
		$pa     = new PHPExcel_Chart_PlotArea(NULL, array($ds));
		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
		$chart  = new PHPExcel_Chart(
			'chart1',
			$title,
			$legend,
			$pa,
			true,
			0,
			NULL, 
			NULL
		);

		$chart->setTopLeftPosition('S1');
		$chart->setBottomRightPosition('AE25');
		$excel->getActiveSheet()->addChart($chart);

		$excel->createSheet(2);
		$excel->setActiveSheetIndex(2)->setTitle("Graph2");
		$excel->setActiveSheetIndex(2)->setCellValue('A1', "PROSES");
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(2)->setCellValue('B1', "ALLOCATED TIME");
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(2)->setCellValue('C1', "PT");
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(2)->setCellValue('D1', "UCL");
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		
		$excel->setActiveSheetIndex(2)->setCellValue('E1', "LCL");
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		$excel->getActiveSheet()->getStyle('A1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D1')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E1')->applyFromArray($style_col);
		
		$query_graph = $this->db->query("SELECT distinct proses_id, allocated_time, proses_name, master_workflow_id 
		from skill_matrix_header LEFT JOIN workflow_view on workflow_view.master_proses_id::VARCHAR = skill_matrix_header.proses_id 
		and workflow_view.style = skill_matrix_header.style where matrix_batch_id = '$matrix_batch_id' and jenis_wip is true 
		ORDER BY master_workflow_id")->result();

		$numrow2 = 2;
		foreach($query_graph as $qg){
			
			$excel->setActiveSheetIndex(2)->setCellValue('A' . $numrow2, $qg->proses_name);
			$excel->getActiveSheet()->getStyle('A' . $numrow2)->applyFromArray($style_row);
			
			$excel->setActiveSheetIndex(2)->setCellValue('B' . $numrow2, round($qg->allocated_time, 2));
			$excel->getActiveSheet()->getStyle('B' . $numrow2)->applyFromArray($style_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			
			$excel->setActiveSheetIndex(2)->setCellValue('C' . $numrow2, $pt);
			$excel->getActiveSheet()->getStyle('C' . $numrow2)->applyFromArray($style_row);
			
			$excel->setActiveSheetIndex(2)->setCellValue('D' . $numrow2, $ucl);
			$excel->getActiveSheet()->getStyle('D' . $numrow2)->applyFromArray($style_row);
			
			$excel->setActiveSheetIndex(2)->setCellValue('E' . $numrow2, $lcl);
			$excel->getActiveSheet()->getStyle('E' . $numrow2)->applyFromArray($style_row);
			$numrow2 += 1;
		}

		$last_data_2 = $numrow2-1;
		
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		$excel->setActiveSheetIndex(0);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->setActiveSheetIndex(0);

		$last_data = $numrow2-1;

		$dsl = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$B$1', NULL, 1),
			// 'Allocated'
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$C$1', NULL, 1),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$D$1', NULL, 1),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$E$1', NULL, 1),
		);

		$xal = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$A$2:$A$' . $last_data_2, NULL, $last_data_2),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$A$2:$A$' . $last_data_2, NULL, $last_data_2),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$A$2:$A$' . $last_data_2, NULL, $last_data_2),
			new PHPExcel_Chart_DataSeriesValues('String', 'Graph2!$A$2:$A$' . $last_data_2, NULL, $last_data_2),
		);

		// var_dump('Data!$K$3:$K' . $last_data_2);
		// var_dump($no - 1);
		// die();
		$dsv = array(
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph2!$B$2:$B$' . $last_data_2, NULL, $last_data_2),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph2!$C$2:$C$' . $last_data_2, NULL, $last_data_2),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph2!$D$2:$D$' . $last_data_2, NULL, $last_data_2),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Graph2!$E$2:$E$' . $last_data_2, NULL, $last_data_2),
		);

		$ds = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_LINECHART,
			PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
			range(0, count($dsv) - 1),
			$dsl,
			$xal,
			$dsv
		);

		
		$title  = new PHPExcel_Chart_Title('CONTROL CHART');
		$pa     = new PHPExcel_Chart_PlotArea(NULL, array($ds));
		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
		$chart  = new PHPExcel_Chart(
			'chart2',
			$title,
			$legend,
			$pa,
			true,
			0,
			NULL, 
			NULL
		);

		$chart->setTopLeftPosition('S28');
		$chart->setBottomRightPosition('AE53');
		$excel->getActiveSheet()->addChart($chart);

		// Proses file excel
		// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="REPORT Line Balancing.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		// $filename = 'Skill_Matrix_AOI_'.$factory.'.xlsx';
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->setIncludeCharts(TRUE);
		// $write->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
		// $path = $_SERVER["DOCUMENT_ROOT"]."/line_system/assets/excel/";
		// $write->save($path.$fileName);
		// $write->save(str_replace(__FILE__,''.$path.'/'.$filename,__FILE__));
		$write->save('php://output');
	}

	public function repot_skill(){
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

		if ($cek_permision == 0) {
			redirect('error_page', 'refresh');
		} else {
			$factory   = $this->session->userdata('factory');
			$this->template->set('title', 'Report Skill');
			$this->template->set('desc_page', 'Report Skill');
			$this->template->load('layout', 'report/skill_matrix/skill_all');
		}
	}

	public function export_skill(){
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
		$start = $this->input->get('start');
		$end = $this->input->get('end');
		$factory = $this->session->userdata('factory');

		
		$this->db->where('factory_id',$factory);
		$this->db->where('tgl >=',$start);
		$this->db->where('tgl <=',$end);
		$this->db->order_by('tgl','asc');
		$data = $this->db->get('skill_base')->result();

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator('ICT Team')
			->setLastModifiedBy('ICT Team')
			->setTitle("Data Skill")
			->setSubject("Data Skill")
			->setDescription("Laporan Data Skill")
			->setKeywords("Data Skill");
		
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Set text jadi ditengah secara horizontal (center)
				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER      // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
		);

		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER  // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA SKILL ".$start." until ".$end);
		$excel->getActiveSheet()->mergeCells('A1:O1');
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);  
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
		$excel->setActiveSheetIndex(0)->setCellValue('B2', "Date");
		$excel->setActiveSheetIndex(0)->setCellValue('C2', "NIK");
		$excel->setActiveSheetIndex(0)->setCellValue('D2', "Name");
		$excel->setActiveSheetIndex(0)->setCellValue('E2', "Style");
		$excel->setActiveSheetIndex(0)->setCellValue('F2', "Oprt. Complexity");
		$excel->setActiveSheetIndex(0)->setCellValue('G2', "Process");
		$excel->setActiveSheetIndex(0)->setCellValue('H2', "Ctg. Proses");
		$excel->setActiveSheetIndex(0)->setCellValue('I2', "Machine");
		$excel->setActiveSheetIndex(0)->setCellValue('J2', "Ctg. Maching");
		$excel->setActiveSheetIndex(0)->setCellValue('K2', "Performance");
		$excel->setActiveSheetIndex(0)->setCellValue('L2', "Quality");
		$excel->setActiveSheetIndex(0)->setCellValue('M2', "Star");
		$excel->setActiveSheetIndex(0)->setCellValue('N2', "Line");
		$excel->setActiveSheetIndex(0)->setCellValue('O2', "IE");

		$excel->getActiveSheet()->getStyle('A2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O2')->applyFromArray($style_col);


		$no = 1;
		$row = 3;
		$star = "";
		foreach ($data as $key => $dt) {
			if ($dt->bintang == 0) {
				$star = '☆';
			} else if ($dt->bintang == 1) {
				$star = '★';
			} else if ($dt->bintang == 2) {
				$star = '★★';
			} else if ($dt->bintang == 3) {
				$star = '★★★';
			} else {
				$star = '☆';
			}

			$excel->setActiveSheetIndex(0)->setCellValue('A' . $row, $no++);
			$excel->setActiveSheetIndex(0)->setCellValue('B'. $row, date_format(date_create($dt->tgl),'d-m-Y'));
			$excel->setActiveSheetIndex(0)->setCellValue('C'. $row,$dt->nik_sewer);
			$excel->setActiveSheetIndex(0)->setCellValue('D'. $row,$dt->nama_sewer);	
			$excel->setActiveSheetIndex(0)->setCellValue('E'. $row,$dt->proses_name);
			$excel->setActiveSheetIndex(0)->setCellValue('F'. $row,$dt->nik_sewer);
			$excel->setActiveSheetIndex(0)->setCellValue('E'. $row,$dt->style);
			$excel->setActiveSheetIndex(0)->setCellValue('F'. $row,$dt->operation_complexity);
			$excel->setActiveSheetIndex(0)->setCellValue('G'. $row,$dt->proses_name);
			$excel->setActiveSheetIndex(0)->setCellValue('H'. $row,$dt->kategori_proses);
			$excel->setActiveSheetIndex(0)->setCellValue('I'. $row,$dt->nama_mesin);
			$excel->setActiveSheetIndex(0)->setCellValue('J'. $row,$dt->kategori_mesin);
			$excel->setActiveSheetIndex(0)->setCellValue('K'. $row,$dt->performance);
			$excel->setActiveSheetIndex(0)->setCellValue('L'. $row,$dt->inline);
			$excel->setActiveSheetIndex(0)->setCellValue('M'. $row,$star);
			$excel->setActiveSheetIndex(0)->setCellValue('N'. $row,$dt->line_name);
			$excel->setActiveSheetIndex(0)->setCellValue('O'. $row,$dt->nama_ie."(".$dt->nik_ie.")");

			$row++;
		}

		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$excel->getActiveSheet(0)->setTitle("Laporan Skill");
		$excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="REPORT SKILL".xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}
}

/* End of file qc_inspect.php */
/* Location: ./application/controllers/qc_inspect.php */
