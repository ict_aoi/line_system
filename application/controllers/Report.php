<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		chek_session();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('ReportModel','Cekpermision_model'));
		$this->load->helper('download');
		$this->load->library('form_validation');
	}

	public function index()
	{

	}
	public function index_inspection($tanggal=NULL)
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Line Performance');
            $this->template->set('desc_page','Line Performance');
            $this->template->load('layout','report/DailyInlineInspection_view');
        }
	}

	public function operator_performance($tanggal=NULL)
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Operator Performance');
            $this->template->set('desc_page','Operator Performance');
            $this->template->load('layout','report/OperatorPerformance_view');
        }
	}
	public function dailydownload($date=NULL)
	{
         	$factory              = $this->session->userdata('factory');
			$date                 = $this->input->get('tanggal');
			$defect               = 0;


			if ($date<>'') {
				$report = $this->ReportModel->download($date,$factory);
				$defect_rate = $this->ReportModel->reportdefect($date,$factory);
			}else{
				$tanggal = date('Y-m-d');
				$report = $this->ReportModel->download($tanggal,$factory);
				$defect_rate = $this->ReportModel->reportdefect($tanggal,$factory);
			}

			header("Content-Type:application/vnd.ms-excel");
  			header('Content-Disposition:attachment; filename="report_daily_inspect '.$date.'.xls"');
			$draw='';
			if ($report!=NULL) {
				foreach ($report as $key => $d) {

					$draw .='<tr style="background-color:'.$d->color.'">'.
								'<td>'.$d->line_name.'</td>'.
								'<td>'.$d->poreference.'</td>'.
								'<td>'.$d->style.'</td>'.
								'<td>'.strtolower($d->proses_name).'</td>'.
								'<td>'.$d->sewer_nik.'</td>'.
								'<td >'.$d->round.'</td>'.
								'<td>'.$d->grade_name.'</td>'.
								'<td>'.$d->defect_id.'</td>'.
					       '</tr>';
				}
			}


			$defect ='';
			if($defect_rate!=0){
				foreach ($defect_rate as $key => $d) {
					$total_defect  = $this->ReportModel->total_defect($date,$factory,$d->line_id,$d->style);
					$total_inspect = $this->ReportModel->total_inspect($date,$factory,$d->line_id,$d->style);
					$defect_rate   = 100*($total_defect/$total_inspect);

					$defect .='<tr>'.
								'<td>'.$d->line_name.'</td>'.
								'<td>'.$d->style.'</td>'.
								'<td>'.$total_defect.'</td>'.
								'<td>'.number_format($total_inspect,1,',',',').'</td>'.
								'<td>'.number_format($defect_rate,1,',',',').'%</td>'.
					       '</tr>';
				}
			}


			$data = array(
				'draw' => $draw,
				'defect' => $defect
			);

            $this->load->view('report/downloadinspect_view', $data);
	}

	public function download_line_performance()
	{
         	$factory           = $this->session->userdata('factory');
			$date              = $this->input->get('tanggal');
			$nik_sewer         = $this->input->get('nik_sewer');
			$round             = $this->input->get('round');
			$color             = $this->input->get('color');
			$filter            = $this->input->get('filter');

			$report = $this->ReportModel->downloadLinePerformance($date, $nik_sewer, $round, $color, $filter, $factory);


			header("Content-Type:application/vnd.ms-excel");
  			header('Content-Disposition:attachment; filename="report_line_performance '.$date.'.xls"');
			$draw='';
			if ($report!=NULL) {
				foreach ($report as $key => $d) {

					$draw .='<tr style="">'.
								'<td>'.$d->line_name.'</td>'.
								'<td>'.$d->style.'</td>'.
								'<td>'.$d->proses_name.'</td>'.
								'<td>'.$d->sewer_nik.'</td>'.
								'<td>'.$d->sewer_name.'</td>'.
								'<td style="background-color: '.$d->grade_round1.'">'.$d->round1.'</td>'.
								'<td style="background-color: '.$d->grade_round2.'">'.$d->round2.'</td>'.
								'<td style="background-color: '.$d->grade_round3.'">'.$d->round3.'</td>'.
								'<td style="background-color: '.$d->grade_round4.'">'.$d->round4.'</td>'.
								'<td style="background-color: '.$d->grade_round5.'">'.$d->round5.'</td>'.
					       '</tr>';
				}
			}


			$data = array(
				'draw' => $draw
			);

            $this->load->view('report/downloadlineperformance_view', $data);
	}

	public function download_operator_performance()
	{
			$factory   = $this->session->userdata('factory');
			$datefrom  = $this->input->get('datefrom');
			$dateto    = $this->input->get('dateto');
			$nik_sewer = $this->input->get('nik_sewer');
			$grade     = $this->input->get('grade');
			$filter    = $this->input->get('filter');
			$line_from = $this->input->get('line_from');
			$line_to   = $this->input->get('line_to');

			$sekarang = date('Y-m-d');
			if($dateto == $sekarang || $datefrom == $sekarang){
		
				$report = $this->ReportModel->downloadOperatorPerformance($datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_from,$line_to);
				

				header("Content-Type:application/vnd.ms-excel");
				if($datefrom != $dateto){
					header('Content-Disposition:attachment; filename="report_operator_performance '.$datefrom.' sd '.$dateto.'.xls"');
				}
				else{
					header('Content-Disposition:attachment; filename="report_operator_performance '.$datefrom.'.xls"');	
				}
				$draw='';
				if ($report!=NULL) {
					foreach ($report as $key => $d) {
						
						$defect = [];
						if ($d->point_defect!=0) {
							$jenisDefect = $this->ReportModel->getDefect($d->sewer_nik,$d->date,$d->master_proses_id);
							
							foreach ($jenisDefect->result() as $key => $jenis) {

								$defect[] = $jenis->defect_jenis."=".$jenis->jumlah."<br>";
								
							}
						}
						$draw .='<tr>'.
									'<td>'.$d->date.'</td>'.
									'<td>'.$d->line_name.'</td>'.
									'<td>'.$d->name.'</td>'. 
									'<td>'.$d->sewer_nik.'</td>'.
									'<td>'.$d->proses_name.'</td>'.
									'<td>'.$d->point.'</td>'.
									'<td>'.$d->total.'</td>'.
									'<td>'.$d->grade.'</td>'.
									'<td>'.implode(',', $defect).'</td>'.
							'</tr>';
					}
				}
			}
			else{
				$report = $this->ReportModel->download_ops_perform($datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from);

				header("Content-Type:application/vnd.ms-excel");
				if($datefrom != $dateto){
					header('Content-Disposition:attachment; filename="report_operator_performance '.$datefrom.' sd '.$dateto.'.xls"');
				}
				else{
					header('Content-Disposition:attachment; filename="report_operator_performance '.$datefrom.'.xls"');	
				}
				$draw='';
				if ($report!=NULL) {
					foreach ($report as $key => $d) {
						$draw .='<tr>'.
									'<td>'.$d->date.'</td>'.
									'<td>'.$d->line_name.'</td>'.
									'<td>'.$d->name.'</td>'. 
									'<td>'.$d->nik_sewer.'</td>'.
									'<td>'.$d->nama_proses.'</td>'.
									'<td>'.$d->total_score.'</td>'.
									'<td>'.$d->total_avg.'</td>'.
									'<td>'.$d->grade.'</td>'.
									'<td>'.$d->jenis_defect.'</td>'.
							'</tr>';
					}
				}
			}


			$data = array(
				'draw' => $draw
			);

            $this->load->view('report/downloadoperatorperformance_view', $data);
	}


	//download measurement inline
	public function download_measurement()
	{
         	$factory   = $this->session->userdata('factory');
         	$dari      = $this->input->get('dari');
         	$sampai    = $this->input->get('sampai');
         	$line_from = $this->input->get('line_from');
         	$line_to   = $this->input->get('line_to');
         	$filter    = $this->input->get('filter');

			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = date('Y-m-d');
			}
	
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = date('Y-m-d');
			}

			$report = $this->ReportModel->downloadMeasurement($dari, $sampai, $line_from, $line_to, $filter, $factory);


			header("Content-Type:application/vnd.ms-excel");
			if($dari!=$sampai){
				header('Content-Disposition:attachment; filename="report_measurement_inline '.$dari.' sd '.$sampai.'.xls"');
			}
			else{
				header('Content-Disposition:attachment; filename="report_measurement_inline '.$dari.'.xls"');
			}
  			
			$draw='';
			if ($report!=NULL) {
				foreach ($report as $key => $d) {

					if ($d->grade_round1 == '' || $d->grade_round1 == null) {
	            		$grade_round1 = '#C0C0C0';

	            	}else{
	            		$grade_round1 = $d->grade_round1;
	            	}

	            	if ($d->grade_round2 == NULL && $d->grade_round1 != 'red') {
	            		$grade_round2 = '#C0C0C0';
	            	}elseif ($d->grade_round2 == NULL && $d->grade_round1 == 'red') {
	            		$grade_round2 = 'green';
	            	
	            	}else{
	            		$grade_round2 = $d->grade_round2;
	            	}

	            	if ($d->grade_round3 == NULL && $d->grade_round2 != 'red') {
	            		$grade_round3 = '#C0C0C0';
	            	}elseif ($d->grade_round3 == NULL && $d->grade_round2 == 'red') {
	            		$grade_round3 = 'green';
	            	}else{
	            		$grade_round3 = $d->grade_round3;
	            	}

	            	if ($d->grade_round4 == NULL && $d->grade_round3 != 'red') {
	            		$grade_round4 = '#C0C0C0';
	            	}elseif ($d->grade_round4 == NULL && $d->grade_round3 == 'red') {
	            		$grade_round4 = 'green';
	            	}else{
	            		$grade_round4 = $d->grade_round4;
	            	}

	            	if ($d->grade_round5 == NULL && $d->grade_round4 != 'red') {
	            		$grade_round5 = '#C0C0C0';
	            	}elseif ($d->grade_round5 == NULL && $d->grade_round4 == 'red') {
	            		$grade_round5 = 'green';
	            	}else{
	            		$grade_round5 = $d->grade_round5;
	            	}


					$draw .='<tr>'.
								'<td>'.$d->date.'</td>'.
								'<td>'.$d->line_name.'</td>'.
								'<td>'.$d->sewer_nik.'</td>'.
								'<td>'.$d->sewer_name.'</td>'.
								'<td>'.$d->proses_name.'</td>'.
								'<td style="background-color: '.$grade_round1.'">'.$d->round1.'</td>'.
								'<td style="background-color: '.$grade_round2.'">'.$d->round2.'</td>'.
								'<td style="background-color: '.$grade_round3.'">'.$d->round3.'</td>'.
								'<td style="background-color: '.$grade_round4.'">'.$d->round4.'</td>'.
								'<td style="background-color: '.$grade_round5.'">'.$d->round5.'</td>'.
					       '</tr>';
				}
			}


			$data = array(
				'draw' => $draw
			);

            $this->load->view('report/downloadmeasurement_view', $data);
	}
	//

	//download measurement inline
	public function download_measurement_endline()
	{
         	$factory   = $this->session->userdata('factory');
         	$dari      = $this->input->get('dari');
         	$sampai    = $this->input->get('sampai');
         	$line_from = $this->input->get('line_from');
         	$line_to   = $this->input->get('line_to');
         	$filter    = $this->input->get('filter');

			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = date('Y-m-d');
			}
	
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = date('Y-m-d');
			}

			$report = $this->ReportModel->downloadMeasurementEndline($dari, $sampai, $line_from, $line_to, $filter, $factory);


			header("Content-Type:application/vnd.ms-excel");
			if($dari!=$sampai){
				header('Content-Disposition:attachment; filename="report_measurement_endline '.$dari.' sd '.$sampai.'.xls"');
			}
			else{
				header('Content-Disposition:attachment; filename="report_measurement_endline '.$dari.'.xls"');
			}
  			$draw='';
			if ($report!=NULL) {
				foreach ($report as $key => $d) {
					if ($d->i == 0) {
	            		$grade_i = '#C0C0C0';
	            	}else{
	            		$grade_i = 'red';
	            	}

	            	if ($d->ii == 0 && $d->i == 0) {
	            		$grade_ii = '#C0C0C0';
	            	}elseif ($d->ii == 0 && $d->i != 0) {
	            		$grade_ii = 'green';
	            	
	            	}else{
	            		$grade_ii = 'red';
	            	}
	            	if ($d->iii == 0 && $d->ii == 0) {
	            		$grade_iii = '#C0C0C0';
	            	}elseif ($d->iii == 0 && $d->ii != 0) {
	            		$grade_iii = 'green';
	            	
	            	}else{
	            		$grade_iii = 'red';
	            	}
	            	if ($d->iv == 0 && $d->iii == 0) {
	            		$grade_iv = '#C0C0C0';
	            	}elseif ($d->iv == 0 && $d->iii != 0) {
	            		$grade_iv = 'green';
	            	
	            	}else{
	            		$grade_iv = 'red';
	            	}
	            	if ($d->v == 0 && $d->iv == 0) {
	            		$grade_v = '#C0C0C0';
	            	}elseif ($d->v == 0 && $d->iv != 0) {
	            		$grade_v = 'green';
	            	
	            	}else{
	            		$grade_v = 'red';
	            	}

					$draw .='<tr>'.
								'<td>'.$d->create_date.'</td>'.
								'<td>'.$d->line_name.'</td>'.
								'<td>'.$d->style.'</td>'.
								'<td style="background-color: '.$grade_i.'">'.$d->i.'</td>'.
								'<td style="background-color: '.$grade_ii.'">'.$d->ii.'</td>'.
								'<td style="background-color: '.$grade_iii.'">'.$d->iii.'</td>'.
								'<td style="background-color: '.$grade_iv.'">'.$d->iv.'</td>'.
								'<td style="background-color: '.$grade_v.'">'.$d->v.'</td>'.
					       '</tr>';
				}
			}


			$data = array(
				'draw' => $draw
			);

            $this->load->view('report/downloadmeasurementendline_view', $data);
	}
	//
	public function report_rft()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	$data['tanggal'] = $this->input->post('tanggal',TRUE);
        	$data['defect_id'] =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();
        	$this->template->set('title','Daily Inspection');
            $this->template->set('desc_page','Daily Inspection');
            $this->template->load('layout','report/tls_report',$data);
        }
	}
	public function rftreport_ajax()
	{
		$columns = array(
                            0 =>'create_date',
                            1 =>'line_id',
                            2 =>'style',
                            3 =>'poreference',
                            4 =>'style',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $date    = $this->input->post('tanggal');

  		if ($date!='') {
  			$date = date('Y-m-d',strtotime($date));
  		}else{
  			$date = date('Y-m-d');
  		}

        $totalData = $this->ReportModel->allposts_count_reportrft($date);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->ReportModel->allposts_reportrft($limit,$start,$order,$dir,$date);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->ReportModel->posts_search_reportrft($limit,$start,$search,$order,$dir,$date);

            $totalFiltered = $this->ReportModel->posts_search_count_reportrft($search,$date);
        }
        $data = array();
        $nomor_urut = 0;

        if(!empty($master))
        {
            foreach ($master as $master)
            {
				$rft                         = (($master->total_checked-$master->count_defect)/$master->total_checked)*100;
				$nestedData['create_date']   = $master->create_date;
				$nestedData['line']          = ucwords($master->line_name);
				$nestedData['poreference']   = $master->poreference;
				$nestedData['style']         = strtoupper($master->style);
				$nestedData['total_checked'] = $master->total_checked;
				$nestedData['count_defect']  = $master->count_defect;
				$nestedData['rft']           = number_format($rft,1,',',',').'%';
				$nestedData['defect1']       = $master->defect1;
				$nestedData['defect2']       = $master->defect2;
				$nestedData['defect3']       = $master->defect3;
				$nestedData['defect4']       = $master->defect4;
				$nestedData['defect5']       = $master->defect5;
				$nestedData['defect6']       = $master->defect6;
				$nestedData['defect7']       = $master->defect7;
				$nestedData['defect8']       = $master->defect8;
				$nestedData['defect9']       = $master->defect9;
				$nestedData['defect10']      = $master->defect10;
				$nestedData['defect11']      = $master->defect11;
				$nestedData['defect12']      = $master->defect12;
				$nestedData['defect13']      = $master->defect13;
				$nestedData['defect14']      = $master->defect14;
				$nestedData['defect15']      = $master->defect15;
				$nestedData['defect16']      = $master->defect16;
				$nestedData['defect17']      = $master->defect17;
				$nestedData['defect18']      = $master->defect18;
				$nestedData['defect19']      = $master->defect19;
				$nestedData['defect20']      = $master->defect20;
				$nestedData['defect21']      = $master->defect21;
				$nestedData['defect22']      = $master->defect22;
				$nestedData['defect23']      = $master->defect23;
				$nestedData['defect24']      = $master->defect24;
				$nestedData['defect25']      = $master->defect25;
				$nestedData['defect26']      = $master->defect26;
				$nestedData['defect27']      = $master->defect27;
				$nestedData['defect28']      = $master->defect28;
				$nestedData['defect29']      = $master->defect29;
				$nestedData['defect30']      = $master->defect30;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function downloadrft_bystyle($get=NULL)
	{
		$get = $this->input->get();
		if ($get!=NULL) {
			$tanggal =$this->input->get('tanggal');
			$detail_report='';

			$date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

			$details = $this->ReportModel->downloadReportTls($date);

			$defect_id =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();

			header("Content-Type:application/vnd.ms-excel");
			header('Content-Disposition:attachment; filename="Report TLS Inline Per Style '.$date.'.xls"');

			foreach ($details->result() as $key => $detail) {
				$rft  = (($detail->total_checked-$detail->count_defect)/$detail->total_checked)*100;
				$detail_report .='<tr>'.
									'<td>'.$detail->create_date.'</td>'.
									'<td>'.$detail->line_name.'</td>'.
									'<td>'.$detail->poreference.'</td>'.
									'<td>'.$detail->style.'</td>'.
									'<td>'.$detail->total_checked.'</td>'.
									'<td>'.$detail->count_defect.'</td>'.
									'<td>'.number_format($rft,1,',',',').'%'.'</td>'.
									'<td>'.$detail->defect1.'</td>'.
									'<td>'.$detail->defect2.'</td>'.
									'<td>'.$detail->defect3.'</td>'.
									'<td>'.$detail->defect4.'</td>'.
									'<td>'.$detail->defect5.'</td>'.
									'<td>'.$detail->defect6.'</td>'.
									'<td>'.$detail->defect7.'</td>'.
									'<td>'.$detail->defect8.'</td>'.
									'<td>'.$detail->defect9.'</td>'.
									'<td>'.$detail->defect10.'</td>'.
									'<td>'.$detail->defect11.'</td>'.
									'<td>'.$detail->defect12.'</td>'.
									'<td>'.$detail->defect13.'</td>'.
									'<td>'.$detail->defect14.'</td>'.
									'<td>'.$detail->defect15.'</td>'.
									'<td>'.$detail->defect16.'</td>'.
									'<td>'.$detail->defect17.'</td>'.
									'<td>'.$detail->defect18.'</td>'.
									'<td>'.$detail->defect19.'</td>'.
									'<td>'.$detail->defect20.'</td>'.
									'<td>'.$detail->defect21.'</td>'.
									'<td>'.$detail->defect22.'</td>'.
									'<td>'.$detail->defect23.'</td>'.
									'<td>'.$detail->defect24.'</td>'.
									'<td>'.$detail->defect25.'</td>'.
									'<td>'.$detail->defect26.'</td>'.
									'<td>'.$detail->defect27.'</td>'.
									'<td>'.$detail->defect28.'</td>'.
									'<td>'.$detail->defect29.'</td>'.
									'<td>'.$detail->defect30.'</td>'.
									'<td>'.$detail->defect31.'</td>'.
									'<td>'.$detail->defect32.'</td>'.
									'<td>'.$detail->defect33.'</td>'.
									'<td>'.$detail->defect34.'</td>'.
									'<td>'.$detail->defect35.'</td>'.
									'<td>'.$detail->defect36.'</td>'.
									'<td>'.$detail->defect37.'</td>'.
									'<td>'.$detail->defect38.'</td>'.
									'<td>'.$detail->defect39.'</td>'.
									'<td>'.$detail->defect40.'</td>'.
									'<td>'.$detail->defect41.'</td>'.
									'<td>'.$detail->defect42.'</td>'.
									'<td>'.$detail->defect43.'</td>'.
									'<td>'.$detail->defect44.'</td>'.
									'<td>'.$detail->defect45.'</td>'.
									'<td>'.$detail->defect46.'</td>'.
									'<td>'.$detail->defect47.'</td>'.
									'<td>'.$detail->defect48.'</td>'.
									'<td>'.$detail->defect49.'</td>'.
									'<td>'.$detail->defect50.'</td>'.
									'<td>'.$detail->defect51.'</td>'.
									'<td>'.$detail->defect52.'</td>'.
									'<td>'.$detail->defect53.'</td>'.
									'<td>'.$detail->defect54.'</td>'.
									'<td>'.$detail->defect55.'</td>'.
									'<td>'.$detail->defect56.'</td>'.
									'<td>'.$detail->defect57.'</td>'.
									'<td>'.$detail->defect58.'</td>'.
									'<td>'.$detail->defect59.'</td>'.
									'<td>'.$detail->defect60.'</td>'.
									'<td>'.$detail->defect61.'</td>'.
									'<td>'.$detail->defect62.'</td>'.
									'<td>'.$detail->defect63.'</td>'.
									'<td>'.$detail->defect64.'</td>'.
									'<td>'.$detail->defect65.'</td>'.
									'<td>'.$detail->defect66.'</td>'.
									'<td>'.$detail->defect67.'</td>'.
									'<td>'.$detail->defect68.'</td>'.
									'<td>'.$detail->defect69.'</td>'.
									'<td>'.$detail->defect70.'</td>'.
									'<td>'.$detail->defect71.'</td>'.
									'<td>'.$detail->defect72.'</td>'.
									'<td>'.$detail->defect73.'</td>'.
									'<td>'.$detail->defect74.'</td>'.
									'<td>'.$detail->defect75.'</td>'.
									'<td>'.$detail->defect76.'</td>'.
									'<td>'.$detail->defect77.'</td>'.
									'<td>'.$detail->defect78.'</td>'.
									'<td>'.$detail->defect79.'</td>'.
									'<td>'.$detail->defect80.'</td>'.
									'<td>'.$detail->defect81.'</td>'.
									'<td>'.$detail->defect82.'</td>'.
									'<td>'.$detail->defect83.'</td>'.
									'<td>'.$detail->defect84.'</td>'.
									'<td>'.$detail->defect85.'</td>'.
									'<td>'.$detail->defect86.'</td>'.
									'<td>'.$detail->defect87.'</td>'.
									'<td>'.$detail->defect88.'</td>'.
									'<td>'.$detail->defect89.'</td>'.
									'<td>'.$detail->defect90.'</td>'.
									'<td>'.$detail->defect91.'</td>'.
									'<td>'.$detail->defect92.'</td>'.
									'<td>'.$detail->defect93.'</td>'.
									'<td>'.$detail->defect94.'</td>'.
									'<td>'.$detail->defect95.'</td>'.
									'<td>'.$detail->defect96.'</td>'.
									'<td>'.$detail->defect97.'</td>'.
									'<td>'.$detail->defect98.'</td>'.
									'<td>'.$detail->defect99.'</td>'.
									'<td>'.$detail->defect100.'</td>'.
									'<td>'.$detail->defect101.'</td>'.
									'<td>'.$detail->defect102.'</td>'.
									'<td>'.$detail->defect103.'</td>'.
									'<td>'.$detail->defect104.'</td>'.
									'<td>'.$detail->defect105.'</td>'.
									'<td>'.$detail->defect106.'</td>'.
									'<td>'.$detail->defect107.'</td>'.
									'<td>'.$detail->defect108.'</td>'.
									'<td>'.$detail->defect109.'</td>'.
									'<td>'.$detail->defect110.'</td>'.
									'<td>'.$detail->defect111.'</td>'.
									'<td>'.$detail->defect112.'</td>'.
									'<td>'.$detail->defect113.'</td>'.
									'<td>'.$detail->defect114.'</td>'.
									'<td>'.$detail->defect115.'</td>'.
									'<td>'.$detail->defect116.'</td>'.
									'<td>'.$detail->defect117.'</td>'.
									'<td>'.$detail->defect118.'</td>'.
									'<td>'.$detail->defect119.'</td>'.
									'<td>'.$detail->defect120.'</td>'.
									'<td>'.$detail->defect121.'</td>'.
									'<td>'.$detail->defect122.'</td>'.
									'<td>'.$detail->defect123.'</td>'.
									'<td>'.$detail->defect124.'</td>'.
									'<td>'.$detail->defect125.'</td>'.
									'<td>'.$detail->defect126.'</td>'.
									'<td>'.$detail->defect127.'</td>'.
									'<td>'.$detail->defect128.'</td>'.
									'<td>'.$detail->defect129.'</td>'.
									'<td>'.$detail->defect130.'</td>'.
									'<td>'.$detail->defect131.'</td>'.
									'<td>'.$detail->defect132.'</td>'.
									'<td>'.$detail->defect133.'</td>'.
									'<td>'.$detail->defect134.'</td>'.
									'<td>'.$detail->defect135.'</td>'.
									'<td>'.$detail->defect136.'</td>'.
									'<td>'.$detail->defect137.'</td>'.
									'<td>'.$detail->defect138.'</td>'.
									'<td>'.$detail->defect139.'</td>'.
									'<td>'.$detail->defect140.'</td>'.
									'<td>'.$detail->defect141.'</td>'.
									'<td>'.$detail->defect142.'</td>'.
									'<td>'.$detail->defect143.'</td>'.
									'<td>'.$detail->defect144.'</td>'.
									'<td>'.$detail->defect145.'</td>'.
									'<td>'.$detail->defect146.'</td>'.
									'<td>'.$detail->defect147.'</td>'.
									'<td>'.$detail->defect148.'</td>'.
									'<td>'.$detail->defect149.'</td>'.
									'<td>'.$detail->defect150.'</td>'.
									'<td>'.$detail->defect151.'</td>'.
									'<td>'.$detail->defect152.'</td>'.
									'<td>'.$detail->defect153.'</td>'.
									'<td>'.$detail->defect154.'</td>'.
									'<td>'.$detail->defect155.'</td>'.
									'<td>'.$detail->defect156.'</td>'.
									'<td>'.$detail->defect157.'</td>'.
									'<td>'.$detail->defect158.'</td>'.
									'<td>'.$detail->defect159.'</td>'.
									'<td>'.$detail->defect160.'</td>'.
									'<td>'.$detail->defect161.'</td>'.
									'<td>'.$detail->defect162.'</td>'.
									'<td>'.$detail->defect163.'</td>'.
									'<td>'.$detail->defect164.'</td>'.
									'<td>'.$detail->defect165.'</td>'.
									'<td>'.$detail->defect166.'</td>'.
									'<td>'.$detail->defect167.'</td>'.
									'<td>'.$detail->defect168.'</td>'.
									'<td>'.$detail->defect169.'</td>'.
									'<td>'.$detail->defect170.'</td>'.

								'</tr>';

			}
			$data = array(
				'tanggal' => $date,
				'defect_id' => $defect_id,
				'detail_report' => $detail_report
			);
			
			$this->load->view('report/down_tls_view', $data);
			
		}
		
	}
	public function downloadrft_byline($get=NULL)
	{
		
		$get = $this->input->get();
		if ($get!=NULL) {
			$tanggal =$this->input->get('tanggal');
			$data = "";
			$week = week_of_today();
			
			$date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

			$details1 = $this->ReportModel->downloadReportTlsLine($date,1);
			$details2 = $this->ReportModel->downloadReportTlsLine($date,2);
			$details3 = $this->ReportModel->downloadReportTlsLine($date,3);
			$details4 = $this->ReportModel->downloadReportTlsLine($date,4);
			$details5 = $this->ReportModel->downloadReportTlsLine($date,5);

			$line = $this->ReportModel->count_line($date);

			$defect_id =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();


			header("Content-Type:application/vnd.ms-excel");
			header('Content-Disposition:attachment; filename="Report TLS Inline Round '.$date.'.xls"');

			foreach ($details1->result() as $key => $r1) {
				$rft1  = (($r1->total_checked-$r1->total_defect)/$r1->total_checked)*100;
				$data = $data.'<tr>
						<td>'.$r1->create_date.'</td>
						<td>'.$line.'</td>
						<td> 1 </td>
						<td>'.$r1->total_checked.'</td>
						<td>'.$r1->defect1.'</td>
						<td>'.$r1->defect2.'</td>
						<td>'.$r1->defect3.'</td>
						<td>'.$r1->defect4.'</td>
						<td>'.$r1->defect5.'</td>
						<td>'.$r1->defect6.'</td>
						<td>'.$r1->defect7.'</td>
						<td>'.$r1->defect8.'</td>
						<td>'.$r1->defect9.'</td>
						<td>'.$r1->defect10.'</td>
						<td>'.$r1->defect11.'</td>
						<td>'.$r1->defect12.'</td>
						<td>'.$r1->defect13.'</td>
						<td>'.$r1->defect14.'</td>
						<td>'.$r1->defect15.'</td>
						<td>'.$r1->defect16.'</td>
						<td>'.$r1->defect17.'</td>
						<td>'.$r1->defect18.'</td>
						<td>'.$r1->defect19.'</td>
						<td>'.$r1->defect20.'</td>
						<td>'.$r1->defect21.'</td>
						<td>'.$r1->defect22.'</td>
						<td>'.$r1->defect23.'</td>
						<td>'.$r1->defect24.'</td>
						<td>'.$r1->defect25.'</td>
						<td>'.$r1->defect26.'</td>
						<td>'.$r1->defect27.'</td>
						<td>'.$r1->defect28.'</td>
						<td>'.$r1->defect29.'</td>
						<td>'.$r1->defect30.'</td>
						<td>'.$r1->defect31.'</td>
						<td>'.$r1->defect32.'</td>
						<td>'.$r1->defect33.'</td>
						<td>'.$r1->defect34.'</td>
						<td>'.$r1->defect35.'</td>
						<td>'.$r1->defect36.'</td>
						<td>'.$r1->defect37.'</td>
						<td>'.$r1->defect38.'</td>
						<td>'.$r1->defect39.'</td>
						<td>'.$r1->defect40.'</td>
						<td>'.$r1->defect41.'</td>
						<td>'.$r1->defect42.'</td>
						<td>'.$r1->defect43.'</td>
						<td>'.$r1->defect44.'</td>
						<td>'.$r1->defect45.'</td>
						<td>'.$r1->defect46.'</td>
						<td>'.$r1->defect47.'</td>
						<td>'.$r1->defect48.'</td>
						<td>'.$r1->defect49.'</td>
						<td>'.$r1->defect50.'</td>
						<td>'.$r1->defect51.'</td>
						<td>'.$r1->defect52.'</td>
						<td>'.$r1->defect53.'</td>
						<td>'.$r1->defect54.'</td>
						<td>'.$r1->defect55.'</td>
						<td>'.$r1->defect56.'</td>
						<td>'.$r1->defect57.'</td>
						<td>'.$r1->defect58.'</td>
						<td>'.$r1->defect59.'</td>
						<td>'.$r1->defect60.'</td>
						<td>'.$r1->defect61.'</td>
						<td>'.$r1->defect62.'</td>
						<td>'.$r1->defect63.'</td>
						<td>'.$r1->defect64.'</td>
						<td>'.$r1->defect65.'</td>
						<td>'.$r1->defect66.'</td>
						<td>'.$r1->defect67.'</td>
						<td>'.$r1->defect68.'</td>
						<td>'.$r1->defect69.'</td>
						<td>'.$r1->defect70.'</td>
						<td>'.$r1->defect71.'</td>
						<td>'.$r1->defect72.'</td>
						<td>'.$r1->defect73.'</td>
						<td>'.$r1->defect74.'</td>
						<td>'.$r1->defect75.'</td>
						<td>'.$r1->defect76.'</td>
						<td>'.$r1->defect77.'</td>
						<td>'.$r1->defect78.'</td>
						<td>'.$r1->defect79.'</td>
						<td>'.$r1->defect80.'</td>
						<td>'.$r1->defect81.'</td>
						<td>'.$r1->defect82.'</td>
						<td>'.$r1->defect83.'</td>
						<td>'.$r1->defect84.'</td>
						<td>'.$r1->defect85.'</td>
						<td>'.$r1->defect86.'</td>
						<td>'.$r1->defect87.'</td>
						<td>'.$r1->defect88.'</td>
						<td>'.$r1->defect89.'</td>
						<td>'.$r1->defect90.'</td>
						<td>'.$r1->defect91.'</td>
						<td>'.$r1->defect92.'</td>
						<td>'.$r1->defect93.'</td>
						<td>'.$r1->defect94.'</td>
						<td>'.$r1->defect95.'</td>
						<td>'.$r1->defect96.'</td>
						<td>'.$r1->defect97.'</td>
						<td>'.$r1->defect98.'</td>
						<td>'.$r1->defect99.'</td>
						<td>'.$r1->defect100.'</td>
						<td>'.$r1->defect101.'</td>
						<td>'.$r1->defect102.'</td>
						<td>'.$r1->defect103.'</td>
						<td>'.$r1->defect104.'</td>
						<td>'.$r1->defect105.'</td>
						<td>'.$r1->defect106.'</td>
						<td>'.$r1->defect107.'</td>
						<td>'.$r1->defect108.'</td>
						<td>'.$r1->defect109.'</td>
						<td>'.$r1->defect110.'</td>
						<td>'.$r1->defect111.'</td>
						<td>'.$r1->defect112.'</td>
						<td>'.$r1->defect113.'</td>
						<td>'.$r1->defect114.'</td>
						<td>'.$r1->defect115.'</td>
						<td>'.$r1->defect116.'</td>
						<td>'.$r1->defect117.'</td>
						<td>'.$r1->defect118.'</td>
						<td>'.$r1->defect119.'</td>
						<td>'.$r1->defect120.'</td>
						<td>'.$r1->defect121.'</td>
						<td>'.$r1->defect122.'</td>
						<td>'.$r1->defect123.'</td>
						<td>'.$r1->defect124.'</td>
						<td>'.$r1->defect125.'</td>
						<td>'.$r1->defect126.'</td>
						<td>'.$r1->defect127.'</td>
						<td>'.$r1->defect128.'</td>
						<td>'.$r1->defect129.'</td>
						<td>'.$r1->defect130.'</td>
						<td>'.$r1->defect131.'</td>
						<td>'.$r1->defect132.'</td>
						<td>'.$r1->defect133.'</td>
						<td>'.$r1->defect134.'</td>
						<td>'.$r1->defect135.'</td>
						<td>'.$r1->defect136.'</td>
						<td>'.$r1->defect137.'</td>
						<td>'.$r1->defect138.'</td>
						<td>'.$r1->defect139.'</td>
						<td>'.$r1->defect140.'</td>
						<td>'.$r1->defect141.'</td>
						<td>'.$r1->defect142.'</td>
						<td>'.$r1->defect143.'</td>
						<td>'.$r1->defect144.'</td>
						<td>'.$r1->defect145.'</td>
						<td>'.$r1->defect146.'</td>
						<td>'.$r1->defect147.'</td>
						<td>'.$r1->defect148.'</td>
						<td>'.$r1->defect149.'</td>
						<td>'.$r1->defect150.'</td>
						<td>'.$r1->defect151.'</td>
						<td>'.$r1->defect152.'</td>
						<td>'.$r1->defect153.'</td>
						<td>'.$r1->defect154.'</td>
						<td>'.$r1->defect155.'</td>
						<td>'.$r1->defect156.'</td>
						<td>'.$r1->defect157.'</td>
						<td>'.$r1->defect158.'</td>
						<td>'.$r1->defect159.'</td>
						<td>'.$r1->defect160.'</td>
						<td>'.$r1->defect161.'</td>
						<td>'.$r1->defect162.'</td>
						<td>'.$r1->defect163.'</td>
						<td>'.$r1->defect164.'</td>
						<td>'.$r1->defect165.'</td>
						<td>'.$r1->defect166.'</td>
						<td>'.$r1->defect167.'</td>
						<td>'.$r1->defect168.'</td>
						<td>'.$r1->defect169.'</td>
						<td>'.$r1->defect170.'</td>
						<td>'.$r1->total_defect.'</td>
						<td>'.number_format($rft1,1,',',',').'%'.'</td>
						</tr>';
			}

			foreach ($details2->result() as $key => $r2) {
				$rft2  = (($r2->total_checked-$r2->total_defect)/$r2->total_checked)*100;
				$data = $data.'<tr>
						<td>'.$r2->create_date.'</td>
						<td>'.$line.'</td>
						<td> 2 </td>
						<td>'.$r2->total_checked.'</td>
						<td>'.$r2->defect1.'</td>
						<td>'.$r2->defect2.'</td>
						<td>'.$r2->defect3.'</td>
						<td>'.$r2->defect4.'</td>
						<td>'.$r2->defect5.'</td>
						<td>'.$r2->defect6.'</td>
						<td>'.$r2->defect7.'</td>
						<td>'.$r2->defect8.'</td>
						<td>'.$r2->defect9.'</td>
						<td>'.$r2->defect10.'</td>
						<td>'.$r2->defect11.'</td>
						<td>'.$r2->defect12.'</td>
						<td>'.$r2->defect13.'</td>
						<td>'.$r2->defect14.'</td>
						<td>'.$r2->defect15.'</td>
						<td>'.$r2->defect16.'</td>
						<td>'.$r2->defect17.'</td>
						<td>'.$r2->defect18.'</td>
						<td>'.$r2->defect19.'</td>
						<td>'.$r2->defect20.'</td>
						<td>'.$r2->defect21.'</td>
						<td>'.$r2->defect22.'</td>
						<td>'.$r2->defect23.'</td>
						<td>'.$r2->defect24.'</td>
						<td>'.$r2->defect25.'</td>
						<td>'.$r2->defect26.'</td>
						<td>'.$r2->defect27.'</td>
						<td>'.$r2->defect28.'</td>
						<td>'.$r2->defect29.'</td>
						<td>'.$r2->defect30.'</td>
						<td>'.$r2->defect31.'</td>
						<td>'.$r2->defect32.'</td>
						<td>'.$r2->defect33.'</td>
						<td>'.$r2->defect34.'</td>
						<td>'.$r2->defect35.'</td>
						<td>'.$r2->defect36.'</td>
						<td>'.$r2->defect37.'</td>
						<td>'.$r2->defect38.'</td>
						<td>'.$r2->defect39.'</td>
						<td>'.$r2->defect40.'</td>
						<td>'.$r2->defect41.'</td>
						<td>'.$r2->defect42.'</td>
						<td>'.$r2->defect43.'</td>
						<td>'.$r2->defect44.'</td>
						<td>'.$r2->defect45.'</td>
						<td>'.$r2->defect46.'</td>
						<td>'.$r2->defect47.'</td>
						<td>'.$r2->defect48.'</td>
						<td>'.$r2->defect49.'</td>
						<td>'.$r2->defect50.'</td>
						<td>'.$r2->defect51.'</td>
						<td>'.$r2->defect52.'</td>
						<td>'.$r2->defect53.'</td>
						<td>'.$r2->defect54.'</td>
						<td>'.$r2->defect55.'</td>
						<td>'.$r2->defect56.'</td>
						<td>'.$r2->defect57.'</td>
						<td>'.$r2->defect58.'</td>
						<td>'.$r2->defect59.'</td>
						<td>'.$r2->defect60.'</td>
						<td>'.$r2->defect61.'</td>
						<td>'.$r2->defect62.'</td>
						<td>'.$r2->defect63.'</td>
						<td>'.$r2->defect64.'</td>
						<td>'.$r2->defect65.'</td>
						<td>'.$r2->defect66.'</td>
						<td>'.$r2->defect67.'</td>
						<td>'.$r2->defect68.'</td>
						<td>'.$r2->defect69.'</td>
						<td>'.$r2->defect70.'</td>
						<td>'.$r2->defect71.'</td>
						<td>'.$r2->defect72.'</td>
						<td>'.$r2->defect73.'</td>
						<td>'.$r2->defect74.'</td>
						<td>'.$r2->defect75.'</td>
						<td>'.$r2->defect76.'</td>
						<td>'.$r2->defect77.'</td>
						<td>'.$r2->defect78.'</td>
						<td>'.$r2->defect79.'</td>
						<td>'.$r2->defect80.'</td>
						<td>'.$r2->defect81.'</td>
						<td>'.$r2->defect82.'</td>
						<td>'.$r2->defect83.'</td>
						<td>'.$r2->defect84.'</td>
						<td>'.$r2->defect85.'</td>
						<td>'.$r2->defect86.'</td>
						<td>'.$r2->defect87.'</td>
						<td>'.$r2->defect88.'</td>
						<td>'.$r2->defect89.'</td>
						<td>'.$r2->defect90.'</td>
						<td>'.$r2->defect91.'</td>
						<td>'.$r2->defect92.'</td>
						<td>'.$r2->defect93.'</td>
						<td>'.$r2->defect94.'</td>
						<td>'.$r2->defect95.'</td>
						<td>'.$r2->defect96.'</td>
						<td>'.$r2->defect97.'</td>
						<td>'.$r2->defect98.'</td>
						<td>'.$r2->defect99.'</td>
						<td>'.$r2->defect100.'</td>
						<td>'.$r2->defect101.'</td>
						<td>'.$r2->defect102.'</td>
						<td>'.$r2->defect103.'</td>
						<td>'.$r2->defect104.'</td>
						<td>'.$r2->defect105.'</td>
						<td>'.$r2->defect106.'</td>
						<td>'.$r2->defect107.'</td>
						<td>'.$r2->defect108.'</td>
						<td>'.$r2->defect109.'</td>
						<td>'.$r2->defect110.'</td>
						<td>'.$r2->defect111.'</td>
						<td>'.$r2->defect112.'</td>
						<td>'.$r2->defect113.'</td>
						<td>'.$r2->defect114.'</td>
						<td>'.$r2->defect115.'</td>
						<td>'.$r2->defect116.'</td>
						<td>'.$r2->defect117.'</td>
						<td>'.$r2->defect118.'</td>
						<td>'.$r2->defect119.'</td>
						<td>'.$r2->defect120.'</td>
						<td>'.$r2->defect121.'</td>
						<td>'.$r2->defect122.'</td>
						<td>'.$r2->defect123.'</td>
						<td>'.$r2->defect124.'</td>
						<td>'.$r2->defect125.'</td>
						<td>'.$r2->defect126.'</td>
						<td>'.$r2->defect127.'</td>
						<td>'.$r2->defect128.'</td>
						<td>'.$r2->defect129.'</td>
						<td>'.$r2->defect130.'</td>
						<td>'.$r2->defect131.'</td>
						<td>'.$r2->defect132.'</td>
						<td>'.$r2->defect133.'</td>
						<td>'.$r2->defect134.'</td>
						<td>'.$r2->defect135.'</td>
						<td>'.$r2->defect136.'</td>
						<td>'.$r2->defect137.'</td>
						<td>'.$r2->defect138.'</td>
						<td>'.$r2->defect139.'</td>
						<td>'.$r2->defect140.'</td>
						<td>'.$r2->defect141.'</td>
						<td>'.$r2->defect142.'</td>
						<td>'.$r2->defect143.'</td>
						<td>'.$r2->defect144.'</td>
						<td>'.$r2->defect145.'</td>
						<td>'.$r2->defect146.'</td>
						<td>'.$r2->defect147.'</td>
						<td>'.$r2->defect148.'</td>
						<td>'.$r2->defect149.'</td>
						<td>'.$r2->defect150.'</td>
						<td>'.$r2->defect151.'</td>
						<td>'.$r2->defect152.'</td>
						<td>'.$r2->defect153.'</td>
						<td>'.$r2->defect154.'</td>
						<td>'.$r2->defect155.'</td>
						<td>'.$r2->defect156.'</td>
						<td>'.$r2->defect157.'</td>
						<td>'.$r2->defect158.'</td>
						<td>'.$r2->defect159.'</td>
						<td>'.$r2->defect160.'</td>
						<td>'.$r2->defect161.'</td>
						<td>'.$r2->defect162.'</td>
						<td>'.$r2->defect163.'</td>
						<td>'.$r2->defect164.'</td>
						<td>'.$r2->defect165.'</td>
						<td>'.$r2->defect166.'</td>
						<td>'.$r2->defect167.'</td>
						<td>'.$r2->defect168.'</td>
						<td>'.$r2->defect169.'</td>
						<td>'.$r2->defect170.'</td>
						<td>'.$r2->total_defect.'</td>
						<td>'.number_format($rft2,1,',',',').'%'.'</td>
						</tr>';
			}

			foreach ($details3->result() as $key => $r3) {
				$rft3  = (($r3->total_checked-$r3->total_defect)/$r3->total_checked)*100;
				$data = $data.'<tr>
						<td>'.$r3->create_date.'</td>
						<td>'.$line.'</td>
						<td> 3 </td>
						<td>'.$r3->total_checked.'</td>
						<td>'.$r3->defect1.'</td>
						<td>'.$r3->defect2.'</td>
						<td>'.$r3->defect3.'</td>
						<td>'.$r3->defect4.'</td>
						<td>'.$r3->defect5.'</td>
						<td>'.$r3->defect6.'</td>
						<td>'.$r3->defect7.'</td>
						<td>'.$r3->defect8.'</td>
						<td>'.$r3->defect9.'</td>
						<td>'.$r3->defect10.'</td>
						<td>'.$r3->defect11.'</td>
						<td>'.$r3->defect12.'</td>
						<td>'.$r3->defect13.'</td>
						<td>'.$r3->defect14.'</td>
						<td>'.$r3->defect15.'</td>
						<td>'.$r3->defect16.'</td>
						<td>'.$r3->defect17.'</td>
						<td>'.$r3->defect18.'</td>
						<td>'.$r3->defect19.'</td>
						<td>'.$r3->defect20.'</td>
						<td>'.$r3->defect21.'</td>
						<td>'.$r3->defect22.'</td>
						<td>'.$r3->defect23.'</td>
						<td>'.$r3->defect24.'</td>
						<td>'.$r3->defect25.'</td>
						<td>'.$r3->defect26.'</td>
						<td>'.$r3->defect27.'</td>
						<td>'.$r3->defect28.'</td>
						<td>'.$r3->defect29.'</td>
						<td>'.$r3->defect30.'</td>
						<td>'.$r3->defect31.'</td>
						<td>'.$r3->defect32.'</td>
						<td>'.$r3->defect33.'</td>
						<td>'.$r3->defect34.'</td>
						<td>'.$r3->defect35.'</td>
						<td>'.$r3->defect36.'</td>
						<td>'.$r3->defect37.'</td>
						<td>'.$r3->defect38.'</td>
						<td>'.$r3->defect39.'</td>
						<td>'.$r3->defect40.'</td>
						<td>'.$r3->defect41.'</td>
						<td>'.$r3->defect42.'</td>
						<td>'.$r3->defect43.'</td>
						<td>'.$r3->defect44.'</td>
						<td>'.$r3->defect45.'</td>
						<td>'.$r3->defect46.'</td>
						<td>'.$r3->defect47.'</td>
						<td>'.$r3->defect48.'</td>
						<td>'.$r3->defect49.'</td>
						<td>'.$r3->defect50.'</td>
						<td>'.$r3->defect51.'</td>
						<td>'.$r3->defect52.'</td>
						<td>'.$r3->defect53.'</td>
						<td>'.$r3->defect54.'</td>
						<td>'.$r3->defect55.'</td>
						<td>'.$r3->defect56.'</td>
						<td>'.$r3->defect57.'</td>
						<td>'.$r3->defect58.'</td>
						<td>'.$r3->defect59.'</td>
						<td>'.$r3->defect60.'</td>
						<td>'.$r3->defect61.'</td>
						<td>'.$r3->defect62.'</td>
						<td>'.$r3->defect63.'</td>
						<td>'.$r3->defect64.'</td>
						<td>'.$r3->defect65.'</td>
						<td>'.$r3->defect66.'</td>
						<td>'.$r3->defect67.'</td>
						<td>'.$r3->defect68.'</td>
						<td>'.$r3->defect69.'</td>
						<td>'.$r3->defect70.'</td>
						<td>'.$r3->defect71.'</td>
						<td>'.$r3->defect72.'</td>
						<td>'.$r3->defect73.'</td>
						<td>'.$r3->defect74.'</td>
						<td>'.$r3->defect75.'</td>
						<td>'.$r3->defect76.'</td>
						<td>'.$r3->defect77.'</td>
						<td>'.$r3->defect78.'</td>
						<td>'.$r3->defect79.'</td>
						<td>'.$r3->defect80.'</td>
						<td>'.$r3->defect81.'</td>
						<td>'.$r3->defect82.'</td>
						<td>'.$r3->defect83.'</td>
						<td>'.$r3->defect84.'</td>
						<td>'.$r3->defect85.'</td>
						<td>'.$r3->defect86.'</td>
						<td>'.$r3->defect87.'</td>
						<td>'.$r3->defect88.'</td>
						<td>'.$r3->defect89.'</td>
						<td>'.$r3->defect90.'</td>
						<td>'.$r3->defect91.'</td>
						<td>'.$r3->defect92.'</td>
						<td>'.$r3->defect93.'</td>
						<td>'.$r3->defect94.'</td>
						<td>'.$r3->defect95.'</td>
						<td>'.$r3->defect96.'</td>
						<td>'.$r3->defect97.'</td>
						<td>'.$r3->defect98.'</td>
						<td>'.$r3->defect99.'</td>
						<td>'.$r3->defect100.'</td>
						<td>'.$r3->defect101.'</td>
						<td>'.$r3->defect102.'</td>
						<td>'.$r3->defect103.'</td>
						<td>'.$r3->defect104.'</td>
						<td>'.$r3->defect105.'</td>
						<td>'.$r3->defect106.'</td>
						<td>'.$r3->defect107.'</td>
						<td>'.$r3->defect108.'</td>
						<td>'.$r3->defect109.'</td>
						<td>'.$r3->defect110.'</td>
						<td>'.$r3->defect111.'</td>
						<td>'.$r3->defect112.'</td>
						<td>'.$r3->defect113.'</td>
						<td>'.$r3->defect114.'</td>
						<td>'.$r3->defect115.'</td>
						<td>'.$r3->defect116.'</td>
						<td>'.$r3->defect117.'</td>
						<td>'.$r3->defect118.'</td>
						<td>'.$r3->defect119.'</td>
						<td>'.$r3->defect120.'</td>
						<td>'.$r3->defect121.'</td>
						<td>'.$r3->defect122.'</td>
						<td>'.$r3->defect123.'</td>
						<td>'.$r3->defect124.'</td>
						<td>'.$r3->defect125.'</td>
						<td>'.$r3->defect126.'</td>
						<td>'.$r3->defect127.'</td>
						<td>'.$r3->defect128.'</td>
						<td>'.$r3->defect129.'</td>
						<td>'.$r3->defect130.'</td>
						<td>'.$r3->defect131.'</td>
						<td>'.$r3->defect132.'</td>
						<td>'.$r3->defect133.'</td>
						<td>'.$r3->defect134.'</td>
						<td>'.$r3->defect135.'</td>
						<td>'.$r3->defect136.'</td>
						<td>'.$r3->defect137.'</td>
						<td>'.$r3->defect138.'</td>
						<td>'.$r3->defect139.'</td>
						<td>'.$r3->defect140.'</td>
						<td>'.$r3->defect141.'</td>
						<td>'.$r3->defect142.'</td>
						<td>'.$r3->defect143.'</td>
						<td>'.$r3->defect144.'</td>
						<td>'.$r3->defect145.'</td>
						<td>'.$r3->defect146.'</td>
						<td>'.$r3->defect147.'</td>
						<td>'.$r3->defect148.'</td>
						<td>'.$r3->defect149.'</td>
						<td>'.$r3->defect150.'</td>
						<td>'.$r3->defect151.'</td>
						<td>'.$r3->defect152.'</td>
						<td>'.$r3->defect153.'</td>
						<td>'.$r3->defect154.'</td>
						<td>'.$r3->defect155.'</td>
						<td>'.$r3->defect156.'</td>
						<td>'.$r3->defect157.'</td>
						<td>'.$r3->defect158.'</td>
						<td>'.$r3->defect159.'</td>
						<td>'.$r3->defect160.'</td>
						<td>'.$r3->defect161.'</td>
						<td>'.$r3->defect162.'</td>
						<td>'.$r3->defect163.'</td>
						<td>'.$r3->defect164.'</td>
						<td>'.$r3->defect165.'</td>
						<td>'.$r3->defect166.'</td>
						<td>'.$r3->defect167.'</td>
						<td>'.$r3->defect168.'</td>
						<td>'.$r3->defect169.'</td>
						<td>'.$r3->defect170.'</td>
						<td>'.$r3->total_defect.'</td>
						<td>'.number_format($rft3,1,',',',').'%'.'</td>
						</tr>';
			}

			foreach ($details4->result() as $key => $r4) {
				$rft4  = (($r4->total_checked-$r4->total_defect)/$r4->total_checked)*100;
				$data = $data.'<tr>
						<td>'.$r4->create_date.'</td>
						<td>'.$line.'</td>
						<td> 4 </td>
						<td>'.$r4->total_checked.'</td>
						<td>'.$r4->defect1.'</td>
						<td>'.$r4->defect2.'</td>
						<td>'.$r4->defect3.'</td>
						<td>'.$r4->defect4.'</td>
						<td>'.$r4->defect5.'</td>
						<td>'.$r4->defect6.'</td>
						<td>'.$r4->defect7.'</td>
						<td>'.$r4->defect8.'</td>
						<td>'.$r4->defect9.'</td>
						<td>'.$r4->defect10.'</td>
						<td>'.$r4->defect11.'</td>
						<td>'.$r4->defect12.'</td>
						<td>'.$r4->defect13.'</td>
						<td>'.$r4->defect14.'</td>
						<td>'.$r4->defect15.'</td>
						<td>'.$r4->defect16.'</td>
						<td>'.$r4->defect17.'</td>
						<td>'.$r4->defect18.'</td>
						<td>'.$r4->defect19.'</td>
						<td>'.$r4->defect20.'</td>
						<td>'.$r4->defect21.'</td>
						<td>'.$r4->defect22.'</td>
						<td>'.$r4->defect23.'</td>
						<td>'.$r4->defect24.'</td>
						<td>'.$r4->defect25.'</td>
						<td>'.$r4->defect26.'</td>
						<td>'.$r4->defect27.'</td>
						<td>'.$r4->defect28.'</td>
						<td>'.$r4->defect29.'</td>
						<td>'.$r4->defect30.'</td>
						<td>'.$r4->defect31.'</td>
						<td>'.$r4->defect32.'</td>
						<td>'.$r4->defect33.'</td>
						<td>'.$r4->defect34.'</td>
						<td>'.$r4->defect35.'</td>
						<td>'.$r4->defect36.'</td>
						<td>'.$r4->defect37.'</td>
						<td>'.$r4->defect38.'</td>
						<td>'.$r4->defect39.'</td>
						<td>'.$r4->defect40.'</td>
						<td>'.$r4->defect41.'</td>
						<td>'.$r4->defect42.'</td>
						<td>'.$r4->defect43.'</td>
						<td>'.$r4->defect44.'</td>
						<td>'.$r4->defect45.'</td>
						<td>'.$r4->defect46.'</td>
						<td>'.$r4->defect47.'</td>
						<td>'.$r4->defect48.'</td>
						<td>'.$r4->defect49.'</td>
						<td>'.$r4->defect50.'</td>
						<td>'.$r4->defect51.'</td>
						<td>'.$r4->defect52.'</td>
						<td>'.$r4->defect53.'</td>
						<td>'.$r4->defect54.'</td>
						<td>'.$r4->defect55.'</td>
						<td>'.$r4->defect56.'</td>
						<td>'.$r4->defect57.'</td>
						<td>'.$r4->defect58.'</td>
						<td>'.$r4->defect59.'</td>
						<td>'.$r4->defect60.'</td>
						<td>'.$r4->defect61.'</td>
						<td>'.$r4->defect62.'</td>
						<td>'.$r4->defect63.'</td>
						<td>'.$r4->defect64.'</td>
						<td>'.$r4->defect65.'</td>
						<td>'.$r4->defect66.'</td>
						<td>'.$r4->defect67.'</td>
						<td>'.$r4->defect68.'</td>
						<td>'.$r4->defect69.'</td>
						<td>'.$r4->defect70.'</td>
						<td>'.$r4->defect71.'</td>
						<td>'.$r4->defect72.'</td>
						<td>'.$r4->defect73.'</td>
						<td>'.$r4->defect74.'</td>
						<td>'.$r4->defect75.'</td>
						<td>'.$r4->defect76.'</td>
						<td>'.$r4->defect77.'</td>
						<td>'.$r4->defect78.'</td>
						<td>'.$r4->defect79.'</td>
						<td>'.$r4->defect80.'</td>
						<td>'.$r4->defect81.'</td>
						<td>'.$r4->defect82.'</td>
						<td>'.$r4->defect83.'</td>
						<td>'.$r4->defect84.'</td>
						<td>'.$r4->defect85.'</td>
						<td>'.$r4->defect86.'</td>
						<td>'.$r4->defect87.'</td>
						<td>'.$r4->defect88.'</td>
						<td>'.$r4->defect89.'</td>
						<td>'.$r4->defect90.'</td>
						<td>'.$r4->defect91.'</td>
						<td>'.$r4->defect92.'</td>
						<td>'.$r4->defect93.'</td>
						<td>'.$r4->defect94.'</td>
						<td>'.$r4->defect95.'</td>
						<td>'.$r4->defect96.'</td>
						<td>'.$r4->defect97.'</td>
						<td>'.$r4->defect98.'</td>
						<td>'.$r4->defect99.'</td>
						<td>'.$r4->defect100.'</td>
						<td>'.$r4->defect101.'</td>
						<td>'.$r4->defect102.'</td>
						<td>'.$r4->defect103.'</td>
						<td>'.$r4->defect104.'</td>
						<td>'.$r4->defect105.'</td>
						<td>'.$r4->defect106.'</td>
						<td>'.$r4->defect107.'</td>
						<td>'.$r4->defect108.'</td>
						<td>'.$r4->defect109.'</td>
						<td>'.$r4->defect110.'</td>
						<td>'.$r4->defect111.'</td>
						<td>'.$r4->defect112.'</td>
						<td>'.$r4->defect113.'</td>
						<td>'.$r4->defect114.'</td>
						<td>'.$r4->defect115.'</td>
						<td>'.$r4->defect116.'</td>
						<td>'.$r4->defect117.'</td>
						<td>'.$r4->defect118.'</td>
						<td>'.$r4->defect119.'</td>
						<td>'.$r4->defect120.'</td>
						<td>'.$r4->defect121.'</td>
						<td>'.$r4->defect122.'</td>
						<td>'.$r4->defect123.'</td>
						<td>'.$r4->defect124.'</td>
						<td>'.$r4->defect125.'</td>
						<td>'.$r4->defect126.'</td>
						<td>'.$r4->defect127.'</td>
						<td>'.$r4->defect128.'</td>
						<td>'.$r4->defect129.'</td>
						<td>'.$r4->defect130.'</td>
						<td>'.$r4->defect131.'</td>
						<td>'.$r4->defect132.'</td>
						<td>'.$r4->defect133.'</td>
						<td>'.$r4->defect134.'</td>
						<td>'.$r4->defect135.'</td>
						<td>'.$r4->defect136.'</td>
						<td>'.$r4->defect137.'</td>
						<td>'.$r4->defect138.'</td>
						<td>'.$r4->defect139.'</td>
						<td>'.$r4->defect140.'</td>
						<td>'.$r4->defect141.'</td>
						<td>'.$r4->defect142.'</td>
						<td>'.$r4->defect143.'</td>
						<td>'.$r4->defect144.'</td>
						<td>'.$r4->defect145.'</td>
						<td>'.$r4->defect146.'</td>
						<td>'.$r4->defect147.'</td>
						<td>'.$r4->defect148.'</td>
						<td>'.$r4->defect149.'</td>
						<td>'.$r4->defect150.'</td>
						<td>'.$r4->defect151.'</td>
						<td>'.$r4->defect152.'</td>
						<td>'.$r4->defect153.'</td>
						<td>'.$r4->defect154.'</td>
						<td>'.$r4->defect155.'</td>
						<td>'.$r4->defect156.'</td>
						<td>'.$r4->defect157.'</td>
						<td>'.$r4->defect158.'</td>
						<td>'.$r4->defect159.'</td>
						<td>'.$r4->defect160.'</td>
						<td>'.$r4->defect161.'</td>
						<td>'.$r4->defect162.'</td>
						<td>'.$r4->defect163.'</td>
						<td>'.$r4->defect164.'</td>
						<td>'.$r4->defect165.'</td>
						<td>'.$r4->defect166.'</td>
						<td>'.$r4->defect167.'</td>
						<td>'.$r4->defect168.'</td>
						<td>'.$r4->defect169.'</td>
						<td>'.$r4->defect170.'</td>
						<td>'.$r4->total_defect.'</td>
						<td>'.number_format($rft4,1,',',',').'%'.'</td>
						</tr>';
			}
			foreach ($details5->result() as $key => $r5) {
				$rft5  = (($r5->total_checked-$r4->total_defect)/$r5->total_checked)*100;
				$data = $data.'<tr>
						<td>'.$r5->create_date.'</td>
						<td>'.$line.'</td>
						<td> 5 </td>
						<td>'.$r5->total_checked.'</td>
						<td>'.$r5->defect1.'</td>
						<td>'.$r5->defect2.'</td>
						<td>'.$r5->defect3.'</td>
						<td>'.$r5->defect4.'</td>
						<td>'.$r5->defect5.'</td>
						<td>'.$r5->defect6.'</td>
						<td>'.$r5->defect7.'</td>
						<td>'.$r5->defect8.'</td>
						<td>'.$r5->defect9.'</td>
						<td>'.$r5->defect10.'</td>
						<td>'.$r5->defect11.'</td>
						<td>'.$r5->defect12.'</td>
						<td>'.$r5->defect13.'</td>
						<td>'.$r5->defect14.'</td>
						<td>'.$r5->defect15.'</td>
						<td>'.$r5->defect16.'</td>
						<td>'.$r5->defect17.'</td>
						<td>'.$r5->defect18.'</td>
						<td>'.$r5->defect19.'</td>
						<td>'.$r5->defect20.'</td>
						<td>'.$r5->defect21.'</td>
						<td>'.$r5->defect22.'</td>
						<td>'.$r5->defect23.'</td>
						<td>'.$r5->defect24.'</td>
						<td>'.$r5->defect25.'</td>
						<td>'.$r5->defect26.'</td>
						<td>'.$r5->defect27.'</td>
						<td>'.$r5->defect28.'</td>
						<td>'.$r5->defect29.'</td>
						<td>'.$r5->defect30.'</td>
						<td>'.$r5->defect31.'</td>
						<td>'.$r5->defect32.'</td>
						<td>'.$r5->defect33.'</td>
						<td>'.$r5->defect34.'</td>
						<td>'.$r5->defect35.'</td>
						<td>'.$r5->defect36.'</td>
						<td>'.$r5->defect37.'</td>
						<td>'.$r5->defect38.'</td>
						<td>'.$r5->defect39.'</td>
						<td>'.$r5->defect40.'</td>
						<td>'.$r5->defect41.'</td>
						<td>'.$r5->defect42.'</td>
						<td>'.$r5->defect43.'</td>
						<td>'.$r5->defect44.'</td>
						<td>'.$r5->defect45.'</td>
						<td>'.$r5->defect46.'</td>
						<td>'.$r5->defect47.'</td>
						<td>'.$r5->defect48.'</td>
						<td>'.$r5->defect49.'</td>
						<td>'.$r5->defect50.'</td>
						<td>'.$r5->defect51.'</td>
						<td>'.$r5->defect52.'</td>
						<td>'.$r5->defect53.'</td>
						<td>'.$r5->defect54.'</td>
						<td>'.$r5->defect55.'</td>
						<td>'.$r5->defect56.'</td>
						<td>'.$r5->defect57.'</td>
						<td>'.$r5->defect58.'</td>
						<td>'.$r5->defect59.'</td>
						<td>'.$r5->defect60.'</td>
						<td>'.$r5->defect61.'</td>
						<td>'.$r5->defect62.'</td>
						<td>'.$r5->defect63.'</td>
						<td>'.$r5->defect64.'</td>
						<td>'.$r5->defect65.'</td>
						<td>'.$r5->defect66.'</td>
						<td>'.$r5->defect67.'</td>
						<td>'.$r5->defect68.'</td>
						<td>'.$r5->defect69.'</td>
						<td>'.$r5->defect70.'</td>
						<td>'.$r5->defect71.'</td>
						<td>'.$r5->defect72.'</td>
						<td>'.$r5->defect73.'</td>
						<td>'.$r5->defect74.'</td>
						<td>'.$r5->defect75.'</td>
						<td>'.$r5->defect76.'</td>
						<td>'.$r5->defect77.'</td>
						<td>'.$r5->defect78.'</td>
						<td>'.$r5->defect79.'</td>
						<td>'.$r5->defect80.'</td>
						<td>'.$r5->defect81.'</td>
						<td>'.$r5->defect82.'</td>
						<td>'.$r5->defect83.'</td>
						<td>'.$r5->defect84.'</td>
						<td>'.$r5->defect85.'</td>
						<td>'.$r5->defect86.'</td>
						<td>'.$r5->defect87.'</td>
						<td>'.$r5->defect88.'</td>
						<td>'.$r5->defect89.'</td>
						<td>'.$r5->defect90.'</td>
						<td>'.$r5->defect91.'</td>
						<td>'.$r5->defect92.'</td>
						<td>'.$r5->defect93.'</td>
						<td>'.$r5->defect94.'</td>
						<td>'.$r5->defect95.'</td>
						<td>'.$r5->defect96.'</td>
						<td>'.$r5->defect97.'</td>
						<td>'.$r5->defect98.'</td>
						<td>'.$r5->defect99.'</td>
						<td>'.$r5->defect100.'</td>
						<td>'.$r5->defect101.'</td>
						<td>'.$r5->defect102.'</td>
						<td>'.$r5->defect103.'</td>
						<td>'.$r5->defect104.'</td>
						<td>'.$r5->defect105.'</td>
						<td>'.$r5->defect106.'</td>
						<td>'.$r5->defect107.'</td>
						<td>'.$r5->defect108.'</td>
						<td>'.$r5->defect109.'</td>
						<td>'.$r5->defect110.'</td>
						<td>'.$r5->defect111.'</td>
						<td>'.$r5->defect112.'</td>
						<td>'.$r5->defect113.'</td>
						<td>'.$r5->defect114.'</td>
						<td>'.$r5->defect115.'</td>
						<td>'.$r5->defect116.'</td>
						<td>'.$r5->defect117.'</td>
						<td>'.$r5->defect118.'</td>
						<td>'.$r5->defect119.'</td>
						<td>'.$r5->defect120.'</td>
						<td>'.$r5->defect121.'</td>
						<td>'.$r5->defect122.'</td>
						<td>'.$r5->defect123.'</td>
						<td>'.$r5->defect124.'</td>
						<td>'.$r5->defect125.'</td>
						<td>'.$r5->defect126.'</td>
						<td>'.$r5->defect127.'</td>
						<td>'.$r5->defect128.'</td>
						<td>'.$r5->defect129.'</td>
						<td>'.$r5->defect130.'</td>
						<td>'.$r5->defect131.'</td>
						<td>'.$r5->defect132.'</td>
						<td>'.$r5->defect133.'</td>
						<td>'.$r5->defect134.'</td>
						<td>'.$r5->defect135.'</td>
						<td>'.$r5->defect136.'</td>
						<td>'.$r5->defect137.'</td>
						<td>'.$r5->defect138.'</td>
						<td>'.$r5->defect139.'</td>
						<td>'.$r5->defect140.'</td>
						<td>'.$r5->defect141.'</td>
						<td>'.$r5->defect142.'</td>
						<td>'.$r5->defect143.'</td>
						<td>'.$r5->defect144.'</td>
						<td>'.$r5->defect145.'</td>
						<td>'.$r5->defect146.'</td>
						<td>'.$r5->defect147.'</td>
						<td>'.$r5->defect148.'</td>
						<td>'.$r5->defect149.'</td>
						<td>'.$r5->defect150.'</td>
						<td>'.$r5->defect151.'</td>
						<td>'.$r5->defect152.'</td>
						<td>'.$r5->defect153.'</td>
						<td>'.$r5->defect154.'</td>
						<td>'.$r5->defect155.'</td>
						<td>'.$r5->defect156.'</td>
						<td>'.$r5->defect157.'</td>
						<td>'.$r5->defect158.'</td>
						<td>'.$r5->defect159.'</td>
						<td>'.$r5->defect160.'</td>
						<td>'.$r5->defect161.'</td>
						<td>'.$r5->defect162.'</td>
						<td>'.$r5->defect163.'</td>
						<td>'.$r5->defect164.'</td>
						<td>'.$r5->defect165.'</td>
						<td>'.$r5->defect166.'</td>
						<td>'.$r5->defect167.'</td>
						<td>'.$r5->defect168.'</td>
						<td>'.$r5->defect169.'</td>
						<td>'.$r5->defect170.'</td>
						<td>'.$r5->total_defect.'</td>
						<td>'.number_format($rft5,1,',',',').'%'.'</td>
						</tr>';
			}



			$data = array(
				'tanggal' => $date,
				'week' => $week,
				'data_list'=>$data
			);
 
			$this->load->view('report/download/download_rft_inline_byline', $data);
			
		}
	}
	public function list_inspection()
	{
		$columns = array(
                            // 0 =>'mesin_id',
                            0 =>'sewer_nik',
                            1 =>'sewer_name',
                            2 =>'round1',
                            3 =>'round2',
                            4 =>'round3',
                            5 =>'round4',
                            6 =>'round5',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $factory  = $this->session->userdata('factory');

        $date    = $this->input->post('tanggal');

        $nik_sewer = $this->input->post('nik_sewer');
		$round = $this->input->post('round');
		$color = $this->input->post('color');
		$filter = $this->input->post('filter');

		$master = '';
		$totalFiltered = 0;
		$totalData = 0;

  		if ($date!='' || $nik_sewer!='' || $round!='' || $color!='') {

	        $totalData = $this->ReportModel->allposts_count_reportinspect($date, $nik_sewer, $color, $filter, $factory);

	        $totalFiltered = $totalData;

	        if(empty($this->input->post('search')['value']))
	        {
	            $master = $this->ReportModel->allposts_reportinspect($limit,$start,$order,$dir,$date, $nik_sewer, $color, $filter, $factory);
	        }
	        else {
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_reportinspect($limit,$start,$search,$order,$dir,$date, $nik_sewer, $color, $filter, $factory);

	            $totalFiltered = $this->ReportModel->posts_search_count_reportinspect($search,$date, $nik_sewer, $color, $filter, $factory);
	        }
	    }

        $data = array();
        $nomor_urut = 0;

        if(!empty($master))
        {
            foreach ($master as $master)
            {
				$nestedData['line_name'] 		= $master->line_name;
				$nestedData['proses_name'] 		= $master->proses_name;
				$nestedData['sewer_nik']    	= $master->sewer_nik;
				$nestedData['sewer_name']       = $master->sewer_name;
				$nestedData['round1']     		= "<input type='text' style='background-color:".$master->grade_round1.";border: 0px;text-align: right;width: 75%' value='".$master->round1."' disabled>";
				$nestedData['round2']   		= "<input type='text' style='background-color:".$master->grade_round2.";border: 0px;text-align: right;width: 75%' value='".$master->round2."' disabled>";
				$nestedData['round3']   		= "<input type='text' style='background-color:".$master->grade_round3.";border: 0px;text-align: right;width: 75%' value='".$master->round3."' disabled>";
				$nestedData['round4']   		= "<input type='text' style='background-color:".$master->grade_round4.";border: 0px;text-align: right;width: 75%' value='".$master->round4."' disabled>";
				$nestedData['round5']   		= "<input type='text' style='background-color:".$master->grade_round5.";border: 0px;text-align: right;width: 75%' value='".$master->round5."' disabled>";
				$nestedData['grade_round1']   	= $master->grade_round1;
				$nestedData['grade_round2']   	= $master->grade_round2;
				$nestedData['grade_round3']   	= $master->grade_round3;
				$nestedData['grade_round4']   	= $master->grade_round4;
				$nestedData['grade_round5']   	= $master->grade_round5;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}

	public function list_inspection_operator()
	{
		$columns = array(
                            0 =>'line_id',
                            1 =>'total_score',
                            2 =>'point_avg',
                            3 =>'grade',
                        );
		$limit     = $this->input->post('length');
		$start     = $this->input->post('start');
		$order     = $columns[$this->input->post('order')[0]['column']];
		$dir       = $this->input->post('order')[0]['dir'];
		$draw      = $this->input->post('draw');
		
		$factory   = $this->session->userdata('factory');
		
		$datefrom  = $this->input->post('datefrom');
		$dateto    = $this->input->post('dateto');
		$line_from = $this->input->post('line_from');
		$line_to   = $this->input->post('line_to');
		$nik_sewer = $this->input->post('nik_sewer');
		$grade     = $this->input->post('grade');
		$filter    = $this->input->post('filter');

		$sekarang = date('Y-m-d');
		if($dateto == $sekarang || $datefrom == $sekarang){
			
			$master = '';
			$totalFiltered = 0;
			$totalData = 0;

			if ($dateto!='' || $nik_sewer!='' || $grade!='') {

				$totalData = $this->ReportModel->allposts_count_reportinspect_operator($datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from);

				$totalFiltered = $totalData;

				if(empty($this->input->post('search')['value']))
				{
					$master = $this->ReportModel->allposts_reportinspect_operator($limit,$start,$order,$dir,$datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from);
				}
				else {
					$search = $this->input->post('search')['value'];

					$master =  $this->ReportModel->posts_search_reportinspect_operator($limit,$start,$search,$order,$dir,$datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from);

					$totalFiltered = $this->ReportModel->posts_search_count_reportinspect_operator($search,$datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from);
				}
			}

			$data = array();
			$nomor_urut = 0;

			if(!empty($master))
			{
				
				foreach ($master as $master)
				{
					
					$defect = [];
					if ($master->point_defect!=0) {
						$jenisDefect = $this->ReportModel->getDefect($master->sewer_nik,$master->date,$master->master_proses_id);
					
						foreach ($jenisDefect->result() as $key => $jenis) {

							$defect[] = $jenis->defect_jenis."=".$jenis->jumlah."<br>";
							
						}
					}
					$nestedData['date']        = $master->date;
					$nestedData['line_name']   = $master->line_name;
					$nestedData['sewer_nik']   = ucwords($master->name.'('.$master->sewer_nik.')');
					$nestedData['proses_name'] = ucwords($master->proses_name);
					$nestedData['total_score'] = $master->point;
					$nestedData['point_avg']   = $master->total;
					$nestedData['grade']       = $master->grade;
					$nestedData['jenisDefect'] = implode(',', $defect);
					

					$data[] = $nestedData;
				}
			}
		}
		else{
			
			$master = '';
			$totalFiltered = 0;
			$totalData = 0;

			if ($dateto!='' || $nik_sewer!='' || $grade!='') {

				$totalData = $this->ReportModel->allpost_count_operator($datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from);

				$totalFiltered = $totalData;

				$master = $this->ReportModel->allpost_result_operator($limit,$start,$order,$dir,$datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_to,$line_from);
			}

			$data = array();
			$nomor_urut = 0;

			if(!empty($master))
			{
				foreach ($master as $master)
				{
					$nestedData['date']        = $master->date;
					$nestedData['line_name']   = $master->line_name;
					$nestedData['sewer_nik']   = ucwords($master->name.'('.$master->nik_sewer.')');
					$nestedData['proses_name'] = ucwords($master->nama_proses);
					$nestedData['total_score'] = $master->total_score;
					$nestedData['point_avg']   = $master->total_avg;
					$nestedData['grade']       = $master->grade;
					$nestedData['jenisDefect'] = $master->jenis_defect;
					

					$data[] = $nestedData;
				}
			}
		}

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function report_wft ()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	$data['tanggal'] = $this->input->post('tanggal',TRUE);
        	$data['defect_id'] =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();
        	$this->template->set('title','Report WFT');
            $this->template->set('desc_page','Report WFT');
            $this->template->load('layout','report/wft/index_report_wft',$data);
        }
	}

	public function filter_wft($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$tgl1    = $post['tanggal1'];
			$tgl2    = $post['tanggal2'];

			$data = array (
				'tgl1'    => $tgl1,
				'tgl2'    => $tgl2,
			);
			
			$this->load->view("report/wft/list_report_wft",$data);

			// echo "sukses";
		}
	}

	public function listfwft_ajax()
	{
		$columns = array(
                            0 =>'no_urut',
                            1 =>'line_name',
                            2 =>'style',
                            3 =>'wft'
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

		$dari    = $this->input->post('dari');
		$sampai    = $this->input->post('sampai');
		
  		// if ($date!='') {
		$dari = date('Y-m-d',strtotime($dari));
		$sampai = date('Y-m-d',strtotime($sampai));
  		// }else{
  		// 	$date = date('Y-m-d');
  		// }

        $totalData = $this->ReportModel->allposts_count_reportwft($dari,$sampai);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->ReportModel->allposts_reportwft($limit,$start,$order,$dir,$dari,$sampai);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->ReportModel->posts_search_reportwft($limit,$start,$search,$order,$dir,$dari,$sampai);

            $totalFiltered = $this->ReportModel->posts_search_count_reportwft($search,$dari,$sampai);
        }
		$data          = array();
		$nomor_urut    = 0;
		
        if(!empty($master))
        {	

            foreach ($master as $master)
            {
				// $total_defect_endline = $this->ReportModel->total_defectEndline($master->date,$master->factory_id,$master->line_id);
				// $total_defect_inline  = $this->ReportModel->total_defectInline($master->date,$master->factory_id,$master->line_id);

				// $defectInline         =0;
				// $defectEndline        =0;
            	// foreach ($total_defect_endline as $key => $defect) {
            	// 	$defectEndline = $defect->defect_endline;
            	// }
            	// foreach ($total_defect_inline as $key => $defect_inline) {
            	// 	$defectInline = $defect_inline->defect_inline;
				// }
				
				$defect_inline  = $master->total_inline;
				$defect_endline = $master->total_endline;
				$total_good     = $master->total_qc;

            	$hitung_wft = 100*(((int)$defect_endline+(int)$defect_inline)/(int)$total_good);

				$wft                         = number_format((float)$hitung_wft, 2, '.', '');
				$nestedData['date']          = ucwords($master->date);
				$nestedData['line']          = ucwords($master->line_name);
				$nestedData['total_good']    = $master->total_qc;
				$nestedData['defectinline']  = ($defect_inline!=0?$defect_inline:0);
				$nestedData['defectendline'] = ($defect_endline!=0?$defect_endline:0);
				$nestedData['wft']           = $wft.'%';
				
				$data[]                      = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function downloadwftstyle($value='')
	{
		
		$factory = $this->session->userdata('factory');
		$dari   = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$dari   = ($dari!=''?$dari:date('Y-m-d'));
		$sampai = ($sampai!=''?$sampai:date('Y-m-d'));
		$report = $this->ReportModel->downloadwft_style($dari,$sampai);


		header("Content-Type:application/vnd.ms-excel");
		if($dari!=$sampai){
			header('Content-Disposition:attachment; filename="Report WFT By Style '.$dari.' sd '.$sampai.'.xls"');
		}else{
			header('Content-Disposition:attachment; filename="Report WFT By Style '.$dari.'.xls"');
		}
		
		$draw='';
		
		if ($report!=NULL) {
			foreach ($report as $r) {

				// $defectinlines = $this->db->get_where('wft_inline', array('date'=>$d->date,'style'=>$d->style,'master_line_id'=>$d->line_id,'factory_id'=>$d->factory_id))->result();

				// $defectendlines = $this->db->get_where('wft_endline', array('date'=>$d->date,'style'=>$d->style,'line_id'=>$d->line_id,'factory_id'=>$d->factory_id))->result();

				// $defectInline=0;
				// $defectEndline=0;
				// foreach ($defectinlines as $key => $defectinline) {

				// 	$defectInline = $defectinline->defect_inline;
				// }
				// foreach ($defectendlines as $key => $defectendline) {
				// 	$defectEndline = $defectendline->defect_endline;
				// }
				
				$defect_inline  = $r->total_inline;
				$defect_endline = $r->total_endline;
				$total_good     = $r->total_qc;

				

				if ($total_good>0) {
					$hitung_wft = 100*(((int)$defect_endline+(int)$defect_inline)/(int)$total_good);
					$wft = number_format((float)$hitung_wft, 2, '.', '');
				}else{
					$wft = 0;
				}
				
				
				$draw .='<tr>'.
							'<td>'.$r->date.'</td>'.
							'<td>'.$r->line_name.'</td>'.
							'<td>'.$r->style.'</td>'.
							'<td>'.$r->total_qc.'</td>'.
							'<td>'.($defect_inline!=0?$defect_inline:0).'</td>'.
							'<td>'.($defect_endline!=0?$defect_endline:0).'</td>'.
							'<td>'.$wft.'</td>'.
				       '</tr>';
			}
		}


		$data = array(
			'draw' => $draw
		);

        $this->load->view('report/download/download_wft_bystyle', $data);
	}

	public function downloadwft()
	{
		$factory = $this->session->userdata('factory');
		// $tanggal = $this->input->get('tanggal');

		// $date              = ($tanggal!=''?$tanggal:date('Y-m-d'));

		$dari   = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$dari   = ($dari!=''?$dari:date('Y-m-d'));
		$sampai = ($sampai!=''?$sampai:date('Y-m-d'));
		$report = $this->ReportModel->downloadwft($factory,$dari,$sampai);

		header("Content-Type:application/vnd.ms-excel");
		// header('Content-Disposition:attachment; filename="Report WFT'.$date.'.xls"');

		if($dari!=$sampai){
			header('Content-Disposition:attachment; filename="Report WFT '.$dari.' sd '.$sampai.'.xls"');
		}else{
			header('Content-Disposition:attachment; filename="Report WFT '.$dari.'.xls"');
		}
		$draw='';
		
		if ($report!=NULL) {
			foreach ($report as $r) {
				
				$defect_inline  = $r->total_inline;
				$defect_endline = $r->total_endline;
				$total_good     = $r->total_qc;

				$hitung_wft = 100*(((int)$defect_endline+(int)$defect_inline)/(int)$total_good);
				$wft = number_format((float)$hitung_wft, 2, '.', '');
				
				$draw .='<tr>'.
							'<td>'.$r->date.'</td>'.
							'<td>'.$r->line_name.'</td>'.
							'<td>'.$r->total_qc.'</td>'.
							'<td>'.($defect_inline!=0?$defect_inline:0).'</td>'.
							'<td>'.($defect_endline!=0?$defect_endline:0).'</td>'.
							'<td>'.$wft.'</td>'.
				       '</tr>';
			}
		}


		$data = array(
			'draw' => $draw
		);

        $this->load->view('report/download/download_wft', $data);
	}
	public function downloadWftPo()
	{
		if ($this->input->get('dari') == '')
		{
			echo "Tanggal Awal tidak boleh kosong";
			return FALSE;
		}
		if ($this->input->get('sampai') == '')
		{
			echo "Tanggal Akhir tidak boleh kosong";
			return FALSE;
		}
		$factory = $this->session->userdata('factory');
		$dari    = $this->input->get('dari');
		$sampai  = $this->input->get('sampai');
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('ICT Team')
			->setLastModifiedBy('ICT Team')
			->setTitle("Data Defect Inline & Endline")
			->setSubject("Defect Inline & Endline")
			->setDescription("Defect Inline & Endline")
			->setKeywords("Defect Inline & Endline");
		
		$excel->setActiveSheetIndex(0);
		$dataInline = $this->ReportModel->downloadwftPoInline($dari,$sampai,$factory);
		
		$excel->getActiveSheet()->setCellValue('A1', 'Date');
		$excel->getActiveSheet()->setCellValue('B1', 'Line');
		$excel->getActiveSheet()->setCellValue('C1', 'Style');
		$excel->getActiveSheet()->setCellValue('D1', 'Po Buyer');
		$excel->getActiveSheet()->setCellValue('E1', 'Article');
		$excel->getActiveSheet()->setCellValue('F1', 'Size');
		$excel->getActiveSheet()->setCellValue('G1', 'Kode Defect');
		$excel->getActiveSheet()->setCellValue('H1', 'Nama Defect');
		$excel->getActiveSheet()->setCellValue('I1', 'Total Defect');

		$num_row=2;
		foreach ($dataInline as $key => $inline) {
			$excel->setActiveSheetIndex(0)->setCellValue('A'.$num_row, $inline->date);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$num_row, $inline->line_name);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$num_row, $inline->style);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$num_row, $inline->poreference);
			$excel->setActiveSheetIndex(0)->setCellValue('E'.$num_row, $inline->article);
			$excel->setActiveSheetIndex(0)->setCellValue('F'.$num_row, $inline->size);
			$excel->setActiveSheetIndex(0)->setCellValue('G'.$num_row, $inline->defect_code);
			$excel->setActiveSheetIndex(0)->setCellValue('H'.$num_row, $inline->defect_jenis);
			$excel->setActiveSheetIndex(0)->setCellValue('I'.$num_row, $inline->total);
			$num_row++;
		}
		
		// Rename sheet
		$excel->getActiveSheet()->setTitle('Qc Inline');
		
		// Create a new worksheet, after the default sheet
		$excel->createSheet();
		
		// Add some data to the second sheet, resembling some different data types
		$excel->setActiveSheetIndex(1);

		$dataEndline = $this->ReportModel->downloadwftPoEndline($dari,$sampai,$factory);
		
		$excel->getActiveSheet()->setCellValue('A1', 'Date');
		$excel->getActiveSheet()->setCellValue('B1', 'Line');
		$excel->getActiveSheet()->setCellValue('C1', 'Style');
		$excel->getActiveSheet()->setCellValue('D1', 'Po Buyer');
		$excel->getActiveSheet()->setCellValue('E1', 'Article');
		$excel->getActiveSheet()->setCellValue('F1', 'Size');
		$excel->getActiveSheet()->setCellValue('G1', 'Kode Defect');
		$excel->getActiveSheet()->setCellValue('H1', 'Nama Defect');
		$excel->getActiveSheet()->setCellValue('I1', 'Total Defect');

		$num_rows=2;
		foreach ($dataEndline as $key => $endline) {
			$excel->setActiveSheetIndex(1)->setCellValue('A'.$num_rows, $endline->date);
			$excel->setActiveSheetIndex(1)->setCellValue('B'.$num_rows, $endline->line_name);
			$excel->setActiveSheetIndex(1)->setCellValue('C'.$num_rows, $endline->style);
			$excel->setActiveSheetIndex(1)->setCellValue('D'.$num_rows, $endline->poreference);
			$excel->setActiveSheetIndex(1)->setCellValue('E'.$num_rows, $endline->article);
			$excel->setActiveSheetIndex(1)->setCellValue('F'.$num_rows, $endline->size);
			$excel->setActiveSheetIndex(1)->setCellValue('G'.$num_rows, $endline->defect_code);
			$excel->setActiveSheetIndex(1)->setCellValue('H'.$num_rows, $endline->defect_jenis);
			$excel->setActiveSheetIndex(1)->setCellValue('I'.$num_rows, $endline->total);
			$num_rows++;
		}
		
		// Rename 2nd sheet
		$excel->getActiveSheet()->setTitle('Qc Endline');
		$uuid = $this->uuid->v4();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="report Defect'.$uuid.'".xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
		
	}

	public function measurement_inline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	// $data['tanggal'] = $this->input->post('tanggal',TRUE);
        	$data['tanggal'] = date('Y-m-d');
        	$this->template->set('title','Measurement Inline');
            $this->template->set('desc_page','Measurement  Inline');
            $this->template->load('layout','report/index_measurement_inline',$data);
        }
	}

	public function measurement_endline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	// $data['tanggal'] = $this->input->post('tanggal',TRUE);
        	$data['tanggal'] = date('Y-m-d');
        	$this->template->set('title','Measurement endline');
            $this->template->set('desc_page','Measurement  endline');
            $this->template->load('layout','report/index_measurement_endline',$data);
        }
	}

	public function list_measurementinline_ajax()
	{
		$columns = array(
							0 =>'date',
							1 =>'no_urut',
                            2 =>'line_id',
                            3 =>'line_name',
                            4 =>'sewer_nik',
                            5 =>'sewer_name',
                            6 =>'proses_name',
                            7 =>'round1',
                           	8 =>'round2',
                            9 =>'round3',
                            10 =>'round4',
                            11 =>'round5',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $factory  = $this->session->userdata('factory');

        $dari   = $this->input->post('dari');
        $sampai = $this->input->post('sampai');

		if ($dari!='') {
			$dari = date('Y-m-d',strtotime($dari));
		}else{
			$dari = date('Y-m-d');
		}

		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = date('Y-m-d');
		}

        $line_from = $this->input->post('line_from');
		$line_to   = $this->input->post('line_to');

        $filter    = $this->input->post('filter');

		$master = '';
		$totalFiltered = 0;
		$totalData = 0;

  		if ($dari!='' || $sampai!='' || $line_from!='' || $line_to!='') {

	        $totalData = $this->ReportModel->allposts_count_reportmeasurement($dari, $sampai, $line_from, $line_to, $filter, $factory);

	        $totalFiltered = $totalData;

	        if(empty($this->input->post('search')['value']))
	        {
	            $master = $this->ReportModel->allposts_reportmeasurement($limit,$start,$order,$dir,$dari, $sampai, $line_from, $line_to, $filter, $factory);
	        }
	        else {
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_reportmeasurement($limit,$start,$search,$order,$dir,$dari, $sampai, $line_from, $line_to, $filter, $factory);

	            $totalFiltered = $this->ReportModel->posts_search_count_reportmeasurement($search,$dari, $sampai, $line_from, $line_to, $filter, $factory);
	        }
	    }

        $data = array();
        $nomor_urut = 0;

        if(!empty($master))
        {
            foreach ($master as $master)
            {

            	if ($master->grade_round1 == '' || $master->grade_round1 == null) {
            		$grade_round1 = '#C0C0C0';

            	}else{
            		$grade_round1 = $master->grade_round1;
            	}

            	if ($master->grade_round2 == NULL && $master->grade_round1 != 'red') {
            		$grade_round2 = '#C0C0C0';
            	}elseif ($master->grade_round2 == NULL && $master->grade_round1 == 'red') {
            		$grade_round2 = 'green';
            	
            	}else{
            		$grade_round2 = $master->grade_round2;
            	}

            	if ($master->grade_round3 == NULL && $master->grade_round2 != 'red') {
            		$grade_round3 = '#C0C0C0';
            	}elseif ($master->grade_round3 == NULL && $master->grade_round2 == 'red') {
            		$grade_round3 = 'green';
            	}else{
            		$grade_round3 = $master->grade_round3;
            	}

            	if ($master->grade_round4 == NULL && $master->grade_round3 != 'red') {
            		$grade_round4 = '#C0C0C0';
            	}elseif ($master->grade_round4 == NULL && $master->grade_round3 == 'red') {
            		$grade_round4 = 'green';
            	}else{
            		$grade_round4 = $master->grade_round4;
            	}

            	if ($master->grade_round5 == NULL && $master->grade_round4 != 'red') {
            		$grade_round5 = '#C0C0C0';
            	}elseif ($master->grade_round5 == NULL && $master->grade_round4 == 'red') {
            		$grade_round5 = 'green';
            	}else{
            		$grade_round5 = $master->grade_round5;
            	}


				$nestedData['date'] 			= $master->date;
				$nestedData['line_name'] 		= $master->line_name;
				$nestedData['proses_name'] 		= $master->proses_name;
				$nestedData['sewer_nik']    	= $master->sewer_nik;
				$nestedData['sewer_name']       = $master->sewer_name;
				$nestedData['round1']     		= "<input type='text' style='background-color:".$grade_round1.";border: 0px;text-align: right;width: 75%' value='".$master->round1."' disabled>";
				$nestedData['round2']   		= "<input type='text' style='background-color:".$grade_round2.";border: 0px;text-align: right;width: 75%' value='".$master->round2."' disabled>";
				$nestedData['round3']   		= "<input type='text' style='background-color:".$grade_round3.";border: 0px;text-align: right;width: 75%' value='".$master->round3."' disabled>";
				$nestedData['round4']   		= "<input type='text' style='background-color:".$grade_round4.";border: 0px;text-align: right;width: 75%' value='".$master->round4."' disabled>";
				$nestedData['round5']   		= "<input type='text' style='background-color:".$grade_round5.";border: 0px;text-align: right;width: 75%' value='".$master->round5."' disabled>";
				$nestedData['grade_round1']   	= $master->grade_round1;
				$nestedData['grade_round2']   	= $master->grade_round2;
				$nestedData['grade_round3']   	= $master->grade_round3;
				$nestedData['grade_round4']   	= $master->grade_round4;
				$nestedData['grade_round5']   	= $master->grade_round5;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}

	public function list_measurementendline_ajax()
	{
		$columns = array(
                            0 =>'create_date',
                            1 =>'line_id',
                            2 =>'line_name',
                            3 =>'style',
                            4 =>'i',
                            5 =>'ii',
                            6 =>'iii',
                           	7 =>'iv',
                            8 =>'v',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $factory  = $this->session->userdata('factory');

		$dari   = $this->input->post('dari');
        $sampai = $this->input->post('sampai');

		if ($dari!='') {
			$dari = date('Y-m-d',strtotime($dari));
		}else{
			$dari = date('Y-m-d');
		}

		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = date('Y-m-d');
		}

        $line_from = $this->input->post('line_from');
		$line_to = $this->input->post('line_to');
		$filter = $this->input->post('filter');

		$master = '';
		$totalFiltered = 0;
		$totalData = 0;

  		if ($dari!='' || $sampai!='' || $line_from!='' || $line_to!='') {

	        $totalData = $this->ReportModel->allposts_count_reportmeasurement_endline($dari, $sampai, $line_from, $line_to, $filter, $factory);

	        $totalFiltered = $totalData;

	        if(empty($this->input->post('search')['value']))
	        {
	            $master = $this->ReportModel->allposts_reportmeasurement_endline($limit,$start,$order,$dir,$dari, $sampai, $line_from, $line_to, $filter, $factory);
	        }
	        else {
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_reportmeasurement_endline($limit,$start,$search,$order,$dir,$dari, $sampai, $line_from, $line_to, $filter, $factory);

	            $totalFiltered = $this->ReportModel->posts_search_count_reportmeasurement_endline($search,$dari, $sampai, $line_from, $line_to, $filter, $factory);
	        }
	    }

        $data = array();
        $nomor_urut = 0;

        if(!empty($master))
        {
            foreach ($master as $master)
            {
            	if ($master->i == 0) {
            		$grade_i = '#C0C0C0';
            	}else{
            		$grade_i = 'red';
            	}

            	if ($master->ii == 0 && $master->i == 0) {
            		$grade_ii = '#C0C0C0';
            	}elseif ($master->ii == 0 && $master->i != 0) {
            		$grade_ii = 'green';
            	
            	}else{
            		$grade_ii = 'red';
            	}
            	if ($master->iii == 0 && $master->ii == 0) {
            		$grade_iii = '#C0C0C0';
            	}elseif ($master->iii == 0 && $master->ii != 0) {
            		$grade_iii = 'green';
            	
            	}else{
            		$grade_iii = 'red';
            	}
            	if ($master->iv == 0 && $master->iii == 0) {
            		$grade_iv = '#C0C0C0';
            	}elseif ($master->iv == 0 && $master->iii != 0) {
            		$grade_iv = 'green';
            	
            	}else{
            		$grade_iv = 'red';
            	}
            	if ($master->v == 0 && $master->iv == 0) {
            		$grade_v = '#C0C0C0';
            	}elseif ($master->v == 0 && $master->iv != 0) {
            		$grade_v = 'green';
            	
            	}else{
            		$grade_v = 'red';
            	}

				$nestedData['create_date'] 		= $master->create_date;
				$nestedData['line_id'] 			= $master->line_id;
				$nestedData['line_name']		= $master->line_name;
				$nestedData['style'] 			= $master->style;
				$nestedData['i']    			= "<input type='text' style='background-color:".$grade_i.";border: 0px;text-align: right;width: 50%' value='".$master->i."' disabled>";
				$nestedData['ii']       		= "<input type='text' style='background-color:".$grade_ii.";border: 0px;text-align: right;width: 50%' value='".$master->ii."' disabled>";
				$nestedData['iii']       		= "<input type='text' style='background-color:".$grade_iii.";border: 0px;text-align: right;width: 50%' value='".$master->iii."' disabled>";
				$nestedData['iv']       		= "<input type='text' style='background-color:".$grade_iv.";border: 0px;text-align: right;width: 50%' value='".$master->iv."' disabled>";
				$nestedData['v']       			= "<input type='text' style='background-color:".$grade_v.";border: 0px;text-align: right;width: 50%' value='".$master->v."' disabled>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function tls_endline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	$data['tanggal'] = $this->input->post('tanggal',TRUE);
        	$data['defect_id'] =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();
        	$this->template->set('title','Daily Inspection');
            $this->template->set('desc_page','Daily Inspection');
            $this->template->load('layout','report/tls/index_tls_endline',$data);
        }
	}
	public function tls_endlline_ajax()
	{
		$columns = array(
                            0 =>'date',
                            1 =>'line_id',
                            2 =>'style',
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');
		
		$dari   = $this->input->post('dari');
		$sampai = $this->input->post('sampai');
		
  		if ($dari!='') {
  			$dari = date('Y-m-d',strtotime($dari));
  		}else{
  			$dari = date('Y-m-d');
		}
		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = date('Y-m-d');
		}


        $totalData = $this->ReportModel->allposts_count_tlsendline($dari,$sampai);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->ReportModel->allposts_tlsendline($limit,$start,$order,$dir,$dari,$sampai);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->ReportModel->posts_search_tlsendline($limit,$start,$search,$order,$dir,$dari,$sampai);

            $totalFiltered = $this->ReportModel->posts_search_count_tlsendline($search,$dari,$sampai);
        }
        $data = array();
        $defect = array();
        
        $nomor_urut = 0;
        $def = 1;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
            	$this->db->where('style', $master->style);
            	$this->db->where('factory_id', $master->factory_id);
            	$this->db->where('date', $master->date);
            	$this->db->where('line_id', $master->line_id);
            	$rft_endline = $this->db->get('rft_endline');
            	$is_have_defect =  count($rft_endline->result());
            	//echo $is_have_defect;
            	//die();

            	$total_defect=array();
            	if($is_have_defect == 0){
            		$total_defect = 0;
            		for ($i=0; $i <= 170 ; $i++) { 
        				$nestedData['defect'.($i+1)] = 0;
        			}
            	}else{
	            	foreach ($rft_endline->result_array() as $key => $rft) {
						
						$total_defect         = $rft['total_defect'];
	            		if ($rft['total_defect']!=0) {
	            			$defect_id =$this->db->get_where('master_defect',array('defect_id<>'=>0))->result();
							foreach ($defect_id as $key => $def) {
								$nestedData['defect'.$def->defect_id] = $rft['defect'.$def->defect_id];
							}
	            		}
	            	}
            	}

            	$nestedData['date']       = $master->date;
				$nestedData['line']       = $master->line_name;
				$nestedData['style']      = $master->style;
				$nestedData['total_good'] = $master->total_good_endline;
				$nestedData['total_defect'] = ($total_defect!=NULL?$total_defect:0);				
				
                $data[] = $nestedData;
            }
        }
       
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
       
        echo json_encode($json_data);
	}
	public function downloadrftendline_bystyle($get=NULL)
	{
		$get = $this->input->get();
		if ($get!=NULL) {

			$dari   = $this->input->get('dari');
			$sampai = $this->input->get('sampai');
			
			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = date('Y-m-d');
			}
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = date('Y-m-d');
			}
			
			// $tanggal =$this->input->get('tanggal');
			$detail_report='';

			// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

			$totGood = $this->ReportModel->downloadReportTlsEndline($dari,$sampai);

			$defect =$this->db->order_by('defect_id','ASC')->get_where('master_defect',array('defect_id<>'=>0))->result();
		
			$list = [];
			$totperdef = [];
			foreach ($totGood->result() as $tG) {
					
				$l['date']=$tG->date;
				$l['line_name']=$tG->line_name;
				$l['style']=$tG->style;
				$l['total_good']=$tG->total_good_endline;
				$l['total_defect']=0;

				foreach ($defect as $df) {
					
					
					$rwhere = array('date'=>$tG->date,'line_id'=>$tG->line_id,'style'=>$tG->style);
					$rft = $this->db->query("SELECT * FROM ns_qcdeftdayline('".$tG->date."',$tG->line_id,'".$tG->style."',$df->defect_id)")->row_array();

					$l['defect_'.$df->defect_id]=isset($rft['qty']) ? (int)$rft['qty'] : 0;
					$l['total_defect'] +=(isset($rft['qty']) ? (int)$rft['qty'] : 0);
					if (array_key_exists('defect_'.$df->defect_id,$totperdef)==false) {
						$totperdef['defect_'.$df->defect_id]=isset($rft['qty']) ? (int)$rft['qty'] : 0;
					}else{
						$totperdef['defect_'.$df->defect_id]+=isset($rft['qty']) ? (int)$rft['qty'] : 0;
					}
					
				}


				$list[]=$l;
			}
		
			
			$data = array(
				'dari' => $dari,
				'sampai' => $sampai,
				'defect' => $defect,
				'data' => $list,
				'totdef'=>$totperdef


			);

			
			$this->load->view('report/download/download_tls_endline', $data);
			
		}
	}
	public function download_dataentryendline($get=NULL)
	{
		$get = $this->input->get();
		if ($get!=NULL) {
			
			$dari   = $this->input->get('dari');
			$sampai = $this->input->get('sampai');
			
			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = date('Y-m-d');
			}
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = date('Y-m-d');
			}
			
			// $tanggal =$this->input->get('tanggal');
			$detail_report='';


			$cek_permision = $this->Cekpermision_model->cekpermision(2);
			
			// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

			$totGood = $this->ReportModel->downloadReportTlsEndline($dari,$sampai);
			
			header("Content-Type:application/vnd.ms-excel");
			if($dari != $sampai){
				header('Content-Disposition:attachment; filename="Data Entry Endline '.$dari.' sd '.$sampai.'.xls"');	
			}else{
				header('Content-Disposition:attachment; filename="Data Entry Endline '.$dari.'.xls"');
			}
			foreach ($totGood->result() as $key => $tot) {

				$dataEntry = $this->ReportModel->downloadDataEntryEndline($tot->date,$tot->line_id,$tot->style,$tot->factory_id);

				$is_have_defect =  count($dataEntry->result());
								
				if ($is_have_defect==0) {
					$detail_report .='<tr>'.
									'<td>'.$tot->date.'</td>'.
									'<td>'.$tot->line_name.'</td>'.
									'<td>'.$tot->style.'</td>'.
									'<td>'.$tot->total_good_endline.'</td>'.
									'<td>0</td>'.
									'<td>0</td>';
				}else{
					foreach ($dataEntry->result() as $key => $data) {
						$detail_report .= '<tr><td>'.$tot->date.'</td>'.
										  '<td>'.$tot->line_name.'</td>'.
										  '<td>'.$data->style.'</td>'.
										  '<td>'.$tot->total_good_endline.'</td>'.
										  '<td>'.$data->defect_code.'</td>'.
										  '<td>'.$data->total_defect.'</td></tr>';
					}
				}
			}
			$data = array(
				// 'tanggal'       => $date,
				'detail_report' => $detail_report
			);

			$this->load->view('report/download/download_data_entry_endline', $data);
			
		}

	}
	public function downloaddataentryinline($value='')
	{
		$get = $this->input->get();
		if ($get!=NULL) {
			$tanggal =$this->input->get('tanggal');
			$detail_inline='';

			$date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

			$datainline = $this->ReportModel->dataEntryInline($date);
			
			header("Content-Type:application/vnd.ms-excel");
			header('Content-Disposition:attachment; filename="Data Entry Inline '.$date.'.xls"');
			foreach ($datainline->result() as $key => $data) {
				$detail_inline.='<tr>'.
				'<td>'.$data->date.'</td>'.
				'<td>'.$data->line_name.'</td>'.
				'<td>'.$data->round.'</td>'.
				'<td>'.$data->style.'</td>'.
				'<td>'.$data->defect_code.'</td>'.
				'<td>'.$data->total_per_defect.'</td>'.
				'</tr>';
			}
			$data = array(
				'tanggal'       => $date,
				'detail_inline' => $detail_inline
			);
			
			$this->load->view('report/download/download_data_entry_inline', $data);
			
		}
	}

	public function distribution_receive()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);


        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Distribution Receive');
            $this->template->set('desc_page','Distribution Receive');
            $this->template->load('layout','report/production_output/index_distribution_receive');
        }
	}

	public function detail_scanned($get=NULL)
	{
		$get = $this->input->get();

		if ($get!=NULL) {
			// $po   = $get['po'];
			// $size = $get['size'];
			
	
			$article = $get['article'];
			$size    = $get['size'];
			$style   = $get['style'];
			$pobuyer = $get['po'];

			$data = array(
				'poreference' => $pobuyer,
				'style'       => $style,
				'article'     => $article,
				'size'        => $size
			);

			// var_dump($data);
			// die();
			// $data['list_karton']	= $this->FoldingModel->output_folding_detail($po,$size);
			$this->load->view('report/production_output/detail_scanned',$data);			
		}
	}

	public function detail_scanned_ajax($post=NULL)
	{
		$columns = array(
                            0 => 'barcode_id',
                            6 => 'cut_num',
                            8 => 'component_name',
                            10 => 'create_date',
                            11 => 'status'
                        );
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');
		
		$article = $this->input->post('article');
		$size    = $this->input->post('size');
		$style   = $this->input->post('style');
		$pobuyer = $this->input->post('pobuyer');

		$factory = $this->session->userdata('factory');

		$totalData = $this->ReportModel->allposts_count_detail_scanned($pobuyer,$style,$size,$article);

		$totalFiltered = $totalData;
		
		$master = $this->ReportModel->allposts_detail_scanned($limit,$start,$order,$dir,$pobuyer,$style,$size,$article);
		
		$data = array();
		
		
		$nomor_urut = 0;
		if(!empty($master))
		{	
			
			foreach ($master as $master)
			{
				// $from = ($dari==''?'2019-01-01':$dari);
				// $to   = ($sampai==''?date('Y-m-d'):$sampai);

				// $temp = '"'.$master->poreference.'","'.$master->size.'","'.$master->style.'","'.$master->article.'"';
				// $grandTotal    = $this->ReportModel->sum_GrandTotal($master->poreference,$master->size,$master->style,$from,$to,$master->line_id);

				$nestedData['barcode_id']     = $master->barcode_id;
				$nestedData['poreference']    = $master->poreference;
				$nestedData['style']          = $master->style;
				$nestedData['article']        = $master->article;
				$nestedData['size']           = $master->size;
				$nestedData['qty']            = $master->qty;
				$nestedData['cut_num']        = $master->cut_num;
				$nestedData['stiker']         = "".$master->start_num." - ".$master->start_end;
				$nestedData['component_name'] = $master->component_name;
				$nestedData['operator']       = $master->name;
				$nestedData['create_date']    = $master->create_date;
				$nestedData['status']         = $master->status;
				$data       []                = $nestedData;
			}
				
		}
		
		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		
		echo json_encode($json_data);
	}

	public function distribution_scanned_ajax($post=NULL)
	{
		$columns = array(
                            0 => 'date',
                            1 => 'line_id',
                            2 => 'poreference',
                            3 => 'style',
                            4 => 'size'
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
			// $tanggal = $this->input->post('tanggal');
			$dari    = $this->input->post('dari');
			$sampai  = $this->input->post('sampai');
			$pobuyer = $this->input->post('pobuyer');

			$factory = $this->session->userdata('factory');

			if ($dari!='') {
				
				$date1 = date('Y-m-d',strtotime($dari));
				$date2 = date('Y-m-d',strtotime($sampai));
				
				
			}else{
			//   $now = new DateTime();
			//   $now->setTimezone(new DateTimezone('Asia/Jakarta'));
			  $date1 = date('Y-m-d');
			  $date2 = date('Y-m-d');

			}

	        $totalData = $this->ReportModel->allposts_count_distribution_scanned($date1,$factory);

	        $totalFiltered = $totalData;

			// var_dump(empty($pobuyer) && (empty($dari)));
			// die();	
			if(empty($pobuyer) && (empty($dari)))
	        {
	            $master = $this->ReportModel->allposts_distribution_scanned($limit,$start,$order,$dir,$date1,$factory);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_distribution_scanned($limit,$start,$search,$order,$dir,$date1,$date2,$pobuyer,$factory);

	            $totalFiltered = $this->ReportModel->posts_search_count_distribution_scanned($search,$date1,$date2,$pobuyer,$factory);
			}
			
			$data = array();
			
	        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {
					$from = ($dari==''?'2019-01-01':$dari);
					$to   = ($sampai==''?date('Y-m-d'):$sampai);

					$temp = '"'.$master->poreference.'","'.$master->size.'","'.$master->style.'","'.$master->article.'"';
					$grandTotal    = $this->ReportModel->sum_GrandTotal($master->poreference,$master->size,$master->style,$from,$to,$master->line_id);

					$nestedData['date']        = $master->date;
					$nestedData['line']        = $master->line_name;
					$nestedData['poreference'] = $master->poreference;
					$nestedData['style']       = $master->style;
					$nestedData['size']        = $master->size;
					$nestedData['article']     = $master->article;
					$nestedData['total']       = $master->total;
					$nestedData['grandtotal']  = $grandTotal->sum;
					$nestedData['action']      = "<center><a onclick='return detail($temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-align-justify'></i> Detail</a></center>";
					// $nestedData['action']      = $master->status;
					$data[]            		   = $nestedData;
				}
				   
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
	}

	public function distribution_output_ajax($post=NULL)
	{
		$columns = array(
                            0 => 'date',
                            1 => 'line_id',
                            2 => 'poreference',
                            3 => 'style',
                            4 => 'size'
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
			// $tanggal = $this->input->post('tanggal');
			$dari    = $this->input->post('dari');
			$sampai  = $this->input->post('sampai');
			$pobuyer = $this->input->post('pobuyer');

			$factory = $this->session->userdata('factory');

			if ($dari!='') {
				
				$date1 = date('Y-m-d',strtotime($dari));
				$date2 = date('Y-m-d',strtotime($sampai));
				
				
			}else{
			  $now = new DateTime();
			  $now->setTimezone(new DateTimezone('Asia/Jakarta'));
			  $date1 = $now->format('Y-m-d');
			  $date2 = $now->format('Y-m-d');

			}

	        $totalData = $this->ReportModel->allposts_count_distributionrecieve($date1,$factory);

	        $totalFiltered = $totalData;

			// var_dump(empty($pobuyer) && (empty($dari)));
			// die();	
			if(empty($pobuyer) && (empty($dari)))
	        {
	            $master = $this->ReportModel->allposts_distributionrecieve($limit,$start,$order,$dir,$date1,$factory);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_distributionrecieve($limit,$start,$search,$order,$dir,$dari,$sampai,$pobuyer,$factory);

	            $totalFiltered = $this->ReportModel->posts_search_count_distributionrecieve($search,$dari,$sampai,$pobuyer,$factory);
			}
			
			$data = array();
			
	        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {
					$from = ($dari==''?'2019-01-01':$dari);
					$to   = ($sampai==''?date('Y-m-d'):$sampai);
	            
					$grandTotal    = $this->ReportModel->sum_GrandTotal($master->poreference,$master->size,$master->style,$from,$to,$master->line_id);

					$nestedData['date']        = $master->date;
					$nestedData['line']        = $master->line_name;
					$nestedData['poreference'] = $master->poreference;
					$nestedData['style']       = $master->style;
					$nestedData['size']        = $master->size;
					$nestedData['article']     = $master->article;
					$nestedData['total']       = $master->total;
					$nestedData['grandtotal']  = $grandTotal->sum;
					$nestedData['status']      = $master->status;
					$data[]            		   = $nestedData;
				}
				   
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
	}
	
	public function distribution_output_ajax_po($post=NULL)
	{
		$columns = array(
                            0 => 'date',
                            1 => 'line_id',
                            2 => 'poreference',
                            3 => 'style',
                            4 => 'size'
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
			// $tanggal = $this->input->post('tanggal');
			$dari    = $this->input->post('dari');
			$sampai  = $this->input->post('sampai');
			$pobuyer = $this->input->post('pobuyer');

			$factory = $this->session->userdata('factory');

			if ($dari!='') {
				
				$date1 = date('Y-m-d',strtotime($dari));
				$date2 = date('Y-m-d',strtotime($sampai));
				
				
			}else{
			  $now = new DateTime();
			  $now->setTimezone(new DateTimezone('Asia/Jakarta'));
			  $date1 = $now->format('Y-m-d');
			  $date2 = $now->format('Y-m-d');

			}

	        $totalData = $this->ReportModel->allposts_count_distributionrecieve_po($date1,$factory);

	        $totalFiltered = $totalData;

			// var_dump(empty($pobuyer) && (empty($dari)));
			// die();	
			if(empty($pobuyer) && (empty($dari)))
	        {
	            $master = $this->ReportModel->allposts_distributionrecieve_po($limit,$start,$order,$dir,$date1,$factory);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_distributionrecieve_po($limit,$start,$search,$order,$dir,$dari,$sampai,$pobuyer,$factory);

	            $totalFiltered = $this->ReportModel->posts_search_count_distributionrecieve_po($search,$dari,$sampai,$pobuyer,$factory);
			}
			
			$data = array();
			
	        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {
					$from = ($dari==''?'2019-01-01':$dari);
					$to   = ($sampai==''?date('Y-m-d'):$sampai);
	            
					$grandTotal    = $this->ReportModel->sum_GrandTotal_po($master->poreference,$master->style,$from,$to,$master->line_id);

					$nestedData['date']        = $master->date;
					$nestedData['line']        = $master->line_name;
					$nestedData['poreference'] = $master->poreference;
					$nestedData['style']       = $master->style;
					$nestedData['article']     = $master->article;
					$nestedData['total']       = $master->total;
					$nestedData['grandtotal']  = $grandTotal->sum;
					$nestedData['status']      = $master->status;
					$data[]            		   = $nestedData;
				}
				   
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
	}

	public function downloadDistribution_scanned($get=NULL)
	{
		$get = $this->input->get();
		
		$session = $this->session->userdata('logged_in');

		if($session == "true"){
			$dari    = $this->input->get('dari');
			$sampai  = $this->input->get('sampai');
			$pobuyer = $this->input->get('pobuyer');
			$factory = $this->session->userdata('factory');

			if ($dari!='') {
				
				$date1 = date('Y-m-d',strtotime($dari));
				$date2 = date('Y-m-d',strtotime($sampai));
				
				
			}else{
			//   $now = new DateTime();
			//   $now->setTimezone(new DateTimezone('Asia/Jakarta'));
			  $date1 = date('Y-m-d');
			  $date2 = date('Y-m-d');
			}
			
			$report = $this->ReportModel->download_distribution_scanned($date1,$date2,$pobuyer,$factory);

			header("Content-Type:application/vnd.ms-excel");
  			header('Content-Disposition:attachment; filename="Report Recieve Distribution Detail Scan.xls"');
			$draw='';
			if ($report!=NULL) {
				
				foreach ($report as $key => $r) {
					$master = $this->ReportModel->download_detail_scanned($r->poreference,$r->style,$r->size,$r->article);
		
					$grandTotal    = $this->ReportModel->sum_GrandTotal($r->poreference,$r->size,$r->style,$date1,$date2,$r->line_id);
					
					foreach($master as $m){
						$draw .='<tr>'.
						'<td>'.$r->date.'</td>'.
						'<td>'.$r->line_name.'</td>'.
						'<td>'.$r->poreference.'</td>'.
						'<td>'.$r->style.'</td>'.
						'<td>'.$r->article.'</td>'.
						'<td>'.$r->size.'</td>'.
						'<td>'.$r->total.'</td>'.
						'<td>'.$grandTotal->sum.'</td>'.
						// '<td>'.$r->status.'</td>'.
						'<td>'.$m->barcode_id.'</td>'.
						'<td>'.$m->qty.'</td>'.
						'<td>'.$m->cut_num.'</td>'.
						'<td>'.$m->start_num." - ".$m->start_end.'</td>'.
						'<td>'.$m->component_name.'</td>'.
						'<td>'.$m->name.'</td>'.
						'<td>'.$m->create_date.'</td>'.
						'<td>'.$m->status.'</td>'.
				   		'</tr>';
					}
					
				}
			}


			$data = array(
				'report' => $draw
			);

			// var_dump($data);
			// die();
            $this->load->view('report/download/download_distribution_scanned', $data);
		// }
		}
		else{
			redirect('auth');
		}
			
	}

	public function downloadDistribution($get=NULL)
	{
		$get = $this->input->get();
		
		$session = $this->session->userdata('logged_in');

		if($session == "true"){
			$dari = $this->input->get('dari');
			$sampai = $this->input->get('sampai');
			$pobuyer = $this->input->get('pobuyer');
			$factory = $this->session->userdata('factory');

			
			$from = ($dari==''?'2019-01-01':$dari);
			$to   = ($sampai==''?date('Y-m-d'):$sampai);
			// if ($dari!='') {
				
			// 	$date1 = date('Y-m-d',strtotime($dari));
			// 	$date2 = date('Y-m-d',strtotime($sampai));
				
				
			// }else{
			//   $now = new DateTime();
			//   $now->setTimezone(new DateTimezone('Asia/Jakarta'));
			//   $date1 = $now->format('Y-m-d');
			//   $date2 = $now->format('Y-m-d');

			// }
			
			$report = $this->ReportModel->download_outputDistribution($from,$to,$pobuyer,$factory);
			
			if(empty($pobuyer))
	        {
	            $report = $this->ReportModel->download_outputDistribution($from,$to,NULL,$factory);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $report =  $this->ReportModel->download_outputDistribution($from,$to,$pobuyer,$factory);
			}

			header("Content-Type:application/vnd.ms-excel");
  			header('Content-Disposition:attachment; filename="Report Recieve Distribution Detail PO.xls"');
			$draw='';
			if ($report!=NULL) {
				
				foreach ($report as $key => $r) {
					
					$grandTotal    = $this->ReportModel->sum_GrandTotal($r->poreference,$r->size,$r->style,$from,$to,$r->line_id);
					$draw .='<tr>'.
								'<td>'.$r->date.'</td>'.
								'<td>'.$r->line_name.'</td>'.
								'<td>'.$r->poreference.'</td>'.
								'<td>'.$r->style.'</td>'.
								'<td>'.$r->size.'</td>'.
								'<td>'.$r->article.'</td>'.
								'<td>'.$r->total.'</td>'.
								'<td>'.$grandTotal->sum.'</td>'.
								'<td>'.$r->status.'</td>'.
					       '</tr>';
				}
			}


			$data = array(
				'report' => $draw
			);

			// var_dump($data);
			// die();
            $this->load->view('report/download/download_distribution_recieve', $data);
		// }
		}
		else{
			redirect('auth');
		}
			
	}

	public function downloadDistribution_po($get=NULL)
	{
		$get = $this->input->get();
		
		$session = $this->session->userdata('logged_in');

		if($session == "true"){
			$dari = $this->input->get('dari');
			$sampai = $this->input->get('sampai');
			$pobuyer = $this->input->get('pobuyer');
			$factory = $this->session->userdata('factory');

			
			$from = ($dari==''?'2019-01-01':$dari);
			$to   = ($sampai==''?date('Y-m-d'):$sampai);
			// if ($dari!='') {
				
			// 	$date1 = date('Y-m-d',strtotime($dari));
			// 	$date2 = date('Y-m-d',strtotime($sampai));
				
				
			// }else{
			//   $now = new DateTime();
			//   $now->setTimezone(new DateTimezone('Asia/Jakarta'));
			//   $date1 = $now->format('Y-m-d');
			//   $date2 = $now->format('Y-m-d');

			// }
			
			$report = $this->ReportModel->download_outputDistribution_po($from,$to,$pobuyer,$factory);
			
			if(empty($pobuyer))
	        {
	            $report = $this->ReportModel->download_outputDistribution_po($from,$to,NULL,$factory);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $report =  $this->ReportModel->download_outputDistribution_po($from,$to,$pobuyer,$factory);
			}

			header("Content-Type:application/vnd.ms-excel");
  			header('Content-Disposition:attachment; filename="Report Recieve Distribution Summary PO.xls"');
			$draw='';
			if ($report!=NULL) {
				
				foreach ($report as $key => $r) {
					
					$grandTotal    = $this->ReportModel->sum_GrandTotal_po($r->poreference,$r->style,$from,$to,$r->line_id);
					$draw .='<tr>'.
								'<td>'.$r->date.'</td>'.
								'<td>'.$r->line_name.'</td>'.
								'<td>'.$r->poreference.'</td>'.
								'<td>'.$r->style.'</td>'.
								'<td>'.$r->article.'</td>'.
								'<td>'.$r->total.'</td>'.
								'<td>'.$grandTotal->sum.'</td>'.
								'<td>'.$r->status.'</td>'.
					       '</tr>';
				}
			}


			$data = array(
				'report' => $draw
			);

			// var_dump($data);
			// die();
            $this->load->view('report/download/download_distribution_recieve_po', $data);
		// }
		}
		else{
			redirect('auth');
		}
			
	}

	public function filterSummaryDetail($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$dari    = $post['dari'];
			$sampai  = $post['sampai'];
			$po      = $post['po'];
			$laporan = $post['laporan'];

			$data = array (
				'dari'    => $dari,
				'sampai'  => $sampai,
				'po'      => $po,
				'laporan' => $laporan,
			);
			// $cekHeader = $this->QcEndlineModel->cekHeader($header_id,$size_id);
			if ($laporan == 0) {
				$this->load->view("report/production_output/result_distribution_detail",$data);
			}
			elseif($laporan == 1){
				$this->load->view("report/production_output/result_distribution_po",$data);
			}
			else{
				$this->load->view("report/production_output/result_distribution_scanned",$data);
			}

			// echo "sukses";
		}
	}

	public function production_output()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);


        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Production Output');
            $this->template->set('desc_page','Production Output');
            $this->template->load('layout','report/production_output/index_production_output');
        }
	}
	public function production_output_ajax($post=NULL)
	{
		$columns = array(
                            0 =>'date',
                            1 =>'no_urut',
                            2 =>'poreference',
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
	        $filter    = $this->input->post('filter');
	        $pobuyer   = $this->input->post('pobuyer');
	        $line_from = $this->input->post('line_from');
	        $line_to   = $this->input->post('line_to');
	        $balance   = $this->input->post('balance');
	        $factory   = $this->session->userdata('factory');
	        $dari      = $this->input->post('dari');
	        $sampai    = $this->input->post('sampai');
	
			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = 0;
			}
	
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = 0;
			}

	        $totalData = $this->ReportModel->allposts_count_productionoutput($dari,$sampai,$factory);

	        $totalFiltered = $totalData;

	        if(empty(($line_from&&$line_to)||$pobuyer||$balance))
	        {
	            $master = $this->ReportModel->allposts_productionoutput($limit,$start,$order,$dir,$dari,$sampai,$factory);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_productionoutput($limit,$start,$search,$order,$dir,$dari,$sampai,$filter,$pobuyer,$line_from,$line_to,$balance,$factory);

	            $totalFiltered = $this->ReportModel->posts_search_count_productionoutput($search,$dari,$sampai,$filter,$pobuyer,$line_from,$line_to,$balance,$factory);
	        }
	        $data = array();
	        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {

					$nestedData['date']            = $master->date;
					$nestedData['line']            = $master->line_name;
					$nestedData['poreference']     = $master->poreference;
					$nestedData['article']         = $master->article;
					$nestedData['style']           = $master->style;
					$nestedData['size']            = $master->size;
					$nestedData['order']           = $master->order;
					$nestedData['maxqc']           = $master->max;
					$nestedData['daily_output']    = $master->daily_output;
					$nestedData['defect_perday']   = $master->defect_perday;
					$nestedData['balance_per_day'] = $master->balance_per_day;
					$nestedData['b_grade']		   = $master->b_grade;
					$nestedData['c_grade']		   = $master->c_grade;
					
	                $data[] = $nestedData;
	            }
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
	}
	public function downloadproduction($get=NULL)
	{
		$get = $this->input->get();


		if ($get!=NULL) {
			$dari   = $this->input->get('dari');
			$sampai   = $this->input->get('sampai');
			$filter    = $this->input->get('filter');
			$pobuyer   = $this->input->get('pobuyer');
			$line_from = $this->input->get('line_from');
			$line_to   = $this->input->get('line_to');
			$balance   = $this->input->get('balance');
			
			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = 0;
			}
	
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = 0;
			}
			/*if ($tanggal=='') {
	  			$date = date('Y-m-d');
	  		}else{
	  			$date = $tanggal;
	  		}*/
	  		$factory = $this->session->userdata('factory');
	  		
			$report = $this->ReportModel->downloadProduction($dari,$sampai,$filter,$pobuyer,$line_from,$line_to,$balance,$factory);
			

			header("Content-Type:application/vnd.ms-excel");
			
			if($dari!=$sampai){
				header('Content-Disposition:attachment; filename="Report Output Production '.$dari.' sd '.$sampai.'.xls"');
			}
			else{
				header('Content-Disposition:attachment; filename="Report Output Production '.$dari.'.xls"');
			}
			$draw='';
			if ($report!=NULL) {
				
				foreach ($report->result() as $key => $r) {
					
					$draw .='<tr>'.
								'<td>'.$r->date.'</td>'.
								'<td>'.$r->line_name.'</td>'.
								'<td>'.$r->poreference.'</td>'.
								'<td>'.$r->article.'</td>'.
								'<td>'.$r->style.'</td>'.
								'<td>'.$r->size.'</td>'.
								'<td>'.$r->order.'</td>'.
								'<td>'.$r->max.'</td>'.
								'<td>'.$r->daily_output.'</td>'.
								'<td>'.$r->defect_perday.'</td>'.
								'<td>'.$r->balance_per_day.'</td>'.
								'<td>'.$r->b_grade.'</td>'.
								'<td>'.$r->c_grade.'</td>'.
					       '</tr>';
				}
			}


			$data = array(
				'draw' => $draw
			);

            $this->load->view('report/download/download_production_output', $data);
		}
	}

	public function line_performance()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	$this->template->set('title','Line Performance');
            $this->template->set('desc_page','Line Performance');
            $this->template->load('layout','report/performance/index_line_performance');
        }
	}
	public function line_performance_ajax()
	{
		$columns = array(
                            0 =>'line_id',
                            1 =>'line_name',
                            2 =>'style',
                            3 =>'wft'
                        );
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $date    = $this->input->post('tanggal');
  		if ($date!='') {
  			$date = date('Y-m-d',strtotime($date));
  		}else{
  			$date = date('Y-m-d');
  		}

        $totalData = $this->ReportModel->allposts_count_lineperformance($date);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $master = $this->ReportModel->allposts_lineperformance($limit,$start,$order,$dir,$date);
        }
        else {
            $search = $this->input->post('search')['value'];

            $master =  $this->ReportModel->posts_search_lineperformance($limit,$start,$search,$order,$dir,$date);

            $totalFiltered = $this->ReportModel->posts_search_count_lineperformance($search,$date);
        }
		$data          = array();
		$nomor_urut    = 0;
		
        if(!empty($master))
        {	

            foreach ($master as $master)
            {
				$total_defect_endline = $this->ReportModel->total_defectEndline($master->date,$master->factory_id,$master->line_id);
				$total_defect_inline  = $this->ReportModel->total_defectInline($master->date,$master->factory_id,$master->line_id);

				$defectInline         =0;
				$defectEndline        =0;
            	foreach ($total_defect_endline as $key => $defect) {
            		$defectEndline = $defect->defect_endline;
            	}
            	foreach ($total_defect_inline as $key => $defect_inline) {
            		$defectInline = $defect_inline->defect_inline;
            	}

            	$hitung_wft = 100*(((int)$defectEndline+(int)$defectInline)/(int)$master->total_good_endline);

				$wft                         = number_format((float)$hitung_wft, 2, '.', '');
				$nestedData['date']          = ucwords($master->date);
				$nestedData['line']          = ucwords($master->line_name);
				$nestedData['total_good']    = $master->total_good_endline;
				$nestedData['defectinline']  = ($defectInline!=0?$defectInline:0);
				$nestedData['defectendline'] = ($defectEndline!=0?$defectEndline:0);
				$nestedData['wft']           = $wft.'%';
				
				$data[]                      = $nestedData;
            }
        }

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function output_folding()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Production Output');
            $this->template->set('desc_page','Production Output');
            $this->template->load('layout','report/production_output/index_production_output_folding');
        }
	}
	public function production_output_folding_ajax($post=NULL)
	{
		$columns = array(
                            0 =>'line_id',
                            1 =>'date',
                            2 =>'poreference',
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
			// $tanggal   = $this->input->post('tanggal');
			$filter    = $this->input->post('filter');
			$pobuyer   = $this->input->post('pobuyer');
			$line_from = $this->input->post('line_from');
			$line_to   = $this->input->post('line_to');
			$balance   = $this->input->post('balance');
	        $factory   = $this->session->userdata('factory');
	        $dari      = $this->input->post('dari');
	        $sampai    = $this->input->post('sampai');

			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = 0;
			}
	
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = 0;
			}

	        $totalData = $this->ReportModel->allposts_count_productionoutputfolding($dari,$sampai,$factory);

	        $totalFiltered = $totalData;

	        if(empty(($line_from&&$line_to)||$pobuyer||$balance))
	        {
	            $master = $this->ReportModel->allposts_productionoutputfolding($limit,$start,$order,$dir,$dari,$sampai,$factory);
	        }else{
	            $search = $this->input->post('search')['value'];

	            $master =  $this->ReportModel->posts_search_productionoutputfolding($limit,$start,$search,$order,$dir,$dari,$sampai,$factory,$filter,$pobuyer,$line_from,$line_to,$balance);

	            $totalFiltered = $this->ReportModel->posts_search_count_productionoutputfolding($search,$dari,$sampai,$factory,$filter,$pobuyer,$line_from,$line_to,$balance);
	        }
	        $data = array();
	        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {
					$nestedData['date']            = $master->date;
					$nestedData['line']            = $master->line_name;
					$nestedData['poreference']     = $master->poreference;
					$nestedData['article']     	   = $master->article;
					$nestedData['style']           = $master->style;
					$nestedData['size']            = $master->size;
					$nestedData['order']           = $master->order;
					$nestedData['total_folding']   = $master->total_folding;
					$nestedData['daily_output']    = $master->daily_output;
					$nestedData['balance_folding'] = $master->balance_folding;			
					
	                $data[] = $nestedData;
	            }
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
	}
	public function downloadfolding($get=NULL)
	{
		$get = $this->input->get();


		if ($get!=NULL) {
			$dari      = $this->input->get('dari');
			$sampai    = $this->input->get('sampai');
			$filter    = $this->input->get('filter');
			$pobuyer   = $this->input->get('pobuyer');
			$line_from = $this->input->get('line_from');
			$line_to   = $this->input->get('line_to');
			$balance   = $this->input->get('balance');

			
			if ($dari!='') {
				$dari = date('Y-m-d',strtotime($dari));
			}else{
				$dari = 0;
			}
	
			if ($sampai!='') {
				$sampai = date('Y-m-d',strtotime($sampai));
			}else{
				$sampai = 0;
			}

			
			$factory = $this->session->userdata('factory');
	  		
			$report = $this->ReportModel->download_outputfolding($dari,$sampai,$factory,$filter,$pobuyer,$line_from,$line_to,$balance);
			

			header("Content-Type:application/vnd.ms-excel");
  			header('Content-Disposition:attachment; filename="Report Output Production Folding .xls"');
			/*$draw='';
			if ($report!=NULL) {
				
				foreach ($report->result() as $key => $r) {
					
					$draw .='<tr>'.
								'<td>'.$r->date.'</td>'.
								'<td>'.$r->line_name.'</td>'.
								'<td>'.$r->poreference.'</td>'.
								'<td>'.$r->style.'</td>'.
								'<td>'.$r->size.'</td>'.
								'<td>'.$r->order.'</td>'.
								'<td>'.$r->total_folding.'</td>'.
								'<td>'.$r->daily_output.'</td>'.
								'<td>'.$r->balance_folding.'</td>'.
					       '</tr>';
				}
			}*/


			$data = array(
				'report' => $report
			);

            $this->load->view('report/download/download_production_output_folding', $data);
		}
	}
	public function hourlyoutput()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','download Output');
            $this->template->set('desc_page','download Output');
            $this->template->load('layout','report/production_output/index_production_output_hourly');
        }
	}
	public function downloadhourlyoutput($post=NULL)
	{
		if ($this->session->userdata('logged_in')=='t') {
			$post = $this->input->post();

			if ($post!=NULL) {
				$date =($post['dari']==$post['sampai']?$post['dari']:$post['dari'].' sampai '.$post['sampai']);
				if ($post['filter']==0) {

					header("Content-Type:application/vnd.ms-excel");
					header('Content-Disposition:attachment; filename="download report output line per jam '.$date.'.xls');
					$data['getLineSummary'] = $this->ReportModel->getLineSummaryPO($post['dari'],$post['sampai']);
					$this->load->view('report/download/download_hourly_output', $data);
				}else if ($post['filter']==1) {
					header("Content-Type:application/vnd.ms-excel");
					header('Content-Disposition:attachment; filename="report output QC per jam '.$date.'.xls');
					$data['getLineSummary'] = $this->ReportModel->getLineSummaryQC($post['dari'],$post['sampai']);
					$this->load->view('report/download/download_hourly_outputqc', $data);
				}else if ($post['filter']==2) {
					header("Content-Type:application/vnd.ms-excel");
					header('Content-Disposition:attachment; filename="report data entry TLS Inline '.$date.'.xls');
					$data['getdataentryinline'] = $this->ReportModel->dataentryinlinerange($post['dari'],$post['sampai']);
					$this->load->view('report/download/download_data_entry_inline_range', $data);
				}
				
			}
		}else{
			redirect('auth','refresh');
		}
	}
	public function fastReactInput()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Fast React Input');
            $this->template->set('desc_page','Fast React Input');
            $this->template->load('layout','report/production_output/index_fast_react_input');
        }
	}
	public function downloadfastReactInput($post=NULL)
	{
		if ($this->session->userdata('logged_in')=='t') {
			 $this->load->helper('csv');
			 $date = $this->input->post("tanggal") ? $this->input->post("tanggal"): date('Y-m-d');
			 
	        $export_arr = array();
	        $fastreact_details = $this->ReportModel->getDetailOutputForFastReact($date);
	        $title = array(
	        	"GROUP NAME", "U.GROUP_EXTERNAL_ID","LINE NAME","U.LINE_EXTERNAL_ID", "U.ORDER", "U.OPERATION", "Strip quantity","TOTAL QTY","TOTAL QTY MADE", "BAL","DATE","U.QTY","U.OPN_COMPLETE"
			);
			
	        array_push($export_arr, $title);
	        if (!empty($fastreact_details)) {
	            foreach ($fastreact_details->result() as $fastreact) {
					//var_dump($fastreact->factory_id);
					
	                $groupname = 'AOI#'.$fastreact->factory_id;
	                $factory = ($fastreact->factory_id==2?25:20);
	               //$qtyMade = $this->ReportModel->getQtyMade($fastreact->inline_header_id);
					array_push($export_arr, array($groupname,$factory,$groupname.$fastreact->desc_name_fastreact,$fastreact->id_fastreact,$fastreact->kst_joborder.'==LIB01',75,$fastreact->qty_ordered_split,$fastreact->qty_ordered, $fastreact->total_output_po,$fastreact->balance,$date,$fastreact->total_output_line,$fastreact->qty_ordered==$fastreact->total_output_line?1:0));
					// array_push($export_arr, $title);
				}
	        }
	        convert_to_csv($export_arr, 'UPDATE SEW.CSV', ',');
		}else{
			redirect('auth','refresh');
		}
	}

	public function report_set()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		// var_dump($this->uri->segment(1));
		// die();

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Report SETS');
            $this->template->set('desc_page','Report SETS');
            $this->template->load('layout','report/style_set/report_set');
        }
	}

	public function report_set_search($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$dari        = $post['dari'];
			$sampai      = $post['sampai'];
			$poreference = $post['poreference'];

			$data = array (
				'dari'    => $dari,
				'sampai'  => $sampai,
				'pobuyer' => $poreference,
			);

			$this->load->view("report/style_set/report_set_result",$data);
		}
	}

	public function report_set_ajax($post=NULL)
	{
		$columns = array(
                            0 =>'date',
                            1 =>'style',
							3 =>'poreference',
							4 =>'size'
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
			$dari        = $this->input->post('dari');
			$sampai      = $this->input->post('sampai');
			$poreference = $this->input->post('poreference');

	  		if ($dari!='') {
	  			$dari = date('Y-m-d',strtotime($dari));
	  		}else{
	  			$dari = date('Y-m-d');
			}
			  
	  		if ($sampai!='') {
	  			$sampai = date('Y-m-d',strtotime($sampai));
	  		}else{
	  			$sampai = date('Y-m-d');
			  }
			  
			if($poreference!=''){
				$poreference = $poreference;
			}
			else{
				$poreference = 0;
			}

	        $totalData = $this->ReportModel->allposts_count_report_set($dari,$sampai,$poreference);

	        $totalFiltered = $totalData;

			$master = $this->ReportModel->allposts_report_set($limit,$start,$order,$dir,$dari,$sampai,$poreference);
			
	        $data = array();
	        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {
					$nestedData['date']        = $master->date;
					$nestedData['style']       = $master->style;
					$nestedData['poreference'] = $master->poreference;
					$nestedData['article']     = $master->article;
					$nestedData['size']        = $master->size;
					$nestedData['order']       = $master->qty_ordered;
					// $nestedData['balance']     = $master->balance_day;
					$nestedData['output']      = $master->counter_day;
					$nestedData['line']        = $master->line;
					
	                $data[] = $nestedData;
	            }
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
	}

	public function download_report_set()
    {
    	$dari        = $_GET['dari'];
		$sampai      = $_GET['sampai'];
		$poreference = $_GET['pobuyer'];

  		if ($dari!='') {
  			$dari = date('Y-m-d',strtotime($dari));
  		}else{
  			$dari = date('Y-m-d');
		}

		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = date('Y-m-d');
		}
 		  
		if($poreference!=''){
			$poreference = $poreference;
		}
		else{
			$poreference = 0;
		}

        $master = $this->ReportModel->download_report_set($dari,$sampai,$poreference);
        
		
        $data = array();
		$nomor_urut = 0;
		$tgl = date('Y-m-d');
        if(!empty($master))
        {
            foreach ($master as $master)
            {
				$nestedData['date']        = $master->date;
				$nestedData['style']       = $master->style;
				$nestedData['poreference'] = $master->poreference;
				$nestedData['article']     = $master->article;
				$nestedData['size']        = $master->size;
				$nestedData['order']       = $master->qty_ordered;
				// $nestedData['balance']     = $master->balance_day;
				$nestedData['output']      = $master->counter_day;
				$nestedData['line']        = $master->line;
				
                $baris = '<tr>
					<td class="text-center">'.$nestedData['date'].'</td>
					<td class="text-center">'.$nestedData['style'].'</td>
					<td class="text-center">'.$nestedData['poreference'].'</td>
					<td class="text-center">'.$nestedData['article'].'</td>
					<td class="text-center">'.$nestedData['size'].'</td>
					<td class="text-center">'.$nestedData['order'].'</td>
					<td class="text-center">'.$nestedData['output'].'</td>
					<td class="text-center">'.$nestedData['line'].'</td>
					</tr>';
				$data[] = $baris;

            }
        }
          
        $json_data = array(
			"data"            => $data,
		);
		$this->load->view("report/style_set/report_set_download",$json_data);
	}
	
	public function export_tls_inline($factory_id){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$tgl     = date('Y-m-d');
		$dari    = date('Y-m-d',strtotime('-7 days'));
		$sampai  = $tgl;
		$factory = $factory_id;

		// var_dump($dari);
		// var_dump($sampai);
		// die();

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('ICT Team')
					 ->setLastModifiedBy('ICT Team')
					 ->setTitle("Data TLS Inline".$dari." sd ".$sampai."")
					 ->setSubject("TLS Inline")
					 ->setDescription("Laporan TLS Inline ".$dari." sd ".$sampai."")
					 ->setKeywords("Data TLS Inline");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
		  'font' => array('bold' => true), // Set font nya jadi bold
		  'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Set text jadi ditengah secara horizontal (center)
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER      // Set text jadi di tengah secara vertical (middle)
		  ),
		  'borders' => array(
			'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
			'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
			'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
		  )
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
		  'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
		  ),
		  'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		  )
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA TLS Inline ".$dari." sd ".$sampai.""); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:AL1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3

		
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
		$excel->getActiveSheet()->mergeCells('A2:A3'); // Set kolom A3 dengan tulisan "NO"
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('B2', "DATE");
		$excel->getActiveSheet()->mergeCells('B2:B3'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('C2', "LINE");
		$excel->getActiveSheet()->mergeCells('C2:C3');  // Set kolom B3 dengan tulisan "NIS"
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('D2', "PO BUYER");
		$excel->getActiveSheet()->mergeCells('D2:D3');  // Set kolom C3 dengan tulisan "NAMA"
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('E2', "STYLE");
		$excel->getActiveSheet()->mergeCells('E2:E3');  // Set kolom D3 dengan tulisan "JENIS KELAMIN"
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('F2', "R1 RANDOM");
		$excel->getActiveSheet()->mergeCells('F2:F3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('G2', "TOTAL DEFECT");
		$excel->getActiveSheet()->mergeCells('G2:G3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('H2', "RFT");
		$excel->getActiveSheet()->mergeCells('H2:H3');  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom A

		$excel->setActiveSheetIndex(0)->setCellValue('I2', "DEFECT CODE");
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom A

		$excel->getActiveSheet()->mergeCells('I2:AL2');
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "1");
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "2");
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "3");
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "4");
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "5");
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('N3', "6");
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('O3', "7");
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('P3', "8");
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('Q3', "9");
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('R3', "10");
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('S3', "11");
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('T3', "12");
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('U3', "13");
		$excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('V3', "14");
		$excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('W3', "15");
		$excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('X3', "16");
		$excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('Y3', "17");
		$excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('Z3', "18");
		$excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AA3', "19");
		$excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AB3', "20");
		$excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AC3', "21");
		$excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AD3', "2");
		$excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AE3', "23");
		$excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AF3', "24");
		$excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AG3', "25");
		$excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AH3', "26");
		$excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AI3', "27");
		$excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AJ3', "28");
		$excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AK3', "29");
		$excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true); // Set width kolom A
		$excel->setActiveSheetIndex(0)->setCellValue('AL3', "30");  // Set kolom E3 dengan tulisan "ALAMAT"
		$excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true); // Set width kolom A
		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A2:A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B2:B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C2:C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D2:D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E2:E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F2:F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G2:G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H2:H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I2:AL2')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3:AL3')->applyFromArray($style_col);
		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		// $siswa = $this->SiswaModel->view();

		// $tanggal =$this->input->get('tanggal');
		// $detail_report='';

		// $date = ($tanggal!=NULL?$tanggal:date('Y-m-d'));

		$details = $this->ReportModel->downloadReportTls_ranged($dari,$sampai,$factory);
	
		
		$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4

		foreach ($details->result() as $key => $detail) {
			$rft  = (($detail->total_checked-$detail->count_defect)/$detail->total_checked)*100;
		// foreach($siswa as $data){ // Lakukan looping pada variabel siswa
		  $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		  $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $detail->create_date);
		  $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $detail->line_name);
		  $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $detail->poreference);
		  $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $detail->style);
		  $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $detail->total_checked);
		  $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $detail->count_defect);
		  $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($rft,1,',',',').'%');
		  $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $detail->defect1);
		  $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $detail->defect2);
		  $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $detail->defect3);
		  $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $detail->defect4);
		  $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $detail->defect5);
		  $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $detail->defect6);
		  $excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $detail->defect7);
		  $excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, $detail->defect8);
		  $excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, $detail->defect9);
		  $excel->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $detail->defect10);
		  $excel->setActiveSheetIndex(0)->setCellValue('S'.$numrow, $detail->defect11);
		  $excel->setActiveSheetIndex(0)->setCellValue('T'.$numrow, $detail->defect12);
		  $excel->setActiveSheetIndex(0)->setCellValue('U'.$numrow, $detail->defect13);
		  $excel->setActiveSheetIndex(0)->setCellValue('V'.$numrow, $detail->defect14);
		  $excel->setActiveSheetIndex(0)->setCellValue('W'.$numrow, $detail->defect15);
		  $excel->setActiveSheetIndex(0)->setCellValue('X'.$numrow, $detail->defect16);
		  $excel->setActiveSheetIndex(0)->setCellValue('Y'.$numrow, $detail->defect17);
		  $excel->setActiveSheetIndex(0)->setCellValue('Z'.$numrow, $detail->defect18);
		  $excel->setActiveSheetIndex(0)->setCellValue('AA'.$numrow, $detail->defect19);
		  $excel->setActiveSheetIndex(0)->setCellValue('AB'.$numrow, $detail->defect20);
		  $excel->setActiveSheetIndex(0)->setCellValue('AC'.$numrow, $detail->defect21);
		  $excel->setActiveSheetIndex(0)->setCellValue('AD'.$numrow, $detail->defect22);
		  $excel->setActiveSheetIndex(0)->setCellValue('AE'.$numrow, $detail->defect23);
		  $excel->setActiveSheetIndex(0)->setCellValue('AF'.$numrow, $detail->defect24);
		  $excel->setActiveSheetIndex(0)->setCellValue('AG'.$numrow, $detail->defect25);
		  $excel->setActiveSheetIndex(0)->setCellValue('AH'.$numrow, $detail->defect26);
		  $excel->setActiveSheetIndex(0)->setCellValue('AI'.$numrow, $detail->defect27);
		  $excel->setActiveSheetIndex(0)->setCellValue('AJ'.$numrow, $detail->defect28);
		  $excel->setActiveSheetIndex(0)->setCellValue('AK'.$numrow, $detail->defect29);
		  $excel->setActiveSheetIndex(0)->setCellValue('AL'.$numrow, $detail->defect30);
		  
		  // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		  $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('T'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('U'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('V'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('W'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('X'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Y'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('Z'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AA'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AB'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AC'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AD'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AE'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AF'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AG'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AH'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AI'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AJ'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AK'.$numrow)->applyFromArray($style_row);
		  $excel->getActiveSheet()->getStyle('AL'.$numrow)->applyFromArray($style_row);
		  
		  $no++; // Tambah 1 setiap kali looping
		  $numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
		// $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom E
		// $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true); // Set width kolom E
		
		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Laporan TLS Inline");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment; filename="DATA TLS Inline "'.$dari.'" sd "'.$sampai.'".xlsx"'); // Set nama file excel nya
		// header('Cache-Control: max-age=0');
		$filename = 'TLS_Inline_AOI_'.$factory.'.xlsx';
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        // $write->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
		$path = $_SERVER["DOCUMENT_ROOT"]."/line_system/assets/excel/";
		// $write->save($path.$fileName);
		$write->save(str_replace(__FILE__,''.$path.'/'.$filename,__FILE__));
		// $write->save('php://output');
	}

	public function download_tls_inline_week()
	{
		$factory = $this->session->userdata('factory');
		$filename = 'TLS_Inline_AOI_'.$factory.'.xlsx';
		$path = $_SERVER["DOCUMENT_ROOT"]."/assets/excel/".$filename;
		// var_dump($path);
		// $data = file_get_contents($path);

		force_download($path,null);
		// return $data;
	}

	public function report_supply_time()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		// var_dump($this->uri->segment(1));
		// die();

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Report Supply Time');
            $this->template->set('desc_page','Report Supply Time');
            $this->template->load('layout','report/supply_time/report_st');
        }
	}

	public function report_supply_time_search($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$dari    = $post['dari'];
			$sampai  = $post['sampai'];
			$line_id = $post['line_id'];

			$data = array (
				'dari'    => $dari,
				'sampai'  => $sampai,
				'line_id' => $line_id,
			);
			$this->load->view("report/supply_time/report_st_result",$data);
		}
	}

	public function report_supply_time_ajax($post=NULL)
	{
		$columns = array(
						0 =>'no_urut',
						1 =>'jam'
					);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');
		
		$dari    = $this->input->post('dari');
		$sampai  = $this->input->post('sampai');
		$line_id = $this->input->post('line_id');

		if ($dari!='') {
			$dari = date('Y-m-d',strtotime($dari));
		}else{
			$dari = date('Y-m-d');
		}
			
		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = date('Y-m-d');
		}
			
		if($line_id!=''){
			$line_id = $line_id;
		}
		else{
			$line_id = 0;
		}

		$totalData = $this->ReportModel->allposts_count_report_st($dari,$sampai,$line_id);

		$totalFiltered = $totalData;

		$master = $this->ReportModel->allposts_report_st($limit,$start,$order,$dir,$dari,$sampai,$line_id);
		
		$data = array();
		
		$nomor_urut = 0;
		if(!empty($master))
		{	
			
			foreach ($master as $master)
			{
				$nestedData['line_name'] = $master->line_name;
				$nestedData['qty']       = $master->qty;
				$nestedData['jam']       = $master->jam;
				
				$data[] = $nestedData;
			}
		}
		
		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		
		echo json_encode($json_data);
	}

	public function download_report_supply_time()
    {
    	$dari    = $_GET['dari'];
    	$sampai  = $_GET['sampai'];
    	$line_id = $_GET['line_id'];

  		if ($dari!='') {
  			$dari = date('Y-m-d',strtotime($dari));
  		}else{
  			$dari = date('Y-m-d');
		}

		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = date('Y-m-d');
		}
 		  
		if($line_id!=''){
			$line_id = $line_id;
		}
		else{
			$line_id = 0;
		}

        $master = $this->ReportModel->download_report_st($dari,$sampai,$line_id);
        
		
        $data = array();
		$nomor_urut = 0;
		$tgl = date('Y-m-d');
        if(!empty($master))
        {
            foreach ($master as $master)
            {
				$nestedData['line_name'] = $master->line_name;
				$nestedData['qty']       = $master->qty;
				$nestedData['jam']       = $master->jam;
				
                $baris = '<tr>
					<td class="text-center">'.$nestedData['line_name'].'</td>
					<td class="text-center">'.$nestedData['qty'].'</td>
					<td class="text-center">'.$nestedData['jam'].'</td>
					</tr>';
				$data[] = $baris;

            }
        }
          
        $json_data = array(
			"data"            => $data,
		);
		$this->load->view("report/supply_time/report_st_download",$json_data);
	}

	
    public function report_change_over()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	$this->template->set('title','Report CO');
            $this->template->set('desc_page','Report CO');
            $this->template->load('layout','report/change_over/index_report_co');
        }
	}
	public function report_copt($post=NULL)
	{
		if ($this->input->get('tanggal1') == '')
		{
			echo "Tanggal tidak boleh kosong";
			return FALSE;
		}
		$factory = $this->session->userdata('factory');
		$tanggal = $this->input->get('tanggal1');
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('ICT Team')
			->setLastModifiedBy('ICT Team')
			->setTitle("Report COPT")
			->setSubject("Report COPT")
			->setDescription("Report COPT")
			->setKeywords("Report COPT");
		
		$excel->setActiveSheetIndex(0);
		$dataCopt = $this->ReportModel->downloadReportCopt($tanggal,$factory);
		/* var_dump($dataCopt);
		die(); */
		$excel->getActiveSheet()->setCellValue('A1', 'Date');
		$excel->getActiveSheet()->setCellValue('B1', 'Line');
		$excel->getActiveSheet()->setCellValue('C1', 'Style');
		$excel->getActiveSheet()->setCellValue('D1', 'Tanggal Awal');
		$excel->getActiveSheet()->setCellValue('E1', 'Tanggal Akhir');

		$num_row=2;
		foreach ($dataCopt as $key => $co) {
			$excel->setActiveSheetIndex(0)->setCellValue('A'.$num_row, $co->date);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$num_row, $co->line_name);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$num_row, $co->style);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$num_row, $co->min_date);
			$excel->setActiveSheetIndex(0)->setCellValue('E'.$num_row, $co->max_date);
			$num_row++;
		}
		$excel->getActiveSheet(0)->setTitle("Report COPT");
		$excel->setActiveSheetIndex(0);
		
		// Proses file excel
		// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="REPORT COPT TGL "'.$tanggal.'".xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}
	public function report_co_ajax($post=NULL)
	{
		
			$dari    = $this->input->get('tanggal1');

			
			$factory = $this->session->userdata('factory');

			$query = $this->db->query("SELECT
				ih.line_id,
				qet.style,
				ih.article,
				ih.inline_header_id,
				min(qet.create_date) as min,
				max(qet.create_date) as max
			FROM
				qc_endline_trans qet
			LEFT JOIN
				inline_header ih on ih.inline_header_id = qet.inline_header_id
			WHERE
				DATE(qet.create_date) = '$dari'
				and ih.factory_id = '$factory'
			GROUP BY
				line_id, article, ih.inline_header_id,
				qet.style
			ORDER BY line_id, min
			");

			$hasil = $query->result_array();
			$jml   =  count($hasil);

			$header_id = array();
			$ih_id = '';
			for ($i=0; $i<count($hasil) ; $i++) { 
				if($i+1>=$jml){
					$ih_id .= " '".$hasil[$i]['inline_header_id']."'";
					array_push($header_id,$ih_id);
				}
				else{
					if($hasil[$i]['line_id'] != $hasil[$i+1]['line_id']){
						$ih_id .= " '".$hasil[$i]['inline_header_id']."'";
						array_push($header_id,$ih_id);
						$ih_id = '';
					}
					else{
						$ih_id .= " '".$hasil[$i]['inline_header_id']."' , ";
					}
				}
			}

			// var_dump($header_id);
			$master  = array();
			foreach ($header_id as $hi) {

				$qc_trans = $this->db->query("SELECT qet.*, ih.article, ih.line_id from qc_endline_trans qet
				LEFT JOIN inline_header ih on ih.inline_header_id = qet.inline_header_id
				where qet.inline_header_id in (
					$hi
				)
				and date(qet.create_date) = '$dari'
				ORDER BY qet.create_date");

				$jawaban = $qc_trans->result_array();	
				$panjang = count($jawaban);

				$line    = array(); 
				$style   = array(); $article = array();
				$awal    = array(); $akhir   = array();
				$c_style = ''; $c_article    = '';
				$sudah   = 0;
				for ($j=0; $j< $panjang ; $j++) {
					if($j+1>=$panjang){
						array_push($akhir,$jawaban[$j]['create_date']);

						array_push($line,$jawaban[$j]['line_id'], $style, $article, $awal, $akhir);
						array_push($master,$line);
						$line  = array();
						$sudah = 0;
					}
					else{
						if( $jawaban[$j]['style'] == $jawaban[$j+1]['style'] && $jawaban[$j]['article'] == $jawaban[$j+1]['article'])
						{
							if($sudah > 0){

							}else{
								array_push($style,$jawaban[$j]['style']);
								array_push($article,$jawaban[$j]['article']);
								array_push($awal,$jawaban[$j]['create_date']);
								$sudah++;
							}
						}else{
							if($j>0){
								if( $jawaban[$j]['style'] ==  $jawaban[$j-1]['style'] && $jawaban[$j]['article'] ==  $jawaban[$j-1]['article']){							
									array_push($akhir,$jawaban[$j]['create_date']);							
									$sudah = 0;
								}
								else{
									array_push($style,$jawaban[$j]['style']);
									array_push($article,$jawaban[$j]['article']);
									array_push($awal,$jawaban[$j]['create_date']);
									array_push($akhir,$jawaban[$j]['create_date']);							
									$sudah = 0;
								}
							}
						}

						if($jawaban[$j]['line_id'] == $jawaban[$j+1]['line_id']){
					
						}
						else{
							array_push($line,$jawaban[$j]['line_id'], $style, $article, $awal, $akhir);
							array_push($master,$line);
							$line  = array();
							$sudah = 0;
						}
					}

				
				}
			}

			$totalData = count($master);
			$totalFiltered = count($master);
	        // $nomor_urut = 0;
	        if(!empty($master))
	        {	
				for ($i=0; $i < count($master) ; $i++) { 
					for ($j=0; $j < 2; $j++) { 
						// var_dump(count($master[$i][1]));
						if($j>0){
							for ($k=0; $k < count($master[$i][1]); $k++) {
								$idd       = $master[$i][0];
								$line_name = $this->db->query("SELECT * FROM master_line where master_line_id = '$idd'")->row_array();
								$nama      = $line_name['line_name'];
											
								
								$nestedData['date']      = $dari;
								$nestedData['line_id']   = $master[$i][0];
								$nestedData['line_name'] = $nama;
								$nestedData['style']     = $master[$i][1][$k];
								$nestedData['article']   = $master[$i][2][$k];
								$nestedData['awal']     = $master[$i][3][$k];
								$nestedData['akhir']     = $master[$i][4][$k];

								$data[] = $nestedData;
							}
						}

					}


				}
			}
			// Load plugin PHPExcel nya
			include APPPATH.'third_party/PHPExcel/PHPExcel.php';
			
			$excel = new PHPExcel();
			// Settingan awal fil excel
			$excel->getProperties()->setCreator('ICT Team')
						->setLastModifiedBy('ICT Team')
						->setTitle("Data Change Over".$dari)
						->setSubject("Change Over")
						->setDescription("Laporan Change Over Tanggal ".$dari)
						->setKeywords("Data Change Over");
			// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
			$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Set text jadi ditengah secara horizontal (center)
				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER      // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
			);
			// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
			$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER  // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top'    => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border top dengan garis tipis
				'right'  => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),   // Set border bottom dengan garis tipis
				'left'   => array('style'  => PHPExcel_Style_Border::BORDER_THIN)    // Set border left dengan garis tipis
			)
			);
			$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA CHANGE OVER TGL ".$dari); // Set kolom A1 dengan tulisan "DATA SISWA"
			$excel->getActiveSheet()->mergeCells('A1:G1'); // Set Merge Cell pada kolom A1 sampai E1
			$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);  // Set bold kolom A1
			$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
			$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
			// Buat header tabel nya pada baris ke 3

			
			$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
			$excel->getActiveSheet()->mergeCells('A2:A3'); // Set kolom A3 dengan tulisan "NO"
			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A

			$excel->setActiveSheetIndex(0)->setCellValue('B2', "DATE");
			$excel->getActiveSheet()->mergeCells('B2:B3'); // Set Merge Cell pada kolom A1 sampai E1
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom A

			$excel->setActiveSheetIndex(0)->setCellValue('C2', "LINE");
			$excel->getActiveSheet()->mergeCells('C2:C3');  // Set kolom B3 dengan tulisan "NIS"
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom A

			$excel->setActiveSheetIndex(0)->setCellValue('D2', "STYLE");
			$excel->getActiveSheet()->mergeCells('D2:D3');  // Set kolom C3 dengan tulisan "NAMA"
			$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom A

			$excel->setActiveSheetIndex(0)->setCellValue('E2', "ARTICLE");
			$excel->getActiveSheet()->mergeCells('E2:E3');  // Set kolom D3 dengan tulisan "JENIS KELAMIN"
			$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom A

			$excel->setActiveSheetIndex(0)->setCellValue('F2', "INPUT AWAL");
			$excel->getActiveSheet()->mergeCells('F2:F3');  // Set kolom E3 dengan tulisan "ALAMAT"
			$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A

			$excel->setActiveSheetIndex(0)->setCellValue('G2', "INPUT AKHIR");
			$excel->getActiveSheet()->mergeCells('G2:G3');  // Set kolom E3 dengan tulisan "ALAMAT"
			$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom A

			// Apply style header yang telah kita buat tadi ke masing-masing kolom header
			$excel->getActiveSheet()->getStyle('A2:A3')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('B2:B3')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C2:C3')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D2:D3')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E2:E3')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('F2:F3')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('G2:G3')->applyFromArray($style_col);
			// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya

			$no = 0; // Untuk penomoran tabel, di awal set dengan 1
			$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
			$smh = '';
			foreach ($data as $key => $detail) {
				// $rft  = (($detail->total_checked-$detail->count_defect)/$detail->total_checked)*100;
			// foreach($siswa as $data){ // Lakukan looping pada variabel siswa
			$no++; // Tambah 1 setiap kali looping
			$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $detail['date']);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $detail['line_name']);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $detail['style']);
			$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $detail['article']);
			$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $detail['awal']);
			$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $detail['akhir']);
			
			
			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
			
			$numrow++; // Tambah 1 setiap kali looping
			}
			// Set width kolom
			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
			$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
			$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
			$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom E
			$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom E
			
			// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
			$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
			// $excel->getActiveSheet()->getDefaultColumnDimension()->setColumnWidth(-1);
			// Set orientasi kertas jadi LANDSCAPE
			$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			// Set judul file excel nya
			$excel->getActiveSheet(0)->setTitle("LAPORAN CHANGE OVER");
			$excel->setActiveSheetIndex(0);
			
			// Proses file excel
			// $path = $_SERVER["DOCUMENT_ROOT"]."/excel/";
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="REPORT CHANGE OVER TGL "'.$dari.'".xlsx"'); // Set nama file excel nya
			header('Cache-Control: max-age=0');
			// $filename = 'Skill_Matrix_AOI_'.$factory.'.xlsx';
			$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
			// $write->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
			// $path = $_SERVER["DOCUMENT_ROOT"]."/line_system/assets/excel/";
			// $write->save($path.$fileName);
			// $write->save(str_replace(__FILE__,''.$path.'/'.$filename,__FILE__));
			$write->save('php://output');
	}

	public function report_pengajuan_ad()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		// var_dump($this->uri->segment(1));
		// die();

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{

            $this->template->set('title','Report Pengajuan AD');
            $this->template->set('desc_page','Report Pengajuan AD');
            $this->template->load('layout','report/pengajuan_ad/report_pengajuan_ad');
        }
	}
	
	public function report_pengajuan_ad_ajax($post=NULL)
	{
		$columns = array(
			0 =>'created_at', 
			1 =>'tanggal_permintaan',
			3 =>'style',
			4 =>'poreference',
			6 =>'status',
		);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');
		
		$dari    = $this->input->post('dari');
		$sampai  = $this->input->post('sampai');
		$line_id = $this->input->post('line_id');
		$factory = $this->session->userdata('factory');

		if ($dari!='') {
			$dari = date('Y-m-d',strtotime($dari));
		}else{
			$dari = '';
		}
			
		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = '';
		}
			
		if($line_id!=''){
			$line_id = $line_id;
		}
		else{
			$line_id = 0;
		}

		$totalData = $this->ReportModel->allposts_count_ad($factory,$line_id,$dari,$sampai);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->ReportModel->allposts_ad($limit,$start,$order,$dir,$factory,$line_id,$dari,$sampai);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master = $this->ReportModel->posts_search_ad($limit,$start,$search,$order,$dir,$factory,$line_id,$dari,$sampai);

            $totalFiltered = $this->ReportModel->posts_search_count_ad($search,$factory,$line_id,$dari,$sampai);
        }
		
		$data = array();
		if(!empty($master))
		{	
			
			foreach ($master as $master)
			{
				
				$temp = '"'.$master->pengajuan_id.'"';
                $nestedData['tanggal_permintaan'] = ucwords($master->tanggal_permintaan);
                $nestedData['jam_permintaan']     = ucwords(date('H:m',strtotime($master->jam_permintaan)));
                $nestedData['style']              = ucwords($master->style);
                $nestedData['poreference']        = ucwords($master->poreference);
				$nestedData['name']          = ucwords($master->name);
                $nestedData['line_name']          = ucwords($master->line_name);
				$nestedData['status']             = ucwords($master->status);
				if($master->status == 'belum siap'){
					$nestedData['action'] = "<center><a onclick='return siap_action($temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-repeat'></i> SIAP</a> </center>";
				}else{
					$nestedData['action'] = '';
				}

                

				// $nestedData['line_name'] = $master->line_name;
				// $nestedData['qty']       = $master->qty;
				// $nestedData['jam']       = $master->jam;
				
				$data[] = $nestedData;
			}
		}
		
		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		
		echo json_encode($json_data);
	}

	public function pengajuan_ad_siap($post = NULL)
    {
		$post = $this->input->post();
		$id   = $post['id'];

		$this->db->trans_start();

		$this->db->query("UPDATE pengajuan_ad SET status = 'siap' where pengajuan_id='$id'");

		if ($this->db->trans_status() == FALSE)
		{
			$this->db->trans_rollback();
			$status = 500;
			$pesan  = "Insert Data gagal";
		}else{
			$this->db->trans_complete();
			$status = 200;
			$pesan  = "SUKSES";
		}
		
		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
	}
	
	
	public function download_report_pengajuan_ad()
    {
    	$dari    = $_GET['dari'];
    	$sampai  = $_GET['sampai'];
    	$line_id = $_GET['line_id'];

  		if ($dari!='') {
  			$dari = date('Y-m-d',strtotime($dari));
  		}else{
  			$dari = date('Y-m-d');
		}

		if ($sampai!='') {
			$sampai = date('Y-m-d',strtotime($sampai));
		}else{
			$sampai = date('Y-m-d');
		}
 		  
		if($line_id!=''){
			$line_id = $line_id;
		}
		else{
			$line_id = 0;
		}

        $master = $this->ReportModel->download_report_pengajuan_ad($dari,$sampai,$line_id);
        
		
        $data = array();
		$nomor_urut = 0;
		$tgl = date('Y-m-d');
        if(!empty($master))
        {
            foreach ($master as $master)
            {
				// $nestedData['line_name'] = $master->line_name;
				// $nestedData['qty']       = $master->qty;
				// $nestedData['jam']       = $master->jam;

                $nestedData['line_name']          = ucwords($master->line_name);
				$nestedData['name']          = ucwords($master->name);
				$nestedData['tanggal_permintaan'] = ucwords($master->tanggal_permintaan);
                $nestedData['jam_permintaan']     = ucwords(date('H:m',strtotime($master->jam_permintaan)));
                $nestedData['style']              = ucwords($master->style);
                $nestedData['poreference']        = ucwords($master->poreference);
				$nestedData['status']             = ucwords($master->status);

				
                $baris = '<tr>
					<td class="text-center">'.$nestedData['line_name'].'</td>
					<td class="text-center">'.$nestedData['name'].'</td>
					<td class="text-center">'.$nestedData['tanggal_permintaan'].'</td>
					<td class="text-center">'.$nestedData['jam_permintaan'].'</td>
					<td class="text-center">'.$nestedData['style'].'</td>
					<td class="text-center">'.$nestedData['poreference'].'</td>
					<td class="text-center">'.$nestedData['status'].'</td>
					</tr>';
				$data[] = $baris;

            }
        }
          
        $json_data = array(
			"data"            => $data,
		);
		$this->load->view("report/supply_time/report_st_download",$json_data);
	}

	public function outputPerjam(){
		$this->template->set('title','download Output');
        $this->template->set('desc_page','download Output');
        $this->template->load('layout','report/production_output/output_perjam');
	}

	public function downloadOutputPerjam($post=null){
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
		$filter = $this->input->post('filter');
		$dari = $this->input->post('dari');
		$sampai = $this->input->post('sampai');
		$factory              = $this->session->userdata('factory');

		if ($filter=="all") {
			$query = $this->db->query(
				"SELECT line_id,line_name,activity_name,jam_awal,jam_akhir,date,factory_id,sum(output) as output FROM line_summary_po WHERE (date BETWEEN '$dari' and '$sampai') and factory_id=$factory and activity_name in ('outputqc','sewing')  GROUP BY line_id,line_name,activity_name,jam_awal,jam_akhir,date,factory_id ORDER BY jam_awal,activity_name,line_id"
			);
		}else{
			$query = $this->db->query(
				"SELECT line_id,line_name,activity_name,jam_awal,jam_akhir,date,factory_id,sum(output) as output FROM line_summary_po WHERE (date BETWEEN '$dari' and '$sampai') and factory_id=$factory and activity_name='$filter'  GROUP BY line_id,line_name,activity_name,jam_awal,jam_akhir,date,factory_id ORDER BY jam_awal,activity_name,line_id"
			);
		}

		$excel = new PHPExcel();
		$excel->getProperties()->setCreator('ICT Team')
			->setLastModifiedBy('ICT Team')
			->setTitle("Data  OUTPUT PERJAM ".$filter ." ". $dari . " sd " . $sampai . "")
			->setSubject(" Skill Based")
			->setDescription("Laporan  Skill Based " . $dari . " sd " . $sampai . "")
			->setKeywords("Data  Skill Based");

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA OUTPUT PERJAM ".$filter." " . $dari . " sd " . $sampai . "");
		$excel->getActiveSheet()->mergeCells('A1:G1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);  // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$excel->setActiveSheetIndex(0)->setCellValue('A2', "NO");
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$excel->setActiveSheetIndex(0)->setCellValue('B2', "Tanggal");
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);  
		$excel->setActiveSheetIndex(0)->setCellValue('C2', "Line ID");
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
		$excel->setActiveSheetIndex(0)->setCellValue('D2', "Nama Line");
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
		$excel->setActiveSheetIndex(0)->setCellValue('E2', "Proses");
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$excel->setActiveSheetIndex(0)->setCellValue('F2', "Jam Tarikan");
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		$excel->setActiveSheetIndex(0)->setCellValue('G2', "Output");
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);   

		$no = 1;
		$rw =3;
		foreach ($query->result() as $data) {
			
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $rw, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $rw, $data->date);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $rw, $data->line_id);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $rw, $data->line_name);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $rw, $data->activity_name);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $rw, substr($data->jam_awal,11,8)."-".substr($data->jam_akhir,11,8));
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $rw, $data->output);

			$no++;
			$rw++;
		}

		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
		$excel->getActiveSheet(0)->setTitle("DATA OUTPUT PERJAM");
		$excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="DATA OUTPUT PERJAM".xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}
	
}

/* End of file Report.php */
/* Location: ./application/controllers/Report.php */