<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_process extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Master_proses_model','Cekpermision_model'));
        chek_session();
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Master Proses');
            $this->template->set('desc_page','Master Proses');
            $this->template->load('layout','master/proses/list_proses_view');
        }

	}
    public function listmaster()
    {
        $columns = array( 
                            0 =>'master_proses_id', 
                            1 =>'proses_name'
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');

  
        $totalData = $this->Master_proses_model->allposts_count_proses();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Master_proses_model->allposts_proses($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Master_proses_model->posts_search_proses($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Master_proses_model->posts_search_count_proses($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $key=>$master)
            {
                
                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['proses_name'] = '<input name="nama_input['.$key.']" class="form-control proses_'.$master->master_proses_id.'" value="'.$master->proses_name.'" onkeyup="this.value = this.value.toUpperCase()"  disabled />';
                $nestedData['action'] = "<center><a onclick='return edit_proses($master->master_proses_id,1)' href='javascript:void(0)' class='edit_".$master->master_proses_id." btn btn-primary'><i class='fa fa-pencil-square-o'></i> Edit</a><a onclick='return edit_proses($master->master_proses_id,2)' style='display: none' href='javascript:void(0)' class='edit_".$master->master_proses_id."  btn btn-success'><i class='fa fa-save'></i></a><a onclick='return edit_proses($master->master_proses_id,3)' style='display: none' href='javascript:void(0)' class='edit_".$master->master_proses_id."  btn btn-danger'><i class='fa fa-times'></i></a></center>";                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }
	public function add_master()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Master Proses');
            $this->template->set('desc_page','Master Proses');
            $this->template->load('layout','master/proses/add_proses_view');
        }
		
	}
    public function savesubmitproses()
    {
        
        $i     =0;
        $proses_name =[];
        $items = json_decode($this->input->post('items'));
        
        foreach ($items as $key => $item) {
            $i++;
            $proses_name[$i] = strtoupper($item->proses_name);
            
            $this->db->where('proses_name', $proses_name[$i]);
            $cek = $this->db->get('master_proses')->num_rows();
            if ($cek==0) {
                $data = array('proses_name'=>$proses_name[$i]);
                $this->db->insert('master_proses', $data);
            }
        }
        echo "sukses";
    }
	

}

/* End of file master_process.php */
/* Location: ./application/controllers/master_process.php */