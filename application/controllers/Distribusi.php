<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distribusi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$CI = &get_instance();
		date_default_timezone_set('Asia/Jakarta');
		chek_session_folding();
		$this->load->model(array('DistribusiModel'));
		// $this->dist 	= $CI->load->database('dist',TRUE);		
		// $this->cdms 	= $CI->load->database('cdms',TRUE);
		$this->cdms_new = $CI->load->database('cdms_new',TRUE);
	}

	public function index()
	{
		$post =$this->input->post();
		if ($post!=NULL) {
			$nik = $post['nik'];			
			$this->db->where('factory', $this->session->userdata('factory_name'));
			$this->db->where('nik', $nik);
			$cekNik = $this->db->get('master_sewer');

			if ($cekNik->num_rows()>0) {
				$detail 	= $cekNik->row_array();
				$nama   	= $detail['name'];
				$ses 		= $this->session->userdata();				
				$login_data = array(
					'nik'          => $ses['nik'],
					'name'         => $ses['name'],
					'line_id'      => $ses['line_id'],
					'mesin_id'     => $ses['mesin_id'],
					'logged_in'    => $ses['logged_in'],
					'factory'      => $ses['factory'], 
					'factory_name' => $ses['factory_name'], 
					'nik_adm'      => $nik,
					'name_adm'      => $cekNik->row_array()['name']
				);

				$this->session->set_userdata($login_data);
				$line_id = $this->session->userdata('line_id');
				$line 	 = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();
				$nik 	 = $this->input->post('nik');
				$data 	 = array(
					'line' => $line, 
					'nama' => $nama
				);

				$this->template->set('title',$line['line_name'].' - Receive Loading');
		        $this->template->set('desc_page','Receive Loading');
				$this->template->load('layout2','folding/index_distribution_receive', $data);
			}else{
				redirect('auth/folding','refresh');	
			}
		}else if ($this->session->userdata('nik_adm')!=0) {
			$line_id = $this->session->userdata('line_id');
			$line 	 = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();
			$data 	 = array(
				'line' => $line, 
				'nama' => $this->session->userdata('name_adm')
			);
			$this->template->set('title',$line['line_name'].' - Receive Loading');
	        $this->template->set('desc_page','Receive Loading');
			$this->template->load('layout2','folding/index_distribution_receive',$data);
		}
	}

	public function pindah_line($post=NULL)
	{
		$post =$this->input->post();

		if($post!=NULL){
			$nik 	= $post['nik'];
			$this->db->where('factory', $this->session->userdata('factory_name'));
			$this->db->where('nik', $nik);
			$cekNik = $this->db->get('master_sewer');

			if ($cekNik->num_rows()>0) {
				$detail = $cekNik->row_array();
				$nama 	= $detail['name'];
				$ses 	= $this->session->userdata();
				
				$login_data = array(
					'nik'          => $ses['nik'],
					'name'         => $ses['name'],
					'line_id'      => $ses['line_id'],
					'mesin_id'     => $ses['mesin_id'],
					'logged_in'    => $ses['logged_in'],
					'factory'      => $ses['factory'], 
					'factory_name' => $ses['factory_name'], 
					'nik_adm'      => $nik,
					'name_adm'     => $cekNik->row_array()['name']
				);
				
				$this->session->set_userdata($login_data);
				$line_id = $this->session->userdata('line_id');
				$line 	 = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();
				$nik	 = $this->input->post('nik');				
				$temp	 = $this->loadDataPindah();		
				$data 	 = array(
					'line' => $line, 
					'nama' => $nama, 
					'temp' => json_encode($temp), 
					'nik'  => $nik
				);

				$this->template->set('title',$line['line_name'].' - Receive Loading');
		        $this->template->set('desc_page','Receive Loading');
				$this->template->load('layout2','folding/index_distribution_pindah_line',$data);

			} else{
				redirect('auth/folding','refresh');	
			}
		}else if ($this->session->userdata('nik_adm')!=0) {
			$line_id = $this->session->userdata('line_id');
			$line 	 = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();
			$temp	 = $this->loadDataPindah();
			$data 	 = array(
				'line' =>$line, 
				'temp' =>json_encode($temp), 
				'nama' =>$this->session->userdata('name')
			);

			$this->template->set('title',$line['line_name'].' - Receive Loading');
	        $this->template->set('desc_page','Receive Loading');
			$this->template->load('layout2','folding/index_distribution_pindah_line',$data);
		}
	}

	private function loadDataPindah()
	{
		$line_id = $this->session->userdata('line_id');
		$temps 	 = $this->DistribusiModel->getTempPindah($line_id,'')->result();
		$temp	 = array();
		$status  = 200;

		foreach ($temps as $key => $t) {
			$temp[] = array(
				'status'         => $status, 
				'style'          => $t->style, 
				'barcode_id'     => $t->barcode_id, 
				'distribusi_id'  => $t->distribusi_id, 
				'poreference'    => $t->poreference, 
				'size'           => $t->size, 
				'article'        => $t->article, 
				'qty'            => $t->qty, 
				'cut_num'        => $t->cut_num, 
				'component_name' => $t->component_name, 
				'startend'       => $t->start_num.' - '.$t->start_end,
				'sources'		 => $t->source

			);
		}

		return $temp;
	}

	public function scanbandle($barcode_id=NULL)
	{
		$barcode_id = $this->input->post('barcode_id',TRUE);

		if($barcode_id!=NULL){
			$ceksources = $this->cdms_new->query('SELECT * FROM f_barcode_sources(?)', $barcode_id)->row();												
			// if(isset($ceksources->sources) && $ceksources->sources == 'sds'){				
			// 	$this->db->where('delete_at is null');
			// 	$this->db->where('barcode_id', $barcode_id);
			// 	$cekbarcode = $this->db->get('distribusi_detail');								
			// 	if ($cekbarcode->num_rows()==0) {
			// 		// $line = $this->supplied_to();
			// 		// $getBundleLineValid = $this->DistribusiModel->getFBundleLineValid($barcode_id,$line)->row_array();
			// 		// $getBundleLineValid = $this->DistribusiModel->getBundleLineValid($barcode_id)->row_array();
			// 		$getBundleLineValid = $this->DistribusiModel->getBundleLineValid($barcode_id, $ceksources->sources)->row_array();									
			// 		$style_dist 		= $getBundleLineValid['style'];
			// 		$set_type 			= substr($getBundleLineValid['set_type'], 0,3);						
			// 		$sds_supp_factory 	= $getBundleLineValid['factory_name'];
			// 		$supp_factory  		= preg_replace('/\s+/', '', $sds_supp_factory);
			// 		$line_factory  		= $this->session->userdata('factory_name');
			// 		if($supp_factory == $line_factory){						
			// 			if ($set_type!='0') {
			// 				$this->db->like('style', $set_type, 'BOTH');
			// 			}
			// 			$this->db->like('style', $style_dist, 'BOTH');
			// 			$this->db->where('delete_date', null);
			// 			$this->db->where('poreference', $getBundleLineValid['poreference']);
			// 			// $this->db->where('size', $getBundleLineValid['size']);
			// 			// $this->db->where('poreference', $po_ref);
			// 			$cekstyle = $this->db->get('master_order');
	
			// 			if ($getBundleLineValid!=NULL) {
			// 				// if ($getBundleLineValid['right_line']=='t') {
			// 					if ($cekstyle->num_rows()!=0) {
			// 						$cekdistribusi = $this->DistribusiModel->cekDistribusi($getBundleLineValid['poreference'],$getBundleLineValid['size'],$getBundleLineValid['cut_num'],$getBundleLineValid['start_num'],$getBundleLineValid['end_num'],$cekstyle->row_array()['style'],$getBundleLineValid['article'],$this->session->userdata('line_id'), $ceksources->sources);
			// 						// $cekdistribusi = $this->DistribusiModel->cekDistribusi($po_ref,$getBundleLineValid['size'],$getBundleLineValid['cut_num'],$getBundleLineValid['start_num'],$getBundleLineValid['end_num'],$cekstyle->row_array()['style'],$getBundleLineValid['article']);									
			// 						// $allowed = $cekdistribusi->row_array()['line_id']==$this->session->userdata('line_id');

			// 						if ($cekdistribusi->num_rows()==0 || $cekdistribusi->row_array()['line_id']==$this->session->userdata('line_id')) {
			// 							// ||$getBundleLineValid['status_trial']==0
			// 							if ($getBundleLineValid['status_trial']==1) {																																
			// 								$data = array(												
			// 									'poreference'    => $getBundleLineValid['poreference'], 												
			// 									'style'          => $cekstyle->row_array()['style'], 
			// 									'size'           => $getBundleLineValid['size'], 
			// 									'barcode_id'     => $getBundleLineValid['barcode_id'], 
			// 									'qty'            => $getBundleLineValid['qty'], 
			// 									'article'        => $getBundleLineValid['article'], 
			// 									'component_name' => $getBundleLineValid['component_name'], 
			// 									'startend'       => $getBundleLineValid['start_num'].'-'.$getBundleLineValid['end_num'], 
			// 									'cut_num'        => $getBundleLineValid['cut_num']
			// 								);

			// 								$data_response = [
			// 									'status'  => 200,
			// 									'data' 	  => $data,
			// 									'sources' => 'sds' 
			// 								];
											
			// 								// var_dump($data_response); die();
			// 								echo json_encode($data_response);
											
			// 							}else if ($getBundleLineValid['status_trial']==2) {
			// 								$status = 500;
			// 								$pesan  = "Barcode Sudah Pernah di Supply";
			// 								$data 	= array(
			// 									'status' => $status, 
			// 									'pesan'  => $pesan, 
			// 								);
			// 								echo json_encode($data);
	
			// 							}else if ($getBundleLineValid['status_trial']==-1) {
			// 								$status = 500;
			// 								$pesan 	= "Barcode Belum di Checkout, Kembalikan Bundle ke Distribusi Untuk di checkout";
			// 								$data 	= array(
			// 									'status' => $status, 
			// 									'pesan'  => $pesan, 
			// 								);
			// 								echo json_encode($data);
			// 							}else if ($getBundleLineValid['status_trial']==0) {
			// 								$status = 500;
			// 								$pesan 	= "Barcode belum selesai di secondary process, Kembalikan ke Distribusi";
			// 								$data 	= array(
			// 									'status' => $status, 
			// 									'pesan'  => $pesan, 
			// 								);
			// 								echo json_encode($data);
			// 							}
			// 						}else{
			// 							//cek disini
			// 							$status    = 500;
			// 							$line_name = $this->db->get_where('master_line', array('master_line_id'=>$cekdistribusi->row_array()['line_id']))->row_array()['line_name'];
			// 							$pesan     = "Bundle yang sama sudah di scan di ".$line_name.", Pindah Loading Bundle tersebut di ".$line_name." apabila ingin di loading di line ini";
			// 							$data = array(
			// 								'status' => $status, 
			// 								'pesan'  => $pesan, 
			// 							);
			// 							echo json_encode($data);
			// 						}
			// 					}else{
			// 						$status = 500;
			// 						$pesan 	= "PO Tidak di temukan ! ! !";
			// 						$data 	= array(
			// 							'status' => $status, 
			// 							'pesan'  => $pesan, 
			// 						);
			// 						echo json_encode($data);
			// 					}
	
			// 				/*}else if ($getBundleLineValid['right_line']=='f') {
			// 					$status = 500;
			// 					$factory = 'AOI#'.$this->session->userdata('factory');
			// 					$getLinePlaning = $this->DistribusiModel->getLinePlaning($factory,$getBundleLineValid['poreference'])->row_array()['line'];
			// 					$pesan = "Planing po buyer ".$getBundleLineValid['poreference']." di ".$getLinePlaning." , Info PPC  bila po buyer tersebut ingin jalan di line ini";
			// 					$data = array(
			// 						'status' => $status, 
			// 						'pesan'  => $pesan, 
			// 					);
			// 					echo json_encode($data);
			// 				}else{
			// 					$status = 500;
			// 					$pesan = "po buyer ".$getBundleLineValid['poreference']." belum memiliki planning info PPC untuk di buatkan Planing";
			// 					$data = array(
			// 						'status' => $status, 
			// 						'pesan'  => $pesan, 
			// 					);
			// 					echo json_encode($data);
			// 				}*/
			// 			}else{
			// 				$status = 500;
			// 				$pesan  = "Barcode Bundle Tidak di temukan";
			// 				$data   = array(
			// 					'status' => $status, 
			// 					'pesan'  => $pesan, 
			// 				);
			// 				echo json_encode($data);
			// 			}
			// 		}
			// 		else{
			// 			$status = 500;
			// 			$pesan  = "Barcode Bundle Bukan Untuk Pabrik Ini";
			// 			$data   = array(
			// 				'status' => $status, 
			// 				'pesan'  => $pesan, 
			// 			);
			// 			echo json_encode($data);
			// 		}
					
			// 	}else{
			// 		$detail_distribusi  = $cekbarcode->row();
			// 		$queryy 			= $this->db->query("SELECT ml.line_name from distribusi d LEFT JOIN master_line ml on ml.master_line_id = d.line_id where d.distribusi_id = '$detail_distribusi->distribusi_id'")->row();						
			// 		$status 			= 500;
			// 		$pesan  			= "Barcode ".$barcode_id." Sudah di receive oleh line ".$queryy->line_name." !";
			// 		$data   			= array(
			// 								'status' => $status, 
			// 								'pesan'  => $pesan, 
			// 							);

			// 		echo json_encode($data);
			// 	}
			// } else 
			if(isset($ceksources->sources) && $ceksources->sources == 'cdms_new'){
				$this->db->where('delete_at is null');
				$this->db->where('barcode_id', $barcode_id);
				$cekbarcode = $this->db->get('distribusi_detail');

				$this->cdms_new->where('barcode_id', $barcode_id);
				$barcode_cdms = $this->cdms_new->get('v_supply_line');
				
				if($cekbarcode->num_rows() != $barcode_cdms->num_rows()){
					$array = array();					
					$line_factory  		= $this->session->userdata('factory_name');					
					$getBundleLineValid = $this->DistribusiModel->getBundleLineValid($barcode_id, $ceksources->sources)->result_array();							
					$this->cdms_new->where('id', $getBundleLineValid[0]['factory_id']);
					$cdms_supp_factory 	= $this->cdms_new->get('master_factory')->row_array();

					if($getBundleLineValid != null){
						if($cdms_supp_factory['factory_alias'] == $line_factory){
							$result_array = array();

							foreach($getBundleLineValid as $gblv){								
								if($gblv['current_locator_id'] == 3 && $gblv['current_status'] == 'out'){
									$style_dist 		= $gblv['style'];
									$set_type 			= substr($gblv['set_type'], 0,3);
									if($gblv['set_type'] != 'Non'){
										$this->db->like('style', $set_type, 'BOTH');
									} 
									$this->db->like('style', $style_dist, 'BOTH');
									$this->db->where('delete_date', null);
									$this->db->where('poreference', $gblv['poreference']);								
									$cekstyle = $this->db->get('master_order');	

									if ($cekstyle->num_rows()!=0) {
										$cekdistribusi = $this->DistribusiModel->cekDistribusi($gblv['poreference'],$gblv['size'],$gblv['cut_num'],$gblv['start_no'],$gblv['end_no'],$cekstyle->row_array()['style'],$gblv['article'],$this->session->userdata('line_id'), $ceksources->sources);											
										if($cekdistribusi->num_rows() > 0){
											$cekdetail = $this->DistribusiModel->cekDetail($cekdistribusi->row_array()['distribusi_id'], $barcode_id);
										}
										
										if(isset($cekdetail) && $cekdetail->num_rows() == 0 || $cekdistribusi->num_rows() == 0){
											$data = array(												
												'poreference'    => $gblv['poreference'], 												
												'style'          => $cekstyle->row_array()['style'], 
												'size'           => $gblv['size'], 
												'barcode_id'     => $gblv['barcode_id'], 
												'qty'            => $gblv['qty'], 
												'article'        => $gblv['article'], 
												'component_name' => $gblv['komponen_name'], 
												'startend'       => $gblv['start_no'].'-'.$gblv['end_no'], 
												'cut_num'        => $gblv['cut_num']
											);
	
											array_push($result_array, $data);	
										} else{
											//cek disini
											$status    = 500;
											$line_name = $this->db->get_where('master_line', array('master_line_id'=>$cekdistribusi->row_array()['line_id']))->row_array()['line_name'];
											$pesan     = "Bundle yang sama sudah di scan di ".$line_name.", Pindah Loading Bundle tersebut di ".$line_name." apabila ingin di loading di line ini";
											$data = array(
												'status' => $status, 
												'pesan'  => $pesan, 
											);
											echo json_encode($data);
											return false;
										}																		
									} else{
										$status = 500;
										$pesan 	= "PO Tidak di temukan ! ! !";
										$data 	= array(
											'status' => $status, 
											'pesan'  => $pesan, 
										);
										echo json_encode($data);
										return false;
									}
										
								} else if($gblv['current_locator_id'] == 3 && $gblv['current_status'] == 'in'){
									$data_response = array(
										'status' => 500,
										'pesan'  => 'Barcode Sudah diloading'										
									);

									echo json_encode($data_response);
									return false;

								} else{
									$this->cdms_new->where('id', $gblv['current_locator_id']);
									$lastLocator = $this->cdms_new->get('master_locator')->row_array();									
									$data		 = [
										'status' => 500,
										'pesan'	 => "Bundle belum selesai di distribusi. Kembalikan ke Last locator ".($lastLocator ? $lastLocator['locator_name']:null)
									];			

									echo json_encode($data);
									return false;
								}
							}

							if(sizeof($result_array) > 0){
								$data_response = [
									'status'  => 200,
									'data' 	  => $result_array,
									'sources' => 'cdms_new'
								];																	
								echo json_encode($data_response);	
							}

						} else{
							$status = 500;
							$pesan  = "Barcode Bundle Bukan Untuk Pabrik Ini";
							$data 	= array(
								'status' => $status, 
								'pesan'  => $pesan, 
							);
							echo json_encode($data);
						}

					} else{
						$status = 500;
						$pesan 	= "Barcode Bundle Tidak di temukan";
						$data 	= array(
							'status' => $status, 
							'pesan'  => $pesan, 
						);
						echo json_encode($data);
					}
					
				} else {
					$status = 500;
					$pesan 	= "Barcode sudah di receive !";
					$data 	= array(
						'status' => $status, 
						'pesan'  => $pesan, 
					);
					echo json_encode($data);
				}
			} else{
				$status = 500;
				$pesan  = "Barcode sources not found !";
				$data 	= array(
					'status' => $status, 
					'pesan'  => $pesan, 
				);
				echo json_encode($data);
			}			

		}
	}

	public function SaveReceiveBundle($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			//pindah loading validasi			
			$ScanBandles   = json_decode($post['items']);			
			$this->db->trans_start();
			// $this->dist->trans_start();
			$this->cdms_new->trans_start();

			foreach ($ScanBandles as $key => $scan){
				list($start, $end)  = explode('-', $scan->startend);
				$aman 				= 0;
				$po     			= $scan->poreference;
				$po2 				= substr($scan->poreference,-2);
				$sample 			= "-S";				

				if($sample != $po2){
					$poreference = $po;
					$sample 	 = false;
				} else{
					$poreference = rtrim($po, "-S");
					$sample 	 = true;					
				}

				$cekdistribusi = $this->DistribusiModel->cekDistribusi($poreference,$scan->size,$scan->cut_num,$start,$end,$scan->style,$scan->article,$this->session->userdata('line_id'), $scan->sources);
				$distribusi_id = $this->uuid->v4();

				if ($cekdistribusi->num_rows()==0) {
					$cekbundle = $this->DistribusiModel->cekBundlePindah($poreference,$scan->size,$scan->cut_num,$start,$end,$scan->style,$scan->article,$this->session->userdata('line_id'));
					if ($cekbundle->num_rows()==0) {
						$this->InserDistribusi($scan,$distribusi_id,$start,$end);
						$this->InsertDistribusiDetail($scan,$distribusi_id,$post);
						$aman = 1;
					}
					else{						
						$cekk	 = $cekbundle->result_array();
						$pindah  = 0;
						foreach ($cekk as $c) {
							$dist_id 		= $cekk['distribusi_id'];
							$detail 		= $this->db->query("SELECT * FROM distribusi_detail where distribusi_id = '$dist_id'")->result_array();
							$total_pindah 	= $detail->num_rows();

							foreach ($detail as $d) {
								if($d->status == 'pindah line'){
									$pindah++;
								}
							}
						}
						
						if($pindah==$total_pindah){
							$this->InserDistribusi($scan,$distribusi_id,$start,$end);
							$this->InsertDistribusiDetail($scan,$distribusi_id,$post);
							$aman = 1;
						}
						else{
							$status = 500;
							$pesan 	= "SCAN PINDAH SEMUA KOMPONEN BUNDLE DULU YA";
							$aman 	= 0;
						}						
					}
				}else{					
					$this->InsertDistribusiDetail($scan,$cekdistribusi->row_array()['distribusi_id'],$post);
					$aman = 1;
				}
				
				if($aman == 1){
					$supplied_to  	= $this->supplied_to();
					$location 		= 'SEW'.$this->session->userdata('factory');
					$factory 		= 'AOI '.$this->session->userdata('factory');

					// if($scan->sources == 'sds'){
					// 	$this->dist->where_in('barcode_id', $scan->barcode_id);
					// 	$this->dist->update('bundle_info_new', array('supply'=>date('Y-m-d H:i:s'),'supply_pic'=>'-9999999999','supply_unlock'=>'FALSE','factory'=>$factory,'location'=>$location,'supplied_to'=>$supplied_to));							
					// 	$cekComplete = $this->DistribusiModel->cekComplete($scan->poreference,$scan->size,$scan->cut_num,$start,$end,$scan->style,$scan->article, $scan->sources);
						
					// 	if ($cekComplete->row_array()['gmt_ready_trial']==2) {
					// 		$distribusi = $this->DistribusiModel->getDistribusi($scan->poreference,$scan->size,$scan->cut_num,$start,$end,$scan->style,$scan->article,$this->session->userdata('line_id'));

					// 		foreach ($distribusi->result() as $key => $dist) {
					// 			$cekheader= $this->DistribusiModel->getHeader($dist->poreference,$dist->style,$dist->article);								
					// 			if ($cekheader->num_rows()==0) {
					// 				$inline_header = $this->uuid->v4();
					// 				$this->InsertHeader($dist,$this->session->userdata('nik_adm'),$inline_header);
					// 				$cekHeaderSize = $this->DistribusiModel->getHeaderSize($dist->poreference,$dist->style,$dist->size,$inline_header);

					// 				if ($cekHeaderSize->num_rows()==0) {
					// 					$this->InsertSize($dist,$this->session->userdata('nik_adm'),$inline_header);
					// 				} else{
					// 					$this->UpdateHeaderSize($cekHeaderSize->row_array()['header_size_id'],$dist);
					// 				}	

					// 			} else{
					// 				$cekHeaderSize = $this->DistribusiModel->getHeaderSize($dist->poreference,$dist->style,$dist->size,$cekheader->row_array()['inline_header_id']);
					// 				if ($cekHeaderSize->num_rows()==0) {
					// 					$this->InsertSize($dist,$this->session->userdata('nik_adm'),$cekheader->row_array()['inline_header_id']);
					// 				} else{
					// 					$this->UpdateHeaderSize($cekHeaderSize->row_array()['header_size_id'],$dist);
					// 				}
					// 			}
	
					// 		}
					// 	}
					// } else 
					if($scan->sources == 'cdms_new'){						
						$line_name =$this->db->get_where('master_line', array('master_line_id'=>$this->session->userdata('line_id')))->row_array()['line_name'];
						$this->cdms_new->where('barcode_group', $scan->barcode_id);
						$this->cdms_new->where('komponen_name', $scan->component_name);
						$this->cdms_new->update('bundle_detail', 
							[
								'sewing_received_at' 	=> date('Y-m-d H:i:s'),
								'sewing_received_by' 	=> $this->session->userdata('nik')."|".$this->session->userdata('factory_name'),
								'sewing_received_line' 	=> $line_name,
								'current_description'	=> 'Supply in line '.$line_name,
								'current_status_to'		=> 'in',
								'factory_id' 		    => $this->session->userdata('factory'),
							]
						);
						
						$this->cdms_new->where('deleted_at is null');
						$this->cdms_new->where('barcode_id', $scan->barcode_id);
						$this->cdms_new->where('is_canceled', false);
						$this->cdms_new->order_by('created_at', 'desc');
						$last_status_movement = $this->cdms_new->get('distribusi_movements')->row_array();

						if($last_status_movement != null){
							if($last_status_movement['status_to'] == 'out' && $last_status_movement['locator_to'] == '3'){
								
								$movement = array(
									'id' 				 => $this->uuid->v4(), 
									'barcode_id' 		 => $scan->barcode_id,
									'locator_from' 		 => $last_status_movement['locator_to'],
									'locator_to' 		 => '3',
									'status_from' 		 => $last_status_movement['status_to'],
									'status_to' 		 => $last_status_movement['status_from'],
									'description' 		 => 'SUPPLY to '.$line_name.' | '.$this->session->userdata('factory_name'),
									'user_id' 			 => ($this->session->userdata('factory_name') == 'AOI2' ? '34' : '35'),
									'ip_address' 		 => $this->input->ip_address(),
									'factory_id' 		 => $this->session->userdata('factory'),
									'created_at' 		 => date('Y-m-d H:i:s'),
									'updated_at' 		 => date('Y-m-d H:i:s'),
									'supply_line' 		 => $line_name.' | '.$this->session->userdata('factory_name'),
									'sewing_received_by' => $this->session->userdata('nik')."|".$this->session->userdata('name'),
								);
	
								$this->cdms_new->insert('distribusi_movements', $movement);
							}
						} else{							
							$data = array(
								'status' => 500,
								'pesan'  => 'Contact Administrator'
							);
							echo json_encode($data);
							return false;
						}
						
						$cekComplete 		= $this->DistribusiModel->cekComplete($scan->poreference,$scan->size,$scan->cut_num,$start,$end,$scan->style,$scan->article, $scan->sources);
						$distribusi 		= $this->DistribusiModel->getDistribusi($scan->poreference,$scan->size,$scan->cut_num,$start,$end,$scan->style,$scan->article,$this->session->userdata('line_id'));
						$distribusi_details = $this->db->where('distribusi_id', $distribusi->row_array()['distribusi_id'])->get('distribusi_detail');
						
						if($cekComplete->row_array()['qty_komponen'] == $distribusi_details->num_rows()) {
							foreach ($distribusi->result() as $key => $dist) {
								$cekheader= $this->DistribusiModel->getHeader($dist->poreference,$dist->style,$dist->article);
								
								if ($cekheader->num_rows()==0) {
									$inline_header = $this->uuid->v4();
									$this->InsertHeader($dist,$this->session->userdata('nik_adm'),$inline_header);
									$cekHeaderSize = $this->DistribusiModel->getHeaderSize($dist->poreference,$dist->style,$dist->size,$inline_header);
									if ($cekHeaderSize->num_rows()==0) {
										$this->InsertSize($dist,$this->session->userdata('nik_adm'),$inline_header);
									}else{
										$this->UpdateHeaderSize($cekHeaderSize->row_array()['header_size_id'],$dist);
									}
									
								}else{
									$cekHeaderSize = $this->DistribusiModel->getHeaderSize($dist->poreference,$dist->style,$dist->size,$cekheader->row_array()['inline_header_id']);
									if ($cekHeaderSize->num_rows()==0) {
										$this->InsertSize($dist,$this->session->userdata('nik_adm'),$cekheader->row_array()['inline_header_id']);
									}else{
										$this->UpdateHeaderSize($cekHeaderSize->row_array()['header_size_id'],$dist);
									}
								}

							}
						}						
						// $cekComplete = $this->DistribusiModel->cekComplete($scan->poreference,$scan->size,$scan->cut_num,$start,$end,$scan->style,$scan->article);						
					}
				}
			}

			// $this->cekQtyBeda();
			if($aman == 1){
				if ($this->db->trans_status() == FALSE)
				{
					$status = 500;
					$pesan 	= "Simpan Data gagal";
					$this->db->trans_rollback();
					// $this->dist->trans_rollback();
					$this->cdms_new->trans_rollback();
				} else{
					$status = 200;
					$pesan 	= "Berhasil";					
					$this->db->trans_complete();
					// $this->dist->trans_complete();
					$this->cdms_new->trans_complete();
				}
			} else{
				$status = 500;
				$pesan 	= "SCAN PINDAH SEMUA KOMPONEN BUNDLE DULU YA";	
				$this->db->trans_rollback();
				// $this->dist->trans_rollback();
				$this->cdms_new->trans_rollback();
			}
			
			$data = array(
				'status' => $status, 
				'pesan'	 => $pesan, 
			);
			echo json_encode($data);
		}
	}

	private function supplied_to()
	{
		$location  = 'SEW'.$this->session->userdata('factory');
		$factory   = 'AOI '.$this->session->userdata('factory');
		$line_name = $this->db->get_where('master_line', array('master_line_id'=>$this->session->userdata('line_id')))->row_array()['desc_name_fastreact'];

		return 'AOI#'.$this->session->userdata('factory').' '.$line_name;
	}

	private function InserDistribusi($scan,$distribusi_id,$start,$end)
	{		
		$po     = $scan->poreference;
		$po2 	= substr($scan->poreference,-2);
		$sample = "-S";

				// if( strpos( $po, $sample ) !== false) {
				// 	$poreference = rtrim($po, "-S");
				// 	$sample = true;
				// }
				// else{
				// 	$poreference = $po;
				// 	$sample = false;
				// }

				if($sample != $po2){
					$poreference = $po;
					$sample 	 = false;
				} else{
					$poreference = rtrim($po, "-S");
					$sample 	 = true;
					
				}



		$distribusi = array(
			'distribusi_id' => $distribusi_id,
			'style'         => $scan->style,
			'poreference'   => $poreference,
			'size'          => $scan->size,
			'article'       => $scan->article,
			'qty'           => $scan->qty,
			'cut_num'       => $scan->cut_num,
			'start_num'     => $start,
			'start_end'     => $end,
			'factory_id'    => $this->session->userdata('factory'),
			'status'        => 'onprogress',
			'line_id'       => $this->session->userdata('line_id'),
			'is_sample'     => $sample,
			'source' 		=> $scan->sources
		);
		
		$this->db->insert('distribusi', $distribusi);
	}

	private function InsertDistribusiDetail($scan,$distribusi_id,$post)
	{
		$cekDistribusiDetail = $this->DistribusiModel->cekDistribusiDetail($scan->barcode_id);
		if ($cekDistribusiDetail->num_rows()==0 && $scan->sources == 'sds') {			
			$distribusidetail = array(
				'detail_id'      => $this->uuid->v4(), 
				'distribusi_id'  => $distribusi_id, 
				'barcode_id'     => $scan->barcode_id, 
				'create_by'      => $post['nik'], 
				'component_name' => $scan->component_name,
				'status'         => 'onprogress',
				'ip_address'     => $this->input->ip_address(),
				// 'sources'		 => $scan->sources
			);
		} else {
			$distribusidetail = array(
				'detail_id'      => $this->uuid->v4(), 
				'distribusi_id'  => $distribusi_id, 
				'barcode_id'     => $scan->barcode_id, 
				'create_by'      => $post['nik'], 
				'component_name' => $scan->component_name,
				'status'         => 'onprogress',
				'ip_address'     => $this->input->ip_address(),
				// 'sources'		 => $scan->sources
			);
		}
		$this->db->insert('distribusi_detail', $distribusidetail);			
	}

	private function InsertHeader($dist,$nik,$inline_header)
	{
		$this->db->where('line_id', $this->session->userdata('line_id'));
        $this->db->where('delete_at is null');
        $history_id = $this->db->get('history_line')->row_array();		
		$header = array(
			'inline_header_id' => $inline_header, 
			'poreference'      => $dist->poreference, 
			'style'            => $dist->style, 
			'line_id'          => $this->session->userdata('line_id'), 
			'factory_id'       => $this->session->userdata('factory'),
			'startdateactual'  => date('Y-m-d'), 
			'history_id'       => $history_id ? $history_id['history_id'] : null, 
			'article'          => $dist->article, 
			'create_by'        => $nik 
		);
		$this->db->insert('inline_header', $header);
	}

	private function InsertSize($dist,$nik,$inline_header)
	{		
		$headerSize = array(
			'inline_header_id' => $inline_header,
			'header_size_id'   => $this->uuid->v4(),  
			'size'             => $dist->size,   
			'style'            => $dist->style,    
			'qty'              => $dist->qty,
			'poreference'      => $dist->poreference,
			'create_by'        => $nik        
		);
		$this->db->insert('inline_header_size', $headerSize);

		$this->db->where('distribusi_id', $dist->distribusi_id);
		$this->db->update('distribusi_detail', array('status'=>'completed'));

		$this->db->where('distribusi_id', $dist->distribusi_id);
		$this->db->update('distribusi', array('status'=>'completed','inline_header_id'=>$inline_header,'complete_at'=>date('Y-m-d h:i:s')));

		$this->db->where('inline_header_id', $inline_header);
		$this->db->select('sum(qty) as qty');
		$qty = $this->db->get('inline_header_size')->row_array()['qty'];

		$this->db->where('inline_header_id', $inline_header);
		$this->db->update('inline_header',array('targetqty'=>$qty));
	}

	private function UpdateHeaderSize($size_id,$dist)
	{
		$this->db->where('header_size_id', $size_id);
		$qty_header = $this->db->get('inline_header_size')->row_array();

		$this->db->where('distribusi_id', $dist->distribusi_id);
		$this->db->update('distribusi_detail', array('status'=>'completed'));

		$this->db->where('distribusi_id', $dist->distribusi_id);
		$this->db->update('distribusi', array('status'=>'completed','inline_header_id'=>$qty_header['inline_header_id'],'complete_at'=>date('Y-m-d h:i:s')));

		$qty_receive = $this->DistribusiModel->getQtyReceive($dist->poreference,$dist->size,$dist->style,$dist->article,$this->session->userdata('line_id'));
		$this->db->where('header_size_id', $size_id);
		$this->db->update('inline_header_size', array('qty'=>$qty_receive));

		$this->db->where('inline_header_id', $qty_header['inline_header_id']);
		$this->db->select('sum(qty) as qty');
		$qty = $this->db->get('inline_header_size')->row_array()['qty'];

		$this->db->where('inline_header_id', $qty_header['inline_header_id']);
		$this->db->update('inline_header',array('targetqty'=>$qty));
	}

	private function cekQtyBeda()
	{
		$line_id = $this->session->userdata('line_id');
		$cek 	 = $this->DistribusiModel->getQtyBeda($line_id);
		if ($cek->num_rows()>0) {
			foreach ($cek->result() as $key => $c) {
				$this->db->where('style', $c->style);
				$this->db->where('size', $c->size);
				$this->db->where('inline_header_id', $c->inline_header_id);
				$this->db->update('inline_header_size', array('qty'=>$c->total_receive));

				$this->db->where('inline_header_id', $c->inline_header_id);
				$this->db->select('sum(qty) as qty');
				$qty = $this->db->get('inline_header_size')->row_array()['qty'];

				$this->db->where('inline_header_id', $c->inline_header_id);
				$this->db->update('inline_header',array('targetqty'=>$qty));

				$query = "select * from update_targetqty('$c->style',$c->line_id,$c->factory_id)";
				$this->db->query($query);
			}
		}
	}

	public function Report_Receive()
	{
		$line_id = $this->session->userdata('line_id');
		$line	 = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();		
		$data 	 = array('line' =>$line , );

		$this->template->set('title',$line['line_name'].' - Folding');
        $this->template->set('desc_page','Folding');
		$this->template->load('layout2','folding/index_report_receive_distribusi');

	}

	public function ReportReceiveModal()
	{
		$this->load->view('folding/index_report_receive_distribusi');
	}
	
	public function report_receive_ajax()
	{
		$columns = array(
			0 =>'create_date',
			1 =>'style',
			2 =>'poreference',
			3 =>'size',
			4 =>'article',
			5 =>'qty',
			6 =>'cut_num',
			7 =>'start_num',
			8 =>'create_date',
			9 =>'status',
		);

		$limit  	 	= $this->input->post('length');
		$start  	 	= $this->input->post('start');
		$order  	 	= $columns[$this->input->post('order')[0]['column']];
		$dir    	 	= $this->input->post('order')[0]['dir'];
		$draw   	 	= $this->input->post('draw');
		$factory	 	= $this->session->userdata('factory');
		$line   	 	= $this->session->userdata('line_id');
		$poreference 	= $this->input->post('poreference');
        $date    	 	= $this->input->post('tanggal');
        $pilih   	 	= $this->input->post('pilih_size');
        $totalData 	 	= $this->DistribusiModel->allposts_count_receive($line,$factory);
        $totalFiltered  = $totalData;
		
        if($poreference==''&&$date=='')
        {
            $master = $this->DistribusiModel->allposts_receive($limit,$start,$order,$dir,$line,$factory);
        }
        else {        	
            $search 		= $this->input->post('search')['value'];
            $master 		= $this->DistribusiModel->posts_search_receive($limit,$start,$search,$order,$dir,$line,$factory,$date,$poreference,$pilih);
            $totalFiltered  = $this->DistribusiModel->posts_search_count_receive($search,$line,$factory,$date,$poreference,$pilih);
        }

		$data          = array();
		$nomor_urut    = 0;
		
        if(!empty($master))
        {	

            foreach ($master as $master)
            {
            	$_temp 						= '"'.$master->poreference.'","'.$master->size.'","'.$master->cut_num.'","'.$master->start_num.'","'.$master->start_end.'","'.$master->style.'","'.$master->distribusi_id.'","'.$master->article.'"';
            	$status 					= ($master->status=='completed'?'<span class="badge bg-success">'.$master->status.'</span>':'<span class="badge bg-primary">'.$master->status.'</span>');
				$nestedData['no']           = (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['style']        = $master->style;
				$nestedData['poreference']  = $master->poreference;
				$nestedData['size']         = $master->size;
				$nestedData['article']      = $master->article;
				$nestedData['qty']          = $master->qty;
				$nestedData['cut_num']      = $master->cut_num;				
				$nestedData['sticker']      = $master->start_num."-".$master->start_end;
				$nestedData['create_date']  = $master->create_date;
				$nestedData['status']       = $status;
				$nestedData['detail']       = "<div class='btn-group'>
                                                  <a onclick='return detail($_temp)' class='btn btn-primary' href='javascript:void(0)'><span data-icon='&#xe101'></i></a>
                                              </div>";								
				$data[]                    	= $nestedData;
				
            }
        }
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}

	public function report_summary_ajax($post='')
	{

		
		$post 	 = $this->input->post('poreference');
		$size 	 = $this->input->post('size');
		$factory = $this->session->userdata('factory');
		$line    = $this->session->userdata('line_id');

		$no=1;
		$draw='';
		if ($post!='') {
			
			$master = $this->DistribusiModel->posts_search_distributionsummary($line,$factory,$post,$size);
			// $size = $this->db->get('distribusi');

			$draw .= '<div class="box-title">Summary</div>
			<div class="box-body">
			<table class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="text-center" width="20px">NO</th>
						<th class="text-center">STYLE</th>
						<th class="text-center" width="100px">PO BUYER</th>
						<th class="text-center" width="80px">SIZE</th>
						<th class="text-center" width="80px">ARTICLE</th>
						<th class="text-center" width="50px">QTY</th>
						<th width="80px">STATUS</th>
					</tr>
              	</thead>
              	<tbody>';
				foreach($master as $d){
					$status = ($d->status=='onprogress'||$d->status=='pindah line'?'<center><span class="badge bg-primary">'.$d->status.'</span></center>':'<center><span class="badge bg-success">'.$d->status.'</span></center>');
					
					$draw .='<tr>'.
								'<td>'.$no++.'</td>'.
								'<td>'.$d->style.'</td>'.
								'<td>'.$d->poreference.'</td>'.
								'<td>'.$d->size.'</td>'.
								'<td>'.$d->article.'</td>'.
								'<td>'.$d->sum.'</td>'.
								'<td>'.$status.'</td>'.
						'</tr>';
					}
					$draw .= '
				</tbody>
			</table>';
		}

		$data = array('draw' => $draw );
	    echo json_encode($data);
	}
	
	public function scanbandle_pindah($barcode_id=NULL)
	{
		$barcode_id = $this->input->post('barcode_id');
		if ($barcode_id!=NULL) {			
			$cekbarcode = $this->DistribusiModel->cekBarcode($barcode_id);			
			if ($cekbarcode->num_rows()>0) {
				// var_dump($cekbarcode->row_array());
				// if($cekbarcode->row_array()['source'] == 'sds' || $cekbarcode->row_array()['source'] == null){
				// 	// var_dump($cekbarcode->row_array());die();
				// 	$distribusi_detail 	= $cekbarcode->row_array();					
				// 	$cekdistribusi		= $this->DistribusiModel->cekDistribusi($distribusi_detail['poreference'],$distribusi_detail['size'],$distribusi_detail['cut_num'],$distribusi_detail['start_num'],$distribusi_detail['start_end'],$distribusi_detail['style'],$distribusi_detail['article'],$this->session->userdata('line_id'), $cekbarcode->row_array()['source']);
				// 	$getTemp 			= $this->DistribusiModel->getTempPindah('',$distribusi_detail['barcode_id']);
	
				// 	if ($getTemp->num_rows()==0) {
				// 		$this->db->trans_start();
				// 		if ($cekdistribusi->row_array()['status']=='completed') {
				// 			$getTemp = $this->cekAvailable($distribusi_detail);
							
				// 			if ($getTemp==1) {									
				// 				$this->InsertTemp($cekbarcode);
				// 				$detail = $this->resultScanPindah($cekbarcode);
				// 				echo json_encode($detail);
				// 			} else{
				// 				$status = 500;
				// 				$pesan  = "QTY tidak bisa di pindah karena sudah di input QC endline";
				// 				$data   = array(
				// 					'status' => $status, 
				// 					'pesan'  => $pesan, 
				// 				);
				// 				echo json_encode($data);
				// 			}	
				// 		} else{
				// 			$this->InsertTemp($cekbarcode);
				// 			$detail = $this->resultScanPindah($cekbarcode);
				// 			echo json_encode($detail);
				// 		}

				// 		if ($this->db->trans_status() == FALSE)
				// 		{
				// 			$this->db->trans_rollback();
				// 		}else{
				// 			$this->db->trans_complete();
				// 		}
				// 	}else{
				// 		$status = 500;
				// 		$pesan  = "Barcode Sudah di Scan";
				// 		$data 	= array(
				// 			'status' => $status, 
				// 			'pesan'  => $pesan, 
				// 		);
				// 		echo json_encode($data);
				// 	}
				// } else
				 if($cekbarcode->row_array()['source'] == 'cdms_new'){
					$result_array 		= array();					
					$distribusi_detail  = $cekbarcode->row_array();			
					$cekdistribusi 		= $this->DistribusiModel->cekDistribusi($distribusi_detail['poreference'],$distribusi_detail['size'],$distribusi_detail['cut_num'],$distribusi_detail['start_num'],$distribusi_detail['start_end'],$distribusi_detail['style'],$distribusi_detail['article'],$this->session->userdata('line_id'), $cekbarcode->row_array()['source']);	
					$getTemp 			= $this->DistribusiModel->getTempPindah('',$distribusi_detail['barcode_id']);

					if ($getTemp->num_rows()==0) {
						$this->db->trans_start();

						if ($cekdistribusi->row_array()['status']=='completed') {
							$getTemp = $this->cekAvailable($distribusi_detail);							

							if($getTemp==1) {	
								$this->InsertTemp($cekbarcode);	
								$detail = $this->resultScanPindah($cekbarcode);	

								echo json_encode($detail);
							} else{
								$status = 500;
								$pesan 	= "QTY tidak bisa di pindah karena sudah di input QC endline";
								$data 	= array(
									'status' => $status, 
									'pesan'  => $pesan, 
								);

								echo json_encode($data);
							}	
						} else{
							$this->InsertTemp($cekbarcode);
							$detail = $this->resultScanPindah($cekbarcode);

							echo json_encode($detail);
						}

						if($this->db->trans_status() == FALSE){
							$this->db->trans_rollback();
						} else{
							$this->db->trans_complete();
						}
					} else{
						$status = 500;
						$pesan  = "Barcode Sudah di Scan";
						$data   = array(
							'status' => $status, 
							'pesan'  => $pesan, 
						);
						echo json_encode($data);
					}				
				}
			}else{
				$status = 500;
				$pesan  = "Barcode belum di receive";
				$data   = array(
					'status' => $status, 
					'pesan'  => $pesan, 
				);
				echo json_encode($data);
			}
		}
	}

	private function resultScanPindah($cekbarcode)
	{		
		// if($cekbarcode->row_array()['source'] == 'sds'){
		// 	$status = 200;			
		// 	$data 	= array(
		// 		'status'  => $status, 
		// 		'sources' => $cekbarcode->row_array()['source'],
		// 		'data'	  => array(
		// 			'distribusi_id'  => $cekbarcode->row_array()['distribusi_id'], 
		// 			'poreference'    => $cekbarcode->row_array()['poreference'], 
		// 			'style'          => $cekbarcode->row_array()['style'], 
		// 			'size'           => $cekbarcode->row_array()['size'], 
		// 			'barcode_id'     => $cekbarcode->row_array()['barcode_id'], 
		// 			'qty'            => $cekbarcode->row_array()['qty'], 
		// 			'article'        => $cekbarcode->row_array()['article'], 
		// 			'component_name' => $cekbarcode->row_array()['component_name'], 
		// 			'startend'       => $cekbarcode->row_array()['start_num'].'-'.$cekbarcode->row_array()['start_end'], 
		// 			'cut_num'        => $cekbarcode->row_array()['cut_num']
		// 		)
		// 	);
		// } else 
		if($cekbarcode->row_array()['source'] == 'cdms_new'){
			$result_array = array();
			$status 	  = 200;	

			foreach($cekbarcode->result_array() as $ckbr){				
				$array = array(
					'status' => $status, 
					'data' 	 => array(
						'distribusi_id'  => $ckbr['distribusi_id'], 
						'poreference'    => $ckbr['poreference'], 
						'style'          => $ckbr['style'], 
						'size'           => $ckbr['size'], 
						'barcode_id'     => $ckbr['barcode_id'], 
						'qty'            => $ckbr['qty'], 
						'article'        => $ckbr['article'], 
						'component_name' => $ckbr['component_name'], 
						'startend'       => $ckbr['start_num'].'-'.$ckbr['start_end'], 
						'cut_num'        => $ckbr['cut_num'],
						'sources'		 => $ckbr['source']
					)
				);
				array_push($result_array, $array);

				$data = array(
					'status'  => $status,
					'sources' => $cekbarcode->row_array()['source'],
					'data' 	  => $result_array
				);
			}
		}
		return $data;
	}

	private function InsertTemp($cekbarcode)
	{
		foreach ($cekbarcode->result() as $cek) {
			$detail_insert = array( 
				'temp_id'        => $this->uuid->v4(), 
				'poreference'    => $cek->poreference, 
				'line_id'        => $cek->line_id, 
				'factory_id'     => $this->session->userdata('factory'), 
				'distribusi_id'  => $cek->distribusi_id, 
				'style'          => $cek->style, 
				'size'           => $cek->size, 
				'barcode_id'     => $cek->barcode_id, 
				'qty'            => $cek->qty, 
				'article'        => $cek->article, 
				'component_name' => $cek->component_name, 
				'start_num'      => $cek->start_num,
				'start_end'      => $cek->start_end, 
				'cut_num'        => $cek->cut_num,
				'source'		 => $cek->source
			);			
			$this->db->insert('temp_distribusi_pindah',$detail_insert);
		}		
	}

	public function cekAvailable($distribusi_detail)
	{
		$getTemp = $this->DistribusiModel->getTemp($distribusi_detail['poreference'],$distribusi_detail['size'],$distribusi_detail['cut_num'],$distribusi_detail['start_num'],$distribusi_detail['start_end'],$distribusi_detail['style'],$distribusi_detail['article']);

		if ($getTemp->num_rows()==0) {
			$available = $this->DistribusiModel->getAvailable($distribusi_detail['poreference'],$distribusi_detail['size'],$distribusi_detail['style'],$distribusi_detail['qty'],$distribusi_detail['article']);
			// $calculate = $available - $cekbarcode->row_array()['qty'];
			$calculate  = (int)$available['available'] - (int)$distribusi_detail['qty'];
			
			if ($calculate>=0) {
				$where = array(
					'inline_header_id' => $distribusi_detail['inline_header_id'], 
					'poreference'      => $distribusi_detail['poreference'], 
					'size'             => $distribusi_detail['size'], 
					'style'            => $distribusi_detail['style'], 
				);
				$newQty = $available['qty_order']-(int)$distribusi_detail['qty']; 

				$this->db->where($where);
				$this->db->update('inline_header_size', array('qty'=>$newQty));
				return 1;
			}else{
				return 0;
			}
		} else{
			return 1;
		}
	}

	public function deletepindah($barcode_id=NULL)
	{
		$barcode_id = $this->input->post('id');
		
		if ($barcode_id!=NULL) {
			$komponen 		= $this->input->post('komponen');
			$line_id 		= $this->session->userdata('line_id');
			$cekdistribusi 	= $this->DistribusiModel->getTempPindah($line_id,$barcode_id);

			if ($cekdistribusi->num_rows()>0) {
				$this->db->trans_start();
				$cekdistribusi = $this->DistribusiModel->cekBarcode($barcode_id);

				if ($cekdistribusi->row_array()['status']=='completed') {
					$distribusi_detail = $cekdistribusi->row_array();
					$this->db->where('barcode_id', $barcode_id);
					$this->db->where('component_name', $komponen);
					$this->db->delete('temp_distribusi_pindah');
					
					$getTemp = $this->DistribusiModel->getTemp($distribusi_detail['poreference'],$distribusi_detail['size'],$distribusi_detail['cut_num'],$distribusi_detail['start_num'],$distribusi_detail['start_end'],$distribusi_detail['style'],$distribusi_detail['article']);
					
					if ($getTemp->num_rows()==0) {
						$available 	= $this->DistribusiModel->getAvailable($distribusi_detail['poreference'],$distribusi_detail['size'],$distribusi_detail['style'],$distribusi_detail['qty'],$distribusi_detail['article']);
						$newQty  	= (int)$available['qty_order'] + (int)$distribusi_detail['qty'];						
						$header_id 	= $this->DistribusiModel->getHeaders($distribusi_detail['poreference'],$distribusi_detail['article'])->row_array()['inline_header_id'];

						$where = array(
							'inline_header_id' => $header_id, 
							'poreference'      => $distribusi_detail['poreference'], 
							'size'             => $distribusi_detail['size'], 
							'style'            => $distribusi_detail['style'], 
						);
						$this->db->where($where);
						$this->db->update('inline_header_size', array('qty'=>$newQty));
					}
				}else{
					$this->db->where('barcode_id', $barcode_id);
					$this->db->where('component_name', $komponen);
					$this->db->delete('temp_distribusi_pindah');
				}
				if ($this->db->trans_status() == FALSE)
				{
					$status = 500;
					$this->db->trans_rollback();
				}else{
					$status = 200;
					$this->db->trans_complete();
				}
				$data = array(
					'status' => $status, 
				);
				echo json_encode($data);
			}

		}
	}

	public function SavePindahBundle($post=NULL)
	{
		$post 	= $this->input->post();
		$this->db->trans_start();
		// $this->dist->trans_start();
		$this->cdms_new->trans_start();
		$status	= 0;
		if ($post!=NULL) {
			$ScanBundles = json_decode($post['items']);						
			$line 		 = $this->session->userdata('line_id');
			$factory 	 = $this->session->userdata('factory'); 			

			if ($factory=='1' || $factory==1) {
				$locPin = "LOC.A1              ";
			}else if ($factory=='2' || $factory==2) {
				$locPin = "L.1                 ";
			}

			$cek_bundle   = $this->db->query("SELECT distinct distribusi_id FROM temp_distribusi_pindah 
							where line_id = '$line' and factory_id = '$factory'");

			if($cek_bundle->num_rows()>1){
				$status = 500;
				$pesan  = "TIDAK BOLEH LEBIH DARI 1 BUNDLE";
			} else{
				foreach ($ScanBundles as $key => $bundle) {
					// var_dump($bundle);die();
					$detail 		= $this->db->query("SELECT * FROM distribusi_detail where distribusi_id = '$bundle->distribusi_id'");
					$total_pindah 	= $detail->num_rows();
					$size_of_bundle = sizeof($ScanBundles);

					if($total_pindah == $size_of_bundle){
						$nik = $this->session->userdata('nik_adm');
						$this->db->where('barcode_id', $bundle->barcode_id);
						$this->db->update('distribusi_detail', array('hand_over'=>'t','delete_at'=>date('Y-m-d H:i:s'),'status'=>'pindah line','delete_by'=>$nik));

						$this->db->where('barcode_id', $bundle->barcode_id);
						$this->db->delete('temp_distribusi_pindah');
						
						$this->db->where('distribusi_id', $bundle->distribusi_id);
						$this->db->update('distribusi', array('status'=>'pindah line','delete_at'=>date('Y-m-d H:i:s')));
						
						// if($bundle->sources == 'sds' || $bundle->sources == null){
						// 	$this->dist->where_in('barcode_id', $bundle->barcode_id);
						// 	$this->dist->update('bundle_info_new', array('supply'=>NULL,'supply_pic'=>NULL,'supply_unlock'=>'FALSE','location'=>$locPin,'supplied_to'=>NULL));	
						// }

						if($bundle->sources == 'cdms_new'){
							$line_name =$this->db->get_where('master_line', array('master_line_id'=>$this->session->userdata('line_id')))->row_array()['line_name'];
							$this->cdms_new->where_in('barcode_group', $bundle->barcode_id);
							$this->cdms_new->update('bundle_detail', array(
								'current_status_to' 	=> 'out',
								'current_description' 	=> null,
								'sewing_received_at' 	=> null,
								'sewing_received_by' 	=> null,
								'sewing_received_line' 	=> null
							));
							
							$this->cdms_new->where('deleted_at is null');
							$this->cdms_new->where('barcode_id', $bundle->barcode_id);
							$this->cdms_new->where('is_canceled', false);
							$this->cdms_new->order_by('created_at', 'desc');
							$cdms_move = $this->cdms_new->get('distribusi_movements')->row_array();

							if($cdms_move != null){
								if($cdms_move['status_to'] == 'in'){
									$this->cdms_new->where('id', $cdms_move['id']);
									$this->cdms_new->update('distribusi_movements', array(								
										'is_canceled'		 => true,
										'canceled_by'		 => ($this->session->userdata('factory_name') == 'AOI2' ? '34' : '35'),
										'sewing_canceled_by' => $this->session->userdata('nik')."|".$this->session->userdata('name'),
										'updated_at' 		 => date('Y-m-d H:i:s'),
									));			
									
									$movement = array(
										'id' 				 => $this->uuid->v4(), 
										'barcode_id' 		 => $bundle->barcode_id,
										'locator_from' 		 => $cdms_move['locator_to'],
										'locator_to' 		 => '3',
										'status_from' 		 => $cdms_move['status_to'],
										'status_to' 		 => $cdms_move['status_from'],
										'description' 		 => 'SUPPLY out from '.$line_name.' | '.$this->session->userdata('factory_name'),
										'user_id' 			 => ($this->session->userdata('factory_name') == 'AOI2' ? '34' : '35'),
										'ip_address' 		 => $this->input->ip_address(),
										'factory_id' 		 => $this->session->userdata('factory'),
										'created_at' 		 => date('Y-m-d H:i:s'),
										'updated_at' 		 => date('Y-m-d H:i:s'),
										'supply_line' 		 => $line_name.' | '.$this->session->userdata('factory_name'),
										'sewing_received_by' => $this->session->userdata('nik')."|".$this->session->userdata('name')
									);
		
									$this->cdms_new->insert('distribusi_movements', $movement);
								}
							} else{
								$data = array(
									'status' => 500,
									'pesan'  => 'Error, Contact Administrator'
								);

								echo json_encode($data);
								return false;
							}							
						}
					}
					else{
						$status = 500;
						$pesan  = "SCAN SEMUA KOMPONEN DARI BUNDLE YG SUDAH DI SCAN YA";
					}
					// $this->cekQtyBeda();
				}	
			}
		}
		if($status == 500){
			$this->db->trans_rollback();
			// $this->dist->trans_rollback();
			$this->cdms_new->trans_rollback();
		}
		else{
			if ($this->db->trans_status() == FALSE)
			{
				$status = 500;
				$pesan 	= "Simpan Data gagal";
				$this->db->trans_rollback();
				// $this->dist->trans_rollback();
				$this->cdms_new->trans_rollback();
			}else{
				$status = 200;
				$pesan 	= "Berhasil";
				$this->db->trans_complete();
				// $this->dist->trans_complete();
				$this->cdms_new->trans_complete();
			}
		}
		$data = array(
			'status' => $status, 
			'pesan'  => $pesan, 
		);
		
		echo json_encode($data);
	}

	public function detailComponent($get=NULL)
	{
		$get =$this->input->get();

		if ($get!=NULL) {
			
			$this->db->select('is_sample');
			$this->db->where('distribusi_id', $get['distribusi_id']);
		
			$cek_sample = $this->db->get('distribusi');
			$dist_id 	= $get['distribusi_id'];
			$cek 		= $this->db->query("SELECT is_sample, source from distribusi where distribusi_id = '$dist_id'")->row_array();

			if($cek['is_sample'] == 't'){
				$sample = "-S";
				$po 	= $get['poreference'] . $sample;
			} else{
				$po 	= $get['poreference'];
			}
						
			$detailComponent = $this->DistribusiModel->getDetailBundle($po,$get['size'],$get['start_num'],$get['start_end'],$get['cut_num'],$get['style'],$get['article'], $cek['source']);
			$draw			 = '';

			$this->db->where('nik', $this->session->userdata('nik'));
			$this->db->where('aplication', 'distribusi');
			$trust = $this->db->get('trust')->num_rows();			
			
			// if($this->session->userdata('line_id') == '61' || $this->session->userdata('line_id') == '63'
			// || $this->session->userdata('line_id') == '77' || $this->session->userdata('line_id') == '70')
			// {
			// 	$trust = $trust;
			// }   
			// else{
			// 	$trust = 0;
			// }			
			
			$no = 1;

			foreach ($detailComponent['data']->result() as $key => $detail) {
				// var_dump($detail); die();
				$this->db->where('distribusi_id', $get['distribusi_id']);				
				$this->db->where('barcode_id', $detail->barcode_id);
				$this->db->where('line_id', $this->session->userdata('line_id'));
				$this->db->where('component_name', isset($detail->komponen_name) ? $detail->komponen_name : $detail->component_name);
				$detailDistribusi = $this->db->get('distribusi_detail_view');
				$status		 	  = '<center><span class="badge bg-primary">Belum Diterima</span></center>';
				$create_by	 	  = '-';
				$delete_at	 	  = '-';
				$delete_by	 	  = '-';
				$supply_time 	  = '-';

				foreach ($detailDistribusi->result() as $key => $d) {										
					$status 	 = ($d->status=='onprogress'||$d->status=='completed'?'<center><span class="badge bg-success">Receive</span></center>':'<center><span class="badge bg-primary">'.ucwords($d->status).'</span></center>');
					$create_by 	 = $d->name;					
					$supply_time = date('Y-m-d H:i:s',strtotime($d->create_date));
					$delete_by   = $d->name_del;
					$delete_at   = $d->delete_at;
				}

				$draw .='<tr>'.
							'<td>'.$no++.'</td>';
							if ($trust==1) {
								$draw.='<td>'.$detail->barcode_id.'</td>';
							}
							if(isset($detail->component_name)){
								$draw.='<td>'.$detail->component_name.'</td>';
							} else if(isset($detail->komponen_name)) {
								$draw.='<td>'.$detail->komponen_name.'</td>';
							}							
							$draw.='<td>'.$supply_time.'</td>'.
							'<td>'.$create_by.'</td>'.
							'<td>'.$delete_at.'</td>'.
							'<td>'.$delete_by.'</td>'.
							'<td>'.$status.'</td>'.
				       '</tr>';
			}
			$data = array('draw' => $draw,'trust'=>$trust );
			$this->load->view('folding/index_component_receive_', $data);
		}
	}
	
	public function showSize($post='')
	{
		$post = $this->input->post('poreference');
		$draw = '';

		if ($post!='') {
			$this->db->order_by('size', 'asc');
			$this->db->like('poreference', $post, 'BOTH');
			$this->db->where('line_id', $this->session->userdata('line_id') );
			$this->db->select('size');
			$this->db->distinct();
			$size = $this->db->get('distribusi');
			
			if ($size->num_rows()>0) {
				$draw .='<select data-placeholder="Pilih Size" class="form-control " name="pilih" id="pilih_size">';
				foreach ($size->result() as $key => $size) {
	              $draw.= "<option value='".$size->size."' name=''>".$size->size."</option>";
	            }
			}
		}

		$data = array('draw' => $draw );
	    echo json_encode($data);

	}
}

/* End of file Distribusi.php */
/* Location: ./application/controllers/Distribusi.php */
