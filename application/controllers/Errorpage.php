<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errorpage extends CI_Controller {

	public function index()
	{
		$this->template->set('title','Error Page');
		$this->template->set('desc_page','Master Proses');
        $this->template->load('layout','errors/index_errorpage');
	}

}

/* End of file Errorpage.php */
/* Location: ./application/controllers/Errorpage.php */