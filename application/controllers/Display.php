<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('DisplayModel','DailyModel', 'DashboardModel'));
	}

	public function index()
	{
		
	}
	public function line($line=0,$factory=0)
	{
		if ($line!=0) {
			$data = array('line' => $line,'factory'=>$factory );
			// $this->template->set('title','MONITOR LINE'.$line);
	        // $this->template->set('desc_page','MONITOR LINE'.$line);
			// $this->template->load('layout3','display/main_dashboard',$data);

			// $this->load->view('display/main_dashboard',$data);
		
			$this->data_line($line,$factory);
			// $this->template->load('layout3','display/main_view',$data);
		}
	}
	public function data_line($line=0,$factory=0)
	{
		$qc_status = $this->DisplayModel->qcStatus($line,$factory)??['style'];
		
		$line_name      = $this->db->get_where('master_line', array('master_line_id'=>$line))->row_array()['line_name'];
		
		$jalan_style = $this->DisplayModel->JalanStyle($line,$factory);
		

		// $daily = $jalan_style->row_array();
		$dailystatus = $jalan_style->row_array();
		


		$cekStyle = $jalan_style->result();
		$cek = 0;
		foreach($cekStyle as $ds){
			// var_dump($ds->job_status);
			if($ds->job_status == 't'){
				$cek=$cek+1;
			} 

			// var_dump($cek);
		}
		// if ($dailystatus!=NULL&&$dailystatus['job_status']!='f') {
		if ($dailystatus!=NULL&&$cek>0) {
			//cek daily kalo ada yang aktif == liat, gak ada view before
			$style     = '';
			$smv       = '';
			$cum_day   = '';
			$operator  = '';
			$wft       = 0;
			$n_style   = 0;
			$date      = date('Y-m-d');
			
			$total_output_qc     = 0;
			$total_inline        = 0;
			$total_endline       = 0;
			$total_target        = 0;
			$qty_order           = 0;
			$total_sewing        = 0;
			$cek_qc              = 0;
			$sewing_jam          = 0;
			$qc_jam              = 0;
			$jam_ke              = '';
			$output_qc_min_1     = 0;
			$output_sewing_min_1 = 0;
			$potong              = 0;
			$style               = '<table id="table-style" class="borderless"><tr><td><ul style="list-style-type: none;margin: 0; padding: 0;">';
			$smv                 = '<table id="table-smv" class="borderless"><tr><td><ul style="list-style-type: none; margin: 0; padding: 0;">';
			
			$j_sekarang_1 = '';
			$j_sekarang_2 = '';
			$jm_lalu      = '';

			$query_jam_kerja = $this->db->query("SELECT jam_mulai, created_by,shift from daily_jam_kerja 
				where date(create_date) = '$date' and line_id ='$line' and factory_id = '$factory' ORDER BY create_date desc")->row_array();
			
			if($query_jam_kerja == null){		
				// $factory_name = $this->db->get_where('master_factory', array('factory_id'=>$factory))->row_array()['name'];
				// $line_name    = $this->db->get_where('master_line', array('master_line_id'=>$line))->row_array()['line_name'];
				// $data         = array('factory_name' => $factory_name,'line_name'=>$line_name );
				// $this->load->view('display/before',$data);
					$jam_kerja_mulai = '07:00:00';
			}else{		
				$jam_kerja_mulai = $query_jam_kerja['jam_mulai'];
			}
			
			$banyak_style = $jalan_style->num_rows(); 
			if($jalan_style->num_rows()>0){
				$list_style = $jalan_style->result();

				foreach ($list_style as $s) {
					$time  = time();
					// $time = '2020-01-09 07:10:00';
					// $time = strtotime($time);
					$ke = 0;

					// $shift = $query_jam_kerja['shift'];
					// $jam_kerja = $this->db->query("SELECT * FROM jam_kerja WHERE factory = '$factory' and shift = '$shift'")->result_array();	
					// $jam_kerja    = $this->db->get('jam_kerja')->result_array();
					$jam_kerja    = $this->db->query("SELECT * FROM jam_kerja WHERE factory=$factory ORDER BY awal")->result_array();

				

				
					foreach ($jam_kerja as $key => $jam) {
						
						if($jam['factory'] ==  $factory && $jam['awal']>=$jam_kerja_mulai)
							{
								// var_dump($jam['factory']);
								// die();
								$ke++;
							
								$awal  = date("Y-m-d").' '.$jam['awal'];
								$akhir = date("Y-m-d").' '.$jam['akhir'];
								
								$now   = date("Y-m-d H:i:s",$time);
								$jam_sekarang = date("H:i:s",$time);
								
								$x = strtotime($now);
								
								if($time >= strtotime($awal) && $time < strtotime($akhir)){
									$j_sekarang_1 = $awal;
									$j_sekarang_2 = $akhir;
							
	
									$jam_ke = $ke;
									$jam_nama = $jam['name'];
									
								}
							}
					
						
					}

					$awal         = $j_sekarang_1;
					$akhir        = $j_sekarang_2;
					$jam_pertama  = $jam_kerja_mulai;
					$waktu_akhir  = strtotime($awal);
					$jam_terakhir = date("H:i",$waktu_akhir);
				

					if($awal == null){
						$x    = date("Y-m-d H");
						$xx   = $x.":00:00";
						$awal = $xx;
					}
					
					$q = $this->db->query("SELECT sum(output) as output from line_summary_po where style = '$s->style' and date = '$date'
					and line_id = '$line' and activity_name = 'sewing' and jam_awal >= '$awal'")->row_array();
	
					$sewing_jam = $sewing_jam + $q['output'];

					$q1 = $this->db->query("SELECT sum(output) as output from line_summary_po where style = '$s->style' and date = '$date'
					and line_id = '$line' and activity_name = 'outputqc' and jam_awal >= '$awal'")->row_array();
					
					$qc_jam = $qc_jam + $q1['output'];

					
					
					$qc_min_1 = $this->db->query("SELECT sum(output) as output from line_summary_po where 
					date = '$date' and style = '$s->style' and line_id = '$line' and activity_name = 'outputqc' 
					and jam_akhir <= '$awal'")->row_array();
				
					$output_qc_min_1 = $output_qc_min_1 + $qc_min_1['output'];

					$sewing_min_1 = $this->db->query("SELECT sum(output) as output from line_summary_po where 
					date = '$date' and style = '$s->style' and line_id = '$line' and activity_name = 'sewing' 
					and jam_akhir <= '$awal'")->row_array();

					$output_sewing_min_1 = $output_sewing_min_1 + $sewing_min_1['output'];
					
			
					if($n_style > (($banyak_style-1)/2)){
						if($potong==0){
							$potong=1;
							$style .= '</ul></td><td><ul style="list-style-type: none; margin: 0;
							padding: 0;">';
						}
					}	
					$qty_order = (int)$qty_order + (int)$s->order_qty;
					$style    .= "<li>".$s->style."</li>";
					$smv      .= '<li style="display: inline;">'.$s->gsd_smv.'</li> &nbsp &nbsp';
					$cum_day   = $s->cumulative_day;
					$operator  = $s->present_sewer+$s->present_folding;
					$jamkrj    = $s->working_hours;
					

					//wft
					$inline   = $this->DashboardModel->defect_count($date, $line, $s->style, 'defect_inline');
					$endline  = $this->DashboardModel->defect_count($date, $line, $s->style, 'defect_endline');
					$outputqc = $this->DashboardModel->output_count($date, $line, $s->style, 'outputqc');

					$sewing_output = $this->db->query("SELECT * from line_summary_po where 
					date = '$date' and style = '$s->style' and line_id = '$line' and activity_name = 'sewing'")->result();

					if($sewing_output==null){
						$total_sewing = $total_sewing;
					}
					else{
						foreach($sewing_output as $so){
							$total_sewing = (int)$total_sewing + (int)$so->output;
						}
					}
					if($jam_ke > $jamkrj){
						$jam_ke = $jamkrj;
					}
					$total_output_qc = $total_output_qc + $outputqc->total;
					$total_inline    = $total_inline + $inline->total;
					$total_endline   = $total_endline + $endline->total;
					$total_target    = $total_target + $s->target_output;

					// $wft = $wft+$nilai_wft;
					$n_style++;	
				}

			}
			else{
				// $list_style = $jalan_style->row();

				// foreach ($list_style as $s) {
				// 	$style .= "".$s->style.",";
				// }
			}
			if ($total_output_qc>0) {
				$total_wft = 100*(($total_inline+$total_endline)/$total_output_qc);
			}else{
				$total_wft =0;
			}

			$query_dist = $this->db->query("SELECT DISTINCT poreference 
			from distribusi where line_id = '$line'")->result();

			$list_po = '';
			foreach ($query_dist as $qd) {
				$list_po .= "'".$qd->poreference."',";
			}
			$listpo = rtrim($list_po, ',');
	
			$query_qc = $this->db->query("SELECT DISTINCT qc_output.poreference from qc_output 
			left join inline_header on inline_header.inline_header_id = qc_output.inline_header_id
			where 
			in_active_qc_date is null and
			qc_output.line_id = '$line' and qc_output.qc_output < qc_output.qty_order and qc_output.poreference in (".$listpo.")")->result();

			$list_po2 = '';
			foreach ($query_qc as $qc) {
				$list_po2 .= "'".$qc->poreference."',";
			}
			$listpo2 = rtrim($list_po2, ',');
		

			$query_pobuyer = $this->db->query("SELECT pb.* from po_buyers pb 
			LEFT JOIN(SELECT min(crd) as min from po_buyers where poreference in (".$listpo2.") and crd > '".date('Y-m-d')."') as x on x.min = pb.crd
			where pb.crd = x.min and pb.poreference in (".$listpo2.") ORDER BY crd ASC")->result_array();
			// poreference in (".$listpo2.") ORDER BY crd ASC

			if(sizeof($query_pobuyer) < 1){
				$query_pobuyer = $this->db->query("SELECT pb.* from po_buyers pb 
				LEFT JOIN(SELECT min(crd) as min from po_buyers where poreference in (".$listpo2.")) as x on x.min = pb.crd
				where pb.crd = x.min and pb.poreference in (".$listpo2.") ORDER BY crd ASC")->result_array();
			}

			// var_dump($listpo);
			// var_dump($listpo2);
			// echo "<pre>";
			// print_r($query_pobuyer);
			// echo "</pre>";
			// die();
			$tanggal = date('d-m-Y',strtotime($query_pobuyer[0]['crd']));
			$pobuyer_text = '<b style="color: white"> DELIVERY DATE : '.$tanggal.'</b>';
			foreach ($query_pobuyer as $qp) {
				if ($qp['recycle']=='t') {
					$recy = ' <i class="fa fa-recycle" style="color: white;"></i>';
				}else{
					$recy = null;
				}

				$pobuyer_text .= '<b style="color: white"> | </b>';
				$pobuyer_text .= '<b style="color: white">PO : '.$qp['poreference'].'</b> '.$recy;
				// $pobuyer_text .= '<b style="color: white"> | </b>';
			}
			// $po_buyer = $query_pobuyer['poreference'];
			// $po_date  = $query_pobuyer['kst_statisticaldate'];
			

			// if($n_style == 0){
			// 	$total_wft = 0;
			// }
			// else{
			// 	$total_wft = $wft/$n_style;
			// }
			
			if ($total_wft>3) {
				$bg_actual_wft = '#F00;color:#EAEAEA';
				$bg_text       = '#ffffff';
			}else{
				$bg_actual_wft = '#0F0';
				$bg_text       = '#000000';
			}

			$draw_actual_wft = "<b style='background:".$bg_actual_wft."; color:".$bg_text."'>".number_format($total_wft,1,',',',')."</b>";

			// $output_aktual = $this->db->query("SELECT sum(output) as total from line_summary_po 
			// where style = '$jalan_style->style' and line_id = '$line' and date = '$date' 
			// and activity_name = 'outputqc'")->row();


			// $sum_output_aktual = (int)$output_aktual->total;

			$eff = $this->list_eff($line,$factory);

			$tgl = date('Y-m-d');
			$last_update = $this->db->query("SELECT max(create_date) from line_summary_po 
			where line_id = $line and factory_id = '$factory'
			and date = '$tgl'")->row_array()['max'];

			// var_dump($last_update);
			// die();

			// $sewing_output = $this->db->query("SELECT * from line_summary_po where 
			// date(create_date) = '$tgl' and line_id = '$line' and activity_name = 'sewing'")->result();

			// $total_sewing=0;
			// if($sewing_output==null){
			// 	$total_sewing = 0;
			// }
			// else{
			// 	foreach($sewing_output as $so){
			// 		$total_sewing = (int)$total_sewing + (int)$so->output;
			// 	}
			// }
			
			if($jamkrj==0){
				$target_jam = 0;
			}else{
				$target_jam = (int)$total_target/(float)$jamkrj;
			}
			
			// var_dump($jam_ke );
			// var_dump($jam_kerja);
			if($jam_ke == $jam_kerja){
				$jam_kanan = $jam_kerja+1;
			}else{
				$jam_kanan = $jam_ke;
			}

			$jam_min_1 = (int)$jam_kanan - (int)1;
			// die();
			$style 	.= '</ul></td></tr></table>';
			// rtrim($smv, '<td> ');
			$smv 	.= '</ul></td></tr></table>';

			$target_akumulatif = (float)$target_jam * $jam_min_1;

			
			$batas_mulai = strtotime($jam_kerja_mulai) + 60*60;

			$batas_jam_mulai = date('H:i:s', $batas_mulai);

			if($jam_sekarang < $batas_jam_mulai){
				$jam_pertama         = '';
				$jam_terakhir        = '';
				$output_qc_min_1     = '-';
				$output_sewing_min_1 = '-';
				$target_akumulatif   = '-';
			}
			else{
				$target_akumulatif = round($target_akumulatif);
			}
			// var_dump($jam_min_1);
			// var_dump($target_jam);
			// var_dump($target_akumulatif);
			// die();
			$data = array(
				'line_name'           => $line_name,
				'style'               => $style,
				//rtrim($style, ', '),
				'smv'                 => rtrim($smv, ', '),
				'cum_day'             => rtrim($cum_day, ', '),
				'operator'            => rtrim($operator, ', '),
				'wft'                 => $draw_actual_wft,
				'today_target_harian' => $total_target,                                 //target commitment
				'today_pencapaian'    => $total_output_qc,                              //aktual output qc
				'target_eff'          => $eff['text_berjalan1'],                        //qty order
				'actual_eff'          => $eff['text_berjalan2'],
				'last_update'         => date('Y-m-d H:i:s',strtotime($last_update)),
				'sewing_output'       => $total_sewing,
				'sewing_jam'          => $sewing_jam,
				'qc_jam'              => $qc_jam,
				'target_jam'          => round($target_jam),
				'target_akumulatif'   => $target_akumulatif,
				'pukul'               => $jam_nama,
				'pobuyer_text'        => $pobuyer_text,
				'n_jam'               => $jam_ke,
				'qc_min_1'            => $output_qc_min_1,
				'sewing_min_1'        => $output_sewing_min_1,
				'jam_pertama'         => $jam_pertama,
				'jam_terakhir'        => $jam_terakhir,
				// 'qty_order'           => $qty_order,                                    //qty order
				// 'po_date'			  => $po_date,
				// 'cek_qc'			  => $cek_qc


			);
			$this->load->view('display/line_dashboard',$data);
		}
		else{
			$factory_name = $this->db->get_where('master_factory', array('factory_id'=>$factory))->row_array()['name'];
			$line_name    = $this->db->get_where('master_line', array('master_line_id'=>$line))->row_array()['line_name'];
			$data         = array('factory_name' => $factory_name,'line_name'=>$line_name );
			$this->load->view('display/before',$data);
		}
		// $dailystatus = $this->DisplayModel->getStyleDetail($qc_status, $line, $factory)->row_array();

		
		// if ($dailystatus!=NULL&&$dailystatus['job_status']!='f') {
		// 	$line_name      = $this->db->get_where('master_line', array('master_line_id'=>$line))->row_array()['line_name'];
		// 	$style          = $this->DisplayModel->qcStatus($line,$factory)['style'];
		// 	// $getStyleDetail = $this->DailyModel->getStyleDetail($qc_status, $line, $factory)->row_array();
		// 	$getStyleDetail = $dailystatus;
			
		// 	$cekline        = $this->db->get_where ('master_line', array('master_line_id'=>$line))->row_array()['reqular_line'];
		// 	$line_category  = ($cekline=='t'?'RL':'SC');
		// 	$daily          = $this->db->get_where('daily_header', array('line_id'=>$line,'style'=>$style,'job_status'=>'t'))->row_array();
			
		// 	// $cumday         = ($daily['cumulative_day']!=0?$daily['cumulative_day']:1);
		// 	if ($daily['cumulative_day']!=0&&(int)$daily['cumulative_day']<12) {
		// 		$cumday =$daily['cumulative_day'];
		// 	}elseif ((int)$daily['cumulative_day']>=12) {
		// 		$cumday =12;
		// 	}else{
		// 		$cumday =1;
		// 	}

		// 	if ($daily['change_over']=='REPEAT') {
		// 		$this->db->where('line_category', $line_category);
		// 		$this->db->where('category',$daily['change_over']);
		// 		$co           = $this->db->get('master_co')->row_array();
		// 	}else{
		// 		$this->db->where('line_category', $line_category);
		// 		$this->db->where('day',$cumday);
		// 		$this->db->where('category',$daily['change_over']);
		// 		$co           = $this->db->get('master_co')->row_array();
		// 	}
			
		// 	/*if ($change_over['eff']!=NULL) {
		// 		$co = $change_over;
		// 	}else{

		// 		$co = 
		// 	}*/

		// 	$target_hour  = $getStyleDetail['present_sewer']*(60/$getStyleDetail['gsd_smv'])*($co['eff']/100);
		// 	// /var_dump (round($target_hour,0, PHP_ROUND_HALF_UP));
			
		// 	$jam_kerja    = $this->jam_kerja();
			
		// 	$jam          = $jam_kerja['jam'];
		// 	$time         = $jam_kerja['time'];
		// 	$waktu        = $jam_kerja['waktu'];
			
		// 	$sumSewing    = $this->sumSewing($jam,$style,$line,$factory);
		// 	$sumQcEndline = $this->sum_qc_endline($jam,$style,$line,$factory,$getStyleDetail,$time,$waktu,$co);
		// 	$sumfolding   = $this->sum_folding($jam,$style,$line,$factory);
		// 	$last_update = $this->DisplayModel->LastUpdate($line,$style);
					
		// 	$data = array(
		// 		'line_name'        => $line_name,
		// 		'style'        => $style,
		// 		'detail'       =>$getStyleDetail,
		// 		'jam'          =>$jam,
		// 		'time'         =>$time,
		// 		'sumSewing'    =>$sumSewing,
		// 		'last_update'    =>$last_update,
		// 		'target_hour'  =>ceil($target_hour),
		// 		'sumqcendline' =>$sumQcEndline['draw_qc'],
		// 		'sumfolding'   =>$sumfolding,
		// 		'target'       =>$sumQcEndline['draw_target'], 
		// 		'actual'       =>$sumQcEndline['draw_actual'],
		// 		'sum_inline'   =>$sumQcEndline['draw_inline'], 
		// 		'sum_endline'  =>$sumQcEndline['draw_endline'], 
		// 		'wft_target'   =>$sumQcEndline['draw_target_wft'], 
		// 		'wft_actual'   =>$sumQcEndline['draw_actual_wft'] 
		// 	);
		// 	$this->load->view('display/operator_performance_view',$data);
		// }else{
		// 	$factory_name = $this->db->get_where('master_factory', array('factory_id'=>$factory))->row_array()['name'];
		// 	$line_name    = $this->db->get_where('master_line', array('master_line_id'=>$line))->row_array()['line_name'];
		// 	$data         = array('factory_name' => $factory_name,'line_name'=>$line_name );
		// 	$this->load->view('display/before',$data);
		// }
		
	}

	private function jam_kerja($factory)
	{
		$waktu = 0;
		if($factory == '1'){
			$jam[0] = array(
				'name' => '07:00 - 08:00',
				'awal' => '07:00:00',
				'akhir' => '08:00:00'
			);
			$jam[1] = array(
				'name' => '08:00 - 09:00',
				'awal' => '08:00:00',
				'akhir' => '09:00:00'
			);
			$jam[2] = array(
				'name' => '09:00 - 10:00',
				'awal' => '09:00:00',
				'akhir' => '10:00:00'
			);
			$jam[3] = array(
				'name' => '10:00 - 11:00',
				'awal' => '10:00:00',
				'akhir' => '11:00:00'
			);
			$jam[4] = array(
				'name' => '11:00-12:00',
				'awal' => '11:00:00',
				'akhir' => '12:00:00'
			);
			$jam[5] = array(
				'name' => '12:00-13:45',
				'awal' => '12:00:00',
				'akhir' => '13:45:00'
			);
	
			if (date("H:i:s") > $jam[5]['akhir']) {
				$jam[] = array(
					'name' => '13:45-14:45',
					'awal' => '13:45:00',
					'akhir' => '14:45:00'
				);
	
				$waktu = 3;
				unset($jam[1]);
				unset($jam[2]);
				$jam[0] = array(
					'name' => '07-10',
					'awal' => '07:00:00',
					'akhir' => '10:00:00'
				);
				if (date("H:i:s") > $jam[6]['akhir']) {
					$jam[] = array(
						'name' => '14:45-15:45',
						'awal' => '14:45:00',
						'akhir' => '15:45:00'
					);
					$waktu = 4;
					unset($jam[3]);
					$jam[0] = array(
						'name' => '07-11',
						'awal' => '07:00:00',
						'akhir' => '11:00:00'
					);
					if (date("H:i:s") > $jam[7]['akhir']) {
						$jam[] = array(
							'name' => '16:00-17:00',
							'awal' => '15:45:00',
							'akhir' => '17:00:00'
						);
						$waktu = 5;
						unset($jam[4]);
						$jam[0] = array(
							'name' => '07-12',
							'awal' => '07:00:00',
							'akhir' => '12:00:00'
						);
				
						if (date("H:i:s") > $jam[8]['akhir']) {
							$jam[] = array(
								'name' => '17:00-18:00',
								'awal' => '17:00:00',
								'akhir' => '18:00:00'
							);
							$waktu = 6;
							unset($jam[5]);
							$jam[0] = array(
								'name' => '07-13.45',
								'awal' => '07:00:00',
								'akhir' => '13:45:00'
							);
							if (date("H:i:s") > $jam[9]['akhir']) {
								$jam[] = array(
									'name' => '18:30-19:15',
									'awal' => '18:00:00',
									'akhir' => '19:30:00'
								);
				
								if (date("H:i:s") > $jam[10]['akhir']) {
									$jam[] = array(
										'name' => '19:30-20:30',
										'awal' => '19:30:00',
										'akhir' => '20:30:00'
									);
								}
							}
						}
					}
				}
			}
	
			$jam[] = array(
				'name' => 'TOTAL',
				'awal' => '06:00:00',
				'akhir' => '23:30:00'
			);
				$timess = max(array_keys($jam));
				return array('jam'=>$jam,'time'=>$timess,'waktu'=>$waktu);
			 
		} else if($factory == '2'){
			// $jam[0] = array(
			// 	'name' => '07:30 - 08:30',
			// 	'awal' => '07:30:00',
			// 	'akhir' => '08:30:00'
			// );
			// $jam[1] = array(
			// 	'name' => '08:30 - 09:30',
			// 	'awal' => '08:30:00',
			// 	'akhir' => '09:30:00'
			// );
			// $jam[2] = array(
			// 	'name' => '09:30 - 10:30',
			// 	'awal' => '09:30:00',
			// 	'akhir' => '10:30:00'
			// );
			// $jam[3] = array(
			// 	'name' => '10:30 - 11:30',
			// 	'awal' => '10:30:00',
			// 	'akhir' => '11:30:00'
			// );
			// $jam[4] = array(
			// 	'name' => '13:00 - 14:00',
			// 	'awal' => '11:30:00',
			// 	'akhir' => '14:00:00'
			// );
			// $jam[5] = array(
			// 	'name' => '14:00-15:00',
			// 	'awal' => '14:00:00',
			// 	'akhir' => '15:00:00'
			// );
	
			// if (date("H:i:s") > $jam[5]['akhir']) {
			// 	$jam[] = array(
			// 		'name' => '15:00-16:00',
			// 		'awal' => '15:00:00',
			// 		'akhir' => '16:00:00'
			// 	);
	
			// 	$waktu = 3;
			// 	unset($jam[1]);
			// 	unset($jam[2]);
			// 	$jam[0] = array(
			// 		'name' => '07-10',
			// 		'awal' => '07:30:00',
			// 		'akhir' => '10:30:00'
			// 	);
			// 	if (date("H:i:s") > $jam[6]['akhir']) {
			// 		$jam[] = array(
			// 			'name' => '16:00 - 16:15',
			// 			'awal' => '16:00:00',
			// 			'akhir' => '16:15:00'
			// 		);
			// 		$waktu = 4;
			// 		unset($jam[3]);
			// 		$jam[0] = array(
			// 			'name' => '07-11',
			// 			'awal' => '07:30:00',
			// 			'akhir' => '11:30:00'
			// 		);
			// 		if (date("H:i:s") > $jam[7]['akhir']) {
			// 			$jam[] = array(
			// 				'name' => '16:00-16:15',
			// 				'awal' => '16:00:00',
			// 				'akhir' => '16:15:00'
			// 			);
			// 			$waktu = 5;
			// 			unset($jam[4]);
			// 			$jam[0] = array(
			// 				'name' => '07-12',
			// 				'awal' => '07:30:00',
			// 				'akhir' => '12:30:00'
			// 			);
				
			// 			if (date("H:i:s") > $jam[8]['akhir']) {
			// 				$jam[] = array(
			// 					'name' => '16:30-17:30',
			// 					'awal' => '16:15:00',
			// 					'akhir' => '17:30:00'
			// 				);
			// 				$waktu = 6;
			// 				unset($jam[5]);
			// 				$jam[0] = array(
			// 					'name' => '07-13.45',
			// 					'awal' => '07:30:00',
			// 					'akhir' => '14:15:00'
			// 				);
			// 				if (date("H:i:s") > $jam[9]['akhir']) {
			// 					$jam[] = array(
			// 						'name' => '17:30-18:30',
			// 						'awal' => '17:30:00',
			// 						'akhir' => '18:30:00'
			// 					);
				
			// 					if (date("H:i:s") > $jam[10]['akhir']) {
			// 						$jam[] = array(
			// 							'name' => '19:00-20:00',
			// 							'awal' => '18:30:00',
			// 							'akhir' => '20:00:00'
			// 						);
			// 					}
			// 				}
			// 			}
			// 		}
			// 	}
			// }
	
			// $jam[] = array(
			// 	'name' => 'TOTAL',
			// 	'awal' => '06:00:00',
			// 	'akhir' => '23:30:00'
			// );
			// 	$timess = max(array_keys($jam));
			// 	return array('jam'=>$jam,'time'=>$timess,'waktu'=>$waktu);
			$jam[0] = array(
				'name' => '07:00 - 08:00',
				'awal' => '07:00:00',
				'akhir' => '08:00:00'
			);
			$jam[1] = array(
				'name' => '08:00 - 09:00',
				'awal' => '08:00:00',
				'akhir' => '09:00:00'
			);
			$jam[2] = array(
				'name' => '09:00 - 10:00',
				'awal' => '09:00:00',
				'akhir' => '10:00:00'
			);
			$jam[3] = array(
				'name' => '10:00 - 11:00',
				'awal' => '10:00:00',
				'akhir' => '11:00:00'
			);
			$jam[4] = array(
				'name' => '11:00-12:00',
				'awal' => '11:00:00',
				'akhir' => '12:00:00'
			);
			$jam[5] = array(
				'name' => '12:00-13:45',
				'awal' => '12:00:00',
				'akhir' => '13:45:00'
			);
	
			if (date("H:i:s") > $jam[5]['akhir']) {
				$jam[] = array(
					'name' => '13:45-14:45',
					'awal' => '13:45:00',
					'akhir' => '14:45:00'
				);
	
				$waktu = 3;
				unset($jam[1]);
				unset($jam[2]);
				$jam[0] = array(
					'name' => '07-10',
					'awal' => '07:00:00',
					'akhir' => '10:00:00'
				);
				if (date("H:i:s") > $jam[6]['akhir']) {
					$jam[] = array(
						'name' => '14:45-15:45',
						'awal' => '14:45:00',
						'akhir' => '15:45:00'
					);
					$waktu = 4;
					unset($jam[3]);
					$jam[0] = array(
						'name' => '07-11',
						'awal' => '07:00:00',
						'akhir' => '11:00:00'
					);
					if (date("H:i:s") > $jam[7]['akhir']) {
						$jam[] = array(
							'name' => '16:00-17:00',
							'awal' => '15:45:00',
							'akhir' => '17:00:00'
						);
						$waktu = 5;
						unset($jam[4]);
						$jam[0] = array(
							'name' => '07-12',
							'awal' => '07:00:00',
							'akhir' => '12:00:00'
						);
				
						if (date("H:i:s") > $jam[8]['akhir']) {
							$jam[] = array(
								'name' => '17:00-18:00',
								'awal' => '17:00:00',
								'akhir' => '18:00:00'
							);
							$waktu = 6;
							unset($jam[5]);
							$jam[0] = array(
								'name' => '07-13.45',
								'awal' => '07:00:00',
								'akhir' => '13:45:00'
							);
							if (date("H:i:s") > $jam[9]['akhir']) {
								$jam[] = array(
									'name' => '18:30-19:15',
									'awal' => '18:00:00',
									'akhir' => '19:30:00'
								);
				
								if (date("H:i:s") > $jam[10]['akhir']) {
									$jam[] = array(
										'name' => '19:30-20:30',
										'awal' => '19:30:00',
										'akhir' => '20:30:00'
									);
								}
							}
						}
					}
				}
			}
	
			$jam[] = array(
				'name' => 'TOTAL',
				'awal' => '06:00:00',
				'akhir' => '23:30:00'
			);
				$timess = max(array_keys($jam));
				return array('jam'=>$jam,'time'=>$timess,'waktu'=>$waktu);
			 

		}  else{
			
			 
		}

	}
	private function sumSewing($jam,$style,$line,$factory)
	{
		$where = array(
			'line_id'       =>$line,
			'style'         =>$style,
			'activity_name' =>'sewing',
			'factory_id'    =>$factory 
		);
		$draw='';
		foreach ($jam as $key => $jam) {
			$awal  = date("Y-m-d").' '.$jam['awal'];
			$akhir = date("Y-m-d").' '.$jam['akhir'];

			
			$now = date("Y-m-d H:i:s");

    		if(strtotime($now) > strtotime($awal) && strtotime($now) < strtotime($akhir)){
    			$bg = "#CCC";
    		}else{
    			$bg = "#FFF";
    		}

			
			$this->db->where($where);
			$this->db->where('jam_awal >=',$awal);
    		$this->db->where('jam_akhir <=',$akhir);
    		$this->db->select('sum(output) as output');
    		$get = $this->db->get('line_summary_po')->row_array()['output'];
    		
    		$sum= ($get!=NULL?$get:0);
    		$draw .="<td class='count' style='background:".$bg."'>".$sum."</td>";
    			
		}
		return $draw;
		
	}
	private function sum_qc_endline($jam,$style,$line,$factory,$getStyleDetail,$time,$waktu,$co)
	{
		$where = array(
			'line_id'    =>$line,
			'style'      =>$style,
			'factory_id' =>$factory 
		);
		$draw_qc         ='';
		$draw_target     ='';
		$draw_actual     ='';
		$draw_endline    ='';
		$draw_inline     ='';
		$draw_target_wft ='';
		$draw_actual_wft ='';
		$x               =1;
		foreach ($jam as $key => $jam) {
			$awal  = date("Y-m-d").' '.$jam['awal'];
			$akhir = date("Y-m-d").' '.$jam['akhir'];
			
				$now   = date("Y-m-d H:i:s");
				
				$jam_awal = strtotime(date('Y-m-d 07:00:00'));
				$jam_istirahat = date('Y-m-d 11:45:00');
				
				if ($time>$jam_istirahat) {
					$result =abs((strtotime($now)-$jam_awal)-2700)/3600;
				}else{
					$result = abs(strtotime($now)-$jam_awal)/3600;
				}
				$jam_berjalan = $result;

    		if(strtotime($now) > strtotime($awal) && strtotime($now) < strtotime($akhir)){
    			$bg = "#CCC";
    		}else{
    			$bg = "#FFF";
    		}
    		/*JUMLAH GOOD ENDLINE*/
			$sum_endline     = $this->DisplayModel->sum_qcendline($awal,$akhir,$style,$line,$factory);
			$sum_goodendline =($sum_endline!=NULL?$sum_endline:0);

    		if ($sum_goodendline == 0 && strtotime(date("Y-m-d H:i:s")) < strtotime($awal)) {
    			$draw_actual .="<td class='count' style='background:#fff;'></td>";
    		}else{
    			
    			
    			if($time > 6 && $key == 0){
					$eff  = 100*($getStyleDetail['gsd_smv']*$sum_goodendline)/($waktu*60*($getStyleDetail['present_sewer']));
				}
				else if($key < $time){
					$eff  = 100*($getStyleDetail['gsd_smv']*$sum_goodendline)/(60*($getStyleDetail['present_sewer']));
				}else{
					$eff  = 100*(($getStyleDetail['gsd_smv']*$sum_goodendline))/(floatval($jam_berjalan)*60*$getStyleDetail['present_sewer']);
				}
				
				$effw = 100*(floatval($eff)/$co['eff']);

				if($eff < $co['eff']){
					$bg_actual = '#F00;color:#EAEAEA';
				}else{
					$bg_actual = '#0F0'; 

				}
				$draw_actual .="<td class='count' style='background:".$bg_actual."'>". number_format($eff,1,',',',') ."</td>";
				$x++;
    		}

			$defect_endline     = $this->DisplayModel->sum_defect_endline($awal,$akhir,$style,$line,$factory);
			$sum_defect_endline =($defect_endline!=NULL?$defect_endline:0);
			
			$defect_inline      = $this->DisplayModel->sum_defect_inline($awal,$akhir,$style,$line,$factory);
			$sum_defect_inline  =($defect_inline!=NULL?$defect_inline:0);

    		if(($sum_defect_inline+$sum_defect_endline) == 0 || $sum_goodendline == 0){
				if($now > $awal){
					$draw_actual_wft.= "<td class='count' style='background:".$bg."'>0</td>";
				}else{
					$draw_actual_wft.= "<td class='count' style='background:".$bg."'></td>";
				}
			
			}else{
	    		$wft = 100*(($sum_defect_inline+$sum_defect_endline)/$sum_goodendline);
	    		
	    		if ($wft>3) {
	    			$bg_actual_wft = '#F00;color:#EAEAEA';
	    		}else{
	    			$bg_actual_wft = '#0F0';
	    		}

				$draw_actual_wft.= "<td class='count' style='background:".$bg_actual_wft."'>".number_format($wft,1,',',',')."</td>";
			}
			if($awal < $now){
				$draw_target_wft.= '<td class="count" style="background:'.$bg.'">'.$getStyleDetail['target_wft'].'</td>';
    		}else{
    			$draw_target_wft.= "<td style='background:#fff'></td>";
		    }

			$draw_endline .="<td class='count' style='background:".$bg."'>".$sum_defect_endline."</td>";			
			$draw_inline  .="<td class='count' style='background:".$bg."'>".$sum_defect_inline."</td>";			
			
			$draw_qc      .="<td class='count' style='background:".$bg."'>".$sum_goodendline."</td>";
			$draw_target  .='<td class="count" style="background:'.$bg.'">'.$co['eff'].'</td>';
			
		}
		$data = array(
			'draw_qc'         => $draw_qc, 
			'draw_actual'     => $draw_actual, 
			'draw_target'     => $draw_target, 
			'draw_endline'    => $draw_endline, 
			'draw_inline'     => $draw_inline, 
			'draw_target_wft' => $draw_target_wft, 
			'draw_actual_wft' => $draw_actual_wft 
		);
		return $data;
	}
	private function sum_folding($jam,$style,$line,$factory)
	{
		$where = array(
			'line_id'    =>$line,
			'style'      =>$style,
			'factory_id' =>$factory 
		);
		$draw='';
		foreach ($jam as $key => $jam) {
			$awal  = date("Y-m-d").' '.$jam['awal'];
			$akhir = date("Y-m-d").' '.$jam['akhir'];
			
			
			$now   = date("Y-m-d H:i:s");

    		if(strtotime($now) > strtotime($awal) && strtotime($now) < strtotime($akhir)){
    			$bg = "#CCC";
    		}else{
    			$bg = "#FFF";
    		}    		
			
			$get  = $this->DisplayModel->sum_folding($awal,$akhir,$style,$line,$factory);

			$sum= ($get!=NULL?$get:0);
			
			$draw .="<td class='count' style='background:".$bg."'>".$sum."</td>";			
		}
		return $draw;
	}
	function adibooster($showlinefrom,$showlineto){
			$this->db->where('master_line_id >=',$showlinefrom); 
			$this->db->where('master_line_id <=',$showlineto);
			$this->db->order_by('master_line_id','ASC');
		$lines = $this->db->get('master_line');
		foreach ($lines->result() as $key => $line) {
			$this->db->where('line_id',$line->master_line_id);
			$style = $this->db->get('status_qc_endline');
			if ($style->num_rows()!=0) {
				
			}

		}
		$this->load->view('display/adibooster',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto));
	}
	function adibooster_graph($showlinefrom,$showlineto){
		if(date('s') < 10) 
			$this->load->view('display/adibooster_graph',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto));
		else if(date('s') < 20)
			$this->load->view('display/adibooster_graph_wft',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto));
		else if(date('s') < 30)
			$this->load->view('display/adibooster_graph',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto));
		else if(date('s') < 40)
			$this->load->view('display/adibooster_graph_wft',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto));
		else if(date('s') < 50)
			$this->load->view('display/adibooster_graph',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto));
		else if(date('s') < 60)
			$this->load->view('display/adibooster_graph_wft',array('showlinefrom'=>$showlinefrom,'showlineto'=>$showlineto));
	}	

	public function list_eff($line,$factory)
	{
		//  $date = date('2020-03-27');
		//  2020-11-23 14:15:20
		$date = date('Y-m-d');

		$dashe = $this->DisplayModel->eff_daily($date,$factory,$line);

		$query_jam_kerja = $this->db->query("SELECT jam_mulai, created_by from daily_jam_kerja 
		where date(create_date) = '$date' and line_id ='$line' and factory_id = '$factory' ORDER BY create_date desc")->row_array();

		if($query_jam_kerja == null){		
			$jam_kerja_mulai = '07:00:00';
		}else{		
			$jam_kerja_mulai = $query_jam_kerja['jam_mulai'];
		}
			
		// var_dump($dashe->result());
		// var_dump($dashe->num_rows());
		// var_dump($dashe->result());
		// die();
		$dashboard = $dashe->result();
		// $dashboard = $dashe->row();
		// if($dashe->num_rows()>1){
		// 	$dashboard = $dashe->result();
		// }
		// else{
		// 	$dashboard = $dashe->row();
		// }
		// $banyak = $dashe->num_rows();
		// $batas  = $banyak/2;
		$no     = 1;
		$nilai  = 0;
		$goal   = 0;
		$text_berjalan = '';
		// $line   = '';
		$lineid = '';
		$sum_output_aktual1 = null;
		$sum_output_aktual2 = null;
        if(!empty($dashboard))
        {
			// $i          = 0;
			$output1    = 0;
			$smv1       = 0;
			// $targeteff1 = 0;
			$jamkerja1  = 0;
			// foreach ($dash as $dash)
			foreach ($dashboard as $index => $dash)
            {
				$query = $this->db->query("SELECT count(line_id) as total,line_id,line_name 
				from (SELECT line_id, line_name,style from daily_view where date(create_date) = '$date' 
				and factory_id = $factory and job_status = 't' and line_id = $dash->line_id GROUP BY line_id,line_name,style 
				ORDER BY line_id) as daily
				GROUP BY line_id,line_name")->result();

				// $query = $this->db->query("SELECT 
				// -- count(line_id) as total,line_id from 
				// -- (SELECT line_id, line_name,style from daily_view where date(create_date) = 
				// -- '$date' and factory_id = $factory GROUP BY line_id,line_name,style ORDER BY 
				// -- line_id) as daily GROUP BY line_id
				// count(line_id) as total, line_id, line_name
				// from daily_view where date(create_date) = '$date' and factory_id = '$factory' 
				// and line_id = $dash->line_id
				// GROUP BY line_id,line_name ORDER BY line_id
				// ")->result();

				// $cek_shift = explode(":",$jam_kerja_mulai);
				// if($cek_shift[1] == '30'){
				// 	$shift='2';
				// }
				// else{
				// 	$shift='1';
				// }
				// var_dump($shift);
				// die();

				$time = $this->jam_kerja($factory)['time'];
						//Efficiency
				$now   = date("Y-m-d H:i:s");
				
				if($date!=date("Y-m-d")){
					$jam_berjalan = 8;
				}
				else{
					$jam_awal = strtotime(date('Y-m-d').' '.$jam_kerja_mulai);

					if($jam_kerja_mulai == '09:00:00'){
						$jam_istirahat = date('Y-m-d 13:15:00');
					}
					else{
						$jam_istirahat = date('Y-m-d 11:45:00');
					}
					//now ganti jam akhir
					if ($now>$jam_istirahat) {
						$result = abs((strtotime($now)-$jam_awal)-2700)/3600;
					}else{
						$result = abs(strtotime($now)-$jam_awal)/3600;
					}
					$jam_berjalan = $result;

					// var_dump($jam_berjalan);
					// die();
				}

				// var_dump(($date!=date("Y-m-d")));
				// var_dump($now);
				// var_dump($jam_istirahat);
				// var_dump(($now>$jam_istirahat));
				// var_dump($jam_berjalan);
				// var_dump(($date!=date("Y-m-d")));
				$cek=0;
				foreach($query as $q){
					// if($q->total>1){
						// $cek=1;
						$cek = $this->db->query("SELECT * from 
						daily_view where date(create_date) = '$date' and line_id = '$q->line_id' and job_status = 't'
						 and factory_id = '$factory' order by create_date");

						$array_style  = array();
						$array_target = array();
						$array_smv    = array();
						$array_jam    = 0;
						$array_worker = 0;
						$array_output = array();
						$outputqc     = 0;


						foreach ($cek->result() as $c) {
							array_push($array_style,$c->style);
							array_push($array_target,$c->target_output);
							array_push($array_smv,$c->gsd_smv);
							$array_jam = $c->working_hours;
							$array_worker = (int)$c->present_sewer + (int)$c->present_folding;

							$cek2 = $this->db->query("SELECT sum(output) as total from line_summary_po 
							where style = '$c->style' and line_id = '$q->line_id' and date = '$date'
							and activity_name = 'outputqc'")->row_array();
							
							$output = (int)$cek2['total'];
							$outputqc = $outputqc+$output;

							array_push($array_output,$output);
						}
						
						$target = 0;
						$actual = 0;
						foreach ($array_target as $key => $at) {
							$target = $target + ($at*(float)$array_smv[$key]);
							$actual = $actual + ($array_output[$key]*(float)$array_smv[$key]);
						}

						// target eff = [ ( target_komitmen * smv ) / ( manpower * jamkerja * 60 ) ] * 100
						// actual eff
						// jam kerja = diganti = jam berjalan
						// target komitmen =diganti = output sampe sekarang
						

						if($array_worker == 0 || $array_jam == 0){
							$actual_eff = 0;
							$target_eff = 0;
						}
						else{
							$target_eff = (($target)/($array_worker*$array_jam*60))*100;
							if($jam_berjalan >= $array_jam){
								$actual_eff = (($actual)/((int)$array_worker*(float)$array_jam*60))*100;
							}else{
								$actual_eff = (($actual)/((int)$array_worker*(float)$jam_berjalan*60))*100;
							}	
						}

						// var_dump("actual:");
						// var_dump($actual);
						// var_dump("<br>target:");
						// var_dump($target);
						// var_dump("<br>jamberjalan:");
						// var_dump($jam_berjalan);
						// var_dump("<br>");
						// var_dump("style:");
						// var_dump($array_style);
						// var_dump("<br>target:");
						// var_dump($array_target);
						// var_dump("<br>smv:");
						// var_dump($array_smv);
						// var_dump("<br>jam:");
						// var_dump($array_jam);
						// var_dump("<br>worker:");
						// var_dump($array_worker);
						// var_dump("<br>ouput:");
						// var_dump($array_output);
						// var_dump("<br>qc:");
						// var_dump($outputqc);
						// die();
						
						// $query2 = $cek->result();
						// $cek2 = $cek->num_rows();
						// if($cek2 > 2){
						// 	$style1 = $query2[0]->style;
						// 	$style2 = $query2[1]->style;
						// 	$style3 = $query2[2]->style;

						// 	$daily1 = $this->db->query("SELECT * from daily_view where style = '$style1' 
						// 	and line_id = '$q->line_id' and job_status = 't' and date(create_date) = '$date'")->row();

						// 	$daily2 = $this->db->query("SELECT * from daily_view where style = '$style2' 
						// 	and line_id = '$q->line_id' and job_status = 't' and date(create_date) = '$date'")->row();

						// 	$daily3 = $this->db->query("SELECT * from daily_view where style = '$style3' 
						// 	and line_id = '$q->line_id' and job_status = 't' and date(create_date) = '$date'")->row();

						// 		//output1 commitment
						// 		//output2 commitment

						// 	$worker = (int)$daily2->present_sewer + (int)$daily2->present_folding;
						// 	$target_eff = (($daily1->target_output * $daily1->gsd_smv)+($daily2->target_output * $daily2->gsd_smv)+($daily3->target_output * $daily3->gsd_smv))/
						// 	($worker*$daily2->working_hours*60)*100;	
							
							
						// 	// var_dump($daily1->target_output);
						// 	// var_dump($daily1->gsd_smv);
						// 	// var_dump($daily2->target_output);
						// 	// var_dump($daily2->gsd_smv);
						// 	// var_dump($daily3->target_output);
						// 	// var_dump($daily3->gsd_smv);
						// 	// var_dump($worker);
						// 	// var_dump($daily2->working_hours);
						// 	// var_dump($target_eff);
							
						// 	// die();
							
						// 	$output_aktual1 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						// 	where style = '$style1' and line_id = '$q->line_id' and date = '$date'
						// 	and activity_name = 'outputqc'")->row();

						// 	$output_aktual2 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						// 	where style = '$style2' and line_id = '$q->line_id' and date = '$date'
						// 	and activity_name = 'outputqc'")->row();

						// 	$output_aktual3 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						// 	where style = '$style3' and line_id = '$q->line_id' and date = '$date' 
						// 	and activity_name = 'outputqc'")->row();

						// 	$sum_output_aktual1 = (int)$output_aktual1->total;
						// 	$sum_output_aktual2 = (int)$output_aktual2->total;
						// 	$sum_output_aktual3 = (int)$output_aktual3->total;
							
						// 	// if($daily2->present_sewer==0 || $daily2->present_sewer == null || $daily2->working_hours==0 || $daily2->working_hours == null ){
						// 	if($worker==null || $worker ==0){
							
						// 		// $target_eff = 0;
						// 		$actual_eff = 0;
						// 	}else{
						// 		// $actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv)+($sum_output_aktual3*(float)$daily3->gsd_smv))/((int)$daily2->present_sewer*(int)$daily2->working_hours*60)*100;
						// 		if($jam_berjalan >= $daily2->working_hours){
						// 			$actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv)+($sum_output_aktual3*(float)$daily3->gsd_smv))/((int)$worker*(float)$daily2->working_hours*60)*100;
						// 		}else{
						// 			$actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv)+($sum_output_aktual3*(float)$daily3->gsd_smv))/((int)$worker*(float)$jam_berjalan*60)*100;
						// 		}
						// 		// $actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv)+($sum_output_aktual3*(float)$daily3->gsd_smv))/((int)$worker*(float)$jam_berjalan*60)*100;
						// 	}	


						// 	$qc_output = (int)$sum_output_aktual1 +(int)$sum_output_aktual2+(int)$sum_output_aktual3;
						// }
						// else{
						// 	$style1 = $query2[0]->style;
						// 	$style2 = $query2[1]->style;

						// 	$daily1 = $this->db->query("SELECT * from daily_view where style = '$style1' 
						// 	and line_id = '$q->line_id' and job_status = 't' and date(create_date) = '$date'")->row();

						// 	$daily2 = $this->db->query("SELECT * from daily_view where style = '$style2' 
						// 	and line_id = '$q->line_id' and job_status = 't' and date(create_date) = '$date'")->row();

						// 	$worker = (int)$daily2->present_sewer + (int)$daily2->present_folding;
														
						// 	$target_eff = (($daily1->target_output * $daily1->gsd_smv)+($daily2->target_output * $daily2->gsd_smv))/
						// 	($worker*$daily2->working_hours*60)*100;	
							
						// 	$output_aktual1 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						// 	where style = '$style1' and line_id = '$q->line_id' and date = '$date' 
						// 	and activity_name = 'outputqc'")->row();

						// 	$output_aktual2 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						// 	where style = '$style2' and line_id = '$q->line_id' and date = '$date' 
						// 	and activity_name = 'outputqc'")->row();

						// 	$sum_output_aktual1 = (int)$output_aktual1->total;
						// 	$sum_output_aktual2 = (int)$output_aktual2->total;
							
						// 	// if($daily2->present_sewer==0 || $daily2->present_sewer == null || $daily2->working_hours==0 || $daily2->working_hours == null ){
						// 	if($worker==null || $worker ==0){
						// 		// $target_eff = 0;
						// 		$actual_eff = 0;
						// 	}else{
						// 		// $actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv))/((int)$daily2->present_sewer*(int)$daily2->working_hours*60)*100;
						// 		if($worker==0 || $jam_berjalan ==0){
						// 			$actual_eff = 0;
						// 		}
						// 		else{
						// 			if($jam_berjalan >= $daily2->working_hours){
						// 				$actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv))/((int)$worker*(float)$daily2->working_hours*60)*100;
						// 			}else{
						// 				$actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv))/((int)$worker*(float)$jam_berjalan*60)*100;
						// 			}
						// 			// $actual_eff = 100*((($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv))/((int)$worker*(float)$jam_berjalan*60));
						// 		}
								
						// 	}
						// 	$qc_output = (int)$sum_output_aktual1 +(int)$sum_output_aktual2;
						// }

						// $sum_order = $this->db->query("SELECT sum(qty_order) as total from qc_output
						//  where style='$style1' and line_id = '$q->line_id';")->row();
						// $sumorder = $sum_order->total;


						// $sum_qc = $this->db->query("SELECT sum(qc_output) as total from qc_output
						// where style='$style1' and line_id = '$q->line_id';")->row();
						// $sumqc = $sum_qc->total;

						// $daily1 = $this->db->query("SELECT * from daily_view where style = '$style1' 
						// and line_id = '$q->line_id' and date(create_date) = '$date'")->row();

						// $sewer1  = $daily1->present_sewer;
						// $smv1    = $daily1->gsd_smv;
						// if ($daily1->cumulative_day!=0&&(int)$daily1->cumulative_day<12) {
						// 	$cum1 =$daily1->cumulative_day;
						// }elseif ((int)$daily1->cumulative_day>=12) {
						// 	$cum1 =12;
						// }else{
						// 	$cum1 =1;
						// }
						// $target1 = $this->EffModel->eff_count($q->line_id, $daily1->change_over, $cum1);

						// $output1 = (int)$sumorder - (int)$sumqc;
						
						// $jamkerja1 = (($output1*$smv1)/($sewer1*60*($target1->eff/100)));

						// $daily2 = $this->db->query("SELECT * from daily_view where style = '$style2' 
						// and line_id = '$q->line_id' and date(create_date) = '$date'")->row();
						
						// if ($daily2->cumulative_day!=0&&(int)$daily2->cumulative_day<12) {
						// 	$cum2 =$daily2->cumulative_day;
						// }elseif ((int)$daily2->cumulative_day>=12) {
						// 	$cum2 =12;
						// }else{
						// 	$cum2 =1;
						// }

						// $daily3 = $this->db->query("SELECT * from daily_view where style = '$style3' 
						// and line_id = '$q->line_id' and date(create_date) = '$date'")->row();
						

						// $jamkerja2 = $daily2->working_hours - $jamkerja1;

						// $target2 = $this->EffModel->eff_count($q->line_id, $daily2->change_over, $cum2);

						// $output2 = ($daily2->present_sewer * $jamkerja2 * 60 * ($target2->eff/100))/$daily2->gsd_smv;

						// if($daily2->present_sewer==0 || $daily2->present_sewer == null || $daily2->working_hours==0 || $daily2->working_hours == null ){
						// 	$target_eff = 0;
						// 	$actual_eff = 0;
						// }else{							
						// 	$target_eff = (($output1 * $daily1->gsd_smv)+($output2 * $daily2->gsd_smv))/
						// 	($daily2->present_sewer*$daily2->working_hours*60)*100;

						// 	//output1 commitment
						// 	//output2 commitment
						// 	$target_eff = (($daily1->target_output * $daily1->gsd_smv)+($daily2->target_output * $daily2->gsd_smv)+($daily3->target_output * $daily3->gsd_smv))/
						// 	($daily2->present_sewer*$daily2->working_hours*60)*100;

						// }

						//  line 22
						//  target -59.412808641975
						//  output1 1256
						//  smv1 20.54
						//  output2 -1642.0656862745
						//  smv2 20.4
						//  sewer 27
						//  jam 8

						// jamkerja1 39.780401234568" string(26) "jamkerja2 -31.780401234568" string(4) "

						// $output_aktual1 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						// where style = '$style1' and line_id = '$q->line_id' and date = '$date' 
						// and activity_name = 'outputqc'")->row();

						// $output_aktual2 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						// where style = '$style2' and line_id = '$q->line_id' and date = '$date' 
						// and activity_name = 'outputqc'")->row();

						// $sum_output_aktual1 = (int)$output_aktual1->total;
						// $sum_output_aktual2 = (int)$output_aktual2->total;
						
						// if($daily2->present_sewer==0 || $daily2->present_sewer == null || $daily2->working_hours==0 || $daily2->working_hours == null ){
						// 	// $target_eff = 0;
						// 	$actual_eff = 0;
						// }else{
						// 	$actual_eff = (($sum_output_aktual1*(float)$daily1->gsd_smv)+($sum_output_aktual2*(float)$daily2->gsd_smv))/((int)$daily2->present_sewer*(int)$daily2->working_hours*60)*100;
						// }	
						
						
					// }
					// else{
					// 	// $cumday = $this->EffModel->cum_count($dash->daily_header_id, $date);
					// 	// $sewing = $this->EffModel->output_count($date, $dash->line_id, 'sewing');
					// 	$outputqc = $this->DisplayModel->output_count($date, $dash->line_id, 'outputqc');
					// 	// $folding = $this->EffModel->output_count($date, $dash->line_id, 'folding');
						
					// 	// if($folding->total==NULL){
					// 	// 	$folding->total = 0;
					// 	// }
					// 	// if($sewing->total==NULL){
					// 	// 	$sewing->total = 0;
					// 	// }
					// 	// if($outputqc->total==NULL){
					// 	// 	$outputqc->total = 0;
					// 	// }

					// 	//Target Efficiency
					// 	// if ($cumday!=0&&(int)$cumday<12) {
					// 	// 	$cum =$cumday;
					// 	// }elseif ((int)$cumday>=12) {
					// 	// 	$cum =12;
					// 	// }else{
					// 	// 	$cum =1;
					// 	// }
					// 	// $target = $this->EffModel->eff_count($dash->line_id, $dash->change_over, $cum);


					// 	// var_dump($target->eff);

					// 	// var_dump($time);
					// 	// var_dump($jam_istirahat);
					// 	// var_dump($result);
					// 	// var_dump($jam_berjalan);
					// 	// die();

					// 	$worker = (int)$dash->present_sewer + (int)$dash->present_folding;
							
					// 	if($worker == 0){
					// 		$actual_eff = 0;
					// 		$target_eff = 0;
					// 	}else{
					// 		$target_eff = ($dash->target_output * $dash->gsd_smv)/($worker*$dash->working_hours*60)*100;	

					// 		if($jam_berjalan >= $dash->working_hours){
					// 			$actual_eff  = 100*(($dash->gsd_smv*$outputqc->total))/(floatval($dash->working_hours)*60*$worker);
					// 		}else{
					// 			$actual_eff  = 100*(($dash->gsd_smv*$outputqc->total))/(floatval($jam_berjalan)*60*$worker);
					// 		}
					// 		// $actual_eff  = 100*(($dash->gsd_smv*$outputqc->total))/(floatval($jam_berjalan)*60*$worker);
					// 	}
					// 	$qc_output = (int)$outputqc->total;
					// }
				}


				// var_dump($jam_berjalan);
				// die();
				if($actual_eff>=$target_eff){
					$arrow = ' <i class="glyphicon glyphicon-arrow-up" style="color:green; font-size: 50px;"></i>';
				}else{
					$arrow = ' <i class="glyphicon glyphicon-arrow-down" style="color:red; font-size: 50px;"></i>';
				}

				
				if (round($actual_eff)<round($target_eff)) {
					$bg_actual_eff = '#F00;color:#EAEAEA';
					$bg_text       = '#ffffff';
				}else{
					$bg_actual_eff = '#0F0';
					$bg_text       = '#000000';
				}
				// style='background:".$bg_actual_eff."; color:".$bg_text."'
				// $draw_actual_eff = "<b style='background:".$bg_actual_eff."; color:".$bg_text."'>".number_format($actual_eff,1,'.',',')."</b>";

				$draw_actual_eff = "<b style='background:".$bg_actual_eff."; color:".$bg_text."'>".round($actual_eff)."</b>";


				if($dash->line_id == $lineid){

				}
				else{
					// $text_berjalan = 'TARGET EFF: '.number_format($target_eff,1,'.',',').''.$arrow.' 
					// ACTUAL EFF: '.number_format($actual_eff,1,'.',',');
					
					// $text_berjalan = 'TARGET EFF: '.number_format($target_eff,1,'.',',').' | 
					// ACTUAL EFF: '.$draw_actual_eff;

					
					$text_berjalan1 = round($target_eff);
					$text_berjalan2 = $draw_actual_eff;
				}
				$lineid = $dash->line_id;
				$no++;
			}

			// if(!empty($sum_output_aktual1) && !empty($sum_output_aktual2)){
			// 	$sum_output = (int)$sum_output_aktual1 +(int)$sum_output_aktual2;
				
			// }else{
			// 	$sum_output = (int)$outputqc->total;
			// }
			$sum_output = $outputqc;

			$data = array(
				'text_berjalan1' => $text_berjalan1,
				'text_berjalan2' => $text_berjalan2,
				'sum_output'    => $sum_output,
			);
		

			return $data;
        }
	}
}

/* End of file Display.php */
/* Location: ./application/controllers/Display.php */
