<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_critical extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Master_critical_model');
        
    }
    

    public function index()
    {
    
        $this->template->set('title','Master Critical');
        $this->template->set('desc_page','Master Critical');
        $this->template->load('layout','master/critical/list_critical_view');
    }
    public function dashboard_add_critical()
	{
       
        $data['product'] = $this->Master_critical_model->getdata_product();
    
		
		$this->template->load('layout','master/critical/add_critical', $data);
			
    }
    
    public function add_critical()
    {
        // $data['proses'] = $this->Master_critical_model->getdata_proses();
        // $data['product'] = $this->Master_critical_model->getdata_product();
        $this->load->view('master/critical/add_critical');
    }

    function search()
    {
        $json = [];
         if(!empty($this->input->get("q"))){
			$this->db->like('proses_name', $this->input->get("q"));
			$query = $this->db->select('master_proses_id,proses_name')
						->limit(10)
						->get("master_proses");
			$json = $query->result();
		}else{
            $query = $this->db->select('master_proses_id,proses_name')
						->limit(10)
						->get("master_proses");
			$json = $query->result();
        }
        echo json_encode($json);
    }

    function search_product()
    {
        $json = [];
         if(!empty($this->input->get("q"))){
			$this->db->like('nama_product', $this->input->get("q"));
			$query = $this->db->select('id,nama_product')
						->limit(10)
						->get("master_product");
			$json = $query->result();
		}else{
            $query = $this->db->select('id,nama_product')
                        ->limit(10)
                        ->get("master_product");
$json = $query->result();
        }
        echo json_encode($json);
    }

    public function save_critical($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
            $product     = $post['product'];
            $proses      = $post['proses'];
            // var_dump($proses);
            // die();
            $is_critical = $post['optionsRadios'];

            $proses_arr = explode(',', $proses);
            $proses_name =$proses_arr[1];
            $proses_id =$proses_arr[0];
            // var_dump($proses_name);
            // die();

            $product_arr = explode(',', $product);
            $product_id = $product_arr[0];
            $product_name = $product_arr[1];
            

			$cek = $this->db->query("SELECT * FROM master_critical where product_id = '$product_id'
            and proses_id = '$proses_id'  and is_critical = '$is_critical'");
    
            if($cek->num_rows()>0 ){
                $status = 2;
				$pesan = "DATA YANG DI INPUT SUDAH ADA";
            }else{

                $nomor = $this->db->query("SELECT max(no_urut) as max from master_critical where
                product_id = '$product_id'")->row_array();
                // var_dump($nomor['max']);
                // die();
        
                if($nomor['max'] != NULL){
                    $id = $nomor['max'];
                    $getid = (int) $id;
                    $no = $getid + 1;
                    
                    $id_crit = 'crit-'.$no .'-'. $product_id;
                }else{
                    $no = 1;
                    $id_crit = 'crit-'.$no .'-'. $product_id;
                }

                $data = array(
                    'master_critical_id' => $id_crit,
                    'no_urut' => $no,
                    'product_name'       => $product_name,
                    'product_id'         => $product_id,
                    'proses_id'          => $proses_id,
                    'proses_name'        => $proses_name,
                    'is_critical'        => $is_critical,
                    'create_date'        => date('Y-m-d')
                );
                $this->db->trans_start();
                $this->db->insert('master_critical', $data);
                $this->db->trans_complete();
				$status = 1;
				$pesan = "SUKSES";
			}
		}else{
			$status = 3;
			$pesan = "DATA ADA YANG KOSONG";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
    }
    
    public function listcritical()
    {
        $columns = array( 
                            0 =>'product_name', 
                            
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $totalData = $this->Master_critical_model->allposts_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Master_critical_model->allposts($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Master_critical_model->posts_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Master_critical_model->posts_search_count($search);
        }

        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $key=>$master)
            {
               $_temp                  = '"'.$master->product_id.'"';
                
                $nestedData['no']                 = (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['product_name']         = ucwords($master->product_name);
                $nestedData['action']             = "<center><a href='Master_critical/detail_critical/$master->product_id' class='btn btn-xs btn-info'><i class='fa fa-pencil'></i></a> </center>";
           //$nestedData['action']             = "<center><a onclick='return edit($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-pencil'></i></a> <a onclick='return delete_critical($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash-o'></i></a></center>";
         
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }

    public function detail_critical($product_id)
    {
        $data['product_id'] = $product_id;
        $this->template->set('title','Master Critical');
        $this->template->set('desc_page','Master Critical');
        $this->template->load('layout','master/critical/detail_critical', $data);

    }

    public function list_group_product()
    {
    
                $columns = array( 
                    0 =>'proses_name',
                    1 =>'id_critical' 
                    
                );
        $product_id = $this->input->post('product_id');
     
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $totalData = $this->Master_critical_model->allposts_count_detail($product_id);

        $totalFiltered = $totalData; 

        if(empty($this->input->post('search')['value']))
        {            
        $master = $this->Master_critical_model->allposts_detail($limit,$start,$order,$dir,$product_id);
        }
        else {
        $search = $this->input->post('search')['value']; 

        $master =  $this->Master_critical_model->posts_search_detail($limit,$start,$search,$order,$dir, $product_id);

        $totalFiltered = $this->Master_critical_model->posts_search_count_detail($search, $product_id);
        }

        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
        foreach ($master as $key=>$master)
        {
        $_temp                  = '"'.$master->master_critical_id.'"';
        if($master->is_critical == "true"){
            $crit = "Yes";
        } else{
            $crit = "No";
        }

        $nestedData['no']                 = (($draw-1) * 10) + (++$nomor_urut);
        $nestedData['proses_name']         = ucwords($master->proses_name);
        $nestedData['is_critical']         = ucwords($crit);
        $nestedData['action']             = "<center><a onclick='return edit($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-pencil'></i></a> <a onclick='return delete_critical($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash-o'></i></a></center>";
             
        $data[] = $nestedData;

        }
        }
    
      

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );
    
        echo json_encode($json_data);
    }

    public function edit($post=0)
    {
		$post          = $this->input->get();
		$id            = $post['id'];
		// $cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		$data['critical'] = $this->Master_critical_model->getData($id);
		$data['proses'] = $this->Master_critical_model->getdata_proses();
        $data['product'] = $this->Master_critical_model->getdata_product();
   
        $this->load->view('master/critical/edit_critical',$data);
    
    }

    public function update_critical($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
            $product     = $post['product'];
            $proses      = $post['proses'];
            $is_critical = $post['optionsRadios'];
            $master_critical_id = $post['master_critical_id'];

            $proses_arr = explode(',', $proses);
            $proses_name =$proses_arr[1];
            $proses_id =$proses_arr[0];

            $data = array(
                'master_critical_id' => $master_critical_id,
                'product_name'         => $product,
                'proses_id' => $proses_id,
                'proses_name' =>$proses_name,
                'is_critical' => $is_critical
            );

                $this->db->trans_start();
                $this->db->where('master_critical_id',$master_critical_id);
                $this->db->update('master_critical',$data);
                $this->db->trans_complete();
				$status = 1;
				$pesan = "SUKSES";
		}else{
			$status = 3;
			$pesan = "DATA ADA YG KOSONG";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
    }


    public function delete_critical($post=NULL)
    {
		$post = $this->input->get();
    
        if ($post!='') {
            $master_critical_id   = $post['id'];
        
            $this->db->trans_start();
            $this->db->where('master_critical_id',$master_critical_id);
            $this->db->delete('master_critical');
            $this->db->trans_complete();
            // $status = 1;
            echo '1';
            // $pesan = "SUKSES";
		}else{
            // $status = 3;
            echo '3';
			// $pesan = "DATA TIDAK DITEMUKAN";
		}
        
	}	


    
    
    


}

/* End of file Master_critical.php */
