<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voice extends CI_Controller {

	public function index()
	{
		$this->load->view('voice/index.php');
	}
	public function getwft()
	{
		$line_id=array();
		$line = $this->db->query('select line_name from cap_endline_view where sewing_deadline_date>now() GROUP BY line_name')->result();
		foreach ($line as $key => $l) {
			$line_id[] = $l->line_name;
		}
		$pesan = " panggilan untuk Supervaiser line, 1, 2 ,15, 16 segera menuju ke line masing-masing";

		$data = array('pesan' => $pesan,'line_name'=>$line_id );
		echo json_encode($data);
	}

}

/* End of file Voice.php */
/* Location: ./application/controllers/Voice.php */