<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		chek_session_qc();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('QcEndlineModel','Button_model'));
	}
	public function index()
	{
		$line_id = $this->session->userdata('line_id');
		$line = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();

		$data = array('line' =>$line , );
		$this->template->set('title',$line['line_name'].' - QC EndLine');
        $this->template->set('desc_page','QC EndLine');
		$this->template->load('layout2','qc_endline/index_qc_endline',$data);
	}

	public function loadpo($listpo=NULL)
	{

		$data['listpo'] = $this->QcEndlineModel->headerline();

		$this->load->view('qc_endline/show_poreference',$data);
	}
	public function loadsize($id=NULL)
	{
		$id = $this->input->get('id');
		if ($id!=NULL) {
			$data['list_size'] = $this->QcEndlineModel->listSize($id);

			$this->load->view('qc_endline/show_size', $data);

		}
	}
	public function searchsubmit($id=NULL,$po=0,$size=0)
	{
		$id      = $this->input->post('id');
		$po      = $this->input->post('poreference');
		$style   = $this->input->post('style');
		$article = $this->input->post('article');
		$size    = $this->input->post('size_id');
		$line    = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');

		if ($id!=NULL) {
			$qc = $this->QcEndlineModel->qc_output($id,$size);
			
			// $update_counter = $this->QcEndlineModel->update_counter($qc->qc_endline_id);
			// $update_counter_qc = $this->QcEndlineModel->update_counter_qc($qc->qc_endline_id);
			
			
			
			$cekHeader = $this->QcEndlineModel->cekHeader($id,$size)->row_array();
				$data = array(
					'line'        => $line,
					'id'          => $id,
					'size_id'     => $size,
					'poreference' => $cekHeader['poreference'],
					'size'        => $cekHeader['size'],
					// 'article'     => $cekHeader['article'],
					'factory'     => $factory
				);

			if ($cekHeader>0) {
				$this->load->view('qc_endline/result_qc_endline',$data);
			}
		}
	}

	//inputan IE nggak perlu
	private function insertStatusQc($id,$size)
	{
		/*$Header = $this->QcEndlineModel->cekHeader($id,$size)->row_array();
		$factory = $this->session->userdata('factory');
		
		$this->db->where('Date(update_at)', date('Y-m-d'));
		$this->db->where(array('style'=>$Header['style'],'line_id'=>$Header['line_id']));
		$cekStatusEndline = $this->db->get('status_qc_endline');
		if ($cekStatusEndline->num_rows()==0) {
			$detailStatus = array(
				'id'         => $this->uuid->v4(),
				'style'      =>$Header['style'], 
				'line_id'    =>$Header['line_id'], 
				'update_at'  =>date('Y-m-d H:i:s'), 
				'factory_id' =>$factory 
			);
			
			$this->db->insert('status_qc_endline', $detailStatus);
		}*/
		$Header = $this->QcEndlineModel->cekHeader($id,$size)->row_array();
		$factory = $this->session->userdata('factory');
		
		$this->db->where('Date(update_at)', date('Y-m-d'));
		$this->db->where(array('line_id'=>$Header['line_id'],'factory_id'=>$factory));
		$cekStatusEndline = $this->db->get('status_qc_endline');
		if ($cekStatusEndline->num_rows()==0) {
			$detailStatus = array(
				'id'         => $this->uuid->v4(),
				'style'      =>$Header['style'], 
				'line_id'    =>$Header['line_id'], 
				'update_at'  =>date('Y-m-d H:i:s'), 
				'factory_id' =>$factory 
			);
			
			$this->db->insert('status_qc_endline', $detailStatus);
		}else{
			$this->db->where('Date(update_at)', date('Y-m-d'));
			$this->db->where(array('line_id'=>$Header['line_id'],'factory_id'=>$factory));
			$this->db->update('status_qc_endline', array('style'=>$Header['style'],'update_at'=>date('Y-m-d H:i:s')));
		}
		
		
	}
	public function loaddata($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$id   = $post['id'];
			$size = $post['size'];
			$data = $this->qcDetail($id,$size);

			echo json_encode($data);
		}
	}
	private function qcDetail($id=NULL,$size=NULL)
	{
		// $header     = $this->db->get_where('inline_header',array('inline_header_id'=>$id))->row_array();

		// $sizeDetail = $this->QcEndlineModel->qtyOrderSize($id,$size);
		$qc = $this->QcEndlineModel->qc_output($id,$size);
		
		// var_dump ($qc);
		// die();

		if ($qc!=NULL) {

			$qcid        = $qc->qc_endline_id;
			$update_date = $qc->update_date;

			
			if($update_date!=date('Y-m-d')){
				$this->QcEndlineModel->cek_daily($qcid);
			}
			
			$qc_endline = $this->QcEndlineModel->qc_output($id,$size);
			$counterQc       = $qc_endline->qc_output;//$this->QcEndlineModel->counterQcall($id,$sizeDetail->header_size_id);
			$counterDaily    = $this->QcEndlineModel->counterDaily($qc_endline->qc_endline_id); //$qc_endline->counter_daily;
			$counterQcall    = $qc_endline->qc_output;
			$repairing       = $qc_endline->repairing;
			$total_repairing = $qc_endline->total_repairing;
		}else{
			$counterQc       = 0;
			$counterQcall    = 0;
			$counterDaily    = 0;
			$repairing       = 0;
			$total_repairing = 0;
		}
			$counterStyle    = $this->QcEndlineModel->counterStyle($qc->line_id,$qc->style);

			$balance         = $qc->balance;
			$countersewing   = $this->Button_model->counterSewing($qc->line_id,$qc->factory_id,$qc->style);
			$wip             = $countersewing-$counterStyle;
			
			$wft = ($wip<0?0:$wip);

			$qc_id           = $qc->qc_endline_id;

			

			
			$data = array(
				'balance'         => $balance,
				'wft'             => $wft,
				'repairing'       => $repairing,
				'qc_id'           => $qc_id,
				'total_repairing' => $total_repairing,
				'counterall'      => $counterQcall,
				'counterDaily'    => $counterDaily,
				'style'           => $qc->style,
				'article'         => $qc->article
			);
			// var_dump($data);
			// die();
		return $data;
	}
	public function qc_up($post=NULL)
	{
		$post = $this->input->post();

		if ($post!=NULL) {

			$id            = $post['id'];
			$size_id       = $post['size_id'];
			$style         = $post['style'];
			$article       = $post['article'];
			$counter_daily = $post ['counter_daily'];
			$line_id       = $this->session->userdata('line_id');
			

			$qc = $this->QcEndlineModel->qc_output($id,$size_id);
			
			// $update_counter = $this->QcEndlineModel->update_counter($qc->qc_endline_id);
			// $update_counter_qc = $this->QcEndlineModel->update_counter_qc($qc->qc_endline_id);
			
			// if($update_counter == 1 && $update_counter_qc ==1){

			$counterStyle    = $this->QcEndlineModel->counterStyle($line_id,$style);

			$balance         = $qc->balance;
			$countersewing   = $this->Button_model->counterSewing($line_id,$qc->factory_id,$qc->style);
			$wip             = $countersewing-$counterStyle;
			
			$wft = ($wip<0?0:$wip);

			if($wft==0){
				$status=100;
			}
			else{

				$cekinputqc = $this->QcEndlineModel->cekInputQc($id,$size_id);
				$this->db->trans_start();
				if ($cekinputqc==0) {
					$this->insert_endline($id,$size_id,$style);

					$this->insert_endlinetrans($id,$size_id,$style,$counter_daily);
					$this->insertStatusQc($id,$size_id);

				}else{
					$this->insert_endlinetrans($id,$size_id,$style,$counter_daily);
					$this->insertStatusQc($id,$size_id);
				}


				if ($this->db->trans_status() == FALSE)
				{
					$this->db->trans_rollback();
					$status = 500;
				}else{
					$this->db->trans_complete();
					$status = 200;
				}
			}

			// $qc = $this->QcEndlineModel->qc_output($id,$size_id);

			// $counterStyle    = $this->QcEndlineModel->counterStyle($line_id,$style);

			// $balance         = $qc->balance;
			// $countersewing   = $this->Button_model->counterSewing($line_id,$qc->factory_id,$qc->style);
			// $wip             = $countersewing-$counterStyle;
			
			// $wft = ($wip<0?0:$wip);
			// }
			$data = array(
				'status'  => $status,
				// 'wip'     => $wft,
				// 'balance' => $balance,
			);
			echo json_encode($data);

		}
	}
	private function insert_endline($id,$size_id,$style)
	{
		$nik     = $this->session->userdata('nik');
		$factory = $this->session->userdata('factory');
		$data = array(
			'qc_endline_id'    => $this->uuid->v4(),
			'inline_header_id' => $id,
			'header_size_id'   => $size_id,
			'create_by'        => $nik,
			'style'            => $style,
			'factory_id'       => $factory
		);
		$this->db->insert('qc_endline', $data);
	}
	private function insert_endlinetrans($id,$size_id,$style,$counter_daily)
	{
		$getendline      = $this->QcEndlineModel->getEndline($id,$size_id);
		$endlinetrans_id = $getendline['qc_endline_id'];
		$counter_trans   = $this->QcEndlineModel->countPcs($endlinetrans_id);

		$counter_qc      =($counter_trans==0?1:$counter_trans+1);
		$factory = $this->session->userdata('factory');
		$nik     = $this->session->userdata('nik');
		$detail = array(
			'qc_endline_trans_id' => $this->uuid->v4(),
			'qc_endline_id'       => $getendline['qc_endline_id'],
			'style'               => $style,
			'factory_id'          => $factory,
			'counter_pcs'         => $counter_qc,
			'inline_header_id'    => $id,
			'create_by'           => $nik,
			'sys'                 => 'normal',
			'ip'                  => $this->input->ip_address()
		);

		$this->db->insert('qc_endline_trans', $detail);

		$counter_max = $counter_qc;//$this->QcEndlineModel->countPcs($endlinetrans_id);

		$this->db->where('qc_endline_id', $endlinetrans_id);
		$this->db->update('qc_endline', array('counter_qc'=>$counter_qc,
		'counter_daily'=>$counter_daily+1, 'update_date'=>date('Y-m-d H:i:s')));

	}

	public function cek_sequence()
	{
		$post = $this->input->post();

		
		if ($post!=NULL) {

			$id      = $post['id'];
			$size_id = $post['size_id'];

			$cektrans = $this->QcEndlineModel->cekCountertrans();
				
			if ($cektrans->num_rows()>0) {
				foreach($cektrans->result() as $cek){
					$update_counter = "select * from update_counter('$cek->qc_endline_id')";
					$this->db->query($update_counter);

					$update_counter_qc = "select * from update_counter_qc('$cek->qc_endline_id')";
					$this->db->query($update_counter_qc);
				}
			}


			$data = $this->qcDetail($id,$size_id);
			// $query = $this->QcEndlineModel->getEndline($id, $size_id);

			// $countersewing   = $this->Button_model->counterSewing($header['line_id'],$header['factory_id'],$header['style']);
		
			// $data['counter_qc']    = $data['counter_qc'];
			// $data['counter_daily'] = $data['counter_daily'];

			// var_dump($data);
			// die();
			echo json_encode($data);

			
		}
	}

	public function cek_balance()
	{
		$post = $this->input->post();

		
		if ($post!=NULL) {

			$id      = $post['id'];
			$size_id = $post['size_id'];

			$data = $this->qcDetail($id,$size_id);

			echo json_encode($data);

			
		}
	}

	public function showdefect()
	{
		$this->db->order_by('defect_id', 'asc');
		$this->db->where('defect_id<>', 0);
		$this->db->where('is_active', true);
		$defect   = $this->db->get('master_defect')->result();
		$id       = $this->input->get('id');
		$size_id  = $this->input->get('size_id');
		$style    = $this->input->get('style');
		$trans_id = $this->uuid->v4();
		$data     = array(
			'defect'   => $defect,
			'id'       => $id,
			'trans_id' => $trans_id,
			'style'    => $style,
			'size_id'  => $size_id
		);

		$this->load->view('qc_endline/list_defect', $data);

	}
	/*result inspect*/
	public function resultinspect()
	{
		
		$line_id   = $this->session->userdata('line_id');
		$factory  = $this->input->get('factory');
		$date = date('Y-m-d');


		// $counters = $this->QcEndlineModel->result_inspect($line_id, $factory);
		
		// $counter_defect = $this->QcEndlineModel->countdefect_endline($line_id,$date)->num_rows();
		$result_inspect = $this->QcEndlineModel->list_result_inspect($line_id, $factory);

		$data = array(
			'results'	=> $result_inspect
		);

		$this->load->view('qc_endline/list_result', $data);

	}
	/**/
	public function adddefectsubmit($post=NULL)
	{
		$post = $this->input->post();

		if ($post!=NULL) {

			$id       = $post['id'];
			$size_id  = $post['size_id'];
			$trans_id = $post['trans_id'];
			$defect_id= $post['defect_id'];
			$style= $post['style'];
			$grade = !empty($post['checkgrade']) ? $post['checkgrade'] : null;

			$qc = $this->QcEndlineModel->qc_output($id,$size_id);
			
			// $update_counter = $this->QcEndlineModel->update_counter($qc->qc_endline_id);
			// $update_counter_qc = $this->QcEndlineModel->update_counter_qc($qc->qc_endline_id);
			
			$this->db->where(array('inline_header_id'=>$id,'header_size_id'=>$size_id));
			$CekEndLine = $this->db->get('qc_endline');
			$this->db->trans_start();
			if ($CekEndLine->num_rows()==0) {
				$this->insert_endline($id,$size_id,$style);

				$endline_id = $this->db->get_where('qc_endline',array('inline_header_id'=>$id,'header_size_id'=>$size_id))->row_array()['qc_endline_id'];

				$this->insert_trans($id,$endline_id,$trans_id,$style);	
				$this->insert_defecttrans($id,$endline_id,$trans_id,$defect_id,$style,$grade);
				$this->insertStatusQc($id,$size_id);

				$counterrepairing = $this->QcEndlineModel->getCounterDefect($endline_id,$trans_id);
				$counterdefect = $this->QcEndlineModel->getTotalDefect($endline_id,$trans_id);

				$this->db->where(array('inline_header_id'=>$id,'header_size_id'=>$size_id));
				$this->db->update('qc_endline', array('repairing'=>$counterrepairing,'total_repairing'=>$counterdefect));
			}else{
				$endline_id = $CekEndLine->row_array()['qc_endline_id'];
				$this->insert_trans($id,$endline_id,$trans_id,$style);
				$this->insert_defecttrans($id,$endline_id,$trans_id,$defect_id,$style,$grade);
				$this->insertStatusQc($id,$size_id);

				$counterrepairing = $this->QcEndlineModel->getCounterDefect($endline_id,$trans_id);
				$counterdefect = $this->QcEndlineModel->getTotalDefect($endline_id,$trans_id);

				$this->db->where(array('inline_header_id'=>$id,'header_size_id'=>$size_id));
				$this->db->update('qc_endline', array('repairing'=>$counterrepairing));
			}
			// validasi jika di table qc_endline_Defect trans id nya ga ada
			$this->db->where('qc_endline_trans_id', $trans_id);
			$cekqcdefect = $this->db->get('qc_endline_defect');
			if ($cekqcdefect->num_rows()==0) {
				// hapus trans id di qc_endline_trans_id
				$this->db->where('qc_endline_trans_id', $trans_id);
				$this->db->delete('qc_endline_defect');
				// update counter repairing
				$counterrepairing = $this->QcEndlineModel->getCounterDefect($endline_id,$trans_id);
				$this->db->where(array('inline_header_id'=>$id,'header_size_id'=>$size_id));
				$this->db->update('qc_endline', array('repairing'=>$counterrepairing));
				$status = 500;
			}else{
				$status = 200;
			}
						
			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();		        
			}else{
				$this->db->trans_complete();
			}
			
			$data['status'] = $status;


			echo json_encode($data);
		}


	}
	private function insert_trans($id,$endline_id,$trans_id,$style)
	{
		$factory =$this->session->userdata('factory');
		$nik =$this->session->userdata('nik');
		$detail = array(
			'qc_endline_trans_id' => $trans_id,
			'qc_endline_id'       => $endline_id,
			'style'               => $style,
			'factory_id'          => $factory,
			'inline_header_id'    => $id,
			'counter_pcs'         => 0,
			'create_by'           => $nik,
			'sys'                 => 'normal',
			'ip'                  => $this->input->ip_address()
		);
		$this->db->insert('qc_endline_trans', $detail);
	}

	private function insert_defecttrans($id,$endline_id,$trans_id,$defect_id,$style,$grade)
	{
		$defect=array();
		$factory =$this->session->userdata('factory');
		foreach ($defect_id as $key => $d) {

			$defect[] = array(
				'endline_defect_id'   => $this->uuid->v4(),
				'qc_endline_id'       => $endline_id,
				'qc_endline_trans_id' => $trans_id,
				'style'               => $style,
				'factory_id'          => $factory,
				'inline_header_id'    => $id,
				'defect_id'           => $d,
				'grade'				  => $grade
            );
		}
		// $this->db->insert_batch('qc_endline_defect', $defect);
		foreach($defect_id as $a => $b){

			$cek = $this->db->get_where('qc_endline_defect', array('defect_id'=>$b,'qc_endline_id'=>$endline_id,'qc_endline_trans_id'=>$trans_id));
			if ($cek->num_rows()==0) {
			 	$this->db->insert('qc_endline_defect',$defect[$a]);
			 }

		}
	}
	public function repairing_list($get=NULL)
	{
		$get = $this->input->get();
		if ($get!=0) {
			$id = $get['id'];
			$data['listrepairing'] = $this->QcEndlineModel->show_repairing($id);

			$this->load->view('qc_endline/list_repairing_proses',$data);
		}
	}
	public function repairing_proses($get=NULL)
	{
		$get = $this->input->get();
		if ($get!=NULL) {
			$id         = $get['id'];
			$endline_id = $get['endline_id'];

			$repair_defect = $this->db->get_where('qc_endline_defect', array('qc_endline_trans_id'=>$id,'status_defect'=>0));

			foreach ($repair_defect->result() as $repair) {
				$list[] = $repair->defect_id;
			}
			$list = array_unique($list);

						$this->db->order_by('defect_id', 'asc');
						$this->db->where('defect_id<>', 0);
						$this->db->where_in('defect_id',$list);
						$this->db->where('is_active', true);
			$defect_in = $this->db->get('master_defect');

						$this->db->order_by('defect_id', 'asc');
						$this->db->where('defect_id<>', 0);
						$this->db->where('is_active', true);
						$this->db->where_not_in('defect_id',$list);
			$defect_not = $this->db->get('master_defect');
			$data = array(
				'id'         => $id,
				'endline_id' => $endline_id,
				'defect_in'  => $defect_in,
				'defect_not' => $defect_not
			);
			$this->load->view('qc_endline/show_repairing_proses',$data);
		}
	}
	public function update_status_defect($endline_id,$trans_id,$defect_id)
	{
		$this->db->where(array('qc_endline_trans_id'=> $trans_id,'qc_endline_id'=>$endline_id));
		if (count($defect_id)>0) {
			$this->db->where_not_in('defect_id', $defect_id);
		}
		$this->db->update('qc_endline_defect', array('status_defect'=>1,'update_at'=>date('Y-m-d H:i:s')));
	}
	public function submit_repairing($post=NULL,$adddefect_id=NULL,$defect_id=NULL)
	{
		$post = $this->input->post();


		if ($post!=NULL) {
			
			$trans_id   = $post['id'];
			$endline_id = $post['endline_id'];

			$this->db->trans_start();

			$defect_id = array();

			$this->update_status_defect($endline_id,$trans_id,$defect_id);
			
			$repair         = $this->QcEndlineModel->getCounterDefect($endline_id,$trans_id);;
			$total_repair   = $this->QcEndlineModel->getTotalDefect($endline_id,$trans_id);
			$counter_trans  = $this->QcEndlineModel->countPcs($endline_id);
			$counter_qc     =($counter_trans==0?1:$counter_trans+1);
			

			$this->db->where('qc_endline_trans_id', $trans_id);
			$this->db->update('qc_endline_trans', array('counter_pcs'=>$counter_qc,'create_date'=>date('Y-m-d H:i:s')));

			$this->db->where('qc_endline_id', $endline_id);
			$this->db->update('qc_endline', array('counter_qc'=>$counter_qc,'repairing'=>$repair,'total_repairing'=>$total_repair));
			/*validasi ketika counter di header dan transaksi tidak sama*/
			// $cektrans = $this->QcEndlineModel->cekCountertrans($endline_id);
			
			// if ($cektrans>0) {
			// 	$update_counter = "select * from update_counter('$endline_id')";
			// 	$this->db->query($update_counter);

			// 	$update_counter_qc = "select * from update_counter_qc('$endline_id')";
			// 	$this->db->query($update_counter_qc);
			// }
			/*if (isset($post['defect_id'])) {
				if (isset($post['defect_id'])) {
					$defect_id = $post['defect_id'];
				}else{
					$defect_id=array();
				}
				if (isset($post['adddefect_id'])) {
					$adddefect_id = $post['adddefect_id'];
				}else{
					$adddefect_id=array();
				}

				$combine = array_merge($defect_id,$adddefect_id);

				$this->insert_defecttrans($qc_endline['inline_header_id'],$endline_id,$trans_id,$combine,$qc_endline['style']);
				$this->update_status_defect($endline_id,$trans_id,$combine);
			}else{
				

			}*/
			
			
			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
		        $status = 500;

			}else{
				$status = 200;
				$this->db->trans_complete();
			}

			$data['status'] = $status;


			echo json_encode($data);
		}

	}
	public function qc_masal($post=NULL)
	{
		$post= $this->input->post();

		if ($post!=NULL) {

			$qty     = $post['qty'];
			$id      = $this->input->post('id');
			$size_id = $this->input->post('size_id');
			$style   = $this->input->post('style');
			$cekinputqc = $this->QcEndlineModel->cekInputQc($id,$size_id);

			$qc = $this->QcEndlineModel->qc_output($id,$size_id);
			// $update_counter = $this->QcEndlineModel->update_counter($qc->qc_endline_id);
			// $update_counter_qc = $this->QcEndlineModel->update_counter_qc($qc->qc_endline_id);
			
			$this->db->trans_start();
				if ($cekinputqc==0) {
					$this->insert_endline($id,$size_id,$style);
	
					$this->insert_endlinetrans_masal($id,$size_id,$style,$qty);
					$this->insertStatusQc($id,$size_id);
	
				}else{
					$this->insert_endlinetrans_masal($id,$size_id,$style,$qty);
					$this->insertStatusQc($id,$size_id);
				}
			
			
			

			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
		        $status = 500;
			}else{
				$this->db->trans_complete();
				$status = 200;
			}

			$data['status'] = $status;


			echo json_encode($data);
			
		}
	}
	private function insert_endlinetrans_masal($id,$size_id,$style,$qty)
	{
		$getendline      = $this->QcEndlineModel->getEndline($id,$size_id);
		$endlinetrans_id = $getendline['qc_endline_id'];
		$counter_trans   = $this->QcEndlineModel->countPcs($endlinetrans_id);
		$counter_qc      = ($counter_trans==0?0:$counter_trans);
		$factory         = $this->session->userdata('factory');
		$nik             = $this->session->userdata('nik');
		for ($i=1;$i<=$qty;$i++){
			$counter_qc+=1;
            $detail = array(
				'qc_endline_trans_id' => $this->uuid->v4(),
				'qc_endline_id'       => $getendline['qc_endline_id'],
				'style'               => $style,
				'factory_id'          => $factory,
				'counter_pcs'         => $counter_qc,
				'inline_header_id'    => $id,				
				'create_by'           => $nik,
				'sys'                 => 'manual-'.$qty,
				'ip'                  => $this->input->ip_address()
			);
            $this->db->insert('qc_endline_trans', $detail);
        }
        $counter_max = $this->QcEndlineModel->countPcs($endlinetrans_id);

		$this->db->where('qc_endline_id', $endlinetrans_id);
		$this->db->update('qc_endline', array('counter_qc'=>$counter_max));
		// $cektrans = $this->QcEndlineModel->cekCountertrans($endlinetrans_id);
			
		// if ($cektrans>0) {
		// 	$update_counter = "select * from update_counter('$endlinetrans_id')";
		// 	$this->db->query($update_counter);

		// 	$update_counter_qc = "select * from update_counter_qc('$endlinetrans_id')";
		// 	$this->db->query($update_counter_qc);
		// }
	}
	public function qcoutstanding()
	{
		$line_id = $this->session->userdata('line_id');
		$factory = $this->session->userdata('factory');

		$listdefects = $this->QcEndlineModel->defect_outstanding($line_id,$factory);

		$data = array(
			'listdefects' => $listdefects 
		);
		$this->load->view('qc_endline/show_defect_outstanding', $data);
	}

	public function detaildefect($get=NULL)
	{

		$get = $this->input->get();

		if ($get!=NULL) {
			
			$data['defects'] = $this->QcEndlineModel->detailDefect($get['qc_id']);
			
			$this->load->view('qc_endline/show_detail_defect',$data);
		}
	}
	public function cekstatuspo()
	{
		$this->load->view('qc_endline/show_statuspo');
	}
	public function statusposubmit($po=NULL)
	{
		$po               = $this->input->post('poreference');
		$poreference = ($po==''?'NULL':$po);
		
		$draw             = '';
		$no               = 1;
		$draw             .= '<table id="table-defectoutstanding" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%"><thead><tr>
                            <th class="text-center">NO.</th>
                            <th class="text-center">TANGGAL</th>
                            <th class="text-center">PO BUYER</th>
                            <th class="text-center">STYLE</th>
                            <th class="text-center">ARTICLE</th>
                            <th class="text-center">SIZE</th>
                            <th class="text-center">QTY ORDER</th>
                            <th class="text-center">TOTAL OUTPUT</th>
                            <th class="text-center">BALANCE</th>
							<th class="text-center">STATUS</th>
							<th class="text-center">DETAIL</TH>
                        </tr></thead><tbody>';
		$headers = $this->QcEndlineModel->header_po($poreference);
		if ($headers->num_rows()==0) {
			$draw.='<tr><td colspan="8">Tidak Ada Data</td></tr>';
		}else{
			foreach ($headers->result() as $key => $header) {
			$details = $this->QcEndlineModel->detailPo($header->inline_header_id,$header->size);
			$tanggal='';
			$total_output=0;
			$balance=0;
			$id_temp='';
			foreach ($details->result() as $key => $detail) {
				$tanggal      = $detail->update_date;
				$total_output = $detail->qc_output;
				$balance      = $detail->balance;
				$id_temp         = '"'.$detail->qc_endline_id.'"';
			}
			// var_dump($header);
			// die();
			// $_temp = '"'.$header->qc_endline_id.'"';
			$temp = '"'.$header->inline_header_id.'","'.$header->header_size_id.'","'.$header->style.'","'.$header->poreference.'","'.$header->size.'","'.$header->article.'"';
			$draw.="<tr>".
						'<td>'.$no++.'</td>'.
						'<td>'.$tanggal.'</td>'.
						'<td>'.$header->poreference.'</td>'.
						'<td>'.$header->style.'</td>'.
						'<td>'.$header->article.'</td>'.
						'<td>'.$header->size.'</td>'.
						'<td>'.$header->qty.'</td>'.
						'<td>'.$total_output.'</td>'.
						'<td>'.$balance.'</td>';
						if ($header->qty==$total_output) {
							$draw.='<td><span class="label label-success">BALANCE</span></td>';
						}elseif ($total_output>$header->qty) {
							$draw.='<td><span class="label label-danger">OVER</span></td>';
						}else{
							$draw.='<td><span class="label label-info">OUT STANDING</span></td>';
						}
						
					$draw .= "<td> <div class='btn-group'>".
						"<a onclick='return detail($id_temp)'". "class='btn btn-primary' href='javascript:void(0)'><span data-icon='&#xe101'></i></a>".
						"<a onclick='return selectstatus($temp)'". "class='btn btn-success' href='javascript:void(0)'><span data-icon='&#x52'></i></a>".
						"</div>".

						
					'<tr>';					
		}
		}
		
		$draw.='</tbody></table>';

		echo $draw;

	}
	
	public function detailComponent($get=NULL)
	{
		$get =$this->input->get();

		if ($get!=NULL) {
			$detailComponent = $this->QcEndlineModel->DetailEndline($get['qc_endline_id']);
			$draw='';
			
			$no=1;
			foreach ($detailComponent->result() as $key => $detail) {
				// $this->db->where('barcode_id', $detail->barcode_id);
				// $this->db->where('line_id', $this->session->userdata('line_id'));
				// $detailDistribusi = $this->db->get('distribusi_detail_view');
				// $status='';
				// $create_by='';
				// foreach ($detailDistribusi->result() as $key => $d) {
				// 	$status = ($d->status=='onprogress'||$d->status=='completed'?'<center><span class="badge bg-success">Receive</span></center>':'<center><span class="badge bg-primary">'.$d->status.'</span></center>');
				// 	$create_by = $d->name;
				// }
				$draw .='<tr>'.
							'<td>'.$no++.'</td>'.
							'<td>'.$detail->date.'</td>'.
							'<td>'.$detail->line_name.'</td>'.
							'<td>'.$detail->poreference.'</td>'.
							'<td>'.$detail->style.'</td>'.
							'<td>'.$detail->article.'</td>'.
							'<td>'.$detail->size.'</td>'.
							'<td>'.$detail->order.'</td>'.
							'<td>'.$detail->max.'</td>'.
							'<td>'.$detail->daily_output.'</td>'.
							'<td>'.$detail->defect_perday.'</td>'.
							'<td>'.$detail->balance_per_day.'</td>'.
				       '</tr>';
			}
			$data = array('draw' => $draw );
			$this->load->view('qc_endline/index_detail_history', $data);
		}
	}

}

/* End of file Qc.php */
/* Location: ./application/controllers/Qc.php */