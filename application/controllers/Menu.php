<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Menu_model','Cekpermision_model'));
        chek_session();
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Add Menu');
            $this->template->set('desc_page','Add Menu');
            $this->template->load('layout','menu/list_menu_view');
        }
       
	}
	public function list_menu()
	{
		$columns = array( 
                            0 =>'menu_id', 
                            1 =>'nama_menu',
                            2=> 'link',
                            3=> 'icon',
                            4=> 'is_main_menu'
                        );

		$limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');

  
        $totalData = $this->Menu_model->allposts_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $menu = $this->Menu_model->allposts($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $menu =  $this->Menu_model->posts_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Menu_model->posts_search_count($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($menu))
        {
            foreach ($menu as $menu)
            {
                $_temp = '"'.$menu->menu_id.'"';
            	$nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['nama_menu'] = strtoupper($menu->nama_menu);
                $nestedData['link'] = strtoupper($menu->link);
                if ($menu->is_main_menu=='0'){ 
                    $nestedData['main_menu'] = "Is Main Menu";
                }else{
                    $nestedData['main_menu'] = "Sub Menu";
                }
                $nestedData['action'] = "<center><a onclick='return edit_menu($_temp)' href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-edit'></i></a> <a onclick='return hapus($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash-o'></i></a></center>";
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
	public function add_menu_view()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('menu/add_menu_view');
        }
		
	}
    public function edit_menu_view()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->db->where('menu_id', $_GET['menu_id']);
            $data['record'] = $this->db->get('menu')->row_array();
            $this->load->view('menu/edit_menu_view',$data);
        }
        
    }
	public function save_menu()
	{
        $post = $_POST;
        $detail = array(
            'menu_id'       => $this->uuid->v4(),
            'nama_menu'     => $post['nama_menu'], 
            'link'          => $post['link'], 
            'icon'          => $post['icon'], 
            'urut'          => $post['urut'], 
            'is_main_menu'  => $post['is_main_menu'] 
        );
        $this->db->insert('menu', $detail);
        echo "sukses";
	}
    public function edit_menu()
    {
        $post = $_POST;
        $detail = array(
            'nama_menu'     => $post['nama_menu'], 
            'link'          => $post['link'], 
            'icon'          => $post['icon'],
            'urut'          => $post['urut'], 
            'is_main_menu'  => $post['is_main_menu'] 
        );
        $this->db->where('menu_id', $post['menu_id']);
        $this->db->update('menu', $detail);
        echo "sukses";
    }
    public function delete_menu()
    {
        $this->db->where('menu_id', $_GET['menu_id']);
        $cek = $this->db->get('menu');
        if ($cek->num_rows()>0) {
            $this->db->where('menu_id', $_GET['menu_id']);
            $this->db->delete('menu');
            echo "sukses";
        }
    }

}

/* End of file menu.php */
/* Location: ./application/controllers/menu.php */