<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cap extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		chek_session();
		$this->load->model(array('CapModel','Cekpermision_model'));
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{

	}
	public function create_cap_endline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	$date = date('Y-m-d');
        	$nik = $this->session->userdata('nik');
        	$this->db->order_by('sewing_deadline_date', 'desc');
        	$this->db->where(array('date'=> $date/*,'spv_production'=>$nik*/));
        	$data['record']= $this->db->get('cap_endline_view')->row_array();

        	$data['line'] = $this->CapModel->getLine();

            $this->template->set('title','Create CAP Endline');
            $this->template->set('desc_page','Create CAP Endline');
            $this->template->load('layout','cap/index_create_endlline',$data);
        }
	}
	/*public function loadwft()
	{
		$draw ='';
		$jum=1;
		$date = date('Y-m-d');
		$now  = date('Y-m-d H:i:s');
		$nik = $this->session->userdata('nik');
		$this->db->order_by('sewing_deadline_date', 'desc');
		$this->db->where('date', $date);

        $record= $this->db->get('cap_endline_view');

        $time = $record->row_array()['sewing_deadline_date'];

        $draw.='<br><table id="table-top" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20px">LINE</th>
                        <th width="20px">WFT</th>
                        <th>Jenis Defect</th>
                        <th>Total</th>
                        <th width="50px">ACTION</th>
                        <th>Status</th>

                    </th>
                </thead>
                <tbody>';
		foreach ($record->result() as $key => $r) {
			$temp = '"'.$r->cap_endline_id.'","'.$r->cap_endline_detail_id.'"';
            $draw.= '<tr>';
            if($jum <= 1) {
            $draw.='<td align="center" rowspan="'.$r->jumlah.'">'.$r->line_name.'</td>';
            $draw.='<td align="center" rowspan="'.$r->jumlah.'">'.$r->wft.'%'.'</td>';
                $jum = $r->jumlah;
            } else {
                $jum = $jum - 1;
            }
            $draw.='<td>'.$r->defect_jenis.'</td>'
            .'<td>'.$r->total_defect.' PCS'.'</td><td>';
            if ($now<$r->sewing_deadline_date&&$r->corective_action_date==NULL) {
            	$draw.="<a onclick='return create($temp)' href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-edit'></i> Create</a>";
            }else if ($now<$r->sewing_deadline_date||$r->corective_action_date!=NULL) {
            	$draw.="<a href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-thumbs-o-up'></i></a>";
            }else{
            	$draw.="<a onclick='return create($temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-times'></i></a>";
            }
            if ($r->qc_verify_date!=NULL) {
            	$draw.="<a href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-check-square'></i></a>";
            }
            $draw.='</td><td><span class="label label-success">'.ucwords($r->status).'</td>';

         	$draw.='</td></tr>';
		}
		$draw.='</tbody></table></div>';
		$data = array('time' => $time,'draw'=>$draw );
		echo json_encode($data);
	}*/
	public function showwft_ajax()
	{
		$columns = array(
							0 =>'line_id',
							1 =>'wft',
							2 => 'jam',
							3 =>'jenis_defect',
							4 => 'total',
							5 => 'action',
							6 => 'status'
                        );

		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');
		$date    = date('Y-m-d');
		$factory = $this->session->userdata('factory');
		$nik     = $this->session->userdata('nik');

		$fromline = ($this->input->post('fromline')!=NULL?$this->input->post('fromline'):NULL);
		$toline = ($this->input->post('toline')!=NULL?$this->input->post('toline'):NULL);
		
		$now  = date('Y-m-d H:i:s');
		// $date = '2018-11-29';

		$totalData     = $this->CapModel->allposts_count_wft($fromline,$toline);

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value'])&&$fromline==NULL&&$toline==NULL)
		{
		$wft        = $this->CapModel->allposts_wft($limit,$start,$order,$dir,$fromline,$toline);
		}
		else {
		$search        = $this->input->post('search')['value'];


		$wft        =  $this->CapModel->posts_search_wft($limit,$start,$search,$order,$dir,$fromline,$toline);

		$totalFiltered = $this->CapModel->posts_search_count_wft($search,$date,$factory,$fromline,$toline);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($wft))
        {
            foreach ($wft as $wft)
            {
				$_temp = '"'.$wft->cap_endline_id.'","'.$wft->cap_endline_detail_id.'"';

				$nestedData['no']          = (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['line']        = $wft->line_name;
				$nestedData['wft']        = $wft->wft.'%';
				$nestedData['defect_jenis']        = ucwords($wft->defect_jenis);
				$nestedData['total']        = $wft->total_defect;
				$nestedData['jam']        = $wft->jam;
				if ($now<$wft->sewing_deadline_date&&$wft->corective_action_date==NULL) {
					$status = ($wft->status!=NULL?$wft->status:'Waiting CAP');
				$nestedData['aksi']                                         = "<center><a onclick='return create($_temp)' href='javascript:void(0)' class='btn btn-sm btn-primary'><i class='fa fa-pencil-square-o'></i> Membuat CAP</a></center>";
				$nestedData['status']                                         = "<span class='badge bg-primary'>".ucwords($status)."</span></center>";
				}else if ($now<$wft->sewing_deadline_date&&$wft->corective_action_date!=NULL&&$wft->qc_verify_date==NULL) {
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-sm btn-primary'><i class='fa fa-thumbs-o-up'></i></a></center>";
				$nestedData['status']                                         = "<span class='badge bg-success'>".$wft->status."</span></center>";
				}else if ($now<$wft->sewing_deadline_date||($wft->corective_action_date!=NULL&&$wft->qc_verify_date!=NULL)) {
					$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-sm btn-primary'><i class='fa fa-thumbs-o-up'></i></a><a href='javascript:void(0)' class='btn btn-sm btn-success'><i class='fa fa-check-square'></i></a></center>";
					$nestedData['status']                                         = "<span class='badge bg-success'>".ucwords($wft->status)."</span></center>";
				}else{
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-sm btn-danger'><i class='fa fa-times'></i> Expired</a></center>";
				$nestedData['status']                                         = "<span class='badge bg-danger'>Expired</span></center>";
				}

                $data[] = $nestedData;

            }
        }


        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function showCreate_cap($get=NULL)
	{
		$get = $this->input->get();
		if ($get!=NULL) {
			$this->db->where('qc_verify_date is null',NULL,FALSE);
			$this->db->where('corective_action_date is null',NULL,FALSE);
			$this->db->where(array('cap_endline_id'=>$get['id_cap'],'cap_endline_detail_id'=>$get['id_detail']));
			$this->db->select('cap_endline_detail_id,defect_id,defect_jenis,style,line_name,jam');
			$cek = $this->db->get('cap_endline_view');

			if ($cek->num_rows()>0) {
				$data['notif']  =1;
				$data['record'] = $cek->row_array();

				echo json_encode($data);
			}
		}
	}
	public function saveCapEndline($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$DateTime = new DateTime();
		  	$DateTime->modify('+1 hours');
		  	$qc_deadline=  $DateTime->format("Y-m-d H:i:s");
			$detail = array(
				'cap_endline_trans_id'  => $this->uuid->v4(),
				'cap_endline_detail_id' =>$post['id_detail'],
				'major_couse'           =>$post['major_couse'],
				'root_couse'            =>$post['root_couse'],
				'create_by'             =>$post['nik'],
				'action'                =>$post['action']
			);
			$this->db->insert('cap_endline_trans', $detail);

			$this->db->where('cap_endline_detail_id', $post['id_detail']);
			$this->db->update('cap_endline_detail', array('corective_action_date'=>date('Y-m-d H:i:s'),'qc_deadline_date'=>$qc_deadline,
				'status'=>'Cap On Progress'));
			echo "1";
		}
	}
	public function create_cap_inline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
        	$data['line'] = $this->CapModel->getLine();
            $this->template->set('title','Create CAP Inline');
            $this->template->set('desc_page','Create CAP Inline');
            $this->template->load('layout','cap/index_create_inline',$data);
        }
	}
	public function showdefectinline()
	{
		$columns = array(
							0 =>'line_id',
							1 =>'line_id',
							2 => 'round',
							3 => 'proses_name',
							4 => 'defect_id',
							5 => 'action',
							6 => 'status'
                        );

		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');

		$fromline = ($this->input->post('fromline')!=NULL?$this->input->post('fromline'):NULL);
		$toline = ($this->input->post('toline')!=NULL?$this->input->post('toline'):NULL);

		$totalData     = $this->CapModel->allposts_count_capinline($fromline,$toline);


		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value'])&&$fromline==NULL&&$toline==NULL)
		{
		$capinline        = $this->CapModel->allposts_capinline($limit,$start,$order,$dir,$fromline,$toline);

		}else {
		$search        = $this->input->post('search')['value'];


		$capinline        =  $this->CapModel->posts_search_capinline($limit,$start,$search,$order,$dir,$fromline,$toline);

		$totalFiltered = $this->CapModel->posts_search_count_capinline($search,$fromline,$toline);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($capinline))
        {
            foreach ($capinline as $capinline)
            {
            	$query ="select * from master_defect where defect_id in($capinline->defect_id) and defect_id<>0";
				$defect = $this->db->query($query);

				foreach ($defect->result() as $key => $d) {

					$nama_defect[] = $d->defect_id.'.'.$d->defect_jenis.'</br>';
				}
				$nama_defect = array_unique($nama_defect);
				/*var_dump ($nama_defect);
				die();*/
				$_temp = '"'.$capinline->cap_inline_id.'"';

				$nestedData['no']          = (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['line']        = $capinline->line_name;
				$nestedData['round']       = $capinline->round;
				$nestedData['nama']        = ucwords($capinline->sewer_name);
				$nestedData['proses_name'] = ucwords($capinline->proses_name);
				$nestedData['defect_id']   = $capinline->defect_jenis;
				if (($capinline->max==$capinline->round&&$capinline->corective_action_date==NULL)||($capinline->repair_date!=NULL&&$capinline->corective_action_date==NULL)) {
				$nestedData['aksi']                                         = "<center><a onclick='return create($_temp)' href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-pencil-square-o'></i> Membuat CAP</a></center>";
				if ($capinline->status!='') {
					$nestedData['status']                                         = "<span class='badge bg-warning'>".ucwords($capinline->status)."</span></center>";
				}else{
					$nestedData['status']                                         = "<span class='badge bg-primary'> Waiting CAP</span></center>";
				}
				}else if (($capinline->max==$capinline->round||$capinline->verify_date!=NULL)||($capinline->repair_date==NULL&&$capinline->corective_action_date!=NULL)) {
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-thumbs-o-up'></i></a></center>";
				$nestedData['status']                                         = "<span class='badge bg-success'>".ucwords($capinline->status)."</span></center>";
				}else{
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-times'></i></a></center>";
				$nestedData['status']                                         = "<span class='badge badge-danger'>Expired</span></center>";
				}

                $data[] = $nestedData;

            }
        }


        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function verify_endline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Verify Endline');
            $this->template->set('desc_page','Verify Endline');
            $this->template->load('layout','cap/index_verify_endline');
        }
	}
	public function showVerifyEndline()
	{
		$columns = array(
                            0 =>'line_id',
                            1 =>'wft',
                            2 =>'jenis_defect',
                            3=> 'total_defect',
                            4=> 'qc_verify_date'
                        );

		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');
		$date    = date('Y-m-d');
		$factory = $this->session->userdata('factory');
		$nik     = $this->session->userdata('nik');
		$now     = date('Y-m-d H:i:s');

		$totalData     = $this->CapModel->allposts_count_verify($date,$factory,$nik);

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{
		$verify        = $this->CapModel->allposts_verify($limit,$start,$order,$dir,$date,$factory,$nik);
		}
		else {
		$search        = $this->input->post('search')['value'];


		$verify        =  $this->CapModel->posts_search_verify($limit,$start,$search,$order,$dir,$date,$factory,$nik);

		$totalFiltered = $this->CapModel->posts_search_count_verify($search,$date,$factory,$nik);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($verify))
        {
            foreach ($verify as $verify)
            {
				$_temp                      = '"'.$verify->cap_endline_detail_id.'"';
				$major_couse='';
				$root_couse='';
				$action='';

				$this->db->where('delete_at is NULL', NULL,FALSE);
				$this->db->where('cap_endline_detail_id', $verify->cap_endline_detail_id);
				$cap_trans = $this->db->get('cap_endline_trans');
				foreach ($cap_trans->result() as $key => $trans) {

					$major_couse = $trans->major_couse;
					$root_couse = $trans->root_couse;
					$action = $trans->action;
				}

				$nestedData['no']                                           = (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['line']                                         = $verify->line_name;
				$nestedData['wft']                                          = $verify->wft;
				$nestedData['jam']                                          = $verify->jam;
				$nestedData['jenis_defect']                                 = ucwords($verify->defect_jenis);
				$nestedData['total']                                        = $verify->total_defect;
				$nestedData['major_couse']                                  = ucwords($major_couse);
				$nestedData['root_couse']                                   = ucwords($root_couse);
				$nestedData['action']                                       = ucwords($action);
				$nestedData['status']                                       = ucwords($verify->status);
				$nestedData['deadlineqc']                                   = "<span id='data-countdown' class='data-countdown'>".$verify->qc_deadline_date."</span>";
				if ($now<$verify->qc_deadline_date&&$verify->qc_verify_date ==NULL) {
				$nestedData['aksi']                                         = "<center><a onclick='return verify($_temp)' href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-pencil-square-o'></i> Verify</a><a onclick='return perbaiki($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-retweet'></i> Perbaiki CAP</a></center>";
				}else if ($verify->qc_verify_date!=NULL) {
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-check'></i> Telah Verify</a></center>";
				}else{
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-times'></i> Expired</a></center>";
				}

                $data[] = $nestedData;

            }
        }


        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function verifyEndline($get=NULL)
	{
		$get = $this->input->get();
		$now     = date('Y-m-d H:i:s');
		if ($get!=NULL) {
			$cek = $this->db->get_where('cap_endline_detail', array('cap_endline_detail_id'=>$get['id_detail']))->row_array();

			if ($now<$cek['qc_deadline_date']) {
				$this->db->where('cap_endline_detail_id', $get['id_detail']);
				$this->db->update('cap_endline_detail', array('qc_verify_date'=>date('Y-m-d H:i:s'),'status'=>'cap verify'));
				echo "1";
			}else{
				echo "0";
			}

		}
	}
	public function loadinline()
	{
		$draw ='';
		$date = date('Y-m-d');
		$nik = $this->session->userdata('nik');

		$this->db->order_by('create_date', 'desc');
		$this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
        /*$this->db->where(array('spv_production'=>$nik));*/
        $record= $this->db->get('cap_inline_view');

        $draw.='<br><div style="overflow-x:auto;overflow-y:auto"><table id="table-top" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10px">LINE</th>
                        <th width="10px">ROUND</th>
                        <th>NAMA</th>
                        <th>NAMA PROSES</th>
                        <th>KODE DEFECT</th>
                        <th width="50px">ACTION</th>
                        <th>Status</th>

                    </th>
                </thead>
                <tbody>';

		foreach ($record->result() as $key => $r) {
			$temp = '"'.$r->cap_inline_id.'"';
			/*$this->db->where('defect_id<>0',NULL,FALSE);
			$this->db->where_in('defect_id', $r->defect_id);
			$defect = $this->db->get('master_defect');*/
			$query ="select * from master_defect where defect_id in($r->defect_id) and defect_id<>0";
			$defect = $this->db->query($query);


			foreach ($defect->result() as $key => $d) {
				$nama_defect = $d->defect_id.'. '.$d->defect_jenis;
			}

            $draw.= '<tr>'
             .'<td>'.$r->line_name.'</td>'
             .'<td>'.$r->round.'</td>'
             .'<td>'.$r->sewer_name.'</td>'
             .'<td>'.$r->proses_name.'</td>'
             .'<td>'.$nama_defect.'</td><td>';

             if ($r->max==$r->round&&$r->corective_action_date==NULL) {

             	$draw.="<a onclick='return create($temp)' href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-edit'></i> Create</a>"
             	.'<td><span class="label label-primary">'.$r->status.'</span></td>';
             }elseif ($r->corective_action_date!=NULL) {

             	$draw.="<a href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-thumbs-o-up'></i></a>";
             	if ($r->verify_date!=NULL) {
             		$draw.="<a href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-check-square'></i></a>";

             	}
             	$draw.='</td><td><span class="class="label label-success">'.$r->status.'</span></td>';
            }else{
            	$draw.="<a onclick='return create($temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-times'></i></a>"
            	.'</td><td><span class="label label-danger">Expired</span></td>';
            }
		}
		$draw.='</tbody></table></div>';
		$data = array('draw'=>$draw );
		echo json_encode($data);
	}
	public function saveCapInline($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			// $this->db->where('cap_inline_id', $post['id_detail']);
			// $cap = $this->db->get('cap_inline_view')->row_array();
			$cap_id = $post['id_detail'];

			$cap = $this->db->query("SELECT * from cap_inline 
			LEFT JOIN (
			SELECT max(round) as max, line_id from round_status where date(create_date) = date(now())
			GROUP BY line_id ) x on x.line_id = cap_inline.line_id::VARCHAR
			where cap_inline_id = '$cap_id'
			and corective_action_date is null and verify_date is null
			")->row_array();
			
			if ($cap['round']==$cap['max']) {
				$detail = array(
				'cap_inline_trans_id'  => $this->uuid->v4(),
				'cap_inline_id' =>$post['id_detail'],
				'major_couse'           =>$post['major_couse'],
				'root_couse'            =>$post['root_couse'],
				'create_by'             =>$post['nik'],
				'action'                =>$post['action']
				);
				$this->db->insert('cap_inline_trans', $detail);

				$this->db->where('cap_inline_id', $post['id_detail']);
				$this->db->update('cap_inline', array('corective_action_date'=>date('Y-m-d H:i:s'),'status'=>'Cap On Progress'));
				echo "1";
			}else{
				echo "0";
			}

		}
	}
	public function showCreateinline($get=NULL)
	{
		$get = $this->input->get();
		
		if ($get!=NULL) {
			$cap_id = $get['id_cap'];

			$cap = $this->db->query("SELECT * from cap_inline 
			LEFT JOIN (
			SELECT max(round) as max, line_id from round_status where date(create_date) = date(now())
			GROUP BY line_id ) x on x.line_id = cap_inline.line_id::VARCHAR
			where cap_inline_id = '$cap_id'
			and corective_action_date is null and verify_date is null
			")->row_array();

			// $this->db->where('cap_inline_id', $get['id_cap']);
			// $cap = $this->db->get('cap_inline_view')->row_array();
			if ($cap['round']==$cap['max']) {
				// $this->db->where('verify_date is null',NULL,FALSE);
				// $this->db->where('corective_action_date is null',NULL,FALSE);
				// $this->db->where('cap_inline_id', $get['id_cap']);
				// $this->db->select('cap_inline_id,line_name,round,sewer_name,proses_name');

				// $line = $get['id_cap'];

				// $query = $this->db->query("SELECT *
				// FROM cap_inline WHERE 
				// (verify_date is null or corective_saction_date is null) AND repair_date is not null and
				// cap_inline_id = '$line'");

				// $data['record']  = $this->db->get('cap_inline')->row_array();
				$data['record']  = $cap;
				// $data['record']  = $query->row_array();
				$data['notif']  =1;

				echo json_encode($data);
			}else{
				$data['notif']  =0;

				echo json_encode($data);
			}


		}
	}
	public function verify_inline()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Verify Inline');
            $this->template->set('desc_page','Verify Inline');
            $this->template->load('layout','cap/index_verify_inline');
        }
	}
	public function showVerifyinline()
	{
		$columns = array(
                            0 =>'line_id',
                            1 =>'line_id',
                            2 =>'round',
                            3 =>'jenis_defect',
                            4=> 'total_defect',
                            5=> 'qc_verify_date'
                        );

		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');
		$date    = date('Y-m-d');
		$factory = $this->session->userdata('factory');
		$nik     = $this->session->userdata('nik');

		$totalData     = $this->CapModel->allposts_count_verify_inline($date,$factory,$nik);

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{
		$verify        = $this->CapModel->allposts_verify_inline($limit,$start,$order,$dir,$date,$factory,$nik);
		}
		else {
		$search        = $this->input->post('search')['value'];


		$verify        =  $this->CapModel->posts_search_verify_inline($limit,$start,$search,$order,$dir,$date,$factory,$nik);

		$totalFiltered = $this->CapModel->posts_search_count_verify_inline($search,$date,$factory,$nik);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($verify))
        {
            foreach ($verify as $verify)
            {
				$_temp                      = '"'.$verify->cap_inline_id.'"';
				$major_couse='';
				$root_couse='';
				$action='';

				$this->db->where('delete_at is NULL', NULL,FALSE);
				$this->db->where('cap_inline_id', $verify->cap_inline_id);
				$cap_trans = $this->db->get('cap_inline_trans');
				foreach ($cap_trans->result() as $key => $trans) {

					$major_couse = $trans->major_couse;
					$root_couse = $trans->root_couse;
					$action = $trans->action;
				}

				$nestedData['no']          = (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['line']        = $verify->line_name;
				$nestedData['round']       = $verify->round;
				$nestedData['nama_sewer']  = $verify->sewer_name;
				$nestedData['proses_name'] = ucwords($verify->proses_name);
				$nestedData['nama_defect'] = $verify->defect_jenis;
				$nestedData['major_couse'] = ucwords($major_couse);
				$nestedData['root_couse']  = ucwords($root_couse);
				$nestedData['action']      = ucwords($action);
				$nestedData['status']      = ucwords($verify->status);
				
				if ($verify->max==$verify->round&&$verify->verify_date==NULL) {
				$nestedData['aksi']                                         = "<center><a onclick='return verify($_temp)' href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-pencil-square-o'></i> Verify</a> <a onclick='return perbaiki($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-pencil-square-o'></i> Perbaiki CAP</a></center>";
				}else if ($verify->verify_date!=NULL) {
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-check'></i> Telah Verify</a></center>";
				}else{
				$nestedData['aksi']                                         = "<center><a href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-times'></i> Expired</a></center>";
				}

                $data[] = $nestedData;

            }
        }


        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}
	public function verifyInline($get=NULL)
	{
		$get = $this->input->get();
		if ($get!=NULL) {
			$cap_id = $get['id_detail'];

			$cek = $this->db->query("SELECT * from cap_inline 
			LEFT JOIN (
			SELECT max(round) as max, line_id from round_status where date(create_date) = date(now())
			GROUP BY line_id ) x on x.line_id = cap_inline.line_id::VARCHAR
			where cap_inline_id = '$cap_id'
			and corective_action_date is null and verify_date is null
			")->row_array();

			// $cek = $this->db->get_where('cap_inline_view', array('cap_inline_id'=>$get['id_detail']))->row_array();

			if ($cek['round']==$cek['max']) {
				$this->db->where('cap_inline_id', $get['id_detail']);
				$this->db->update('cap_inline', array('verify_date'=>date('Y-m-d H:i:s'),'status'=>'cap verify'));
				echo "1";
			}else{
				echo "0";
			}

		}
	}
	public function searchNik($get=null)
	{
		$get = $this->input->get();

        if ($get!='') {

            $this->db->where('nik', $get['id']);
            $this->db->select('name,nik');
            $record=$this->db->get('master_sewer');

            if ($record->num_rows()>0) {

                $notif      ="1";
                $data = array('record' => $record->row_array(),'notif'=>$notif);

                echo json_encode($data);
            }else{
                $data['notif']="2";
                echo json_encode($data);
            }


        }
	}
	public function showrepairinline($get=NULL)
	{
		$get =$this->input->get();
		if ($get!=NULL) {
			$data['record']= $get['id'];
			$this->load->view('cap/show_repair_capinline',$data);
		}

	}
	public function showrepairendline($get=NULL)
	{
		$get =$this->input->get();
		if ($get!=NULL) {
			$data['record']= $get['id'];
			$this->load->view('cap/show_repair_capendline',$data);
		}

	}
	public function SubmitRepairCapInline($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$this->db->trans_start();
			$date = date('Y-m-d H:i:s');

			$this->db->where('cap_inline_id', $post['id']);
			$this->db->update('cap_inline', array('corective_action_date'=>NULL,'status'=>'perbaiki CAP','repair_date'=>$date,'repair_desc'=>$post['description']));

			$this->db->where('cap_inline_id', $post['id']);
			$this->db->update('cap_inline_trans', array('delete_at'=>$date));

			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
		        $status = 500;
		        $pesan = "insert Data Gagal";
			}else{
				$this->db->trans_complete();
				$status = 200;
				$pesan = "Perbaikan Cap Berhasil";
			}
			$data = array('pesan' => $pesan,'status'=>$status );
			echo json_encode($data);
		}
	}
	public function SubmitRepairCapEndline($post=NULL)
	{
		$post = $this->input->post();
		if ($post!=NULL) {
			$this->db->trans_start();
			$date = date('Y-m-d H:i:s');
			$date = date('Y-m-d H:i:s');
			$DateTime        = new DateTime();
			$DateTime->modify('+2 hours');
			$sewing_deadline =  $DateTime->format("Y-m-d H:i:s");

			$this->db->where('cap_endline_detail_id', $post['id']);
			$this->db->update('cap_endline_detail', array('corective_action_date'=>NULL,'status'=>'perbaiki CAP','sewing_deadline_date'=>$sewing_deadline,'repair_date'=>$date,'repair_desc'=>$post['description']));

			$this->db->where('cap_inline_id', $post['id']);
			$this->db->update('cap_inline_trans', array('delete_at'=>$date));

			if ($this->db->trans_status() == FALSE)
			{
				$this->db->trans_rollback();
		        $status = 500;
		        $pesan = "insert Data Gagal";
			}else{
				$this->db->trans_complete();
				$status = 200;
				$pesan = "Perbaikan Cap Berhasil";
			}
			$data = array('pesan' => $pesan,'status'=>$status );
			echo json_encode($data);
		}
	}

	public function report_cap()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Report CAP');
            $this->template->set('desc_page','Report CAP');
            $this->template->load('layout','cap/report_cap');
        }

	}

	public function summary_inline()
	{
		$columns = array(
							0 => 'civ.date',
							1 => 'civ.no_urut',
							2 => 'civ.line_id',
							3 => 'civ.round',
							8 => 'status',
                        );

		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');

		$date1         = $this->input->post('tanggal1');
		$date2         = $this->input->post('tanggal2');

  		if ($date1!='') {
  			$date1 = date('Y-m-d',strtotime($date1));
  		}else{
  			$date1 = date('Y-m-d');
		}

		if ($date2!='') {
			$date2 = date('Y-m-d',strtotime($date2));
		}else{
			$date2 = date('Y-m-d');
		}


		$totalData     = $this->CapModel->allposts_count_summary_inline($date1,$date2);
		$totalFiltered = $totalData;

		$search_line   = $this->input->post('search_line');

		if(empty($search_line))
		{
			$capinline = $this->CapModel->allposts_summary_inline($limit,$start,$order,$dir,$date1,$date2);
		}else {
			$capinline     = $this->CapModel->posts_search_summary_inline($limit,$start,$search_line,$order,$dir,$date1,$date2);
			$totalFiltered = $this->CapModel->posts_search_count_summary_inline($search_line,$date1,$date2);
        }
        $data = array();
		$nomor_urut = 0;
		$tgl = date('Y-m-d');
        if(!empty($capinline))
        {
            foreach ($capinline as $capinline)
            {
				$_temp = '"'.$capinline->cap_inline_id.'"';
				
				if($capinline->status!=NULL){
					$status_cap = $capinline->status;
				}else{
					if($tgl > $capinline->date){
						$status_cap = 'Expired';
					}
					else{
						if($capinline->max > $capinline->round){
							$status_cap = 'Expired';
						}
						else{
							$status_cap = 'Waiting on CAP';
						}
					}
				}

				$nestedData['date']           = $capinline->date;
				$nestedData['line']           = $capinline->line_name;
				$nestedData['round']          = $capinline->round;
				$nestedData['proses_name']    = ucwords(($capinline->proses_name!=NULL?$capinline->proses_name:"-"));
				$nestedData['defect_id']      = ucwords(($capinline->defect_jenis!=NULL?$capinline->defect_jenis:"-"));
				$nestedData['correct_action'] = $capinline->action;
				$nestedData['dibuat_kapan']   = ucwords(($capinline->dibuat_kapan!=NULL?$capinline->dibuat_kapan:"-"));
				$nestedData['dibuat_oleh']    = ucwords(($capinline->operator!=NULL?$capinline->operator:"-"));
				$nestedData['status']         = ucwords($status_cap);

                $data[] = $nestedData;

            }
        }


        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}

	public function download_inline()
    {
        $columns = array(
			0 => 'no_urut',
			1 => 'line_id',
			2 => 'round',
			8 => 'status'
		);

        $limit = NULL;
        $start = NULL;
        $order = NULL;
        $dir   = NULL;
        $draw  = $this->input->post('draw');

		
		$date1        = $_GET['dari'];
		$date2        = $_GET['sampai'];
		// $this->input->post('tanggal');
		$search_line = $_GET['search_line'];
		// $this->input->post('search_line');

		// var_dump($date);
		// var_dump($search_line);
		// die();

		// $date1         = $this->input->post('tanggal1');
		// $date2         = $this->input->post('tanggal2');

  		if ($date1!='') {
  			$date1 = date('Y-m-d',strtotime($date1));
  		}else{
  			$date1 = date('Y-m-d');
		}

		if ($date2!='') {
			$date2 = date('Y-m-d',strtotime($date2));
		}else{
			$date2 = date('Y-m-d');
		}

        $totalData     = $this->CapModel->allposts_count_summary_inline($date1,$date2);
            
        $totalFiltered = $totalData; 
            
        if(empty($search_line))
        {            
            $capinline = $this->CapModel->allposts_summary_inline($limit,$start,$order,$dir,$date1,$date2);
        }
        else {
			
            $capinline     = $this->CapModel->posts_search_summary_inline($limit,$start,$search_line,$order,$dir,$date1,$date2);
			$totalFiltered = $this->CapModel->posts_search_count_summary_inline($search_line,$date1,$date2);
		}
		
        $data = array();
		$nomor_urut = 0;
		$tgl = date('Y-m-d');
        if(!empty($capinline))
        {
            foreach ($capinline as $capinline)
            {
				$_temp = '"'.$capinline->cap_inline_id.'"';
				
				if($capinline->status!=NULL){
					$status_cap = $capinline->status;
				}else{
					if($tgl > $capinline->date){
						$status_cap = 'Expired';
					}
					else{
						if($capinline->max > $capinline->round){
							$status_cap = 'Expired';
						}
						else{
							$status_cap = 'Waiting on CAP';
						}
					}
				}

				$nestedData['date']           = $capinline->date;
				$nestedData['line']           = $capinline->line_name;
				$nestedData['round']          = $capinline->round;
				$nestedData['proses_name']    = ucwords(($capinline->proses_name!=NULL?$capinline->proses_name:"-"));
				$nestedData['defect_id']      = ucwords(($capinline->defect_jenis!=NULL?$capinline->defect_jenis:"-"));
				$nestedData['correct_action'] = $capinline->action;
				$nestedData['dibuat_kapan']   = ucwords(($capinline->dibuat_kapan!=NULL?$capinline->dibuat_kapan:"-"));
				$nestedData['dibuat_oleh']    = ucwords(($capinline->operator!=NULL?$capinline->operator:"-"));
				$nestedData['status']         = ucwords($status_cap);

                $baris = '<tr>
					<td class="text-center">'.$nestedData['date'].'</td>
					<td class="text-center">'.$nestedData['line'].'</td>
					<td class="text-center">'.$nestedData['round'].'</td>
					<td class="text-center">'.$nestedData['proses_name'].'</td>
					<td class="text-center">'.$nestedData['defect_id'].'</td>
					<td class="text-center">'.$nestedData['correct_action'].'</td>
					<td class="text-center">'.$nestedData['dibuat_kapan'].'</td>
					<td class="text-center">'.$nestedData['dibuat_oleh'].'</td>
					<td class="text-center">'.$nestedData['status'].'</td>
					</tr>';
				$data[] = $baris;

            }
        }
          
        $json_data = array(
                    // "draw"            => intval($this->input->post('draw')),  
                    // "recordsTotal"    => intval($totalData),  
                    // "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
					);

						// var_dump($data);
						// die();
					// var_dump($json_data['data'][0]['qc']);
					// var_dump($json_data['data'][0]['create_date']);
					// die();
		$this->load->view("cap/cap_download_inline",$json_data);
	}

	public function filter_cap($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$tgl1    = $post['tanggal1'];
			$tgl2    = $post['tanggal2'];
			$line    = $post['line'];
			$laporan = $post['laporan'];

			$data = array (
				'tgl1'    => $tgl1,
				'tgl2'    => $tgl2,
				'line'    => $line,
				'laporan' => $laporan,
			);
			// $cekHeader = $this->QcEndlineModel->cekHeader($header_id,$size_id);
			if ($laporan == 0) {
				$this->load->view("cap/report_cap_inline",$data);
			}
			else{
				$this->load->view("cap/report_cap_endline",$data);
			}

			// echo "sukses";
		}
	}

	public function summary_endline()
	{
		$columns = array(
							0 => 'date',
							1 => 'no_urut',
							2 => 'line_id',
							3 => 'jam',
							8 => 'status'
                        );

		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');

		$date1         = $this->input->post('tanggal1');
		$date2         = $this->input->post('tanggal2');

  		if ($date1!='') {
  			$date1 = date('Y-m-d',strtotime($date1));
  		}else{
  			$date1 = date('Y-m-d');
		}

		if ($date2!='') {
			$date2 = date('Y-m-d',strtotime($date2));
		}else{
			$date2 = date('Y-m-d');
		}

		$totalData     = $this->CapModel->allposts_count_summary_endline($date1,$date2);
		$totalFiltered = $totalData;

		$search_line   = $this->input->post('search_line');

		if(empty($search_line))
		{
			$capendline = $this->CapModel->allposts_summary_endline($limit,$start,$order,$dir,$date1,$date2);
		}else {
			$capendline    = $this->CapModel->posts_search_summary_endline($limit,$start,$search_line,$order,$dir,$date1,$date2);
			$totalFiltered = $this->CapModel->posts_search_count_summary_endline($search_line,$date1,$date2);
        }
        $data = array();
		$nomor_urut = 0;
		$jam = date('Y-m-d H:i:s');
        if(!empty($capendline))
        {
            foreach ($capendline as $capendline)
            {
				$_temp = '"'.$capendline->cap_endline_id.'"';
				
				if($capendline->status!=NULL){
					$status_cap = $capendline->status;
				}else{
					if($jam > $capendline->date){
						$status_cap = 'Expired';
					}
					else{
						if($capendline->sewing_deadline_date > $jam){
							$status_cap = 'Waiting on CAP';
						}
						else{
							$status_cap = 'Expired';
						}
					}
				}

				$nestedData['date']           = $capendline->date;
				$nestedData['line']           = $capendline->line_name;
				$nestedData['jam']            = $capendline->jam;
				$nestedData['wft']            = $capendline->wft;
				$nestedData['defect_id']      = ucwords(($capendline->defect_jenis!=NULL?$capendline->defect_jenis:"-"));
				$nestedData['major_couse']    = ucwords(($capendline->major_couse!=NULL?$capendline->major_couse:"-"));
				$nestedData['root_couse']     = ucwords(($capendline->root_couse!=NULL?$capendline->root_couse:"-"));
				$nestedData['correct_action'] = $capendline->action;
				$nestedData['dibuat_kapan']   = ucwords(($capendline->dibuat_kapan!=NULL?$capendline->dibuat_kapan:"-"));
				$nestedData['dibuat_oleh']    = ucwords(($capendline->operator!=NULL?$capendline->operator:"-"));
				$nestedData['status']         = ucwords($status_cap);

                $data[] = $nestedData;

            }
        }


        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}

	public function download_endline()
    {
        $columns = array(
			0 => 'no_urut',
			1 => 'line_id',
			2 => 'jam',
			8 => 'status'
		);

        $limit = NULL;
        $start = NULL;
        $order = NULL;
        $dir   = NULL;
        $draw  = $this->input->post('draw');

		
		$date1        = $_GET['dari'];
		$date2        = $_GET['sampai'];
		// $this->input->post('tanggal');
		$search_line = $_GET['search_line'];
		// $this->input->post('search_line');

		// var_dump($date);
		// var_dump($search_line);
		// die();

		// $date1         = $this->input->post('tanggal1');
		// $date2         = $this->input->post('tanggal2');

  		if ($date1!='') {
  			$date1 = date('Y-m-d',strtotime($date1));
  		}else{
  			$date1 = date('Y-m-d');
		}

		if ($date2!='') {
			$date2 = date('Y-m-d',strtotime($date2));
		}else{
			$date2 = date('Y-m-d');
		}


        $totalData     = $this->CapModel->allposts_count_summary_endline($date1,$date2);
		$totalFiltered = $totalData;
            
        if(empty($search_line))
		{
			$capendline = $this->CapModel->allposts_summary_endline($limit,$start,$order,$dir,$date1,$date2);
		}else {
			$capendline     = $this->CapModel->posts_search_summary_endline($limit,$start,$search_line,$order,$dir,$date1,$date2);
			$totalFiltered = $this->CapModel->posts_search_count_summary_endline($search_line,$date1,$date2);
        }
		
        $data = array();
		$nomor_urut = 0;
		$jam = date('Y-m-d H:i:s');
        if(!empty($capendline))
        {
            foreach ($capendline as $capendline)
            {
				$_temp = '"'.$capendline->cap_endline_id.'"';
				
				if($capendline->status!=NULL){
					$status_cap = $capendline->status;
				}else{
					if($jam > $capendline->date){
						$status_cap = 'Expired';
					}
					else{
						if($capendline->sewing_deadline_date > $jam){
							$status_cap = 'Waiting on CAP';
						}
						else{
							$status_cap = 'Expired';
						}
					}
				}

				$nestedData['date']           = $capendline->date;
				$nestedData['line']           = $capendline->line_name;
				$nestedData['jam']            = $capendline->jam;
				$nestedData['wft']            = $capendline->wft;
				$nestedData['defect_id']      = ucwords(($capendline->defect_jenis!=NULL?$capendline->defect_jenis:"-"));
				$nestedData['major_couse']    = ucwords(($capendline->major_couse!=NULL?$capendline->major_couse:"-"));
				$nestedData['root_couse']     = ucwords(($capendline->root_couse!=NULL?$capendline->root_couse:"-"));
				$nestedData['correct_action'] = $capendline->action;
				$nestedData['dibuat_kapan']   = ucwords(($capendline->dibuat_kapan!=NULL?$capendline->dibuat_kapan:"-"));
				$nestedData['dibuat_oleh']    = ucwords(($capendline->operator!=NULL?$capendline->operator:"-"));
				$nestedData['status']         = ucwords($status_cap);

                $baris = '<tr>
					<td class="text-center">'.$nestedData['date'].'</td>
					<td class="text-center">'.$nestedData['line'].'</td>
					<td class="text-center">'.$nestedData['jam'].'</td>
					<td class="text-center">'.$nestedData['wft'].'</td>
					<td class="text-center">'.$nestedData['defect_id'].'</td>
					<td class="text-center">'.$nestedData['major_couse'].'</td>
					<td class="text-center">'.$nestedData['root_couse'].'</td>
					<td class="text-center">'.$nestedData['correct_action'].'</td>
					<td class="text-center">'.$nestedData['dibuat_kapan'].'</td>
					<td class="text-center">'.$nestedData['dibuat_oleh'].'</td>
					<td class="text-center">'.$nestedData['status'].'</td>
					</tr>';
				$data[] = $baris;

            }
        }
          
        $json_data = array(
                    // "draw"            => intval($this->input->post('draw')),  
                    // "recordsTotal"    => intval($totalData),  
                    // "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
					);

						// var_dump($data);
						// die();
					// var_dump($json_data['data'][0]['qc']);
					// var_dump($json_data['data'][0]['create_date']);
					// die();
		$this->load->view("cap/cap_download_endline",$json_data);
	}

}

/* End of file Cap.php */
/* Location: ./application/controllers/Cap.php */
