<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Header_inline extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		chek_session();
		$this->load->model(array('Inline_header_model','Cekpermision_model'));
        date_default_timezone_set('Asia/Jakarta');
		$CI = &get_instance();
		$this->fg = $CI->load->database('fg',TRUE);
	}
    
	public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Master Header Inline');
            $this->template->set('desc_page','Master Header Inline');
            $this->template->load('layout','master/header_inline/list_header_inline');
        }
		
	}
	public function loadporeference()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('master/header_inline/list_poreference');
        }		
	}
    public function loadline()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('master/header_inline/list_line');
        }
        
    }
	public function addHeader()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Master Header Inline');
            $this->template->set('desc_page','Master Header Inline');
            $this->template->load('layout','master/header_inline/create_header_line');
            // $this->load->view('master/header_inline/add_header_inline');
        }
		
	}
	public function list_header()
	{
		
		$columns = array( 
                            0 =>'startdateactual', 
                            1 =>'poreference',
                            2 =>'style',
                            3 =>'article'
                        );
		$factory = $this->session->userdata('factory');
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');

  
        $totalData = $this->Inline_header_model->allposts_count($factory);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Inline_header_model->allposts($limit,$start,$order,$dir,$factory);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Inline_header_model->posts_search($limit,$start,$search,$order,$dir,$factory);

            $totalFiltered = $this->Inline_header_model->posts_search_count($search,$factory);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
                $line_name = $this->db->get_where ('master_line', array('master_line_id'=>$master->line_id))->result()[0]->line_name;
                $_temp = '"'.$master->inline_header_id.'"';
            	$_new = '"'.$master->inline_header_id.'","'.$master->poreference.'","'.$master->style.'"';
                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['poreference'] = ucwords($master->poreference);
                $nestedData['style'] = $master->style;
                $nestedData['article'] = $master->article;
                $nestedData['line_name'] = ucwords($line_name);
                $nestedData['qtyorder'] = $master->targetqty;
                $nestedData['detail'] = "<center><a onclick='return detail($_new)' href='javascript:void(0)'>Detail</a></center>";
                $nestedData['action'] = "<center><a onclick='return daily($_new)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-tag'></i> Daily Data</a><a onclick='return Close($_temp)' href='javascript:void(0)' class='btn btn-danger'><i class='fa fa-times'></i> Close</a></center>";                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
	public function addpobuyer()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Add Header Inline');
            $this->template->set('desc_page','Add Header Inline');
            $this->template->load('layout','master/header_inline/add_header_inline');
        }
		
	}
	public function listpoajax()
	{
		$columns = array( 
                            0 =>'poreference', 
                            1 =>'style',
                            // 2 =>'article',
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');

        $factory = $this->session->userdata('factory');

  
        $totalData = $this->Inline_header_model->allposts_count_list();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Inline_header_model->allposts_list($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Inline_header_model->posts_search_list($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Inline_header_model->posts_search_count_list($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
                // $_temp = '"'.$master->poreference.'","'.trim($master->style).'","'.$master->kst_articleno.'"';
            	$_temp = '"'.$master->poreference.'","'.trim($master->style).'","'.$master->kst_articleno.'"';
                $nestedData['no']          = (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['poreference'] = ucwords($master->poreference);
                $nestedData['style']       = $master->style;
                $nestedData['article']     = $master->kst_articleno;
                $nestedData['action']      = "<center><a onclick='return select($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";                
                $data[] = $nestedData;

            }
        }
        
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}

    public function listline()
    {
        $columns = array( 
                            0 =>'mesin_id', 
                            1 =>'line_name',
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');
        $factory = $this->session->userdata('factory');

  
        $totalData = $this->Inline_header_model->allposts_count_line($factory);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Inline_header_model->allposts_line($limit,$start,$order,$dir,$factory);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Inline_header_model->posts_search_line($limit,$start,$search,$order,$dir,$factory);

            $totalFiltered = $this->Inline_header_model->posts_search_count_line($search,$factory);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
                $_temp = '"'.$master->master_line_id.'","'.trim($master->line_name).'"';
                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['line_name'] = ucwords($master->line_name);
                $nestedData['action'] = "<center><a onclick='return select($_temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-check-square'></i> Select</a></center>";                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }
    public function addheader_submit($poreference=NULL,$style=NULL,$line_id=NULL,$detailsize=NULL)
    {
        
        $nik             = $this->session->userdata('nik');
        $factory         = $this->session->userdata('factory');
        $poreference     = $this->input->post('poreference',TRUE);
        $style           = $this->input->post('style',TRUE);
        $line_id         = $this->input->post('line_id',TRUE);
        $startdateactual = $this->input->post('startdateactual',TRUE);
        $co_category     = $this->input->post('co',TRUE);
        $detailsize      = $this->input->post('detailsize');
        $article      = $this->input->post('article');
        $_cek =$this->db->get_where('inline_header',array('line_id'=>$line_id,'poreference'=>$poreference,'style'=>$style,'article'=>$article,'isactive'=>'t','factory_id'=>$factory));
        $i =0;
        if ($poreference!=NULL) {
            if ($_cek->num_rows()==0) {
                $this->db->where('line_id', $line_id);
                $this->db->where('delete_at is null');
                $history_id = $this->db->get('history_line')->row_array()['history_id'];
            // insert ke table header inline
                $detail = array(
                    'inline_header_id' => $this->uuid->v4(), 
                    'poreference'      => $poreference, 
                    'style'            => $style, 
                    'article'          => $article, 
                    'line_id'          => $line_id, 
                    'startdateactual'  => $startdateactual,
                    'co_category'      => $co_category, 
                    'factory_id'       => $factory, 
                    'history_id'       => $history_id, 
                    'create_by'        => $nik 
                );
                $this->db->insert('inline_header', $detail);
                // end insert header inline
                $this->db->where(array('line_id'=>$line_id,'poreference'=>$poreference,'style'=>$style,'isactive'=>'t'));
                $header_id = $this->db->get('inline_header')->result()[0]->inline_header_id;
                // insert ke table inline_header_size
                foreach ($detailsize as $detail) {
                    list($size, $qty)             = explode(';', $detail);
                    $data[$i]['header_size_id']   = $this->uuid->v4();
                    $data[$i]['inline_header_id'] = $header_id;
                    $data[$i]['size']             = $size;
                    $data[$i]['qty']              = $qty;
                    $data[$i]['poreference']      = $poreference;
                    $data[$i]['style']            = $style;
                    $data[$i]['create_by']        = $nik;
                    $i++;
                    
                } 
                
                $this->db->insert_batch('inline_header_size', $data);
                // end insert inline_header_size            
                
            }else{            
                $header_id = $_cek->row_array()['inline_header_id'];
                // insert ke table inline_header_size
                $data = array();
                foreach ($detailsize as $detail) {
                    list($size, $qty)             = explode(';', $detail);

                    $cek_size = $this->db->get_where('inline_header_size',array('inline_header_id'=>$header_id,'size'=>$size));
                    
                    foreach ($cek_size->result() as $key => $ck) {
                        $cek = $this->db->get_where('inline_header_size',array('inline_header_id'=>$header_id,'size'=>$ck->size));
                        
                       if ($cek->num_rows()>0) {
                            $qty = $ck->qty+$qty;
                            $this->db->where('inline_header_id', $header_id);
                            $this->db->where('size', $ck->size);
                            $this->db->update('inline_header_size', array('qty'=>$qty));                            
                        }
                    } 
                }
                foreach ($detailsize as $detail) {

                    list($size, $qty)             = explode(';', $detail);
                    $cek_size = $this->db->get_where('inline_header_size',array('inline_header_id'=>$header_id,'size'=>$size));
                    if ($cek_size->num_rows()==0) {
                        $data[$i]['header_size_id']   = $this->uuid->v4();
                        $data[$i]['inline_header_id'] = $header_id;
                        $data[$i]['size']             = $size;
                        $data[$i]['qty']              = $qty;
                        $data[$i]['poreference']      = $poreference;
                        $data[$i]['style']            = $style;
                        $data[$i]['create_by']        = $nik;
                        $i++;  
                    }
                                                  
                }
                
                if (count($data)>0) {
                    $this->db->insert_batch('inline_header_size', $data);
                }
                  
            }
           
            $targetqty = $this->Inline_header_model->get_sum_inline_size($header_id)['qty'];

            $this->db->where(array('line_id'=>$line_id,'poreference'=>$poreference,'style'=>$style));
            $this->db->update('inline_header', array('targetqty'=>$targetqty));
            echo "sukses";    
        }   
    }
    public function inactivesubmit($id=NULL)
    {
        $id     = $_GET['id'];
        $nik    = $this->session->userdata('nik');
        $date   = date('Y-m-d H:i:s');
        $this->db->where('inline_header_id', $id);
        $cekQcInline = $this->db->get('inline_detail');

        $this->db->where('inline_header_id', $id);
        $cekQcEndline = $this->db->get('qc_endline');

        if ($cekQcInline->num_rows()==0&& $cekQcEndline->num_rows()==0){
            $this->db->where('inline_header_id', $id);
            $this->db->update('inline_header', array('isactive'=>'f','delete_at'=>$date,'delete_by'=>$nik));

            $this->db->where('inline_header_id', $id);
            $this->db->delete('inline_header_size');
            echo "sukses";
        }else{
            echo "gagal";
        }
        
    }
    public function getdetailstyle($style=NULL,$po=NULL)
    {
        $style   = $this->input->get('style');
        $po      = $this->input->get('po');
        $article = $this->input->get('article');
        

        $where = array(
            'style'         =>$style , 
            'kst_articleno' =>$article , 
            'poreference'   =>$po
        );

        $master_mo = $this->Inline_header_model->get_detail_mo($where)->row_array();

        // $master_mo = $this->Inline_header_model->get_detail_header($where)->row_array();
        
        $qtyorder = $this->Inline_header_model->get_sum_qtyorder($where)['qty_ordered'];
       
        
        $details  = $this->Inline_header_model->get_detail_mo($where);
        
        $draw     = $this->get_size($details);

            $data = array(
                'master_mo'   =>$master_mo , 
                'draw'   =>$draw , 
                'sumqtyorder' =>$qtyorder
            );
        

            echo json_encode($data);        

    }
    private function get_size($details)
    {
        $draw = "<table class='table table-striped table-bordered table-hover table-full-width'>
                        <thead>
                            <tr>
                                <th width='20px'>#</th>
                                <th>SIZE</th>
                                <th>QTY</td>
                                <th width='100px'>ACTION</td>
                            </tr>
                        </thead>
                        <tbody>";
            $no=1;
            
            foreach ($details->result() as $key=> $detail) {
               $_temp = '"'.$detail->size.'","'.$detail->qty_ordered.'"';
                $draw .= '<tr>'.
                    '<td><input type="checkbox" name="detailsize[]" id="check_'.$detail->size.'" value="'.$detail->size.";".$detail->balance.'"checked></td>'.
                    '<td>'.$detail->size.'</td>'.
                    '<td><input type="text" value="'.$detail->balance.'" id="qtyordered_'.$detail->size.'" class="form-control" style="background-color: inherit" disabled></td>'.
                    "<td><a onclick='return editsize($_temp,1)' href='javascript:void(0)' class='btn btn-primary edit_".$detail->size."''><i class='fa fa-pencil-square-o'></i></a><a onclick='return editsize($_temp,2)' style='display: none' href='javascript:void(0)' class='btn btn-success edit_".$detail->size."''><i class='fa fa-save'></i></a><a onclick='return editsize($_temp,3)' style='display: none' href='javascript:void(0)' class='btn btn-danger edit_".$detail->size."'><i class='fa fa-times'></i></a></td>".
                    
                '</tr>';

            }

            $draw .= '</tbody></table>';
            return $draw;
    }
    function loadcreatedaily($id=0)
    {
       $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $id   = $this->input->get('id');
            $date = date('Y-m-d');
            if ($id!=0) {
                $this->db->where('inline_header_id', $id);
                $header = $this->db->get('inline_header')->row_array();
                                
                $this->db->where(array('inline_header_id'=>$id,'today'=>$date));
                $linedetail = $this->db->get('line_daily')->row_array();
                
                $data = array(
                    'header'     => $header,
                    'linedetail' => $linedetail
                      );
                $this->load->view('master/header_inline/create_daily_data',$data);
            }
            
        }   
    }
    public function createdaily_submit($id=NULL,$present=NULL,$working_hours=NULL,$today=NULL)
    {
        $id            = $this->input->post('inline_header_id');
        $present       = $this->input->post('present');
        $working_hours = $this->input->post('working_hours');
        $today         = $this->input->post('today');
        $nik           = $this->session->userdata('nik');

        $this->db->where(array('today'=>$today,'inline_header_id'=>$id));
        $cekData = $this->db->get('line_daily');
        if ($cekData->num_rows()==0) {
            $detail = array(
                'inline_header_id' => $id, 
                'present_sewer'    => $present, 
                'working_hours'    => $working_hours, 
                'today'            => $today, 
                'create_by'        => $nik 
            );
            $this->db->insert('line_daily', $detail);

            $this->db->where('inline_header_id',$id);
            $co = $this->db->get('line_daily');
            $this->db->where('inline_header_id',$id);
            $this->db->update('inline_header',array('cumulative_day'=>$co->num_rows()));
            echo "sukses";
        }else{
            $detail = array(
                'present_sewer'    => $present, 
                'working_hours'    => $working_hours, 
                'today'            => $today, 
                'create_by'        => $nik 
            );
            $this->db->where('inline_header_id',$id);
            $this->db->update('line_daily', $detail);
            echo "sukses";
        }
    }
    public function list_detail_po($get=NULL)
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $get = $this->input->get();
            
            if ($get!=NULL) {
                $where = array(
                    'inline_header_id' => $get['id'], 
                    'poreference'      => $get['po'] 
                );
                $this->db->where($where);
                $data['records'] = $this->db->get('inline_header_size');

                $this->load->view('master/header_inline/list_detail_po',$data); 
            }
                       
        }  
    }

    public function edit_qty_submit($post=NULL)
    {
        $post = $this->input->post();

        if ($post!=NULL) {
            $where = array(
                'inline_header_id' => $post['id'], 
                'size'             => $post['size'] 
            ); 
            $this->db->where($where);
            $size_id = $this->db->get('inline_header_size')->row_array()['header_size_id'];
            
            $this->db->where(array('inline_header_id'=>$post['id'],'header_size_id'=>$size_id));
            $cekqc = $this->Inline_header_model->cekQcEndline($post['id'],$size_id);
            
            if ($post['qty']>=$cekqc) {
                $this->db->where($where);  
                $this->db->update('inline_header_size', array('qty'=>$post['qty']));

                $targetqty = $this->Inline_header_model->get_sum_inline_size($post['id'])['qty'];

                $this->db->where('inline_header_id', $post['id']);
                $this->db->update('inline_header',array('targetqty'=>$targetqty));
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $status = 500;
                }else{
                    $this->db->trans_complete();
                    $status = 200;
                }
            }else{
                $status = 500;
            } 
            echo $status;
        }
    }
    public function deleteaction($post=NULL)
    {
        $post = $this->input->get();
        if ($post!=NULL) {
            $where = array(
                'inline_header_id' => $post['id'], 
                'size'             => $post['size'] 
            );
            $this->db->where($where);
            $size_id = $this->db->get('inline_header_size')->row_array()['header_size_id'];

            $this->db->where(array('inline_header_id'=>$post['id'],'header_size_id'=>$size_id));
            $cekqc = $this->db->get('qc_endline');
            if ($cekqc->num_rows()==0) {
                $this->db->trans_start();
                $this->db->where($where);
                $this->db->delete('inline_header_size');

                $targetqty = $this->Inline_header_model->get_sum_inline_size($post['id'])['qty'];

                $this->db->where('inline_header_id', $post['id']);
                $this->db->update('inline_header',array('targetqty'=>$targetqty));
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $status = 500;
                }else{
                    $this->db->trans_complete();
                    $status = 200;
                }
             }else{
                $status = 500;
             } 
            

            echo $status;
        }
    }
    
    public function findpo()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Laporan');
            $this->template->set('desc_page','Laporan');
            $this->template->load('layout','track_line/laporan_view');
        }

	} 

    public function filter_find_po($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$po      = $post['po'];

			$data = array (
				'po'      => $po,
			);
            
            $this->load->view("track_line/result_pencarian",$data);
			
		}
    }
    
    public function hasil_pencarian_ajax($post=NULL)
	{
		$columns = array(
                            0 => 'line_id',
                            1 => 'poreference',
                            2 => 'style',
                            3 => 'article',
                            4 => 'size'
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
			// $tanggal = $this->input->post('tanggal');
			// $dari    = $this->input->post('dari');
			// $sampai  = $this->input->post('sampai');
			$pobuyer = $this->input->post('pobuyer');

			$factory = $this->session->userdata('factory');

            // $query = $this->db->query("SELECT qo.*, ml.line_name, ml.no_urut from qc_output qo
            // LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
            // where poreference like '%$pobuyer%' and qo.factory_id = '$factory'
            // ORDER BY no_urut,poreference,style,size");
            
            // $query = $this->db->query("SELECT sum(qty_order) as total,qo.line_id,qo.poreference,qo.style, qo.article,ml.line_name,ml.no_urut from qc_output qo
            //     LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
            //     where poreference like '%$pobuyer%' and qo.factory_id = '$factory'
            //     GROUP BY qo.line_id,qo.poreference,qo.style,qo.article,ml.line_name,ml.no_urut
            //     ORDER BY qo.poreference,qo.style,qo.article,ml.no_urut
            // ");

            $query = $this->db->query("SELECT sum(qty_order) as loading, sum(qc_output) as output ,qo.line_id,qo.poreference,qo.style, qo.article,ml.line_name,ml.no_urut from qc_output qo
                LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
                where poreference like '%$pobuyer%' and qo.factory_id = '$factory'
                GROUP BY qo.line_id,qo.poreference,qo.style,qo.article,ml.line_name,ml.no_urut
                ORDER BY qo.poreference,qo.style,qo.article,ml.no_urut
            ");

            $totalData = $query->num_rows();

            $totalFiltered = $totalData;
            
            $master = $query->result();
			
			$data = array();
				        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	        	
	            foreach ($master as $master)
	            {
                    $temp = '"'.$master->line_id.'","'.$master->poreference.'","'.$master->style.'","'.$master->article.'"';
					
					// $nestedData['date']        = $master->date;
					$nestedData['line']        = $master->line_name;
					$nestedData['poreference'] = $master->poreference;
					$nestedData['style']       = $master->style;
					$nestedData['article']     = $master->article;
					// $nestedData['total']       = $master->total;
                    $nestedData['loading']       = $master->loading;
                    $nestedData['output']       = $master->output;
                    $nestedData['balance']       = $master->output - $master->loading;
					// $nestedData['grandtotal']  = $grandTotal->sum;
					$nestedData['action']      = "<center><a onclick='return detail($temp)' href='javascript:void(0)' class='btn btn-success'><i class='fa fa-align-justify'></i> Detail</a></center>";
					// $nestedData['action']      = $master->status;
					$data[]            		   = $nestedData;
				}
				   
	        }
	       
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
    }
    
    public function detail_track($get=NULL)
	{
		$get = $this->input->get();

		if ($get!=NULL) {
			// $po   = $get['po'];
			// $size = $get['size'];
			
	
			$article = $get['article'];
			$line    = $get['line'];
			$style   = $get['style'];
			$pobuyer = $get['po'];

			$data = array(
				'poreference' => $pobuyer,
				'style'       => $style,
				'article'     => $article,
				'line'        => $line
			);

			// var_dump($data);
			// die();
			// $data['list_karton']	= $this->FoldingModel->output_folding_detail($po,$size);
			$this->load->view('track_line/detail_track',$data);			
		}
    }

    public function detail_track_ajax($post=NULL)
	{
		$columns = array(
                            0 => 'size',
                        );
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');
		
		$article = $this->input->post('article');
		$line    = $this->input->post('line');
		$style   = $this->input->post('style');
		$pobuyer = $this->input->post('pobuyer');

		$factory = $this->session->userdata('factory');

        $query = $this->db->query("SELECT qo.*, ml.line_name, ml.no_urut from qc_output qo
        LEFT JOIN master_line ml on ml.master_line_id = qo.line_id
        where poreference like '%$pobuyer%' and line_id = '$line' and qo.factory_id = '$factory'
        ORDER BY no_urut,poreference,style,size");

        $totalData = $query->num_rows();

        $totalFiltered = $totalData;

        $master = $query->result();

		$data = array();
		
		
		$nomor_urut = 0;
		if(!empty($master))
		{	
			
			foreach ($master as $master)
			{
				$nestedData['size']           = $master->size;
				// $nestedData['qty']            = $master->qty_order;
                $nestedData['loading']            = $master->qty_order;
                $nestedData['output']            = $master->qc_output;
                $nestedData['balance']            = $master->balance;
				$data       []                = $nestedData;
			}
				
		}
		
		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		
		echo json_encode($json_data);
    }
    
//=============================================track karton ===============================================

    public function findkarton()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Track Karton');
            $this->template->set('desc_page','Track Karton');
            $this->template->load('layout','track_karton/laporan_view');
        }

	}

    public function filter_find_karton($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$po    = $post['po'];
			$size  = $post['size'];
			$rasio = $post['rasio'];

			$data = array (
				'po'    => $po,
				'size'  => $size,
				'rasio' => $rasio,
            );
            
            $this->load->view("track_karton/result_pencarian",$data);
			
		}
    }
    
    public function find_karton_line_ajax($post=NULL)
	{
		$columns = array(
                            0 => 'barcode_id',
                            1 => 'poreference',
                            2 => 'size',
                            3 => 'total',
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');
	        
            $pobuyer = $this->input->post('pobuyer');
            $size    = $this->input->post('size');
            $rasio   = $this->input->post('rasio');

            $factory = $this->session->userdata('factory');
            
            // if($rasio == 't'){
                // $sql = "SELECT fpb.barcode_id, fpb.create_date, fpb.create_by, fpb.status, fpb.line_id, x.jumlah, x.inner_pack from folding_package_barcode fpb 
                // LEFT JOIN (
                // select count(*) as jumlah,fp.inner_pack,style,barcode_package from folding fo
                // LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
                // where fo.poreference='$pobuyer'";
            // }
            // else{
                
                $sql = "SELECT fpb.barcode_id, fpb.create_date, fpb.create_by, fpb.status, fpb.line_id, COALESCE(x.jumlah,0) as jumlah, fpp.inner_pack from folding_package_barcode fpb 
                LEFT JOIN (
                select count(*) as jumlah,fp.inner_pack,style,barcode_package from folding fo
                LEFT JOIN folding_package fp on fp.scan_id=fo.scan_id
                where fo.poreference='$pobuyer'";
            // }

            if($size!=''){            
                $sql .= "and fo.size='$size'
                GROUP BY fp.inner_pack,barcode_package,style
                ORDER BY barcode_package) x on x.barcode_package =  fpb.barcode_id 
                LEFT JOIN folding_package fpp on fpp.scan_id = fpb.scan_id
                where fpp.scan_id in (
                SELECT scan_id from folding_package WHERE poreference = '$pobuyer' and size = '$size'
                )
                ORDER BY jumlah";
            }
            else{
                $sql .= "
                GROUP BY fp.inner_pack,barcode_package,style
                ORDER BY barcode_package) x on x.barcode_package =  fpb.barcode_id 
                LEFT JOIN folding_package fpp on fpp.scan_id = fpb.scan_id
                where fpp.scan_id in (
                SELECT scan_id from folding_package WHERE poreference = '$pobuyer'
                )
                ORDER BY jumlah";
            }

            $query = $this->db->query($sql);

            $totalData = $query->num_rows();

            $totalFiltered = $totalData;
            
            $master = $query->result();

			$data = array();
				        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	            foreach ($master as $master)
	            {
					$nestedData['barcode_id']  = $master->barcode_id;
					$nestedData['poreference'] = $pobuyer;
					$nestedData['status']      = $master->status;
					$nestedData['size']        = $size;
					$nestedData['total']       = $master->jumlah;
					$nestedData['inner']       = $master->inner_pack;
					$data      []              = $nestedData;
				}
				   
	        }
	  
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
    }

    public function find_karton_packing_ajax($post=NULL)
	{
		$columns = array(
                            0 => 'barcode_id',
                            1 => 'poreference',
                            2 => 'size',
                            3 => 'status',
                        );
	        $limit = $this->input->post('length');
	        $start = $this->input->post('start');
	        $order = $columns[$this->input->post('order')[0]['column']];
	        $dir   = $this->input->post('order')[0]['dir'];
	        $draw  = $this->input->post('draw');

            $pobuyer = $this->input->post('pobuyer');
            $size    = $this->input->post('size');
            $rasio   = $this->input->post('rasio');

            $factory = $this->session->userdata('factory');
            
            
            $sql = "SELECT
                pk.barcode_id,
                pd.po_number,
                pd.customer_size,
                pd.manufacturing_size,
                pd.inner_pack,
                pk.current_department,
                pk.current_status 
            FROM
                package_detail pd
                JOIN package pk ON pd.scan_id = pk.scan_id 
            WHERE
                pk.current_department = 'preparation' 
                AND pd.deleted_at IS NULL 
                AND pk.deleted_at IS NULL
                AND pd.po_number = '$pobuyer'";

            if($size!=''){            
                $sql .= "and pd.manufacturing_size='$size' ";
            }

            $sql .= "ORDER BY barcode_id";

            $query = $this->fg->query($sql);

            $totalData = $query->num_rows();

            $totalFiltered = $totalData;
            
            $master = $query->result();
			
			$data = array();
				        
	        $nomor_urut = 0;
	        if(!empty($master))
	        {	
	            foreach ($master as $master)
	            {
					$nestedData['barcode_id']  = $master->barcode_id;
					$nestedData['poreference'] = $master->po_number;
					$nestedData['size']        = $master->manufacturing_size;
					$nestedData['inner']       = $master->inner_pack;;
					$nestedData['status']      = $master->current_department;
					$data      []              = $nestedData;
				}
				   
	        }
	  
	        $json_data = array(
	                    "draw"            => intval($this->input->post('draw')),
	                    "recordsTotal"    => intval($totalData),
	                    "recordsFiltered" => intval($totalFiltered),
	                    "data"            => $data
	                    );
	       
	        echo json_encode($json_data);
    }
    
}

/* End of file header_inline.php */
/* Location: ./application/controllers/header_inline.php */