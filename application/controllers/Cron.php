<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$CI = &get_instance();
		date_default_timezone_set('Asia/Jakarta');
    	$this->db2 = $CI->load->database('erp',TRUE);
    	$this->db3 = $CI->load->database('absen',TRUE);
		$this->load->model('Cron_model');
		$this->load->model('Cekpermision_model');
		$this->load->model('QcEndlineModel');
		$this->load->model('ReportModel');
	}

	public function get_style()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$this->db2->distinct();
		$this->db2->select('style,poreference,kst_lcdate');
		$this->db2->where('to_char(kst_lcdate,\'YYYY-mm\')','2018-03');
		$_get = $this->db2->get('adt_so');
		foreach ($_get->result() as $get) {
			$this->db->where('style', $get->style);
			$this->db->where('poreference', $get->poreference);
			$_cek = $this->db->get('master_style');
			if ($_cek->num_rows()==0) {
				$detail = array(
					'master_style_id'   => $this->uuid->v4(),
					'style'				=>	$get->style,
					'poreference'		=>	$get->poreference,
					'kst_lcdate'		=>	$get->kst_lcdate,
				);
				$this->db->trans_start();
				$this->db->insert('master_style', $detail);
				$this->db->trans_complete();
			}	
		}
	}
	
	public function get_sewer()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$this->db->trans_start();
		// $this->db->truncate('master_sewer ');
		$nik = array('11111111', '22222222');
		$this->db->where_not_in('nik', $nik);
		$this->db->delete('master_sewer');
		$_get_sewer = $this->db3->get('get_employee');
		foreach ($_get_sewer->result() as $get) {
			$this->db->where('nik', $get->nik);
			$_cek = $this->db->get('master_sewer');
			if($_cek->num_rows()==0){
				$detail = array(
					'master_sewer_id' 		=> $this->uuid->v4(),
					'nik' 					=> $get->nik,
					'name' 					=> $get->name,
					'department_name' 		=> $get->department_name,
					'subdept_name' 			=> $get->subdept_name,
					'job_desc' 				=> $get->jdesc,
					'factory' 				=> $get->factory					
				);
				$this->db->insert('master_sewer', $detail);
			}
		}
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
	        $status = 500;
		}else{
			$this->db->trans_complete();
			$status = 200;
		}
		
	}
	function get_mo()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$this->db2->where('to_char(kst_lcdate,\'YYYY\')','2018');
		$_get = $this->db2->get('edm_mo');
		foreach ($_get->result() as $get) {
			$this->db->where('doc_mo', $get->doc_mo);
			$_cek = $this->db->get('master_mo');
			if ($_cek->num_rows()==0) {
				$detail = array(
					'master_mo_id'       => $this->uuid->v4(),
					'doc_mo'             =>	$get->doc_mo,
					'style'              =>	$get->upc,
					'style'              =>	$get->upc,
					'poreference'        =>	$get->poreference,
					'kst_joborder'       =>	$get->kst_joborder,
					'product_code'       =>	$get->value,
					'warehouse'          =>	$get->warehouse,
					'workflow'           =>	$get->workflow,
					'size'               =>	$get->size,
					'qtyordered'		 =>	$get->qtyordered,
					'kst_lcdate'         =>	$get->kst_lcdate,
					'datestartschedule'  =>	$get->datestartschedule,
					'datefinishschedule' =>	$get->datefinishschedule,
					'kst_statisticaldate' =>$get->kst_statisticaldate,
				);
				$this->db->trans_start();
				$this->db->insert('master_mo', $detail);
				$this->db->trans_complete();
			}	
		}
	}
	public function uploadmo()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$year = date('Y');
		$query = "SELECT MAX(to_char(updated,'yyyy-mm-dd')) as updated FROM edm_mo where to_char(kst_lcdate,'yyyy')='$year'";
		$max = $this->db2->query($query)->row_array();
		
			$this->db2->where('to_char(updated,\'YYYY-mm-dd\')',$max['updated']);
		$uploadmo = $this->db2->get('edm_mo');
		
		foreach ($uploadmo->result() as $get) {
			$this->db->where('doc_mo', $get->doc_mo);
			$_cek = $this->db->get('master_mo');
			if ($_cek->num_rows()==0) {
				$detail = array(
					'master_mo_id'       => $this->uuid->v4(),
					'doc_mo'             =>	$get->doc_mo,
					'style'              =>	$get->upc,
					'style'              =>	$get->upc,
					'poreference'        =>	$get->poreference,
					'kst_joborder'       =>	$get->kst_joborder,
					'product_code'       =>	$get->value,
					'warehouse'          =>	$get->warehouse,
					'workflow'           =>	$get->workflow,
					'size'               =>	$get->size,
					'qtyordered'		 =>	$get->qtyordered,
					'kst_lcdate'         =>	$get->kst_lcdate,
					'datestartschedule'  =>	$get->datestartschedule,
					'datefinishschedule' =>	$get->datefinishschedule,
					'kst_statisticaldate' =>$get->kst_statisticaldate,
				);
				$this->db->trans_start();
				$this->db->insert('master_mo', $detail);
				$this->db->trans_complete();
			}	
		}
	}
	public function get_order()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}		
		$poupt = [];
		$now = date('Y-m-d');
		// $this->db->select_max('kst_lcdate');
		
		// $max = $this->db->get('po_buyers')->row_array()['kst_lcdate'];
 		
 		// // $this->db2->where('to_char(kst_lcdate,\'YYYY-MM\')', $tahun);
 		// $this->db2->where('date(updated) >=', '2024-01-01');
 		// $this->db2->where('date(updated) <=', '2024-03-31');

 		// $this->db2->where('date(kst_lcdate)', '2019-04-11');
 		$this->db2->where('date(created)', $now);
 		$this->db2->or_where('date(updated)', $now);
 		// $this->db2->or_where('date(kst_lcdate)', $now);
 		// $this->db2->or_where('kst_season', 'SS22');
 		// $this->db2->where('poreference','0130598259');
 		// $this->db2->distinct();
 		// $this->db2->select('poreference,kst_lcdate,kst_statisticaldate,recycle,documentno,kst_season,upc','podd_check','orderno');
		// $this->db2->where('brand','ELITE');

		$lcdate = $this->db2->get('adt_order_v_header');
		
		foreach ($lcdate->result() as $key => $lc) {

			// var_dump($lc);die();
			// var_dump($lc);die();
			if ($lc->recycle=='Y') {
				$isrecy = true;
			}else{
				$isrecy = false;
			}
			$docno = trim(explode('^', $lc->documentno)[0]);

			$this->db->where('poreference', $lc->poreference);
			$cek_po = $this->db->get('po_buyers')->row_array();

			$poupt[]=$lc->poreference;

			if ($cek_po==null) {
				$detail_po = array(
					'id'          => $this->uuid->v4(), 
					'poreference' => $lc->poreference, 
					'kst_lcdate'  => $lc->kst_lcdate,
					'kst_statisticaldate' => $lc->kst_statisticaldate,
					'recycle'=>$isrecy,
					'create_date'=>date('Y-m-d H:i:s'),
					'update_date'=>date('Y-m-d H:i:s'),
					'documentno'=>$docno,
					'season'=>$lc->kst_season,
					'style'=>$lc->upc,
					'podd'=>isset($lc->podd_check) ? $lc->podd_check : $lc->kst_statisticaldate,
					'customer_order_number'=>$lc->orderno,
					'brand'=>$lc->brand
				);
			
				$this->db->trans_start();
				$this->db->insert('po_buyers', $detail_po);
				$this->db->trans_complete();		
			
			}else if ($cek_po['poreference']==$lc->poreference){
			
				$update = array(
					'poreference' => $lc->poreference, 
					'kst_lcdate'  => $lc->kst_lcdate,
					'kst_statisticaldate' => $lc->kst_statisticaldate,
					'recycle'=>$isrecy,
					'update_date'=>date('Y-m-d H:i:s'),
					'documentno'=>$docno,
					'season'=>$lc->kst_season,
					'style'=>$lc->upc,
					'podd'=>isset($lc->podd_check) ? $lc->podd_check : $lc->kst_statisticaldate,
					'customer_order_number'=>$lc->orderno,
					'brand'=>$lc->brand
				);

				$this->db->trans_start();
				$this->db->where('id',$cek_po['id']);
				$this->db->update('po_buyers',$update);
				$this->db->trans_complete();
				
			}else if ($cek_po['poreference']!=$lc->poreference){
				
		

				$detail_po = array(
					'id'          => $this->uuid->v4(), 
					'poreference' => $lc->poreference, 
					'kst_lcdate'  => $lc->kst_lcdate,
					'kst_statisticaldate' => $lc->kst_statisticaldate,
					'recycle'=>$isrecy,
					'create_date'=>date('Y-m-d H:i:s'),
					'update_date'=>date('Y-m-d H:i:s'),
					'documentno'=>$docno,
					'season'=>$lc->kst_season,
					'style'=>$lc->upc,
					'podd'=>isset($lc->podd_check) ? $lc->podd_check : $lc->kst_statisticaldate,
					'customer_order_number'=>$lc->orderno,
					'brand'=>$lc->brand
				);
				$this->db->trans_start();
				$this->db->insert('po_buyers', $detail_po);
				$this->db->trans_complete();
				

			}


		}

			
		$this->daily_update($poupt);
		
	}
	private function daily_update($polist)
	{
		// $tahun = date('Y-m');
		// $now = date('Y-m-d');
		// $this->db->where('delete_date is NULL',NULL,FALSE);
		// $this->db->where('requirement_date is NULL',NULL,FALSE);
		// $this->db->group_start();
		// $this->db->where('date(create_date)', $now);
		// $this->db->group_end();
		$this->db->where_in('poreference',$polist);
		// $this->db->where('to_char(kst_lcdate,\'YYYY-MM\')', $tahun);
		$list_po = $this->db->get('po_buyers');
		
		foreach ($list_po->result() as $key => $list) {
			$po    = $list->poreference;
			$order = $this->Cron_model->get_detail($po);
		
			foreach ($order->result() as $key => $or) {
		
				$where = array(
					'style'         => $or->style, 
					'poreference'   => $or->poreference, 
					'size'          => $or->size, 
					'kst_articleno' => $or->kst_articleno, 
				);

				$this->db->where($where);
				$cekorder = $this->db->get('master_order');

				if ($cekorder->num_rows()==0) {


					$orderdetail = array(
						'master_order_id'     => $this->uuid->v4(),
						'poreference'         => $or->poreference,
						'style'               => $or->style,
						'poreference'         => $or->poreference,
						'size'                => $or->size,
						'qty_ordered'         => (int)$or->qtyordered,
						'kst_joborder'        => $or->kst_joborder,
						'documentno'          => $or->documentno,
						'kst_statisticaldate' => $or->kst_statisticaldate,
						'kst_lcdate'          => $or->kst_lcdate,
						'kst_articleno'       => $or->kst_articleno,
						'c_doctype_id'        => $or->c_doctypetarget_id,
						'business_partner'    => $or->bp,
						'location'            => $or->location,
						'balance'             => (int)$or->balance,
						'value'               => $or->value,
						'item_name'			  => $or->kst_name
					);
					$this->db->trans_start();
					$this->db->insert('master_order', $orderdetail);
					$date = date('Y-m-d H:i:s');
					$this->db->where('poreference',$or->poreference);
					$this->db->update('po_buyers',array('requirement_date'=>$date));
					$this->db->trans_complete();
				}else{
					$uorderdetail = array(
						'master_order_id'     => $this->uuid->v4(),
						'qty_ordered'         => (int)$or->qtyordered,
						'value'               => $or->value,
						'item_name'			  => $or->kst_name
					);
					$this->db->trans_start();
					$this->db->where($where);
					$this->db->update('master_order',$uorderdetail);
					$date = date('Y-m-d H:i:s');
					$this->db->where('poreference',$or->poreference);
					$this->db->update('po_buyers',array('requirement_date'=>$date));
					$this->db->trans_complete();
				}
			}		
		}
	}
	public function update_perpo($po)
	{
		/*if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}*/
		$where = array(
			$po,
		);
		$this->db->where_in('poreference',$where);
		// $this->db->where('kst_lcdate', '2019-02-15');
		$list_po = $this->db->get('po_buyers');
		
		foreach ($list_po->result() as $key => $list) {
			$po = $list->poreference;
			$order = $this->Cron_model->get_detail($po);
			
			foreach ($order->result() as $key => $or) {
				$where = array(
					'style' => $or->style, 
					'poreference' => $or->poreference, 
					'size' => $or->size, 
				);

				$this->db->where($where);
				$cekorder = $this->db->get('master_order');
				if ($cekorder->num_rows()==0) {

					$orderdetail = array(
						'master_order_id'     => $this->uuid->v4(),
						'poreference'         =>$or->poreference,
						'style'               =>$or->style,
						'poreference'         =>$or->poreference,
						'size'                =>$or->size,
						'qty_ordered'         =>(int)$or->qtyordered,
						'kst_joborder'        =>$or->kst_joborder,
						'documentno'          =>$or->documentno,
						'kst_statisticaldate' =>$or->kst_statisticaldate,
						'kst_lcdate'          =>$or->kst_lcdate, 
						'kst_articleno'       =>$or->kst_articleno, 
						'c_doctype_id'        =>$or->c_doctypetarget_id, 
						'business_partner'    =>$or->bp, 
						'location'            =>$or->location, 
						'balance'             =>(int)$or->balance,
						'value'               => $or->value,
						'item_name'			  => $or->kst_name
					);
					$this->db->trans_start();
					$this->db->insert('master_order', $orderdetail);
					$date = date('Y-m-d H:i:s');
					$this->db->where('poreference',$or->poreference);
					$this->db->update('po_buyers',array('requirement_date'=>$date));
					$this->db->trans_complete();
				}
			}		
		}
	}
	public function updateTargetQTY()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$date = date('Y-m-d');
		
		$this->db->where('date', $date);
		$this->db->select('style,line_id,factory_id');
		$this->db->distinct();
		$getstyle = $this->db->get('line_summary_po');
		foreach ($getstyle->result() as $key => $get) {
			// $this->db->where('to_char(create_date,\'YYYY-mm-dd\')<',$date);
			$query = "select * from update_targetqty('$get->style',$get->line_id,$get->factory_id)";
			$this->db->query($query);
		}

	}
	
	public function update_counter_sewing($value='')
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$cekBeda = $this->Cron_model->cekBeda();
		foreach ($cekBeda->result() as $key => $cek) {
			$update_counter = "select * from update_counter_sewing('$cek->style',$cek->line_id)";
			$this->db->query($update_counter);
		}
	}
	public function update_counter_folding()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$cekBeda = $this->Cron_model->cekBedaFolding();
		foreach ($cekBeda->result() as $key => $cek) {
			$update_counter = "select * from update_folding('$cek->inline_header_id','$cek->poreference','$cek->size')";
			$this->db->query($update_counter);
		}
	}

	public function po_buyer()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
				
			$this->template->set('title','PO Buyer');
			$this->template->set('desc_page','PO Buyer');
			$this->template->load('layout','po_buyer/list_po_buyer');
        }        
	}

	public function list_po()
    {
        $columns = array( 
							0 => 'kst_lcdate',
							1 => 'poreference',
							2 => 'requirement_date',
							3 => 'create_date',
							4 => 'update_date',
							5 => 'delete_date',
							6 => 'po_new'
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

  
        $totalData = $this->Cron_model->allposts_po_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $pobuy = $this->Cron_model->allposts_po($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value'];
            

            $pobuy =  $this->Cron_model->posts_po_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Cron_model->posts_po_search_count($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($pobuy))
        {
            foreach ($pobuy as $pobuy)
            {
                $_temp                  = '"'.$pobuy->poreference.'"';
                // $level                  = $this->db->get_where('level_user',array('level_id'=>$pobuy->level_id))->result()[0]->level_name;
                // $factory                = $this->db->get_where('master_factory',array('factory_id'=>$pobuy->factory))->result()[0]->factory_name;
				// $nestedData['no']                = (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['poreference']      = ucwords($pobuy->poreference);
				$nestedData['requirement_date'] = ucwords($pobuy->requirement_date);
				$nestedData['kst_lcdate']       = ucwords($pobuy->kst_lcdate);
				$nestedData['create_date']      = ucwords($pobuy->create_date);
				$nestedData['update_date']      = ucwords($pobuy->update_date);
				$nestedData['delete_date']      = ucwords($pobuy->delete_date);
				$nestedData['po_new']           = ucwords($pobuy->po_new);
				$nestedData['action']           = "<center><a onclick='return reduce($_temp)' href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-th-list'></i></a> <a onclick='return reroute($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-paper-plane'></i></a></center>";
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
	
	public function add_po()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('po_buyer/add_po_buyer');
        }        
	}
	
	public function save_po($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
			$pobuyer = $post['po'];
			// $this->db2->distinct();
			// $this->db2->select('poreference,kst_lcdate,kst_statisticaldate,recycle,documentno,kst_season,upc','podd_check','orderno');
			$this->db2->like('poreference',$pobuyer, 'BOTH');
			$lcdate = $this->db2->get('adt_order_v_header');

			
			
			if($lcdate->num_rows()>0){				
				foreach ($lcdate->result() as $key => $lc) {
					$this->db->where('poreference', $lc->poreference);
					$cek_po = $this->db->get('po_buyers');
					if ($cek_po->num_rows()==0) {
						$detail_po = array(
							'id'          => $this->uuid->v4(), 
							'poreference' => $lc->poreference, 
							'kst_lcdate'  => $lc->kst_lcdate,
							'kst_statisticaldate' => $lc->kst_statisticaldate,
							'recycle'=>$isrecy,
							'create_date'=>date('Y-m-d H:i:s'),
							'documentno'=>$docno,
							'season'=>$lc->kst_season,
							'style'=>$lc->upc,
							'podd'=>$lc->podd_check,
							'customer_order_number'=>$lc->orderno
						);
						$this->db->trans_start();
						$this->db->insert('po_buyers', $detail_po);
						$this->db->trans_complete();				
					}
				}

				$this->update_perpo($pobuyer);
				
				$status = 1;
				$pesan = "SUKSES";
			}
			else{
				$status = 2;
				$pesan = "PO BUYER TIDAK ADA DI ERP, SILAHKAN DI CEK LAGI";
			}
		}else{
			$status = 3;
			$pesan = "PO KOSONG";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
	}
	
	public function reduce_po($post=0)
    {
		$post = $this->input->get();
		$po = $post['po'];
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		$x = $this->Cron_model->get_detail($po);
		
		$data = array (
			'po_buyer'=> $po,
			'distribusi' => $this->Cron_model->getDistribusi($po),
			'erp' => $x->result(),
		);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('po_buyer/reduce_po_buyer',$data);
        }
	}
	
	public function sinkron_po($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
			$pobuyer = $post['po'];
			$this->Cron_model->reduce_po($pobuyer);
			$this->update_perpo($pobuyer);
			$this->Cron_model->update_po_buyers($pobuyer);
			$status = 1;
			$pesan = "SUKSES";
		}
		else{
			$status = 2;
			$pesan = "GAGAL";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
	}

	public function cancel_po($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
			$pobuyer = $post['po'];
			$this->Cron_model->cancel_po($pobuyer);
			$status = 1;
			$pesan = "SUKSES";
		}
		else{
			$status = 2;
			$pesan = "GAGAL";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
	}

	public function reroute_po($post=0)
    {
		$post = $this->input->get();
		$po = $post['po'];
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		$x = $this->Cron_model->get_detail($po);
		
		$data = array (
			'po_buyer'=> $po,
			'distribusi' => $this->Cron_model->getDistribusi($po),
			'erp' => $x->result(),
		);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('po_buyer/reroute_po_buyer',$data);
        }
	}

	public function insert_po($po)
    {
		$pobuyer = $po;
		// $this->db2->distinct();
		// $this->db2->select('poreference,kst_lcdate,kst_statisticaldate');
		$this->db2->like('poreference',$pobuyer, 'BOTH');
		$lcdate = $this->db2->get('adt_order_v_header');
		
		if($lcdate->num_rows()>0){				
			foreach ($lcdate->result() as $key => $lc) {
				$this->db->where('poreference', $lc->poreference);
				$cek_po = $this->db->get('po_buyers');
				if ($cek_po->num_rows()==0) {
					$detail_po = array(
						'id'                  => $this->uuid->v4(),
						'poreference'         => $lc->poreference,
						'kst_lcdate'          => $lc->kst_lcdate,
						'kst_statisticaldate' => $lc->kst_statisticaldate
					);
					$this->db->trans_start();
					$this->db->insert('po_buyers', $detail_po);
					$this->db->trans_complete();				
				}
			}

			$this->update_perpo($pobuyer);
			
			// $status = 1;
			// $pesan = "SUKSES";
		}
		else{
			// $status = 2;
			// $pesan = "PO BUYER TIDAK ADA DI ERP, SILAHKAN DI CEK LAGI";
		}	

		// $data = array(
		// 	'status'     => $status, 
		// 	'pesan'      => $pesan, 
		// );
		// echo json_encode($data);
	}

	public function update_po_baru($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
			$po_lama = $post['po_lama'];
			$po_baru = $post['po_baru'];
			$this->Cron_model->update_po_baru($po_lama,$po_baru);
			$this->insert_po($po_baru);
			$status = 1;
			$pesan = "SUKSES";
		}
		else{
			$status = 2;
			$pesan = "GAGAL";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
	}

	public function cek_trans()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$cektrans = $this->QcEndlineModel->cekCountertrans();
			
		if ($cektrans->num_rows()>0) {	
			foreach($cektrans->result() as $cek){
				$update_counter = "select * from update_counter('$cek->qc_endline_id')";
				$this->db->query($update_counter);

				$update_counter_qc = "select * from update_counter_qc('$cek->qc_endline_id')";
				$this->db->query($update_counter_qc);
			}
		}

	}
	public function tarikulang()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		// $query = "select * from po_buyers where requirement_date is not null and delete_date is null and to_char(kst_lcdate,'YYYY')='2019'";
		// $query = "select * from po_buyers where poreference='0123313252'";

		$query = "select distinct poreference from master_order where poreference 
		in (select distinct poreference from inline_header where article is null 
		and to_char(create_date,'YYYY') = '2019') and kst_articleno is null;";
		$pobuyer = $this->db->query($query);
		foreach ($pobuyer->result() as $key => $list) {
			$po    = $list->poreference;
			$order = $this->Cron_model->get_detail($po);
			
			foreach ($order->result() as $key => $or) {
				$where = array(
					'style'         => $or->style, 
					'poreference'   => $or->poreference, 
					'size'          => $or->size 
				);
				$this->db->where('kst_articleno is NULL',NULL,FALSE);
				$this->db->where($where);
				$cekorder = $this->db->get('master_order');
				if ($cekorder->num_rows()>0) {
					$this->db->where('style', $or->style);
					$this->db->where('poreference', $or->poreference);
					$this->db->where('size', $or->size);
					$this->db->where('kst_articleno is NULL',NULL,FALSE);
					$this->db->delete('master_order');
				}
					// $this->db->where($where);
					// $this->db->delete('master_order');
				$where = array(
					'style'         => $or->style, 
					'poreference'   => $or->poreference, 
					'size'          => $or->size, 
					'kst_articleno' => $or->kst_articleno, 
				);
				$this->db->where($where);
				$cekorder = $this->db->get('master_order');
					if ($cekorder->num_rows()==0) {
						$orderdetail = array(
							'master_order_id'     => $this->uuid->v4(),
							'poreference'         =>$or->poreference,
							'style'               =>$or->style,
							'poreference'         =>$or->poreference,
							'size'                =>$or->size,
							'qty_ordered'         =>(int)$or->qtyordered,
							'kst_joborder'        =>$or->kst_joborder,
							'documentno'          =>$or->documentno,
							'kst_statisticaldate' =>$or->kst_statisticaldate,
							'kst_lcdate'          =>$or->kst_lcdate, 
							'kst_articleno'       =>$or->kst_articleno, 
							'c_doctype_id'        =>$or->c_doctypetarget_id, 
							'business_partner'    =>$or->bp, 
							'location'            =>$or->location, 
							'balance'             =>(int)$or->balance,
							'value'               => $or->value 
						);
						$this->db->trans_start();
						$this->db->insert('master_order', $orderdetail);
						$date = date('Y-m-d H:i:s');
						$this->db->where('poreference',$or->poreference);
						$this->db->update('po_buyers',array('requirement_date'=>$date));
						$this->db->trans_complete();
					}
				
			}
			$query = "select DISTINCT poreference,style,kst_articleno from master_order where poreference='$po'";
			$cek = $this->db->query($query);
			foreach ($cek->result() as $key => $c) {
				$this->db->where('style', $c->style);
				$this->db->where('poreference', $c->poreference);
				$this->db->update('inline_header',array('article'=>$c->kst_articleno));
			}

		}
	}

	public function daily_po()
	{
		$date = date('Y-m-d');
		$tgl  = date('Y-m-d', strtotime('-3 months', strtotime($date)));

		$this->db->where('kst_lcdate>=',$tgl);
		$list_po = $this->db->get('po_buyers');
		
		foreach ($list_po->result() as $key => $list) {
			$po    = $list->poreference;
			$order = $this->Cron_model->get_detail($po);
			
			foreach ($order->result() as $key => $or) {
				$this->db->trans_start();
				$this->db->where('poreference',$or->poreference);
				$this->db->update('po_buyers',array('kst_statisticaldate'=>$or->kst_statisticaldate, 'crd'=>$or->datepromised));

				$this->db->trans_complete();
			}		
		}
	}

	public function ops_day(){
		$awal  	= date('Y-m-d');
		$akhir  = date('Y-m-d');
		// $awal  = '2020-01-10';
		// $akhir = '2020-01-10';
		$this->operator_perform_day($awal,$akhir,1);
		$this->operator_perform_day($awal,$akhir,2);
	}

	public function operator_perform_day($awal,$akhir,$factory)
	{
			$factory   = $factory;
			$datefrom  = $awal;
			$dateto    = $akhir;
			$nik_sewer = '';
			$grade     = '';
			$filter    = 'by_date';    
			$line_from = '';	
			$line_to   = '';

			$report = $this->ReportModel->downloadOperatorPerformance($datefrom,$dateto, $nik_sewer, $grade, $filter, $factory,$line_from,$line_to);
			

			if ($report!=NULL) {
				
				$this->db->trans_start();	
				foreach ($report as $key => $d) {
					
					$defect = [];
	            	if ($d->point_defect!=0) {
	            		$jenisDefect = $this->ReportModel->getDefect_cron($d->sewer_nik,$d->date,$d->master_proses_id,$factory);
	            		
	            		foreach ($jenisDefect->result() as $key => $jenis) {

							$defect[] = $jenis->defect_jenis."=".$jenis->jumlah."<br>";
							
						}
					}
					
					$data_insert = array( 
						'inline_summary_id' => $this->uuid->v4(),
						'date'              => $d->date,
						'line_id'           => $d->line_id,
						'nik_sewer'         => $d->sewer_nik,
						'nama_proses'		=> $d->proses_name,
						'total_score'		=> $d->point,
						'total_avg'			=> $d->total,
						'grade'				=> $d->grade,
						'jenis_defect'		=> implode(',', $defect),
						'factory_id'        => $factory,
						'create_date'		=> date('Y-m-d H:i:s')
					);
					
					$this->db->insert('inline_operator_performance',$data_insert);
				}
				
				$this->db->trans_complete();
			}

			// $data = array(
			// 	'draw' => $draw
			// );

            // $this->load->view('report/downloadoperatorperformance_view', $data);
	}

	public function update_value_erp()
	{
		$this->db->where('delete_date is NULL',NULL,FALSE);
		$this->db->where('date(create_date) >=','2020-06-01');
		$list_po = $this->db->get('po_buyers');
		
		foreach ($list_po->result() as $key => $list) {
			$po    = $list->poreference;
			$order = $this->Cron_model->get_detail($po);
			
			foreach ($order->result() as $key => $or) {
				$where = array(
					'style'         => $or->style,
					'poreference'   => $or->poreference,
					'size'          => $or->size,
					'kst_articleno' => $or->kst_articleno,
					'value'         => $or->value
				);

				$this->db->where($where);
				$cekorder = $this->db->get('master_order');
				
				if ($cekorder->num_rows()==1) {
					$this->db->trans_start();
						$mo = $cekorder->row();
						// var_dump($mo);
						// die();
						$this->db->where('master_order_id',$mo->master_order_id);
						$this->db->update('master_order',array('value'=>$or->value));
					$this->db->trans_complete();
				}
			}		
		}
	}


	
	
	//reduce
	public function cron_reduce($po)
    {

		$pobuyer = $po;
		$this->Cron_model->reduce_po($pobuyer);
		$this->update_perpo($pobuyer);
		$this->Cron_model->update_po_buyers($pobuyer);
	}

	//cancel
	public function cron_cancel($po)
    {
		$pobuyer = $po;
		$this->Cron_model->cancel_po($pobuyer);
	}

	//Add
	public function cron_add($po)
    {
		$pobuyer = $po;
		// $this->db2->distinct();
		// $this->db2->select('poreference,kst_lcdate');
		$this->db2->like('poreference',$pobuyer, 'BOTH');
		$lcdate = $this->db2->get('adt_order_v_header');

		
		
		if($lcdate->num_rows()>0){				
			foreach ($lcdate->result() as $key => $lc) {
				$this->db->where('poreference', $lc->poreference);
				$cek_po = $this->db->get('po_buyers');
				if ($cek_po->num_rows()==0) {
					$detail_po = array(
						'id'          => $this->uuid->v4(), 
						'poreference' => $lc->poreference, 
						'kst_lcdate'  => $lc->kst_lcdate 
					);
					$this->db->trans_start();
					$this->db->insert('po_buyers', $detail_po);
					$this->db->trans_complete();				
				}
			}

			$this->update_perpo($pobuyer);
		}
		else{
		}	
	}

	//reroute
	public function cron_reroute($po_lama,$po_baru)
    {
		$this->Cron_model->update_po_baru($po_lama,$po_baru);
		$this->insert_po($po_baru);
	}

	public function so_handle()
	{
		$this->db2->where('flag_erp', 'Y');
		$this->db2->where('flag_ls', null);
		$handle = $this->db2->get('bw_report_monitoring_ba');

		// var_dump($handle->num_rows());
		if($handle->num_rows()>0){				
			foreach ($handle->result() as $key => $value) {
				$remark = $value->flag_so_case;
				
				
				$query = $this->db->query("SELECT * FROM inline_header where poreference = '$value->old_po'")->num_rows();
				if($query>0){
					$this->db2->query("UPDATE tpb_berita_acara_line SET flag_ls = 'N' WHERE tpb_berita_acara_line_uu = '$value->tpb_berita_acara_line_uu'");
				}
				else{
					if($remark==1){
						//change prefix';
						$this->cron_reroute($value->old_po, $value->new_po);
					}
					else if($remark==2){
						//reroute';
						$this->cron_reroute($value->old_po, $value->new_po);
					}
					else if($remark==3){
						//cancel';
						$this->cron_cancel($value->old_po);
					}
					else if($remark==4){
						//reduce';
						$this->cron_reduce($value->old_po);
					}
					else{

					}

					$this->db2->query("UPDATE tpb_berita_acara_line SET flag_ls = 'Y' WHERE tpb_berita_acara_line_uu = '$value->tpb_berita_acara_line_uu'");
				}
			}
		}

		echo 'Proses selesai pada '.date('Y-m-d H:i:s').' .';
	}
}

/* End of file cron.php */
/* Location: ./application/controllers/cron.php */
