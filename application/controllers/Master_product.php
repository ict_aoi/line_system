<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_product extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Product_model','Cekpermision_model'));
        chek_session();
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
        // $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        // if ($cek_permision==0) {
        //     redirect('error_page','refresh');
        // }else{
            $this->template->set('title','Master Product');
            $this->template->set('desc_page','Master Product');
            $this->template->load('layout','master/product/list_product_view');
        // }

    }
    public function add_product()
    {
        // $cek_permision = $this->Cekpermision_model->cekpermision(1);

        // if ($cek_permision==0) {
        //     redirect('error_page','refresh');
        // }else{
            $this->load->view('master/product/add_product');
        // }        
    }
    
    public function save_product($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
            $nama         = $post['nama_product'];
            $nama_product = strtoupper(trim($nama));

			$cek = $this->db->query("SELECT * FROM master_product where nama_product = '$nama_product'");

            if($cek->num_rows()>0){
                $status = 2;
				$pesan = "DATA YANG DI INPUT SUDAH ADA";
            }else{
    
                $data = array(					
					'id'           => $this->uuid->v4(),
					'nama_product' => $nama_product,
					'created_by'   => $this->session->userdata('nik'),
					'created_at'   => date('Y-m-d H:i:s'),
					'updated_by'   => $this->session->userdata('nik'),
					'updated_at'   => date('Y-m-d H:i:s'),
                );
                $this->db->trans_start();
                $this->db->insert('master_product', $data);
                $this->db->trans_complete();
				$status = 1;
				$pesan = "SUKSES";
			}
		}else{
			$status = 3;
			$pesan = "DATA ADA YANG KOSONG";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
	}
    
    public function listmaster()
    {
        $columns = array( 
                            0 =>'created_at', 
                            1 =>'nama_product'
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $totalData = $this->Product_model->allposts_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Product_model->allposts($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Product_model->posts_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Product_model->posts_search_count($search);
        }

        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $key=>$master)
            {
                $_temp                  = '"'.$master->id.'"';
                
                $nestedData['no']           = (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['nama_product'] = ucwords($master->nama_product);
                $nestedData['action']       = "<center><a onclick='return edit($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-pencil'></i></a> <a onclick='return delete_product($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash-o'></i></a></center>";
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }
    
    public function edit($post=0)
    {
		$post          = $this->input->get();
		$id            = $post['id'];
		// $cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		$x = $this->Product_model->getData($id);
		
		$data = array (
			'product' => $x,
		);

        // if ($cek_permision==0) {
        //     redirect('error_page','refresh');
        // }else{
            $this->load->view('master/product/edit_product',$data);
        // }
    }
    
    public function edit_product($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
            $id           = $post['id'];
            $nama_product = $post['nama_product'];

            $data = array(
				'nama_product' => strtoupper(trim($nama_product)),
				'updated_by'   => $this->session->userdata('nik'),
				'updated_at'   => date('Y-m-d H:i:s'),
            );

			$this->db->trans_start();
			$this->db->where('id',$id);
			$this->db->update('master_product',$data);
			$this->db->trans_complete();

			$status = 1;
			$pesan = "SUKSES";
		}else{
			$status = 3;
			$pesan = "DATA ADA YG KOSONG";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
    }
    
    public function delete_product($post=NULL)
    {
		$post = $this->input->get();
    
        if ($post!='') {
			$id = $post['id'];

            $data = array(
				'deleted_by'   => $this->session->userdata('nik'),
				'deleted_at'   => date('Y-m-d H:i:s'),
            );

			$this->db->trans_start();
			$this->db->where('id',$id);
			$this->db->update('master_product',$data);
			$this->db->trans_complete();
            // $status = 1;
            echo '1';
            // $pesan = "SUKSES";
		}else{
            // $status = 3;
            echo '3';
			// $pesan = "DATA TIDAK DITEMUKAN";
		}
        
	}	

}

/* End of file master_process.php */
/* Location: ./application/controllers/master_process.php */
