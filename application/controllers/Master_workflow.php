<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_workflow extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Master_workflow_model','Cekpermision_model'));
        chek_session();
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		if ($cek_permision==0) {
			redirect('error_page','refresh');
		}else{
			$this->template->set('title','Master workflow');
		    $this->template->set('desc_page','Master workflow');
		    $this->template->load('layout','master/workflow/list_master_view');
		}
		
	}
	public function listmaster_ajax()
	{
		$columns = array( 
                            0 =>'style', 
                            1 =>'style'
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');

  
        $totalData = $this->Master_workflow_model->allposts_count_list();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Master_workflow_model->allposts_list($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Master_workflow_model->posts_search_list($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Master_workflow_model->posts_search_count_list($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
            	
                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['style'] = ucwords($master->style);
                $nestedData['action'] = "<center><a href='master_workflow/edit?id=$master->style' class='btn btn-success'><i class='fa fa-pencil-square-o'></i> Edit</a></center>";                 
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
	public function addworkflow()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		if ($cek_permision==0) {
			redirect('error_page','refresh');
		}else{
			$this->template->set('title','Add WorkFlow');
		    $this->template->set('desc_page','Add WorkFlow');
		    $this->template->load('layout','master/workflow/add_workflow_view');
		}		
	}
	public function loadstyle()
	{
		$cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		if ($cek_permision==0) {
			redirect('error_page','refresh');
		}else{
			$this->load->view('master/workflow/select_style');
		}
		
	}
	//serverside select style
	public function liststyle_ajax()
	{
		$columns = array( 
                            0 =>'style', 
                            1 =>'style'
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');

  
        $totalData = $this->Master_workflow_model->allposts_count_style();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Master_workflow_model->allposts_style($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Master_workflow_model->posts_search_style($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Master_workflow_model->posts_search_count_style($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $master)
            {
            	$_temp = '"'.$master->style.'"';
                $nestedData['no']= (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['style'] = ucwords($master->style);
                $nestedData['action'] = "<center><a onclick='return select($_temp)' href='javascript:void(0)' class='btn btn btn-success'><i class='fa fa-check-square' aria-hidden='true'></i> Select</a></center>";                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
	public function savesubmit($style=NULL,$items=NULL)
	{
		
        $items = json_decode($this->input->post('items',TRUE));
        $gsd   = $this->input->post('gsd-hid');
        $style = $this->input->post('style-hid',TRUE);
		$i     = 0;
		if (!empty($items)) {
			foreach ($items as $key => $item) {
                $data[$i]['style']            = $style;
                $data[$i]['gsd_smv']          = $gsd;
                $data[$i]['master_proses_id'] = $item->proses_id;
                $data[$i]['lastproses']       = $item->lastproses;
                $data[$i]['cycle_time']       = $item->cycle_time;
                $angka                        = ($item->cycle_time)/60;
				$data[$i]['smv_spt']          = number_format($angka,2);
				if($item->cekcrit == false){
					$kategori_proses = 'f';
				}
				else{
					$kategori_proses = 't';
				}
				$data[$i]['is_critical'] = $kategori_proses;
				$i++;
			}
			$this->db->insert_batch('master_workflow', $data);
			echo "sukses";
		}else{
			echo "gagal";
		}
	}
	public function edit($id=0)
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		if ($cek_permision==0) {
			redirect('error_page','refresh');
		}else{
			$style = $this->input->get('id');
			$record = $this->Master_workflow_model->getworkflow($style);


			$data['style'] =$this->db->get_where('master_workflow',array('style'=>$_GET['id']))->row_array();
			$data['record'] = json_encode($record);

			$this->template->set('title','Edit WorkFlow');
	        $this->template->set('desc_page','Edit WorkFlow');
	        $this->template->load('layout','master/workflow/edit_workflow_view',$data);
		}
    }
    public function deletesubmit()
    {
    	$id = $this->input->post('id');
    	$cek = $this->Master_workflow_model->getworkflow_id($id);
    	if ($cek>0) {
    		$date   = date('Y-m-d H:i:s');
    		$this->db->where('master_workflow_id',$id)->update('master_workflow', array('delete_at'=>$date));
    		echo "sukses";
    	}else{
    		echo "gagal";
    	}
    }
    public function editsubmit()
    {
    	$items = json_decode($this->input->post('items'));
        $gsd   = $this->input->post('gsd-hid');
        $style = $this->input->post('style-hid',TRUE);
        
    	$i           = 0;
    	$data        = array();
    	$data_update = array();
    	if (!empty($items)) {
           
            // var_dump("<pre>");
            // var_dump(print_r($items));
            // var_dump("</pre>");
            // die();
    		foreach ($items as $key => $item) {
	    		if ($item->master_workflow_id == -1) {
                    $this->db->where('delete_at is NULL',null,false);
                    $this->db->where(array('master_proses_id'=>$item->proses_id,'style'=>$style));
                    $cek = $this->db->get('master_workflow');
                    if ($cek->num_rows()==0) {
                        $data[$i]['master_proses_id'] = $item->proses_id;
                        $data[$i]['style']            = $style;
                        $data[$i]['gsd_smv']          = $gsd;
                        $data[$i]['lastproses']       = $item->lastproses;
						$data[$i]['cycle_time']       = $item->cycle_time;
						$angka                        = ($item->cycle_time)/60;
						$data[$i]['smv_spt']          = number_format($angka,2);
						if($item->cekcrit == false){
							$kategori_proses = 'f';
						}
						else{
							$kategori_proses = 't';
						}
                        $data[$i]['is_critical'] = $kategori_proses;
                        $i++;
                        
                    }

                }
                else {
					// var_dump($item->cekcrit == null);
					// die();
                    $data_update['master_proses_id'] = $item->proses_id;
                    $data_update['style']            = $style;
                    $data_update['gsd_smv']          = $gsd;
                    $data_update['lastproses']       = $item->lastproses;
                    $data_update['cycle_time']       = $item->cycle_time;
					$angka            				 = ($item->cycle_time)/60;
                    $data_update['smv_spt']      = number_format($angka,2);
					if (array_key_exists("cekcrit",$item))
					{
						if(($item->cekcrit) == false){
							$kategori_proses = 'f';
						}
						else{
							$kategori_proses = 't';
						}
					}
					else{
						$kategori_proses = 'f';
					}
					$data_update['is_critical'] = $kategori_proses;
					// var_dump('ok');
                    $this->db->where('master_workflow_id',$item->master_workflow_id)->update('master_workflow', $data_update);
                }
	    	}
            if (count($data)>0) {
               $this->db->insert_batch('master_workflow', $data);
            }
            
            $this->db->where('style',$style)->update('master_workflow', array('gsd_smv'=>$gsd));
            
            // var_dump("<pre>");
            // var_dump(print_r($data_update));
            // var_dump("</pre>");
            // die;
            // $this->db->where('style',$style)->update_batch('master_workflow', $data_update);
	    	echo "sukses";
    	}else{
    		echo "gagal";
    	}
    	
    }

}

/* End of file master_workflow.php */
/* Location: ./application/controllers/master_workflow.php */
