<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		chek_session();
        $this->load->model(array('Role_model','Cekpermision_model'));
	}

	public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Permision');
            $this->template->set('desc_page','Add Permision');
            $this->template->load('layout','role/list_role_view');
        }		
	}
	public function load_level()
	{
		$draw = '<table id="mytable" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                <tr>
                	<td>Pilih Level</td>
                	<td>'.cmb_dinamis2('level_user', 'level_user', 'level_name', 'level_id', null,"id='level_user' onchange='loadData()'").'</td>
                </tr>
            </table>';
		echo $draw;
	}
	public function add_level_view()
	{
		$this->load->view('role/add_role_view');
	}
	public function save_level()
	{
        $post = $_POST;
        $_cek = $this->db->get_where('level_user',array('level_name'=>$post['level_name']));
        if ($_cek->num_rows()==0) {
        	$detail = array(
	            'level_id'       	=> $this->uuid->v4(),
	            'level_name'  		=> $post['level_name'],
	            'description'   	=> $post['description'],
	        );
	        $this->db->insert('level_user', $detail);
	        echo "sukses";
        }else{
        	echo "gagal";
        }        
	}
    public function modul()
    {
    	$level_user = $_GET['level_user'];
    	echo "<table id='mytable2' class='table table-striped table-bordered table-hover table-full-width dataTable'>
                <thead>
                    <tr>
                        <th width='10'>NO</th>
                        <th>NAMA MODULE</th>
                        <th>LINK</th>
                        <th width='100'>HAK AKSES</th>
                    </tr>";
        
        $menu = $this->db->get('menu');
        $no=1;
        foreach ($menu->result() as $row){
        	$_temp = '"'.$row->menu_id.'"';
            echo "<tr>
                <td>$no</td>
                <td>".  strtoupper($row->nama_menu)."</td>
                <td>$row->link</td>
                <td align='center'><input type='checkbox' ";
            $this->chek_akses($level_user, $row->menu_id);
             echo " onclick='addRule($_temp)'></td>
                </tr>";
            $no++;
        }
        echo"</thead>
            </table>";
    }
    private function chek_akses($level_user,$menu_id){
        $data = array('level_id'=>$level_user,'menu_id'=>$menu_id);
        $chek = $this->db->get_where('role_user',$data);
        if($chek->num_rows()>0){
            echo "checked";
        }
    }
    public function add_role()
    {
    	$level_user = $_GET['level_user'];
        $menu_id    = $_GET['menu_id'];
        $chek       = $this->db->get_where('role_user',array('level_id'=>$level_user,'menu_id'=>$menu_id));
        if($chek->num_rows()==0){
        	$data       = array('role_id'=>$this->uuid->v4(),'level_id'=>$level_user,'menu_id'=>$menu_id);
            $this->db->insert('role_user',$data);
            echo "success";
        }else{
            $this->db->where('menu_id',$menu_id);
            $this->db->where('level_id',$level_user);
            $this->db->delete('role_user');
            echo "delete";
        }
    }
    public function user()
    {
        $this->template->set('title','Master User');
        $this->template->set('nav', 'User');
        $this->template->set('desc_page','Master User');
        $this->template->load('layout','role/list_user_view');
    }
    public function list_user()
    {
        $columns = array( 
                            0 =>'users_id', 
                            1 =>'username',
                            2 =>'nik',
                            3=> 'email',
                            4=> 'level_id',
                            5=> 'factory'
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $draw   = $this->input->post('draw');

  
        $totalData = $this->Role_model->allposts_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $role = $this->Role_model->allposts($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value'];
            

            $role =  $this->Role_model->posts_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Role_model->posts_search_count($search);
        }
        $data = array();
        $nomor_urut = 0;
        if(!empty($role))
        {
            foreach ($role as $role)
            {
                $_temp                  = '"'.$role->users_id.'"';
                $level                  = $this->db->get_where('level_user',array('level_id'=>$role->level_id))->result()[0]->level_name;
                $factory                = $this->db->get_where('master_factory',array('factory_id'=>$role->factory))->result()[0]->factory_name;
                $nestedData['no']       = (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['username'] = ucwords($role->username);
                $nestedData['nik']      = ucwords($role->nik);
                $nestedData['nohp']    = ucwords($role->nohp);
                $nestedData['level']    = ucwords($level);
                $nestedData['factory']  = ucwords($factory);
                $nestedData['action']   = "<center><a onclick='return edit_user($_temp)' href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-edit'></i></a> <a onclick='return hapus($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash-o'></i></a></center>";
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }
    public function add_user_view()
    {
        $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->load->view('role/add_user_view');
        }        
    }
    public function edit_user_view()
    {
       $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->db->where('users_id', $_GET['id']);
            $record = $this->db->get('users')->row_array();

            $this->db->where('factory_id', $record['factory']);
            $factory_name = $this->db->get('master_factory')->row_array()['factory_name'];
            
            $data = array('record' => $record, 'factory_name'=>$factory_name );

            $this->load->view('role/edit_user_view',$data);
        } 
        
    }
    public function save_user($post=NULL)
    {
        $post = $this->input->post();
        if ($post!='') {
            
            $_cek_user = $this->db->get_where('users',array('nik'=>$post['nik']));
            if ($_cek_user->num_rows()==0) {
                $password = bCrypt($post['password'],12);
                $detail = array(
                        'users_id'   => $this->uuid->v4(),
                        'nik'        => $post['nik'], 
                        'username'   => $post['user'], 
                        'password'   => $password,
                        'level_id'   => $post['level'], 
                        'factory'    => $post['factory'], 
                        'departement' => $post['departement'], 
                        'nohp'       => $post['nohp'], 
                        'active'     =>1
                );
                $this->db->insert('users', $detail);
                echo "sukses";
            }else{
                echo "gagal";
            }
        }
        
    }
    public function edit_user($post=0)
    {
        $post = $_POST;
        $_cek_nik = $this->db->get_where('users',array('users_id'=>$post['id']));
        if (empty($post['password'])) {
            $detail = array(
                'nohp'          => $post['nohp'], 
                'level_id'       => $post['level']
            );

            $this->db->where('users_id', $post['id']);
            $this->db->update('users', $detail);
            echo "sukses";
        }else{
            $password = bCrypt($post['password'],12);
            $detail = array(
                'password'       => $password,
                'nohp'          => $post['nohp'], 
                'level_id'       => $post['level']
            );
            $this->db->where('users_id', $post['id']);
            $this->db->update('users', $detail);
            echo "sukses";
        }
    }
    public function delete_user()
    {
        $this->db->where('users_id', $_GET['id']);
        $cek = $this->db->get('users');
        if ($cek->num_rows()>0) {
            $this->db->where('users_id', $_GET['id']);
            $this->db->update('users',array('active'=>0));
            echo "sukses";
        }
    }
    public function accountsetting()
    {
        $data['record'] = $this->Role_model->getUserDetail();
        
        $this->template->set('title','Cange');
        $this->template->set('desc_page','Add Permision');
        $this->template->load('layout','role/accountsetting_view',$data);
    }
    function changesubmit($password=NULL,$nik=NULL)
    {
       $nik =$this->session->userdata('nik');
       $password = bCrypt($this->input->post('password_new',TRUE),12);
       if (!empty($password)) {
            $this->db->where('nik', $nik);
            $this->db->update('users',array('password'=>$password));
            $this->session->sess_destroy();
            
            echo "sukses";
       }else{
        echo "gagal";
       }
    }
    public function searchNik($get=NULL)
    {
        $get = $this->input->get();

        if ($get!='') {
            $this->db->where('nik', $get['id']);
            $cekUser = $this->db->get('users');
            if ($cekUser->num_rows()==0) {
                $this->db->where('nik', $get['id']);
                $record=$this->db->get('master_sewer');

                if ($record->num_rows()>0) {
                    $this->db->where('factory_name', $record->row_array()['factory']);
                    $factory_id = $this->db->get('master_factory')->row_array()['factory_id'];
                    $notif      ="1";
                    $data = array('record' => $record->row_array(), 'factory_id'=>$factory_id,'notif'=>$notif);

                    echo json_encode($data);
                }else{
                    $data['notif']="2";
                    echo json_encode($data);
                }
                
            }else{
                $data['notif']="0";
                echo json_encode($data);
            }
            
        }
    }
}

/* End of file role.php */
/* Location: ./application/controllers/role.php */