<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
    {
  		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		chek_session();
		$this->load->model('Cekpermision_model');
		$this->load->model('DashboardModel');
		$this->load->model('DisplayModel');
		
    }

    public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(1);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Dashboard');
            $this->template->set('desc_page','Dashboard');
            $this->template->load('layout','dashboard/dashboard_view');
        }

	}

	public function download($tgl)
    {
        $columns = array( 
							0  => 'no_urut',
							1  => 'no_urut',
                        );

        $limit = NULL;
        $start = NULL;
        $order = NULL;
        $dir   = NULL;
        $draw  = $this->input->post('draw');

		$date    = $tgl;

		// var_dump($date);
		// die();

  		if ($date!='') {
  			$date = date('Y-m-d',strtotime($date));
  		}else{
  			$date = date('Y-m-d');
		}

		// var_dump($date);
		// die();
        $totalData = $this->DashboardModel->allposts_daily_count($date);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
			$dash = $this->DashboardModel->allposts_daily($limit,$start,$order,$dir,$date);
        }
        else {
            $search = $this->input->post('search')['value'];
			
            $dash =  $this->DashboardModel->posts_daily_search($limit,$start,$search,$order,$dir,$date);

			// var_dump($dash);
			// die();
            $totalFiltered = $this->DashboardModel->posts_daily_search_count($search,$date);
		}
		
        $data = array();
        $nomor_urut = 0;
        if(!empty($dash))
        {
			$i=0;
            foreach ($dash as $dash)
            {
				
				
                $cumday = $this->DashboardModel->cum_count($dash->daily_header_id, $date);
                $sewing = $this->DashboardModel->output_count($date, $dash->line_id, $dash->style, 'sewing');
                $outputqc = $this->DashboardModel->output_count($date, $dash->line_id, $dash->style, 'outputqc');
                $folding = $this->DashboardModel->output_count($date, $dash->line_id, $dash->style, 'folding');

                if($folding->total==NULL){
                    $folding->total = 0;
                }
                if($sewing->total==NULL){
                    $sewing->total = 0;
                }
                if($outputqc->total==NULL){
                    $outputqc->total = 0;
                }

                //Target Efficiency
                if ($cumday!=0&&(int)$cumday<12) {
                    $cum =$cumday;
                }elseif ((int)$cumday>=12) {
                    $cum =12;
                }else{
                    $cum =1;
                }
                $target = $this->DashboardModel->eff_count($dash->line_id, $dash->change_over, $cum);

                $time = $this->jam_kerja()['time'];
                //Efficiency
                $now   = date("Y-m-d H:i:s");
				
				if($date!=date("Y-m-d")){
					$jam_berjalan = 8;

				}
				else{
					$jam_awal = strtotime(date('Y-m-d 07:00:00'));
					$jam_istirahat = date('Y-m-d 11:45:00');

					//now ganti jam akhir
					if ($time>$jam_istirahat) {
						$result =abs((strtotime($now)-$jam_awal)-2700)/3600;
					}else{
						$result = abs(strtotime($now)-$jam_awal)/3600;
					}
					$jam_berjalan = $result;
				}
				
                
				//wft
				$inline = $this->DashboardModel->defect_count($date, $dash->line_id, $dash->style, 'defect_inline');
				$endline = $this->DashboardModel->defect_count($date, $dash->line_id, $dash->style, 'defect_endline');

				// $sum_line = ($inline->total+$endline->total);

				if($outputqc->total == 0){
					$wft = 0;
				}
				else{
					$wft = 100*(($inline->total+$endline->total)/$outputqc->total);
				}
				
				
              
                $factory   = $this->session->userdata('factory');
                $eff = $this->list_eff($date,$dash->line_id,$factory);
	   
				if($eff['actual_eff']<$eff['target_eff']){
                    $cek = 1;
                }
                else{
                    $cek = 0;
				}

				$target_eff = $eff['target_eff'];
				$actual_eff = $eff['actual_eff'];
				
				$_temp  = '"'.$dash->daily_header_id.'"';

				
				$tgl    = '"'.$date.'"';
				$_style = '"'.$dash->style.'"';
				$co     = '"'.$dash->change_over.'"';

				$baris = '<tr>
				<td class="text-center">'.$dash->create_date.'</td>
				<td class="text-center">'.$dash->line_name.'</td>
				<td class="text-center">'.$dash->style.'</td>
				<td class="text-center">'.$dash->gsd_smv.'</td>
				<td class="text-center">'.$dash->present_sewer.'</td>
				<td class="text-center">'.$dash->present_folding.'</td>
				<td class="text-center">'.$dash->working_hours.'</td>
				<td class="text-center">'.$dash->change_over.'</td>
				<td class="text-center">'.$cumday.'</td>
				<td class="text-center">'.number_format($actual_eff,1,',',',').'</td>
				<td class="text-center">'.$target_eff.'</td>
				<td class="text-center">'.number_format($wft,1,',',',').'</td>
				<td class="text-center">'.$sewing->total.'</td>
				<td class="text-center">'.$outputqc->total.'</td>
				<td class="text-center">'.$folding->total.'</td>
				</tr>';
				$data[] = $baris;
				$i++;
            }
        }
          
        $json_data = array(
                    // "draw"            => intval($this->input->post('draw')),  
                    // "recordsTotal"    => intval($totalData),  
                    // "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
					);

						// var_dump($data);
						// die();
					// var_dump($json_data['data'][0]['qc']);
					// var_dump($json_data['data'][0]['create_date']);
					// die();
		$this->load->view("dashboard/report_view",$json_data);
	}

	public function list_dashboard()
    {
        $columns = array( 
							0  => 'no_urut',
							1  => 'no_urut',
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

  
		$date    = $this->input->post('tgl');

        $factory   = $this->session->userdata('factory');
		// var_dump($date);
		// die();

  		if ($date!='') {
  			$date = date('Y-m-d',strtotime($date));
  		}else{
  			$date = date('Y-m-d');
		}

		// var_dump($date);
		// die();
        $totalData = $this->DashboardModel->allposts_daily_count($date);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $dash = $this->DashboardModel->allposts_daily($limit,$start,$order,$dir,$date);
        }
        else {
            $search = $this->input->post('search')['value'];
			
            $dash =  $this->DashboardModel->posts_daily_search($limit,$start,$search,$order,$dir,$date);

			// var_dump($dash);
			// die();
            $totalFiltered = $this->DashboardModel->posts_daily_search_count($search,$date);
		}
		
        $data = array();
        $nomor_urut = 0;
        if(!empty($dash))
        {
            foreach ($dash as $dash)
            {
				
				
                $cumday   = $this->DashboardModel->cum_count($dash->daily_header_id, $date);
                $sewing   = $this->DashboardModel->output_count($date, $dash->line_id, $dash->style, 'sewing');
                $outputqc = $this->DashboardModel->output_count($date, $dash->line_id, $dash->style, 'outputqc');
                $folding  = $this->DashboardModel->output_count($date, $dash->line_id, $dash->style, 'folding');

                if($folding->total==NULL){
                    $folding->total = 0;
                }
                if($sewing->total==NULL){
                    $sewing->total = 0;
                }
                if($outputqc->total==NULL){
                    $outputqc->total = 0;
                }

                //Target Efficiency
                if ($cumday!=0&&(int)$cumday<12) {
                    $cum =$cumday;
                }elseif ((int)$cumday>=12) {
                    $cum =12;
                }else{
                    $cum =1;
                }
                $target = $this->DashboardModel->eff_count($dash->line_id, $dash->change_over, $cum);

                $time = $this->jam_kerja()['time'];
				//Efficiency
				$tgl = strtotime($date);
                $now   = date("Y-m-d H:i:s",$tgl);
				
				if($date!=date("Y-m-d")){
					$jam_berjalan = 8;

				}
				else{
					$jam_awal = strtotime(date('Y-m-d 07:00:00'));
					$jam_istirahat = date('Y-m-d 11:45:00');

					//now ganti jam akhir
					if ($time>$jam_istirahat) {
						$result =abs((strtotime($now)-$jam_awal)-2700)/3600;
					}else{
						$result = abs(strtotime($now)-$jam_awal)/3600;
					}
					$jam_berjalan = $result;
				}
				
                
				//wft
				$inline = $this->DashboardModel->defect_count($date, $dash->line_id, $dash->style, 'defect_inline');
				$endline = $this->DashboardModel->defect_count($date, $dash->line_id, $dash->style, 'defect_endline');

				// $sum_line = ($inline->total+$endline->total);

				if($outputqc->total == 0){
					$wft = 0;
				}
				else{
					$wft = 100*(($inline->total+$endline->total)/$outputqc->total);
				}
				
				
				$sekarang   = date("Y-m-d H:i:s");
				$eff = $this->list_eff($sekarang,$dash->line_id,$factory);
				
				// if($dash->line_id == '17')
				// {
				// 	var_dump($sekarang);
				// 	var_dump($eff);
				// 	die();
				// }
				
                // $eff  = 100*(($dash->gsd_smv*$outputqc->total))/(floatval($jam_berjalan)*60*$dash->present_sewer);

                // $level                  = $this->db->get_where('level_user',array('level_id'=>$pobuy->level_id))->result()[0]->level_name;
                // $factory                = $this->db->get_where('master_factory',array('factory_id'=>$pobuy->factory))->result()[0]->factory_name;
                // $nestedData['no']                = (($draw-1) * 10) + (++$nomor_urut);
                if($eff['actual_eff']<$eff['target_eff']){
                    $cek = 1;
                }
                else{
                    $cek = 0;
				}

				$target_eff = $eff['target_eff'];
				$actual_eff = $eff['actual_eff'];
				
				$_temp  = '"'.$dash->daily_header_id.'"';

				
				$tgl    = '"'.$date.'"';
				$_style = '"'.$dash->style.'"';
				$co     = '"'.$dash->change_over.'"';

				$nestedData['create_date']     = $dash->create_date;
				$nestedData['line_name']       = $dash->line_name;
				$nestedData['style']           = $dash->style;
				$nestedData['start_date']      = '0';
				$nestedData['export_date']     = '0';
				$nestedData['gsd_smv']         = $dash->gsd_smv;
				$nestedData['present_sewer']   = $dash->present_sewer;
				$nestedData['present_folding'] = $dash->present_folding;
				$nestedData['working_hours']   = $dash->working_hours;
				$nestedData['change_over']     = $dash->change_over;
				$nestedData['cum_day']         = $cumday;
				$nestedData['efficiency']      = $actual_eff;
				$nestedData['target_eff']      = $target_eff;
				$nestedData['wft']             = number_format($wft,1,',',',');
				$nestedData['sewing']          = $sewing->total;
				$nestedData['qc']              = $outputqc->total;
				$nestedData['folding']         = $folding->total;
				$nestedData['cek']             = $cek;
				$nestedData['action']          = "<center><a onclick='return detail($tgl, $_temp, $dash->line_id,$_style,$dash->gsd_smv,$dash->present_sewer,$co,$cumday,$target_eff)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-th-list'></i></a> </center>";
                
				$data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }
    
    private function jam_kerja($waktu=0)
	{
		$jam[0] = array(
			'name' => '07:00 - 08:00',
			'awal' => '07:00:00',
			'akhir' => '08:00:00'
		);
		$jam[1] = array(
			'name' => '08:00 - 09:00',
			'awal' => '08:00:00',
			'akhir' => '09:00:00'
		);
		$jam[2] = array(
			'name' => '09:00 - 10:00',
			'awal' => '09:00:00',
			'akhir' => '10:00:00'
		);
		$jam[3] = array(
			'name' => '10:00 - 11:00',
			'awal' => '10:00:00',
			'akhir' => '11:00:00'
		);
		$jam[4] = array(
			'name' => '11:00-12:00',
			'awal' => '11:00:00',
			'akhir' => '12:00:00'
		);
		$jam[5] = array(
			'name' => '12:00-13:45',
			'awal' => '12:00:00',
			'akhir' => '13:45:00'
		);

		if (date("H:i:s") > $jam[5]['akhir']) {
			$jam[] = array(
				'name' => '13:45-14:45',
				'awal' => '13:45:00',
				'akhir' => '14:45:00'
			);

			$waktu = 3;
			unset($jam[1]);
			unset($jam[2]);
			$jam[0] = array(
				'name' => '07-10',
				'awal' => '07:00:00',
				'akhir' => '10:00:00'
			);
			if (date("H:i:s") > $jam[6]['akhir']) {
				$jam[] = array(
					'name' => '14:45-15:45',
					'awal' => '14:45:00',
					'akhir' => '15:45:00'
				);
				$waktu = 4;
				unset($jam[3]);
				$jam[0] = array(
					'name' => '07-11',
					'awal' => '07:00:00',
					'akhir' => '11:00:00'
				);
				if (date("H:i:s") > $jam[7]['akhir']) {
					$jam[] = array(
						'name' => '16:00-17:00',
						'awal' => '15:45:00',
						'akhir' => '17:00:00'
					);
					$waktu = 5;
					unset($jam[4]);
					$jam[0] = array(
						'name' => '07-12',
						'awal' => '07:00:00',
						'akhir' => '12:00:00'
					);
			
					if (date("H:i:s") > $jam[8]['akhir']) {
						$jam[] = array(
							'name' => '17:00-18:00',
							'awal' => '17:00:00',
							'akhir' => '18:00:00'
						);
						$waktu = 6;
						unset($jam[5]);
						$jam[0] = array(
							'name' => '07-13.45',
							'awal' => '07:00:00',
							'akhir' => '13:45:00'
						);
						if (date("H:i:s") > $jam[9]['akhir']) {
							$jam[] = array(
								'name' => '18:30-19:15',
								'awal' => '18:00:00',
								'akhir' => '19:30:00'
							);
			
							if (date("H:i:s") > $jam[10]['akhir']) {
								$jam[] = array(
									'name' => '19:30-20:30',
									'awal' => '19:30:00',
									'akhir' => '20:30:00'
								);
							}
						}
					}
				}
			}
		}

		$jam[] = array(
			'name' => 'TOTAL',
			'awal' => '06:00:00',
			'akhir' => '23:30:00'
		);
		    $timess = max(array_keys($jam));
		    return array('jam'=>$jam,'time'=>$timess,'waktu'=>$waktu);
	}

	private function sum_qc_endline($date,$jam,$style,$line,$factory,$smv,$wft_target,$sewer,$time,$waktu,$target_eff)
	{
		
		// $where = array(
		// 	'line_id'    =>$line,
		// 	'style'      =>$style,
		// 	'factory_id' =>$factory 
		// );
		$draw_qc         ='';
		$draw_target     ='';
		$draw_actual     ='';
		$draw_endline    ='';
		$draw_inline     ='';
		$draw_target_wft ='';
		$draw_actual_wft ='';
		$x         
		      =1;
		foreach ($jam as $key => $jam) {
			$awal  = $date.' '.$jam['awal'];
			$akhir = $date.' '.$jam['akhir'];

			$now   = $date.' '.date("H:i:s");
			
			$jam_awal = strtotime(($date.' 07:00:00'));
			$jam_istirahat = date(($date.' 11:45:00'));
			
			if($date!=date("Y-m-d")){
				$jam_berjalan = 8;

			}
			else{
				$jam_awal = strtotime(date('Y-m-d 07:00:00'));
				$jam_istirahat = date('Y-m-d 11:45:00');

				//now ganti jam akhir
				if ($time>$jam_istirahat) {
					$result =abs((strtotime($now)-$jam_awal)-2700)/3600;
				}else{
					$result = abs(strtotime($now)-$jam_awal)/3600;
				}
				$jam_berjalan = $result;
			}
			
    		if(strtotime($now) > strtotime($awal) && strtotime($now) < strtotime($akhir)){
    			$bg = "#CCC";
    		}else{
    			$bg = "#FFF";
			}
			
			/*JUMLAH GOOD ENDLINE*/
			$sum_endline     = $this->DashboardModel->output_count_jam($awal, $akhir, $line, $style, 'outputqc');
			$sum_goodendline =($sum_endline!=NULL?$sum_endline:0);

    		if ($sum_goodendline == 0 && strtotime(date("Y-m-d H:i:s")) < strtotime($awal)) {
    			$draw_actual .="<td class='count' style='background:#fff;'></td>";
    		}else{
    			
    			if($time > 6 && $key == 0){
					$eff  = 100*($smv*$sum_goodendline)/($waktu*60*($sewer));
				}
				else if($key < $time){
					$eff  = 100*($smv*$sum_goodendline)/(60*($sewer));
				}else{
					$eff  = 100*(($smv*$sum_goodendline))/(floatval($jam_berjalan)*60*$sewer);
				}
				
				$effw = 100*(floatval($eff)/$target_eff);

				if($eff < $target_eff){
					$bg_actual = '#F00;color:#EAEAEA';
				}else{
					$bg_actual = '#0F0'; 

				}
				$draw_actual .="<td class='count' style='background:".$bg_actual."'>". number_format($eff,1,',',',') ."</td>";
				$x++;
    		}

			
			$defect_endline     = $this->DashboardModel->defect_count_jam($awal, $akhir, $line, $style, 'defect_endline');
			$sum_defect_endline =($defect_endline!=NULL?$defect_endline:0);
			
			$defect_inline      = $this->DashboardModel->defect_count_jam($awal, $akhir, $line, $style, 'defect_inline');
			$sum_defect_inline  =($defect_inline!=NULL?$defect_inline:0);

    		if(($sum_defect_inline+$sum_defect_endline) == 0 || $sum_goodendline == 0){
				if($now > $awal){
					$draw_actual_wft.= "<td class='count' style='background:".$bg."'>0</td>";
				}else{
					$draw_actual_wft.= "<td class='count' style='background:".$bg."'></td>";
				}
			
			}else{
	    		$wft = 100*(($sum_defect_inline+$sum_defect_endline)/$sum_goodendline);
	    		
	    		if ($wft>3) {
	    			$bg_actual_wft = '#F00;color:#EAEAEA';
	    		}else{
	    			$bg_actual_wft = '#0F0';
	    		}

				$draw_actual_wft.= "<td class='count' style='background:".$bg_actual_wft."'>".number_format($wft,1,',',',')."</td>";
			}
			if($awal < $now){
				$draw_target_wft.= '<td class="count" style="background:'.$bg.'">'.$wft_target.'</td>';
    		}else{
    			$draw_target_wft.= "<td style='background:#fff'></td>";
		    }

			$draw_endline .="<td class='count' style='background:".$bg."'>".$sum_defect_endline."</td>";			
			$draw_inline  .="<td class='count' style='background:".$bg."'>".$sum_defect_inline."</td>";			
			
			$draw_qc      .="<td class='count' style='background:".$bg."'>".$sum_goodendline."</td>";
			$draw_target  .='<td class="count" style="background:'.$bg.'">'.$target_eff.'</td>';
			
		}
		$data = array(
			'draw_qc'         => $draw_qc, 
			'draw_actual'     => $draw_actual, 
			'draw_target'     => $draw_target, 
			'draw_endline'    => $draw_endline, 
			'draw_inline'     => $draw_inline, 
			'draw_target_wft' => $draw_target_wft, 
			'draw_actual_wft' => $draw_actual_wft 
		);
		return $data;
	}
	private function sum_display($date,$jam,$style,$line,$activity)
	{
		$draw='';
		foreach ($jam as $key => $jam) {
			$awal  = $date.' '.$jam['awal'];
			$akhir = $date.' '.$jam['akhir'];
			
			$now   = $date.' '.date("H:i:s");

    		if(strtotime($now) > strtotime($awal) && strtotime($now) < strtotime($akhir)){
    			$bg = "#CCC";
    		}else{
    			$bg = "#FFF";
    		}    		
			
			$get  = $this->DashboardModel->output_count_jam($awal, $akhir, $line, $style, $activity);

			$sum= ($get!=NULL?$get:0);
			
			$draw .="<td class='count' style='background:".$bg."'>".$sum."</td>";			
		}
		return $draw;
	}

	public function get_detail($post=0)
    {
		$post = $this->input->get();

		$date       = $post['tgl'];
		$id         = $post['id'];
		$line       = $post['line'];
		$style      = $post['style'];
		$smv        = $post['smv'];
		$sewer      = $post['sewer'];
		$change     = $post['change'];
		$cum        = $post['cum'];
		$target_eff = $post['target_eff'];

		//cek line
		$quer = $this->db->query("SELECT * from master_line where master_line_id=$line")->row();
		$nama = $quer->line_name;

		
		$daily = $this->DashboardModel->get_daily_id($id);
		$wft_target   = $daily->target_wft;

		$factory   = $this->session->userdata('factory');
		$jam_kerja = $this->jam_kerja();
			

		// $qc_status = $this->DashboardModel->qcStatus($line,$factory)['style'];
	
		// $dailystatus = $this->DashboardModel->getStyleDetail($qc_status, $line, $factory)->row_array();

		// $getStyleDetail = $dailystatus;

		$jam   = $jam_kerja['jam'];
		$time  = $jam_kerja['time'];
		$waktu = $jam_kerja['waktu'];
		
		$sumSewing    = $this->sum_display($date,$jam,$style,$line,'sewing');
		$sumfolding   = $this->sum_display($date,$jam,$style,$line,'sewing');
		$sumQcEndline = $this->sum_qc_endline($date,$jam,$style,$line,$factory,$smv,$wft_target,$sewer,$time,$waktu,$target_eff);
		
		
		if($date!=date("Y-m-d")){
			$last_update = "-";//date("Y-m-d H:i:s");
		}else{
			$last = $this->DisplayModel->LastUpdate($line,$style);
			$last_update = date('Y-m-d H:i:s',strtotime($last));
		}		
		
		
		$cek_permision = $this->Cekpermision_model->cekpermision(1);

		$target_hour  = $sewer*(60/$smv)*($target_eff/100);
		
		// $x = $this->Cron_model->get_detail($po);
		

		$data = array(
			'line_name'    => $nama,
			'style'        => $style,
			'detail'       => $post,
			'jam'          => $jam,
			'time'         => $time,
			'sumSewing'    => $sumSewing,
			'last_update'  => $last_update,
			'target_hour'  => ceil($target_hour),
			'sumqcendline' => $sumQcEndline['draw_qc'],
			'sumfolding'   => $sumfolding,
			'target'       => $sumQcEndline['draw_target'],
			'actual'       => $sumQcEndline['draw_actual'],
			'sum_inline'   => $sumQcEndline['draw_inline'],
			'sum_endline'  => $sumQcEndline['draw_endline'],
			'wft_target'   => $sumQcEndline['draw_target_wft'],
			'wft_actual'   => $sumQcEndline['draw_actual_wft']
		);

		// print_r($data);
		// die();
		$this->load->view('dashboard/detail_view',$data);


		// $data = array (
		// 	'style'      => $style,
		// 	'smv'        => $smv,
		// 	'sewer'      => $sewer,
		// 	'change'     => $change,
		// 	'cum'        => $cum,
		// 	'target_eff' => $target_eff
		// );

        // if ($cek_permision==0) {
        //     redirect('error_page','refresh');
        // }else{
        //     $this->load->view('dashboard/detail_view',$data);
        // }
	}

	public function list_eff($tgl,$line,$factory)
	{
		// $date = date('2019-05-29');
		$date = $tgl;

		$dashe = $this->DisplayModel->eff_daily($date,$factory,$line);

		// var_dump($dashe->result());
		// var_dump($dashe->num_rows());
		// var_dump($dashe->result());
		// die();
		$dashboard = $dashe->result();
		// $dashboard = $dashe->row();
		// if($dashe->num_rows()>1){
		// 	$dashboard = $dashe->result();
		// }
		// else{
		// 	$dashboard = $dashe->row();
		// }
		// $banyak = $dashe->num_rows();
		// $batas  = $banyak/2;
		$no     = 1;
		$nilai  = 0;
		$goal   = 0;
		$text_berjalan = '';
		// $line   = '';
		$lineid = '';
		$sum_output_aktual1 = null;
		$sum_output_aktual2 = null;
        if(!empty($dashboard))
        {
			// $i          = 0;
			$output1    = 0;
			$smv1       = 0;
			// $targeteff1 = 0;
			$jamkerja1  = 0;
			// foreach ($dash as $dash)
			foreach ($dashboard as $index => $dash)
            {
				// $query = $this->db->query("SELECT count(line_id) as total,line_id,line_name 
				// from (SELECT line_id, line_name,style from daily_view where date(create_date) = '$date' 
				// and factory_id = $factory and line_id = $dash->line_id GROUP BY line_id,line_name,style 
				// ORDER BY line_id) as daily
				// GROUP BY line_id,line_name")->result();

				$query_jam_kerja = $this->db->query("SELECT jam_mulai, created_by from daily_jam_kerja 
				where date(create_date) = '$date' and line_id ='$dash->line_id' and factory_id = '$factory' ORDER BY create_date desc")->row_array();

				if($query_jam_kerja == null){		
					$jam_kerja_mulai = '07:00:00';
				}else{		
					$jam_kerja_mulai = $query_jam_kerja['jam_mulai'];
				}
				
				$query = $this->db->query("SELECT count(line_id) as total,line_id,line_name 
				from (SELECT line_id, line_name,style from daily_view where date(create_date) = '$date' 
				and factory_id = $factory and job_status = 't' and line_id = $dash->line_id GROUP BY line_id,line_name,style 
				ORDER BY line_id) as daily
				GROUP BY line_id,line_name")->result();

				
				$time = $this->jam_kerja()['time'];
						//Efficiency
				$now   = date("Y-m-d H:i:s");

				$date = date('Y-m-d',strtotime($date));

				if($date!=date("Y-m-d")){
					$jam_berjalan = 8;
				}
				else{
					$jam_awal = strtotime(date('Y-m-d').' '.$jam_kerja_mulai);

					if($jam_kerja_mulai == '09:00:00'){
						$jam_istirahat = date('Y-m-d 13:15:00');
					}
					else{
						$jam_istirahat = date('Y-m-d 11:45:00');
					}
					//now ganti jam akhir
					if ($now>$jam_istirahat) {
						$result = abs((strtotime($now)-$jam_awal)-2700)/3600;
					}else{
						$result = abs(strtotime($now)-$jam_awal)/3600;
					}
					$jam_berjalan = $result;

					// var_dump($jam_berjalan);
					// die();
				}

				$cek=0;
				foreach($query as $q){
					// $cek=1;
					$cek = $this->db->query("SELECT * from 
					daily_view where date(create_date) = '$date' and line_id = '$q->line_id' and job_status = 't'
						and factory_id = '$factory' order by create_date");
					

					$array_style  = array();
					$array_target = array();
					$array_smv    = array();
					$array_jam    = 0;
					$array_worker = 0;
					$array_output = array();
					$outputqc     = 0;

					$query2 = $cek->result();
					$cek2 = $cek->num_rows();
					foreach ($cek->result() as $c) {
						array_push($array_style,$c->style);
						array_push($array_target,$c->target_output);
						array_push($array_smv,$c->gsd_smv);
						$array_jam = $c->working_hours;
						$array_worker = (int)$c->present_sewer + (int)$c->present_folding;

						$cek2 = $this->db->query("SELECT sum(output) as total from line_summary_po 
						where style = '$c->style' and line_id = '$q->line_id' and date = '$date'
						and activity_name = 'outputqc'")->row_array();
						
						$output = (int)$cek2['total'];
						$outputqc = $outputqc+$output;

						array_push($array_output,$output);
					}
					
					$target = 0;
					$actual = 0;
					foreach ($array_target as $key => $at) {
						$target = $target + ($at*(float)$array_smv[$key]);
						$actual = $actual + ($array_output[$key]*(float)$array_smv[$key]);
					}

					if($array_worker == 0 || $array_jam == 0){
						$actual_eff = 0;
						$target_eff = 0;
					}
					else{
						$target_eff = (($target)/($array_worker*$array_jam*60))*100;
						if($jam_berjalan >= $array_jam){
							$actual_eff = (($actual)/((int)$array_worker*(float)$array_jam*60))*100;
						}else{
							$actual_eff = (($actual)/((int)$array_worker*(float)$jam_berjalan*60))*100;
						}	
					}
				}

				$lineid = $dash->line_id;
				$no++;
			}

			// if(!empty($sum_output_aktual1) && !empty($sum_output_aktual2)){
			// 	$sum_output = (int)$sum_output_aktual1 +(int)$sum_output_aktual2;
				
			// }else{
			// 	$sum_output = (int)$outputqc->total;
			// }
			// $sum_output = $qc_output;

			$data = array(
				'actual_eff' => round($actual_eff),
				'target_eff' => round($target_eff)
			);

			return $data;
        }
	}

}
