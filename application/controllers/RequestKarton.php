<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestKarton extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$CI = &get_instance();
		date_default_timezone_set('Asia/Jakarta');
		chek_session_folding();
		$this->load->model(array('RequestKartonModel'));
		$this->fg = $CI->load->database('fg',TRUE);
	}
	public function index()
	{
		$factory_id = $this->session->userdata('factory');
		$line_id = $this->session->userdata('line_id');
		$line = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();

		$get_po = $this->RequestKartonModel->get_po($line_id, $factory_id)->result();

		$data = array(
			'line' =>$line,
			'get_po' =>$get_po
		);
		$this->template->set('title',$line['line_name'].' - Receive Loading');
		$this->template->set('desc_page','Receive Loading');
		$this->template->load('layout2','request/index_request_karton',$data);
	}

	function searchArticle(){
		$factory = $this->session->userdata('factory');
		$line	 = $this->session->userdata('line_id');
		$ih 	 = $this->input->post('ih',TRUE);
		
		// $this->db->where('inline_header_id', $ih);
		// $this->db->where('factory_id', $factory);
		// $this->db->distinct('article');
		// $data = $this->db->get('active_po_folding')->result();

        $data 	 = $this->RequestKartonModel->get_article($ih);
        echo json_encode($data);
    }

	public function report_receivekarton()
	{
		$factory_id = $this->session->userdata('factory');
		$line_id = $this->session->userdata('line_id');
		$line = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();

		$data = array(
			'line' =>$line
		);
		$this->template->set('title',$line['line_name'].' - Laporan terima Karton');
		$this->template->set('desc_page','Laporan terima karton');
		$this->template->load('layout2','request/index_report_receive_carton',$data);
	}

	// packing list
	public function packing_list()
	{
		$factory_id = $this->session->userdata('factory');
		$line_id = $this->session->userdata('line_id');
		$line = $this->db->get_where('master_line',array('master_line_id'=>$line_id))->row_array();

		$get_po = $this->RequestKartonModel->get_po($line_id, $factory_id)->result();

		$data = array(
			'line' 		=> $line,
			'get_po'	=> $get_po
		);
		$this->template->set('title','Packing List');
		$this->template->set('desc_page','Packing List');
		$this->template->load('layout2','request/index_packing_list',$data);
	}

	public function searchSize()
	{
		$factory_id = $this->session->userdata('factory');
		$inline_header_id = $this->input->post('inline_header_id');

		$this->db->where('inline_header_id', $inline_header_id);
		$this->db->where('factory_id', $factory_id);
		$data = $this->db->get('active_po_folding_new')->result();

		echo json_encode($data);
	}

	// sewing request
	public function submitRequest()
	{
		$nik = $this->session->userdata('nik');

		$factory_id = $this->session->userdata('factory');
		$line_id = $this->session->userdata('line_id');


		$response = 0;

		$this->fg->where('name', $line_id);
		$this->fg->where('factory_id', $factory_id);
		$this->fg->where('deleted_at is null');
		$line_fg = $this->fg->get('line')->row_array()['id'];

		if (!$line_fg) {
			return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Line Packing Tidak ditemukan..!'));
		}

		$now = date('Y-m-d H:i:s');
		$status_sewing = 'request';

		if ($factory_id == 1) {
			$user_fg = 45;
		}elseif ($factory_id == 2) {
			$user_fg = 44;
		}

		$data_header = [
							'request_at' => $now,
							'line_id' => $line_fg,
							'status_sewing' => $status_sewing,
							'created_at' => $now
						];

		$data_detail = [];

		try {
            // $this->fg->trans_begin();
            $this->fg->trans_start();

            // insert header
            $this->fg->insert('request_carton', $data_header);
            $insert_id = $this->fg->insert_id();

            foreach ($_POST['dynamic_req']['poreference'] as $key => $value) {
					$detail['po_number'] = $_POST['dynamic_req']['poreference'][$key];
					$detail['manufacturing_size'] = $_POST['dynamic_req']['size'][$key];
					$detail['carton_qty'] = $_POST['dynamic_req']['qty'][$key];
					$detail['all_size'] = $_POST['dynamic_req']['check_all_size'][$key];					
					$detail['buyer_item'] = $_POST['dynamic_req']['article'][$key];
					$detail['last_carton'] = $_POST['dynamic_req']['check_last_carton'][$key];
					$detail['request_id'] = $insert_id;
					$detail['created_at'] = $now;
					$detail['created_at'] = $now;
					$data_detail[] = $detail;
			}

			$data_movement = [
								'request_id' => $insert_id,
								'line_id' => $line_fg,
								'status_sewing_to' => 'request',
								'user_id' => $user_fg,
								'description' => 'set request carton',
								'created_at' => $now,
								'nik' => $nik
							];

			// insert detail
			foreach ($data_detail as $key_ => $value_) {
				$this->fg->insert('request_carton_detail', $value_);
			}

			// insert movement
			$this->fg->insert('request_movement', $data_movement);

            // $this->fg->trans_commit();
            $this->fg->trans_complete();
       		$this->fg->trans_status() === FALSE ? FALSE : TRUE ;

            $response = 1;

            }
            catch (Exception $e) {
             $this->fg->trans_rollback();
             log_message('error', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, $e->getCode(), $e->getMessage(), print_r($this->fg->last_query(), TRUE)));
            }

        echo json_encode($response);

	}

	public function ajaxGetListRequest()
	{
		$factory_id = $this->session->userdata('factory');
		$line_id = $this->session->userdata('line_id');

		$this->fg->where('name', $line_id);
		$this->fg->where('factory_id', $factory_id);
		$this->fg->where('deleted_at is null');
		$line_fg = $this->fg->get('line')->row_array()['id'];

		$list_req = $this->RequestKartonModel->get_list_request($line_fg, $factory_id)->result_array();

		$data = array();

		if (!empty($list_req)) {
			foreach ($list_req as $key => $value) {
				// status packing
				if ($value['status_packing'] == 'onprogress') {
                  $status_packing = '<span class="badge badge-info position-right">ON PROGRESS</span>';
                  $button = '';
                }elseif ($value['status_packing'] == 'done') {
                  $status_packing = '<span class="badge badge-success position-right">DONE</span>';
                  $button = '<button class="btn btn-success btn-xs btn-receive" data-po="'.$value['po_number_concat'].'" data-article="'.$value['buyer_item_concat'].'" data-manufaturingsize="'.$value['manufacturing_size_concat'].'" data-customersize="'.$value['customer_size_concat'].'" data-id="'.$value['id'].'" data-line="'.$value['line_id'].'" value="" onclick="receive_carton(this);" data-toggle="tooltip" title="Terima Karton"><i class="fa fa-download" style="font-size:1em;"></i></button>';
                }elseif ($value['status_packing'] == 'hold') {
                  $status_packing = '<span class="badge badge-error position-right">HOLD</span>';
                  $button = '<button class="btn btn-danger btn-xs btn-cancel" data-po="'.$value['po_number_concat'].'" data-article="'.$value['buyer_item_concat'].'" data-manufaturingsize="'.$value['manufacturing_size_concat'].'" data-customersize="'.$value['customer_size_concat'].'" data-id="'.$value['id'].'" data-line="'.$value['line_id'].'" value="" onclick="cancel_carton(this);" data-toggle="tooltip" title="Cancel Permintaan Karton"><i class="fa fa-times" style="font-size:1em;"></i></button>';
                }else{
                  $status_packing = '<span class="badge position-right">Waiting</span>';
                  $button = '<button class="btn btn-danger btn-xs btn-cancel" data-po="'.$value['po_number_concat'].'" data-article="'.$value['buyer_item_concat'].'" data-manufaturingsize="'.$value['manufacturing_size_concat'].'" data-customersize="'.$value['customer_size_concat'].'" data-id="'.$value['id'].'" data-line="'.$value['line_id'].'" value="" onclick="cancel_carton(this);" data-toggle="tooltip" title="Cancel Permintaan Karton"><i class="fa fa-times" style="font-size:1em;"></i></button>';
                }

                // status sewing
                if ($value['status_sewing'] == 'request') {
                  $status_sewing = '<span class="badge badge-info position-right">REQUEST</span>';
                }elseif ($value['status_sewing'] == 'receive') {
                  $status_sewing = '<span class="badge badge-success position-right">RECEIVED</span>';
				}
				
				$list['id'] = $value['id'];
				$list['request_at'] = $value['request_at'];
				$list['po_number_concat'] = $value['po_number_concat'];
				$list['buyer_item_concat'] = $value['buyer_item_concat'];
				$list['manufacturing_size_concat'] = $value['manufacturing_size_concat'];
				$list['carton_qty_concat'] = $value['carton_qty_concat'];
				$list['status_packing'] = $status_packing;
				$list['status_sewing'] = $status_sewing;
				$list['info'] = $value['info'];
				$list['action'] = $button;

				// <i class="fa fa-download" style="font-size:0.85em;"></i>

				$data[] = $list;
			}
		}


		echo json_encode(['data' => $data]);
	}

	// receive sewing
    public function submitRequestReceive() {
      $factory_id = $this->session->userdata('factory');
	  $po_number = $this->input->post('po_number');
	  $article = $this->input->post('article');
      $id = $this->input->post('id');

      if ($factory_id == 1) {
			$user_fg = 45;
		}elseif ($factory_id == 2) {
			$user_fg = 44;
		}

      $now = date('Y-m-d H:i:s');

      $response = 0;

     //check request carton
      $check = $this->_check_request($id);
      if(!$check) {
          return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Permintaan tidak ditemukan..!'));
      }

      if($check['status_packing'] != 'done') {
          return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Status Packing Belum selesai..!'));
      }

      $data_movement = [
          'request_id' => $id,
          'status_sewing_from' => $check['status_sewing'],
          'status_sewing_to' => 'receive',
          'status_packing_from' => $check['status_packing'],
          'user_id' => $user_fg,
          'created_at' => $now,
          'description' => 'receive by sewing',
      ];

      try {
        // $this->fg->trans_begin();
        $this->fg->trans_start();

        //  request movement
        $this->fg->where('request_id', $id);
	    $this->fg->where('is_canceled', false);
	    $this->fg->where('deleted_at is null');

	    $last_data = $this->fg->get('request_movement')->row_array();

	      if(!$last_data) {
	            return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Request not found..!'));
	        }
	        else {
	        	$this->fg->update('request_movement', ['deleted_at' => $now], ['id' => $last_data['id']]);
	        }

	      // insert history
	      $this->fg->insert('request_movement', $data_movement);

	      // update request carton
	      $this->fg->update('request_carton', ['status_sewing' => 'receive', 'updated_at' => $now], ['id' => $id]);

          // update detail request carton
          $this->fg->update('request_carton_detail', ['updated_at' => $now], ['request_id' => $id]);

        // $this->fg->trans_commit();
        $this->fg->trans_complete();
       	$this->fg->trans_status() === FALSE ? FALSE : TRUE ;

        $response = 1;

        }
        catch (Exception $e) {
         $this->fg->trans_rollback();
         log_message('error', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, $e->getCode(), $e->getMessage(), print_r($this->fg->last_query(), TRUE)));
        }

        echo json_encode($response);

    }

    // cancel sewing
    public function submitRequestCancel() {
      $factory_id = $this->session->userdata('factory');
      $po_number = $this->input->post('po_number');
      $id = $this->input->post('id');

      if ($factory_id == 1) {
			$user_fg = 45;
		}elseif ($factory_id == 2) {
			$user_fg = 44;
		}

      $now = date('Y-m-d H:i:s');

      $response = 0;

     //check request carton
      $check = $this->_check_request($id);
      if(!$check) {
          return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Permintaan tidak ditemukan..!'));
      }

      if($check['status_packing'] !== null && $check['status_packing'] !== 'hold') {
          return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Status Packing Sudah terima permintaan..!'));
      }

      if($check['status_sewing'] == 'receive') {
          return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Permintaan karton telah diterima..!'));
      }

      $data_movement = [
          'request_id' => $id,
          'status_sewing_from' => $check['status_sewing'],
          'status_sewing_to' => 'cancel',
          'status_packing_from' => $check['status_packing'],
          'user_id' => $user_fg,
          'created_at' => $now,
          'description' => 'cancel by sewing',
      ];

      try {
        // $this->fg->trans_begin();
        $this->fg->trans_start();

        //  request movement
        $this->fg->where('request_id', $id);
	    $this->fg->where('is_canceled', false);
	    $this->fg->where('deleted_at is null');

	    $last_data = $this->fg->get('request_movement')->row_array();

	      if(!$last_data) {
	            return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Request not found..!'));
	        }
	        else {
	        	$this->fg->update('request_movement', ['deleted_at' => $now], ['id' => $last_data['id']]);
	        }

	      // insert history
	      $this->fg->insert('request_movement', $data_movement);

          // update detail request carton
          $this->fg->update('request_carton_detail', ['updated_at' => $now, 'deleted_at' => $now], ['request_id' => $id]);

          // update request carton
	      $this->fg->update('request_carton', ['status_sewing' => 'cancel', 'updated_at' => $now, 'deleted_at' => $now], ['id' => $id]);

        // $this->fg->trans_commit();
        $this->fg->trans_complete();
       	$this->fg->trans_status() === FALSE ? FALSE : TRUE ;

        $response = 1;

        }
        catch (Exception $e) {
         $this->fg->trans_rollback();
         log_message('error', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, $e->getCode(), $e->getMessage(), print_r($this->fg->last_query(), TRUE)));
        }

        echo json_encode($response);

    }

    public function report_receive_carton_ajax()
	{
		$columns = array(
                            0 =>'request_at',
                            1 =>'description',
                            2 =>'po_number_concat',
                            3 =>'manufacturing_size_concat',
                            4 =>'carton_qty_concat',
                            5 =>'done',
                            6 =>'receive',
                        );
		$limit   = $this->input->post('length');
		$start   = $this->input->post('start');
		$order   = $columns[$this->input->post('order')[0]['column']];
		$dir     = $this->input->post('order')[0]['dir'];
		$draw    = $this->input->post('draw');
		$factory = $this->session->userdata('factory');
		$line    = $this->session->userdata('line_id');

		$this->fg->where('name', $line);
		$this->fg->where('factory_id', $factory);
		$this->fg->where('deleted_at is null');
		$line_fg = $this->fg->get('line')->row_array()['id'];

		// if (!$line_fg) {
		// 	return $this->output->set_status_header('422')->set_content_type('application/json')->set_output(json_encode('Line Packing Tidak ditemukan..!'));
		// }

        $date    = $this->input->post('tanggal');

        $totalData = $this->RequestKartonModel->allposts_count_receive($line_fg,$factory);

        $totalFiltered = $totalData;
        if($date =='')
        {
            $master = $this->RequestKartonModel->allposts_receive($limit,$start,$order,$dir,$line_fg,$factory);
        }
        else {

            $search = $this->input->post('search')['value'];

            $master =  $this->RequestKartonModel->posts_search_receive($limit,$start,$search,$order,$dir,$line_fg,$factory,$date);

            $totalFiltered = $this->RequestKartonModel->posts_search_count_receive($search,$line_fg,$factory,$date);
        }
		$data          = array();
		$nomor_urut    = 0;

        if(!empty($master))
        {
            foreach ($master as $master)
            {
            	$_temp = '"'.$master->request_at.'","'.$master->description.'","'.$master->po_number_concat.'","'.$master->manufacturing_size_concat.'","'.$master->carton_qty_concat.'","'.$master->done.'","'.$master->receive.'"';
				$nestedData['no']          					= (($draw-1) * 10) + (++$nomor_urut);
				$nestedData['request_at']       			= $master->request_at;
				$nestedData['description'] 					= $master->description;
				$nestedData['po_number_concat']        		= $master->po_number_concat;
				$nestedData['buyer_item_concat']        	= $master->buyer_item_concat;
				$nestedData['manufacturing_size_concat']    = $master->manufacturing_size_concat;
				$nestedData['carton_qty_concat']         	= $master->carton_qty_concat;
				$nestedData['done']     					= $master->done;
				$nestedData['receive']      				= $master->receive;

				$data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        echo json_encode($json_data);
	}

    private function _check_request($request_id)
    {

    	$this->fg->where('id', $request_id);
    	$this->fg->where('deleted_at is null');
    	$check = $this->fg->get('request_carton')->row_array();

      if (!$check) {
        return false;
      }else{
        return $check;
      }

    }

    // show packing list

    function show_packing_list()
    {
        $poreference = $this->input->get('inputPoreference');

        $data = '';
        $data_po = '';
        $data_calculate = '';

        if ($poreference != '') {
        	// get po_summary
        	$data_po = $this->RequestKartonModel->gets_po_summary($poreference)->row_array();
        	// package calculate
        	$data_calculate = $this->RequestKartonModel->gets_po_calculate($poreference)->row_array();
        	// package detail
            $data = $this->RequestKartonModel->gets_packing_list($poreference)->result_array();

        }

        echo json_encode(array('data' => $data, 'data_po' => $data_po, 'data_calculate' => $data_calculate));
    }

}

/* End of file RequestKarton.php */
/* Location: ./application/controllers/RequestKarton.php */
