<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_mesin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Mesin_model','Cekpermision_model'));
        chek_session();
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
        // $cek_permision = $this->Cekpermision_model->cekpermision(1);
        
        // if ($cek_permision==0) {
        //     redirect('error_page','refresh');
        // }else{
            $this->template->set('title','Master Mesin');
            $this->template->set('desc_page','Master Mesin');
            $this->template->load('layout','master/mesin/list_mesin_view');
        // }

    }
    public function add_mesin()
    {
        // $cek_permision = $this->Cekpermision_model->cekpermision(1);

        // if ($cek_permision==0) {
        //     redirect('error_page','refresh');
        // }else{
            $this->load->view('master/mesin/add_mesin');
        // }        
    }
    
    public function save_mesin($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
            $kode           = $post['kode'];
            $adidasname     = $post['adidas'];
            $grupname       = $post['grup_mesin'];
            $kategori_mesin = $post['kategori_mesin'];
            $machine_delay  = $post['machine_delay'];
			
            
            $kode_mesin = strtoupper($kode);

			$cek = $this->db->query("SELECT * FROM master_mesin where nama_mesin = '$kode_mesin'
            and adidas_name = '$adidasname' and machine_group_name = '$grupname' and kategori_mesin = '$kategori_mesin'");

            if($cek->num_rows()>0){
                $status = 2;
				$pesan = "DATA YANG DI INPUT SUDAH ADA";
            }else{

                $nomor = $this->db->query("SELECT max(mesin_id) as max from master_mesin")->row_array();
    
                $data = array(
                    'mesin_id'           => $nomor['max']+1,
                    'nama_mesin'         => $kode_mesin,
                    'adidas_name'        => $adidasname,
                    'machine_group_name' => $grupname,
                    'kategori_mesin'     => $kategori_mesin,
                    'machine_delay'      => $machine_delay
                );
                $this->db->trans_start();
                $this->db->insert('master_mesin', $data);
                $this->db->trans_complete();
				$status = 1;
				$pesan = "SUKSES";
			}
		}else{
			$status = 3;
			$pesan = "DATA ADA YANG KOSONG";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
	}
    
    public function listmaster()
    {
        $columns = array( 
                            0 =>'mesin_id', 
                            1 =>'nama_mesin',
                            2 =>'adidas_name',
                            3 =>'machine_group_name',
                            4 =>'kategori_mesin',
                            5 =>'machine_delay'
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $totalData = $this->Mesin_model->allposts_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $master = $this->Mesin_model->allposts($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $master =  $this->Mesin_model->posts_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Mesin_model->posts_search_count($search);
        }

        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
            foreach ($master as $key=>$master)
            {
                $_temp                  = '"'.$master->mesin_id.'"';
                
                $nestedData['no']                 = (($draw-1) * 10) + (++$nomor_urut);
                $nestedData['kode_mesin']         = ucwords($master->nama_mesin);
                $nestedData['adidas_name']        = ucwords($master->adidas_name);
                $nestedData['machine_group_name'] = ucwords($master->machine_group_name);
                $nestedData['kategori_mesin']     = ucwords($master->kategori_mesin);
                $nestedData['machine_delay']      = ucwords($master->machine_delay);
                $nestedData['action']             = "<center><a onclick='return edit($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-pencil'></i></a> <a onclick='return delete_mesin($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash-o'></i></a></center>";
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
    }
    
    public function edit($post=0)
    {
		$post          = $this->input->get();
		$id            = $post['id'];
		// $cek_permision = $this->Cekpermision_model->cekpermision(1);
		
		$x = $this->Mesin_model->getData($id);
		
		$data = array (
			'mesin' => $x,
		);

        // if ($cek_permision==0) {
        //     redirect('error_page','refresh');
        // }else{
            $this->load->view('master/mesin/edit_mesin',$data);
        // }
    }
    
    public function edit_mesin($post=NULL)
    {
		$post = $this->input->post();
		
        if ($post!='') {
            $mesin_id       = $post['mesin_id'];
            $kode           = $post['kode'];
            $adidasname     = $post['adidas'];
            $grupname       = $post['grup_mesin'];
            $kategori_mesin = $post['kategori_mesin'];
            $machine_delay  = $post['machine_delay'];
            
            $kode_mesin = strtoupper($kode);

            $data = array(
                'nama_mesin'         => $kode,
                'adidas_name'        => $adidasname,
                'machine_group_name' => $grupname,
                'kategori_mesin'     => $kategori_mesin,
                'machine_delay'      => $machine_delay
            );

                $this->db->trans_start();
                $this->db->where('mesin_id',$mesin_id);
                $this->db->update('master_mesin',$data);
                $this->db->trans_complete();
				$status = 1;
				$pesan = "SUKSES";
		}else{
			$status = 3;
			$pesan = "DATA ADA YG KOSONG";
		}

		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
        
    }
    
    public function delete_mesin($post=NULL)
    {
		$post = $this->input->get();
    
        if ($post!='') {
            $mesin_id   = $post['id'];
        
            $this->db->trans_start();
            $this->db->where('mesin_id',$mesin_id);
            $this->db->delete('master_mesin');
            $this->db->trans_complete();
            // $status = 1;
            echo '1';
            // $pesan = "SUKSES";
		}else{
            // $status = 3;
            echo '3';
			// $pesan = "DATA TIDAK DITEMUKAN";
		}
        
	}	

}

/* End of file master_process.php */
/* Location: ./application/controllers/master_process.php */
