<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resewing extends CI_Controller {

	function __construct()
    {
  		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		chek_session();
		$this->load->model('Cekpermision_model');
		$this->load->model('ResewingModel');
		
    }

    public function index()
	{
        $cek_permision = $this->Cekpermision_model->cekpermision(2);

        if ($cek_permision==0) {
            redirect('error_page','refresh');
        }else{
            $this->template->set('title','Reactive Style Sewing');
            $this->template->set('desc_page','Reactive Style Sewing');
            $this->template->load('layout','resewing/reactive_sewing');
        }
	}

	public function showLine()
	{
		$draw='';
		$no = 1;
		$this->db->order_by('no_urut');
		$this->db->where('factory_id', $this->session->userdata('factory'));
		$this->db->where('active', 't');
		$line = $this->db->get('master_line');

		foreach ($line->result() as $key => $l) {
			$_temp = '"'.$l->master_line_id.'","'.$l->line_name.'"';
			$draw .="<tr onclick='return selectline($_temp)'>".
						"<td>".$l->line_name."</td>".
			       '</tr>';
		}

		$data = array('draw' => $draw );
		$this->load->view('resewing/showLine', $data);
	}

	public function search_sewing($post=NULL)
	{
		$post 	= $this->input->post();

		if ($post!=NULL) {

			$line    = $post['line'];

			$query = $this->db->query("SELECT * FROM master_line WHERE master_line_id = '$line'")->row();

			$nama_line  = $query->line_name;

			$data = array (
				'line' => $line,
				'nama_line' => $nama_line
			);
		
			$this->load->view("resewing/index_resewing",$data);
		}
	}

	public function list_style_sewing()
    {
        $columns = array( 
							0  => 'line_id',
							1  => 'style',
							2  => 'status',
                        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

  
		$line         = $this->input->post('line');

        $totalData = $this->ResewingModel->allposts_style_count($line);
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $dash = $this->ResewingModel->allposts_style($limit,$start,$order,$dir,$line);
        }
        else {
			$search = $this->input->post('search')['value'];
			
            $dash =  $this->ResewingModel->posts_style_search($limit,$start,$order,$dir,$search,$line);

            $totalFiltered = $this->ResewingModel->posts_style_search_count($search,$line);
		}
		
        $data = array();
        $nomor_urut = 0;
        if(!empty($dash))
        {
            foreach ($dash as $dash)
            {
				$target = $dash->targetqty;
				$sewing = $dash->qty_sewing;

				if((int)$target > (int)$sewing){
					$alert = '<i class="btn-xs btn-info btn-icon-pg">';
					$status_style = "$alert ACTIVE </i>";
				}
				else{
					$alert = '<i class="btn-xs btn-danger btn-icon-pg">';
					$status_style = "$alert NOT ACTIVE </i>";
				}

				$nestedData['line']   = $dash->line_name;
				$nestedData['style']  = $dash->style;
				$nestedData['status'] = $status_style;

				$temp = '"'.$dash->line_id.'","'.$dash->style.'","'.$dash->targetqty.'","'.$dash->qty_sewing.'"';

				$nestedData['action']      = "<center><a onclick='return resewing($temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-repeat'></i> REACTIVE</a> </center>";
                
				$data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);
	}
 
	public function reactive_sewing($post = NULL)
    {
		$post = $this->input->post();
		$line   = $post['line'];
		$style  = $post['style'];
		$target = $post['target'];
		$sewing = $post['sewing'];

		$pertambahan = ((int)$sewing - (int)$target)+(int)1;
		
		$query = $this->db->query("SELECT * from inline_header where style='$style' and line_id= '$line' limit 1")->row();

		$q_inlineheader = $query->inline_header_id;
		$q_target       = $query->targetqty;

		$hasil = (int)$q_target + (int)$pertambahan;

		$this->db->trans_start();

		$this->db->query("UPDATE inline_header SET targetqty = $hasil where style='$style'
		and line_id= '$line' and inline_header_id = '$q_inlineheader'");
		

		if ($this->db->trans_status() == FALSE)
		{
			$this->db->trans_rollback();
			$status = 500;
			$pesan  = "Insert Data gagal";
		}else{
			$this->db->trans_complete();
			$status = 200;
			$pesan  = "SUKSES";
		}
		
		$data = array(
			'status'     => $status, 
			'pesan'      => $pesan, 
		);
		echo json_encode($data);
	}

	public function download($post=NULL)
    {
		$post = $this->input->post();
        $columns = array( 
							0  => 'no_urut',
							1  => 'no_urut',
                        );

        $limit = NULL;
        $start = NULL;
        $order = NULL;
        $dir   = NULL;
        $draw  = $this->input->post('draw');


		$search_tgl     = $_GET['tgl'];
		$search_po      = $_GET['po'];
		$search_factory = $_GET['factory'];
		$search_style   = $_GET['style'];
		// $this->input->post('style');

		// if($search_po == 'a'){
		// 	$search_po = null;
		// }

		// if($search_style == 'a'){
		// 	$search_style = null;
		// }



		// $date    = $search_tgl;

  		if ($search_tgl!='') {
  			$date = date('Y-m-d',strtotime($search_tgl));
  		}else{
  			$date = date('Y-m-d');
		}

        $totalData = $this->LaporanModel->allposts_daily_count($date,$search_factory);
            
        $totalFiltered = $totalData; 
            
        if(empty($search_po) && empty($search_style))
        {            
            $dash = $this->LaporanModel->allposts_daily($limit,$start,$order,$dir,$date,$search_factory);
        }
        else {
			
            $dash =  $this->LaporanModel->posts_daily_search($limit,$start,$order,$dir,$date,$search_po,$search_style,$search_factory);

            $totalFiltered = $this->LaporanModel->posts_daily_search_count($search_po,$search_style,$date,$search_factory);
		}
		
        $data = array();
		$nomor_urut = 0;
		$excel = "'\@'";
        if(!empty($dash))
        {
            foreach ($dash as $dash)
            {
				$nestedData['date']        = $date;
				$nestedData['factory_name'] = $dash->factory_name;
				$nestedData['line_name']   = $dash->line_name;
				$nestedData['style']       = $dash->style;
				$nestedData['poreference'] = $dash->poreference;
				$nestedData['size']        = $dash->size;
				$nestedData['wip_qc']      = $dash->wip_sewing;
				$nestedData['wip_folding'] = $dash->wip_folding;
				$nestedData['output']      = $dash->folding_output;

				$query = $this->db->query("SELECT DISTINCT component_name from distribusi_detail_view 
				where poreference = '$dash->poreference' and size ='$dash->size' and style = '$dash->style' and line_id='$dash->line_id'")->result();

// <td class="text-center">'.$nestedData['poreference'].'</td>
				if($query==NULL){
					$baris = '<tr>
					<td class="text-center">'.$nestedData['date'].'</td>
					<td class="text-center">'.$nestedData['factory_name'].'</td>
					<td class="text-center">'.$nestedData['line_name'].'</td>
					<td class="text-center">'.$nestedData['style'].'</td>
					<td style="mso-number-format:'.$excel.'" class="text-center">'.$nestedData['poreference'].'</td>
					<td class="text-center">'.$nestedData['size'].'</td>
					<td class="text-center">'.$nestedData['wip_qc'].'</td>
					<td class="text-center">'.$nestedData['wip_folding'].'</td>
					<td class="text-center">'.$nestedData['output'].'</td>
					<td class="text-center">-</td>
					<td class="text-center">'.$dash->wip_sewing.'</td>
					</tr>';
					$data[] = $baris;
				}
				else{
					foreach ($query as $q) {
						$nama_component = $q->component_name;
	
						$baris = '<tr>
						<td class="text-center">'.$nestedData['date'].'</td>
						<td class="text-center">'.$nestedData['factory_name'].'</td>
						<td class="text-center">'.$nestedData['line_name'].'</td>
						<td class="text-center">'.$nestedData['style'].'</td>
						<td style="mso-number-format:'.$excel.'" class="text-center">'.$nestedData['poreference'].'</td>
						<td class="text-center">'.$nestedData['size'].'</td>
						<td class="text-center">'.$nestedData['wip_qc'].'</td>
						<td class="text-center">'.$nestedData['wip_folding'].'</td>
						<td class="text-center">'.$nestedData['output'].'</td>
						<td class="text-center">'.$nama_component.'</td>
						<td class="text-center">'.$dash->wip_sewing.'</td>
						</tr>';
						$data[] = $baris;
					}
				}
        	}
        }
          
        $json_data = array(
                    "data" => $data,
					);
		$this->load->view("laporan/report_view",$json_data);
	}

}
