<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_cap extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('CapModel'));
        date_default_timezone_set('Asia/Jakarta');
	}

	
	public function capendline()
	{
		if (!$this->input->is_cli_request()) {
			redirect('auth','refresh');
		}
		$DateTime        = new DateTime();
		$DateTime->modify('+2 hours');
		$sewing_deadline =  $DateTime->format("Y-m-d H:i:s");
		
		$capheader       = array();
		$insertdetail    = array();
		$_line           = array();
		$date            = date('Y-m-d');
		$_temp           = array();

		$this->db->where('date', date('Y-m-d'));	
		$wft             = $this->db->get('wft_intv');

		 $now = date("H");

		 if ($wft->num_rows()>0) {
		 	foreach ($wft->result() as $key => $w) {
			 	$wft = number_format((float)$w->wft, 2, '.', '');
			 	
			 	if ($now>=7&&$now<9) {
			 		$jam = 1;
			 	}else if ($now>=9&&$now<11) {
			 		$jam = 2;
			 	}else if ($now>=11&&$now<13){
			 		$jam = 3;
			 	}else if ($now>=13&&$now<15){
			 		$jam = 4;
			 	}else if ($now>=15&&$now<17){
					$jam = 5;
			 	}else if ($now>=16&&$now<18){
			 		$jam = 6;
			 	}else if ($now>=18&&$now<20){
			 		$jam = 7;
			 	}else{
			 		$jam = 8;
			 	}

				if ($wft>5) {

					$this->db->where('to_char(create_date,\'YYYY-mm-dd\')',$date);
			 		$this->db->where(array('line_id'=>$w->line_id,'style'=>$w->style,'wft'=>$wft));
			 		$cek = $this->db->get('cap_endline');
			 		
			 		// if ($cek->num_rows()==0) {
			 			$_temp_id = $this->uuid->v4();
			 			$line_id = $w->line_id;
			 			$capheader [] = array(
							'cap_endline_id' => $_temp_id, 
							'line_id'        => $line_id, 
							'wft'            => $wft,
							'style'          => $w->style,  
							'jam'          => $jam,  
							'factory_id'     => $w->factory_id,  
					 	);
					 	
					 	
					 	$_temp [] = $_temp_id;
					 	$_line [] = $line_id;
			 		// }
			 	
			 	}
			 }
			 $this->db->insert_batch('cap_endline', $capheader);
			 
			 if (count($capheader)>0) {
			 	$this->db->where_not_in('cap_endline_id', $_temp);

			 	$this->db->where('status', 'cap verify');
				$this->db->where('date',$date);
				$measure = $this->db->get('cap_endline_view');
				

				foreach ($measure->result() as $key => $m) {
					$this->db->where('cap_endline_detail_id', $m->cap_endline_detail_id);
					$this->db->update('cap_endline_detail', array('status'=>'close'));
				}
			 	$this->db->where_in('cap_endline_id', $_temp);
			 	$header = $this->db->get('cap_endline');

			 	foreach ($header->result() as $key => $h) {
			 		$this->db->limit(3);
			 		$this->db->where(array('style'=>$h->style,'line_id'=>$h->line_id,'factory_id'=>$h->factory_id));
			 		$detail = $this->db->get('count_defect_intv');

			 		
			 		foreach ($detail->result() as $key => $d) {
			 			$insertdetail[] = array(
							'cap_endline_detail_id' => $this->uuid->v4(),
							'cap_endline_id'        =>$h->cap_endline_id,
							'defect_id'             =>$d->defect_id,
							'total_defect'          =>$d->total,
							'sewing_deadline_date'  =>$sewing_deadline
			 			);
			 		}
			 		
			 	}
			 	
			 	$this->db->insert_batch('cap_endline_detail', $insertdetail);		 	
		 	
		 }
		}

	}
}

/* End of file Cron_cap.php */
/* Location: ./application/controllers/Cron_cap.php */