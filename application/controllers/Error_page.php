<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_page extends CI_Controller {

	public function index()
	{
		$this->template->set('title','Error Page');
		$this->template->set('desc_page','Master Proses');
        $this->template->load('layout','errors/errorpage_view');
	}

}

/* End of file error_page.php */
/* Location: ./application/controllers/error_page.php */