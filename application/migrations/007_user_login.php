<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_User_login extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->drop_table('role_user', TRUE);

		// Table structure for table 'groups'
		$this->dbforge->add_field(array(
			'level_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'role_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
			),
			'description' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			)
		));
		$this->dbforge->add_key('level_id', TRUE);
		$this->dbforge->create_table('level_user');

		$this->dbforge->add_field(array(
			'users_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'nik' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'active' => array(
				'type' => 'TINYINT',
				'constraint' => '1',
				'unsigned' => TRUE,
				'null' => TRUE
			),
			'factory' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'role_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'create_date'		=>array(
				'type'      => 'timestamp',
                'on update' => 'NOW()',
                'null' => TRUE
			),

		));
		$this->dbforge->add_key('users_id', TRUE);
		$this->dbforge->create_table('users');

		$data = array(
			'users_id' => '1',
			'nik' => '22222222',			
			'username' => 'ICT Admin AOI2',
			'password' => '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36',
			'email' => 'admin@aoi.co.id.com',
			'active' => '1',
			'factory' => 'AOI2',
			'role_id' => '1',
		);
		$this->db->insert('users', $data);

		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'ip_address' => array(
				'type' => 'VARCHAR',
				'constraint' => '16'
			),
			'login' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'time' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('login_attempts');
	}

	public function down() {
		
	}

}

/* End of file 007_user_login.php */
/* Location: ./application/migrations/007_user_login.php */