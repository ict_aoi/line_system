<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_mo extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'master_mo_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'doc_mo' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE
			),
			'style'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'poreference'	=>array(
				'type'		=>'VARCHAR',
				'constraint'=>15,
				'unsigned'	=>TRUE
			),
			'kst_joborder'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>70,
				'unsigned'	=>TRUE
			),
			'product_code'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>100,
				'unsigned'	=>TRUE
			),
			'warehouse'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'workflow'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'size'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>15,
				'unsigned'	=>TRUE
			),
			'qtyordered' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE
			),
			'kst_lcdate'		=>array(
				'type'      => 'timestamp',
                'null' => TRUE
			),
			'datestartschedule'		=>array(
				'type'      => 'timestamp',
                'null' => TRUE
			),
			'datefinishschedule'		=>array(
				'type'      => 'timestamp',
                'null' => TRUE
			),
			'kst_statisticaldate'		=>array(
				'type'      => 'timestamp',
                'null' => TRUE
			),
			'create_date'		=>array(
				'type'      => 'timestamp',
                'on update' => 'NOW()'
			),
		));
		$this->dbforge->add_key('master_mo_id', TRUE);
		$this->dbforge->create_table('master_mo');
	}

	public function down() {
		
	}

}

/* End of file 018_master_mo.php */
/* Location: ./application/migrations/018_master_mo.php */	