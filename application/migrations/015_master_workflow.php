<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_workflow extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
	$this->dbforge->add_field(array(
		'master_workflow_id' => array(
		'type' => 'MEDIUMINT',
		'constraint' => '8',
		'unsigned' => TRUE,
		'auto_increment' => TRUE
	),
	'style' => array(
	'type'		=> 'VARCHAR',
	'constraint'=> 50,
	'unsigned'	=> TRUE
	),
	'master_proses_id' => array(
		'type' => 'MEDIUMINT',
		'constraint' => '8',
		'unsigned' => TRUE,
		'auto_increment' => TRUE
	),
	'create_date'		=>array(
		'type'      => 'timestamp',
        'on update' => 'NOW()',
        'null' => TRUE
	),
	'update_at'		=>array(
		'type'      => 'timestamp',
        'null' => TRUE
	),
	));
		$this->dbforge->add_key('master_workflow_id', TRUE);
		$this->dbforge->create_table('master_workflow');
	}

	public function down() {
		
	}

}

/* End of file 011_master_proses.php */
/* Location: ./application/migrations/011_master_proses.php */