<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Round_status extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'round_status_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE,
			'auto_increment' => TRUE
		),
		'inline_header_id' => array(
			'type' => 'VARCHAR',
			'constraint' => '50',
			'unsigned' => TRUE
		),
		'round' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE
		),
		'create_date'		=>array(
			'type'      => 'timestamp',
	        'on update' => 'NOW()',
	        'null' => TRUE
		),

		));

		$this->dbforge->add_key('round_status_id', TRUE);
		$this->dbforge->create_table('round_status');
	}

	public function down() {
		
	}

}

/* End of file 017_round_status.php */
/* Location: ./application/migrations/017_round_status.php */			