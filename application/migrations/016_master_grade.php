<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_grade extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'grade_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE,
			'auto_increment' => TRUE
		),
		'grade_name' => array(
			'type'		=> 'VARCHAR',
			'constraint'=> 50,
			'unsigned'	=> TRUE
		),

		));

		$this->dbforge->add_key('grade_id', TRUE);
		$this->dbforge->create_table('master_grade');
	}

	public function down() {
		
	}

}

/* End of file 016_master_grade.php */
/* Location: ./application/migrations/016_master_grade.php */