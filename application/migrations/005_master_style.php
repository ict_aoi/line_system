<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_style extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'master_style_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),			
			'id'		=>array(
				'type'		=>'INT',
				'constraint'=>11,
				'auto_increment'	=>TRUE
			),
			'style'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'poreference'	=>array(
				'type'		=>'VARCHAR',
				'constraint'=>15,
				'unsigned'	=>TRUE
			),
			'create_date'		=>array(
				'type'      => 'timestamp',
                'ON UPDATE CURRENT_TIMESTAMP' => TRUE,
			),
		));
		$this->dbforge->add_key('master_style_id', TRUE);
		$this->dbforge->create_table('master_style');
	}

	public function down() {
		$this->dbforge->drop_table('master_style');
	}

}

/* End of file 005_master_style.php */
/* Location: ./application/migrations/005_master_style.php */