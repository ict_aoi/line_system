<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_factory extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'factory_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE,
			'auto_increment' => TRUE
		),
		'factory_name' => array(
			'type'		=> 'VARCHAR',
			'constraint'=> 25,
			'unsigned'	=> TRUE
		),

		));

		$this->dbforge->add_key('factory_id', TRUE);
		$this->dbforge->create_table('master_factory');
	}

	public function down() {
		
	}

}

/* End of file 009_role_user.php */
/* Location: ./application/migrations/009_role_user.php */