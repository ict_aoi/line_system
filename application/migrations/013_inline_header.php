<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Inline_header extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'inline_header_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE,
			'auto_increment' => TRUE
			),
			'poreference' => array(
			'type'		=> 'VARCHAR',
			'constraint'=> 50,
			'unsigned'	=> TRUE
			),
			'style' => array(
			'type'		=> 'VARCHAR',
			'constraint'=> 50,
			'unsigned'	=> TRUE
			),
			'factory_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE
			),
			'line_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '4',
			'unsigned' => TRUE
			),
			'isactive' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '4',
			'unsigned' => TRUE
			),
			'create_date'		=>array(
				'type'      => 'timestamp',
		        'on update' => 'NOW()',
		        'null' => TRUE
			),
			'update_at'		=>array(
				'type'      => 'timestamp',
		        'null' => TRUE
			),
		));
		$this->dbforge->add_key('inline_header_id', TRUE);
		$this->dbforge->create_table('inline_header');
	}

	public function down() {
		
	}

}

/* End of file 013_inline_header.php */
/* Location: ./application/migrations/013_inline_header.php */