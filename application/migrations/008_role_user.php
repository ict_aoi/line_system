<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Role_user extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {

		$this->dbforge->add_field(array(
			'role_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'menu_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'level_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
		));
		$this->dbforge->add_key('role_id', TRUE);
		$this->dbforge->create_table('role_user');
	}

	public function down() {
		$this->dbforge->drop_table('role_user');
	}

}

/* End of file 008_role_user.php */
/* Location: ./application/migrations/008_role_user.php */