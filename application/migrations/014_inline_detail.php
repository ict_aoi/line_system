<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Inline_detail extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'inline_detail_id' => array(
			'type' => 'VARCHAR',
			'constraint' => '50',
			'unsigned' => TRUE
			),
			'inline_header_id' => array(
			'type' => 'VARCHAR',
			'constraint' => '50',
			'unsigned' => TRUE
			),
			'round' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE
			),
			'master_proses_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE
			),
			'defect_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE
			),
			'factory_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE
			),
			'sewer_nik' => array(
			'type'		=> 'VARCHAR',
			'constraint'=> 20,
			'unsigned'	=> TRUE
			),
			'qc_nik' => array(
			'type'		=> 'VARCHAR',
			'constraint'=> 20,
			'unsigned'	=> TRUE
			),
			'isactive' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '4',
			'unsigned' => TRUE
			),
			'create_date'		=>array(
				'type'      => 'timestamp',
		        'on update' => 'NOW()',
		        'null' => TRUE
			),
		));
		$this->dbforge->add_key('inline_detail_id', TRUE);
		$this->dbforge->create_table('inline_detail');
	}

	public function down() {
		
	}

}

/* End of file 014_inline_detail.php */
/* Location: ./application/migrations/014_inline_detail.php */