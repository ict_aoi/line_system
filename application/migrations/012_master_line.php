<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_line extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
		'master_line_id' => array(
		'type' => 'MEDIUMINT',
		'constraint' => '8',
		'unsigned' => TRUE,
		'auto_increment' => TRUE
		),
		'line_name' => array(
		'type'		=> 'VARCHAR',
		'constraint'=> 50,
		'unsigned'	=> TRUE
		),
		'create_date'		=>array(
			'type'      => 'timestamp',
	        'on update' => 'NOW()',
	        'null' => TRUE
		),
		'update_at'		=>array(
			'type'      => 'timestamp',
	        'null' => TRUE
		),
	));
		$this->dbforge->add_key('master_line_id', TRUE);
		$this->dbforge->create_table('master_line');
	}

	public function down() {
		
	}

}

/* End of file 012_master_line.php */
/* Location: ./application/migrations/012_master_line.php */