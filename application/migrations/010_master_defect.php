<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_defect extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'defect_id' => array(
			'type' => 'MEDIUMINT',
			'constraint' => '8',
			'unsigned' => TRUE,
			'auto_increment' => TRUE
		),
		'defect_jenis' => array(
			'type'		=> 'VARCHAR',
			'constraint'=> 100,
			'unsigned'	=> TRUE
		),

		));

		$this->dbforge->add_key('defect_id', TRUE);
		$this->dbforge->create_table('master_defect');

		$data = array(
			array('defect_id' => '1','defect_jenis' => '1. Color Defects ( shading within one garment or garments, migration/crocking/bleeding)'),
			array('defect_id' => '2','defect_jenis' => '2. Fabric defect (crease mark, Pilling, contamination)'),
			array('defect_id' => '3','defect_jenis' => '3. Incorrect stitch density ( eg. Button hole, seam SPI, Embroidery)'),
			array('defect_id' => '4','defect_jenis' => '4. Broken stitch'),
			array('defect_id' => '5','defect_jenis' => '5. Skipped stitches – All types of stitches'),
			array('defect_id' => '6','defect_jenis' => '6. Uneven/ wavy top & cover stitching'),
			array('defect_id' => '7','defect_jenis' => '7. Open seam/run off'),
			array('defect_id' => '8','defect_jenis' => '8. Seam puckering / Sewn-in pleats'),
			array('defect_id' => '9','defect_jenis' => '9. Roping, Twisted, wavy, uneven body/hem'),
			array('defect_id' => '10','defect_jenis' => '10. Needle cuts and needle holes'),
			array('defect_id' => '11','defect_jenis' => '11. Fabric raw edge/excess fabric'),
			array('defect_id' => '12','defect_jenis' => '12. Back tack/ bar tack missing or misplaced'),
			array('defect_id' => '13','defect_jenis' => '13. De-lamination and bubbling due to incorrect fusing'),
			array('defect_id' => '14','defect_jenis' => '14. Insecure and harmful accessories attachment'),
			array('defect_id' => '15','defect_jenis' => '15. Incorrect 3-stripe quality, position and measurements.'),
			array('defect_id' => '16','defect_jenis' => '16. Construction detail not symmetric'),
			array('defect_id' => '17','defect_jenis' => '17. Scissor cut'),
			array('defect_id' => '18','defect_jenis' => '18. Measurement out of tolerance'),
			array('defect_id' => '19','defect_jenis' => '19. Oil spots/ Soil/ Any kind of Stain/ Glue mark'),
			array('defect_id' => '20','defect_jenis' => '20. Untrimmed sewing threads or loose threads'),
			array('defect_id' => '21','defect_jenis' => '21. Chalk marks or pen/pencil marks are visble'),
			array('defect_id' => '22','defect_jenis' => '22. Incorrect size, color, shape, position, appearance on artwork'),
			array('defect_id' => '23','defect_jenis' => '23. Poor print / heat transfer quality ( Cracking, Sticky, Smell, Peel off and poor coverage)'),
			array('defect_id' => '24','defect_jenis' => '24. Faulty stitching on embroidery'),
			array('defect_id' => '25','defect_jenis' => '25. Uneven gathering on elastic construction or design'),
			array('defect_id' => '26','defect_jenis' => '26. Wrong Accessories/ missing / uncompleted'),
			array('defect_id' => '27','defect_jenis' => '27. Poor pressing'),
			array('defect_id' => '28','defect_jenis' => '28. Wrongly packed or wrong size'),
			array('defect_id' => '29','defect_jenis' => '29. Incorrect / missing information on polybag or carton box/wrong label'),
			array('defect_id' => '30','defect_jenis' => '30. Miscellaneous (please specify)')			
		);
		$this->db->insert_batch('master_defect', $data);
	}

	public function down() {
		
	}

}

/* End of file 009_master_defect.php */
/* Location: ./application/migrations/009_master_defect.php */