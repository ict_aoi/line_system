<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Master_employee extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'master_sewer_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'nik'		=>array(
				'type'		=>'INT',
				'unsigned'	=>TRUE
			),			
			'name'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'department_name'	=>array(
				'type'		=>'VARCHAR',
				'constraint'=>15,
				'unsigned'	=>TRUE
			),
			'subdept_name'	=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'job_desc'	=>array(
				'type'		=>'VARCHAR',
				'constraint'=>20,
				'unsigned'	=>TRUE
			),
			'factory'	=>array(
				'type'		=>'VARCHAR',
				'constraint'=>10,
				'unsigned'	=>TRUE
			),
			'create_date'		=>array(
				'type'      => 'timestamp',
                'ON UPDATE CURRENT_TIMESTAMP' => TRUE,
			),
		));
		$this->dbforge->add_key('master_sewer_id', TRUE);
		$this->dbforge->create_table('master_sewer');
	}

	public function down() {
		$this->dbforge->drop_table('master_sewer');
	}

}

/* End of file 006_master_employee.php */
/* Location: ./application/migrations/006_master_employee.php */