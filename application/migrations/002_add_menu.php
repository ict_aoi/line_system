<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_menu extends CI_Migration {


	public function up() {
		$this->dbforge->add_field(array(
			'menu_id' => array(
				'type'		=> 'VARCHAR',
				'constraint'=> 50,
				'unsigned'	=> TRUE
			),
			'nama_menu'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'link'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),
			'icon'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>25,
				'unsigned'	=>TRUE
			),
			'is_main_menu'		=>array(
				'type'		=>'VARCHAR',
				'constraint'=>50,
				'unsigned'	=>TRUE
			),

		));
		$this->dbforge->add_key('menu_id', TRUE);
		$this->dbforge->create_table('menu');
	}
	public function down()
    {
            $this->dbforge->drop_table('menu');
    }

}

/* End of file 001_add_menu.php */
/* Location: ./application/migrations/001_add_menu.php */