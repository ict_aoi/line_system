<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database','session','template','uuid', 'curl');

$autoload['drivers'] = array();


$autoload['helper'] = array('url','form','inline','apifunction');


$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();

// $autoload['libraries'] = array('curl');
