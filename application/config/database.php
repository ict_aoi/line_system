<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;
// local

/*$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'postgres',
	'password' => 'SEMANGAT45',
	'database' => 'inline_system',
	'dbdriver' => 'postgre',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);*/
// live

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.51.8',
	'username' => 'postgres',
	'password' => '',
	'database' => 'line_system',
	'dbdriver' => 'postgre',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
); 
$db['erp'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.51.123',
	'username' => 'adempiere',
	'password' => 'Becarefulwithme',
	'database' => 'aimsdbc',
	'schema'   => 'adempiere',
	'port'	   => 5432,
	'dbdriver' => 'postgre',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt'  => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
$db['absen'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.15.56',
	'username' => 'absensi',
	'password' => 'Absensi12345',
	'database' => 'absensi_bbi_apparel',
	'dbdriver' => 'postgre',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
// finish good live

$db['fg'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.51.19',
	'username' => 'dev',
	'password' => 'Pass@123',
	'database' => 'finish_goods',
	'dbdriver' => 'postgre',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

/*$db['fg'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'postgres',
	'password' => 'SEMANGAT45',
	'database' => 'finishgoods',
	'dbdriver' => 'postgre',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);*/
// $db['dist'] = array(
// 	'dsn'	=> '',
// 	'hostname' => '192.168.51.32',
// 	'username' => 'smartdist',
// 	'password' => 'Pass@1234',
// 	'database' => 'smart_dist',
// 	'dbdriver' => 'postgre',
// 	'dbprefix' => '',
// 	'pconnect' => FALSE,
// 	'db_debug' => (ENVIRONMENT !== 'production'),
// 	'cache_on' => FALSE,
// 	'cachedir' => '',
// 	'char_set' => 'utf8',
// 	'dbcollat' => 'utf8_general_ci',
// 	'swap_pre' => '',
// 	'encrypt' => FALSE,
// 	'compress' => FALSE,
// 	'stricton' => FALSE,
// 	'failover' => array(),
// 	'save_queries' => TRUE
// );

//live
// $db['cdms'] = array(
// 	'dsn'          => '',
// 	'hostname'     => '192.168.51.53',
// 	'username'     => 'postgres',
// 	'password'     => 'Becarefulwithme',
// 	'database'     => 'cdms',
// 	'dbdriver'     => 'postgre',
// 	'dbprefix'     => '',
// 	'pconnect'     => FALSE,
// 	'db_debug'     => (ENVIRONMENT !== 'production'),
// 	'cache_on'     => FALSE,
// 	'cachedir'     => '',
// 	'char_set'     => 'utf8',
// 	'dbcollat'     => 'utf8_general_ci',
// 	'swap_pre'     => '',
// 	'encrypt'      => FALSE,
// 	'compress'     => FALSE,
// 	'stricton'     => FALSE,
// 	'failover'     => array(),
// 	'save_queries' => TRUE
// );

// cdms new
$db['cdms_new'] = array(
	'dsn'          => '',
	'hostname'     => '192.168.51.87',
	'username'     => 'dev',
	'password'     => 'Becarefulwithme',
	'database'     => 'cdms',
	'dbdriver'     => 'postgre',
	'dbprefix'     => '',
	'pconnect'     => FALSE,
	'db_debug'     => (ENVIRONMENT !== 'production'),
	'cache_on'     => FALSE,
	'cachedir'     => '',
	'char_set'     => 'utf8',
	'dbcollat'     => 'utf8_general_ci',
	'swap_pre'     => '',
	'encrypt'      => FALSE,
	'compress'     => FALSE,
	'stricton'     => FALSE,
	'failover'     => array(),
	'save_queries' => TRUE
);

// pdc new
$db['pdc_new'] = array(
	'dsn'          => '',
	'hostname'     => '192.168.51.49',
	'username'     => 'dev',
	'password'     => 'Becarefulwithme',
	'database'     => 'pdc',
	'dbdriver'     => 'postgre',
	'dbprefix'     => '',
	'pconnect'     => FALSE,
	'db_debug'     => (ENVIRONMENT !== 'production'),
	'cache_on'     => FALSE,
	'cachedir'     => '',
	'char_set'     => 'utf8',
	'dbcollat'     => 'utf8_general_ci',
	'swap_pre'     => '',
	'encrypt'      => FALSE,
	'compress'     => FALSE,
	'stricton'     => FALSE,
	'failover'     => array(),
	'save_queries' => TRUE
);
