$(".proses_choses").chosen();
$(document).ready(function() {
  
  var url_loading = $('#loading_gif').attr('href');
	var base_url = $('#logo').attr('href');
	
	// console.log(base_url);
	$('#close_job').click(function() {
		// console.log(panjang);
		var batch_id = $('#batch_id').val(); 
		var data = 'batch_id='+batch_id;

		swal({
			title: "PERHATIAN!!!",
			text: "Apakah Anda Yakin Untuk Close Batch?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((wilsave) => {
			if (wilsave) {
				$.ajax({
					url: 'qc_matrix/close_matrix_job',
					type: "GET",
					data: data,
					// data: {
					//   data : data,
					//   array : listWaktu,
					// },
					contentType: false,
					cache: false,
					processData: false,
					beforeSend: function() {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});
						// console.log(data);
					},
					success: function(value) {
						if(value=='success'){

							$("#alert_success").trigger("click", 'Batch Job Closed');	
							window.location.replace(base_url);
							$.unblockUI();
						}
						else {
							$.unblockUI();
							$("#alert_error").trigger("click", 'Info ICT');
						}
					},
					error: function(data) {
						$.unblockUI();
						$("#alert_info").trigger("click", 'Contact Administrator');
					},
					// timeout: 15000
				});
			}
		});
		return false;
	});
	// $('#start_job').click(function() {
	// 	var batch_id = $('#batch_id').val();
		
	// 	if(batch_id == 0){
	// 		var style   = $('#style').val();
	// 		var line_id = $('#line_id').val();
	// 		$.ajax({
	// 			type:'GET',
	// 			url :'Qc_matrix/add_matrix_batch',
	// 			data:'style='+style+'&line='+line_id,
	// 			beforeSend: function () {
	// 			$.blockUI({
	// 					message: "<img src='" + url_loading + "' />",
	// 					css: {
	// 						backgroundColor: 'transaparant',
	// 						border: 'none',
	// 					}
	// 				});
	// 			},
	// 			success:function(value){
	// 					if(value=='success'){
	// 						// $("#alert_warning").trigger("click", 'NIK TIDAK TERDAFTAR');
	// 						$("#alert_success").trigger("click", 'Batch Job Dimulai');
	// 						$.unblockUI();
	// 					}
	// 			},
	// 			error:function(data){
	// 					if (data.status==500) {
	// 						$.unblockUI();
	// 						$("#alert_warning").trigger("click", 'Kontak ICT');
	// 					}
	// 					$.unblockUI();
	// 			},
	// 			timeout: 15000
	// 		});
	// 		$('#batch_id').val(1);
	// 	}
	// 	else{
	// 		$("#alert_warning").trigger("click", 'Job sudah aktif');
	// 	}
		
	// });

  $('#wip').change(function() {
    var temp = $('#wip').val();
    $('#wip-hid').val(temp);
  });

	$('#nik').change(function() {
		var proses = $('#proses-hid').val();
		var style = $('#style').val();
		
		$.ajax({
			type:'GET',
			url :'Qc_matrix/search_smv',
			data:'id='+proses+'&style='+style,
			beforeSend: function () {
			},
			success:function(value){
				$('#smv_spt').text(value);
			},
			error:function(data){
				$('#smv_spt').text('-');
			}
		});
	})

  $('#tc').change(function() {
    var temp = $('#tc').val();
		$('#tc-hid').val(temp);
  });

  $('#burst').change(function() {
    var temp = $('#burst').val();
    $('#burst-hid').val(temp);
  });

  $('#proses').change(function() {
    var temp = $('#proses').val();
    var proses = temp.split(':')[0];
		var lastproses = temp.split(':')[1];
    $('#proses-hid').val(proses);
		$('#lastproses-hid').val(lastproses);
  });

  $('#nik').change(function(e) {
    var header_id = $('#header_id').val();
    var nik = $("#nik").val();
      $.ajax({
          type:'GET',
          url :'Qc_matrix/searchnik',
          data:'id='+nik,
          beforeSend: function () {
          $.blockUI({
              message: "<img src='" + url_loading + "' />",
              css: {
                backgroundColor: 'transaparant',
                border: 'none',
              }
            });
          },
          success:function(value){
              if(value=='gagal'){
                $("#alert_warning").trigger("click", 'NIK TIDAK TERDAFTAR');
                $('#nik').val('');
                $('#sewer').val('');
                $('#grade-hid').val('');
                $('#sewer-hid').val('');
                $('#refresh').trigger("click"); 
                $.unblockUI();
              }else{
                $.unblockUI();
                $('#sewer').val(value);
                $('#sewer-hid').val(nik);
                $('#grade-hid').val('');
                $('#refresh').trigger("click"); 
                // getcolors(header_id, nik);
                // document.getElementById("proses").focus();
              }
          },
            error:function(data){
                if (data.status==500) {
                  $.unblockUI();
                  $("#alert_warning").trigger("click", 'Kontak ICT');
                }
                $.unblockUI();
                window.location.replace(base_url);
            },
            timeout: 15000
      });
  });

  $(function(){
			
			function submit_lagi(){
				var line = $('#line_id').val();
				var style = $('#style').val();			
				var data = 'line_id='+line+'&style='+style;
				var url_loading = $('#loading_gif').attr('href');
				console.log(line);
				console.log(style);
				$.ajax({
					url:'Qc_matrix/searchsubmit_ulang',
					type: "GET",
					data: data,
					contentType: false,       
					cache: false,          
					processData:false,
					beforeSend: function () {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});
					},
					success: function(response){
							if (response=='gagal') {
							$("#alert_warning").trigger("click", 'Data Tidak Ada');
							$.unblockUI();
							}else{
								$('#result').html(response);
								$.unblockUI();
							}
					},
					error:function(response){
						if (response.status=500) {
							$("#alert_info").trigger("click", 'Cek Input Data');
							$.unblockUI();
						}
					}
				});

				return false
			}

      function reset() {
        $('#nik').val('');
        $('#sewer').val('');
        $('#proses').val('');
        $('#mesin').val('');
        $('#wip').val('');
        $('#tc').val('');
        $('#burst').val('');
        
        $('#sewer-hid').val('');
        $('#proses-hid').val('');
        $('#mesin-hid').val('');
        $('#wip-hid').val('');
        $('#tc-hid').val('');
        $('#burst-hid').val('');
        
        $('#reset').trigger("click"); 
				items = [];
			}

      $('.frmsave').submit(function() {
        var panjang = items.length;
        // console.log(panjang);
        var data    = new FormData(this);

        data.append("waktu", JSON.stringify(listWaktu));

        // console.log(data);
        // var lastproses = $('#lastproses-hid').val();
        // var grade      = $('#grade-hid').val();
          if (panjang<3) {
            $("#alert_error").trigger("click", 'Minimal 3 Sample Waktu!!');
          }else{
              swal({
                title: "PERHATIAN!!!",
                text: "Apakah Anda Yakin Untuk Menyimpan Data?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((wilsave) => {
                if (wilsave) {
                  $.ajax({
                    url:'qc_matrix/savesubmit',
                    type: "POST",
                    data: data,
                    // data: {
                    //   data : data,
                    //   array : listWaktu,
                    // },
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function () {
                    $.blockUI({
                        message: "<img src='" + url_loading + "' />",
                        css: {
                          backgroundColor: 'transaparant',
                          border: 'none',
                        }
                      });
                      // console.log(data);
                    },
                    success: function(data){
                      var result = JSON.parse(data);
                      if(result.notif == 'sukses'){
                        $.unblockUI();
                        if(jalan == 0 ){
                          submit_lagi();
                        }else{
                          $('#startStop').trigger("click"); 
                          submit_lagi();
                        }
                        $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                        $('#sewer').val('');
                      }else{
                        $.unblockUI();
                        $("#alert_error").trigger("click", 'Data Sudah Ada');
                        if(jalan == 0 ){
                          submit_lagi();
                        }else{
                          $('#startStop').trigger("click"); 
                          submit_lagi();
                        }

                      }
                    }
                    ,error:function(data){
                          $.unblockUI();
                          if (response.status==500) {
                            $.unblockUI();
                            $("#alert_info").trigger("click", 'Contact Administrator');
                            $('#refresh').trigger("click"); 
                          }
                          // window.location.replace(base_url);
                      },
                      // timeout: 15000
                  });
                }
              });
          }
          return false;
      })
  });
});
function loadproses() {
    
  var url_loading = $('#loading_gif').attr('href');
  // var sewer       = $('#sewer-hid').val();
  // if (!sewer) {
  //   $("#alert_error").trigger("click", 'isi nik dulu');
  // }else{
    // $('#myModal').on('hidden.bs.modal', function (e) {
    //   if($('#myModal').hasClass('in')) {
    //     $('body').addClass('modal-open');
    //   }    
    // });
		//.css('overflowY', 'auto')
		var style = $('#style').val();
    $("#myModal").modal('show');
    $('.modal-title').text("Pilih Proses");
    $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>").css('overflowY', 'auto');
    $('.modal-body').load('Qc_matrix/proses_view?id='+style);
  // }

  return false;
}
