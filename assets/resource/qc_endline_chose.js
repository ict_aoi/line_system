
$(document).ready(function() {

	$('#clear_po').click(function () {
		location.reload();
	});

	$('.search_header').submit(function() {
		var po = $('#poreference').val();
		var size = $('#size').val();
		var url_loading = $('#loading_gif').attr('href');
		if (!po && !size) {
			$("#alert_warning").trigger("click", 'PO BUYER TIDAK BOLEH KOSONG');
		}else{
			var data = new FormData(this);
			$.ajax({
				url:'Qc/searchsubmit',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false,
                beforeSend: function () {
					$.blockUI({
						message: "<img src='" + url_loading + "' />",
						css: {
							backgroundColor: 'transaparant',
							border: 'none',
						}
					});
				},
				success: function(response){
	              if (response=='gagal') {
	              	$("#alert_warning").trigger("click", 'Data Tidak Ada');
	              	$.unblockUI();
	              }else{
	              	$('#result').html(' ');
	                $('#result').html(response);
	                // $('#refresh').trigger("click");	
	                $.unblockUI();
	              }
	          	},
	          	error:function(response){
		          if (response.status==500) {
		            $("#alert_info").trigger("click", 'Cek Input Data');
		            $.unblockUI();
		          }
		      }
			});
		}
		return false;
	});
	
});

function loadpo() {
	var url_loading = $('#loading_gif').attr('href');
	var modal = $('#myModal > div > div');
	$('#myModal').modal('show');
	modal.children('.modal-header').children('.modal-title').html('pilih PO');
	modal.parent('.modal-dialog').addClass('modal-lg');
	modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
	modal.children('.modal-body').load('qc/loadpo');
	return false;

	// $('#myModal > div > div');
	// $('#myModal').modal('show');
	// // $("#myModalBig").modal('show');
	// $('.modal-title').text("Pilih PO");
	// $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
	// $('.modal-body').load('qc/loadpo');
	// return false;
}
function loadsize() {
	var url_loading = $('#loading_gif').attr('href');
	// var id = $('#id').val();
	// $("#myModalBig").modal('show');
	// $('.modal-title').text("Pilih PO");
	// $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
	// $('.modal-body').load('qc/loadsize?id='+id);
	var id = $('#id').val();
	var modal = $('#myModal > div > div');
	$('#myModal').modal('show');
	modal.children('.modal-header').children('.modal-title').html('Pilih Size');
	modal.parent('.modal-dialog').addClass('modal-lg');
	modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
	modal.children('.modal-body').load('qc/loadsize?id='+id);
	return false;
}
function reset_po() {
	$('#poreference').val('');
	$('#size').val('');
	$('#result').html('');
}