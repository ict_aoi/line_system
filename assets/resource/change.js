$(document).ready(function() {
  
   $('#password_new, #password_confirm').on('keyup', function () {
    var password_new     = $('#password_new').val();
    var password_confirm = $('#password_confirm').val();
      if (password_new==password_confirm) {
        $('#message').html('Password Sama').css('color', 'green');;
      } else{
        $('#message').html('Password tidak Sama').css('color', 'red');
      }
      
    });
   $('.frmcange').submit(function () {
      var password_new     = $('#password_new').val();
      var password_confirm = $('#password_confirm').val();
      if (!password_new && !password_confirm) {
        $("#alert_error").trigger("click", 'data tidak boleh kosong');
      }else{
        if ($('#password_new').val() == $('#password_confirm').val()) {
          var data  = new FormData(this);
          var url_loading = $('#loading_gif').attr('href');
          $.ajax({
            url:'changesubmit',
            type: "POST",
            data: data,
            contentType: false,       
            cache: false,          
            processData:false,
            beforeSend: function () {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transaparant',
                  border: 'none',
                }
              });
            }, 
            success: function(response){
              if(response == 'sukses'){
                $.unblockUI();
                $("#alert_success").trigger("click", 'Simpan Data Berhasil');
              }else{
                $.unblockUI();
                $("#alert_error").trigger("click", 'Cek inputan kembali');
              }
            }
            ,error:function(response){
              if (response.status=500) {
                $.unblockUI();
                $("#alert_info").trigger("click", 'Contact Administrator');
              }
                      
                  }
          });
        }
      }
      return false;
   });
})