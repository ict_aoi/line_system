var url_loading = $('#loading_gif').attr('href');
var remark = $('#remark').val();
// setTimeout(function(){ 
//     load_data();

//  },5000);
$(document).ready(function() {
    $('.size').trigger('change');

    var scan = $('#scan').val();
    /*var barcode = $('#barcode_id').val();
    if ($('#switch').val()=='0') {
    	$("#good").addClass("hidden");
    	$("#scan").removeClass("hidden");
    	$("#list_barcode").removeClass("hidden");
    }else{
    	$("#good").removeClass("hidden");
    	$("#scan").addClass("hidden");
    }*/

    /*if ($('#change_hid').val()==1) {
		$('#change').removeClass("hidden");
	}else{
		$("#change").addClass("hidden");
	}
*/

    $('#refresh').click(function() {
        load_data();
        document.getElementById("scan").focus();
    });
    $('#good').click(function() {
        if (parseInt($('#qty').val() != parseInt($('.qty_save').text()) * 2)) {
            folding_up();
        } else {
            $("#alert_error").trigger("click", 'QTY Karton Penuh');
        }

    });
    $('#barcode_id').change(function() {
        $("#change").addClass("hidden");
    });

    $('.size').change(function() {

        var qty = $('.size').find(':selected').attr('data-qty');
        $('#qty').val(qty);
        load_data();
    });

    $('#scan').change(function() {
        var scan = $('#scan').val().length;
        var scan2 = $('#scan').val();
        var qty_scan = parseInt($('.qty_scan').text());
        var barcode_garment = $('#barcode_garment').val();
        // var wip             = parseInt($('.wft').text());
        // if (wip>0) {
        if (scan < 14) {
            if ($('#rasio').val() == 1) {


                if (barcode_garment == 0) {
                    $('#barcode_garment').val(scan2);
                    $('#changebarcode').val(scan2);

                    scan_garmentrasio();
                } else if (barcode_garment == $('#scan').val()) {
                    scan_garmentrasio();
                } else {
                    $("#alert_warning").trigger("click", 'Barcode Garment Salah');
                    $('#scan').val('');
                    document.getElementById("scan").focus();
                }
            } else {
                // alert('msg');
                if (barcode_garment == 0) {
                    $('#barcode_garment').val(scan2);
                    $('#changebarcode').val(scan2);
                    scan_garment();
                } else if (barcode_garment == $('#scan').val()) {
                    scan_garment();
                } else {
                    $("#alert_warning").trigger("click", 'Barcode Garment Salah');
                    $('#scan').val('');
                    document.getElementById("scan").focus();
                }
            }
        } else {
            $("#alert_warning").trigger("click", 'Barcode Garment Salah');
            $('#scan').val('');
            document.getElementById("scan").focus();
        }
        // }else{
        // 	$("#alert_warning").trigger("click", 'tidak ada tumpukan garment');
        // 	$('#scan').val('');
        // 	// $('#refresh').trigger("click");
        // 	document.getElementById("scan").focus();
        // }


    });
    $('#simpan').click(function() {
        var save = parseInt($('.qty_save').text());
        var scan = parseInt($('.qty_scan').text());
        var qty_karton = parseInt($('.qty_karton').text());
        var barcode_garment = $('#barcode_garment').val();

        if (save + scan > 0) {
            if (save + scan == qty_karton) {
                complete_up();
                //foldingup_masal(1);				
            } else {
                foldingup_masal(0);
            }
            $('#barcode_garment_hid').val(barcode_garment);
        } else {
            $("#alert_warning").trigger("click", 'QTY scan 0, data tidak bisa di simpan');
        }

    });

    load_data();
});

function folding_up(complete) {
    // var balance    = parseInt($('.balance').text());
    // var wft        = parseInt($('.wft').text());
    var data = 'style=' + $('#style_hid').val() + '&id=' + $('#header_id').val() + '&size_id=' + $('#sizeid').val() + '&barcode_package=' + $('#barcodePackage').val() +
        '&scan=' + $('#scan').val() + '&remark=' + $('#remark').val() + '&scan_id=' + $('#scan_id').val() +
        '&poreference=' + $('.poreference').text() + '&size=' + encodeURIComponent($('.size').val()) + '&complete=' + complete + '&qty=' + $('.qty_scan').text();

    // if (wft>0) {
    // if (balance<0) {
    $.ajax({
        url: 'Folding/foldingupmasal',
        data: data,
        type: 'POST',
        beforeSend: function() {
            $('#scan').val('');
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                }
            });
        },
        success: function(data) {
            var result = JSON.parse(data);
            if (result.status == 200) {
                $.unblockUI();
                hitungClient();
                $('.status').html(result.draw);
                document.getElementById("scan").focus();
            } else if (result.status == 500) {
                $("#alert_warning").trigger("click", result.pesan);
                $('#scan').val('');
                $('#scan').attr('disabled', false);
                $.unblockUI();
                $('#refresh').trigger("click");
                document.getElementById("scan").focus();
            } else if (result.status == 501) {
                $("#alert_warning").trigger("click", result.pesan);
                location.reload();
            } else if (result.status == 504) {
                $.unblockUI();
                $("#alert_warning").trigger("click", result.pesan);
                $('#refresh').trigger("click");
                document.getElementById("scan").focus();
            }
        },
        error: function(data) {
            if (data.status == 500) {
                $.unblockUI();
                $("#alert_warning").trigger("click", 'info ict');
                $('#scan').val('');
            }
        }

    });

    // }else{
    // 	$("#alert_warning").trigger("click", 'po buyer telah balance');
    // }
    // }else{
    // 	$('#scan').val('');
    // 	$("#alert_warning").trigger("click", 'tidak ada tumpukan garment');
    // 	// $('#refresh').trigger("click");
    // 	document.getElementById("scan").focus();
    // }

}

function hitungClient() {
    var wip = parseInt($('.wft').text()) - 1;
    var countertoday = parseInt($('.countertoday').text()) + 1;
    var counter_folding = parseInt($('.counter_folding').text()) + 1;
    var qty_save = parseInt($('.qty_save').text()) + 1;
    var counterscan_rasio = parseInt($('#counterscan_rasio').val()) + 1;
    var balance = parseInt($('.balance').text()) + 1;
    var wip_1 = parseInt($('.wip1').text()) - 1;
    if ($('#rasio').val() == 1) {
        $('#counterscan_rasio').val(counterscan_rasio)
    }
    // $('.wft').text(wip);
    $('.wip1').text(wip_1);
    $('.countertoday').text(countertoday);
    $('.counter_folding').text(counter_folding);
    $('.qty_save').text(qty_save);
    $('.balance').text(balance);

}

function hitungClientMasal() {
    var qtyscan = parseInt($('.qty_scan').text());
    var qty_save = parseInt($('.qty_save').text()) + qtyscan;
    var countertoday = parseInt($('.countertoday').text()) + qtyscan;
    var counter_folding = parseInt($('.counter_folding').text()) + qtyscan;
    var counterscan_rasio = parseInt($('#counterscan_rasio').val()) + qtyscan;
    var balance = parseInt($('.balance').text()) + qtyscan;
    if ($('#rasio').val() == 1) {
        $('#counterscan_rasio').val(counterscan_rasio)
    }
    if ($('#switch').val() == 1) {
        $('.balance').text(balance);
    }
    $('.qty_save').text(qty_save);
    $('.countertoday').text(countertoday);
    $('.counter_folding').text(counter_folding);
    $('.qty_save').text(qty_save);
    // $('.balance').text(balance);
    $('.qty_scan').text('0')

}

function scan_garment() {
    var qty_save = parseInt($('.qty_save').text());
    var qty_scan = parseInt($('.qty_scan').text());
    var qty_karton = parseInt($('.qty_karton').text());
    var wip_1 = parseInt($('.wip1').text()) - 50;
    // var wip             = parseInt($('.wft').text())-1;
    // console.log(qty_karton-1==(qty_save+qty_scan));
    if (((qty_save + qty_scan)) == qty_karton - 50) {
        $('.qty_scan').text(qty_scan + 50);
        if ($('#switch').val() == 1) {
            $('.wip1').text(wip_1);
        }
        // $('.wft').text(wip);
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
        foldingup_masal(1);
    } else if (qty_karton > (qty_save + qty_scan)) {
        $('.qty_scan').text(qty_scan + 50);
        if ($('#switch').val() == 1) {
            $('.wip1').text(wip_1);
        }
        // $('.wft').text(wip);
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
    } else {
        $("#alert_warning").trigger("click", 'QTY Karton penuh');
        location.reload();
        $('#scan').val('');
    }

}

function scan_garmentrasio() {
    var qty_save = parseInt($('.qty_save').text());
    var qty_scan = parseInt($('.qty_scan').text());
    var qty_karton = parseInt($('.qty_karton').text());
    // var wip               = parseInt($('.wft').text())-1;
    var counterscan_rasio = parseInt($('#counterscan_rasio').val());
    var qty = parseInt($('#qty').val());
    var status = $('.status').text();
    var wip_1 = parseInt($('.wip1').text()) - 1;

    // console.log(parseInt($('#qty').val())-1==(qty_save+qty_scan));

    if (qty - 1 == (counterscan_rasio + qty_scan)) {
        $('.qty_scan').text(qty_scan + 1);
        if ($('#switch').val() == 1) {
            $('.wip1').text(wip_1);
        }
        // $('.wft').text(wip);
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
        if (qty_karton - 1 == (qty_save + qty_scan)) {
            var complete = 1;
        } else {
            var complete = 0;
        }


        foldingup_masal(complete);
        // $('.qty_save').text('0')
        // $("#alert_warning").trigger("click", 'QTY Rasio Sudah Penuh');
        // $('.size option:selected').next().attr('selected', 'selected');
        // $('.size').trigger('change');

    } else if (qty > (counterscan_rasio + qty_scan)) {
        $('.qty_scan').text(qty_scan + 1);
        if ($('#switch').val() == 1) {
            $('.wip1').text(wip_1);
        }
        // $('.wft').text(wip);
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
    } else {
        //gara2 ini
        // $("#alert_warning").trigger("click", 'QTY Karton penuh');
        $("#alert_warning").trigger("click", 'QTY Rasio Penuh, Sistem akan merubah ke size selanjut nya');
        $('.size option:selected').next().attr('selected', 'selected');
        $('.size').trigger('change');
        $('#scan').val('');
    }

}

function outputfolding() {
	var modal = $('#myModal > div > div');
	$('#myModal').modal('show');
	modal.children('.modal-header').children('.modal-title').html('Report OutputFolding');
	modal.parent('.modal-dialog').addClass('modal-lg');
	modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
	modal.children('.modal-body').load('Folding/showOutputFolding');
	return false;
}

function load_data() {

    var data = 'id=' + $('#header_id').val() + '&barcode_id=' +
        $('#barcodePackage').val() + '&scan_id=' + $('#scan_id').val() +
        '&poreference=' + $('.poreference').text() + '&size=' + encodeURIComponent($('.size').val()) + '&style=' + $('.style').text() + '&article=' + $('.article').text() + '&rasio=' + $('#rasio').val();

    var rasio = $('#rasio').val();
    var currentRequest = null;

    currentRequest = jQuery.ajax({
        url: 'Folding/loaddata',
        data: data,
        type: 'POST',
        beforeSend: function() {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                }
            });
        },
        success: function(data) {
            $.unblockUI();
        },
        error: function(data) {
            if (data.status == 500) {
                $.unblockUI();
            }
        }
    }).done(function(data) {
        var result = JSON.parse(data);
        var qty = $('.size').find(':selected').attr('data-qty');
        $('#qty').val(qty);
        $('.wip1').html(result.wft);
        $('#barcode_garment').val(result.barcode_garment);
        $('#barcode_garment_hid').val(result.barcode_garment);
        $('#changebarcode').val(result.barcode_garment);
        $('.balance').html(result.balance);
        $('.qty_save').text(result.counterscan);
        $('#counterscan_rasio').val(0);
        $('.total_karton').text(result.totalKarton);
        $('#qc_id').val(result.qc_id);
        $('.status').html(result.draw);
        $('.qty_scan').text('0');
        $('#counterscan_rasio').val(result.counterscan_rasio);
        document.getElementById("scan").focus();

    });

    if (rasio == 1) {
        $.ajax({
            url: 'Folding/loadRasio',
            data: data,
            type: 'POST',
            success: function(data) {}
        }).done(function(data) {
            var result = JSON.parse(data);
            // $('#counterscan_rasio').val(result.counterscan_rasio);
            $('.list_size').text(result.list_size);
        });
    }

}

function foldingup_masal(complete) {
    var data = 'style=' + $('#style_hid').val() + '&id=' + $('#header_id').val() + '&size_id=' + $('#sizeid').val() + '&barcode_package=' + $('#barcodePackage').val() +
        '&scan=' + $('#barcode_garment').val() + '&flag_barcode=' + $('#barcode_garment_hid').val() + '&remark=' + $('#remark').val() + '&scan_id=' + $('#scan_id').val() +
        '&poreference=' + $('.poreference').text() + '&size=' + encodeURIComponent($('.size').val()) + '&complete=' + complete + '&qty=' + $('.qty_scan').text() + '&qty_rasio=' + $('#qty').val() + '&article=' + $('.article').text();

    var rasio = $('#rasio').val();

    // if (balance<0) {
    $.ajax({
        url: 'Folding/foldingupmasal',
        data: data,
        type: 'POST',
        beforeSend: function() {
            $('#scan').val('');
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                }
            });
            // $('#scan').attr('disabled',true);
        },
        success: function(data) {
            var result = JSON.parse(data);
            if (result.status == 200) {
                // $('#scan').val('');
                $.unblockUI();
                hitungClientMasal();
                $('.status').html(result.draw);
                document.getElementById("scan").focus();
                $('#flag_qtyscan').val(0);
                if (rasio == 1) {
                    $('#counterscan_rasio').val(0)
                    ganti_size_rasio();
                }
            } else if (result.status == 500) {
                $("#alert_warning").trigger("click", result.pesan);
                $('#scan').val('');
                $('#scan').attr('disabled', false);
                $.unblockUI();
                $('#refresh').trigger("click");
                document.getElementById("scan").focus();
            } else if (result.status == 501) {
                $("#alert_warning").trigger("click", result.pesan);
                location.reload();
            } else if (result.status == 504) {
                $.unblockUI();
                $("#alert_warning").trigger("click", result.pesan);
                $('#flag_qtyscan').val(0);
                $('#refresh').trigger("click");
                document.getElementById("scan").focus();
            }
        },
        error: function(data) {
            if (data.status == 500) {
                $.unblockUI();
                $("#alert_warning").trigger("click", 'info ict');
                $('#scan').val('');
            }
        }

    });

    // }else{
    // 	$("#alert_warning").trigger("click", 'po buyer telah balance');
    // }

}

function ganti_size_rasio() {
    $("#alert_warning").trigger("click", 'QTY Rasio Sudah Penuh');
    $('.size option:selected').next().attr('selected', 'selected');
    $('.size').trigger('change');
}

function complete_up() {
    var save = parseInt($('.qty_save').text());
    var scan = parseInt($('.qty_scan').text());
    var qty_karton = parseInt($('.qty_karton').text());

    var data = 'barcode_package=' + $('#barcodePackage').val() + '&scan_id=' + $('#scan_id').val();

    if ((save + scan) == qty_karton) {
        $.ajax({
            url: 'Folding/auto_complete_karton',
            data: data,
            type: 'POST',
            beforeSend: function() {
                $('#scan').val('');
                $.blockUI({
                    message: "<img src='" + url_loading + "' />",
                    css: {
                        backgroundColor: 'transaparant',
                        border: 'none',
                    }
                });
                // $('#scan').attr('disabled',true);
            },
            success: function(data) {
                var result = JSON.parse(data);
                if (result.status == 100) {
                    $.unblockUI();
                    $('#flag_qtyscan').val(0);
                    $('.status').html(result.draw);
                    $("#alert_success").trigger("click", result.pesan);
                    document.getElementById("scan").focus();
                } else if (result.status == 500) {
                    $("#alert_warning").trigger("click", result.pesan);
                    $('#scan').val('');
                    $.unblockUI();
                    $('#refresh').trigger("click");
                    document.getElementById("scan").focus();
                }
                load_data_sets();
            },
            error: function(data) {
                if (data.status == 500) {
                    $.unblockUI();
                    $("#alert_warning").trigger("click", 'info ict');
                    $('#refresh').trigger("click");
                    $('#scan').val('');
                }
                load_data_sets();
            }
        });
    }
}

function loadhidden(ih,size) {
    var data = 'ih=' + ih + '&size=' + size;
    $.ajax({
        url: 'Folding/loadhidden',
        data: data,
        type: 'POST',
        success: function(data) {}
    }).done(function(data) {
        var result = JSON.parse(data);
        var qty_scan = $('.qty_scan').text();
        var wip_1 = parseInt(result.wip_1) - parseInt(qty_scan);

        $('#switch').val(1);
        $('.wip1').text(wip_1);
        $('.balance').text(result.balance);
    });
}

function loadwft(header_id, size) {
    var data = 'header_id=' + header_id + '&size=' + size;
    $.ajax({
        url: 'Folding/load_wip',
        data: data,
        type: 'POST',
        success: function(data) {}
    }).done(function(data) {
        var result = JSON.parse(data);
        $('.wft').text(result.wft);
    });
}

function loadbalance(header_id, size) {
    var data = 'header_id=' + header_id + '&size=' + size;
    $.ajax({
        url: 'Folding/load_wip',
        data: data,
        type: 'POST',
        success: function(data) {}
    }).done(function(data) {
        var result = JSON.parse(data);
        $('.balance').text(result.balance);
    });
}
function modal_revisibarcode() {
  var barcode_garment = $('#barcode_garment').val();
  var flag_barcode = $('#barcode_garment_hid').val();
  var url_loading = $('#loading_gif').attr('href');
  if (barcode_garment!=0) {
  	  $("#myModal").modal('show').css('overflowY', 'auto');
	  $('.modal-title').text("Revisi Barcode Garment");
	  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>").css('overflowY', 'auto');
	  $('.modal-body').load('folding/showmodalrevisibarcode?barcode_garment='+barcode_garment+'&header_id='+$('#header_id').val()+'&flag_barcode='+flag_barcode+'&size_id='+encodeURIComponent($('.size').val()));
	  return false;
  }else{
  	 $("#alert_warning").trigger("click", "Belum ada Barcode Garment Yang di Scan");	
  }
  
}
function submiteditbarcode() {
    var data = '&id=' + $('#header_id').val()+ '&newbarcode=' + $('.newbarcode').val()+ '&size_id=' + $('#sizeid').val();

    $.ajax({
        url: 'Folding/updatebarcodegarment',
        data: data,
        type: 'POST',
        beforeSend: function() {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                }
            });
        },
        success: function(data) {
            var result = JSON.parse(data);
            if (result.status == 200) {
                $.unblockUI();
                $('#barcode_garment').val(result.barcode)
                $('#myModal').modal('hidden');
            } else if (result.status == 500) {
                $("#alert_warning").trigger("click", result.pesan);
                $.unblockUI();
            }
        },
        error: function(data) {
            if (data.status == 500) {
                $.unblockUI();
                $("#alert_warning").trigger("click", 'info ict');
                $('#scan').val('');
            }
        }

    });
}
