$(".proses_choses").chosen();
$(document).ready(function() {
  var url_loading = $('#loading_gif').attr('href');
  var base_url = $('.logo').attr('href');

  $('#proses').change(function() {
    var temp = $('#proses').val();
    var proses = temp.split(':')[0];
    var lastproses = temp.split(':')[1];
    $('#proses-hid').val(proses);
    $('#lastproses-hid').val(lastproses);
  });

  $('#nik').change(function(e) {
    var header_id = $('#header_id').val();
    var nik = $("#nik").val();
      $.ajax({
          type:'GET',
          url :'Qc_inspect/searchnik',
          data:'id='+nik,
          beforeSend: function () {
          $.blockUI({
              message: "<img src='" + url_loading + "' />",
              css: {
                backgroundColor: 'transaparant',
                border: 'none',
              }
            });
          },
          success:function(value){
              if(value=='gagal'){
                $("#alert_warning").trigger("click", 'NIK TIDAK TERDAFTAR');
                $('#nik').val('');
                $('#sewer').val('');
                $('#grade-hid').val('');
                $('#sewer-hid').val('');
                $('#refresh').trigger("click"); 
                $.unblockUI();
              }else{
                $.unblockUI();
                $('#sewer').val(value);
                $('#sewer-hid').val(nik);
                $('#grade-hid').val('');
                $('#refresh').trigger("click"); 
                getcolors(header_id, nik);
                // document.getElementById("proses").focus();
              }
          },
            error:function(data){
                if (data.status==500) {
                  $.unblockUI();
                  $("#alert_warning").trigger("click", 'Kontak ICT');
                }
                $.unblockUI();
                window.location.replace(base_url);
            },
            // timeout: 15000
      });
  });

  $('#btnNext').click(function() {
    var next = parseInt($('#round').val());
    var test = parseInt($('#test').val());
    var i =1;
     round = next + i;
    if (round > 5) {
        $('#round').val(5);
        $('#round-hid').val(5);

    }else{
      $('#round').val(round);
      $('#round-hid').val(round);
    }
  })
  $('#btnBack').click(function() {
    var back = parseInt($('#round').val());
    var test = parseInt($('#test').val());
    var i =1;
     round = back - i;

    if (round < back) {
        $('#round').val(test);
        $('#round-hid').val(test);
    }else{
      $('#round').val(round);
      $('#round-hid').val(round);
    }
  });

  $(function(){

      var items = JSON.parse($('#items').val());
      function render(){
        getIndex();
        $('#items').val(JSON.stringify(items));
        var tmpl = $('#items-table').html();
        Mustache.parse(tmpl);
        var data = { item: items };
        var html = Mustache.render(tmpl, data);
        $('#tbody-items').html(html);
        bind();
      }
      $('#refresh').click(function() {
        items = [];
        $('#grade').css('background-color', 'white');
        $('#items_grade').val('');

        render();
      });
      function bind(){
        $('#addGood').on('click', addGood);
        $('#AddDefect').on('click', AddDefect);
        $('.btn-delete-item').on('click', deleteItem);
      }

      function getIndex(){
        for (idx in items) {
          items[idx]['_id'] = idx;
          items[idx]['no'] = parseInt(idx) + 1;
        }
      }

      function addGood() {
        var nik = $('#nik').val();
        var proses = $('#proses').val();
        var size = $('#selc_size').val();
        var grade_before = $('#grade_before').val();
        if (nik=='' || proses=='' || size=='') {
          $("#alert_error").trigger("click", 'Tolong NIK & Proses Di Lengkapi');
        }else{
          if(items.length>4){
           $("#alert_error").trigger("click", 'Maks. Inspection');
          }else{
           var input = {
               'defect_id': '0',
              'defect_code': 'No Defect',
              'grade_id': '1',
              'size':size
            };
           items.push(input);

           $('#grade').val(1);
           $('#grade-hid').val(1);

           grade_calculation(1);

            render();
          }
        }
      }

      function AddDefect() {
        var nik = $('#nik').val();
        var proses = $('#proses').val();
        var size = $('#selc_size').val();
        if (nik=='' || proses=='' || size=='') {
          $("#alert_error").trigger("click", 'Tolong NIK & Proses Di Lengkapi');
        }else{
          if(items.length>4){
           $("#alert_error").trigger("click", 'Maks. Inspection');
          }else{
            defect();
            $('#defect_id').change(function(){
              var temp = $('#defect_id').val();
              var defect_id = temp.split(':')[0] ;
              var defect_name = temp.split(':')[1] ;
              var grade_id = temp.split(':')[2] ;

               var input = {
                 'defect_id': defect_id,
                 'defect_code': defect_name,
                 'grade_id': grade_id,
                 'size':size
                };
                items.push(input);

                grade_calculation(input.grade_id);

                render();
            });
          }
        }
      }

      /*items grade*/
      function grade_calculation(grade) {
        var oldValue = $("#items_grade").val();
        var grade_before = $('#grade_before').val();
        var arr = oldValue === "" ? [] : oldValue.split(',');

        arr.push(grade);

        var newValue = arr.join(',');

        $("#items_grade").val(newValue);

        var arr_new = $("#items_grade").val().split(',');

        var count = 0
        if (arr_new.length == 5) {
          $.each(arr_new,function(index, value) {

            // console.log(value);
              if (value == 3) {
                color_grade(3, grade_before);
                return false;
              }

              if (value == 2) {
                count++;
              }


              if (count > 1) {
                color_grade(3, grade_before);
                // color_grade(2, grade_before);
              }else if (count == 1) {
                color_grade(2, grade_before);
              }else{
                color_grade(1, grade_before);
                // color_grade(2, grade_before);
              }

          });

        }
      }

      function color_grade(grade_now, grade_before) {

        var grade_now = parseInt(grade_now);
        var grade_before = parseInt(grade_before);

        var grade = 0;
        var color = '#f7f7f7';

        console.log("grade before"+grade_before);
        console.log("grade now"+grade_now);

        // if (grade_before >= 1) {

        //     if (grade_before == 3) {
        //         if (grade_now == (grade_before - 1)) {
        //             grade = grade_before;
        //         }else if (grade_now < (grade_before - 1)) {
        //             grade = grade_before - 1;
        //         }else{
        //             grade = grade_now;
        //         }
        //     }else if (grade_before == 2) {
        //         if (grade_now >= grade_before) {
        //             grade = grade_before+1;
        //         }
        //         // else if (grade_now > grade_before) {
        //         //     grade = grade_before + 1;
        //         else{
        //             grade = grade_before - 1;
        //         }
        //     }else{
        //         grade = grade_now;
        //     }

        // }else{
            grade = grade_now;
        // }

        console.log("grade"+grade);

        if (grade == 1) {
            color = 'green';
            grade_name = 'No Defect';
        }else if (grade == 2) {
            color = 'yellow';
            grade_name = '1 minor defect';
        }else if (grade == 3) {
            color = 'red';
            grade_name = '1 Major or 2 Minors';
        }

        $('#grade').val(grade_name).trigger('change');
        $('#grade-hid').val(grade).trigger('change');


        $('#grade').css('background-color', color);
        $('#grade').css('color', 'white');
      }

      /**/

      function deleteItem(){
        var i = parseInt($(this).data('id'), 10);

        var arr_new = $("#items_grade").val().split(',');

        items.splice(i, 1);
        arr_new.splice(i, 1);

        $("#items_grade").val(arr_new.join(',')).trigger('change');

        render();
      }
      function defect() {
        var url_loading = $('#loading_gif').attr('href');
        $("#myModal").modal('show');
        $('.modal-title').text("Pilih Defect");
        $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>").css('overflowY', 'auto');
        $('.modal-body').load('Qc_inspect/defect_view');
        return false;
      }

      render();
      function reset() {
        $('#nik').val('');
        $('#sewer').val('');
        $('#proses').val('');
        $('#proses-hid').val('');
        $('#sewer-hid').val('');
        $('#grade-hid').val('');
        $('#grade').val('').css('background-color', 'white');;
      }
    $('.frmsave').submit(function() {
      var data       = new FormData(this);
      var lastproses = $('#lastproses-hid').val();
      var grade      = $('#grade-hid').val();
        if (items.length<5 || grade=='') {
          $("#alert_error").trigger("click", 'Inspection tidak lengkap!!');
          reset();
          $('#refresh').trigger("click"); 
        }else{
          if (lastproses=='t') {
            swal({
              title: "PERHATIAN!!!",
              text: "Proses ini adalah Proses Terakhir,Klik OK Untuk Pindah Round",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((wilsave) => {
              if (wilsave) {
                $.ajax({
                  url:'qc_inspect/savesubmit',
                  type: "POST",
                  data: data,
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function () {
                  $.blockUI({
                      message: "<img src='" + url_loading + "' />",
                      css: {
                        backgroundColor: 'transaparant',
                        border: 'none',
                      }
                    });
                  },
                  success: function(data){
                    var result = JSON.parse(data);
                    if(result.notif == 'sukses'){
                      $.unblockUI();
                      $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                      $('#refresh').trigger("click"); 
                      $('#items_grade').val('').trigger('change');
                      color_grade('white');
                      $('#round').val(result.round);
                      $('#sewer').val('');
                      reset();
                    }else{
                      $.unblockUI();
                      $("#alert_error").trigger("click", 'Data Sudah Ada');
                      $('#refresh').trigger("click"); 
                      reset();

                    }
                  }
                  ,error:function(data){
                        $.unblockUI();
                        window.location.replace(base_url);
                    },
                    // timeout: 15000
                });
              }
            });
          }else{
            $.ajax({
              url:'qc_inspect/savesubmit',
              type: "POST",
              data: data,
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function () {
              $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                  }
                });
              },
              success: function(data){
                var result = JSON.parse(data);
                if(result.notif == 'sukses'){
                  $.unblockUI();
                  $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                  $('#refresh').trigger("click"); 
                  $('#items_grade').val('').trigger('change');
                  color_grade('white');
                  $('#round').val(result.round);
                  reset();
                }else{
                  $.unblockUI();
                  reset();
                  $('#refresh').trigger("click"); 
                  $("#alert_error").trigger("click", 'Data Sudah Ada');
                }
              }
              ,error:function(response){
                if (response.status==500) {
                  $.unblockUI();
                  $("#alert_info").trigger("click", 'Contact Administrator');
                  $('#refresh').trigger("click"); 
                }

                    }
            });
          }
        }
        return false;
      })
    });
  });
 function loadproses() {
    
  var url_loading = $('#loading_gif').attr('href');
  // var sewer       = $('#sewer-hid').val();
  // if (!sewer) {
  //   $("#alert_error").trigger("click", 'isi nik dulu');
  // }else{
    // $('#myModal').on('hidden.bs.modal', function (e) {
    //   if($('#myModal').hasClass('in')) {
    //     $('body').addClass('modal-open');
    //   }    
    // });
    //.css('overflowY', 'auto')
    $("#myModal").modal('show');
    $('.modal-title').text("Pilih Proses");
    $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>").css('overflowY', 'auto');
    $('.modal-body').load('Qc_inspect/proses_view');
  // }

  return false;
}

function loadgrade() {
  var url_loading = $('#loading_gif').attr('href');
  $("#myModal").modal('show').css('overflowY', 'auto');
  $('.modal-title').text("Pilih Grade");
  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>").css('overflowY', 'auto');
  $('.modal-body').load('Qc_inspect/gradeview');
  return false;
}

function loadinspect() {
  var url_loading = $('#loading_gif').attr('href');
  $("#myModal").modal('show');
  $('.modal-title').text("Data Inspection");
  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px;margin-top:50px'>");
  $('.modal-body').load('Qc_inspect/inspection_view');
  return false;
}

function loadsummary() {
  var line = $('#line_id').val();
  var url_loading = $('#loading_gif').attr('href');
  $("#myModal").modal('show');
  $('.modal-title').text("Detail Defect Inline");
  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px;margin-top:50px'>");
  $('.modal-body').load('Qc_inspect/inspection_summary/'+line);
  return false;
}