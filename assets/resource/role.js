$(document).ready(function(){
	loadData();
	$('#refresh').bind('click', function () {
	 	load_level();
	});
})
function load_level() {
	$.ajax({
        type:"GET",
        url:"Role/load_level",
        data:'',
        success:function(html){
            $("#list_load").html(html);
        }
    });
}
function tambah(){       
	$("#myModal").modal('show');
	$('.modal-title').text("Add Role");
	$('.modal-body').html('Loading');
	$('.modal-body').load('Role/add_level_view');
	return false;
}
function loadData(){
    var level_user = $("#level_user").val();
    $.ajax({
        type:'GET',
        url :'Role/modul',
        data:'level_user='+level_user,
        success:function(html){
            $("#tabel-role").html(html);
        }
    })
}
function addRule(menu_id){
    var level_user = $("#level_user").val();
    if(!level_user){
		$("#alert_warning").trigger("click", 'Anda Belum Pilih Level');
		loadData();
	}else{
		$.ajax({
	        type:'GET',
	        url :'Role/add_role',
	        data:'level_user='+level_user+'&menu_id='+menu_id,
	        success:function(response){
	            if (response=='success') {
	            	$("#alert_success").trigger("click", 'Sukses Memberikan Akses');
	            }else{
	            	$("#alert_success").trigger("click", 'Sukses Menghapus Akses');
	            }
	        }
	    });
	}
    
}