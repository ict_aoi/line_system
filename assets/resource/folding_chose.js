
$(document).ready(function() {
	document.getElementById("scan_karton").focus();

	// data = 'x';
	// $.ajax({
	// 	url:'Folding/show_output_folding',
	// 	type: "POST",
	// 	// data: data,
	// 	contentType: false,       
	// 	cache: false,          
	// 	processData:false,
	// 	success: function(data){
	// 		var result = JSON.parse(data);
	// 		// console.log(result.data.totaloutput);
	// 		// console.log(result.data.totalkomplete);
	// 		$(".totaloutput").text(result.data.totaloutput);
	// 		$(".totalkomplete").text(result.data.totalkomplete);
	// 	}
	// });
	  
	$('#clear_po').click(function () {
		location.reload();
	});
	$('#change').click(function() {
		var flag = $('#flag_qtyscan').val();
		if (flag==0) {
			if ($('#flag').val()==0) {
				$("#scan_karton").addClass("hidden");
				$(".search_header").removeClass("hidden");
				$('#result').html(' ');
				$('#flag').val("1");
				$('#scan_karton').val('');
			}else if ($('#flag').val()==1) {
				$(".search_header").addClass("hidden");
				$("#scan_karton").removeClass("hidden");
				$('#result').html(' ');
				$('#scan_karton').focus(); 
				$('#scan_karton').val('');
				$('#flag').val("0");
			}
		}else{
			$("#alert_error").trigger("click", 'Ada Qty Scan yang belum tersimpan, lakukan save garment terlebih dahulu');
			var barcode = $('#barcodePackage').val();
			$('#scan_karton').val(barcode);
		}
	});
	$('#scan_karton').change(function () {
		// if (qty_Scan =is isNaN ) {}
		// var qty_ScanNan = isNaN(parseInt($('.qty_Scan').text()));
		var flag = $('#flag_qtyscan').val();

		// console.log(qty_scan);
		// console.log(qty_Scan==0);
		if (flag==0) {
			var url_loading = $('#loading_gif').attr('href');
			var data = 'scan_karton='+$('#scan_karton').val();			
			$.ajax({
	            url:'Folding/searchsubmitbyscan',
	            data:data,
	            type:'POST',
	            beforeSend: function () {

		          $.blockUI({
		              message: "<img src='" + url_loading + "' />",
		              css: {
		                backgroundColor: 'transaparant',
		                border: 'none',
		              }
		            });
		          },	            
	            success:function(response){
	                if (response==1) {
		              	$("#alert_warning").trigger("click", 'Karton Bukan Untuk Line Ini');
		              	document.getElementById("scan_karton").focus();
		              	$('#scan_karton').val('');
		              	$.unblockUI();
		              	$('#result').html(' ');
		            }else if (response==2) {
		            	$("#alert_warning").trigger("click", "Barcode Karton Tidak di temukan, Info Packing");	
		              	document.getElementById("scan_karton").focus();
		              	location.reload();
		              	$('#scan_karton').val(' ');
		              	$.unblockUI();
		              	$('#result').html(' ');
	                }else if (response==3) {
	                	$.unblockUI();
	                	$('#scan_karton').val('');
	                	$('#result').html(' ');
	                	$("#alert_warning").trigger("click", "Status karton masih preparation on progress(blm print) info packing");
	                	document.getElementById("scan_karton").focus();
	                }else if (response==4) {
	                	$.unblockUI();
	                	$('#scan_karton').val('');
	                	$('#result').html(' ');
	                	$("#alert_warning").trigger("click", "Karton Sudah Complete");
						document.getElementById("scan_karton").focus();
					}else if (response==5) {
	                	$.unblockUI();
	                	$('#scan_karton').val('');
	                	$('#result').html(' ');
	                	$("#alert_warning").trigger("click", "Karton Bermasalah, INFO PACKING!");
	                	document.getElementById("scan_karton").focus();
		            }else if (response==6) {
	                	$.unblockUI();
	                	$('#scan_karton').val('');
	                	$('#result').html(' ');
	                	$("#alert_warning").trigger("click", "Karton Status Bermasalah, INFO PACKING");
	                	document.getElementById("scan_karton").focus();
		            }else{
		                $('#result').html(response);
		                document.getElementById("scan").focus();
		                // $('.size').trigger('change');
		                 // $('#flag_qtyscan').val(1);
		                // $('#refresh').trigger("click");
		                $.unblockUI();
		            }     
	            },
	            error:function(response){
	                if (response.status==500) {
	                  $.unblockUI();
	                  $('#scan').val('');
	                  $('#result').html(' ');
	                  // location.reload();
	                  document.getElementById("scan_karton").focus();
	                }                    
	            }
	        })
		}else{
			$("#alert_warning").trigger("click", 'Ada Qty Scan yang belum tersimpan, lakukan save garment terlebih dahulu');
			var barcode = $('#barcodePackage').val();
			$('#scan_karton').val(barcode);
		}
		
	});
	$('.search_header').submit(function() {
		var po = $('#poreference').val();
		var size = $('#size').val();
		var url_loading = $('#loading_gif').attr('href');
		var flag = $('#flag_qtyscan').val();
		if (flag==0) {
			if (!po && !size) {
				$("#alert_warning").trigger("click", 'PO BUYER TIDAK BOLEH KOSONG');
			}else{
				var data = new FormData(this);
				$.ajax({
					url:'Folding/searchsubmitfolding',
	                type: "POST",
	                data: data,
	                contentType: false,       
	                cache: false,          
	                processData:false,
	                beforeSend: function () {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});
					},
					success: function(response){
		              if (response=='gagal') {
		              	$("#alert_warning").trigger("click", 'Data Tidak Ada');
		              	$.unblockUI();
		              }else{
		                $('#result').html(response);
		                document.getElementById("scan").focus();
		                $.unblockUI();
		              }

		          	},
		          	error:function(response){
			          if (response.status==500) {
			            $("#alert_info").trigger("click", 'Cek Input Data');
			            $.unblockUI();
			          }
			      }
				});
			}
			return false;
		}else{
			$("#alert_warning").trigger("click", 'Ada Qty Scan yang belum tersimpan, lakukan save garment terlebih dahulu');

		}
		return false;
	});
	
});

function loadpo() {
	var url_loading = $('#loading_gif').attr('href');
	var flag = $('#flag_qtyscan').val();
	if (flag==0) {
		$("#myModal").modal('show');
		$('.modal-title').text("Pilih PO BUYER");
		$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
		$('.modal-body').load('Folding/loadporeference');
	}else{
		$("#alert_warning").trigger("click", 'Ada Qty Scan yang belum tersimpan, lakukan save garment terlebih dahulu');
	}
	
	return false;
}
function loadsize() {
	var url_loading = $('#loading_gif').attr('href');
	var id = $('#id').val()
	var po = $('#poreference').val();
	var flag = $('#flag_qtyscan').val();
	if (flag==0) {
		if (id!='') {
			$("#myModal").modal('show');
			$('.modal-title').text("Pilih PO");
			$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
			$('.modal-body').load('Folding/showsize?id='+id+'&po='+po);
			return false;
		}
	}else{
		$("#alert_warning").trigger("click", 'Ada Qty Scan yang belum tersimpan, lakukan save garment terlebih dahulu');
	}
}
function outputfolding() {
	var url_loading = $('#loading_gif').attr('href');
	var modal = $('#myModal > div > div');
	$('#myModal').modal('show');
	modal.children('.modal-header').children('.modal-title').html('Report OutputFolding');
	modal.parent('.modal-dialog').addClass('modal-lg');
	modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
	modal.children('.modal-body').load('Folding/showOutputFolding');
	return false;
}

$('#openReport').click(function(event) {
//{backdrop: 'static', keyboard: true}
	$('#modal_report').modal();
	report_karton();
});

  
function report_karton(){
	var url_loading = $('#loading_gif').attr('href');

	var table =$('#table-result').DataTable({
		"processing": true,
		"serverSide": true,
		"searching": false,
		// "order": [],
		"orderMulti"  : true,
		"ajax":{
			"url": "folding/folding_output_ajax",
			"dataType": "json",
			"type": "POST",
			"beforeSend": function () {
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
					backgroundColor: 'transparant',
					border: 'none',
					}
				});
				},
				"complete": function() {
				$.unblockUI();
				},
				fixedColumns: true,
				"data":function(data) {
				data.pobuyer   = $('.pobuyer').val();
				// data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
				},
					},
		"columns": [           
					{ "data" : null, 'sortable' : false},
					{ "data": "poreference",'sortable' : false },
					{ "data": "style",'sortable' : false },
					{ "data": "size"},
					{ "data": "total_karton",'sortable' : false },
					{ "data": "total_qty",'sortable' : false },
					{ "data": "action",'sortable' : false },
				],
				fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
	});
	$('#table-user_filter input').unbind();
	$('#table-user_filter input').bind('keyup', function(e) {
		if (e.keyCode == 13 || $(this).val().length == 0 ) {
			table.search($(this).val()).draw();
		}
		// if ($(this).val().length == 0 || $(this).val().length >= 3) {
		//     table.search($(this).val()).draw();
		// }
	});

		  
	$('.tampil').click(function () {
		var po = $(".pobuyer").val();
		table.draw();
	});
		  
}

function detail_karton(){
	var table2 =$('#table-detail').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti"  : true,
        "ajax":{
         "url": "folding/folding_detail_ajax",
         "dataType": "json",
         "type": "POST",
         "beforeSend": function () {
                $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transparant',
                    border: 'none',
                  }
                });
              },
              "complete": function() {
                $.unblockUI();
              },
              fixedColumns: true,
             "data":function(data) {
                data.pobuyer = $('#poref').val();
                data.size    = $('#po_size').val();
                data.karton  = $('.karton').val();
                // console.log(data.pobuyer);
                // console.log(data.size);
                // data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
              },
                   },
        "columns": [
                  { "data" : null, 'sortable' : false},
                  { "data": "poreference",'sortable' : false },
                  { "data": "style",'sortable' : false },
                  { "data": "size",'sortable' : false },
                  { "data": "qty_size",'sortable' : false },
                  { "data": "qty_karton",'sortable' : false },
                  { "data": "barcode_package" },
                  { "data": "nama_line",'sortable' : false },
                  { "data": "create_on" },
                  { "data": "complete_on" },
              ],
              fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    });
    $('#table-detail_filter input').unbind();
    $('#table-detail_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 5 ) {
            table2.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
    });

    $('.tampilkan').click(function () {
        table2.draw();
    });
}
    

// });


function detail(po,size) {
    var url_loading = $('#loading_gif').attr('href');
    var modal = $('#myModal_ > div > div');
    $('#myModal_').modal('show');
    modal.children('.modal-header').children('.modal-title').html('Report OutputFolding');
    modal.parent('.modal-dialog').addClass('modal-lg');
    modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
    modal.children('.modal-body').load('Folding/detail_folding_output?po='+po+'&size='+size);
}
