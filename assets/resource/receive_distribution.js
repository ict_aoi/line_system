var url_loading = $('#loading_gif').attr('href');

$(function () {
	var url_loading = $('#loading_gif').attr('href');
	var items 		= JSON.parse($('#items').val());
	$('#scanbandle').change(function () {
		alert('msg');
	});

	function render(){
		getIndex();
		$('#items').val(JSON.stringify(items));
		var tmpl = $('#items-table').html();
		Mustache.parse(tmpl);
		var data = { item: items };
		var html = Mustache.render(tmpl, data);
		$('#tbody-items').html(html);
		document.getElementById('scanbandle').focus();
		bind();
	}

	function bind(){
		$('.btn-delete-item').on('click', deleteItem);
		$('#scanbandle').keypress(function (e) {
	      if (e.keyCode == 13) {
	        e.preventDefault();
	        scanbandle();
	        document.getElementById('scanbandle').focus();
	      }
	    });
	}
	
	function getIndex(){
		for (idx in items) {
			items[idx]['_id'] = idx;
			items[idx]['no'] = parseInt(idx) + 1;
		}
	}
	
	function deleteItem(){
		var i 	  = $(this).data('id');
		var subid = items[i]['id'];
		items.splice(i, 1);
		render();
	}

	function checkItem(barcode_id, index) {
	    for (var i in items) {
	      var proses = items[i];	      
	      if (i == index)
	        continue;
	      
	      if (proses.barcode_id == barcode_id )	        
	        return false;
	    }

	    return true;
	}

	function checkItem2(barcode_id, komponen, index){
		for(var i in items){
			var proses = items[i];			
			if (i == index)
			  continue;
			
			if (proses.barcode_id == barcode_id && proses.component_name == komponen)			  
			  return false;
		}
		return true;
	}

	function scanbandle() {	    
	    var data = 'barcode_id='+$('#scanbandle').val();
	    if ($('#scanbandle').val()!='') {
		    	$.ajax({
	            url			: 'Distribusi/scanbandle',
	            data		: data,
	            type		: 'POST',
	            beforeSend	: function () {
					$('#scanbandle').val('');
					$.blockUI({
						message	: "<img src='" + url_loading + "' />",
						css		: {
							backgroundColor	: 'transaparant',
							border			: 'none',
						}
						});
				},
	            success		: function(response){	  
					response = JSON.parse(response);              					
	                if (response.status==200) {
	                	$.unblockUI();
						if(response.sources == 'sds'){
							var diff = checkItem(response.data.barcode_id);

							if (!diff) {
								$("#alert_error").trigger("click", 'Barcode Sudah di Scan');
								$('#scanbandle').focus();
								return;
							}							

							var input = {
								'barcode_id'	 : response.data.barcode_id,
								'style'			 : response.data.style,
								'poreference'	 : response.data.poreference,
								'qty'			 : response.data.qty,
								'article'		 : response.data.article,
								'component_name' : response.data.component_name,
								'cut_num'		 : response.data.cut_num,
								'startend'		 : response.data.startend,
								'size'			 : response.data.size,
								'sources'		 : response.sources
							}; 						
							items.push(input);							 
							render();							 
						}

						if(response.sources == 'cdms_new'){
							var i = 0;												
							$.each(response.data, function(k, v){
								var diff2 = checkItem2(response.data[i].barcode_id, response.data[i].component_name);								
								if (!diff2) {
									render();
								} else {
									var input = {
									   'barcode_id'		 : response.data[i].barcode_id,
									   'style' 			 : response.data[i].style,
									   'poreference'	 : response.data[i].poreference,
									   'qty'			 : response.data[i].qty,
									   'article'		 : response.data[i].article,
									   'component_name'	 : response.data[i].component_name,
									   'cut_num'		 : response.data[i].cut_num,
									   'startend'		 : response.data[i].startend,
									   'size'			 : response.data[i].size,
									   'sources'		 : response.sources
									}; 						
									items.push(input);
									render();
								}
								i++;
							});						
						}							
						$('#scanbandle').focus();

	                } else if (response.status==500) {
	                	$.unblockUI();
	                	$("#alert_warning").trigger("click", response.pesan);
	                	$('#scanbandle').focus();
	                }
	            },
	            error	: function(response){
	                if (response.status==500) {
	                  $.unblockUI();
	                  $('#scanbandle').focus();
	                  $("#alert_info").trigger("click", 'Contact Administrator');
	                }                    
	            }
	        });	    
		    render();

	    } else{
	    	$("#alert_error").trigger("click", 'Barcode Tidak Boleh Kosong');
	    	$('#scanbandle').focus();
	    }
	 }
	$('.frmsave').submit(function() {
	    var data = new FormData(this);
	    if (items.length==0) {
	      $("#alert_error").trigger("click", 'data tidak lengkap');
	    }else{
	      $.ajax({
	          url			: 'Distribusi/SaveReceiveBundle',
	          type			: "POST",
	          data			: data,
	          contentType	: false,       
	          cache			: false,          
	          processData	: false,	        
	          beforeSend	: function () {
	          $.blockUI({
					message	: "<img src='" + url_loading + "' />",
					css		: {
						backgroundColor	: 'transaparant',
						border			: 'none',
					}
	            });
	          },
	          success	: function(response){
	          	var response = JSON.parse(response);				
	            if(response.status == 200){
					$.unblockUI();
					$("#alert_success").trigger("click", response.pesan);
					items = [];
					render();
					$('#scanbandle').focus();
	            }else{
	            	$.unblockUI();
	            	$("#alert_error").trigger("click", response.pesan);
	            	$('#scanbandle').focus();
	            }
	          }
	          ,error:function(response){
	            if (response.status==500) {
					$.unblockUI();
					$("#alert_info").trigger("click", 'Contact Administrator');
					$('#scanbandle').focus();
	            }
	            $.unblockUI();        
	          },	        
	      })
	          
	    }
	    return false;
	  })
	render();

});
