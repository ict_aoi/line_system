$(document).ready(function() {
  var element = document.getElementById("container");
   element.classList.add("sidebar-closed");
   $('#clear_line').click(function() {
        $('#line').val('');
        $('#poreference').val('');
        $('#result').html(' ');
    });
   $('.search_header').submit(function() {
   		var line = $('#line').val();
      var poreference = $('#poreference').val();
   		if(!poreference){
            $("#alert_warning").trigger("click", 'PO BUYER TIDAK BOLEH KOSONG');
            $('#result').html(' ');
        }else if(!line){
            $("#alert_warning").trigger("click", 'LINE TIDAK BOLEH KOSONG');
            $('#result').html(' ');
        }else{
        	var data = new FormData(this);
            var url_loading = $('#loading_gif').attr('href');
            $.ajax({
                url:'Qc_inspect/searchsubmit',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false,
                beforeSend: function () {
					$.blockUI({
						message: "<img src='" + url_loading + "' />",
						css: {
							backgroundColor: 'transaparant',
							border: 'none',
						}
					});
				},
          success: function(response){
              if (response=='gagal') {
              $("#alert_warning").trigger("click", 'Data Tidak Ada');
              $.unblockUI();
              }else{
                $('#result').html(response);
                $.unblockUI();
              }
          }
          ,
        error:function(response){
          if (response.status=500) {
            $("#alert_info").trigger("click", 'Cek Input Data');
            $.unblockUI();
          }
      }
      });
        }

   		return false
   })
});

function loadline() {
	var url_loading = $('#loading_gif').attr('href');
	$("#myModal").modal('show');
	$('.modal-title').text("Pilih LINE");
	$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
	$('.modal-body').load('Qc_inspect/loadline');
	return false;
}


function loadpo() {

  var url_loading = $('#loading_gif').attr('href');
  var line = $('#line_id').val();
  if (line!='') {
    $("#myModal").modal('show');
    $('.modal-title').text("Pilih LINE");
    $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
    $('.modal-body').load('Qc_inspect/loadpobuyer?id='+line);
    return false;
  }else{
    $("#alert_warning").trigger("click", 'Silahkan Pilih Line');
  }
}