$(document).ready(function () {
	 var table =$('#table-menu').DataTable({
	    "processing": true,
	    "serverSide": true,
	    // "order": [],
	    "orderMulti"  : true,
	    "ajax":{
	     "url": "Menu/list_menu",
	     "dataType": "json",
	     "type": "POST",
	     "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
	                   },
	"columns": [
	          {"data" : null, 'sortable' : false},
	          { "data": "nama_menu" },
	          { "data": "link" },
	          { "data": "main_menu" },
	          { "data": "action" },
	       ],
		  fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
	});
	$('#refresh').bind('click', function () {
	 	$('#table-menu').DataTable().ajax.reload();
	});
});
function tambah(){       
	$("#myModal").modal('show');
	$('.modal-title').text("Add Menu");
	$('.modal-body').html('<center>Loading..</center>');
	$('.modal-body').load('Menu/add_menu_view');
	return false;
}
function edit_menu(id){       
    $("#myModal").modal('show');
    $('.modal-title').text("Edit Menu");
    $('.modal-body').html('<center>Loading..</center>');
    $('.modal-body').load('menu/edit_menu_view?menu_id='+id);
    return false;
}
function hapus(id) {
	swal({
	  title: "Apakah anda yakin?",
	  text: "Data akan di hapus?",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((wildelete) => {
	  if (wildelete) {
	  	$.ajax({
	  		url:'menu/delete_menu?menu_id='+id,
	  		success:function(value){
	  			if(value=='sukses'){
	  				$("#alert_success").trigger("click", 'Hapus data Berhasil');
				    $('#table-menu').DataTable().ajax.reload();
	  			}else{
	  				$("#alert_error").trigger("click", 'Hapus data gagal');
	  			}
	  		}

	  	});
	    
	  }
	});
}
