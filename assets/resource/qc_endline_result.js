var url_loading = $('#loading_gif').attr('href');
var base_url = $('.logo').attr('href');
setTimeout(function(){ 
    load_data();

 },180000);
$(document).ready(function() {
	$('#refresh').click(function() {
		load_data();
	});

	$('#good').click(function() {
		good();
	});
	
	$('#defect').click(function() {
		adddefect();
	});

	$('#view_result').click(function() {
		viewresult();
	});

	$('#defect_outstanding').click(function() {
		defect_outstanding();
	});

	$('#change').click(function() {
		if ($('#flag').val()==0) {
			$(".qty").removeClass("hidden");
			$(".keyboard").removeClass("hidden");
			$('.qty').val('');
			$('#flag').val("1");
			$('.qty').focus();
			// document.getElementById("qty").focus();
		}else{
			$(".qty").addClass("hidden");
			$(".keyboard").addClass("hidden");
			$('#flag').val("0");
			$('.qty').val('');
		}
	});
	$('.qty').change(function () {
		var  qty =parseInt($('.qty').val());
		var balance = parseInt($('.balance').text());
		var wft = $('.wft').text();
		var repairing =  parseInt($('.repairing').text());
		var outbalance = (balance+repairing)+qty;
		var data    = 'style='+$('#style').val()+'&id='+$('#id').val()+'&size_id='+$('#size_id').val();
		// if (wft-qty<0) {
		if (wft-qty<=0) {
			$("#alert_error").trigger("click", 'Input QTY Tidak Boleh Lebih Dari WIP');
			$('#refresh').trigger("click");	
		}else{
			if (qty>10) {
				$("#alert_error").trigger("click", 'Input Masal Tidak Boleh Lebih Dari 10');
				$.unblockUI();
			}else if(qty>wft){
				$("#alert_error").trigger("click", 'Input QTY Tidak Boleh Lebih Dari WIP');
				$.unblockUI();
			}
			else{
				if (outbalance>0){
					swal({
						title: "PERHATIAN!!!",
						text: "QTY Sudah Balance, Apakah Anda Yakin Untuk Melanjutkan",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})
					.then((wilsave) => {
						if (wilsave) {
							good_masal();
						}
						$.unblockUI(); //buat menutup modal
					});
					document.getElementById("qty").focus();
					return false;
				}
				// if((qty+balance)>0){
				// 	swal({
				// 		title: "PERHATIAN!!!",
				// 		text: "Kebanyakan, QTY Akan Over. Apakah Anda Yakin Untuk Melanjutkan",
				// 		icon: "warning",
				// 		buttons: true,
				// 		dangerMode: true,
				// 	})
				// 	.then((wilsave) => {
				// 		if (wilsave) {
				// 			good_masal();
				// 		}
				// 		$.unblockUI(); //buat menutup modal
				// 	});
				// 	document.getElementById("qty").focus();
				// 	return false;
				// }
				else{
					good_masal();
				}
			}



			// if (qty>10) {
			// 	$("#alert_warning").trigger("click", 'Input Masal Tidak Boleh Lebih Dari 10');
			// }else if(qty>wft){
			// 	$("#alert_warning").trigger("click", 'Input QTY Tidak Boleh Lebih Dari WIP');
			
			// }else{
			// 	if (outbalance>0) {
			// 		swal({
			// 			title: "PERHATIAN!!!",
			// 			text: "QTY Sudah Balance, Apakah Anda Yakin Untuk Melanjutkan",
			// 			icon: "warning",
			// 			buttons: true,
			// 			dangerMode: true,
			// 		})
			// 		.then((wilsave) => {
			// 		  if (wilsave) {
			// 		  	good_masal();
			// 		  }
			// 		});
			// 		document.getElementById("qty").focus();
			// 		return false;
			// 	}
			// 	good_masal();	
			// }
		}
	});
	function good_masal() {
		var qty     = parseInt($('.qty').val());
		var wft     = parseInt($('.wft').text());
		var balance = parseInt($('.balance').text());
		var daily   = parseInt($('.daily').text());
		var all     = parseInt($('.all').text());
		var data    = 'style='+$('#style').val()+'&id='+$('#id').val()+'&size_id='+$('#size_id').val()+'&qty='+qty;
		if (qty!='') {
			if(qty>0){
				// if (wft-qty>=0) {
				if (wft-qty>0) {
					$.ajax({
						url:'Qc/qc_masal',
						data:data,
						type:'POST',
						beforeSend: function () {
							
							$.blockUI({
								message: "<img src='" + url_loading + "' />",
								css: {
									backgroundColor: 'transaparant',
									border: 'none',
								}
							});
						},	            
						success:function(data){
							var result = JSON.parse(data);
						
							if (result.status==200) {
								var wip           = wft-qty;
								var balance_after = balance+qty;
								var daily_after   = daily+qty;
								var all_after     = all+qty;
								   $('.wft').text(wip);
								   $('.balance').text(balance_after);
								   $('.daily').text(daily_after);
								   $('.all').text(all_after);
								$.unblockUI();
								$(".qty").addClass("hidden");
								$(".keyboard").addClass("hidden");
								$('#flag').val("0");
								$('.qty').val('');
							}else{
								$.unblockUI();
								$("#alert_warning").trigger("click", 'Gagal');
								$(".qty").addClass("hidden");
								$(".keyboard").addClass("hidden");
								$('#flag').val("0");
								$('.qty').val('');
								$('#refresh').trigger("click");	
							}
			
						},
						error:function(data){
							$.unblockUI();
							window.location.replace(base_url);
						},
						// timeout: 15000
					});
				}else{
					$("#alert_error").trigger("click", 'Inputan melebihi WIP');
				}
				
			}
			else{
				swal({
					title: "For your information",
					text: ("INPUT LEBIH DARI 0").toUpperCase(),
					//confirmButtonColor: "#2196F3",
					showConfirmButton: false,
					icon: "info"
				});
				$.unblockUI();
			}
			
		}
	}
	
	load_data();
});
/**/
function load_data() {
	var data = 'id='+$('#id').val()+'&size='+$('#size_id').val();

    var currentRequest = null;    

	currentRequest = jQuery.ajax({
        url:'Qc/loaddata',
        data:data,
        type:'POST',	            
        success:function(data){
                     
        },
        error:function(data){
            if (data.status==500) {
              $.unblockUI();
            }                    
        }
    })
    .done(function(data){
		var result = JSON.parse(data);
		console.log(result.article);
            $('.wft').text(result.wft);
            $('.balance').text(result.balance);
            $('.all').text(result.counterall);
            $('.repairing').text(result.repairing);
            $('.total_repairing').text(result.total_repairing);
            $('.daily').text(result.counterDaily);                
			$('.style').text(result.style);                
			$('.article').text(result.article);   
            $('#qc_id').val(result.qc_id);
            if (result.balance>0) {
            	$('#balance-head').text("OVER");
            }     
    });

}
function viewresult() {
	var url_loading = $('#loading_gif').attr('href');
	var modal = $('#myModal > div > div');
	$('#myModal').modal('show');
	modal.children('.modal-header').children('.modal-title').html('Summary Cek');
	modal.parent('.modal-dialog').addClass('modal-lg');
	modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
	modal.children('.modal-body').load('Qc/resultinspect?line_id='+$('#line_id').val()+'&factory='+$('#factory_id').val());
	return false;

}
function defect_outstanding() {
	var url_loading = $('#loading_gif').attr('href');
	$("#myModal").modal('show');
	$('.modal-title').text("Defect Outstanding");
	$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
	$('.modal-body').load('Qc/qcoutstanding');
	return false;
}
function statuspo() {
	var url_loading = $('#loading_gif').attr('href');
	var modal = $('#myModal > div > div');
	$('#myModal').modal('show');
	modal.children('.modal-header').children('.modal-title').html('Cek Status PO');
	modal.parent('.modal-dialog').addClass('modal-lg');
	modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
	modal.children('.modal-body').load('Qc/cekstatuspo');
	return false;

}
/**/

function adddefect() {
	var wft = parseInt($('.wft').text());
	var balance = parseInt($('.balance').text());
	var repairing =  parseInt($('.repairing').text());

	var data    = 'style='+$('#style').val()+'&id='+$('#id').val()+'&size_id='+$('#size_id').val();
	// console.log(wft<0);
	if (wft<=0) {
		// if (balance!=0) {
			$("#alert_warning").trigger("click", 'tidak ada tumpukan garment');
			
		/*}else{
			$("#alert_warning").trigger("click", 'po buyer telah balance');
		}*/
	}else{
		if ((balance+repairing)>=0) {
			swal({
				title: "PERHATIAN!!!",
				text: "QTY Sudah Balance, Apakah Anda Yakin Untuk Melanjutkan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((wilsave) => {
			  if (wilsave) {
			  	viewadddefect();
			  }
			});
			return false;
		}
		viewadddefect();
		
	}
	
}
function viewadddefect() {
	$("#myModal").modal('hide');
	var url_loading = $('#loading_gif').attr('href');
	$("#myModal").modal('show');
	$('.modal-title').text("Pilih Defect");
	$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
	$('.modal-body').load('Qc/showdefect?id='+$('#id').val()+'&size_id='+$('#size_id').val()+'&style='+$('#style').val());
	return false;
}
function good() {
	var balance = parseInt($('.balance').text());
	var wft = $('.wft').text();
	var repairing =  parseInt($('.repairing').text());
	var data    = 'style='+$('#style').val()+'&id='+$('#id').val()+'&size_id='+$('#size_id').val();
	if (wft<=0) {
		// if (balance!=0) {
			$("#alert_warning").trigger("click", 'tidak ada tumpukan garment');
			$('#refresh').trigger("click");	
		/*}else{
			$("#alert_warning").trigger("click", 'po buyer telah balance');
		}*/
	}else{
		if ((balance+repairing)>=0) {
			swal({
				title: "PERHATIAN!!!",
				text: "QTY Sudah Balance, Apakah Anda Yakin Untuk Melanjutkan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((wilsave) => {
			  if (wilsave) {
			  	savegood();
			  }
			});
			return false;
		}
		savegood();		
	}

}
function savegood() {
	var wft     = parseInt($('.wft').text());
	var balance = parseInt($('.balance').text());
	var daily   = parseInt($('.daily').text());
	var all     = parseInt($('.all').text());
	var data    = 'style='+$('#style').val()+'&article='+$('#article').val()+'&id='+$('#id').val()+'&size_id='+$('#size_id').val()+'&counter_daily='+daily;
	console.log(data);
	$.ajax({
	    url:'Qc/qc_up',
	    data:data,
	    type:'POST',
	    beforeSend: function () {
			
			$.blockUI({
				message: "<img src='" + url_loading + "' />",
				css: {
					backgroundColor: 'transaparant',
					border: 'none',
				}
			});
		},	            
	    success:function(data){
	    	var result = JSON.parse(data);
	        if (result.status==200) {
				$.unblockUI();
	        	// var wip           = result.wip;
				// var balance_after = result.balance;
								
	        	// var wip           = wft-1;
				// var balance_after = balance+1;
				// var daily_after   = daily+1;
				// var all_after     = all+1;
				// $('.wft').text(wip);
				// $('.balance').text(balance_after);
				// $('.daily').text(daily_after);
				// $('.all').text(all_after);
				load_data();
			// }
			// else if(result.status==100){
			// 	swal({
			// 		title: "For your information",
			// 		text: ("WIP 0").toUpperCase(),
			// 		//confirmButtonColor: "#2196F3",
			// 		showConfirmButton: false,
			// 		icon: "info"
			// 	});
			// 	$.unblockUI();	
			// 	$('#refresh').trigger("click");
				
			}else{
	        	$.unblockUI();
	        	$("#alert_warning").trigger("click", 'Gagal');
	        	$('#refresh').trigger("click");
	        }
	        // $('#refresh').trigger("click");

	    },
	    error:function(data){
	        $.unblockUI();
	        // window.location.replace(base_url);
	    },
	    // timeout: 10000
	});
}
function repairing() {
	var modal       = $('#myModal > div > div');
	var url_loading = $('#loading_gif').attr('href');
	var repairing   = $('.repairing').text();
	var qc_id       = $('#qc_id').val();
	if (repairing!=0) {
		$('#myModal').modal('show');
		modal.children('.modal-header').children('.modal-title').html('Repairing Proses');
		modal.parent('.modal-dialog').addClass('modal-lg');
		modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:190px;margin-right=150px;width=100px'>");
		modal.children('.modal-body').load('Qc/repairing_list?id='+qc_id);
		return false;
	}else{
		$("#alert_warning").trigger("click", 'tidak ada List repairing');
	}
}

    function detail(qc_endline_id) {
        var url_loading = $('#loading_gif').attr('href');
        var modal = $('#myModal_ > div > div');
        $('#myModal_').modal('show');
        modal.children('.modal-header').children('.modal-title').html('Detail Buku Hijau');
        modal.parent('.modal-dialog').addClass('modal-lg');
        modal.children('.modal-body').html("<img src='"+url_loading+"' style='margin-left:300px;margin-right=150px;width=100px'>");
        modal.children('.modal-body').load('Qc/detailComponent?qc_endline_id='+qc_endline_id);
        return false;
    }
