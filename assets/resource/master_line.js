$(document).ready(function() {
    var table =$('#table-list').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti"  : true,
        "ajax":{
         "url": "Master_line/list_line",
         "dataType": "json",
         "type": "POST",
         "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                       },
    "columns": [
              {"data" : null, 'sortable' : false},
              { "data": "line_name" },
              { "data": "mesin_id" },
              { "data": "factory" },
              { "data": "action" },
           ],
          fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
    });

    $('.pilih').submit(function(){
        var data = new FormData(this);
            
        $.ajax({
            url:'master_line/savesubmit',
            type: "POST",
            data: data,
            contentType: false,       
            cache: false,          
            processData:false,
            success: function(response){
                if (response=='sukses') {
                    $("#alert_success").trigger("click", 'Sukses');
                    $('#table-list').DataTable().ajax.reload()
                }
            }
        }); 
        return false
    });
})
var i = 1;
function add_item() {
	
    var proseslist = document.getElementById('proseslist');

//                membuat element
    var row = document.createElement('tr');
    var no = document.createElement('td');
    var nama = document.createElement('td');
    var aksi = document.createElement('td');
    aksi.setAttribute('width', '50px');

//                meng append element
    proseslist.appendChild(row);
    row.appendChild(no);
    row.appendChild(nama);
    row.appendChild(aksi);


//                membuat element input
    var nama_input = document.createElement('input');
    nama_input.setAttribute('class', 'form-control');
    nama_input.setAttribute('name', 'line_name[' + i + ']');
    nama_input.setAttribute("type", "input");
    nama_input.setAttribute("required", "");
    nama_input.setAttribute("autofocus", "");
    

    var hapus = document.createElement('span');

//                meng append element input
	// no.appendChild(no);
    nama.appendChild(nama_input);
    aksi.appendChild(hapus);

    // no.innerHTML = i;

    hapus.innerHTML = "<a class='btn btn-danger'><i class='fa fa-times'></i> Hapus</a>";
//                membuat aksi delete element
    hapus.onclick = function () {
        row.parentNode.removeChild(row);
    };

    i++;
}
 function hapus(id) {
    var myNode = document.getElementById("proseslist");
	while (myNode.firstChild) {
	    myNode.removeChild(myNode.firstChild);
	}
}
Mousetrap.bind('enter', function(e) {
    add_item();
});
function edit_proses(id,act){
    var url_loading = $('#loading_gif').attr('href');
    if (act==1 ) {
        $('.proses_'+id).prop('disabled',false).addClass('actived');
    }else{
        $('.proses_'+id).prop('disabled',true).removeClass('actived');
        var data = 'name='+$('.proses_'+id).val()+'&id='+id;

        $.ajax({
            url:'master_line/editlinesubmit',
            data:data,
            type:'POST',
            beforeSend: function () {
              $.blockUI({
                  message: "<img src='" + url_loading + "' />",
                  css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                  }
                });
              },
            success:function(response){
                if(response == 'sukses'){
                  $.unblockUI();
                  $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                  $('#table-list').DataTable().ajax.reload();
                }else{
                  $.unblockUI();
                  $('#table-list').DataTable().ajax.reload();
                }
            },
            error:function(response){
                if (response.status==500) {
                  $.unblockUI();
                  $("#alert_info").trigger("click", 'Contact Administrator');
                }                    
            }
        })
    }
    $('.edit_'+id).toggle();
}

 function edit(id) {
   
    // $('#myModal_').modal('hide');

    var modal = $('#myModal_ > div > div');
    $('#myModal_').modal('show');
    modal.children('.modal-title').text('List PO BUYER');
    modal.parent('.modal-dialog').addClass('modal-md');
    modal.children('.modal-body').load('Master_line/showedit?id='+id); 
  }
 function hapus(id) {
   swal({
      title: "Apakah anda yakin?",
      text: "Data akan di hapus?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((wildelete) => {
      if (wildelete) {
        $.ajax({
          url:'Master_line/delete_line?id='+id,
          success:function(response){
            var result = JSON.parse(response)
            if(result.status==200){
              $("#alert_success").trigger("click", result.pesan);
              $('#table-list').DataTable().ajax.reload();
            }else{
              $("#alert_info").trigger("click", result.pesan);
               $('#table-list').DataTable().ajax.reload();
            }
          }

        });
        
      }
    });
 }
 function active(id) {
   swal({
      title: "Apakah anda yakin?",
      text: "Line akan di Active?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((wildelete) => {
      if (wildelete) {
        $.ajax({
          url:'Master_line/active_line?id='+id,
          success:function(response){
            var result = JSON.parse(response)
            if(result.status==200){
              $("#alert_success").trigger("click", result.pesan);
              $('#table-list').DataTable().ajax.reload();
            }else{
              $("#alert_info").trigger("click", result.pesan);
               $('#table-list').DataTable().ajax.reload();
            }
          }

        });
        
      }
    });
 }
