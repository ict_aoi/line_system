var url_loading = $('#loading_gif').attr('href');
var remark = $('#remark').val();

$(document).ready(function() {
    $('#size').trigger('change');

    var scan = $('#scan').val();

    $('#refresh').click(function() {
        load_data_sets();
        document.getElementById("scan").focus();
    });
    $('#good').click(function() {
        if (parseInt($('#qty').val() != parseInt($('.qty_save').text()) * 2)) {
            folding_up();
        } else {
            $("#alert_error").trigger("click", 'QTY Karton Penuh');
        }

    });
    $('#barcode_id').change(function() {
        $("#change").addClass("hidden");
    });
    
    $('.size').change(function() {
        // alert('msg');
        var qty = $('.size').find(':selected').attr('data-qty');
        $('#qty').val(qty);
        load_data_sets();

    });

    function load_data_sets(barcode_id) {
        var data = 'barcode_id=' + $('#barcodePackage').val() + '&scan_id=' + $('#scan_id').val() + '&header_id=' + $('#header_id').val() +
            '&poreference=' + $('.poreference').text() + '&size=' + encodeURIComponent($('.size').val()) + '&style=' + $('.style').text() + '&article=' + $('.article').text() + '&rasio=' + $('#rasio').val();

        var rasio = $('#rasio').val();


        var currentRequest = null;

        currentRequest = jQuery.ajax({
            url: 'Folding/loadDataSetsScan',
            data: data,
            type: 'POST',
            success: function(data) {

            },
            error: function(data) {
                if (data.status == 500) {
                    $.unblockUI();
                }
            }
        }).done(function(data) {
            var result = JSON.parse(data);
            var qty = $('.size').find(':selected').attr('data-qty');
            $('#qty').val(qty);
            $('.wip1').html(result.wip_1);
            $('.wip2').html(result.wip_2);
            $('.name1').text('WIP ' + result.names.slice(0, 1));
            $('.name2').text('WIP ' + result.names.slice(1, 2));
            $('.qty_save').text(result.counterscan);
            $('.balance').html(result.balance);
            $('#barcode_garment').val(result.barcode_garment);
            $('#barcode_garment_hid').val(result.barcode_garment);
            $('#changebarcode').val(result.barcode_garment);
            $('.qty_scan').text('0');
            // $('.list_size').text(result.list_size);
            document.getElementById("scan").focus();
            if (result.balance > 0) {
                $('#balance').text("OVER");
            }
        });


        if (rasio == 1) {
            $.ajax({
                url: 'Folding/loadRasioSets',
                data: data,
                type: 'POST',
                success: function(data) {}
            }).done(function(data) {
                var result = JSON.parse(data);
                $('#counterscan_rasio').val(result.counterscan_rasio);
                $('.list_size').text(result.list_size);
            });
        }



    }
    $('#scan').change(function(e) {
        var scan = $('#scan').val().length;
        var barcode_garment = $('#barcode_garment').val();
        var scan2 = $('#scan').val();
        var wip1 = parseInt($('.wip1').text());
        var wip2 = parseInt($('.wip2').text());
        var rasio = $('#rasio').val();

        if (scan < 14) {
            // if (wip1 > 0 && wip2 > 0) {
            if (barcode_garment == 0) {
                $('#barcode_garment').val(scan2);
                $('#changebarcode').val(scan2);

                if (rasio == 1) {
                    scan_garmentrasio();
                } else {
                    scan_garment();
                }

            } else if (barcode_garment == $('#scan').val()) {

                $('#barcode_garment').val(scan2);
                $('#changebarcode').val(scan2);
                if (rasio == 1) {
                    scan_garmentrasio();
                } else {
                    scan_garment();
                }
            } else {
                $("#alert_warning").trigger("click", 'Barcode Garment Salah');
                $('#scan').val('');
                document.getElementById("scan").focus();
            }
            // } else {
            //     $("#alert_error").trigger("click", 'tidak ada tumpukan garment');
            //     // $('#refresh').trigger("click");
            // }
            /*if (scan<14) {
            	if ($('#rasio').val()==1) {
            		// console.log(parseInt($('#qty').val())-1;
            		if (parseInt($('#qtyscan_rasio').text())-1==parseInt($('.qty_save').text())) {
            			folding_up();
            			$("#alert_warning").trigger("click", 'QTY Rasio Sudah Penuh,sistem akan melanjutkan ke size lain nya');
            			$('#size option:selected').next().attr('selected', 'selected');
            			$('#size').trigger('change');
            		}else if (parseInt($('.qty_karton').text())!=parseInt($('.qty_save').text())){
            			folding_up();
            		}else{
            			$("#alert_error").trigger("click", 'QTY Rasio Sudah Penuh, Silahkan Pilih Size lain nya');
            			$('#size option:selected').next().attr('selected', 'selected');
            			$('#size').trigger('change');
            			$('#scan').val('');
            		}
            	}else{
            			if (wip1>0&&wip2>0) {
            				if (barcode_garment==0) {					
            					$('#barcode_garment').val(scan2);
            					scan_garment();
            				}else if(barcode_garment==$('#scan').val()) {
            					scan_garment();
            				}else{
            					$("#alert_warning").trigger("click", 'Barcode Garment Salah');
            					$('#scan').val('');
            					document.getElementById("scan").focus();
            				}
            			}else{
            				$("#alert_error").trigger("click", 'tidak ada tumpukan garment');
            				// $('#refresh').trigger("click");
            			}
            		
            	}*/
        } else {
            $("#alert_warning").trigger("click", 'Barcode Garment Salah');
            $('#scan').val('');
            document.getElementById("scan").focus();
        }

    });

    $('#simpan').click(function() {
        var save = parseInt($('.qty_save').text());
        var scan = parseInt($('.qty_scan').text());
        var barcode_garment = $('#barcode_garment').val();
        var qty_karton = parseInt($('.qty_karton').text());

        if (save + scan > 0) {
            if (save + scan == qty_karton) {
                complete_up();
            } else {
                folding_up(0)
            }
            $('#barcode_garment_hid').val(barcode_garment);
        } else {
            $("#alert_warning").trigger("click", 'QTY scan 0, data tidak bisa di simpan');
        }

    });
    load_data_sets();
});


function complete_up() {
    var save = parseInt($('.qty_save').text());
    var scan = parseInt($('.qty_scan').text());
    var qty_karton = parseInt($('.qty_karton').text());

    var data = 'barcode_package=' + $('#barcodePackage').val() + '&scan_id=' + $('#scan_id').val() +  '&source_data=' + $('#source_data').val();

    if ((save + scan) == qty_karton) {
        $.ajax({
            url: 'Folding/auto_complete_karton',
            data: data,
            type: 'POST',
            beforeSend: function() {
                $('#scan').val('');
                $.blockUI({
                    message: "<img src='" + url_loading + "' />",
                    css: {
                        backgroundColor: 'transaparant',
                        border: 'none',
                    }
                });
                // $('#scan').attr('disabled',true);
            },
            success: function(data) {
                var result = JSON.parse(data);
                if (result.status == 100) {
                    $.unblockUI();
                    $('#flag_qtyscan').val(0);
                    $('.status').html(result.draw);
                    document.getElementById("scan").focus();
                } else if (result.status == 500) {
                    $("#alert_warning").trigger("click", result.pesan);
                    $('#scan').val('');
                    $.unblockUI();
                    $('#refresh').trigger("click");
                    document.getElementById("scan").focus();
                }
                load_data_sets();
            },
            error: function(data) {
                if (data.status == 500) {
                    $.unblockUI();
                    $("#alert_warning").trigger("click", 'info ict');
                    $('#refresh').trigger("click");
                    $('#scan').val('');
                }
                load_data_sets();
            }
        });
    }
}

function folding_up(complete) {
    var balance = parseInt($('.balance').text());

    var qty_karton = parseInt($('.qty_karton').text());
    var qty_save = parseInt($('.qty_save').text());

    var data = 'style=' + $('#style_hid').val() + '&id=' + $('#header_id').val() +
        '&size_id=' + $('#sizeid').val() + '&barcode_package=' + $('#barcodePackage').val() +
        '&scan=' + $('#barcode_garment').val() + '&flag_barcode=' + $('#barcode_garment_hid').val() + '&remark=' + $('#remark').val() + '&poreference=' +
        $('.poreference').text() + '&size=' + encodeURIComponent($('.size').val()) + '&complete=' + complete + '&scan_id=' +
        $('#scan_id').val() + '&qty=' + $('.qty_scan').text() + '&article=' + $('.article').text() +  '&source_data=' + $('#source_data').val();
    //  +'&qty_size='+$('#qty').val();

    // if (wip1>0&&wip2>0) {
    // if (balance < 0) {
    $.ajax({
        url: 'Folding/foldingUpSetsScanMasal',
        data: data,
        type: 'POST',
        beforeSend: function() {
            $('#scan').val('');
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                    backgroundColor: 'transaparant',
                    border: 'none',
                }
            });
            // $('#scan').attr('disabled',true);
        },
        success: function(data) {
            var result = JSON.parse(data);
            if (result.status == 200) {
                $.unblockUI();
                hitungClientMasal();
                $('#flag_qtyscan').val(0);
                $('.status').html(result.draw);
                $('#counterscan_rasio').val(0);
                document.getElementById("scan").focus();
            } else if (result.status == 500) {
                $("#alert_warning").trigger("click", result.pesan);
                $('#scan').val('');
                $.unblockUI();
                document.getElementById("scan").focus();
            } else if (result.status == 501) {
                $("#alert_warning").trigger("click", result.pesan);
                location.reload();
            } else if (result.status == 504) {
                $.unblockUI();
                $("#alert_warning").trigger("click", result.pesan);
                $('#refresh').trigger("click");
                $('#flag_qtyscan').val(0);
                $('#qty').val(0);
                $('.qty_scan').text(0);
                document.getElementById("scan").focus();
            }
            // load_data_sets();
        },
        error: function(data) {
            if (data.status == 500) {
                $.unblockUI();
                $("#alert_warning").trigger("click", 'info ict');
                $('#refresh').trigger("click");
                $('#scan').val('');
            }
            load_data_sets();
        }

    });

    // } else {
    //     $("#alert_warning").trigger("click", 'po buyer telah balance');
    // }


}

function hitungClient() {
    var qty_save = parseInt($('.qty_save').text()) + 1;
    var balance = parseInt($('.balance').text()) + 1;
    var wip_1 = parseInt($('.wip1').text()) - 1;
    var wip_2 = parseInt($('.wip2').text()) - 1;
    var balance = parseInt($('.balance').text()) + 1;
    $('.qty_save').text(qty_save);
    $('.balance').text(balance);
    $('.wip1').text(wip_1);
    $('.wip2').text(wip_2);
}

function hitungClientMasal() {
    var qtyscan = parseInt($('.qty_scan').text());
    var qty_save = parseInt($('.qty_save').text()) + qtyscan;
    var counterscan_rasio = parseInt($('#counterscan_rasio').val()) + qtyscan;
    var balance = parseInt($('.balance').text()) + qtyscan;
    if ($('#rasio').val() == 1) {
        $('#counterscan_rasio').val(counterscan_rasio)
    }
    if ($('#switch').val() == 1) {
        $('.balance').text(balance);
    }
    $('.qty_save').text(qty_save);
    $('.qty_scan').text('0')

}

function scan_garment() {
    var qty_save = parseInt($('.qty_save').text());
    var qty_scan = parseInt($('.qty_scan').text());
    var qty_karton = parseInt($('.qty_karton').text());
    var wip_1 = parseInt($('.wip1').text()) - 1;
    var wip_2 = parseInt($('.wip2').text()) - 1;
    if (((qty_save + qty_scan)) == qty_karton - 1) {
        $('.qty_scan').text(qty_scan + 1);
        if ($('#switch').val() == 1) {
            $('.wip1').text(wip_1);
            $('.wip2').text(wip_2);
        }
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
        folding_up(1);
    } else if (qty_karton > (qty_save + qty_scan)) {
        $('.qty_scan').text(qty_scan + 1);
        if ($('#switch').val() == 1) {
            $('.wip1').text(wip_1);
            $('.wip2').text(wip_2);
        }
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
    } else {
        $("#alert_warning").trigger("click", 'QTY Karton penuh');
        location.reload();
        $('#scan').val('');
    }

}

function scan_garmentrasio() {
    var qty_save = parseInt($('.qty_save').text());
    var qty_scan = parseInt($('.qty_scan').text());
    var qty_karton = parseInt($('.qty_karton').text());
    var wip_1 = parseInt($('.wip1').text()) - 1;
    var wip_2 = parseInt($('.wip2').text()) - 1;
    var qty = parseInt($('#qty').val());
    var counterscan_rasios = parseInt($('#counterscan_rasio').val()) / 2;
    if ((counterscan_rasios + qty_scan) == qty - 1) {
        $('.qty_scan').text(qty_scan + 1);
        if ($('#switch').val() == 1) {
            $('.wip1').text(wip_1);
            $('.wip2').text(wip_2);
        }
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
        if ((qty_karton - 1) == (qty_save + qty_scan)) {
            var complete = 1;
        } else {
            var complete = 0;
        }
        folding_up(complete);
        $('.qty_scan').text('0');
        $('.size option:selected').next().attr('selected', 'selected');
        $('.size').trigger('change');
    } else if (qty > (counterscan_rasios + qty_scan)) {
        $('.qty_scan').text(qty_scan + 1);
        $('.wip1').text(wip_1);
        $('.wip2').text(wip_2);
        $('#scan').val('');
        $('#flag_qtyscan').val(1);
    } else {
        $('.size option:selected').next().attr('selected', 'selected');
        $('.size').trigger('change');
        $('#scan').val('');
        $("#alert_warning").trigger("click", 'QTY Rasio Penuh, Sistem akan merubah ke size selanjut nya');
    }

}

function loadhidden(po, size, article) {
    var data = 'po=' + po + '&size=' + size + '&article=' + article + '&style=' + $('.style').text();
    $.ajax({
        url: 'Folding/loadhiddenset',
        data: data,
        type: 'POST',
        success: function(data) {}
    }).done(function(data) {
        var result = JSON.parse(data);
        var qty_scan = $('.qty_scan').text();
        var wip_1 = parseInt(result.wip_1) - parseInt(qty_scan);
        var wip_2 = parseInt(result.wip_2) - parseInt(qty_scan);

        $('#switch').val(1);
        $('.wip1').text(wip_1);
        $('.wip2').text(wip_2);
        $('.balance').text(result.balance);
    });
}
function modal_revisibarcode() {
  var barcode_garment = $('#barcode_garment').val();
  var flag_barcode = $('#barcode_garment_hid').val();
  var url_loading = $('#loading_gif').attr('href');
  if (barcode_garment!=0) {
  	  $("#myModal").modal('show').css('overflowY', 'auto');
	  $('.modal-title').text("Revisi Barcode Garment");
	  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>").css('overflowY', 'auto');
	  $('.modal-body').load('folding/showmodalrevisibarcode?barcode_garment='+barcode_garment+'&header_id='+$('#header_id').val()+'&flag_barcode='+flag_barcode+'&size_id='+encodeURIComponent($('.size').val()));
	  return false;
  }else{
  	 $("#alert_warning").trigger("click", "Belum ada Barcode Garment Yang di Scan");
  	 return false;	
  }
  
}