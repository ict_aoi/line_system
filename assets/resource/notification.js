$(function() {
   // show_stack_custom_top('info');
    $('#alert_info').bind('click', function (event, arg1) {
        //show_stack_custom_top('info', arg1);
        //show_stack_custom_bottom('info', arg1);
        swal({
            title: "For your information",
            text: arg1.toUpperCase(),
            //confirmButtonColor: "#2196F3",
            showConfirmButton: false,
            icon: "info",
            timer: 2000
        });
    });

    $('#alert_success').bind('click', function (event, arg1) {
        //show_stack_custom_top('success', arg1);
       // show_stack_custom_bottom('success', arg1);
        swal({
            title: "Good job!",
            text: arg1.toUpperCase(),
           // confirmButtonColor: "#66BB6A",
            showConfirmButton: false,
            icon: "success",
            timer: 1000
        });
    });

    $('#alert_error').bind('click', function (event, arg1) {
        //show_stack_custom_top('error', arg1);
        //show_stack_custom_bottom('error', arg1);
        swal({
            title: "Oops...",
            text: arg1.toUpperCase(),
            //confirmButtonColor: "#EF5350",
            icon: "error",
            timer: 2000,
            showConfirmButton: false
        });
    });

    $('#alert_warning').bind('click', function (event, arg1) {
        //show_stack_custom_top('warning', arg1);
       // show_stack_custom_bottom('warning', arg1);
        swal({
            title: "Hm.. ?",
            text: arg1.toUpperCase(),
            icon: "warning",
            showConfirmButton:false,
            timer: 2000
        });
    });
});
