var url_loading = $('#loading_gif').attr('href');

$(function () {
	$('#scanbandle').focus();
	var url_loading = $('#loading_gif').attr('href');
	var items 		= JSON.parse($('#items').val());
	$('#scanbandle').change(function () {
		alert('msg');
	});

	function render(){
		getIndex();
		$('#items').val(JSON.stringify(items));
		var tmpl = $('#items-table').html();
		Mustache.parse(tmpl);
		var data = { item: items };
		var html = Mustache.render(tmpl, data);
		$('#tbody-items').html(html);
		document.getElementById('scanbandle').focus();
		bind();
	}

	function bind(){
		$('.btn-delete-item').on('click', deleteItem);
		$('#scanbandle').keypress(function (e) {
	      if (e.keyCode == 13) {
	        e.preventDefault();
	        scanbandle();
	        document.getElementById('scanbandle').focus();
	      }
	    });
	}
	
	function getIndex(){
		for (idx in items) {
			items[idx]['_id'] = idx;
			items[idx]['no'] = parseInt(idx) + 1;
		}
	}
	
	function deleteItem(){
		var i 			= $(this).data('id');
		var subid 		= items[i]['id'];
		var barcode_id 	= items[i]['barcode_id'];
		var komponen 	= items[i]['component_name'];	
	    if(barcode_id != -1){
	       var id = barcode_id;		   
	        $.ajax({
	            url		 : 'deletepindah',
	            data	 : {id, komponen},
				dataType : "json",
	            type	 : 'POST',
	            success	 : function(data){
	            	var result = data;					
	                if(result.status == 200){
	                  	items.splice(i, 1);
						render();
	                }else{
	                  $("#alert_info").trigger("click", 'Contact ICT');
	                }
	            },
	            error	 : function(response){
	                if (response.status==500) {
	                  $.unblockUI();
	                  $("#alert_info").trigger("click", 'Contact ICT');
	                }                    
	            }
	        });
	    }		
	}

	function checkItem(barcode_id, index) {
	    for (var i in items) {
	      var proses = items[i];	      
	      if (i == index)
	        continue;
	      
	      if (proses.barcode_id == barcode_id )	        
	        return false;
	    }

	    return true;
	}
	function checkItem2(barcode_id, komponen, index){
		for(var i in items){
			var proses = items[i];			
			if (i == index)
			  continue;
			
			if (proses.barcode_id == barcode_id && proses.component_name == komponen)			  
			  return false;
		}
		return true;
	}
	function scanbandle() {	    
	    var data = 'barcode_id='+$('#scanbandle').val();
	    if ($('#scanbandle').val()!='') {
		    	$.ajax({
	            url			: 'scanbandle_pindah',
	            data		: data,
	            type		: 'POST',
	            beforeSend	: function () {
		          $.blockUI({
		              message	: "<img src='" + url_loading + "' />",
		              css		: {
		                backgroundColor	: 'transparant',
		                border			: 'none',
		              }
		            });
		          },
	            success		: function(data){
	                var result = JSON.parse(data);

	                if (result.status==200) {
	                	$.unblockUI();
						if(result.sources == 'sds'){
							var diff = checkItem(result.barcode_id);
							if (!diff) {
								$("#alert_error").trigger("click", 'Barcode Sudah di Scan');
								$('#scanbandle').focus();
								return;
							}							

							var input = {
							   'distribusi_id'	: result.data.distribusi_id,
							   'barcode_id'		: result.data.barcode_id,
							   'style'			: result.data.style,
							   'poreference'	: result.data.poreference,
							   'qty'			: result.data.qty,
							   'article'		: result.data.article,
							   'component_name'	: result.data.component_name,
							   'cut_num'		: result.data.cut_num,
							   'startend'		: result.data.startend,
							   'size'			: result.data.size,
							   'sources' 		: result.sources
							}; 
						  items.push(input);
						  render();
						}

						if(result.sources == 'cdms_new'){
							var i = 0;							
							$.each(result.data, function(k, v){
								var diff2 = checkItem2(result.data[i].barcode_id, result.data[i].component_name);								
								if (!diff2) {
									render();
								} else{
									var input = {
										'distribusi_id'	 : result.data[i].data.distribusi_id,
										'barcode_id'	 : result.data[i].data.barcode_id,
										'style'			 : result.data[i].data.style,
										'poreference'	 : result.data[i].data.poreference,
										'qty'			 : result.data[i].data.qty,
										'article'		 : result.data[i].data.article,
										'component_name' : result.data[i].data.component_name,
										'cut_num'		 : result.data[i].data.cut_num,
										'startend'		 : result.data[i].data.startend,
										'size'			 : result.data[i].data.size,
										'sources' 		 : result.data[i].data.sources
									}; 
									items.push(input);
						  			render();
								}
								i++;
							});							
						}
				      $('#scanbandle').focus();
	                }else if (result.status==500) {
	                	$.unblockUI();
	                	$("#alert_warning").trigger("click", result.pesan);
	                	$('#scanbandle').focus();
	                }
	            },
	            error:function(response){
	                if (response.status==500) {
	                  $.unblockUI();
	                  $('#scanbandle').focus();
	                  $("#alert_info").trigger("click", 'Contact Administrator');
	                }                    
	            }
	        });	    
		    render();
	    } else{
	    	$("#alert_error").trigger("click", 'Barcode Tidak Boleh Kosong');
	    	$('#scanbandle').focus();
	    }
	 }

	$('.frmsave').submit(function() {
	    var data = new FormData(this);
	    if (items.length==0) {
	      	$("#alert_error").trigger("click", 'data tidak lengkap');
	    }else{
			$.ajax({
				url			: 'SavePindahBundle',
				type		: "POST",
				data		: data,
				contentType	: false,       
				cache		: false,          
				processData	: false,
				beforeSend	: function () {					
					$.blockUI({
						message	: "<img src='" + url_loading + "' />",
						css		: {
							backgroundColor	: 'transaparant',
							border			: 'none',
						}
					});
				},
				success		: function(response){
					var result= JSON.parse(response);
					if(result.status == 200){
						$.unblockUI();
						$("#alert_success").trigger("click", result.pesan);
						items = [];
						render();
						$('#scanbandle').focus();
					}else{
						$.unblockUI();
						$("#alert_error").trigger("click", result.pesan);
						$('#scanbandle').focus();
					}
				}
				,error:function(response){
					if (response.status==500) {
						$.unblockUI();
						$("#alert_info").trigger("click", 'Contact Administrator');
						$('#scanbandle').focus();
					}
					$.unblockUI();
						
				},
				/*timeout: 2000*/
			})	          
	    }
	    return false;
	  })
	render();

});