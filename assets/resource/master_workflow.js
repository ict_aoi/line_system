
$(document).ready(function() {
  
$('#gsd').keyup(function() {
   var gsd = $('#gsd').val();
   $('#gsd-hid').val(gsd)
});

$(".proses_choses").chosen();
  var items = JSON.parse($('#items').val());
  $('#clear_line').click(function() {
    $('#style').val('');
    $('#style_hid').val('');
  });

  function render() {
    getIndex();
    setBoolean();
    $('#items').val(JSON.stringify(items));
    var tmpl = $('#items-table').html();

    // console.log(tmpl);
    
    Mustache.parse(tmpl);
    var data = { item: items };
    var html = Mustache.render(tmpl, data);
    $('#tbody-items').html(html);
    bind();
   
  }
  function bind(){
    $('#prosesId').on('change', tambahitem);
    $('.btn-delete-item').on('click', deleteItem);
    $('.btn-edit-item').on('click', editCycleItem);
    $('.btn-save-item').on('click', saveCycleItem);

    $('.input-new').keypress(function (e) {
      if (e.keyCode == 13) {
        e.preventDefault();
        tambahitem();
        document.getElementById("prosesName").focus();
      }
    });
  }

  function getIndex(){
    for (idx in items) {
      items[idx]['_id'] = idx;
      items[idx]['no'] = parseInt(idx) + 1;
    }
  }

  function setBoolean(){
    for (idx in items) {
			var data =  items[idx];
			// console.log(data.is_critical);
      if(data.lastproses == 'f')
       data.lastproses = false;

     	if(data.lastproses == 't')
			 data.lastproses = true;
			
			//  console.log(data);
			if(data.is_critical != null){
				
				if(data.is_critical == 'f')
				data.cekcrit = false;

				if(data.is_critical == 't')
				data.cekcrit = true;
			}

    }


  }
  function checkItem(proses_id, index) {
    for (var i in items) {
      var proses = items[i];
      // console.log(proses_id.proses_id);
      if (i == index)
        continue;
      
      if (proses.proses_id == proses_id )
        // console.log('obj');
        return false;
    }

    return true;
  }
  function tambahitem() {
    var temp       = $('#prosesId').val();
    var temp_cycle = $('#cycleId').val();

    var style       = $('#style_hid').val();
    var proses_id   = temp.split(':')[0] ;
    var proses_name = temp.split(':')[1] ;
    var last_proses = temp.split(':')[2] ;
    var cekcrit     = temp.split(':')[3] ;
    var cycletime   = temp_cycle;
    if (last_proses== 'true') {
      var islast = true;
    }else{
      var islast = false;
		}
		
		if (cekcrit== 'true') {
      var iscrit = true;
    }else{
      var iscrit = false;
    }
    
    
    if (style=='') {
      $("#alert_error").trigger("click", 'Pilih Style Dulu');
    }
    else{
      if(proses_id==''){
        $("#alert_error").trigger("click", 'Pilih Proses Dulu');
      }
      else{
        if(cycletime == ''){
          $("#alert_error").trigger("click", 'Input Cycle Time Dulu');
          
        }else{
          var diff = checkItem(proses_id);
  
          if (!diff) {
            $("#alert_error").trigger("click", 'proses sudah ada silahkan pilih proses lain');
            return;
          }
          var input = {
          'master_workflow_id': -1,
          'proses_id': proses_id,
          'proses_name': proses_name,
          'cycle_time': cycletime,
					'lastproses': islast,
					'cekcrit': iscrit,
          }; 
          items.push(input);
          render();
          $("#cklastproses").prop("checked", false);
          $("#ckcritical").prop("checked", false);
          $("#cycle").val('');
        }
      }
    } 
  }

  function deleteItem(){
    var i = parseInt($(this).data('id'), 10);

    var master_workflow_id = items[i]['master_workflow_id'];
    if(master_workflow_id != -1){
       var data = 'id='+master_workflow_id;

        $.ajax({
            url:'deletesubmit',
            data:data,
            type:'POST',
            success:function(response){
                if(response == 'sukses'){
                  $.unblockUI();
                  $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                  $('#table-proses').DataTable.ajax.reload();
                }else{
                  $.unblockUI();
                  $('#table-proses').DataTable.ajax.reload()
                }
            },
            error:function(response){
                if (response.status=500) {
                  $.unblockUI();
                  $("#alert_info").trigger("click", 'Contact Administrator');
                }                    
            }
        });
    }
    items.splice(i, 1);
    render();
  }

  function editCycleItem(){
    var i = parseInt($(this).data('id'), 10);
    // console.log(i);
    // console.log(items[i]['cycle_time']);
    // console.log(items[i]['cekcrit']);
    $('#in_cycle_'+i).prop("disabled", false);
    $('#cekcrit_'+i).prop("disabled", false);
    $('#edit_cycle_'+i).hide();
    // $('button').toggle(true);  //shows button
    $('#save_cycle_'+i).css("display","inline");
  }

  function saveCycleItem(params) {
    
    var i = parseInt($(this).data('id'), 10);
    // .prop("hidden", false);; //hides button
		
		// var crit_baru = $('#cekcrit_'+i).val();
		var cycle_baru = $('#in_cycle_'+i).val();
		
		
		if ($('#cekcrit_'+i).prop('checked')==true) {
			var crit_baru = 't';
		}else{
			var crit_baru = 'f';
		}
		
    // console.log(crit_baru);
    $('#in_cycle_'+i).val(cycle_baru);
    $('#cekcrit_'+i).val(crit_baru);
    items[i].cycle_time  = cycle_baru;
    items[i].is_critical = crit_baru;
    
    // console.log(items[i]['cycle_time']);
    // console.log(items[i]['cekcrit']);
    
    $('#save_cycle_'+i).css("display","none");
    $('#in_cycle_'+i).prop("disabled", true);
    $('#cekcrit_'+i).prop("disabled", true);
    $('#edit_cycle_'+i).show();

    render();
    // var master_workflow_id = items[i]['master_workflow_id'];
    // if(master_workflow_id != -1){
    //    var data = 'id='+master_workflow_id;

    //     $.ajax({
    //         url:'deletesubmit',
    //         data:data,
    //         type:'POST',
    //         success:function(response){
    //             if(response == 'sukses'){
    //               $.unblockUI();
    //               $("#alert_success").trigger("click", 'Simpan Data Berhasil');
    //               $('#table-proses').DataTable.ajax.reload();
    //             }else{
    //               $.unblockUI();
    //               $('#table-proses').DataTable.ajax.reload()
    //             }
    //         },
    //         error:function(response){
    //             if (response.status=500) {
    //               $.unblockUI();
    //               $("#alert_info").trigger("click", 'Contact Administrator');
    //             }                    
    //         }
    //     });
    // }
    // items.splice(i, 1);
    // render();
  }

  render();

  function checkedItem() {
    var i = $(this).data('id');
    if ($('#check_'+i).prop('checked') == true) {
      items[i].lastproses = true;
    }else{
      items[i].lastproses = false;
		}
		if ($('#cekcrit_'+i).prop('checked') == true) {
      items[i].cekcrit = true;
    }else{
      items[i].cekcrit = false;
    }
    render();
  }

  function check_last_is_exists(){
      var flag = 0;
      for (idx in items) {
        var data = items[idx];

        if(data.lastproses == true){
            flag++;
        }
      }
      return flag;
  }

  $('.frmsave').submit(function() {
      var base_url    = $('#base_url').val();
      var data        = new FormData(this);
      var style       = $('#style_hid').val();
      var gsd         = $('#gsd').val();
      var url_loading = $('#loading_gif').attr('href');
      if (style=='' && items.length<1) {
        $("#alert_error").trigger("click", 'Data Silahkan di lengkapi');
      }else{

        var is_exists = check_last_is_exists();

        if(is_exists == 0){
           $("#alert_error").trigger("click", 'pilih last proses terlebih dahulu');
        }else if(is_exists >=2){
          $("#alert_error").trigger("click", 'last proses tidak boleh lebih dari 1');
        }else if(gsd==''){ 
          $("#alert_error").trigger("click", 'GSD SMV harus di isi!!');
        }else{
          $.ajax({
            url:'savesubmit',
            type: "POST",
            data: data,
            contentType: false,       
            cache: false,          
            processData:false,
            beforeSend: function () {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transaparant',
                  border: 'none',
                }
              });
            }, 
            success: function(response){
              if(response == 'sukses'){
                $.unblockUI();
                $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                window.location.replace(base_url);
              }else{
                $.unblockUI();
                $("#alert_error").trigger("click", 'Cek inputan kembali');
              }
            }
            ,error:function(response){
              if (response.status=500) {
                $.unblockUI();
                $("#alert_info").trigger("click", 'Contact Administrator');
              }
                      
            }
          });
        }        
      }
      return false; 
  });
  $('.frmupdate').submit(function() {
    var base_url = $('#base_url').val();
      var data  = new FormData(this);

      // console.log(data);
      var style = $('#style_hid').val();
      var gsd         = $('#gsd').val();
      var url_loading = $('#loading_gif').attr('href');
      if (style=='' && items.length<1) {
        $("#alert_error").trigger("click", 'Data Silahkan di lengkapi');
      }else{
        var is_exists = check_last_is_exists();
        if (is_exists == 0) {
          $("#alert_error").trigger("click", 'pilih last proses terlebih dahulu');
        }else if (is_exists >=2) {
          $("#alert_error").trigger("click", 'last proses tidak boleh lebih dari 1');
        }else if(gsd==''){ 
          $("#alert_error").trigger("click", 'GSD SMV harus di isi!!'); 
        }else{
          $.ajax({
            url:'editsubmit',
            type: "POST",
            data: data,
            contentType: false,       
            cache: false,          
            processData:false,
            beforeSend: function () {
            $.blockUI({
                message: "<img src='" + url_loading + "' />",
                css: {
                  backgroundColor: 'transaparant',
                  border: 'none',
                }
              });
            }, 
            success: function(response){
              if(response == 'sukses'){
                $.unblockUI();
                $("#alert_success").trigger("click", 'Simpan Data Berhasil');
                window.location.replace(base_url);
              }else{
                $.unblockUI();
                $("#alert_error").trigger("click", 'Cek inputan kembali');
              }
            }
            ,error:function(response){
              if (response.status=500) {
                $.unblockUI();
                $("#alert_info").trigger("click", 'Contact Administrator');
              }
                      
                  }
          });
        }
        
      }
    return false;
  })
  
});
function loadstyle() {
  var url_loading = $('#loading_gif').attr('href');
  $("#myModal").modal('show');
  $('.modal-title').text("Pilih Style");
  $('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
  $('.modal-body').load('loadstyle');
  return false;
}
function addProses() {
  var cycle = $('#cycle').val();
  var proses = $('#prosesName').val();
  var prosesName = $('#prosesName').find('option:selected').attr("name");
   
  if ($('#cklastproses').prop('checked')==true) {
    var islastproses = true;
  }else{
    var islastproses = false;
	}
	
	if ($('#ckcritical').prop('checked')==true) {
    var criticalproses = true;
  }else{
    var criticalproses = false;
	}

	// console.log($('#ckcritical').val());
	// console.log(criticalproses);
	
  var temp = proses+':'+prosesName+':'+islastproses+':'+criticalproses;
  //  var temp_cycle = cycle;

  $('#cycleId').val(cycle).trigger('change');
  $('#prosesId').val(temp).trigger('change');
}
