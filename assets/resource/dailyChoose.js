$(function () {
	var url_loading = $('#loading_gif').attr('href');
	$('#clear_line').click(function () {
		location.reload();
	});
	$('#refresh').click(function() {
		loaddata();
	});
	$('#search').click(function () {
		var line_id = $('#line_id').val();
		if (!line_id) {
			$("#alert_error").trigger("click", 'line tidak boleh kosong');
		}else{
			loaddata();
		}
	});

	/*$('.search_style').submit(function() {
		var data = new FormData(this);
		$.ajax({
			url:'Daily/searchStyle',
            type: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            dataType: "JSON",
            beforeSend: function () {
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
						backgroundColor: 'transparant',
						border: 'none',
					}
				});
			},
			success: function(response){
				$.unblockUI();
				// console.log(response);
				var table = $('#table-daily tbody').empty();
				$('#style_id').val('');
				$('#order_qty').val(0);

				$.each(response, function(index, value) {
					var totaltarget = value['totaltarget'] != null ? value['totaltarget'] : 0;
					var status = '';
					$('#table-daily tbody').append('<tr>'+
													'<td>'+value['style']+'</td>'+
													'<td class="">'+totaltarget+'</td>'+
													'<td>'+
													'<div class="hidden">'+
                          								'<span u_style>'+value["style"]+'</span>'+
                          								'<span u_order_qty>'+value["totaltarget"]+'</span>'+
                      								'</div>'+
                      								'<button type="button" class="btn btn-xs btn-success" onclick="pilih_style(this)">SELECT</button>'+
													'</td>'+
													'</tr>');
				});

          	},
          	error:function(response){
	          if (response.status==500) {
	            $("#alert_info").trigger("click", 'Cek Input Data');
	            $.unblockUI();
	          }
	      }
		});
		return false;
	});*/
	function loaddata() {
		var data = 'id='+$('#line_id').val();
		$.ajax({
			url: "Daily/loaddata",
			type: 'post',
			data:data,
			beforeSend: function () {
	          $.blockUI({
	              message: "<img src='" + url_loading + "' />",
	              css: {
	                backgroundColor: 'transaparant',
	                border: 'none',
	              }
	            });
	          },
			success: function (data) {
				var result = JSON.parse(data);
				$.unblockUI();
				$('#result').html(result);
	        	
			}
		});
	}
	loaddata();

	$('#edit_input').click(function () {
		// location.reload();

		var line = $('#line_id').val();

		if(!line){	
			$("#alert_error").trigger("click", 'Pilih Line Terlebih Dahulu');
		}
		else{

			$("#myModal").modal('show');
			var url_loading = $('#loading_gif').attr('href');
			$('.modal-title').text("Pilih PO BUYER");
			$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
			$('.modal-body').load('Daily/load_inputan?id='+line);
			return false;
		}
	});
});

function pilih_style(self) {
	var url_loading = $('#loading_gif').attr('href');

	var data = $(self).parent().parent();
	var style = data.find('[u_style]').text();
	var order_qty = data.find('[u_order_qty]').text() != "null" ? data.find('[u_order_qty]').text() : 0;

	var line_id = $('#line_id').val();

	$('#style_id').val(style);

	$('#order_qty').val(order_qty);

	$.ajax({
			url:'Daily/dailyStyleByDate',
            type: "POST",
            data: {style_id:style, line_id:line_id },
            // contentType: false,
            // cache: false,
            // processData:false,
            dataType: "JSON",
            beforeSend: function () {
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
						backgroundColor: 'transparant',
						border: 'none',
					}
				});
			},
			success: function(response){
				// console.log(response['result_style'], response['result_style'].length);
				$.unblockUI();

				if (response['result_style'].length == 0) {
					$('#inputStyle').val(style);
					$('#inputQtyOrder').val(order_qty);
					$('#inputChangeOver').val('');
					$('#inputGsdSmv').val('');
					$('#inputPresentSewer').val('');
					$('#inputWorkingHours').val('');
				}else{
					var rows = response['result_style'];
					var details = response['detail_style'];
					$('#inputStyle').val(rows['style']);
					$('#inputQtyOrder').val(rows['order_qty']);
					$('#inputChangeOver').val(rows['change_over']);
					$('#inputGsdSmv').val(rows['gsd_smv']);

					if (response['detail_style'].length == 0) {
						$('#inputPresentSewer').val('');
						$('#inputWorkingHours').val('');
					}else{
						$('#inputPresentSewer').val(details['present_sewer']);
						$('#inputWorkingHours').val(details['working_hours']);
					}
				}

				$('#dailyModal').modal({backdrop: 'static'});

          	},
          	error:function(response){
	            $.unblockUI();
	      }
		});

	return false;
}

$('#dailyModal').on('shown.bs.modal', function() {
	$('#inputPresentSewer').focus();
});

$('#dailyModal').on('hidden.bs.modal', function(event) {
	$('#inputStyle').val('');
	$('#inputQtyOrder').val('');
	$('#inputChangeOver').val('');
	$('#inputGsdSmv').val('');
	$('#inputPresentSewer').val('');
	$('#inputWorkingHours').val('');
});

$('#inputPresentSewer, #inputGsdSmv, #inputWorkingHours').on('focus', function() {
	$(this).select();
});

/*function submitDaily() {
	var url_loading = $('#loading_gif').attr('href');
	var line_id = $('#line_id').val();
	var style = $('#inputStyle').val();
	var order_qty = $('#inputQtyOrder').val();
	var change_over = $('#inputChangeOver').val();
	var gsd_smv = $('#inputGsdSmv').val();
	var present_sewer = $('#inputPresentSewer').val();
	var working_hours = $('#inputWorkingHours').val();
	var targetoutput = $('#inputTargetOutput').val();

	if (style == '' || order_qty == '' || change_over == '' || present_sewer == '' || working_hours == '' || gsd_smv == '' || targetoutput=='' ) {
		$("#alert_warning").trigger("click", 'Data Tidak Lengkap');
		return false;
	}else{
		$.ajax({
			url: 'Daily/submitDaily',
			type: 'POST',
			dataType: 'JSON',
			data: {style: style, order_qty: order_qty, change_over: change_over, gsd_smv: gsd_smv, present_sewer: present_sewer,
					working_hours, line_id: line_id,targetoutput:targetoutput},
			beforeSend: function () {
				$('#dailyModal').css('z-index', 0);
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
						backgroundColor: 'transparant',
						border: 'none',
					}
				});
			},
			success: function(response){
				$.unblockUI();
				$('#dailyModal').css('z-index', '');

				if (response['status'] == 200) {
					$("#alert_success").trigger("click", 'Simpan Data Berhasil');
					$('#dailyModal').modal('hide');
				}else if (response['status'] == 500) {
					$("#alert_error").trigger("click", 'Ada kesalahan Simpan Data');
				}

          	},
          	error:function(response){
	            $.unblockUI();
	            $('#dailyModal').css('z-index', '');
	      }
		});

	}
}*/

function loadline() {
	var url_loading = $('#loading_gif').attr('href');
	$("#myModal").modal('show');
	$('.modal-title').text("Pilih LINE");
	$('.modal-body').html("<img src='"+url_loading+"' style='margin-left:150px;margin-right=150px;width=100px'>");
	$('.modal-body').load('Daily/showLine');
	return false;
}
function tambah(style,order,gsd_smv,previous_co,line_id,working,present_sewer,present_folding,target_output) {
	var val = $('.tambah_pop').html();
	
	$('#myModal').modal({backdrop: 'static', keyboard: false});
	$('.modal-dialog').addClass('modal-sm');
	$('.modal-title').text('Form Daily Line');
	$('.modal-body').html(val);
	$('.style').val(style);
	$('.order').val(order);
	$('.gsd_smv').val(gsd_smv);
	$('.line_id_hid').val(line_id);
	$('.working_hours').val(working);
	$('.present_sewer').val(present_sewer);
	$('.present_folding').val(present_folding);
	$('.target_output').val(target_output);
	$("div.select_co select").val(previous_co);
	$('.submittambah').submit(function(){
		var data = new FormData(this);
		$('.submittambah').attr('disabled',true);
		$.ajax({
			url:'Daily/submitDaily_v2',
			type: "POST",
			data: data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(response){
				var result = JSON.parse(response);
				if (result.status==200) {
					$('#myModal').modal('hide');
					$("#alert_success").trigger("click", 'Simpan Data Berhasil');
					$('#refresh').trigger("click");	
				}else{
					$("#alert_error").trigger("click", 'Simpan data gagal');
					$('#refresh').trigger("click");	
				}

			}
            ,error:function(data){
                if (data.status==500) {
                  $.unblockUI();
                  $("#alert_warning").trigger("click", 'info ict');
                  $('#refresh').trigger("click");	
                }
            }
         });   
		return false;
	})
}
