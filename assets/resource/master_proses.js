$(document).ready(function() {
  var url_loading = $('#loading_gif').attr('href');
  var items = JSON.parse($('#items').val());
  function render() {
    getIndex();
    $('#items').val(JSON.stringify(items));
    var tmpl = $('#items-table').html();
    Mustache.parse(tmpl);
    var data = { item: items };
    var html = Mustache.render(tmpl, data);
    $('#tbody-items').html(html);
    bind();
  }
  function bind(){
    $('#addItem').on('click', tambahitem);
    $('.btn-delete-item').on('click', deleteItem);

    $('.input-new').keypress(function (e) {
      if (e.keyCode == 13) {
        e.preventDefault();

        tambahitem();

        document.getElementById("prosesName").focus();
      }
    });

  }
  function getIndex(){
    for (idx in items) {
      items[idx]['_id'] = idx;
      items[idx]['no'] = parseInt(idx) + 1;
    }
  }

  function tambahitem() {
    var proses = $('#prosesName').val()
    var input = {
      'id': 1,
      'proses_name': proses,
    };
    if (proses) {
      items.push(input);
    }
    
    render();
  }

  function deleteItem(){
    var i = parseInt($(this).data('id'), 10);

    items.splice(i, 1);
    render();
  }

  $('.frmsave').submit(function() {
    var base_url = $('#base_url').val();
    var data = new FormData(this);
    if (items.length==0) {
      $("#alert_error").trigger("click", 'data tidak lengkap');
    }else{
      $.ajax({
          url:'savesubmitproses',
          type: "POST",
          data: data,
          contentType: false,       
          cache: false,          
          processData:false,
          beforeSend: function () {
          $.blockUI({
              message: "<img src='" + url_loading + "' />",
              css: {
                backgroundColor: 'transaparant',
                border: 'none',
              }
            });
          },
          success: function(response){
            if(response == 'sukses'){
              $.unblockUI();
              $("#alert_success").trigger("click", 'Simpan Data Berhasil');
              window.location.replace(base_url);
            }
          }
          ,error:function(response){
            if (response.status==500) {
              $.unblockUI();
              $("#alert_info").trigger("click", 'Contact Administrator');
            }
                    
          }
      })
          
    }
    return false;
  })
  render()

});

