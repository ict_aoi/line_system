
var url_loading = $('#loading_gif').attr('href');
var remark = $('#remark').val();

$(document).ready(function() {
	$('#cmbSize').trigger('change');

	var scan = $('#scan').val();
	var barcode = $('#barcode_id').val();
	
	$('#refresh').click(function() {
		load_data();
	});
	$('#good').click(function() {
		
		if (parseInt($('.balance').text())<0) {
			folding_up();
		}else{
			$("#alert_error").trigger("click", 'PO Dan Size Sudah Balance');
		}
		
	});
	$('#barcode_id').change(function() {
		$("#change").addClass("hidden");
	})
	$('#login_distribution').click(function() {
		$('.view_').html('');
		var val = $('.login_pop').html();
		$('#myModal').modal({backdrop: 'static', keyboard: false});
		$('.modal-dialog').addClass('modal-md');
		$('.modal-title').text('Upload Packing List');
		$('.modal-body').html(val);
	});
	function load_data() {			
		var data = 'id='+$('#header_id').val()+'&size='+$('.size').text()+'&barcode_id='+$('#barcodePackage').val()+'&scan_id='+$('#scan_id').val();

        var currentRequest = null;    

		currentRequest = jQuery.ajax({
            url:'Folding/loadDataSets',
            data:data,
            type:'POST',	            
            success:function(data){
                        
            },
            error:function(data){
                if (data.status==500) {
                  $.unblockUI();
                }                    
            }
        }).done(function(data){
        	var result = JSON.parse(data);
            $('.wft').text(result.wft);
            $('.balance').text(result.balance);
            $('.counter_folding').text(result.counter_folding);
            $('.countertoday').text(result.countertoday);               
            $('.qty_scan').text(result.qty_scan);               
            $('#qc_id').val(result.qc_id);                
            $('.status').html(result.draw);
            if (result.balance>0) {
            	$('#balance').text("OVER");
            }     
        });

	}
	load_data();
	$(document).idle({
	    onIdle: function(){
	    	var qtyidle = parseInt($('#qtyidle').val());
	    	console.log(qtyidle>0);
	        if (qtyidle>0) {
	        	upsave();
	        }      
	    },
	    idle: 3000
	});
});
function folding_up() {
	var balance  = parseInt($('.balance').text());
	var wft      = parseInt($('.wft').text());
	var qtyidle = parseInt($('#qtyidle').val()); 
	
	if (wft>0) {
		if (balance<0) {
			$(".proses").removeClass("hidden");
			$(".selesai").addClass("hidden");
			$('#flag_qtyscan').val('1');
			$('#qtyidle').val(qtyidle+1);
			hitungClient();

		}else{
			$("#alert_warning").trigger("click", 'po buyer telah balance');
		}
	}else{
		$('#scan').val('');
		$("#alert_warning").trigger("click", 'tidak ada tumpukan garment');
		// $('#refresh').trigger("click");
	}

} 
function hitungClient() {
	var wip             = parseInt($('.wft').text())-1;
	var counter_folding = parseInt($('.counter_folding').text())+1;
	var qty_scan        = parseInt($('.qty_scan').text())+1;
	var balance         = parseInt($('.balance').text())+1;
	$('.wft').text(wip);
	$('.counter_folding').text(counter_folding);
	$('.qty_scan').text(qty_scan);
	$('.balance').text(balance);

}
function upsave(argument) {
	var data    = 'id='+$('#header_id').val()+'&barcode_package='+$('#barcodePackage').val()
					+'&scan='+$('#scan').val()+'&remark='+$('#remark').val()
					+'&poreference='+$('.poreference').text()+'&size='+$('.size').text()
					+'&style='+$('.style').text()+'&qtyidle='+$('#qtyidle').val()
					+'&size_id='+$('#sizeid').val();
	$.ajax({
        url:'Folding/saveidle',
        data:data,
        type:'POST',
        beforeSend: function () {
	    	$('#qtyidle').val('0');
			$.blockUI({
				message: "<img src='" + url_loading + "' />",
				css: {
					backgroundColor: 'transaparant',
					border: 'none',
				}
			});
		},	            
        success:function(data){
        	var result = JSON.parse(data);
        	if (result.status==200) {
        		$.unblockUI();
        		$("#alert_success").trigger("click", result.pesan);
        		$('#refresh').trigger("click");
        		$(".proses").addClass("hidden");
        		$(".selesai").removeClass("hidden");
				$('#flag_qtyscan').val('0');
        	}else if (result.status==500) {
        		$("#alert_warning").trigger("click", result.pesan);
        		$('#scan').val('');
	            $.unblockUI();
	            $('#refresh').trigger("click");
	            $('#refresh').trigger("click");
	        }else if (result.status==501) {
	        	$("#alert_warning").trigger("click", result.pesan);
	        	location.reload();
	        }
        },error:function (data) {
        	if (data.status==500) {
              $.unblockUI();
              $("#alert_warning").trigger("click", 'info ict');
              $('#scan').val('');
            }
        }

    });
}