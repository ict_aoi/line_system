$(document).ready(function() {
    var datepicker = $('#datestartactual');

    if (datepicker.length > 0) {
         datepicker.datepicker({
          format: "yyyy-mm-dd",
          startDate: new Date()
        });
    }
    
    $('#clear').click(function() {
        reset()
    });
    $('#clear_line').click(function() {
        reset();
        
    });

    $('.add_header').submit(function() {
        var poreference = $('#poreference').val();
        var style = $('#style').val();
        var line = $('#line').val();
        
        if(!poreference){
            $("#alert_warning").trigger("click", 'PO BUYER DAN STYLE TIDAK BOLEH KOSONG');
        }else if(!line){
            $("#alert_warning").trigger("click", 'LINE TIDAK BOLEH KOSONG');
        }else{
            var data = new FormData(this);
            
            $.ajax({
                url:'addheader_submit',
                type: "POST",
                data: data,
                contentType: false,       
                cache: false,          
                processData:false,
                success: function(response){
                    var base_url = $('#base_url').val();;
                    if (response=='sukses') {
                        $('#myModal').modal('hide');
                        $("#alert_success").trigger("click", 'Good Job');
                        $('#table-list').DataTable().ajax.reload();
                        window.location.replace(base_url);
                    }else{
                        $("#alert_error").trigger("click", 'Data Sudah Ada, Cek Kembali Inputan');
                    }
                },
		          error:function(response){
		            if (response.status=500) {
		              $("#alert_info").trigger("click", 'Failed');
		            }
		        }

            });
        }        
        return false;
    })
})
function reset() {
    $('#poreference').val('');
    $('#style').val('');
    $('#line_id').val('');
    $('#line').val('');
    $('#style').val('');
    $('#datestartschedule').val('');
    $('#datefinishschedule').val('');
    $('#exportdate').val('');
    $('#orderqty').val('');
    $('#result').html('');
}
function search() {
    var modal = $('#myModal_ > div > div');
    $('#myModal_').modal('show');
    modal.children('.modal-title').text('List PO BUYER');
    modal.parent('.modal-dialog').addClass('modal-md');
    modal.children('.modal-body').load('loadporeference');      
}
function loadline() {
    var modal = $('#myModal_ > div > div');
    $('#myModal_').modal('show');
    modal.children('.modal-title').text('LIST LINE');
    modal.parent('.modal-dialog').addClass('modal-md');
    modal.children('.modal-body').load('loadline');      
}



function editsize(size,qty,act) {
    if (act==1) {
        $('#qtyordered_'+size).prop('disabled',false).addClass('actived');
    }else if(act==2){
        var qtyinput = parseInt($('#qtyordered_'+size).val());
        
        if (qtyinput>parseInt(qty)) {
            $("#alert_error").trigger("click", 'qty tidak boleh melebihi order');
            $('#qtyordered_'+size).prop('disabled',false).addClass('actived');
            $('.edit_'+size).toggle();
        }else{
           $('#qtyordered_'+size).prop('disabled',true).removeClass('actived');
           var gabung =size+";"+qtyinput;
           $('#check_'+size).val(gabung); 
        }

    }else{
        $('#qtyordered_'+size).prop('disabled',true).removeClass('actived');
        $('#qtyordered_'+size).val(qty);
    }
   $('.edit_'+size).toggle();
}