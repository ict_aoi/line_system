$(document).ready(function () {
	 var table =$('#table-user').DataTable({
	    "processing": true,
	    "serverSide": true,
	    // "order": [],
	    "orderMulti"  : true,
	    "ajax":{
	     "url": "list_user",
	     "dataType": "json",
	     "type": "POST",
	     "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
	                   },
	"columns": [
	          {"data" : null, 'sortable' : false},
	          { "data": "username" },
	          { "data": "nik" },
	          { "data": "nohp" },
	          { "data": "level" },
	          { "data": "factory" },
	          { "data": "action" },
	       ],
		  fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
	});
	$('#table-user_filter input').unbind();
    $('#table-user_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0 ) {
            table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
    });
	$('#refresh').bind('click', function () {
	 	$('#table-user').DataTable().ajax.reload();
	});
});
function tambah(){       
	$("#myModal").modal('show');
	$('.modal-title').text("Add User");
	$('.modal-body').html('<center>Loading..</center>');
	$('.modal-body').load('add_user_view');
	return false;
}
function edit_user(id){       
    $("#myModal").modal('show');
    $('.modal-title').text("Edit User");
    $('.modal-body').html('<center>Loading..</center>');
    $('.modal-body').load('edit_user_view?id='+id);
    return false;
}
function hapus(id) {
	swal({
	  title: "Apakah anda yakin?",
	  text: "Data akan di hapus?",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((wildelete) => {
	  if (wildelete) {
	  	$.ajax({
	  		url:'delete_user?id='+id,
	  		success:function(value){
	  			if(value=='sukses'){
	  				$("#alert_success").trigger("click", 'Hapus data Berhasil');
				    $('#table-user').DataTable().ajax.reload();
	  			}else{
	  				$("#alert_error").trigger("click", 'Hapus data gagal');
	  			}
	  		}

	  	});
	    
	  }
	});
}
