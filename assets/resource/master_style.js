$(document).ready(function() {

	$('.pilih').submit(function(){
		var kode_style = $('#kode_style').val();
		var base_url = $('#base_url').val();
		
		if(!kode_style){
			$.unblockUI();
			$("#alert_warning").trigger("click", 'Pilih Style Dulu');
		}
		var data = new FormData(this);
            
        $.ajax({
            url:'save_proses',
            type: "POST",
            data: data,
            contentType: false,       
            cache: false,          
            processData:false,
            success: function(response){
            	if (response=='sukses') {
            		$("#alert_success").trigger("click", 'Sukses');
            		$('#kode_style').val('');
            		window.location.replace(base_url);
            		hapus();
            	}
            }
        });	
		return false
	});
})
function search(){       
	$("#myModal").modal('show');
	$('.modal-title').text("Select Style");
	$('.modal-body').html('<center>Loading..</center>');
	$('.modal-body').load('load_view');
	return false;
}
var i = 1;
function add_item() {
	
    var proseslist = document.getElementById('proseslist');

//                membuat element
    var row = document.createElement('tr');
    var no = document.createElement('td');
    var nama = document.createElement('td');
    var aksi = document.createElement('td');
    aksi.setAttribute('width', '50px');

//                meng append element
    proseslist.appendChild(row);
    row.appendChild(no);
    row.appendChild(nama);
    row.appendChild(aksi);


//                membuat element input
    var nama_input = document.createElement('input');
    nama_input.setAttribute('class', 'form-control');
    nama_input.setAttribute('name', 'nama_input[' + i + ']');
    nama_input.setAttribute("type", "input");
    nama_input.setAttribute("required", "");
    

    var hapus = document.createElement('span');

//                meng append element input
	// no.appendChild(no);
    nama.appendChild(nama_input);
    aksi.appendChild(hapus);

    // no.innerHTML = i;

    hapus.innerHTML = "<a class='btn btn-danger'><i class='fa fa-times'></i> Hapus</a>";
//                membuat aksi delete element
    hapus.onclick = function () {
        row.parentNode.removeChild(row);
    };

    i++;
}
 function hapus(id) {
    var myNode = document.getElementById("proseslist");
	while (myNode.firstChild) {
	    myNode.removeChild(myNode.firstChild);
	}
}
function edit_view(id) {
	var modal = $('#myModal > div > div')
	$("#myModal").modal('show');
	modal.parent('.modal-dialog').addClass('modal-lg');
	$('.modal-title').text("Edit Proses");
	$('.modal-body').html('<center>Loading..</center>');
	$('.modal-body').load('Master_process/edit_view?id='+id);
}
function delete_id(id)
{
	console.log(id);
	var ele = id + 'tr';
    var elem = document.getElementById(ele);
    return elem.parentNode.removeChild(elem);
}
function edit_proses(id,act) {
	if (act=='open') {
		$('.proses_'+id).prop('disabled',false).addClass('actived');
	}else{
		$('.proses_'+id).prop('disabled',true).removeClass('actived');
		var data = 'name='+$('.proses_'+id).val()+'&id='+id;

		$.ajax({
			url:'edit_proses_submit',
			data:data,
			type:'POST',
			beforeSend: function () {
	          $.blockUI({
	              message: "<img src='" + url_loading + "' />",
	              css: {
	                backgroundColor: 'transaparant',
	                border: 'none',
	              }
	            });
	          }, 
			success:function(data){
				
			}
		})
	}
	$('.edit_'+id).toggle();
}